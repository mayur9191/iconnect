-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: iconnect
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `an_id` int(11) NOT NULL AUTO_INCREMENT,
  `an_text` longtext,
  `an_file` text,
  `an_date` date DEFAULT NULL,
  `an_created_by` varchar(55) DEFAULT NULL,
  `an_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`an_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
INSERT INTO `announcements` VALUES (1,'<h3><a href=\"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=wEyjlVMGIS.4u_V7MQTKxjUW6x7uqqlZrc88bj0fyvdq7RgZwrzw8FKsmNUCywYtna159O5TVHKZZI9NjOZi_VIsf.tMq9c1AfuqtxFNetHUbpJ6OordGJyptbLfw0FCsBn9cKqnxovn8O3uuOuFW5On.yPt4oPbEBUws9TWUrSXXSjc9bQjz7EmvQyMF6UfXM3ic9vkKzdWXRSiYM1KhFthg0th13h.bGzACrZbweLctiiRQe1DEWbFHXL4kq83v6b8s7yidEPe3A.Z9o6ccj3lA.cnpmo6TirRsfwnlQaaD5thaGmpQtXNxn8Y3wIc00H3e1oixfTLzYVWbT6UwA0HjVDwNG15MYjx7rGvnZ4RJJyaCk5FKgp9OhEA9sLEwVYctxPeKVd17Pg5iWgvUk_tOb.vq1eDxPBqbd4o0YV7U6uCE1dM789Ff1EOAI1vLvUAZYA2C6m_IZ8TOVNMDA3rSazrrMD_mZb.0JRHjAhW9OvtR__wJMMYZkVb8PSumq5YyU0Y_xA2kO9VH6qL.ZgHcT_pPOMKI.KhOGBZfH97iNVz3yXc4cMRDxbt217Qrb1aUiGN9KKzjH.0614e8C9FEwK4H.oxPT343Nvs9TtaaR1hPonL5SsSYRRU%26lp=https%3A%2F%2Fonlineinsurance.hdfclife.com%2Fbuy-online-term-insurance-plans%2Fclick-2-protect-plus%3Fsource%3DY_Yahoo_C2PP_Native-Gentleman_2017-searchRT\" target=\"_blank\">A Term Plan Would Help You Save Tax Too.Plan Now</a></h3>\r\n','announcement_-v9CntpucfwbZ4IGg2BMV.jpg','2017-03-07','1',NULL),(2,'','announcement_-y7RLamtX31lbOQgEuMqY.png','2017-03-07','1',NULL),(3,'','announcement_-3MTiwLgCNXKGOapBEdbe.png','2017-03-07','1',NULL),(4,'','announcement_-2fQqZzpbs3vm7adRGKxW.jpg','2017-03-08','1',NULL),(5,'','announcement_-k1RfKZHvBM8rmlow2JTX.jpg','2017-03-08','1',NULL);
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `behave_soft_skils_list`
--

DROP TABLE IF EXISTS `behave_soft_skils_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `behave_soft_skils_list` (
  `tbl_primary_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `duration_of_days` int(11) NOT NULL,
  `faculty_source` varchar(100) NOT NULL,
  PRIMARY KEY (`tbl_primary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `behave_soft_skils_list`
--

LOCK TABLES `behave_soft_skils_list` WRITE;
/*!40000 ALTER TABLE `behave_soft_skils_list` DISABLE KEYS */;
INSERT INTO `behave_soft_skils_list` VALUES (1,'Achieve More','Behavioral',2,'External'),(2,'Coping with Stress for Personal Effectiveness','Behavioral',1,'Internal '),(3,'Enhancing Team Effectiveness','Behavioral',2,'Internal'),(4,'Managing Time for personal effectiveness','Behavioral',1,'Internal'),(5,'Understanding self through transactional Analysis','Behavioral',1,'External'),(6,'Working in a team','Behavioral',2,'Internal'),(7,'Becoming an Effective Mentor','Soft Skills',2,'External'),(8,'Being assertive for results','Soft Skills',1,'Internal\r\n'),(9,'Business Communication for Effective Functioning','Soft Skills',2,'External'),(10,'Business Etiquette','Soft Skills',2,'External'),(11,'Communication & Presentation Skills','Soft Skills',2,'Internal'),(12,'Enhancing Managerial Effectiveness\r\n','Soft Skills',2,'Internal'),(13,'Foundations of Management','Soft Skills',2,'Internal'),(14,'Giving and Receiving Feedback','Soft Skills',1,'Internal'),(15,'Goal Setting and Action Planning\r\n','Soft Skills\r\n',2,'Internal'),(16,'Interviewing Skills','Soft Skills',1,'Internal'),(17,'Motivating Team for Execution Excellence','Soft Skills',2,'Internal'),(18,'Internal','Soft Skills',3,'Internal');
/*!40000 ALTER TABLE `behave_soft_skils_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `birthday_comments`
--

DROP TABLE IF EXISTS `birthday_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `birthday_comments` (
  `bc_id` int(11) NOT NULL AUTO_INCREMENT,
  `bc_userid` varchar(255) NOT NULL,
  `bc_brith_uid` varchar(255) NOT NULL,
  `bc_comment_group` varchar(255) NOT NULL,
  `bc_comment` text NOT NULL,
  `bc_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `birthday_comments`
--

LOCK TABLES `birthday_comments` WRITE;
/*!40000 ALTER TABLE `birthday_comments` DISABLE KEYS */;
INSERT INTO `birthday_comments` VALUES (5,'6','6','1488803335','happy bithday #ravikimar asdasd ','2017-03-06 12:28:55'),(6,'6','2','1488803335','happy bithday #ravikimar asdasd ','2017-03-06 12:28:55');
/*!40000 ALTER TABLE `birthday_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `birthday_likes`
--

DROP TABLE IF EXISTS `birthday_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `birthday_likes` (
  `bl_id` int(11) NOT NULL AUTO_INCREMENT,
  `bl_buid` varchar(255) NOT NULL,
  `bl_user_id` varchar(255) NOT NULL,
  `bl_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `birthday_likes`
--

LOCK TABLES `birthday_likes` WRITE;
/*!40000 ALTER TABLE `birthday_likes` DISABLE KEYS */;
INSERT INTO `birthday_likes` VALUES (1,'2','2','2017-03-06 13:14:34'),(2,'2','2','2017-03-06 13:14:34'),(3,'2','5','2017-03-06 13:14:34'),(4,'SG0155','SG0175              ','2017-03-06 13:25:31'),(5,'SG0155','SG0174              ','2017-03-06 13:25:31'),(6,'SG0155','SG0140              ','2017-03-06 13:25:32'),(7,'SG0155','SG0155              ','2017-03-06 13:25:32'),(8,'SG0155','SG0098              ','2017-03-06 13:25:32'),(9,'SG0155','SG0133              ','2017-03-06 13:25:32');
/*!40000 ALTER TABLE `birthday_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `birthday_wishes`
--

DROP TABLE IF EXISTS `birthday_wishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `birthday_wishes` (
  `bw_id` int(11) NOT NULL AUTO_INCREMENT,
  `bw_post_user_id` int(11) NOT NULL,
  `bw_user_id` int(11) NOT NULL,
  `bw_image` varchar(255) DEFAULT NULL,
  `bw_comment` text NOT NULL,
  `bw_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`bw_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `birthday_wishes`
--

LOCK TABLES `birthday_wishes` WRITE;
/*!40000 ALTER TABLE `birthday_wishes` DISABLE KEYS */;
INSERT INTO `birthday_wishes` VALUES (1,0,0,'profile_4-O3BNblC5F6MhErD08fLm.jpg','','2017-03-04 11:22:31'),(4,4,0,'profile_4-Nk92C1Tsva3Mnp4tIxjG.png','','2017-03-04 11:35:43');
/*!40000 ALTER TABLE `birthday_wishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celebration`
--

DROP TABLE IF EXISTS `celebration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celebration` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_title` varchar(255) NOT NULL,
  `c_image` varchar(255) DEFAULT NULL,
  `c_date` date NOT NULL,
  `c_user_id` int(11) NOT NULL,
  `c_post_user_id` int(11) NOT NULL,
  `c_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celebration`
--

LOCK TABLES `celebration` WRITE;
/*!40000 ALTER TABLE `celebration` DISABLE KEYS */;
/*!40000 ALTER TABLE `celebration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celebration	_commets`
--

DROP TABLE IF EXISTS `celebration	_commets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celebration	_commets` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_cid` int(11) NOT NULL,
  `cc_user_id` int(11) NOT NULL,
  `cc_comment` text NOT NULL,
  `cc_crate_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celebration	_commets`
--

LOCK TABLES `celebration	_commets` WRITE;
/*!40000 ALTER TABLE `celebration	_commets` DISABLE KEYS */;
/*!40000 ALTER TABLE `celebration	_commets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celebration_likes`
--

DROP TABLE IF EXISTS `celebration_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celebration_likes` (
  `cl_id` int(11) NOT NULL AUTO_INCREMENT,
  `cl_cid` int(11) NOT NULL,
  `cl_user_id` int(11) NOT NULL,
  `cl_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celebration_likes`
--

LOCK TABLES `celebration_likes` WRITE;
/*!40000 ALTER TABLE `celebration_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `celebration_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `du_emp_personal_detail_upd`
--

DROP TABLE IF EXISTS `du_emp_personal_detail_upd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `du_emp_personal_detail_upd` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_row_id` varchar(45) DEFAULT NULL,
  `emp_staffid` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `known_name` varchar(45) DEFAULT NULL,
  `official_name` varchar(45) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `state_of_birth` varchar(45) DEFAULT NULL,
  `country_of_birth` varchar(45) DEFAULT NULL,
  `marital_status` varchar(45) DEFAULT NULL,
  `wedding_date` datetime DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `pan_number` varchar(45) DEFAULT NULL,
  `previous_name` varchar(45) DEFAULT NULL,
  `languages` varchar(45) DEFAULT NULL,
  `blood_group` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `aadhar_no` varchar(45) DEFAULT NULL,
  `ethinic` varchar(45) DEFAULT NULL,
  `emp_is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`auto_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `du_emp_personal_detail_upd`
--

LOCK TABLES `du_emp_personal_detail_upd` WRITE;
/*!40000 ALTER TABLE `du_emp_personal_detail_upd` DISABLE KEYS */;
/*!40000 ALTER TABLE `du_emp_personal_detail_upd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_speaks`
--

DROP TABLE IF EXISTS `people_speaks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_speaks` (
  `ps_id` int(11) NOT NULL AUTO_INCREMENT,
  `ps_user_id` int(11) NOT NULL,
  `ps_comment` text NOT NULL,
  `ps_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ps_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_speaks`
--

LOCK TABLES `people_speaks` WRITE;
/*!40000 ALTER TABLE `people_speaks` DISABLE KEYS */;
/*!40000 ALTER TABLE `people_speaks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_slides`
--

DROP TABLE IF EXISTS `profile_slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_slides` (
  `sl_id` int(11) NOT NULL AUTO_INCREMENT,
  `sl_image` varchar(255) NOT NULL,
  `sl_comment` varchar(255) DEFAULT NULL,
  `sl_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_slides`
--

LOCK TABLES `profile_slides` WRITE;
/*!40000 ALTER TABLE `profile_slides` DISABLE KEYS */;
INSERT INTO `profile_slides` VALUES (1,'slide_-zlHt9VPOWUs1jeYL7oT0.jpg',NULL,'2017-03-07 12:18:44'),(2,'slide_-L15CkJXHNZ4pQ6SDGKqv.jpg',NULL,'2017-03-07 12:18:56'),(3,'slide_-DIi0u4rgZTzjf9lSJbnY.jpg',NULL,'2017-03-07 12:19:34'),(10,'slide_-w7pURWx2SOdLukZ6on9Q.jpg',NULL,'2017-03-10 13:21:27'),(11,'slide_-RrAS0NHkM37v2gK9JZsu.jpg',NULL,'2017-03-11 10:49:53'),(12,'slide_-gtzGRhuF2YVHOdbBN6ln.jpg',NULL,'2017-03-11 10:53:35');
/*!40000 ALTER TABLE `profile_slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qhse_list`
--

DROP TABLE IF EXISTS `qhse_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qhse_list` (
  `tbl_primary_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `duration_in_days` int(11) NOT NULL,
  `faculty_source` varchar(100) NOT NULL,
  PRIMARY KEY (`tbl_primary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qhse_list`
--

LOCK TABLES `qhse_list` WRITE;
/*!40000 ALTER TABLE `qhse_list` DISABLE KEYS */;
INSERT INTO `qhse_list` VALUES (1,'5S Awareness','QHSE',1,'Internal'),(2,'7QC tools','QHSE',1,'Internal'),(3,'Awareness of COQ/COPQ','QHSE',1,'Internal'),(4,'Awareness on ISO 9001: 2008','QHSE',1,'Internal'),(5,'Basic Electrical Safety','QHSE',1,'External'),(6,'Behavioral Based Safety','QHSE',1,'External'),(7,'Crane Safety','QHSE',1,'External'),(8,'Defensive Driving','QHSE',1,'External'),(9,'EMS (ISO 14001 & OHSAS 18001)','QHSE',1,'Internal'),(10,'Fire Fighting -Basic','QHSE',1,'Internal'),(11,'First Aid','QHSE',1,'Internal'),(12,'Hand & Power Tool Safety','QHSE',1,'External'),(13,'Height Evacuation','QHSE',1,'External'),(14,'Lean Manufacturing','QHSE',2,'Internal'),(15,'Lock Out & Tag Out','QHSE',1,'External'),(16,'Material Handling Safety','QHSE',1,'External'),(17,'Quality Circles','QHSE',1,'Internal'),(18,'Root Cause Analysis','QHSE',1,'Internal'),(19,'Safety Management','QHSE\r\n',1,'Internal'),(20,'Six Sigma Awareness','QHSE',2,'Internal'),(21,'Working in Confined Space','QHSE',1,'External');
/*!40000 ALTER TABLE `qhse_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_report`
--

DROP TABLE IF EXISTS `survey_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(255) DEFAULT NULL,
  `employee_code` varchar(255) DEFAULT NULL,
  `location` text,
  `name_of_business` text,
  `name_of_reporting_manager` varchar(255) DEFAULT NULL,
  `name_of_hod` varchar(255) DEFAULT NULL,
  `tech_func_training_need_one` int(11) DEFAULT NULL,
  `tech_func_training_need_two` int(11) DEFAULT NULL,
  `beh_soft_skills_training_need_one` int(11) DEFAULT NULL,
  `beh_soft_skills_training_need_two` int(11) DEFAULT NULL,
  `qhse_training_need_one` int(11) DEFAULT NULL,
  `qhse_training_need_two` int(11) DEFAULT NULL,
  `tr_need_one` text,
  `tr_respondent_one` text,
  `tr_need_two` text,
  `tr_respondent_two` text,
  `is_save` tinyint(1) DEFAULT '1',
  `is_submit` tinyint(1) DEFAULT '0',
  `is_report_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`report_id`),
  FULLTEXT KEY `employee_name` (`employee_name`,`employee_code`,`location`,`name_of_business`,`name_of_reporting_manager`,`name_of_hod`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_report`
--

LOCK TABLES `survey_report` WRITE;
/*!40000 ALTER TABLE `survey_report` DISABLE KEYS */;
INSERT INTO `survey_report` VALUES (1,'Ravi','SG0307','Hyderabad','Job','Vijay Maurya','Vijay Maurya',13,2,12,9,12,10,'Training need 1','','Training need 2','',1,0,1),(2,'Mayank Thapliyal','SG0300','Pune','Project Development','Vijay Maurya','Vijay Maurya',1,2,6,17,1,17,'basic',NULL,'advance',NULL,0,1,1),(3,'Vijay Maurya','SG0261','Pune','IT','GRT','GRT',2,3,4,15,3,4,'','','','',1,0,1),(4,'Yasmeen Bashir Baig','SG0155              ','Pune','test','Vijay Maurya','Vijay Maurya',0,0,0,0,0,0,'','','','',1,0,1);
/*!40000 ALTER TABLE `survey_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tec_func_list`
--

DROP TABLE IF EXISTS `tec_func_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tec_func_list` (
  `tbl_primary_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_no` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `duration_no_of_days` varchar(100) NOT NULL,
  `faculty_source` varchar(255) NOT NULL,
  PRIMARY KEY (`tbl_primary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tec_func_list`
--

LOCK TABLES `tec_func_list` WRITE;
/*!40000 ALTER TABLE `tec_func_list` DISABLE KEYS */;
INSERT INTO `tec_func_list` VALUES (1,'1','Advanced PowerPoint','Functional','2','External'),(2,'2','Basic Excel','Functional','1','External'),(3,'','Basic Power Point','Functional','1','External'),(4,'4','Conversational English ','Functional','3','External'),(5,'5','Essentials of Wind Power Generation','Functional','1','Internal'),(6,'6','Finance for Non-Finance Executives','Functional','2','External'),(7,'7','Supervisory Development (Team Leaders)','Functional','2','External'),(8,'8','Advance excel','Functional ','2','External'),(9,'9','E-mail Etiquette','Functional','1','External'),(10,'10','Basics of Hydraulics','Technical','1','Internal'),(11,'11','Basics of Transformer','Technical','1','Internal'),(12,'12','DFIG  Troubleshooting ','Technical','1','Internal'),(13,'13','Electrical fundamentals ','Technical','1','Internal'),(14,'14','Electrical Installation & HT Design ','Technical','1','Internal'),(15,'15','Electronics Componants of WTG','Technical','1','Internal\r\n'),(16,'16','Gear box,couplings,yaw and Pitch Drive in WTG','Technical','1','Internal'),(17,'17','Induction Generator and Power Factor','Technical','1','Internal'),(18,'18','Introduction to Control system and PLCs','Technical','1','Internal'),(19,'19','Power Electronics ','Technical','1','Internal'),(20,'20','Power Evacuation','Technical','1','Internal'),(21,'21','Sensors in WTG\r\n','Technical','1','Internal'),(22,'22','Switchgears in WTG','Technical','1','Internal'),(23,'23','Understanding Electrical Drawings','Technical','1','Internal'),(24,'24','Vibration Analysis','Technical','1','Internal'),(25,'25','Welding Technology','Technical','1','Internal'),(26,'26','WTG - Components Training','Technical','1','Internal'),(27,'27','WTG - Manufacturing','Technical','1','Internal'),(28,'28','WTG Electrical & Electronics','Technical','1','Internal'),(29,'29','WTG Installation - Electrical & Mechanical','Technical','1','Internal');
/*!40000 ALTER TABLE `tec_func_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_connections`
--

DROP TABLE IF EXISTS `user_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_connections` (
  `uc_id` int(11) NOT NULL,
  `uc_user_id` int(11) NOT NULL,
  `uc_connect_user` int(11) NOT NULL,
  `uc_connect_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uc_connect_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_connections`
--

LOCK TABLES `user_connections` WRITE;
/*!40000 ALTER TABLE `user_connections` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_wall_updates`
--

DROP TABLE IF EXISTS `user_wall_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_wall_updates` (
  `uw_id` int(11) NOT NULL AUTO_INCREMENT,
  `uw_user_id` int(11) NOT NULL,
  `uw_wall_text` text NOT NULL,
  `uw_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uw_status` int(11) NOT NULL,
  PRIMARY KEY (`uw_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_wall_updates`
--

LOCK TABLES `user_wall_updates` WRITE;
/*!40000 ALTER TABLE `user_wall_updates` DISABLE KEYS */;
INSERT INTO `user_wall_updates` VALUES (1,7,'hello','2017-03-10 06:15:05',1),(2,7,'hi','2017-03-10 06:15:05',1),(3,8,'adasasd','2017-03-10 06:15:05',1),(4,1,'sdfsdfsdsdf','2017-03-10 06:15:05',1),(5,5,'Hello','2017-03-10 06:15:05',1),(6,5,'Lost ID card on 3rd floor if found please deposit at reception','2017-03-10 06:15:05',1),(7,5,'.','2017-03-15 07:26:17',0);
/*!40000 ALTER TABLE `user_wall_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_role` int(11) NOT NULL COMMENT '0=user, 1=admin',
  `u_ldap_userid` varchar(255) NOT NULL,
  `u_employeeid` varchar(255) NOT NULL,
  `u_name` varchar(255) NOT NULL,
  `u_email` varchar(255) NOT NULL,
  `u_mobile` varchar(255) DEFAULT NULL,
  `u_password` varchar(255) NOT NULL,
  `u_designation` varchar(255) NOT NULL,
  `u_image` varchar(255) DEFAULT NULL,
  `u_cover_image` varchar(255) DEFAULT NULL,
  `u_streetaddress` varchar(255) DEFAULT NULL,
  `u_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_last_login` timestamp NULL DEFAULT NULL,
  `u_update_date` timestamp NULL DEFAULT NULL,
  `u_status` int(11) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Yasmeen.Baig','SG0155','Yasmeen Baig','Yasmeen.Baig@skeiron.com','','','ENGINEER- Quantity Surveyor',NULL,NULL,'Godrej Millennium 5th Floor Koregaon Park Main Road Pune 411001','2017-03-15 06:19:52','2017-03-15 06:19:52',NULL,1),(2,0,'Vijay.Maurya','SG0263','Vijay Maurya','Vijay.Maurya@skeiron.com','+91 7888041907','','Head IT (ITSS)','profile_2-PGmxoIrzNQ5UOEgVtpwX.jpg',NULL,'5th Floor, Godrej Millennium, 9, Koregaon Park, Pune.','2017-03-15 11:53:30','2017-03-15 11:51:30',NULL,1),(3,0,'sunil.nale','SG0283','sunil nale','sunil.nale@skeiron.com','9763055244','','Ass.Manager IT',NULL,NULL,'','2017-03-10 05:30:00','2017-03-10 05:30:00',NULL,1),(4,0,'ravi.kumar','SG0307','ravi kumar','ravi.kumar@skeiron.com','','','',NULL,NULL,'','2017-03-10 12:32:46','2017-03-10 12:32:46',NULL,1),(5,0,'Nelson.Komathan','SG0282','Nelson Komathan','Nelson.Komathan@skeiron.com','+91 831975953','','Senior Executive – IT Procurement','profile_5-gG2YIth4lkKA7zHsFyuN.jpg',NULL,'','2017-03-15 11:19:00','2017-03-15 11:19:00',NULL,1),(6,0,'Mayank.Thapliyal','SG0300','Mayank Thapliyal','Mayank.Thapliyal@skeiron.com','','','','profile_6-bUBzf3p7dkChxtFGTJce.jpg',NULL,'','2017-03-15 07:32:49','2017-03-15 07:32:49',NULL,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-15 17:46:03
