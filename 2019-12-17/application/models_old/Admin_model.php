<?php
class Admin_model extends CI_Model {
	
	public function __Construct(){
       parent::__Construct();
	}
	
//iconnect start 
	public function get_all_announcements(){
		$qry = $this->db->select("*")->from("announcements")->where('is_deleted',0)->order_by("an_id","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_announcements_by_id($announcements_id = 0){
		if($announcements_id > 0){
			$qry = $this->db->select("*")->from("announcements")->where("an_id",$announcements_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	
	//TV 
	public function get_all_announcements_tv(){
		$qry = $this->db->select("*")->from("announcements_tv")->where('is_deleted',0)->order_by("an_id","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_announcements_by_id_tv($announcements_id = 0){
		if($announcements_id > 0){
			$qry = $this->db->select("*")->from("announcements_tv")->where("an_id",$announcements_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	
	public function get_all_thought(){
		$qry = $this->db->select("*")->from("iconnect_thought")->order_by("thought_id","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_thought_by_id($thought_id=0){
		if($thought_id > 0){
			$qry = $this->db->select("*")->from("iconnect_thought")->where("thought_id",$thought_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	public function get_all_archive(){
		$qry = $this->db->select("*")->from("doc_folder")->order_by("doc_folder_id","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_archive_by_id($archive_id=0){
		if($archive_id > 0){
			$qry = $this->db->select("*")->from("doc_folder")->where("doc_folder_id",$archive_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	public function get_all_archivefiles($archive_id = 0){
		$qry = $this->db->select("*")->from("doc_files")->where('doc_folder_id',$archive_id)->order_by("doc_file_id","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_archivefiles_by_id($archivefile_id=0,$archive_id=0){
		if($archivefile_id > 0){
			$qry = $this->db->select("*")->from("doc_files")->where('doc_folder_id',$archive_id)->where("doc_file_id",$archivefile_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
//iconnect end
	
	
//helpdesk start 

	public function get_all_guideline(){
		$qry = $this->db->select("*")->from("dept_guideline_hd")->order_by("dept_guideline_hd_id","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_guideline_by_id($guideline_id=0){
		if($guideline_id > 0){
			$qry = $this->db->select("*")->from("dept_guideline_hd")->where("dept_guideline_hd_id",$guideline_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_department(){
		$qry = $this->db->select("*")->from("dept_hd")->order_by("dept_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_department_by_id($dept_id=0){
		if($dept_id > 0){
			$qry = $this->db->select("*")->from("dept_hd")->where("dept_id",$dept_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_category(){
		$qry = $this->db->select("*")->from("category_hd")->order_by("category_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_category_by_id($category_id=0){
		if($category_id > 0){
			$qry = $this->db->select("*")->from("category_hd")->where("category_id",$category_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	public function get_all_subcategory(){
		$qry = $this->db->select("*")->from("sub_category_hd")->order_by("sub_category_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_subcategory_by_id($subcategory_id=0){
		if($subcategory_id > 0){
			$qry = $this->db->select("*")->from("sub_category_hd")->where("sub_category_id",$subcategory_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_item(){
		$qry = $this->db->select("*")->from("items_hd")->order_by("items_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_item_by_id($item_id=0){
		if($item_id > 0){
			$qry = $this->db->select("*")->from("items_hd")->where("items_id",$item_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_quicklink(){
		$qry = $this->db->select("*")->from("quick_link_hd")->order_by("quick_link_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_quicklink_by_id($quicklink_id=0){
		if($quicklink_id > 0){
			$qry = $this->db->select("*")->from("quick_link_hd")->where("quick_link_id",$quicklink_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_group(){
		$qry = $this->db->select("*")->from("groups_hd")->order_by("group_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_group_by_id($group_id=0){
		if($group_id > 0){
			$qry = $this->db->select("*")->from("groups_hd")->where("group_id",$group_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_technician(){
		$qry = $this->db->select("*")->from("technician_hd")->order_by("technician_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_technician_by_id($technician_id=0){
		if($technician_id > 0){
			$qry = $this->db->select("*")->from("technician_hd")->where("technician_id",$technician_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	public function get_all_assigntech(){
		$qry = $this->db->select("*")->from("technician_group_map_hd")->order_by("technician_group_map_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_assigntech_by_id($assign_id=0){
		if($assign_id > 0){
			$qry = $this->db->select("*")->from("technician_group_map_hd")->where("technician_group_map_id",$assign_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	public function get_all_implink(){
		$qry = $this->db->select("*")->from("implinks")->order_by("implink_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_implink_by_id($implink_id=0){
		if($implink_id > 0){
			$qry = $this->db->select("*")->from("implinks")->where("implink_id",$implink_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
	public function get_all_assignitem(){
		$qry = $this->db->select("*")->from("group_item_map_hd")->order_by("group_item_map_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_assignitem_by_id($assign_id=0){
		if($assign_id > 0){
			$qry = $this->db->select("*")->from("group_item_map_hd")->where("group_item_map_id",$assign_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_deptannouncement(){
		$qry = $this->db->select("*")->from("announcements_hd")->order_by("announcement_id","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_deptannouncement_by_id($deptannouncement_id=0){
		if($deptannouncement_id > 0){
			$qry = $this->db->select("*")->from("announcements_hd")->where("announcement_id",$deptannouncement_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_all_iplink(){
		$qry = $this->db->select("*")->from("ip_monitor")->order_by("link_down_count","DESC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_iplink_by_id($iplink_id=0){
		if($iplink_id > 0){
			$qry = $this->db->select("*")->from("ip_monitor")->where("ip_monitor_id",$iplink_id)->get();
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	
//helpdesk end
	
}