<?php
class Survey_model extends CI_Model {

	public function __Construct()
	{
       parent::__Construct();
	}
	
	public function get_behave_soft_skils_list(){
		$qry = $this->db->select("*")->from("behave_soft_skils_list")->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function get_qhse_list(){
		$qry = $this->db->select("*")->from("qhse_list")->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function get_tec_func_list(){
		$qry = $this->db->select("*")->from("tec_func_list")->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function get_emp_data($empCode=''){
		$empData = array(
			'employee_name'=>'',
			'employee_code'=>'',
			'location'=>'',
			'name_of_business'=>'',
			'name_of_reporting_manager'=>'',
			'name_of_hod'=>'',
			'tech_func_training_need_one'=>'',
			'tech_func_training_need_two'=>'',
			'beh_soft_skills_training_need_one'=>'',
			'beh_soft_skills_training_need_two'=>'',
			'qhse_training_need_one'=>'',
			'qhse_training_need_two'=>'',
			'tr_need_one'=>'',
			'tr_need_two'=>'',
			'is_save'=>'',
			'is_submit'=>''
		);
		
		if($empCode !=''){
			$qry = $this->db->select("*")->from("survey_report")->where('employee_code',$empCode)->get();
			if($qry->num_rows() > 0){
				$resultData = $qry->row();
				$empData = array(
					'employee_name'=>$resultData->employee_name,
					'employee_code'=>$resultData->employee_code,
					'location'=>$resultData->location,
					'name_of_business'=>$resultData->name_of_business,
					'name_of_reporting_manager'=>$resultData->name_of_reporting_manager,
					'name_of_hod'=>$resultData->name_of_hod,
					'tech_func_training_need_one'=>$resultData->tech_func_training_need_one,
					'tech_func_training_need_two'=>$resultData->tech_func_training_need_two,
					'beh_soft_skills_training_need_one'=>$resultData->beh_soft_skills_training_need_one,
					'beh_soft_skills_training_need_two'=>$resultData->beh_soft_skills_training_need_two,
					'qhse_training_need_one'=>$resultData->qhse_training_need_one,
					'qhse_training_need_two'=>$resultData->qhse_training_need_two,
					'tr_need_one'=>$resultData->tr_need_one,
					'tr_need_two'=>$resultData->tr_need_two,
					'is_save'=>$resultData->is_save,
					'is_submit'=>$resultData->is_submit
				);
			}else{
				//get the data from ldap
				$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $empCode . "'";
				$mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
				$hrdata_user_info = $mssql_user_info_query->result_array();
				
				$empData = array(
					'employee_name'=>$hrdata_user_info[0]['OFFICIAL_NAME'],
					'employee_code'=>$hrdata_user_info[0]['EMP_STAFFID'],
					'location'=>$hrdata_user_info[0]['PLACE_OF_BIRTH'],
					'name_of_business'=>$hrdata_user_info[0]['description'],
					'name_of_reporting_manager'=>'',
					'name_of_hod'=>'',
					'tech_func_training_need_one'=>'',
					'tech_func_training_need_two'=>'',
					'beh_soft_skills_training_need_one'=>'',
					'beh_soft_skills_training_need_two'=>'',
					'qhse_training_need_one'=>'',
					'qhse_training_need_two'=>'',
					'tr_need_one'=>'',
					'tr_need_two'=>'',
					'is_save'=>'',
					'is_submit'=>''
				);
			}
		}
		return $empData;
	}
	
	//
	public function checkEmp($empCode){
		$status = true;
		$qry = $this->db->select("*")->from("survey_report")->where('employee_code',$empCode)->get();
		if($qry->num_rows() > 0){
			$status = false;
		}
		return $status;
	}
	
	//
	public function getReportData(){
		$qry = $this->db->select("*")->from("survey_report")->where('is_submit',1)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//
	public function customSearch($searchText='',$searchField=''){
		$returnData = array();
		if($searchField !=''){
			//search filter by field
			$qry = $this->db->query("SELECT * FROM survey_report WHERE ".$searchField." LIKE '%".trim($searchText)."%' AND is_submit=1");
		}else{
			//search all 
			$qry = $this->db->query("SELECT * FROM survey_report WHERE MATCH(employee_name, employee_code, location, name_of_business, name_of_reporting_manager, name_of_hod) AGAINST ('".$searchText."') AND is_submit=1");
		}
		
		//show all 
		if($searchText=='' && $searchField==''){
			$qry = $this->db->select("*")->from("survey_report")->where('is_submit',1)->get();
		}
		
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}
		return $returnData;
	}
	
	
	//apparelreport 21-3-2017
	public function getApparelReportData(){
		$qry = $this->db->select("*")->from("apparel_survey")->where('is_active',1)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	
	public function get_apparel_data($emp_code){
		$qry = $this->db->select("*")->from("apparel_survey")->where('emp_code',$emp_code)->where('is_active',1)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//V2 start
	public function getReportDataV2(){
		$qry = $this->db->select("*")->from("survey_report_v2")->where('is_submit',1)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	public function checkEmpV2($empCode){
		$status = true;
		$qry = $this->db->select("*")->from("survey_report_v2")->where('emp_code',$empCode)->get();
		if($qry->num_rows() > 0){
			$status = false;
		}
		return $status;
	}
	
	public function get_emp_data_V2($empCode=''){
		$empData = array(
			'emp_name'=>'',
			'business_name'=>'',
			'emp_code'=>'',
			'emp_location'=>'',
			'emp_reporting_manager'=>'',
			'emp_hod'=>'',
			'ldp_mdp_1'=>'',
			'sub_ldp_mdp_1'=>'',
			'ldp_mdp_2'=>'',
			'sub_ldp_mdp_2'=>'',
			'fun_tech_1'=>'',
			'sub_fun_tech_1'=>'',
			'fun_tech_2'=>'',
			'sub_fun_tech_2'=>'',
			'pd_beha_1'=>'',
			'sub_pd_beha_1'=>'',
			'pd_beha_2'=>'',
			'sub_pd_beha_2'=>'',
			'qhse_list_1'=>'',
			'qhse_list_2'=>'',
			'tr_need_1'=>'',
			'tr_respondent_1'=>'',
			'tr_need_2'=>'',
			'tr_respondent_2'=>'',
			'is_save'=>'',
			'is_submit'=>0
		);
		
		if($empCode !=''){
			$qry = $this->db->select("*")->from("survey_report_v2")->where('emp_code',$empCode)->get();
			if($qry->num_rows() > 0){
				$postData = $qry->row_array();
				$empData = array(
					'emp_name'=>$postData['emp_name'],
					'business_name'=>$postData['business_name'],
					'emp_code'=>$postData['emp_code'],
					'emp_location'=>$postData['emp_location'],
					'emp_reporting_manager'=>$postData['emp_reporting_manager'],
					'emp_hod'=>$postData['emp_hod'],
					'ldp_mdp_1'=>$postData['ldp_mdp_1'],
					'sub_ldp_mdp_1'=>$postData['sub_ldp_mdp_1'],
					'ldp_mdp_2'=>$postData['ldp_mdp_2'],
					'sub_ldp_mdp_2'=>$postData['sub_ldp_mdp_2'],
					'fun_tech_1'=>$postData['fun_tech_1'],
					'sub_fun_tech_1'=>$postData['sub_fun_tech_1'],
					'fun_tech_2'=>$postData['fun_tech_2'],
					'sub_fun_tech_2'=>$postData['sub_fun_tech_2'],
					'pd_beha_1'=>$postData['pd_beha_1'],
					'sub_pd_beha_1'=>$postData['sub_pd_beha_1'],
					'pd_beha_2'=>$postData['pd_beha_2'],
					'sub_pd_beha_2'=>$postData['sub_pd_beha_2'],
					'qhse_list_1'=>$postData['qhse_list_1'],
					'qhse_list_2'=>$postData['qhse_list_2'],
					'tr_need_1'=>$postData['tr_need_1'],
					'tr_respondent_1'=>$postData['tr_respondent_1'],
					'tr_need_2'=>$postData['tr_need_2'],
					'tr_respondent_2'=>$postData['tr_respondent_2'],
					'is_save'=>$postData['is_save'],
					'is_submit'=>$postData['is_submit']
				);
			}else{
				//get the data from ldap
				$mssql_user_info_sql = "SELECT * FROM du_emp_personal_detail_upd WHERE emp_staffid = '" . $empCode . "'";
				$mssql_user_info_query = $this->db->query($mssql_user_info_sql);
				$hrdata_user_info = $mssql_user_info_query->result_array();
				
				$empData = array(
					'emp_name'=>$hrdata_user_info[0]['official_name'],
					'business_name'=>'',
					'emp_code'=>$empCode,
					'emp_location'=>$hrdata_user_info[0]['state_of_birth'],
					'emp_reporting_manager'=>'',
					'emp_hod'=>'',
					'ldp_mdp_1'=>'',
					'sub_ldp_mdp_1'=>'',
					'ldp_mdp_2'=>'',
					'sub_ldp_mdp_2'=>'',
					'fun_tech_1'=>'',
					'sub_fun_tech_1'=>'',
					'fun_tech_2'=>'',
					'sub_fun_tech_2'=>'',
					'pd_beha_1'=>'',
					'sub_pd_beha_1'=>'',
					'pd_beha_2'=>'',
					'sub_pd_beha_2'=>'',
					'qhse_list_1'=>'',
					'qhse_list_2'=>'',
					'tr_need_1'=>'',
					'tr_respondent_1'=>'',
					'tr_need_2'=>'',
					'tr_respondent_2'=>'',
					'is_save'=>'',
					'is_submit'=>0
				);
			}
		}
		return $empData;
	}
	//V2 end
}