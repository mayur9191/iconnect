<?php
class Helpdesk_model extends CI_Model {

	public function __Construct()
	{
       parent::__Construct();
	}
	public function get_departments()
	{
	  $qry = $this->db->select("*")->from("dept_hd")->where("dept_is_active",'1')->get();
	    if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
		
	}
	public function get_categories_hd($dept_id=0){
		if($dept_id >0){
			$qry = $this->db->select("*")->from("category_hd")->where("dept_id",$dept_id)->where("category_is_active",'1')->where('category_id !=','62')->get();
		}
		else{
			$qry = $this->db->select("*")->from("category_hd")->where("category_is_active",'1')->where('category_id !=','62')->get();
		}
		
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function get_subcategory_hd($category_id)
	{
		$qry = $this->db->select("*")->from("sub_category_hd")->where("category_id",$category_id)->where("sub_category_is_active",'1')->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	public function get_item_hd($sub_category_id)
	{
		$qry = $this->db->select("*")->from("items_hd")->where("sub_category_id",$sub_category_id)->where("item_is_active",'1')->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function get_location_hd($state_id)
	{
		$qry = $this->db->select("*")->from("site_locations_hd")->where("state_id",$state_id)->where("site_location_is_active",'1')->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function save_user_request()
	{	
		$user['user_ldamp'] = $this->user_ldamp;
		$postData = $this->input->post();
		if(isset($postData['behalf_of'])){
			$behalf_of = $postData['behalf_of'];
			$request_type = 'other';
		}else{
			$behalf_of = '';
			$request_type = 'self';
		}
		
		$data = array(
			'user_id'=>$user['user_ldamp']['user_id'],
			'user_code'=>$user['user_ldamp']['employeeid'],
			'request_type'=>'portal', 
			'behalf_of'=>$behalf_of,
			'service_request_type'=>$postData['request_type'],
			'state_id'=>$postData['request_state'],
			'request_priority'=>$postData['priority'],
			'requester_name'=>$postData['request_name'],
			'requester_location_id'=>$postData['request_location'],
			'requester_location_name'=>$postData['requester_location_name'],
			'dept_id'=>$postData['dept_id'],
			'category_id'=>$postData['category'],
			'subcategory_id'=>$postData['sub_category'], 
			'item_id'=>$postData['item'], 
			'request_subject'=>$postData['request_subject'], 
			'request_desc'=>$postData['request_description'],
			//'technician_assign_status'=> 
			'request_created_dt'=>date('Y-m-d H:i:s')
 		);
		
		$this->db->insert('user_request_hd',$data);
		if($this->db->affected_rows()>0){
			return true;
		}
		return false;
		
	}
	// this function saves the self raised request by technician 
	public function save_user_request_self()
	{	
		$user['user_ldamp'] = $this->user_ldamp;
		
		$postData = $this->input->post();
		
		if(isset($postData['behalf_of'])){
			$behalf_of = $postData['behalf_of'];
			$request_type = 'other';
		}else{
			$behalf_of = '';
			$request_type = 'self';
		}
		$emp = getEmpByEmail($postData["behalf_of"]);
		//echo "<pre>";print_r($emp);print_r($postData);die;
		if(!empty($emp)){
			$behalf_of_ = $user['user_ldamp']['mail'];
			$requester_name = $emp->u_name;
			$emp_code = $emp->u_employeeid;
		}
		else
		{
			$behalf_of_ = $behalf_of;
			$requester_name = $postData['request_name'];
			$emp_code = $user['user_ldamp']['employeeid'];
		}
		
		if($postData['behalf_remark'] !=''){
			$behalf_remark = $postData['behalf_remark'];
		}else{
			$behalf_remark = '';
		}
		$data = array(
			'user_id'=>$user['user_ldamp']['user_id'],
			'user_code'=>$emp_code,
			'request_type'=>'portal', 
			'behalf_of'=>$behalf_of_,
			'service_request_type'=>$postData['request_type'],
			'state_id'=>$postData['request_state'],
			'request_priority'=>$postData['priority'],
			'requester_name'=>$requester_name,
			'requester_location_id'=>$postData['request_location'],
			'requester_location_name'=>$postData['requester_location_name'],
			'behalf_remark'=>$behalf_remark,
			'dept_id'=>$postData['dept_id'],
			'category_id'=>$postData['category'],
			'subcategory_id'=>$postData['sub_category'], 
			'item_id'=>$postData['item'], 
			'request_subject'=>$postData['request_subject'], 
			'request_desc'=>$postData['request_description'],
			//'technician_assign_status'=> 
			'request_created_dt'=>date('Y-m-d H:i:s')
 		);
		
		$this->db->insert('user_request_hd',$data);
		if($this->db->affected_rows()>0){
			return true;
		}
		return false;
		
	}
	//ajx guideline
	public function getAjxGuideDataByDeptId($dept_id){
		//sql statement 
		$qry = $this->db->select("*")->from("dept_guideline_hd")->where("dept_id",$dept_id)->where("guideline_is_active",1)->get();
		if($qry->num_rows() > 0){
			$guidelineData= $qry->row();
			$resultData = $guidelineData->guideline_txt;
		}else{
			$resultData = 'No guideline for the department';
		}
		return $resultData;
	}
	
	
	//RV
	//cron job code start 
	public function get_unassign_request(){
		$qry = $this->db->query("SELECT * FROM user_request_hd WHERE technician_assign_status = 0 AND request_is_active=1 AND is_request_completed =0");
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	//cron job code end
	//nNEW
	public function assign_request($technician_id=0, $request_id=0,$item_id=0){
		//assign status 1
		if($technician_id > 0){
			$assign_person = array(
				'group_id'=>get_groupId_itemId($item_id),
				'technician_id'=>$technician_id,
				'user_request_id'=>$request_id,
				'request_assign_person_dt'=>date("Y-m-d H:i:s"),
				'assign_status'=>1
			);
			if($this->db->insert('request_assign_person_hd',$assign_person)){
				$returnStatus = true;
				
				//update user request tbl 
				$request_ary = array(
					'technician_assign_status'=>1
				);
				$this->db->where('user_request_id',$request_id);
				$this->db->update('user_request_hd',$request_ary);
			}else{
				$returnStatus = false;
			}
		}else{
			$returnStatus = false;
		}
		return $returnStatus;
	}
	
	//task complete 
	public function tech_task_complete($request_id=0){
		//update status - complated 
		$updt_request = array(
			'is_request_completed'=>1,
			'task_completed_dt'=>date('Y-m-d H:i:s')
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->update('user_request_hd',$updt_request);
		
		//free the technician
		$updt_technician = array(
			'assign_status'=>0
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->where('assign_status',1);
		$this->db->where('is_escalated',0);
		if($this->db->update('request_assign_person_hd',$updt_technician)){
			$returnStatus = true;
		}else{
			$returnStatus = false;
		}
		return returnStatus;
	}
	
	//escalated task complete
	public function ho_task_complete($request_id=0){
		//update status - complated 
		$updt_request = array(
			'is_request_completed'=>1,
			'task_completed_dt'=>date('Y-m-d H:i:s')
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->update('user_request_hd',$updt_request);
		
		//free the technician
		$updt_technician = array(
			'assign_status'=>0,
			'escalated_dt'=>date('Y-m-d H:i:s')
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->where('assign_status',1);
		$this->db->where('is_escalated',1);
		if($this->db->update('request_assign_person_hd',$updt_technician)){
			$returnStatus = true;
		}else{
			$returnStatus = false;
		}
		return $returnStatus;
	}
	
	//escalate task to HO cron job
	public function escalate_task($request_id = 0){
		//find task who it is assigned
		$qry = $this->db->query("SELECT * FROM request_assign_person_hd WHERE user_request_id = '$request_id' AND is_escalated = 0 AND request_assign_person_is_active = 1 AND technician_id !=0");
		if($qry->num_rows() > 0){
			$assign_data = $qry->row();
			$assign_request_id = $assign_data->request_assign_person_id;
			$updt_assign_array = array(
				'is_escalated'=>1,
				'escalated_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where('request_assign_person_id',$assign_request_id);
			$this->db->update('request_assign_person_hd',$updt_array);
			
			//update request escalted 
			$updt_request_ary = array(
				'task_escalated'=>1
			);
			$this->db->where('user_request_id',$request_id);
			$this->db->update('user_request_hd',$updt_request_ary);
			
			//add ecalated task 
			$task_data = get_requestdata_by_id($request_id);
			$escalate_ary = array(
				'group_id'=>get_groupId_itemId($task_data->item_id),
				'technician_id'=>ho_technician_assign($task_data->item_id),
				'user_request_id'=>$request_id,
				'request_assign_person_dt'=>date('Y-m-d H:i:s'),
				'assign_status'=>1,
				'escalation_status'=>0
			);
			if($this->db->insert('escalated_task',$escalate_ary)){
				$returnStatus = true;
			}else{
				$returnStatus = false;
			}
			return $returnStatus;
		}
	}
	
	
	//site location
	public function get_site_location(){
		$qry = $this->db->select("*")->from("site_locations_hd")->where("site_location_is_active",1)->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	//get assign report
	public function get_assign_tech_list($u_email="",$u_code="",$qry_extend=""){
		//$qry = $this->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) ORDER BY ur.user_request_id DESC");
		
		$where = ($u_email != "")? " AND t.technician_email = '$u_email'":"";
		//$where = ($u_code != "")? " AND (t.tech_code = '$u_code' OR ur.user_code='$u_code') ":"";	
		$where = ($u_code != "")? " AND (t.tech_code = '$u_code') ":"";	
		$qry = $this->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) WHERE ur.request_is_active='1' AND ra.request_assign_person_is_active=1 $where $qry_extend ORDER BY ur.last_modified_dt DESC, ur.user_request_id DESC");
		//echo $this->db->last_query(); die;
		if($qry->num_rows() > 0){
			$resultAry = $qry->result();
		}else{
			$resultAry = array();
		}
		return $resultAry;
	}
	
	//request details page start 
	public function getRequestData($requestId = 0){
		$returnData = array();
		if($requestId > 0){
			$qry = $this->db->query("SELECT * FROM user_request_hd WHERE user_request_id = '$requestId' and request_is_active='1'");
			$returnData = $qry->row();
		}
		return $returnData;
	}
	
	//request details page end 
	
	//outlook email sync start 
	public function checkEmailExsitsByUniqueId($unique_id=0){
		$returnStatus = array();
		if($unique_id > 0){
			$qry = $this->db->query("SELECT * FROM outlook_email WHERE email_unique_id = '$unique_id' and email_is_active = 1 ");
			if($qry->num_rows() > 0){
				$returnStatus = $qry->row();
			}
		}
		return $returnStatus;
	}
	
	public function save_user_request_outlook($postData)
	{	
		$this->db->insert('user_request_hd',$postData);
		if($this->db->affected_rows()>0){
			return true;
		}
		return false;
	}
	
	public function getEmailByKey($searchKey = 0){
		$returnStatus = array();
		$qry = $this->db->query("SELECT u_email FROM ldapusers WHERE u_email like '$searchKey%' and u_email!='NULL' AND u_status=1 GROUP BY u_name ");
		/*SELECT u_email FROM users WHERE u_email like '$searchKey%' and u_email!='NULL'*/
		if($qry->num_rows() > 0){
			$returnStatus = $qry->result();
		}
		return $returnStatus;
	}
	
	//outlook email sync end
	
	/****** request details code starts from here *******/
	public function get_user_request_details($id){

	  $return = array();
      $query= $this->db->select('u.*,r.*')->from('user_request_hd u')->join('request_assign_person_hd r','u.user_request_id= r.user_request_id')->where('u.user_request_id',$id)->where('request_assign_person_is_active',1)->get();
	  if($query->num_rows() > 0){
	  	$return = $query->row();
	  }
	  return $return;
	}
     
     // get mode of request
	public function get_request_mode()
	{
		$returnmode = array();
		$qry = $this->db->query("SELECT DISTINCT(request_type) FROM user_request_hd WHERE request_type !='NULL'");
		if($qry->num_rows() > 0){
				$returnmode = $qry->result();
			}
		return $returnmode;	
	}
	/****** request details code ends here *******/
	
	function get_categories_by_dept_id($id)
    {   
	     $categories = array();
	     $qry = $this->db->select("*")->from("category_hd")->where("dept_id",$id)->where("category_is_active",'1')->get(); 
	     if($qry->num_rows()>0){
		  $categories = $qry->result();
	    }
	    return $categories;
    }
	function get_subcategories_by_category_id($id)
    {
		$subcategories = array();
	     $qry = $this->db->select("*")->from("sub_category_hd")->where("category_id",$id)->where("sub_category_is_active",'1')->get(); 
	     if($qry->num_rows()>0){
		  $subcategories = $qry->result();
	    }
	    return $subcategories;
    }

    function get_items_by_subcategory_id($id)
    {	
	     $items = array();
	     $qry = $this->db->select("*")->from("items_hd")->where("sub_category_id",$id)->where("item_is_active",'1')->get(); 
	     if($qry->num_rows()>0){
		  $items = $qry->result();
	    }
	    return $items;
    }
	
	function update_request_details()
	{
		$postData = $this->input->post();
		$isComplete = ($postData["status"]=="closed")? 1:0;
		$responseDate =($postData["response_dueby"]!='') ?date('Y-m-d h:i:s',strtotime($postData["response_dueby"])):'';
		/****updating the request assign person information ******/
	    $data_assign = array(
		   "tech_workstation_no"   => $postData["work_Station"],
		   "task_status"           => $postData["status"],
		   "group_id"              => $postData["group"],
           "technician_id"		   => $postData["technician"]
		);
		
		
		$this->db->update("request_assign_person_hd",$data_assign,array("user_request_id" => $postData["request_id"]));
		/****updating the user request information  ****/
		$data = array(
		   
		   "request_type"            =>     $postData["mode"],
		   "request_priority"        =>     $postData["priority"],
           "requester_location_id" 	 =>     $postData["site"],	   
		   "dept_id"                 =>     $postData["dept"],
		   "category_id"             =>     $postData["category"],
		   "subcategory_id"          =>     $postData["subactegory"], 
           "item_id"		         =>     $postData["item"],
           "is_request_completed"	 =>	    $isComplete,
		   "last_modified_dt"        =>     date('Y-m-d h:i:s'),
		   "due_date"                =>     date('Y-m-d h:i:s',strtotime($postData["dueby_date"])),
		   "response_date"           =>     $responseDate
		);
		$this->db->update("user_request_hd",$data, array("user_request_id" => $postData["request_id"] ));
		
		 
		
		if($this->db->affected_rows()>0){
			return true;
		}
		    return false;
	}
	/** save change status and comments ***/
	function save_status_change_details()
    {
		$postData = $this->input->post();
		$isComplete = ($postData["status"]=="closed")? 1:0;
		$data = array(
			
		   "user_request_id"   => $postData['request_id'],
		   "group_id"          => $postData['group'],
		   "technician_id"     => $postData['technician'],
		   "comment_desc"      => $postData['comment'],
		   "comment_created_dt"=> date('Y-m-d h:i:s')
		);
		if($postData['comment']!=''){
			$this->db->insert("status_change_comment_hd",$data);
		}
		
		$data_assign = array("task_status"=> $postData["status"]);
		
		$this->db->update("request_assign_person_hd",$data_assign,array("user_request_id" => $postData["request_id"]));
		$data_request = array(
		"is_request_completed"	 =>	    $isComplete,
		"last_modified_dt"        =>     date('Y-m-d h:i:s'),
		);
		$this->db->update("user_request_hd",$data_request, array("user_request_id" => $postData["request_id"] ));
		if($this->db->affected_rows()>0){
			return true;
		}
		    return false;
	}
	
	//reassign technician for specific task start 
	//Params - request_id, new assign _ technician array
	public function reassign_request($request_id=0,$new_assign_ary=array()){
		$request_assign_data = get_assign_request_data($request_id);
	//deactive currect request 
		$update_request = array(
			'request_assign_person_is_active'=>0
		);
		$this->db->where('request_assign_person_id',$request_assign_data->request_assign_person_id);
		$this->db->update('request_assign_person_hd',$update_request);
		
	//make previous request history 
		$assign_request_history = array(
			'request_assign_person_id'=>$request_assign_data->group_id,
			'group_id'=>$request_assign_data->group_id,
			'technician_id'=>$request_assign_data->technician_id,
			'user_request_id'=>$request_assign_data->user_request_id,
			'request_assign_person_dt'=>$request_assign_data->request_assign_person_dt,
			'assign_status'=>$request_assign_data->assign_status,
			'tech_workstation_no'=>$request_assign_data->tech_workstation_no,
			'is_escalated'=>$request_assign_data->is_escalated,
			'last_modified_dt'=>$request_assign_data->last_modified_dt,
			'escalated_dt'=>$request_assign_data->escalated_dt,
			'task_status'=>$request_assign_data->task_status
		);
		$this->db->insert('request_reassign_hd',$assign_request_history);
		
	//assign new request to new group and new technician 
		$reassign_request_ary = array(
			'group_id'=>$new_assign_ary['group_id'],
			'technician_id'=>$new_assign_ary['technician_id'],
			'user_request_id'=>$request_id,
			'request_assign_person_dt'=>date('Y-m-d H:i:s'),
			'assign_status'=>1,
			'task_status'=>'open'
		);
		$this->db->insert('request_assign_person_hd',$reassign_request_ary);
	//send email to new technician
	//add history when technician is assigned 
        $post_add_history = array(
	        'request_id' => $request_id,
            'group'		 =>	(!empty($new_assign_ary))?$new_assign_ary['group_id']:0 ,
			'prev_group' => (isset($request_assign_data))?$request_assign_data->group_id:0,
			'technician' => (!empty($new_assign_ary))?$new_assign_ary['technician_id']:0 ,
			'prev_technician' =>(isset($request_assign_data))?$request_assign_data->technician_id:0
	    );	 
	    add_history('ReqAssignTech',$post_add_history);
		return true;
	}
	
	//reassign technician for specific task end
	
	// get the system emails
	function get_system_emails($id,$show_items)
	{
	
		if($show_items != 'all'){
			$this->db->limit($show_items);
		}
		$query= $this->db->select("sytem_emails_id,to_emails,cc_emails,subject,description,created_date")->from("system_emails")->where("user_request_id",$id)->where("subject !=" ,"NULL")->where("system_email_is_active",'1')->order_by("created_date","DESC")->get();
		if($query->num_rows()>0)
		{
			$result = $query->result();
		}
		else{
			$result = array();
		}
		return $result;

    }
	// get the ticket history
	function get_ticket_history($user_request_id){
		$result = array();
		$query = $this->db->select("*")->from("request_history_hd")->where("request_id",$user_request_id)->get();
		if($query->num_rows()>0){
			$result = $query->result();
		}
		return $result;
	}
	
//resolution code start 
	/** save change status and comments ***/
	function save_status_change_details1($postData = array()){
		$returnStatus = false;
		//$postData = $this->input->post();
		$request_id = $postData['request_id'];
		$status = $postData["status"];
		$request = get_assign_request_data($request_id);
		$usrRequestData = get_requestdata_by_id($request_id);
		
		$isComplete = ($status =="resolve")? 1:0;
		$data = array(
		   "user_request_id" => $request_id,
		   "group_id" => $usrRequestData->group_id,
		   "technician_id" => $usrRequestData->technician_id,
		   "comment_desc" => $postData['comment'],
		   "comment_created_dt" => date('Y-m-d h:i:s')
		);
		
		if($postData['comment']!=''){
			$this->db->insert("status_change_comment_hd",$data);
			$returnStatus = true;
		}
		
		if(isset($status)){
			$data_assign = array(
				"task_status" => $status
			);
			$this->db->update("request_assign_person_hd",$data_assign,array("user_request_id" => $request_id));
			
			
			
			$data_request = array(
				"is_request_completed" => $isComplete,
				"last_modified_dt" => date('Y-m-d h:i:s')
			);
			
			$this->db->update("user_request_hd",$data_request, array("user_request_id" => $request_id));
			if($this->db->affected_rows()>0){
				// status change history
				$postDataArray = array(
				   'prev_status' => $request->task_status,
				   'status'      =>$status,
				   'request_id'  =>$request_id
				);
				add_history('ReqStatus',$postDataArray);
				$returnStatus = true;
			}
		}
		return $returnStatus;
	}
	
	function get_solution_details($solutionId = 0){
		$qry = $this->db->query("SELECT * FROM request_solutions_hd WHERE request_solution_hd='$solutionId' AND request_solution_is_active='1'");
		$returnData = $qry->row();
		return $returnData;
	}
	//  to pull the ticket history   
	function get_request_history($id){
        $query = $this->db->select("*")->from("ticket_history_hd")->where("request_id",$id)->where("history_is_active",'1')->order_by("request_history_id","DESC")->get();
		
		if($query->num_rows()>0){
		    $result = $query->result();
		}
		else
		{
		    $result = array();
		}
        
		return $result;
    }
//resolution code end
    function save_close_request_hd($data)
	{	$postData = $this->input->post();
	    $status = $postData["status"];
	    $user_request_id = $postData["user_request_id"];
		$req_ack = $postData["req_ack"];
		$fcr_check = $postData["fcr_chk"];
		$req_closer_comment = $postData["req_closer_comment"];
		$request_closer_code = $postData["req_closer"]; 
	    $request_Ack_id = ($req_ack== 'yes')?$data["user_indo"][0]->u_id:0;
		$fcr_gen_comment = $postData["fcr_gen_comment"];
		//'1' in case of yes should be replaced by acknowledger's id else '0'
		$assign_tech_group = get_assign_request_data($user_request_id);
		$group_id = $assign_tech_group->group_id;
		$technician_id =$assign_tech_group->technician_id;
		 $data_comment = array(
		    'user_request_id' => $user_request_id,
			'group_id'        => $group_id,
			'technician_id'   => $technician_id,
			'comment_desc'    => $fcr_gen_comment,
			'comment_created_dt'=> date('Y-m-d h:i:s')
		 );
		 $this->db->insert("status_change_comment_hd",$data_comment);
		 // update the status to close
		 $data_task_status = array(
		 'task_status'=> $status
		 );
		 $this->db->update("request_assign_person_hd",$data_task_status,array("user_request_id" => $user_request_id));
		 if($this->db->affected_rows()>0){
			 $insert_id = $this->db->insert_id();
			$data_closure = array(
				"comment_id" => $insert_id, // comment id 0 should be replaced with comment id from comment table
				"request_closure_code" => $request_closer_code,
				"request_closure_comment" => $req_closer_comment,
				"user_request_id"  => $user_request_id,
				"is_request_acknw" => $req_ack, 
				"acknw_user_id"   => $request_Ack_id,
				"is_first_call_resoln_chk" => $fcr_check
		    );
		    $this->db->insert("user_request_closure",$data_closure);
			if($this->db->affected_rows()>0){
				//add the history of the ticket
				$history_data = array(
				"request_id" => $user_request_id
				);
			    add_history("ReqClosed",$history_data);
				
			    return true;
		    }
		    else{
		        return false;	
		    }
		 }
		 else{
		        return false;	
		 }
		
		   
	}
	
}