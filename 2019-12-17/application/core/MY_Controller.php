<?php
ob_start();

class MY_Controller extends CI_Controller
{

    function __construct()
    {

        parent::__construct();
        $this->adminemail = 'suhasini.susu@gmail.com';
        $logindata = $this->session->userdata('user_data');
        $this->logindata = $logindata;
        if (isset($logindata->user_type) && ($logindata->user_type == 0 || $logindata->user_type == 2)) {
            $walletData = $this->Base_model->run_query("select * from wallet WHERE w_user_id=" . $logindata->id . " ORDER BY w_id DESC LIMIT 0, 1");

            if (isset($walletData) && count($walletData) > 0) {

            } else {
                $walletData = array(0 => (object)array('w_balance' => 0));
            }
//            print_r($walletData);
            $this->wallet = $walletData[0];
        }
    }

    function availableTimeTable($available_times, $catid, $type = 'view')
    {

        $available_records = $this->filter_by_key($available_times, 'avl_cat_id', $catid);


        switch ($type) {
            case "edit":
                ?>
                <div class="row baby_timeline_cal">
                    <?php
                    $sCurrentDate = date("Y-m-d", strtotime("+1 day"));
                    $weeks4Date = date("Y-m-d", strtotime("+4 week"));
                    $date_period = $this->dateRange($sCurrentDate, $weeks4Date);


                    //                                echo '<pre>';
                    //                                print_r($available_times    );
                    //                                print_r($avl_date);
                    //                                echo '</pre>';
                    ?>

                    <div class="col-md-2 col-xs-4 col-sm-2 nopad">
                        <div class="list-group">
                            <li class="list-group-item active">
                                <br/>
                                TIME
                            </li>

                            <?php
                            if (count($this->timeimgs) > 0) {
                                foreach ($this->timeimgs AS $time) {
                                    ?>
                                    <li class="list-group-item nopad   ">
                                        <span class="btn btn-default noborder">
                                            <?= $time ?>
                                        </span>
                                    </li>

                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-10 ">
                        <div class="col-md-12 nopad cal_datesbox">
                            <?php
                            if (count($date_period) > 0) {

                                foreach ($date_period AS $date) {
                                    ?>

                                    <div class="list-group checkboxlist">

                                        <?php
                                        if (count($this->timeimgs) > 0) {
                                            $id = 0;
                                            foreach ($this->timeimgs AS $time) {

                                                $avl_date = $this->getMultiDimenArrayByKeyElements($available_records, 'avl_date');
                                                $avl_dates = $this->filter_by_key($available_records, 'avl_date', $date);
                                                $avl_times = $this->getMultiDimenArrayByKeyElements($avl_dates, 'avl_time');


                                                if ($id == 0) {
                                                    ?>
                                                    <li class="list-group-item active ">
                                                        <?= date('l', strtotime($date)) ?>
                                                        <?= $date ?>
                                                    </li>
                                                    <li class="list-group-item  nopad">

                                                        <?php
                                                        //                                                        if($available_times){
                                                        //
                                                        //                                                        }
                                                        ?>

                                                        <label
                                                            class="btn btn-sm btn-block <?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'btn-success' : '' ?> "
                                                            title="<?= $date . '  ' . $time ?>">

                                                            <input
                                                                type="checkbox" <?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'checked=""' : '' ?>
                                                                class=" " name="available[<?= $date ?>][<?= $time ?>]"
                                                                class="  " value="1"/>
                                                            <span>available</span>
                                                        </label>
                                                    </li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li class="list-group-item nopad ">

                                                        <label
                                                            class="btn btn-sm btn-block <?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'btn-success' : '' ?> "
                                                            title="<?= $date . ' ' . $time ?>">

                                                            <input
                                                                type="checkbox" <?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'checked=""' : '' ?>
                                                                class=" " name="available[<?= $date ?>][<?= $time ?>]"
                                                                class="  " value="1"/>
                                                            <span>available</span>
                                                        </label>
                                                    </li>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                $id++;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>
                </div>
                <?php
                break;
            case "view":
                ?>
                <div class="row baby_timeline_cal">
                    <?php
                    $sCurrentDate = date("Y-m-d", strtotime("+1 day"));
                    $weeks4Date = date("Y-m-d", strtotime("+4 week"));
                    $date_period = $this->dateRange($sCurrentDate, $weeks4Date);


                    //                                echo '<pre>';
                    //                                print_r($available_times    );
                    //                                print_r($avl_date);
                    //                                echo '</pre>';
                    ?>

                    <div class="col-md-2 col-xs-4 col-sm-2 nopad">
                        <ul class="list-group">
                            <li class="list-group-item active">
                                <br/>
                                TIME
                            </li>

                            <?php
                            if (count($this->timeimgs) > 0) {
                                foreach ($this->timeimgs AS $time) {
                                    ?>
                                    <li class="list-group-item nopad   ">
                                        <span class="btn btn-default noborder">
                                            <?= $time ?>
                                        </span>
                                    </li>

                                    <?php
                                }
                            }
                            ?>

                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="col-md-12 col-xs-8 col-sm-10 nopad cal_datesbox">
                            <?php
                            if (count($date_period) > 0) {

                                foreach ($date_period AS $date) {
                                    ?>

                                    <ul class="list-group checkboxlist">
                                        <!--<div class="cal_datebox" >-->

                                        <?php ?>
                                        <?php
                                        if (count($this->timeimgs) > 0) {
                                            $id = 0;
                                            foreach ($this->timeimgs AS $time) {

                                                $avl_date = $this->getMultiDimenArrayByKeyElements($available_records, 'avl_date');
                                                $avl_dates = $this->filter_by_key($available_records, 'avl_date', $date);
                                                $avl_times = $this->getMultiDimenArrayByKeyElements($avl_dates, 'avl_time');


                                                if ($id == 0) {
                                                    ?>
                                                    <li class="list-group-item active ">
                                                        <?= date('l', strtotime($date)) ?>
                                                        <?= $date ?>
                                                    </li>
                                                    <li class="list-group-item  nopad">

                                                        <?php
                                                        //                                                        if($available_times){
                                                        //
                                                        //                                                        }
                                                        ?>

                                                        <label
                                                            class="btn btn-sm btn-block <?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'btn-success' : '' ?> "
                                                            title="<?= $date . '  ' . $time ?>">
                                                            <span><?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'available' : 'unavailable' ?></span>
                                                        </label>
                                                    </li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li class="list-group-item nopad ">

                                                        <label
                                                            class="btn btn-sm btn-block <?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'btn-success' : '' ?> "
                                                            title="<?= $date . '  ' . $time ?>">

                                                            <span><?= (in_array($date, $avl_date) && in_array($time, $avl_times)) ? 'available' : 'unavailable' ?></span>
                                                        </label>
                                                    </li>
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                $id++;
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>
                </div>
                <?php
                break;
        }
    }

    function filter_by_key($array, $key, $value)
    {
        $filtered = array();

        if (is_array($array)) {
            foreach ($array as $k => $v) {
                if ($v->$key == $value)
                    $filtered[$k] = $v;
            }
        }
        return $filtered;
    }

    function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d')
    {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {

            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    public $timeimgs = array('8AM', '9AM', '10AM', '11AM', '12PM', '1PM', '2PM', '3PM', '4PM', '5PM', '6PM', '7PM', '8PM', '9PM', '10PM');

    function user_session_validate()
    {
        $logindata = $this->session->userdata('user_data');

        if (isset($logindata->user_type) && ($logindata->user_type == 0 || $logindata->user_type == 2)) {
//            echo 'sss';
        } else {

            $this->session->unset_userdata('redirect_page');
            $this->session->unset_userdata('user_data');
            $this->session->set_userdata('redirect_page', current_url());
            $this->prepare_flashmessage("Session Expired...", 1);
            redirect('page/login');
        }
    }

    function group_assoc($array, $key)
    {
        $return = array();
        foreach ($array as $v) {
            $return[$v->$key][] = $v;
        }
        $dataa = (object)$return;
        return $dataa;
    }

    function getKeyValyes($array, $key)
    {
        $results = array_column($array, $key);
        return $results;
    }

    function userRate($user_id)
    {

        $user_rating = $this->Base_model->run_query("SELECT AVG(rate) AS rating   FROM `user_rating` WHERE  `user_id` = " . $user_id);


        if (isset($user_rating[0]) && $user_rating[0]->rating > 0) {
            $total_rate = $user_rating[0]->rating + 1;
        } else {
            $total_rate = 0;
        }


        $finalRate = ($total_rate > 0) ? $total_rate : 0;

        $finalRate_temp = $total_rate - 1;

        $finalRate_temp = ($finalRate_temp > 0) ? $finalRate_temp : 0;


        $return_array = array('o' => $finalRate, 't' => $finalRate_temp);


        return $return_array;
    }

    function getMultiDimenArrayByKeyElements($supArr, $key)
    {
        $temp = array();
        if (is_array($supArr) || is_object($supArr)) {
            foreach ($supArr as $obj) {

                if (is_object($obj)) {
                    if ($obj->$key != '') {
                        array_push($temp, $obj->$key);
                    }
                } else {
                    if ($obj[$key] != '') {
                        array_push($temp, $obj[$key]);
                    }
                }
            }
        }
        return array_unique($temp);
    }

    function prepare_flashmessage($msg, $type)
    {
        $returnmsg = '';
        switch ($type) {
            case 0:
                $returnmsg = " <div class='col-md-12'>
    <div class='alert alert-success'>
        <a href='#' class='close' data-dismiss='alert'>&times;</a>
        <strong>Success!</strong> " . $msg . "
    </div>
</div>";
                break;
            case 1:
                $returnmsg = " <div class='col-md-12'>
    <div class='alert alert-danger'>
        <a href='#' class='close' data-dismiss='alert'>&times;</a>
        <strong>Error!</strong> " . $msg . "
    </div>
</div>";
                break;
            case 2:
                $returnmsg = " <div class='col-md-12'>
    <div class='alert alert-info'>
        <a href='#' class='close' data-dismiss='alert'>&times;</a>
        <strong>Info!</strong> " . $msg . "
    </div>
</div>";
                break;
            case 3:
                $returnmsg = " <div class='col-md-12'>
    <div class='alert alert-warning'>
        <a href='#' class='close' data-dismiss='alert'>&times;</a>
        <strong>Warning!</strong> " . $msg . "
    </div>
</div>";
                break;
        }

        $this->session->set_flashdata("message", $returnmsg);
    }

    function set_pagination($url, $offset, $numrows, $perpage)
    {
        $this->load->library('pagination');
        $config['base_url'] = base_url() . $url;
        $config['total_rows'] = $numrows;
        $config['per_page'] = $perpage;
        $config["uri_segment"] = $offset;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
    }

    function create_custom_image($original_file, $destination_file = NULL, $square_width = 100, $square_height = 75)
    {
        if (isset($destination_file) and $destination_file != NULL) {
            if (!is_writable($destination_file)) {
//                @chmod($destination_file, 0755);
//echo '<p style="color:#FF0000">Oops, the destination path is not writable. Make that file or its parent folder wirtable.</p>';
            }
        }
// get width and height of original image
        $imagedata = getimagesize($original_file);
        $original_width = $imagedata[0];
        $original_height = $imagedata[1];
        $new_width = round($square_width);
        $new_height = round($square_height);
// load the image
        if (substr_count(strtolower($original_file), ".jpg") or substr_count(strtolower($original_file), ".jpeg")) {
            $original_image = imagecreatefromjpeg($original_file);
        }
        if (substr_count(strtolower($original_file), ".gif")) {
            $original_image = imagecreatefromgif($original_file);
        }
        if (substr_count(strtolower($original_file), ".png")) {
            $original_image = imagecreatefrompng($original_file);
        }
        $smaller_image = imagecreatetruecolor($new_width, $new_height);
        $square_image = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
        if ($new_width > $new_height) {
            $difference = $new_width - $new_height;
            $half_difference = round($difference / 2);
            imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $new_width, $new_height, $new_width, $new_height);
        }
        if ($new_height > $new_width) {
            $difference = $new_height - $new_width;
            $half_difference = round($difference / 2);
            imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $new_width, $new_height, $new_width, $new_height);
        }
        if ($new_height == $new_width) {
            imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $new_width, $new_height, $new_width, $new_height);
        }
// if no destination file was given then display a png		
        if (!$destination_file) {
            imagepng($square_image, NULL, 9);
        }
// save the smaller image FILE if destination file given
        if (substr_count(strtolower($destination_file), ".jpg")) {
            imagejpeg($square_image, $destination_file, 100);
        }
        if (substr_count(strtolower($destination_file), ".gif")) {
            imagegif($square_image, $destination_file);
        }
        if (substr_count(strtolower($destination_file), ".png")) {
            imagepng($square_image, $destination_file, 9);
        }
        imagedestroy($original_image);
        imagedestroy($smaller_image);
        imagedestroy($square_image);
    }

    function create_square_image($original_file, $destination_file = NULL, $square_size = 96)
    {
//        echo $original_file.'<br />'.$destination_file.'<br />'.$square_size;
//        exit();
        if (isset($destination_file) and $destination_file != NULL) {
            if (!is_writable($destination_file)) {
//                @chmod($destination_file, 0755);
//echo '<p style="color:#FF0000">Oops, the destination path is not writable. Make that file or its parent folder wirtable.</p>';
            }
        }
// get width and height of original image
        $imagedata = getimagesize($original_file);
        $original_width = $imagedata[0];
        $original_height = $imagedata[1];
        if ($original_width > $original_height) {
            $new_height = $square_size;
            $new_width = $new_height * ($original_width / $original_height);
        }
        if ($original_height > $original_width) {
            $new_width = $square_size;
            $new_height = $new_width * ($original_height / $original_width);
        }
        if ($original_height == $original_width) {
            $new_width = $square_size;
            $new_height = $square_size;
        }
        $new_width = round($new_width);
        $new_height = round($new_height);
// load the image
        if (substr_count(strtolower($original_file), ".jpg") or substr_count(strtolower($original_file), ".jpeg")) {
            $original_image = imagecreatefromjpeg($original_file);
        }
        if (substr_count(strtolower($original_file), ".gif")) {
            $original_image = imagecreatefromgif($original_file);
        }
        if (substr_count(strtolower($original_file), ".png")) {
            $original_image = imagecreatefrompng($original_file);
        }
        $smaller_image = imagecreatetruecolor($new_width, $new_height);
        $square_image = imagecreatetruecolor($square_size, $square_size);
        imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
        if ($new_width > $new_height) {
            $difference = $new_width - $new_height;
            $half_difference = round($difference / 2);
            imagecopyresampled($square_image, $smaller_image, 0 - $half_difference + 1, 0, 0, 0, $square_size + $difference, $square_size, $new_width, $new_height);
        }
        if ($new_height > $new_width) {
            $difference = $new_height - $new_width;
            $half_difference = round($difference / 2);
            imagecopyresampled($square_image, $smaller_image, 0, 0 - $half_difference + 1, 0, 0, $square_size, $square_size + $difference, $new_width, $new_height);
        }
        if ($new_height == $new_width) {
            imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $square_size, $square_size, $new_width, $new_height);
        }
// if no destination file was given then display a png		
        if (!$destination_file) {
            imagepng($square_image, NULL, 9);
        }
// save the smaller image FILE if destination file given
        if (substr_count(strtolower($destination_file), ".jpg")) {
            imagejpeg($square_image, $destination_file, 100);
        }
        if (substr_count(strtolower($destination_file), ".gif")) {
            imagegif($square_image, $destination_file);
        }
        if (substr_count(strtolower($destination_file), ".png")) {
            imagepng($square_image, $destination_file, 9);
        }
        imagedestroy($original_image);
        imagedestroy($smaller_image);
        imagedestroy($square_image);
    }

    function createSlug($text)
    {
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

// transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

// remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

// trim
        $text = trim($text, '-');

// remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

// lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    function resize_image($file, $w, $h, $crop = FALSE)
    {
        $uploadedfile = $file;
        $src = imagecreatefromjpeg($uploadedfile);
        list($width, $height) = getimagesize($uploadedfile);
        $newwidth = $w;
        $newheight = ($height / $width) * $h;
        $tmp = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        $filename = $file;
        imagejpeg($tmp, $filename, 80);
        imagedestroy($src);
        imagedestroy($tmp); // NOTE: PHP will clean up the temp file it created when the request
    }

    function sendEmail($path, $data, $to)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'gadipallychandra',
            'smtp_pass' => '1234abcd',
            'smtp_port' => 587,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $body = $this->load->view($path, $data, TRUE);
        $this->email->set_mailtype("html");
        $this->email->from('info@indianbrain.in');
        $this->email->to($to);
        $this->email->bcc('mirrorwebs@gmail.com');
        $this->email->subject($data['subject']);
        $this->email->message($body);
        $this->email->send();
    }

}

?>