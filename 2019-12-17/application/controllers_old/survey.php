<?php
class Survey extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		//if (empty($this->session->userdata('user_ldamp'))) {
        //    redirect('login', 'refresh');
       // }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->model('Survey_model');
		 $mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        $mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        $this->hrdata_user_info = $mssql_user_info_query->result_array();
		
	}
	public function index(){
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		
		if($this->user_ldamp['employeeid'] !=''){
			$this->empCode = $this->user_ldamp['employeeid'];
		}
		
		$data['user_ldamp'] = $this->user_ldamp;
		$data['title'] = 'Skeiron Group Learning & Development';
		$data['empData'] = $this->Survey_model->get_emp_data($this->empCode);
		$data['behave_soft_skils_list_data'] = $this->Survey_model->get_behave_soft_skils_list();
		$data['get_qhse_list_data'] = $this->Survey_model->get_qhse_list();
		$data['get_tec_func_data'] = $this->Survey_model->get_tec_func_list();
		$this->load->view('survey/index',$data);
	}
	
	//save survey
	public function submit_survey(){
		$postData = $this->input->post();
		//echo '<pre>'; print_r($postData);exit;
		if($postData['submit'] !=''){
			switch($postData['submit']){
				case 'Save':
					$save_ary = array(
						'employee_name'=>$postData['emp_name'],
						'employee_code'=>$postData['emp_code'],
						'location'=>$postData['emp_location'],
						'name_of_business'=>$postData['business_name'],
						'name_of_reporting_manager'=>$postData['emp_reporting_manager'],
						'name_of_hod'=>$postData['emp_hod'],
						'tech_func_training_need_one'=>$postData['tech_func_1'],
						'tech_func_training_need_two'=>$postData['tech_func_2'],
						'beh_soft_skills_training_need_one'=>$postData['soft_skill_1'],
						'beh_soft_skills_training_need_two'=>$postData['soft_skill_2'],
						'qhse_training_need_one'=>$postData['qhse_1'],
						'qhse_training_need_two'=>$postData['qhse_2'],
						'tr_need_one'=>$postData['tr_need_1'],
						'tr_need_two'=>$postData['tr_need_2'],
						'tr_respondent_one'=>$postData['tr_respondent_1'],
						'tr_respondent_two'=>$postData['tr_respondent_2'],
						'is_save'=>1,
						'is_submit'=>0
					);
				break;
				case 'Submit':
					$save_ary = array(
						'employee_name'=>$postData['emp_name'],
						'employee_code'=>$postData['emp_code'],
						'location'=>$postData['emp_location'],
						'name_of_business'=>$postData['business_name'],
						'name_of_reporting_manager'=>$postData['emp_reporting_manager'],
						'name_of_hod'=>$postData['emp_hod'],
						'tech_func_training_need_one'=>$postData['tech_func_1'],
						'tech_func_training_need_two'=>$postData['tech_func_2'],
						'beh_soft_skills_training_need_one'=>$postData['soft_skill_1'],
						'beh_soft_skills_training_need_two'=>$postData['soft_skill_2'],
						'qhse_training_need_one'=>$postData['qhse_1'],
						'qhse_training_need_two'=>$postData['qhse_2'],
						'tr_need_one'=>$postData['tr_need_1'],
						'tr_need_two'=>$postData['tr_need_2'],
						'tr_respondent_one'=>$postData['tr_respondent_1'],
						'tr_respondent_two'=>$postData['tr_respondent_2'],
						'is_save'=>0,
						'is_submit'=>1
					);
				break;
			}
			
			if($this->Survey_model->checkEmp(trim($postData['emp_code']))){
				$this->db->insert('survey_report',$save_ary);	
			}else{
				$this->db->where('employee_code',trim($postData['emp_code']));
				$this->db->update('survey_report',$save_ary);
			}
			
		}else{
			echo 'Server error';
		}
		redirect('Survey');
	}
	
	//Report 
	public function report(){
		$data['user_ldamp'] = $this->user_ldamp;
		$data['surveyReportData'] = $this->Survey_model->getReportData();
		$this->load->view('survey/report',$data);
	}
	
	//custom report
	public function customReport(){
		$postData = $this->input->post();
		$data['surveyReportData'] = $this->Survey_model->customSearch($postData['search_field'],$postData['filter_field']);
		$this->load->view('survey/custom_report',$data);
	}
	
	//
	public function getReportDownload(){
		ob_start();
		$postData = $this->input->post();
		if($postData['download_btn'] == 'Download xls'){
			$this->downloadReport();
		}
		if($postData['download_btn'] == 'Download PDF'){
			$this->downloadPDF();
		}
	}
	
	//download custom report csv
	public function downloadReport(){
		$postData = $this->input->post();
		$customReport = $this->Survey_model->customSearch($postData['search_field'],$postData['filter_field']);
	    $contents ='"Sr No","Employee Name","Employee Code","Location","Name of Business","Name of Reporting Manager","Name of HOD","Technical / Functional Training Need 1","Technical / Functional Training Need 2","Behavioral / Soft Skills Training Need 1","Behavioral / Soft Skills Training Need 2","QHSE Training Need 1","QHSE Training Need 2","Tr Need 1","Tr Need 2"';
	    $contents .="\n";
		$i = 1;
	    foreach($customReport as $surveyRecord){
	    	$contents.='"'.$i.'","'.$surveyRecord->employee_name.'","'.$surveyRecord->employee_code.'","'.$surveyRecord->location.'","'.$surveyRecord->name_of_business.'","'.$surveyRecord->name_of_reporting_manager.'","'.$surveyRecord->name_of_hod.'","'.getTechnicalName($surveyRecord->tech_func_training_need_one).'","'.getTechnicalName($surveyRecord->tech_func_training_need_two).'","'.getBehsoftName($surveyRecord->beh_soft_skills_training_need_one).'","'.getBehsoftName($surveyRecord->beh_soft_skills_training_need_two).'","'.getQhseName($surveyRecord->qhse_training_need_one).'","'.getQhseName($surveyRecord->qhse_training_need_two).'","'.$surveyRecord->tr_need_one.'","'.$surveyRecord->tr_need_two.'"';
	    	$contents.="\n";
			$i++;
	    }
	    $contents = strip_tags($contents);
	    Header("Content-Disposition: attachment;filename=survey_report.csv ");
	    print $contents;
	    exit;
	}
	
	//PDF Code Start
	public function downloadPDF(){
		ob_start();
		$this->load->library('PDF_CON_RV/tcpdf');
		ini_set('max_execution_time', 500);
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// add a page
		//$pdf->AddPage();
		$pdf->AddPage('L', 'A2');
		$postData = $this->input->post();
		$customReport = $this->Survey_model->customSearch($postData['search_field'],$postData['filter_field']);
			
		//with rates
		$contents.='<div class="page-header"><center><img src="http://192.168.20.129:4545/assets/images/logo/Logo.png" style="width:200px"><h2>Learning &amp; Development</h4><h4>Individual Training Needs Identification Survey FY 2017 - 2018</h2></center></div>';
		$contents.='<table cellpadding="1" cellspacing="1" border="1">'; 
		$contents.='<tr style="font-weight:bold"><td>Sr No</td><td>Employee Name</td><td>Employee Code</td><td>Location</td><td>Name of Business</td><td>Name of Reporting Manager</td><td>Name of HOD</td><td>Technical / Functional Training Need 1</td><td>Technical / Functional Training Need 2</td><td>Behavioral / Soft Skills Training Need 1</td><td>Behavioral / Soft Skills Training Need 2</td><td>QHSE Training Need 1</td><td>QHSE Training Need 2</td><td>Tr Need 1</td><td>Tr Need 2</td></tr>';
		
		$i = 1;
	    foreach($customReport as $surveyRecord){
	    	$contents.='<tr><td>'.$i.'</td><td>'.$surveyRecord->employee_name.'</td><td>'.$surveyRecord->employee_code.'</td><td>'.$surveyRecord->location.'</td><td>'.$surveyRecord->name_of_business.'</td><td>'.$surveyRecord->name_of_reporting_manager.'</td><td>'.$surveyRecord->name_of_hod.'</td><td>'.getTechnicalName($surveyRecord->tech_func_training_need_one).'</td><td>'.getTechnicalName($surveyRecord->tech_func_training_need_two).'</td><td>'.getBehsoftName($surveyRecord->beh_soft_skills_training_need_one).'</td><td>'.getBehsoftName($surveyRecord->beh_soft_skills_training_need_two).'</td><td>'.getQhseName($surveyRecord->qhse_training_need_one).'</td><td>'.getQhseName($surveyRecord->qhse_training_need_two).'</td><td>'.$surveyRecord->tr_need_one.'</td><td>'.$surveyRecord->tr_need_two.'</td></tr>';
	    	$contents.="\n";
			$i++;
	    }
		$contents .='</table>';
		$pdf->writeHTML($contents, true, false, true, false, '');
		
		//print_r($contents);
		//Close and output PDF document
		$pdf->lastPage();
		$pdf->Output('survey_report.pdf','I');
		exit;
	}
	//PDF Code End
	
	//google pie chart 
	public function chartReport(){
		$this->load->view('survey/chartReport_google');
	}
	
	//
	public function indexV2_old_delete(){
		if(empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		if($this->user_ldamp['employeeid'] !=''){
			$this->empCode = $this->user_ldamp['employeeid'];
		}
		$data['user_ldamp'] = $this->user_ldamp;
		$data['title'] = 'Skeiron Group Learning & Development';
		$data['empData'] = $this->Survey_model->get_emp_data($this->empCode);
		$data['behave_soft_skils_list_data'] = $this->Survey_model->get_behave_soft_skils_list();
		$data['get_qhse_list_data'] = $this->Survey_model->get_qhse_list();
		$data['get_tec_func_data'] = $this->Survey_model->get_tec_func_list();
		$this->load->view('survey/indexv2',$data);
	}
	
	//online shirt and t-shert servey requirement 21-3-2017 start 
	public function apparel($loc=0){
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		
		if($this->user_ldamp['employeeid'] !=''){
			$this->empCode = $this->user_ldamp['employeeid'];
		}
		
		if($loc > 0){
			$data['is_form_submitted'] = true;
		}else{
			$data['is_form_submitted'] = false;
		}
		
		$data['user_ldamp'] = $this->user_ldamp;
		$data['title'] = 'Data collection Tool - Apparel Sizes';
		$data['empApparel'] = $this->Survey_model->get_apparel_data($this->empCode);
		$data['empData'] = $this->Survey_model->get_emp_data($this->empCode);
		
		if(count($data['empApparel']) > 0){
			$data['filledFlag'] = 1;
			$this->load->view('survey/shirts_survey',$data);
		}else{
			$data['filledFlag'] = 0;
			$this->load->view('survey/shirts_survey',$data);
		}
	}
	public function saveRequest(){
		$postData = $this->input->post();
		
		if($postData['emp_code']!=''){
			if($postData['shirt_size_text']!=''){
				$shirt_size = $postData['shirt_size_text'];
				$shirt_size_flag = 1;
			}else{
				$shirt_size = $postData['shirt_size'];
				$shirt_size_flag = 0;
			}
			
			if($postData['shirt_collar_size_text']!=''){
				$shirt_collar_size = $postData['shirt_collar_size_text'];
				$shirt_collar_size_flag = 1;
			}else{
				$shirt_collar_size = $postData['shirt_collar_size'];
				$shirt_collar_size_flag = 0;
			}
			
			if($postData['t_shirt_size_text']!=''){
				$t_shirt_size = $postData['t_shirt_size_text'];
				$t_shirt_size_flag = 1;
			}else{
				$t_shirt_size = $postData['t_shirt_size'];
				$t_shirt_size_flag = 0;
			}
			
			$shirt_survey = array(
				'emp_code'=>$postData['emp_code'],
				'full_name'=>$postData['emp_name'],
				'location'=>$postData['emp_location'],
				'shirt_size'=>$shirt_size,
				'shirt_collar_size'=>$shirt_collar_size,
				't_shirt_size'=>$t_shirt_size,
				'shirt_size_other'=>$shirt_size_flag,
				'shirt_collar_size_other'=>$shirt_collar_size_flag,
				't_shirt_size_other'=>$t_shirt_size_flag,
				'created_date'=>date('Y-m-d H:i:s')
			);
			$this->db->insert('apparel_survey',$shirt_survey);
		}
		//redirect($this->agent->referrer());
		redirect('survey/apparel/2');
	}
	
	public function apparel_report(){
		$data['user_ldamp'] = $this->user_ldamp;
		$data['surveyApparelReportData'] = $this->Survey_model->getApparelReportData();
		$this->load->view('survey/apparelreport',$data);
	}
	
	public function apparel_downloadReport(){
		$postData = $this->input->post();
		$customReport =  $this->Survey_model->getApparelReportData();
	    $contents ='"Sr No","Employee Code","Employee Name","Location","Shirt Size","Shirt Collar size","T-shirt size"';
	    $contents .="\n";
		$i = 1;
	    foreach($customReport as $surveyRecord){
	    	$contents.='"'.$i.'","'.$surveyRecord->emp_code.'","'.$surveyRecord->full_name.'","'.$surveyRecord->location.'","'.$surveyRecord->shirt_size.'","'.$surveyRecord->shirt_collar_size.'","'.$surveyRecord->t_shirt_size.'"';
	    	$contents.="\n";
			$i++;
	    }
	    $contents = strip_tags($contents);
	    Header("Content-Disposition: attachment;filename=apparel_survey_report.csv ");
	    print $contents;
	    exit;
	}
	
	//online shirt and t-shert servey requirement 21-3-2017 end
	
	
	
	//TNI Survey V2 22-3-2017 start 
	
	public function reportV2(){
		$data['user_ldamp'] = $this->user_ldamp;
		$data['surveyReportData'] = $this->Survey_model->getReportDataV2();
		$this->load->view('survey/reportV2',$data);
	}
	public function indexV2_(){
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		
		if($this->user_ldamp['employeeid'] !=''){
			$this->empCode = $this->user_ldamp['employeeid'];
		}
		
		$data['user_ldamp'] = $this->user_ldamp;
		$data['title'] = 'Skeiron Group Learning & Development';
		$data['empData'] = $this->Survey_model->get_emp_data_V2($this->empCode);
		$data['ldp_mdp_cat'] = getCategoryData('mdp_ldp_list_v2');
		//echo '<br>'.$this->db->last_query();
		$data['fun_tech_cat'] = getCategoryData('tec_func_list_v2');
		//echo '<br>'.$this->db->last_query();
		$data['pd_beha_cat'] = getCategoryData('dp_bhav_list_v2');
		//echo '<br>'.$this->db->last_query();
		$data['qhse_list'] = hqse_development();
		$this->load->view('survey/indexv2',$data);
	}
	
	public function indexV2(){
		//if (empty($this->session->userdata('user_ldamp'))) {
        //    redirect('login', 'refresh');
        //}
		
		//if($this->user_ldamp['employeeid'] !=''){
		//	$this->empCode = $this->user_ldamp['employeeid'];
		//}
		$this->empCode = '';
		$data['user_ldamp'] = $this->user_ldamp;
		$data['title'] = 'Skeiron Group Learning & Development';
		$data['empData'] = $this->Survey_model->get_emp_data_V2($this->empCode);
		$data['ldp_mdp_cat'] = getCategoryData('mdp_ldp_list_v2');
		//echo '<br>'.$this->db->last_query();
		$data['fun_tech_cat'] = getCategoryData('tec_func_list_v2');
		//echo '<br>'.$this->db->last_query();
		$data['pd_beha_cat'] = getCategoryData('dp_bhav_list_v2');
		//echo '<br>'.$this->db->last_query();
		$data['qhse_list'] = hqse_development();
		$this->load->view('survey/directsurvey',$data);
	}
	
	public function getDrillDown(){
		$postData = $this->input->post();
		switch(trim($postData['traingType'])){
			case "ldp_mdp":
				$resultData = leadership_development(trim($postData['surveyText']));
			break;
			case "fun_tech":
				$resultData = func_tech_development(trim($postData['surveyText']));
			break;
			case "pd_beha":
				$resultData = personal_behav_development(trim($postData['surveyText']));
			break;
			
		}
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){ 
				$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'">'.$resultRecord->title.'</option>';
		}
		echo $drilDownList;
	}
	
	public function submit_survey_v2(){
		$postData = $this->input->post();
		//echo '<pre>'; print_r($postData);exit;
		if($postData['submit'] !=''){
			switch($postData['submit']){
				case 'Save':
					$save_ary = array(
						'emp_name'=>trim($postData['emp_name']),
						'business_name'=>trim($postData['business_name']),
						'emp_code'=>trim($postData['emp_code']),
						'emp_location'=>trim($postData['emp_location']),
						'emp_reporting_manager'=>trim($postData['emp_reporting_manager']),
						'emp_hod'=>trim($postData['emp_hod']),
						'ldp_mdp_1'=>trim($postData['ldp_mdp_1']),
						'sub_ldp_mdp_1'=>trim($postData['sub_ldp_mdp_1']),
						'ldp_mdp_2'=>trim($postData['ldp_mdp_2']),
						'sub_ldp_mdp_2'=>trim($postData['sub_ldp_mdp_2']),
						'fun_tech_1'=>trim($postData['fun_tech_1']),
						'sub_fun_tech_1'=>trim($postData['sub_fun_tech_1']),
						'fun_tech_2'=>trim($postData['fun_tech_2']),
						'sub_fun_tech_2'=>trim($postData['sub_fun_tech_2']),
						'pd_beha_1'=>trim($postData['pd_beha_1']),
						'sub_pd_beha_1'=>trim($postData['sub_pd_beha_1']),
						'pd_beha_2'=>trim($postData['pd_beha_2']),
						'sub_pd_beha_2'=>trim($postData['sub_pd_beha_2']),
						'qhse_list_1'=>trim($postData['qhse_list_1']),
						'qhse_list_2'=>trim($postData['qhse_list_2']),
						'tr_need_1'=>trim($postData['tr_need_1']),
						'tr_respondent_1'=>trim($postData['tr_respondent_1']),
						'tr_need_2'=>trim($postData['tr_need_2']),
						'tr_respondent_2'=>trim($postData['tr_respondent_2']),
						'is_save'=>1,
						'is_submit'=>0
					);
				break;
				case 'Submit':
					$save_ary = array(
						'emp_name'=>trim($postData['emp_name']),
						'business_name'=>trim($postData['business_name']),
						'emp_code'=>trim($postData['emp_code']),
						'emp_location'=>trim($postData['emp_location']),
						'emp_reporting_manager'=>trim($postData['emp_reporting_manager']),
						'emp_hod'=>trim($postData['emp_hod']),
						'ldp_mdp_1'=>trim($postData['ldp_mdp_1']),
						'sub_ldp_mdp_1'=>trim($postData['sub_ldp_mdp_1']),
						'ldp_mdp_2'=>trim($postData['ldp_mdp_2']),
						'sub_ldp_mdp_2'=>trim($postData['sub_ldp_mdp_2']),
						'fun_tech_1'=>trim($postData['fun_tech_1']),
						'sub_fun_tech_1'=>trim($postData['sub_fun_tech_1']),
						'fun_tech_2'=>trim($postData['fun_tech_2']),
						'sub_fun_tech_2'=>trim($postData['sub_fun_tech_2']),
						'pd_beha_1'=>trim($postData['pd_beha_1']),
						'sub_pd_beha_1'=>trim($postData['sub_pd_beha_1']),
						'pd_beha_2'=>trim($postData['pd_beha_2']),
						'sub_pd_beha_2'=>trim($postData['sub_pd_beha_2']),
						'qhse_list_1'=>trim($postData['qhse_list_1']),
						'qhse_list_2'=>trim($postData['qhse_list_2']),
						'tr_need_1'=>trim($postData['tr_need_1']),
						'tr_respondent_1'=>trim($postData['tr_respondent_1']),
						'tr_need_2'=>trim($postData['tr_need_2']),
						'tr_respondent_2'=>trim($postData['tr_respondent_2']),
						'is_save'=>0,
						'is_submit'=>1
					);
				break;
			}
			
			if($this->Survey_model->checkEmpV2(trim($postData['emp_code']))){
				$this->db->insert('survey_report_v2',$save_ary);	
			}else{
				$this->db->where('emp_code',trim($postData['emp_code']));
				$this->db->update('survey_report_v2',$save_ary);
			}
			
		}else{
			echo 'Server error';
		}
		redirect('survey/indexV2');
	}
	
	//check emp code 
	public function checkEmpCode(){
		$postData = $this->input->post();
		if($this->Survey_model->checkEmpV2(trim($postData['emp_code']))){
			$status = false;
		}else{
			$status = true;
		}
		echo $status;
	}
	
	
	//TNI Survey V2 22-3-2017 end
	
}	


?>