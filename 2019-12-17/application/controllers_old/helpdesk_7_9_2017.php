<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdesk extends MY_Controller {
	public function __construct()
	{ 
		parent::__construct();
		$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->model('Helpdesk_model');
		
		$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        $mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        $this->hrdata_user_info = $mssql_user_info_query->result_array();
	}
	
	//ldap data 
	public function get_ledap(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		return $data;
	}
	
	
	//user panel start 
	public function index(){
		$data = $this->get_ledap();
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$this->load->view('helpdesk/homepage',$data);
	}
	
	//user panel end
	
	//admin panel start 
	public function admin(){
		$data = $this->get_ledap();
		$this->load->view('helpdesk/homepageadmin',$data);
	}
	
	public function open_request(){
		$data = $this->get_ledap();
		$this->load->view('helpdesk/open_request',$data);
	}
	
	public function open_request_details(){
		$data = $this->get_ledap();
		$this->load->view('helpdesk/open_request_details',$data);
	}
	//admin panel end
	
	//assign report
	public function tech_assign_list(){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = $this->Helpdesk_model->get_assign_tech_list();
		$this->load->view('helpdesk/report',$data);
	}
	
	public function testpage(){
		$this->load->view('helpdesk/permission_page');
	}
	public function new_request($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		//echo "<pre>";print_r($data);die;
		$this->load->view('helpdesk/new_request',$data);
	}
	
	public function get_subcategory_hd()
	{	$postData = $this->input->post();
		$resultData = $this->Helpdesk_model->get_subcategory_hd($postData['categoryid']);
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){ 
			//if($resultRecord->tbl_primary_id == $keySelect){
				//$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'" //selected="selected">'.$resultRecord->title.'</option>';
			//}else{
				$drilDownList.='<option value="'.$resultRecord->sub_category_id.'">'.$resultRecord->sub_category_name.'</option>';
			//}
		}
			echo $drilDownList;
	}
	
	public function get_item_hd()
	{	$postData = $this->input->post();
		$resultData = $this->Helpdesk_model->get_item_hd($postData['subcategory_id']);
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){ 
			//if($resultRecord->tbl_primary_id == $keySelect){
				//$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'" //selected="selected">'.$resultRecord->title.'</option>';
			//}else{
				$drilDownList.='<option value="'.$resultRecord->items_id.'">'.$resultRecord->item_name.'</option>';
			//}
		}
			echo $drilDownList;
	}
	
	public function user_request_hd(){
		//echo "<pre>";print_r($this->input->post());
		//echo "<br/>";print_r($_FILES);
		if($this->input->post()){
			if(!empty($_FILES['request_attach']["name"])){
				$fileUpload = $this->upload_attachment_hd();	
				if(empty($fileUpload)){
					//if any attachments
					
				   $this->save_user_request();
				   
				   $item_id = $this->input->post('item');
				   $technician_id = get_technician_id($item_id);
				   $request_id = $this->db->insert_id();
				   $this->Helpdesk_model->assign_request($technician_id,$request_id,$item_id);
				   
				}else{
					$this->session->set_flashdata('error_msg',$fileUpload);
					redirect('helpdesk/new_request/'.$this->input->post('dept_id'));
				}
			}else{
				//if no attachments
				$this->save_user_request();
				
				$item_id = $this->input->post('item');
				$technician_id = get_technician_id($item_id);
				$request_id = $this->db->insert_id();
				$this->Helpdesk_model->assign_request($technician_id,$request_id,$item_id);
			}
		}
	}
	
	public function save_user_request(){
		if($this->Helpdesk_model->save_user_request()){
			$this->session->set_flashdata('msg','A ticket has been raised against this request.');
			redirect('helpdesk/new_request/'.$this->input->post('dept_id'));
		}else{
			$this->session->set_flashdata('error_msg','An error occured while Saving...');
			redirect('helpdesk/new_request/'.$this->input->post('dept_id'));
		}
	}
	
	//new
	public function upload_attachment_hd(){
		
		$valid_formats = array("jpeg","jpg", "png", "gif", "bmp","pdf","xls","xlsx","ppt","pptx","doc","docx","htm","html");
		$max_file_size = 1024*1024*10; 
		$path = "uploads/helpdesk/"; // Upload directory
		$count = 0;
		$user['user_ldamp'] = $this->user_ldamp;
		$userid =	$user['user_ldamp']['user_id'];
		$path .= $userid;
		if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
			// Loop $_FILES to exeicute all files
			$uploadedAttachment = count($_FILES['request_attach']['name']);
			foreach ($_FILES['request_attach']['name'] as $f => $name) {
				if ($_FILES['request_attach']['error'][$f] == 4) {
					continue; // Skip file if any error found
				}
				if ($_FILES['request_attach']['error'][$f] == 0) {
					if ($_FILES['request_attach']['size'][$f] > $max_file_size) {
						$message[] = "$name is too large!.Maximum file size allowed to be  is 10 MB";
						continue; // Skip large files
					}
					elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
						$message[] = "$name is not a valid format";
						continue; // Skip invalid file formats
					}else{ // No error found! Move uploaded files
							$ext = explode('.', $name);
							$file_hd = "hd_" . random_string('alnum', 20) . "." . $ext[1];
						if(move_uploaded_file($_FILES["request_attach"]["tmp_name"][$f], $path.'/'.$file_hd)){
							$data[$count] = $file_hd;
							$count++; // Number of successfully uploaded file
						}
					}
				}
			}
		}
		if($uploadedAttachment === $count){
			// save the uploaded attachments if the count is equal to no of attachments 
		}
	    return $message;
    }
	
	//ajx guideline
	public function getGuide(){
		$data = $this->get_ledap();
		$postData = $this->input->post();
		if($postData['department'] > 0 && $postData['department'] !=''){
			$guideline = $this->Helpdesk_model->getAjxGuideDataByDeptId($postData['department']);
		}else{
			$guideline = 'No guideline for the department';
		}
		echo $guideline;
	}
	
	//cron job request filter
	public function cron_job_request_assign(){
		//get request 
		$requestData = $this->Helpdesk_model->get_unassign_request();
		foreach($requestData as $requestRecord){
			$tech_id = get_technician_id($requestRecord->item_id);
			echo 'tech id '.$tech_id;
			if($tech_id > 0){
				//assgin request to tech		
				$this->Helpdesk_model->assign_request($tech_id,$requestRecord->user_request_id,$requestRecord->item_id);
			}
		}
	}
	
	//request is completed 
	public function complate_request($request_id = 0){
		if($request_id > 0){
			$this->Helpdesk_model->tech_task_complete($request_id);
		}
		echo 'request id is '.$request_id;
	}
}
