<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdesk extends MY_Controller {
	public function __construct()
	{ 
		parent::__construct();
		//$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->model('Helpdesk_model');
		//$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        //$mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        //$this->hrdata_user_info = $mssql_user_info_query->result_array();
		include APPPATH . 'third_party/class.phpmailer.php';
		 $lognSql = "select * from users where u_ldap_userid='" . $this->user_ldamp['samaccountname'] . "' ";
        $this->user_indo = $this->Base_model->run_query($lognSql);
	}
	
	//ldap data 
	public function get_ledap(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		return $data;
	}
	
	
	//user panel start 
	public function index(){
		//$data = $this->get_ledap();
		//$data['request_counter'] = get_request_count_by_email($data); 
		//$data['departments'] = $this->Helpdesk_model->get_departments();
		//$this->load->view('helpdesk/homepage',$data);
		redirect('helpdesk/dashboard');
	}
	
	//user panel end
	
	//admin panel start 
	public function admin(){
		$data = $this->get_ledap();
		$this->load->view('helpdesk/homepageadmin',$data);
	}
	
	public function open_request(){
		$data = $this->get_ledap();
		$this->load->view('helpdesk/open_request',$data);
	}
	
	//request details and resolution
	public function open_request_details($requestId = 0){
		$data = $this->get_ledap();
		$data['requestData'] = $this->Helpdesk_model->getRequestData($requestId);
		$this->load->view('helpdesk/open_request_details',$data);
		
	}
	
	//save resolution comment
	public function resolution_comment(){
		$data = $this->get_ledap();
		//echo '<pre>';print_r($data);
		$postData = $this->input->post();
		$requestData = get_requestdata_by_id($postData['request_id']);
		$resolutionComment = array(
			'user_request_id'=>$postData['request_id'],
			'technician_id'=> '',
			'response_desc'=>$postData['resolution_status'],
			'resolution_status'=>$postData['resolution_status_code'] //WIP, OPEN, CLOSE
		);
		//echo '<pre>'; print_r($postData);
	}
	//
	
	
	//admin panel end
	
	//assign report
	public function tech_assign_list(){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = $this->Helpdesk_model->get_assign_tech_list();
		$this->load->view('helpdesk/report',$data);
	}
	
	public function testpage(){
		$this->load->view('helpdesk/permission_page');
	}
	
	public function new_request_old($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		//echo "<pre>";print_r($data);die;
		$this->load->view('helpdesk/new_request',$data);
	}
	
	public function get_subcategory_hd()
	{	$postData = $this->input->post();
		$resultData = $this->Helpdesk_model->get_subcategory_hd($postData['categoryid']);
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){ 
			//if($resultRecord->tbl_primary_id == $keySelect){
				//$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'" //selected="selected">'.$resultRecord->title.'</option>';
			//}else{
				$drilDownList.='<option value="'.$resultRecord->sub_category_id.'">'.$resultRecord->sub_category_name.'</option>';
			//}
		}
			echo $drilDownList;
	}
	
	public function get_item_hd()
	{	$postData = $this->input->post();
		$resultData = $this->Helpdesk_model->get_item_hd($postData['subcategory_id']);
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){ 
			//if($resultRecord->tbl_primary_id == $keySelect){
				//$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'" //selected="selected">'.$resultRecord->title.'</option>';
			//}else{
				$drilDownList.='<option value="'.$resultRecord->items_id.'">'.$resultRecord->item_name.'</option>';
			//}
		}
			echo $drilDownList;
	}
	
	public function get_location_hd()
	{	$postData = $this->input->post();
		$resultData = $this->Helpdesk_model->get_location_hd($postData['state_id']);
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){
			$drilDownList.='<option value="'.$resultRecord->site_location_id.'">'.$resultRecord->site_location_name.'</option>';
		}
		echo $drilDownList;
	}
	
	public function user_request_hd(){
		if($this->input->post()){
			if(!empty($_FILES['request_attach']["name"])){
				//$fileUpload = $this->upload_attachment_hd();	
				//if(empty($fileUpload)){
					//if any attachments
					$this->save_user_request();
				//	$requestId = $this->db->insert_id();
				//	$fileUpload = $this->upload_attachment_hd($requestId);
				//}else{
				//	$this->session->set_flashdata('error_msg',$fileUpload);
				//	redirect('helpdesk/new_request/'.$this->input->post('dept_id'));
				//}
			}else{
				//if no attachments
				$this->save_user_request();
			}
		}
	}
	// the request url function is commented
	public function save_user_request(){
		$data = $this->get_ledap();
		if($this->Helpdesk_model->save_user_request()){
			
			if(!empty($_FILES['request_attach']["name"])){
				$requestId = $this->db->insert_id();
				$fileUpload = $this->upload_attachment_hd($requestId);
			}
			
			if($this->cron_job_new()){
				$this->session->set_flashdata('msg','A ticket has been raised against this request.');
			}else{
				$this->session->set_flashdata('error_msg','An error occured while ticket raise.');
			}
			
			//redirect('helpdesk/admin_new_request/'.$this->input->post('dept_id')); //$this->requestURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid'],$this->input->post('dept_id'));
			redirect('helpdesk/new_request/'.$this->input->post('dept_id'));
		}else{
			$this->session->set_flashdata('error_msg','An error occured while ticket raise...');
			//redirect('helpdesk/admin_new_request/'.$this->input->post('dept_id'));
           //$this->requestURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid'],$this->input->post('dept_id'));
			redirect('helpdesk/new_request/'.$this->input->post('dept_id'));
		}
	}
	
	//new
	public function upload_attachment_hd($request_id=0){
		
		//$valid_formats = array("jpeg","jpg", "png",,"pdf",, "gif");
		$valid_formats = array("jpeg","jpg", "png", "gif", "bmp","pdf","xls","xlsx","ppt","pptx","doc","docx","htm","html");
		$max_file_size = 1024*1024*1; 
		$path = "uploads/helpdesk/"; // Upload directory
		$count = 0;
		$user['user_ldamp'] = $this->user_ldamp;
		$userid =	$user['user_ldamp']['user_id'];
		$path .= $userid;
		if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
			// Loop $_FILES to exeicute all files
			$uploadedAttachment = count($_FILES['request_attach']['name']);
			foreach ($_FILES['request_attach']['name'] as $f => $name) {
				if ($_FILES['request_attach']['error'][$f] == 4) {
					continue; // Skip file if any error found
				}
				if ($_FILES['request_attach']['error'][$f] == 0) {
					if ($_FILES['request_attach']['size'][$f] > $max_file_size) {
						$message[] = "$name is too large!.Maximum file size allowed to be  is 1 MB";
						continue; // Skip large files
					}
					elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
						$message[] = "$name is not a valid format";
						continue; // Skip invalid file formats
					}else{ // No error found! Move uploaded files
							$ext = explode('.', $name);
							$file_hd = "hdrv" . random_string('alnum', 20) . "." . $ext[1];
						if(move_uploaded_file($_FILES["request_attach"]["tmp_name"][$f], $path.'/'.$file_hd)){
							$data[$count] = $file_hd;
							$count++; // Number of successfully uploaded file
							$attachmentAry = array(
					            'request_id'=>$request_id,
					            'attachment_name'=>$file_hd,
					            'created_date'=>date('Y-m-d H:i:s')
				            );
			                $this->db->insert("request_attach_hd",$attachmentAry);
						}
					}
				}
			}
		}
		
	    return $message;
    }
	
	//ajx guideline
	public function getGuide(){
		$data = $this->get_ledap();
		$postData = $this->input->post();
		if($postData['department'] > 0 && $postData['department'] !=''){
			$guideline = $this->Helpdesk_model->getAjxGuideDataByDeptId($postData['department']);
		}else{
			$guideline = 'No guideline for the department';
		}
		echo $guideline;
	}
	
	//cron job request filter -- not working properly 
	public function cron_job_request_assign(){
		//get request 
		$requestData = $this->Helpdesk_model->get_unassign_request();
		foreach($requestData as $requestRecord){
			$tech_id = get_technician_id($requestRecord->item_id);
			echo 'tech id '.$tech_id;
			if($tech_id > 0){
				//assgin request to tech		
				$this->Helpdesk_model->assign_request($tech_id,$requestRecord->user_request_id,$requestRecord->item_id);
			}
			exit;
		}
	}
	
	//request is completed 
	public function complete_request($request_id = 0){
		if($request_id > 0){
			$this->Helpdesk_model->tech_task_complete($request_id);
		}
		echo 'request id is '.$request_id;
	}
	
	
	
	//NEW * 5_2_2017 new cron job << CODE V2 >>
	public function cron_job_new($frm_email = 0){
		$returnStatus = false;
		$requestData = $this->Helpdesk_model->get_unassign_request();
		
		foreach($requestData as $requestRecord){
			$qry = $this->db->query("SELECT * FROM group_item_map_hd WHERE group_item_map_is_active=1 AND item_id='".$requestRecord->item_id."'");
			$groupData = $qry->row();
			$group_id = $groupData->group_id;
			$locationID = $requestRecord->requester_location_id;
			//echo '<br><br><br>'.$this->db->last_query().'-'.$requestRecord->user_request_id; 
			
			//$tech_qry = $this->db->query("SELECT * FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) WHERE tg.group_id ='$group_id' AND t.technician_is_active = 1");
			
			//SELECT *,t.technician_id as tech_id FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) LEFT JOIN track_assign_count_hd tac ON (tac.technician_id = t.technician_id) WHERE tg.group_id ='1' AND t.technician_is_active = '1' AND t.technician_id IN (SELECT technician_id FROM iconnect_demo.technician_group_location_map WHERE group_id='1' AND location_id='28' AND technician_group_location_map_is_active='1') ORDER BY tac.task_assign_count ASC;
			
			$qry_loc = $this->db->query("SELECT technician_id FROM technician_group_location_map WHERE group_id='$group_id' AND location_id='$locationID' AND technician_group_location_map_is_active='1'");
			
			if($qry_loc->num_rows() > 0 && $requestRecord->dept_id == 5){
				$tech_qry = $this->db->query("SELECT *,t.technician_id as tech_id FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) LEFT JOIN track_assign_count_hd tac ON (tac.technician_id = t.technician_id) WHERE tg.group_id ='$group_id' AND t.technician_is_active = '1' AND t.technician_id IN (SELECT technician_id FROM technician_group_location_map WHERE group_id='$group_id' AND location_id='$locationID' AND technician_group_location_map_is_active='1') ORDER BY tac.task_assign_count ASC");
			}else{
				$tech_qry = $this->db->query("SELECT *,t.technician_id as tech_id FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) LEFT JOIN track_assign_count_hd tac ON (tac.technician_id = t.technician_id) WHERE tg.group_id ='$group_id' AND t.technician_is_active = 1 ORDER BY tac.task_assign_count ASC");
			}
			
			//$tech_qry = $this->db->query("SELECT *,t.technician_id as tech_id FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) LEFT JOIN track_assign_count_hd tac ON (tac.technician_id = t.technician_id) WHERE tg.group_id ='$group_id' AND t.technician_is_active = 1 ORDER BY tac.task_assign_count ASC ");
			$tech_list = $tech_qry->result();
			//echo '<br><br>'.$this->db->last_query().'-'.$requestRecord->user_request_id; 
			
			foreach($tech_list as $tech_record){
				$returnData_tech = $tech_record->tech_id; // get the technician id and assign him to task
				$this->Helpdesk_model->assign_request($tech_record->tech_id,$requestRecord->user_request_id,$requestRecord->item_id);
				$this->addAssignCount($tech_record->tech_id);
				
				//send email to customer
				$this->sendEmailTemple($requestRecord->user_request_id,1,0,'','',$frm_email);
				
				$this->sendEmailTemple($requestRecord->user_request_id,2,$tech_record->tech_id,'','',$frm_email);
				
				if($requestRecord->service_request_type == 'Other'){
					$ldapData = $this->getldapDataByEmail($requestRecord->behalf_of);
					if($ldapData !=0){
						if(count($ldapData) > 0){
							$this->sendEmailTemple($requestRecord->user_request_id,1,0,$ldapData['mail'],$ldapData['displayname']);
							$this->sendEmailTemple($requestRecord->user_request_id,2,$tech_record->tech_id,$ldapData['mail'],$ldapData['displayname']);
							
							//sent email to technician
							//$this->sendEmailTemple($requestRecord->user_request_id,1,0,$tech_record->technician_email,$tech_record->tech_name);
							//$this->sendEmailTemple($requestRecord->user_request_id,2,$tech_record->tech_id,$tech_record->technician_email,$tech_record->tech_name);
						}
					}
				}
				
				//echo '<br>tech is NOT assigned request id '.$requestRecord->user_request_id.' tech id '.$tech_record->tech_id;
				break;
				//echo '<br>tech is assigned '.$tech_record->tech_id;
			}
			$returnStatus = true;
			
		}
		//echo '<pre>'; print_r($returnData_tech);exit;
		//return true;
		
		return $returnStatus;
	}
	
	
	
	//get add assign count << CODE V2 >>
	public function addAssignCount($tech_id=0){
		if($tech_id > 0){
			$qry = $this->db->query("SELECT * FROM track_assign_count_hd WHERE technician_id = '$tech_id'");
			//check if technician is exists or not
			if($qry->num_rows() > 0){
				$countRecord = $qry->row();
				$countVal = $countRecord->task_assign_count + 1;
				$countValAry = array(
					'task_assign_count'=>$countVal
				);
				$this->db->where("technician_id",$tech_id);
				$this->db->update("track_assign_count_hd",$countValAry);
			}else{
				$countVal =1;
				$countValAry = array(
					'technician_id'=>$tech_id,
					'task_assign_count'=>$countVal
				);
				$this->db->insert("track_assign_count_hd",$countValAry);
			}
		}
		return true;
	}
	
	
	//email and ldap sync start 
	
	//send email - SMTP
	public function sendOutlookEmail($toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);
		$mail = '';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name="Helpdesk";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('ravi.kumar@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	
	//recive email - IMAP 
	public function receiveOutlookEmail_old(){
		$this->load->library('email_reader');
		ini_set('max_execution_time', 3000);
		//$email =$this->email_reader->get(207);
		$emails = $this->email_reader->inbox(207);
		echo "<pre>";print_r($emails);exit();
	}
	
	//retrive email tickets start <<< NEW CODE >>> CRON JOB TO FETCH data from outlook **
	public function getOutlookmail(){
		// get emails 
		$emailList = $this->receiveOutlookEmail();
		//insert the email into the database
		foreach($emailList as $emailRecord){
			$emailExists = $this->Helpdesk_model->checkEmailExsitsByUniqueId($emailRecord['email_unique_id']);
			if(count($emailExists) > 0){} else {
				//insert email to database 
				$outlookEmail = array(
					'email_unique_id'=>$emailRecord['email_unique_id'],
					'email_from'=>$emailRecord['email_from'],
					'email_subject'=>$emailRecord['email_subject'],
					'email_body'=>$emailRecord['email_body'],
					'email_created'=>date('Y-m-d H:i:s')
				);
				$this->db->insert('outlook_email',$outlookEmail);
			
		
				$emailId = $this->db->insert_id();
				$postData = getEmpByEmail1($emailRecord['email_from']);
				$behalf_of = $emailRecord['email_from'];
				$request_type = 'Self';
				
				$data = array(
					'email_id'=>$emailId,
					'user_id'=>$postData->u_id,
					'user_code'=>$postData->u_employeeid,
					'request_type'=>'email', 
					'behalf_of'=>$behalf_of,
					'service_request_type'=>$request_type,
					'request_priority'=>'High',
					'requester_name'=>$postData->u_name,
					'requester_location_id'=>0,
					'requester_location_name'=>$postData->u_streetaddress,
					'dept_id'=>0,
					'category_id'=>0,
					'subcategory_id'=>0, 
					'item_id'=>0, 
					'request_subject'=>$emailRecord['email_subject'], 
					'request_desc'=>$emailRecord['email_body'],
					'request_created_dt'=>date('Y-m-d H:i:s')
				);
				// filter and insert into database 
				if($this->Helpdesk_model->save_user_request_outlook($data)){
					// assign technician
					//$this->cron_job_new(1);
				}
			}
		}
	}
	
	//reveive email -- without library
	public function receiveOutlookEmail(){
		$imap = imap_open("{outlook.office365.com:143}", smu,smp);
		$num = imap_num_msg($imap); 
		$num_retrive = $num - 10;
		//echo '<table border="1" style="border-color: red;border-width: 4px;">';
		//echo '<pre>';
		//$str_out .='<tr><th> Subject </th><th> Body </th></tr>';
		for($i=$num_retrive;$i<=$num;$i++){
			$attachments = imap_fetchstructure($imap, $i);
			
			$unique_id = imap_uid($imap, $i);
			$headerData = imap_headerinfo ($imap ,$i);
			
			echo '--------------------Header------------------------------';
			echo '<br><b>From id :</b>'.$headerData->from[0]->mailbox.'@'.$headerData->from[0]->host.' -- <b>Unique Id</b> '.$unique_id.' <br>';
			echo '<b>Subject :</b>'.$headerData->subject.'<br>';
			echo '--------------------Body------------------------------';
			echo '<br><br>';
			
			//print_r($attachments);echo 'attachhhhhment<br>';
			//$body = imap_qprint(imap_body($imap, $i));
			//$body =  imap_fetch_overview($imap, $i);
			$body =  imap_fetchstructure($imap, $i);
			//print_r($body);
		
			$structure = imap_fetchstructure($imap, $i);
			$flattenedParts = $this->flattenParts($structure->parts);
			
			foreach($flattenedParts as $partNumber => $part) {
				//print_r($part);exit;
				switch($part->type) {
					case 0:
						// the HTML or plain text part of the email
						$message = $this->getPart($imap, $i, $partNumber, $part->encoding);
						//echo $message;
						// now do something with the message, e.g. render it
					break;
				
					case 1:
						// multi-part headers, can ignore
				
					break;
					case 2:
						// attached message headers, can ignore
					break;
				
					case 3: // application
					case 4: // audio
					case 5: // image
					case 6: // video
					case 7: // other
						$filename = $this->getFilenameFromPart($part);
						if($filename) {
							// it's an attachment
							$attachment = $this->getPart($imap, $i, $partNumber, $part->encoding);
							//echo '<pre> attachment <br>';print_r($attachment);
							// now do something with the attachment, e.g. save it somewhere
						} else {
							// don't know what it is
						}
					break;
				}
			}
			
			//$str_out .='<tr><td> '.$headerData->subject.' </td><td>'.$body .'</td></tr>';
			$emailReciAry[] = array(
				'email_unique_id' =>$unique_id,
				'email_from' =>$headerData->from[0]->mailbox.'@'.$headerData->from[0]->host,
				'email_subject'=>$headerData->subject,
				'email_body'=>$message
			);
			
			print_r($emailReciAry);
		}
		
		imap_close($imap);
		//echo $str_out;
		//echo '</table>';
		return $emailReciAry;
	}
	
	//IMPA SUPPORT FUNCTION  START 
	public function flattenParts($messageParts, $flattenedParts=array(), $prefix = '', $index = 1, $fullPrefix = true) {
		foreach($messageParts as $part) {
			$flattenedParts[$prefix.$index] = $part;
			if(isset($part->parts)) {
				if($part->type == 2) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.', 0, false);
				}
				elseif($fullPrefix) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.');
				}
				else {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix);
				}
				unset($flattenedParts[$prefix.$index]->parts);
			}
			$index++;
		}
		return $flattenedParts;
	}
	
	public function getPart($connection, $messageNumber, $partNumber, $encoding) {
		$data = imap_fetchbody($connection, $messageNumber, $partNumber);
		switch($encoding) {
			case 0: return $data; // 7BIT
			case 1: return $data; // 8BIT
			case 2: return $data; // BINARY
			case 3: return base64_decode($data); // BASE64
			case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
			case 5: return $data; // OTHER
		}	
	}

	public function getFilenameFromPart($part) {
		$filename = '';
		if($part->ifdparameters) {
			foreach($part->dparameters as $object) {
				if(strtolower($object->attribute) == 'filename') {
					$filename = $object->value;
				}
			}
		}
		if(!$filename && $part->ifparameters) {
			foreach($part->parameters as $object) {
				if(strtolower($object->attribute) == 'name') {
					$filename = $object->value;
				}
			}
		}
		return $filename;
	}
	//IMPA SUPPORT FUNCTION  END 
	
	
	//sync ldap contacts()
	public function getldapDataByEmail($emailId = ''){
		$server = ldap_serverhost;
        $basedn = ldap_basedn;
		$handle = sysu;
		$password = sysp;
        $dn = ldap_basedn . "\\" . $handle;
        $connect = ldap_connect($server);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
		
		//var_dump(ldap_bind($connect, $dn, $password));
        if ($ds = @ldap_bind($connect, $dn, $password)) {
            //$filters = "(samaccountname=$handle)";
			
            $filters = "(cn=*)"; // get all results
            $results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
            $entries = ldap_get_entries($connect, $results);
			
            if(count($entries) > 0){
				for ($i = 0; $i < 2000; $i++) {
					if(strtolower($emailId) === strtolower($entries[$i]["mail"][0])){
						$ldap_userinfo = array(
							'samaccountname' => $entries[$i]["samaccountname"][0],
							'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
							'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
							'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
							'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
							'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
							'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
							'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
							'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
							'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
							'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
							'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
						);
						break;
					}
				}
				//echo '<pre>';print_r($ldap_userinfo);exit;
                return $ldap_userinfo;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
	}
	//email and ldap sync end 
	
	
	//<<NEW CODE >> Twice a day 
	public function cron_job_update_ladp(){
		$server = ldap_serverhost;
        $basedn = ldap_basedn;
		$handle = sysu;
		$password = sysp;
        $dn = ldap_basedn . "\\" . $handle;
        $connect = ldap_connect($server);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
		
		//var_dump(ldap_bind($connect, $dn, $password));
        if ($ds = @ldap_bind($connect, $dn, $password)) {
            //$filters = "(samaccountname=$handle)";
			
            $filters = "(cn=*)"; // get all results
            $results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
            $entries = ldap_get_entries($connect, $results);
			//echo '<pre>';
            if(count($entries) > 0){
				for ($i = 0; $i < 2000; $i++) {
					$ldap_userinfo = array(
						//'usnchanged' => $entries[$i]["usnchanged"][0],
						'samaccountname' => $entries[$i]["samaccountname"][0],
						'unique_id'=> base64_encode($entries[$i]["objectguid"][0]),
						'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
						'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
						'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
						'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
						'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
						'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
						'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
						'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
						'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
						'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
						'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
					);
					//print_r($ldap_userinfo);
					$lognSql = "select * from ldapusers where unique_id='" .$ldap_userinfo['unique_id']. "' ";
					$local_result = $this->Base_model->run_query($lognSql);
					
					if (count($local_result) == 0) {
						$local_user_insert['unique_id'] = $ldap_userinfo['unique_id'];
						$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
						$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
						$local_user_insert['u_name'] = $ldap_userinfo['name'];
						$local_user_insert['u_email'] = $ldap_userinfo['mail'];
						$local_user_insert['u_designation'] = $ldap_userinfo['description'];
						$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
						$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
						
						$local_user_insert['u_image'] = NULL;
						$local_user_insert['u_cover_image'] = NULL;
						
						$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
						$local_user_insert['u_status'] = 1;
						$local_user_insert['u_role'] = 0;
						$this->Base_model->insert_operation($local_user_insert, 'ldapusers');
					} else {
						$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
						$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
						$local_user_insert['u_name'] = $ldap_userinfo['name'];
						$local_user_insert['u_email'] = $ldap_userinfo['mail'];
						$local_user_insert['u_designation'] = $ldap_userinfo['description'];
						$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
						$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
						
						$local_user_insert['u_image'] = NULL;
						$local_user_insert['u_cover_image'] = NULL;
						
						$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
						$local_user_insert['u_status'] = 1;
						$local_user_insert['u_role'] = 0;
						
						$logi_where['unique_id'] = $ldap_userinfo['unique_id'];
						$this->Base_model->update_operation($local_user_insert, 'ldapusers', $logi_where);
					}
				}
			}
		}
	}
	
	//<< NEW CODE >> 
	public function get_technician(){
		$postData = $this->input->post();
		if($postData['group_id'] >= 0){
			$technicianData = getTechnicianByGroupId($postData['group_id']);
		}else{
			$technicianData = array();
		}
		echo json_encode($technicianData);
	}
	
	//<< CODE V2 >>
	public function sendEmailTemple($request_id=0,$temp_code=0,$tech_id=0,$u_email='',$u_name='',$frm_email = 0){
		if($frm_email == 0){
			$qry_1 = $this->db->query("SELECT * FROM users u LEFT JOIN user_request_hd ur ON (u.u_employeeid = ur.user_code) WHERE ur.user_request_id='$request_id'");
			$empData = $qry_1->row();
		}else{
			$qry_1 = $this->db->query("SELECT *,behalf_of as u_email FROM user_request_hd WHERE user_request_id='$request_id'");
			$empData = $qry_1->row();
		}
		
		if($u_name !=''){
			$displayname = $u_name;
		}else{
			$displayname = $empData->requester_name;
		}
		switch($temp_code){
			case 11:
				//The status of the request can be tracked at http://helpdesk.skeiron.com .
				$subject="Your request has been logged with request ID ".getRequestCode($request_id)." ";
				$msg='<h3></h3>
					Dear '.ucfirst($displayname).',

					<p style="margin-left:25px">This is an acknowledgement mail for your request.<br><br>

					Your request has been created with Ticket ID '.getRequestCode($request_id).'.<br><br>

					The subject of the request is : '.$empData->request_subject.'. <br><br>

					</p>
					<br><br>
				<p>	*** This is an automatically generated email, please do not reply *** </p><br>
			<p>
			Regards,<br>
			HelpDesk,<br>
			Skeiron Group.
			</p>';
			break;
			
			case 2:
				$qry_2 = $this->db->query("SELECT * FROM technician_hd WHERE technician_id = '$tech_id' AND technician_is_active = '1'");
				$tech_data = $qry_2->row();
				
				//The status of the request can be tracked at http://helpdesk.skeiron.com .
				
				$subject="Your request has been logged with request ID ".getRequestCode($request_id)." ";
				$msg='<h3></h3>
						Dear '.ucfirst($displayname).',

						<p style="margin-left:25px">This is an acknowledgement mail for your request.<br><br>

						Your request has been created with Ticket ID '.getRequestCode($request_id).'.<br><br>
						
						Your request with id '.getRequestCode($request_id).' has been assigned to '.ucfirst($tech_data->tech_name).'. <br><br>

						The subject of the request is : '.$empData->request_subject.'. <br><br>

						</p>
						<br><br>
					<p>	*** This is an automatically generated email, please do not reply *** </p><br>
				<p>
				Regards,<br>
				HelpDesk,<br>
				Skeiron Group.
				</p>';
			
				$msgSubject= "Created by: ".ucfirst($displayname)." on ".std_time();
				$messagebody = "Operation :CREATE ,performed by:".ucfirst($displayname).".<br/>";
				$messagebody .= "From Host/IP Address:".$_SERVER["REMOTE_ADDR"]."<br/>";
				// add history 
				$postData = array(
				    "request_id"=>$request_id
			    );
		        add_history('ReqCreated',$postData);
				if($tech_data->technician_email !=''){
					$tech_email = $tech_data->technician_email; //uncomment this line
					//$tech_email = 'Dev@SKEIRON.COM';
				}else{
					$tech_email = 'Dev@SKEIRON.COM'; // NEED TO Change Before live
				}
				
			break;
		}
		
		$tech_subject = 'Request ID ##'.getRequestCode($request_id).'## has been assigned to you.';
		$tech_email_temp = $this->sendemail_technician($request_id,$displayname,$empData->request_created_dt,$empData->item_id,$empData->dept_id,$empData->request_subject, $empData->request_desc);
		
		
		if($u_email !=''){
			$empDt = getEmpByEmail($u_email);
			
			//echo 'email : '.$u_email;
			$this->sendOutlookEmail($u_email,$subject,$msg);
			
			//send email to technician
			$this->sendOutlookEmail($tech_email,$tech_subject,$tech_email_temp);
			
			$sys_email_ary = array(
				'to_emails'=>$u_email,
				'subject'=>$subject,
				'description'=>$msg,
				'created_date'=>date('Y-m-d H:i:s'),
				'user_request_id'=>$request_id
			);
			$this->db->insert('system_emails',$sys_email_ary);
			$technician_id = $tech_id;
		   // savehistory($history_type="0",$request_id,$technician_id,$empDt->u_employeeid,$u_email,$msgSubject,$messagebody);
			
		}else{
			//echo 'email : '.$empData->u_email;
			$empDt = getEmpByEmail($u_email);
			
			$this->sendOutlookEmail($empData->u_email,$subject,$msg);
			
			//send email to technician
			$this->sendOutlookEmail($tech_email,$tech_subject,$tech_email_temp);
			
			$sys_email_ary = array(
				'to_emails'=>$empData->u_email,
				'subject'=>$subject,
				'description'=>$msg,
				'created_date'=>date('Y-m-d H:i:s'),
				'user_request_id'=>$request_id
			);
			$this->db->insert('system_emails',$sys_email_ary);
			$technician_id = $tech_id;
		   // savehistory($history_type="0",$request_id,$technician_id,$empDt->u_employeeid,$empData->u_email,$msgSubject,$messagebody);
		}
	
		return true;
	}
	
	//** Email to technician
	public function sendemail_technician($request_code = '',$requester_name='', $request_created_date='',$item_id='',$dept_id='',$request_subj='', $request_desc=''){
		$dept_data = get_record_by_id_code('dept',$dept_id);
		$dept_name = $dept_data->dept_name;
		
		$item_data = get_record_by_id_code('item',$item_id);
		$item_name = $item_data->item_name;
		
		$request_due_date = date('M d ,Y h:i:s A',strtotime('+2 hours',strtotime($request_created_date)));
		$tech_msg ='<p><b>Request details are :</b></p>
			<p><b>Requested by :</b>'.$requester_name.'</p>
			<!--<p><b>Created by :</b> Ravi Arsid</p>-->
			<!--<p><b>Due by date :</b> '.$request_due_date.'</p>-->
			<p><b>Department :</b> '.$dept_name.'</p>
			<!--<p><b>Category :</b> '.$dept_name.'</p>-->
			<p><b>Item :</b> '.$item_name.'</p>
			<p><b>Subject :</b> '.$request_subj.'</p>
			<p><b>Description :</b></p>
			<p>'.$request_desc.'</p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>
		';
		return $tech_msg;
	}
	
	
	
	
	//history **
	public function add_history($historyCode = 0, $postData = 0){
		switch($historyCode){
			case 'tech_assign':
				$history_subject = 'Created by '.$postData['creator_name'].' on May 9,2017 11:23 AM';
				$history_body = 'Operation : CREATE, Performed by : '.$postData['creator_name'].' Techinician assigned to request through <b>Tech Auto Assign</b> From Host/IP Address:<< Ex. ncomsrvr.skeiron.com/192.168.20.51 >>';
				
				$history_ary = array(
					'history_type'=>'tech_assign',
					'request_id'=> $postData['request_id'],
					'technician_id'=> $postData['technician_id'],
					'creator_id'=> $postData['creator_id'],
					'creator_name'=> $postData['creator_name'],
					'creator_email'=> $postData['creator_email'],
					'created_date'=> date('Y-m-d H:i:s'),
					'history_subject'=> $history_subject,
					'history_body'=> $history_body
				);
			break;
			
			case 'tech_re_assign':
			break;
			
			case 'task_create':
			break;
			
			case 'task_update':
			break;
			
			case 'resolution_create':
			break;
			
			case 'resolution_update':
			break;
		}
	}
	
	
	public function test_function(){
		//$postData = $this->input->post();
		echo '<pre>';
		print_r(getHostByName(getHostName()));
		
		$ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
		
		echo $ipaddress;
	}
	/** request details code starts from here*/
	public function requestdetails()
    {
		$data = $this->get_ledap();
        $id = $this->uri->segment(3);
		$view = ($this->uri->segment(4)!='')?$this->uri->segment(4):'';
        if(empty($id)){
            show_404();
        }
		
        $data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($id);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id); 
		$data['emails']  = $this->get_system_mails_for_request($id,$view);
		 
        //echo "<pre>";print_r($data);exit();
        $this->load->view("helpdesk/open_request_details",$data);
    }

    public function forward_request()
    {
        $this->load->view("forward_request_tpl");
    }
	
	/** request details code ends here*/
	
	//email template design start 
	public function emailtemplate(){
		$this->load->view('helpdesk/emailtemplate');
	}
	
	public function emailcomposer($user_request_id,$request_type='FORWARD'){
		$data['request_details']  =  $this->Helpdesk_model->getRequestData($user_request_id);
		$data['ticket_status'] = ($request_type == 'FORWARD') ? false:true;
		$data['request_type']  = $request_type;
		$data['emails'] = $this->get_emails_by_request($user_request_id);
		
		$this->load->view('helpdesk/emailcomposer',$data);
	}
	
	public function getEmail($val=0){
		$postData = $this->input->post();
		$emailList = array();
		$request_param = $postData['phrase'];
		$getEmail = $this->Helpdesk_model->getEmailByKey($request_param);
		foreach($getEmail as $emailrecord){
			$emailAry = array(
				'name'=>$emailrecord->u_email
			);
			$emailList[] = $emailAry;
		}
		echo json_encode($emailList);
	}
	//email template design end 
	
	public function getCategoryByDeptId(){
		$postData = $this->input->post();
		$getcategory = $this->Helpdesk_model->get_categories_by_dept_id($postData['dept_id']);
		echo json_encode($getcategory);
	}
	public function getSubCategoryByCategoryId(){
		$postData = $this->input->post();
		$getsubcategory = $this->Helpdesk_model->get_subcategories_by_category_id($postData['category_id']);
		echo json_encode($getsubcategory);
	}
	public function getItemBySubcategoryId(){
		$postData = $this->input->post();
		$getitems = $this->Helpdesk_model->get_items_by_subcategory_id($postData['sub_category_id']);
		echo json_encode($getitems);
	}
	
	public function saveRequestDetails()
	{	
		//echo "<pre>";print_r($this->input->post());
		//$postData = $this->input->post();
		/*$requestAry = array('request_id','requester_user_code','status','prev_status','work_Station','prev_workstation','mode','prev_mode','dept','prev_dept','site','prev_site','priority','prev_priority','group','prev_group','category','prev_category','technician','prev_technician','subactegory','prev_subcategory','item','prev_item','create_date','dueby_date','prev_dueby_date','response_dueby','prev_res_dueby_date','update_request');
		$matchArray = array('status'=>'prev_status','work_Station'=>'prev_workstation',
		'mode'=>'prev_mode','dept'=>'prev_dept','site'=>'prev_site','priority'=>'prev_priority','group'=>'prev_group','category'=>'prev_category','technician'=>'prev_technician','subactegory'=>'prev_subcategory','item'=>'prev_item','dueby_date'=>'prev_dueby_date','response_dueby'=>'prev_res_dueby_date');
		$history_array = array();
		for($i=0;$i<=count($postData); $i++){
			
			if(in_array($requestAry[$i],array("request_id","requester_user_code","create_date","update_request"))){
				continue;
			} else {
				if(($matchArray[$requestAry[$i]]==$requestAry[$i+1]) && $postData[$requestAry[$i]] != $postData[$requestAry[$i+1]]){
				//	echo '<br>['.$requestAry[$i].']['.$requestAry[$i+1].']';
				//echo '<br>'.$postData[$requestAry[$i]].'=='.$postData[$requestAry[$i+1]];
				  $history_array[$requestAry[$i]] =  $postData[$requestAry[$i]];
				  $history_array[$requestAry[$i+1]] = $postData[$requestAry[$i+1]];
				}
			}
		}
	    echo "<pre>";print_r($history_array);
		exit;*/
		/*foreach($this->input->post() as $post_key){
			
			if(in_array($post_key,array("request_id","requester_user_code","create_date","update_request"))){
				continue;
			} else {
				//echo $post_key."<br/>";
				
			}
			$j=$i;
            $i++;
        }die;
		*/
	    $postData = $this->input->post();
		if($this->Helpdesk_model->update_request_details())
		{
			$request_ary = array(
				'request_id'=>$postData['request_id'],
				'group_id'=>$postData['group'],
				'technician_id'=>$postData['technician']
			);
			$this->reassign_technician($request_ary);
			$this->userRequestChange($postData);
			$this->session->set_flashdata('msg','Request saved with the changes successfully.');
			redirect('helpdesk/admin_requestdetail/'.$this->input->post('request_id'));
		}
		else
		{
			$this->session->set_flashdata('error_msg','An internal error occured while saving changes.');
			redirect('helpdesk/admin_requestdetail/'.$this->input->post('request_id'));; 
		}
	}
	public function statusChange(){
		
		$postData = $this->input->post();
		//status change 
		$statusChangeAry = array(
			'comment' => $postData["comment"],
			'request_id' => $postData["request_id"],
			'status' => $postData['status']
		);
		 if($this->Helpdesk_model->save_status_change_details1($statusChangeAry))
		 {  
	        
			$array_status = array("open"=>"Open","wip" =>"WIP","hold"=>"On Hold","close"=>"Closed","resolve"=>"Resolved");
	        $ldap = $this->user_ldamp;
			//$this->tech_mail_to_resolution_change($postData["request_id"]);
			//$this->user_mail_to_resolution_change($postData["request_id"]);
	        //$msg = "";
		   // $msgSubject = "Updated by ".$ldap["name"]." on ".std_time()."<br/>";
	       // $msg .= "Request Updated by ".$ldap["name"]."<br/>"; 
		//	$msg.= "Status changed from ".$array_status[$postData['prev_status']]." to ".$array_status[$postData['status']]."<br/>";
		//	$msg.= "Reason:".$postData["comment"]."<br/>";
			/* savehistory($history_type="0",$postData["request_id"],$postData["technician"],$postData["requester_user_code"],$creator_email='',$msgSubject,$msg);*/
			 echo "success";exit();
		 }
		 echo "fail";
	}
	public function requester_details($user_request_id)
	{	
		$data['request_details']  =  $this->Helpdesk_model->getRequestData($user_request_id);
		$emp_code = $data['request_details']->user_code;
		
	    $data['emp_details'] = get_all_details_by_emp_code($emp_code);
		//echo $this->db->last_query();
		//echo "<pre>";print_r($data);die;
		$this->load->view("helpdesk/requester_details",$data);
	}
	public function request_details_email($user_request_id)
	{	
		$data['request_details']  =  $this->Helpdesk_model->getRequestData($user_request_id);
		$this->load->view("helpdesk/email_request",$data);
	}
	public function sendRequest()
	{
		//echo "<pre>";print_r($this->input->post());print_r($_FILES);exit();
		$postData = $this->input->post();
		$to      =  trim($postData["suggest"]);
		$cc      =  trim($postData["cc"]); 
		$subject =  trim($postData["subject"]);
		$desc    =  trim($postData["summernote"]);
		$request_type = $postData["request_type"];
		$redirectTo = ($request_type == 'FORWARD')? 'helpdesk/emailcomposer/'.$this->input->post('request_id'):
		 'helpdesk/emailcomposer/'.$this->input->post('request_id').'/'.$request_type;
		if($to !=''){
			//echo 'email : '.$u_email;
			$this->sendOutlookEmail($to,$subject,$desc,$cc);
			$this->session->set_flashdata('msg','Email has been send.');
			redirect($redirectTo);
			//data to save the user action will start from here 
			//file uploads
		}else{
			//echo 'email : '.$empData->u_email;
			$this->session->set_flashdata('error_msg','Unable to send the mail.');
			redirect($redirectTo);
		}
	}
	
	public function get_emails_by_request($id){
		$to_email = '';
		$data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
		$email_details = get_email_by_emp_code($data['request_details']->user_code);
		
		$email = (!empty($email_details))?$email_details[0]->u_email:'';
		if($data['request_details']->request_type == "email"||$data['request_details']->request_type == "other"){ 
			
			$to_email = ($data['request_details']->behalf_of!='')?$data['request_details']->behalf_of.','.$email:$email;
		}else{
			$to_email = $email;
		}
		return $to_email;
	}
	
	//5-7-2016 NEW **  // get email conversation 
	public function get_conversation_email($request_id=0,$conversation_id=0){
		$resultData = array();
		if($request_id > 0 && $conversation_id > 0){
			$qry = $this->db->query("SELECT * FROM system_emails WHERE user_request_id='$request_id' AND sytem_emails_id='$conversation_id' AND system_email_is_active='1';");
			if($qry->num_rows() > 0){
				$resultData = $qry->result();
			}
		}
		return $resultData;
	}
	
	
	
	public function get_system_mails_for_request($user_request_id,$view='')
	{
        $show_items = ($view == 'viewall')? 'all':1;
        $emails = $this->Helpdesk_model->get_system_emails($user_request_id,$show_items);
		return $emails;
	}
	
	//reassign technician for specific task start 
	public function reassign_technician($request_ary = array()){
		
		$returnStatus = '0';
		if(count($request_ary) > 0){
			$postData = $request_ary;
		}else{
			$postData = $this->input->post();
		}
		$request_assign_data = get_assign_request_data($postData['request_id']);
		
		if($request_assign_data->group_id == $postData['group_id'] && $request_assign_data->technician_id == $postData['technician_id']){
			$returnStatus = '2'; // re-assign same group and technician gives error
		}else{
			if($this->Helpdesk_model->reassign_request($postData['request_id'],$postData)){
				
				// code to assign the data 
				$user_req = getReqInfoByUserRequestId($postData['request_id']);
				$user_name = $user_req["requester_name"];
				$dept_name = $user_req["dept_name"];
				$item_name = $user_req["item_name"];
				$request_title = $user_req["request_subject"];
				$tech_name = $user_req["technician_name"];
				//send email to new technician
				$techData = array(
					'requesterName'=> $user_name,
					'ticketId'     => $postData['request_id'],
					'deptName'=> $dept_name,
					'itemName'=> $item_name,
					'requestTitle'=>ucfirst($request_title)
				);
				$tech_template = $this->get_email_template('technician_reassign',$techData);
				$subject_tech = "Request re-assigned";
				$tech_data = get_record_by_id_code('technician',$postData['technician_id']);
				$this->sendOutlookEmail($tech_data->technician_email,$subject_tech,$tech_template);
				
				//send email to enduser
				$enduserData = array(
					'eName'=>$user_name,
					'ticketId'=>$postData['request_id'],
					'techName'=>$tech_name,
					'subject' =>ucfirst($request_title)
				);
				$enduser_template = $this->get_email_template('end_user_reassign',$enduserData);
				$subject_enduser = "Request re-assigned";
				$this->sendOutlookEmail($user_req['requester_email'],$subject_enduser,$enduser_template);
				$returnStatus = '1';
			}else{
				$returnStatus = '0';
			}
		}
		return $returnStatus;
	}
	public function tech_reassign_ajx(){
		$request_ary = $this->input->post();
		$returnStatus = $this->reassign_technician($request_ary);
		echo $returnStatus;
	}
	//reassign technician for specific task end 
	//compare the fields for previous and current state to create a history on request update 
	public function userRequestChange($postData)
	{	
	    $msg = "";
		$ldap = $this->user_ldamp;
		//print_r($ldap);exit();
		$msgSubject = "Updated by ".$ldap["name"]." on ".std_time()."<br/>";
	    $msg .= "Request Updated by ".$ldap["name"]."<br/>";
		$statusFlag =  $workStationFlag = $modeFlag = $deptFlag = $siteFlag =$priorityFlag = $groupFlag = $categoryFlag = $technicianFlag = $subactegoryFlag =
		$itemFlag = $dueby_dateFlag = $response_duebyFlag = $allFlag  = false;
		$counter =0;
	    if(!empty($postData)){
			if($postData["status"]!= $postData["prev_status"]){
				//$statusValue = ($postData["prev_status"]!='')?$postData["prev_status"]:"None";
				//$statusNewValue = ($postData["status"]!='')?$postData["status"]:"None";
				//$msg .= "Request Status Changed from ".$statusValue." to ".$statusNewValue."<br/>";
				$statusFlag = true;
				$counter += 1;
					
			}
			if($postData["work_Station"]!= $postData["prev_workstation"]){
				//$workStationValue = ($postData["prev_workstation"]!='')?$postData["prev_workstation"]:"None";
				//$workStationNewValue = ($postData["work_Station"]!='')?$postData["work_Station"]:"None";
				//$msg .= "Workstation Changed from ".$workStationValue." to ".$workStationNewValue."<br/>"; 
				$workStationFlag = true;
				$counter += 1;
				
			}
			if($postData["mode"]!= $postData["prev_mode"]){
				//$modeValue = ($postData["prev_mode"]!='')?$postData["prev_mode"]:"None";
				//$modeNewValue = ($postData["mode"]!='')?$postData["mode"]:"None";
				//$msg .= "Mode  Changed from ".$modeValue." to ".$modeNewValue."<br/>";
				$modeFlag = true;
				$counter += 1;
				
			}
			if($postData["dept"]!= $postData["prev_dept"]){
				/*$deptArrayValue = ($postData["prev_dept"]!='')?get_record_by_id_code('dept',$postData["prev_dept"]):array();
				
				$deptNewArrayValue = ($postData["dept"]!='')?get_record_by_id_code('dept',$postData["dept"]):array();
				$deptValue = (!empty($deptArrayValue))?$deptArrayValue->dept_name:"None";
				$deptNewValue = (!empty($deptNewArrayValue))?$deptNewArrayValue->dept_name:"None";
				$msg .= "Dept  Changed from ".$deptValue." to ".$deptNewValue."<br/>";*/
				$deptFlag = true;
				$counter += 1;
				
			}
			if($postData["site"]!= $postData["prev_site"]){
				//$siteArrayValue = ($postData["prev_site"]!='')?get_record_by_id_code('sitelocation',$postData["prev_site"]):array();
				
				/*$siteNewArrayValue = ($postData["site"]!='')?get_record_by_id_code('sitelocation',$postData["site"]):array();
				$siteValue = (!empty($siteArrayValue))?$siteArrayValue->site_location_name:"None";
				
				$siteNewValue = (!empty($siteNewArrayValue))?$siteNewArrayValue->site_location_name:"None";
				$msg .= "Site  Changed from ".$siteValue." to ".$siteNewValue."<br/>";*/
				$siteFlag = true;
				$counter += 1;
				
			}
			if($postData["priority"]!= $postData["prev_priority"]){
				
				/*$priorityValue = ($postData["prev_priority"]!='')?$postData["prev_priority"]:"None";
				$priorityNewValue = ($postData["priority"]!='')?$postData["priority"]:"None";
				$msg .= "Priority changed from ".$priorityValue ." to ".$priorityNewValue."<br/>";*/
				$priorityFlag = true;
				$counter += 1;
				
			}
			if($postData["group"]!= $postData["prev_group"]){
				/*$grpArrayValue = ($postData["prev_group"]!='')?get_record_by_id_code('group',$postData["prev_group"]):array();
				
				$grpNewArrayValue = ($postData["group"]!='')?get_record_by_id_code('group',$postData["group"]):array();
				$grpValue = (!empty($grpArrayValue))?$grpArrayValue->group_name:"None";
				
				$grpNewValue = (!empty($grpNewArrayValue))?$grpNewArrayValue->group_name:"None";
				$msg .= "Group  Changed from ".$grpValue." to ".$grpNewValue."<br/>";*/
				$groupFlag = true;
				$counter += 1;
				
			}
			if($postData["category"]!= $postData["prev_category"]){
				/*$categoryArrayValue = ($postData["prev_category"]!='')?get_record_by_id_code('category',$postData["prev_category"]):array();
				
				$categoryNewArrayValue = ($postData["category"]!='')?get_record_by_id_code('category',$postData["category"]):array();
				$categoryValue = (!empty($categoryArrayValue))?$categoryArrayValue->category_name:"None";
				
				$categoryNewValue = (!empty($categoryNewArrayValue))?$categoryNewArrayValue->category_name:"None";
				$msg .= "Category  Changed from ".$categoryValue." to ".$categoryNewValue."<br/>";*/
				$categoryFlag = true;
				$counter += 1;
                			
			}
			if($postData["technician"]!= $postData["prev_technician"]){
				/*$techArrayValue = ($postData["prev_technician"]!='')?get_record_by_id_code('technician',$postData["prev_technician"]):array();
				
				$techNewArrayValue = ($postData["technician"]!='')?get_record_by_id_code('technician',$postData["technician"]):array();
				$techValue = (!empty($techArrayValue))?$techArrayValue->tech_name:"None";
				
				$techNewValue = (!empty($techNewArrayValue))?$techNewArrayValue->tech_name:"None";
				$msg .= "technician  Changed from ".$techValue." to ".$techNewValue."<br/>";*/
				$technicianFlag = true;
				$counter += 1;
				
			}
			if($postData["subactegory"]!= $postData["prev_subcategory"]){
				/*$subcatArrayValue = ($postData["prev_subcategory"]!='')?get_record_by_id_code('subcategory',$postData["prev_subcategory"]):array();

				$subcatNewArrayValue = ($postData["subactegory"]!='')?get_record_by_id_code('subcategory',$postData["subactegory"]):array();
				$subcatValue = (!empty($subcatArrayValue))?$subcatArrayValue->sub_category_name:"None";
				
				$subcatNewValue = (!empty($subcatNewArrayValue))?$subcatNewArrayValue->sub_category_name:"None";
				$msg .= "Subcategory  Changed from ".$subcatValue." to ".$subcatNewValue."<br/>";*/
				$subactegoryFlag = true;
				$counter += 1;
				
		
			}
			if($postData["item"]!= $postData["prev_item"]){
				/*$itemArrayValue = ($postData["prev_item"]!='')?get_record_by_id_code('item',$postData["prev_item"]):array();
				
				$itemNewArrayValue = ($postData["item"]!='')?get_record_by_id_code('item',$postData["item"]):array();
				$itemValue = (!empty($itemArrayValue))?$itemArrayValue->item_name:"None";
				
				$itemNewValue = (!empty($itemNewArrayValue))?$itemNewArrayValue->item_name:"None";
				$msg .= "Item  Changed from ".$itemValue." to ".$itemNewValue."<br/>";*/
				$itemFlag = true;
				$counter += 1;
				
			}
			if( strtotime($postData["dueby_date"])!= strtotime($postData["prev_dueby_date"])){
				/*$duebyDateValue = ($postData["prev_dueby_date"]!='')?$postData["prev_dueby_date"]:"None";
				$duebyDateNewValue = ($postData["dueby_date"]!='')?$postData["dueby_date"]:"None";
				$msg .= "Due By Date  Changed from ".$duebyDateValue." to ".$duebyDateNewValue."<br/>";*/
                $dueby_dateFlag =true;
				$counter += 1;
                			
			}
			if(strtotime($postData["response_dueby"])!= strtotime($postData["prev_res_dueby_date"])){
			/*	$response_duebyDateValue = ($postData["prev_res_dueby_date"]!='')?$postData["prev_res_dueby_date"]:"None";
				$response_duebyDateNewValue = ($postData["response_dueby"]!='')?$postData["response_dueby"]:"None";
				$msg .= "Response Due By Date Changed from ".$response_duebyDateValue." to ".$response_duebyDateNewValue."<br/>";
			}*/
			 $response_duebyFlag = true; 
			 $counter += 1;
			 
		}	
		//return $msg;
		//$technician_id = $postData["prev_technician"];
		
		if($counter>1){
			$allFlag = true;
		}
		
	    if($allFlag){
			add_history('ReqAllChange',$postData);
		}
        else
		{
			if($statusFlag){
				add_history('ReqStatus',$postData);
			}
			else if($workStationFlag){
				add_history('ReqWorkStation',$postData);
			}
			else if($modeFlag){
				add_history('ReqMode',$postData);
			}
			else if($deptFlag){
				add_history('ReqDept',$postData);
			}
            else if($siteFlag){
				add_history('ReqSite',$postData);
			} 
            else if($priorityFlag){
				add_history('ReqPriority',$postData);
			}
            else if($groupFlag){
				add_history('ReqGroup',$postData);
			}
            else if($categoryFlag){
				add_history('ReqCatgry',$postData);
			}
			else if($technicianFlag){
				add_history('ReqTechncian',$postData);
			}
            else if($subactegoryFlag){
				add_history('ReqSubCat',$postData);
			}
            else if($itemFlag){
				add_history('ReqItem',$postData);
			}
            else if($dueby_dateFlag){
				add_history('ReqDueDt',$postData);
			}
            else if($response_duebyFlag){
				add_history('ReqResDt',$postData);
			}			
		}			//savehistory($history_type="0",$postData["request_id"],$technician_id,$postData["requester_user_code"],$creator_email='',$msgSubject,$msg);
		
	  }
    }
	// user counter status list
	public function requestlist($request_code=''){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = get_request_data_by_code($request_code,$data);
		$this->load->view('helpdesk/report_user_request',$data);
	} 
	public function showHistory($user_request_id)
	{
		$data = $this->Helpdesk_model->get_ticket_history($user_request_id);
		return $data;
	}
	
/*	
//backend code start 
	public function admin_tech_assign_list(){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = $this->Helpdesk_model->get_assign_tech_list();
		$data['include'] = 'backend/report';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'adminrequestlist';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function admin_requestlist($request_code=''){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = get_request_data_by_code($request_code,$data);
		$data['include'] = 'backend/report_user_request';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlist';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function admin_new_request($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/new_request';
		$this->load->view('backend/masterpage',$data);
	}
	
	
	public function admin_dashboard(){
		$data = $this->get_ledap();
		$data['request_counter'] = get_request_count_by_email($data);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/homepage';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend/masterpage',$data);
	}
	
	
	public function admin_newrequest(){
		$data = $this->get_ledap();
		$data['request_counter'] = get_request_count_by_email($data);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/newrequest';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'newrequest';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function admin_requestdetail(){
		$data = $this->get_ledap();
        $id = $this->uri->segment(3);
        if(empty($id)){
            show_404();
        }
        $data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($id);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id); 
		$data['emails']  = $this->get_system_mails_for_request($id,$view);
		$data['action_tab'] = 'request';
        $data['include'] = 'backend/open_request_details';
		$this->load->view('backend/masterpage',$data);
    }
	
//backend code end
*/

	
//backend code start 
	public function admin_tech_assign_list(){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = $this->Helpdesk_model->get_assign_tech_list();
		$data['include'] = 'backend/report';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'adminrequestlist';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function admin_requestlist($request_code=''){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = get_request_data_by_code($request_code,$data);
		$data['include'] = 'backend/report_user_request';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlist';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['column_list'] = get_columns($u_code);
		$this->load->view('backend/masterpage',$data);
	}
	
	public function admin_new_request($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/new_request';
		$this->load->view('backend/masterpage',$data);
	}
	
	
	public function admin_dashboard(){
		$data = $this->get_ledap();
		$data['request_counter'] = get_request_count_by_email($data);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/homepage';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend/masterpage',$data);
	}
	
	
	public function admin_newrequest(){
		$data = $this->get_ledap();
		$data['request_counter'] = get_request_count_by_email($data);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/newrequest';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'newrequest';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function admin_requestdetail(){
		$data = $this->get_ledap();
        $id = $this->uri->segment(3);
        if(empty($id)){
            show_404();
        }
        $data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($id);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id); 
		//NEW*
		$data['resolution_data'] = is_resolution_exists($id);
		$data['tried_solutions'] = get_tried_solutions($id);
		
		$data['action_tab'] = 'request';
        $data['include'] = 'backend/technician/open_request_details';
		$this->load->view('backend/masterpage',$data);
    }
	
	//new solution page -- from resolution
	public function admin_ajxresolution(){
		$postData = $this->input->post();
		$success = false;
		$request_id = $postData['request_id'];
		$assignData = get_assign_request_data($request_id);
		$task_status = $assignData->task_status;
		              
		$resolution_ary = array(
			'user_request_id' => $request_id,
			'group_id' => $assignData->group_id,
			'technician_id' => $assignData->technician_id,
			'response_desc' => $postData['resolution_txt'],
			'resolution_status' => $postData['statuschange'],
			'resolution_created_dt' => date('Y-m-d H:i:s')
		);
		if(count(is_resolution_exists($request_id)) > 0){
			// history updates for resolution
			$resolution_id = get_resolution_id($request_id);
		    $postDataArray = array(
		        'resolution_id' =>$resolution_id ,
		        'request_id'  =>$request_id
		    );
	        add_history('ReqResolnUpdate',$postDataArray);
			$this->db->where("user_request_id",$request_id);
			$this->db->update("request_resolution_hd",$resolution_ary);
			$success = true;
			
		}else{
			$this->db->insert("request_resolution_hd",$resolution_ary);
			$success = true;
			// add history
			$postDataArray = array(
			   'resolution_id' =>$this->db->insert_id(),
			   'request_id'  =>$request_id
			);
	        add_history('ReqResolnAdd',$postDataArray);
		}
		
		//status change 
		$statusChangeAry = array(
			'comment' => '',
			'request_id' => $request_id,
			'status' => $postData['statuschange']
		);
		$this->Helpdesk_model->save_status_change_details1($statusChangeAry);
		
		/*if($postData['statuschange'] =='query_to_user'){
			$query_to_user_ary = array(
				'user_request_id'=>$request_id
			);
			$this->db->insert('clock_request_hd',$query_to_user_ary);
		}*/
		
		$this->tech_mail_to_resolution_change($request_id);
	    $this->user_mail_to_resolution_change($request_id);
		// redirection based on the admin or technician for now is_technician flag is set to true
		$is_technician = true;
		if($is_technician)
		{
			$redirect_url = base_url().'helpdesk/requestdetail/'.$request_id; 
		}
		else
		{
			$redirect_url = base_url().'helpdesk/admin_requestdetail/'.$request_id;
		}	
		
		if(isset($postData['addresolution'])){ 	//add only resolution editor text
			$redirectAry = array(
				'redirect_url'=> $redirect_url,
				'status'=>'success'
			);
		}else{ 									//add resolution and solution
			if($success){
				$redirectAry = array(
					'redirect_url'=> base_url().'helpdesk/admin_addresolution/'.$request_id,
					'status'=>'success'
				);
			}else{
				$redirectAry = array(
					'status'=>'failed'
				);
			}
		}
		echo json_encode($redirectAry);
	}
	
	public function admin_addresolution(){
		$request_id = $this->uri->segment(3);
		$data['request_details']  =  $this->Helpdesk_model->getRequestData($request_id);
		$data['include'] = 'backend/newsolution';
		$this->load->view('backend/masterpage',$data);
	}
//backend code end
	
//resolution code start 
	//get solution details 
	public function get_solution_details(){
		$postData = $this->input->post();
		$solutionData = $this->Helpdesk_model->get_solution_details($postData['solnId']);
		$emp_name = get_user_name_by_email($solutionData->creator_email);
		$item_data = get_record_by_id_code('item',$solutionData->item_id);
		$category_data = get_record_by_id_code('category',$item_data->category_id);
		
		$solution_ary = array(
			'creator_name'=>$emp_name,
			'solution_title'=>ucfirst($solutionData->solution_title),
			'solution_desc'=>$solutionData->solution_content,
			'category_name'=>$category_data->category_name,
			'solution_created'=>convert_std_time($solutionData->request_solution_created_dt)
		);
		echo json_encode($solution_ary);
	}
	public function add_resolution(){
		$data = $this->get_ledap();
		$postData = $this->input->post();
		$u_email = $data['user_ldamp']['mail'];
		$empData = getEmpByEmail($u_email);
		//save resolution 
		$solution_ary = array(
			'creator_id'=>$empData->u_id,
			'creator_email'=>$u_email,
			'request_id' => $postData['request_id'],
			'solution_title' => $postData['solution_title'],
			'solution_content' => $postData['solution_content'],
			'item_id' => $postData['solution_item'],
			'solution_keyword' => $postData['solution_keyword'],
			'solution_comment' => $postData['solution_comment'],
			'request_solution_created_dt' => date('Y-m-d H:i:s')
		);
		
		
		$this->db->insert("request_solutions_hd",$solution_ary);
		
		//status change 
		//add comment
		$addCommentAry = array(
			'comment' => $postData['solution_comment'],
			'request_id' => $postData['request_id']
		);
		
		$this->Helpdesk_model->save_status_change_details1($addCommentAry);
		redirect(base_url().'helpdesk/admin_requestdetail/'.$postData['request_id']);
	}
	
	public function save_comments(){
		//save comment
	}
	
	//add worklog -- technician
	public function add_worklog(){
		//header('WWW-Authenticate: NTLM');
		echo '<pre>';var_dump(get_current_user());
		var_dump( $_SERVER['AUTH_USER']);
		echo getenv("HOMEDRIVE") . getenv("HOMEPATH");
		$postData = $this->input->post();
		echo '<pre>';
		print_r($postData);
		
	}
    //resolution code end
    // controller to pass the ticket history
	function getAllHistory($id){
	    $history = $this->Helpdesk_model->get_request_history($id);
        return $history; 		
	
	
	}
	// close Request 
	function close_request_hd()
	{
		$postData = $this->input->post();
		//echo "<pre>";print_r($postData);exit();
		if(!empty($postData)){
			$data = $this->get_ledap();
			//echo "<pre>";print_r($data);exit();
			// save the user closure request details 
			if($this->Helpdesk_model->save_close_request_hd($data)){
				//if the details are saved notify with success msg
				//$this->tech_mail_to_resolution_change($postData["user_request_id"]);
               // $this->user_mail_to_resolution_change($postData["user_request_id"]);
				echo "success";exit();
				
			}
			else
			{
				//if the mandatory parameter are missing alert user with failure
				echo "failed";exit();
			}
			
				
		}
	    
	}
	
	public function testFun(){
		$username=shell_exec("echo %username%");
		echo ("username : $username" );
		echo exec('whoami').'<br>';
		var_dump($_SERVER['LOGON_USER']);
		print_r(getenv("username"));
		echo phpinfo();
	}
	public function save_columns()
	{
		$data = $this->get_ledap();
		//echo "<pre>";print_r($data);die;
		$u_code  = $data['user_ldamp']['employeeid'];
		// table ="request_columns_hd"
		//check the user code in table 
		$postData = $this->input->post('columns_list'); 
		if(!checkColumn($u_code)){
			// if user code is not found
			// save the columns for the user code
			save_columns("request_columns_hd",$postData,$data);
		}
		else{
			update_column("request_columns_hd",$postData,$data);
		}
		
		//echo $this->db->last_query();exit();
		echo "success";
        
	}
	
	//End User panel start --------------------------------------------------------------------
	/*public function new_request($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/new_request';
		$this->load->view('backend/masterpage',$data);
	}*/
	/*public function requestdetail(){
		$data = $this->get_ledap();
        $id = $this->uri->segment(3);
        if(empty($id)){
            show_404();
        }
        $data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($id);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id); 
		$time_spent = sla_time_spent($id);
		$user_req = getReqInfoByUserRequestId($id);
		$get_sla = get_item_sla_duration($user_req["item_id"]);
		$data["is_sla_violated"] = ($time_spent>$this->sla_duration_item($get_sla))?true:false;
	 	$data['time_elapsed_for_request'] = format_time($time_spent);
		$data['sla_duration'] = $get_sla;
		//NEW*
		$data['resolution_data'] = is_resolution_exists($id);
		$data['tried_solutions'] = get_tried_solutions($id);
		$data['emails']  = $this->get_system_mails_for_request($id,"viewall");
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/technician/open_request_details';
        //$data['include'] = 'backend/enduser/open_request_details';
		$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/masterpage',$data);
    }/
	/*public function requestlists($request_code=''){
		$data = $this->get_ledap();
		$data['assign_tech_list'] = get_request_data_by_code($request_code,$data);
		$data['include'] = 'backend/enduser/report_user_request';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlist';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['column_list'] = get_columns($u_code);
		$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/masterpage',$data);
	}*/
	/*public function dashboard(){
		$data = $this->get_ledap();
		$data['request_counter'] = get_request_count_by_email($data);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/enduser/homepage';
		$data['action_tab'] = 'dashboard';
		$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/masterpage',$data);
	}*/
	public function save_notes()
	{
		$postData = $this->input->post();
		$user_request_id = $postData["user_request_id"];
		$notes_description = $postData["notes"];
		$data = $this->get_ledap();
		$u_code  = $data['user_ldamp']['employeeid'];
		$data = array(
			"note_user_id"=>$u_code,
		    "user_request_id"   => $user_request_id,
            "notes_description"	=> $notes_description,
            "notes_is_active"	=> '1',
            "notes_added_date"	=> date('Y-m-d h:i:s')		
		);
		
		$this->db->insert("user_request_notes_hd",$data);
		/***logic to check if end user & technician code starts***/
		$user_req = getReqInfoByUserRequestId($user_request_id);
		
		/***logic to check if end user & technician code ends***/
		if($this->db->affected_rows()>0){
			if($user_req["requester_emp_code"] == $u_code){
				$tempData = array(
					"ticketId"  => $user_request_id,
					"note"      => $notes_description,
					"subject"   => $user_req["request_subject"]
				);
				$msg = $this->get_email_template('technician_add_note',$tempData);
				$email = $user_req["technician_Email"];
				$subject = "Requester has added a note - #".getRequestCode($user_request_id);
				$this->sendOutlookEmail($email,$subject,$msg);
			}
			else
			{	
				$tempData = array(
				 "eName"     => $user_req["requester_name"],
				 "ticketId"  => $user_request_id,
				 "note"      => $notes_description,
				 "subject"   => $user_req["request_subject"]
				);
			   $msg = $this->get_email_template('end_user_add_note',$tempData);
			   $email = $user_req["requester_email"];
			   $subject = "Technician has added a note - #".getRequestCode($user_request_id);
			   $this->sendOutlookEmail($email,$subject,$msg);
			}
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	public function save_attachments()
	{
		$postData = $this->input->post();
		//$this->upload_attachment_hd($postData['user_request_id']);
		
		//save attachments  start 
		$request_id = $postData['user_request_id'];
		$attac_dir = 'uploads/helpdesk/';
		$attac_dir .= $request_id;
		if (!file_exists($attac_dir)){
            mkdir($attac_dir, 0777, true);
			$path = 'uploads/helpdesk/'.$request_id.'/';
        }else{
			$path = 'uploads/helpdesk/'.$request_id.'/';
		}
		$file_name = '';
		$record_image = $_FILES['request_attach']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['request_attach']['name']);
			$file_attach = "hdrv" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['request_attach']['tmp_name'], $path . $file_attach);
			$file_name = $file_attach;
			$attachmentAry = array(
				'request_id'=>$request_id,
				'attachment_name'=>$file_name,
				'created_date'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("request_attach_hd",$attachmentAry);
		}
		//save attachments end
		
		if($this->db->affected_rows()>0)
		{
			//echo 'success';exit();
			$this->session->set_flashdata('msg','Attachment uploaded successfully');
			
		}
		else
		{
			$this->session->set_flashdata('error_msg','Error while uploading the attachment');
			
		}
		redirect("helpdesk/requestdetail/".$postData['user_request_id']);
	}
	public function print_preview($id)
	{
		$data = $this->get_ledap();
		$id = $this->uri->segment(3);
		$data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($id);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id); 
		
		//NEW*
		$data['resolution_data'] = is_resolution_exists($id);
		$data['tried_solutions'] = get_tried_solutions($id);
		$data['emails']  = $this->get_system_mails_for_request($id,"viewall");
		$data['include'] = 'backend/enduser/printpreview';
		//$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/masterpage',$data);
	}	
	/*public function newrequest(){
		$data = $this->get_ledap();
		$data['request_counter'] = get_request_count_by_email($data);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/enduser/newrequest';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'newrequest';
		$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/masterpage',$data);
	}*/
	//End User panel end --------------------------------------------------------------------
    //-- TECHNICIAN  PANEL------------------------------------------------
	/*public function tech_dashboard()
	{
		// $email is static for logged in technician as of now
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';
		$data['request_counter'] = get_technician_assign_request_count($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/technician/homepage';
		$this->checkURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/technician/masterpage',$data);
	}*/
	/*public function tech_newrequest()
	{
		// $email is static for logged in technician as of now
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';
		$data['request_counter'] = get_technician_assign_request_count($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/technician/newrequest';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'newrequest';
		$this->checkURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/technician/masterpage',$data);
	}*/
	/*public function tech_new_request($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/technician/new_request';
		$this->checkURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/technician/masterpage',$data);
	}*/
	/*public function request_tech_lists($request_code=''){
		$data = $this->get_ledap();
		// $email is static for logged in technician as of now
		$data['assign_tech_list'] = get_technician_assign_by_code($request_code,$data);
		$data['include'] = 'backend/technician/report';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlist';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['column_list'] = get_columns($u_code);
		$this->checkURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/technician/masterpage',$data);
	}
	*/
	/*public function tech_requestdetail(){
		$data = $this->get_ledap();
        $id = $this->uri->segment(3);
        if(empty($id)){
            show_404();
        }
        $data['request_details']  =  $this->Helpdesk_model->getRequestData($id);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($id);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id);
		$time_spent = sla_time_spent($id);
		$user_req = getReqInfoByUserRequestId($id);
		$get_sla = get_item_sla_duration($user_req["item_id"]);
		$data["is_sla_violated"] = ($time_spent>sla_duration_item($get_sla))?true:false;
	 	$data['time_elapsed_for_request'] = format_time($time_spent);
		$data['sla_duration'] = $get_sla;	
		
		
		//NEW*
		$data['resolution_data'] = is_resolution_exists($id);
		$data['tried_solutions'] = get_tried_solutions($id);
		$data['emails']  = $this->get_system_mails_for_request($id,"viewall");
		$data['action_tab'] = 'request';
        $data['include'] = 'backend/technician/open_request_details';
		$this->checkURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/technician/masterpage',$data);
    }
	*/
	
	public function checkURL($emp_id=0,$emp_code=0){
		if(check_technician($emp_id,$emp_code)){
			return true;
		}else{
			redirect('helpdesk/dashboard');
		}
	}
	public function requestURL($emp_id=0,$emp_code=0,$dept_id=0){
		if(!check_technician($emp_id,$emp_code)){
			redirect('helpdesk/new_request/'.$dept_id);
		}else{
			redirect('helpdesk/tech_new_request/'.$dept_id);
		}
	}
	
	public function dashboardURL($emp_id=0,$emp_code=0){
		if(!check_technician($emp_id,$emp_code)){
			return true;
		}else{
			redirect('helpdesk/tech_dashboard');
		}
	}
	// redirect url changed from tech_requestdetail to requestdetail
	public function tech_saveRequestDetails()
	{	
		$postData = $this->input->post();
		if($this->Helpdesk_model->update_request_details())
		{
			$request_ary = array(
				'request_id'=>$postData['request_id'],
				'group_id'=>$postData['group'],
				'technician_id'=>$postData['technician']
			);
			$this->reassign_technician($request_ary);
			$this->userRequestChange($postData);
			$this->session->set_flashdata('msg','Request saved with the changes successfully.');
			//redirect('helpdesk/tech_requestdetail/'.$this->input->post('request_id'));
			redirect('helpdesk/requestdetail/'.$this->input->post('request_id'));
		}
		else
		{
			$this->session->set_flashdata('error_msg','An internal error occured while saving changes.');
			//redirect('helpdesk/tech_requestdetail/'.$this->input->post('request_id')); 
			redirect('helpdesk/requestdetail/'.$this->input->post('request_id')); 
		}
	}
	public function tech_mail_to_resolution_change($user_request_id)
	{
		$data = $this->get_ledap();
		$user_req = getReqInfoByUserRequestId($user_request_id);
		$email = $data['user_ldamp']['mail'];
		$name  = $data['user_ldamp']['name'];
		$task_status = $user_req["task_status"];
		//echo $email;
	     //echo "hii";print_r($data);exit();
		$subject ="Resolution Changed for REQUEST ID: ".getRequestCode($user_request_id)." ";
		$msg ='<p><b>Dear '.$name.',</b></p>
			<p>Resolution changed to : '.strtoupper($task_status).'</p>
			<p> The subject of the request is : '.ucfirst($user_req["request_subject"]).' </p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>
		';
		$this->sendOutlookEmail($email,$subject,$msg);
		return;
	}
	public function user_mail_to_resolution_change($user_request_id)
	{	
		$user_req = getReqInfoByUserRequestId($user_request_id);
		$email = $user_req["requester_email"];
		$user_name = $user_req["requester_name"];
		$task_status = $user_req["task_status"];
		if($user_req["isCompleted"] == 1){
			$subject ="Request is resolved ID: ".getRequestCode($user_request_id)." ";
			$msg ='<p><b>Dear '.$user_name.',</b></p>
			<p>Your request has been resolved.</p>
			<p> The subject of the request is : '.ucfirst($user_req["request_subject"]).' </p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>';
		}else{
			$subject ="Resolution Changed for ID: ".getRequestCode($user_request_id)." ";
			$msg ='<p><b>Dear '.$user_name.',</b></p>
			<p>Resolution changed to  :'.strtoupper($task_status).'</p>
			<p> The subject of the request is : '.ucfirst($user_req["request_subject"]).' </p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>';
		}
		$this->sendOutlookEmail($email,$subject,$msg);
		return;
	}
	//-- TECHNICIAN PANEL ENDS-------------------------------------------
	
	/****************************common function for user and technician***************************************/
	public function dashboard()
    {
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';
		$data['departments'] = $this->Helpdesk_model->get_departments();
		//echo $this->db->last_query(); die;
		$data['assign_tech_list'] =(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']))? get_request_data_by_code($request_code,$data):get_technician_assign_by_code('',$data);
		$data['request_counter']=(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']))? get_request_count_by_email($data):get_technician_assign_request_count($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		
		$data['is_technician'] = (!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid'])) ? '0':'1';
		
		$data['my_request'] = count(myrequest($data['user_ldamp']['employeeid']));
		
		$data['include'] = 'backend/technician/homepage';
		$this->load->view('backend/masterpage',$data);
    }
	public function dashboard_ch()
    {
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';
		$data['action_tab_sub'] = 'dashboard_ch';
		$data['departments'] = $this->Helpdesk_model->get_departments(); 
		$data['ERP_count'] = get_request_allERP('ERP',5);
		//$data['request_counter']=get_request_count_all();
		$data['include'] = 'backend/technician/homepage_changes';
		$this->load->view('backend/masterpage',$data);
    }
	public function requestlists($request_code=''){
		$data = $this->get_ledap();
		if($request_code == 'my_request'){
			$data['assign_tech_list'] = myrequest($data['user_ldamp']['employeeid']);
		}else{
			$data['assign_tech_list'] =(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']))? get_request_data_by_code($request_code,$data): get_technician_assign_by_code($request_code,$data);
		}
		
		//$data['assign_tech_list'] =(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']))? get_request_data_by_code($request_code,$data):get_technician_assign_by_code($request_code,$data);
		
		//$data['assign_tech_list'] =  get_request_data_by_code($request_code,$data);
		//$data['include'] = 'backend/enduser/report_user_request';
		$data['include'] = 'backend/technician/report';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlist';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['column_list'] = get_columns($u_code);
		//$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		if (!$this->input->is_ajax_request()) {
			$this->load->view('backend/masterpage',$data);
		}
		else
		{
			$this->load->view('backend/technician/report',$data);
		}
		
	}
	public function requestlists_ajax($dept_id,$request_code=''){
		$data = $this->get_ledap();
		if($request_code == 'ERP'){
			$data['assign_tech_list'] =get_request_allERP($request_code,$dept_id);
		}else{
			$data['assign_tech_list'] =get_request_all($request_code,$dept_id);
		}
		//echo "<pre>";print_r($data);exit();
		//$data['assign_tech_list'] =  get_request_data_by_code($request_code,$data);
		//$data['include'] = 'backend/enduser/report_user_request';
		$data['include'] = 'backend/technician/report_ajax';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlist';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['column_list'] = get_columns($u_code);
		//$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		if (!$this->input->is_ajax_request()) {
			$this->load->view('backend/masterpage',$data);
		}
		else
		{
			$this->load->view('backend/technician/report_ajax',$data);
		}
		
	}
	
	public function getallrequest(){
		$data = $this->get_ledap();
		//$data['assign_tech_list'] =(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid'])) ? get_request_data_by_code($request_code,$data):get_technician_assign_by_code($request_code,$data);
		
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['u_code'] = $u_code;
		$data['assign_tech_list'] = get_all_requestAdmin();
		$data['include'] = 'backend/technician/report';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'requestlistall';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['column_list'] = get_columns($u_code);
		$this->load->view('backend/masterpage',$data);
	}
	
	
	public function escalatedlists(){
		$data = $this->get_ledap();
		/*$qry = $this->db->query("SELECT *,ur.user_request_id AS ur_user_request_id FROM user_request_hd ur 
LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) 
LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) 
LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) 
WHERE ra.request_assign_person_is_active='1'
AND ra.task_status IN ('open','WIP','hold')
AND ra.is_escalated='1' AND ur.request_is_active='1'
ORDER BY ur.user_request_id DESC");*/
		$qry = $this->db->query("SELECT *,ur.user_request_id AS ur_user_request_id FROM user_request_hd ur 
LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) 
LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) 
LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) 
WHERE ra.request_assign_person_is_active='1' AND ur.request_is_active='1'
AND ra.task_status IN ('open','WIP','hold','resolve')
AND ur.task_escalated = '1' GROUP BY ur.user_request_id
ORDER BY ur.user_request_id DESC");

		$data['assign_tech_list'] = $qry->result();
		$data['include'] = 'backend/technician/report_escalate';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'escalatedlists';
		$u_code  = $data['user_ldamp']['employeeid'];
		$data['u_code'] = $u_code;
		$data['column_list'] = get_columns($u_code);
		$this->load->view('backend/masterpage',$data);
	}
	public function requestdetail()
	{
		$data = $this->get_ledap();
        $requestId = $this->uri->segment(3);
		$user_req = getReqInfoByUserRequestId($requestId);
		$technician_id = $user_req['technician_id'];
        if(empty($requestId)||empty($user_req)){
            show_404();
        }
		
		if(check_ticket_view($data['user_ldamp']['employeeid'],$data['user_ldamp']["mail"],$requestId)){	
		}else{
			echo 'Access denied'; die;
		}
		
		// this condition checks if logged in user is technician
		if(check_is_technician($requestId)){  // this function checks if logged in technician is assigned the ticket
			//is_request_assign_technician($requestId,$data['user_ldamp']["mail"]);
		}
        $data['request_details']  =  $this->Helpdesk_model->getRequestData($requestId);
        $data['request_more_details'] = $this->Helpdesk_model->get_user_request_details($requestId);
        $data['site_location_hd'] = $this->Helpdesk_model->get_site_location();
        $data['groups_hd'] = get_groups();
        $data['depts_hd'] = $this->Helpdesk_model->get_departments();
        $data['request_mode'] = $this->Helpdesk_model->get_request_mode();
        $data['technicians_hd'] = getTechnicianByGroupId($data['request_more_details']->group_id); 
		$time_spent = sla_time_spent($requestId,$technician_id);
		$get_sla = get_item_sla_duration($user_req["item_id"]);
		$data["is_sla_violated"] = ($time_spent>sla_duration_item($get_sla))?true:false;
	 	$data['time_elapsed_for_request'] = format_time($time_spent);
		$data['sla_duration'] = $get_sla;
		//NEW*
		$data['resolution_data'] = is_resolution_exists($requestId);
		$data['tried_solutions'] = get_tried_solutions($requestId);
		$data['emails']  = $this->get_system_mails_for_request($requestId,"viewall");
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/technician/open_request_details';
        //$data['include'] = 'backend/enduser/open_request_details';
		//$this->dashboardURL($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$this->load->view('backend/masterpage',$data);
    }
	public function newrequest(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';	$data['request_counter']=(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']))?get_request_count_by_email($data):get_technician_assign_request_count($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/technician/newrequest';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'newrequest';
		$this->load->view('backend/masterpage',$data);
	}
	public function new_request($dept_id=0)
	{
		$data = $this->get_ledap();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/new_request';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function get_email_template($template_code,$tempData){
		switch($template_code){ //<p> The status of the request can be tracked at http://helpdesk.skeiron.com.</p>
			case 'end_user_reassign':
			$msg = '<p><b>Dear '.$tempData['eName'].',</b></p>
			<p>This is an acknowledgement mail for your request.<br/> Your request with id #'.getRequestCode($tempData['ticketId']).' has been re-assigned to '.$tempData['techName'].'.<br/> <p>The subject of the request is : '.$tempData['subject'].' </p><br/> 
			<br><br><br><br><p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>';
			break;
			
			case 'technician_reassign':
				$msg = 'Request details are :
					<br/>Requested by :'.$tempData['requesterName'].'
					<br/>Request Id : #'.getRequestCode($tempData['ticketId']).'
					<br/>Department : '.$tempData['deptName'].'
					<br/>Item : '.$tempData['itemName'].'
					<br/>Title : '.$tempData['requestTitle'].' ';
			break;
			//<p>The status of the request can be tracked at http://helpdesk.skeiron.com.</p>
			case 'end_user_add_note':
			$msg = '<p><b>Dear '.$tempData['eName'].',</b></p>
			<p>Request id: #'.getRequestCode($tempData['ticketId']).' <br/><p>The subject of the request is : '.$tempData['subject'].'  </p><br/><p>Technician Note: '.$tempData['note'].'.</p><br/>
			<br><br><br><br><p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>';
			break;
			//<p> The status of the request can be tracked at http://helpdesk.skeiron.com.</p>
			case 'technician_add_note':
			$msg = '<p>Request id: #'.getRequestCode($tempData['ticketId']).' <br/><p> The subject of the request is : '.$tempData['subject'].' </p><br/><p> User Note: '.$tempData['note'].'.</p><br/>
			<br><br><br><br><p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>';
			break;
		}
		return $msg;
	}
	public function test()
	{
		$user_req = getReqInfoByUserRequestId(12);
		echo "<pre>";print_r($user_req);exit();
	}
	
	
	public function new_self_request(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';	$data['request_counter']=(!check_technician($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']))?get_request_count_by_email($data):get_technician_assign_request_count($data['user_ldamp']['mail'],$data['user_ldamp']['employeeid']);
		$data['departments'] = $this->Helpdesk_model->get_departments();
		$data['include'] = 'backend/technician/newrequest_self';
		$data['action_tab'] = 'request';
		$data['action_tab_sub'] = 'newrequest_self';
		$this->load->view('backend/masterpage',$data);
	}
	
	// to create a new request by technician
	public function request_new_self($dept_id=0)
	{
		$data = $this->get_ledap();
		//echo "<pre>";print_r($data);exit();
		if($dept_id >0){
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd($dept_id);
		}
		else{
			$data['categories_hd'] = $this->Helpdesk_model->get_categories_hd();
		}
		$data['sitelocation'] = $this->Helpdesk_model->get_site_location();
		$data['dept_id'] = $dept_id;
		$data['groups_hd'] = get_groups();
		$data['action_tab'] = 'request';
		$data['include'] = 'backend/new_request _self';
		$data['all_technicians'] = get_all_record_by_code('technician');
		//echo "<pre>";print_r($data);exit();
		$this->load->view('backend/masterpage',$data);
	}
	
	// to save the new request raised by technician
	public function save_request_self()
	{	
	    
		$postData = $this->input->post();
		$ldapData = $this->getldapDataByEmail($postData["behalf_of"]);
		//echo "<pre>";print_r($ldapData);die;
		if(!empty($postData)){
			$data = $this->get_ledap();
			if($this->Helpdesk_model->save_user_request_self()){
				$requestId = $this->db->insert_id();
				
				if(!empty($_FILES['request_attach']["name"])){
					
					$fileUpload = $this->upload_attachment_hd($requestId);
					
					
					$tech_id = $postData["technician"]; // get the technician id and assign him to task
					$item_id = $postData["item"];
					$this->Helpdesk_model->assign_request($tech_id,$requestId,$item_id);
					
					//send email to customer
					//$this->sendEmailTemple($requestId,1,0,'','',0);
					
					//$this->sendEmailTemple($requestId,2,$tech_id,'','',0);
					
					if($postData["request_type"] == 'Other'){
						
						if($ldapData !=0){
							if(count($ldapData) > 0){
								//$this->sendEmailTemple($requestId,1,0,$ldapData['mail'],$ldapData['displayname']);
								$this->sendEmailTemple($requestId,2,$tech_id,$ldapData['mail'],$ldapData['displayname']);
								
								//sent email to technician
								//$this->sendEmailTemple($requestRecord->user_request_id,1,0,$tech_record->technician_email,$tech_record->tech_name);
								//$this->sendEmailTemple($requestRecord->user_request_id,2,$tech_record->tech_id,$tech_record->technician_email,$tech_record->tech_name);
							}
						}
						else{
							// add history 
							$postData = array(
							"request_id"=>$requestId
							);
							add_history('ReqCreated',$postData);
						}
					}
					
				}
				
				$this->session->set_flashdata('msg','A ticket has been raised against this request.');
				redirect('helpdesk/request_new_self/'.$this->input->post('dept_id'));
			}else{
				$this->session->set_flashdata('error_msg','An error occured while Saving...');
				
				redirect('helpdesk/request_new_self/'.$this->input->post('dept_id'));
			}
			;exit();
		}
	}
	/**********************************************************************************************************/
	
	// system report code start 
	public function report(){
		$data = $this->get_ledap();
		$qry_2 = $this->db->query("SELECT * FROM technician_hd WHERE technician_is_active = '1'");
		$tech_data = $qry_2->result();
		$data['technician_list'] = $tech_data;
		$data['include'] = 'backend/system_report';
		$this->load->view('backend/masterpage',$data);
	}
	
	public function get_report(){
		$postData = $this->input->post();
		$reportCode = $postData['request_type'];
		switch($reportCode){
			case 'day_ajax':
				$data = $this->get_ledap();
				$data['assign_tech_list'] = get_request_by_date(date('Y-m-d',strtotime($postData['data_date'])));
				
				$data['include'] = 'backend/report_ajax_sys';
				$u_code  = $data['user_ldamp']['employeeid'];
				$data['column_list'] = get_columns($u_code);
				if (!$this->input->is_ajax_request()) {
					$this->load->view('backend/masterpage',$data);
				} else {
					$this->load->view('backend/\report_ajax_sys',$data);
				}
			break;
		}
	}
	// system report code end
	
}