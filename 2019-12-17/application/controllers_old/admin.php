<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller {
	public function __construct(){
		
		parent::__construct();
		//$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->is_admin = is_Admin($this->user_ldamp["employeeid"]);
		//if(!$this->is_admin){
		//	redirect("profile");
		//}
		$lognSql = "select * from users where u_ldap_userid='" . $this->user_ldamp['samaccountname'] . "' ";
        $this->user_indo = $this->Base_model->run_query($lognSql);
		$this->load->model('Admin_model'); 
		$this->load->model('Dailytask_model'); 
		
		//$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        //$mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        //$this->hrdata_user_info = $mssql_user_info_query->result_array();
		include APPPATH . 'third_party/class.phpmailer.php';
	}
	
	public function index(){
		
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';
		$data['include'] = 'backend_iconnect/homepage';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function admin_dashboard(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/homepage';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
//iconnect code start 
	//announcement start ----------------------------------------------------------------------
	public function manage_announcements(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['announcements_data'] = $this->Admin_model->get_all_announcements();
		$data['include'] = 'backend_iconnect/announcements';
		$data['action_tab'] = 'manage_announcements';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page',$data);
	}
	
	public function add_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/announcements_page',$data);
	}
	public function view_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page',$data);
	}
	
	public function status_announcements(){
		$postData = $this->input->post();
		$data = $this->get_ledap();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'an_status'=>0
			);
			
			$this->track_user_activity($postData['recd_id'], 'annuncement_deactivated');
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'an_status'=>1
			);
			
			$this->track_user_activity($postData['recd_id'], 'annuncement_activated');
		}
		$this->db->where("an_id",$postData['recd_id']);
		$this->db->update("announcements",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_announcement(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$update_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->where("an_id",$postData['anno_id']);
			$this->db->update("announcements",$update_ary);
			
			$this->track_user_activity($postData['anno_id'], 'annuncement_updated');
			
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_announcements");
	}
	
	public function added_announcement(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$insert_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->insert("announcements",$insert_ary);
			
			//add user details - track who added mailer
			$annoucements_id =  $this->db->insert_id();
			$this->track_user_activity($annoucements_id, 'announcement_uploaded');
			
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_announcements");
	}
	// to remove the annoucements
	public function remove_annoucement(){
		   $postData = $this->input->post();
		   $data =  $this->Admin_model->get_announcements_by_id($postData['recd_id']);
		  
		   if(count($data) > 0){
			//echo "<pre>";print_r($data);
			$ann_file = $data->an_file;
			//echo $ann_file;die;
			$path = 'uploads/announcement/'.$ann_file;
			if(isset($path)){
				$delete_ary = array(
					'is_deleted'=>1,
					'an_status'=>0
				);
				$this->db->where("an_id",$postData['recd_id']);
		        $this->db->update("announcements",$delete_ary);
				$this->track_user_activity($postData['recd_id'], 'announcement_deleted');
			    $this->session->set_flashdata('message_submit', 'Record removed successfully');
			    $status = "success";
			}
			else
			{
				$status = "failed";
			}
		}
		else{
			$status = "failed";
		}
		echo $status;die;
	}
	
	function track_user_activity($primary_id=0, $action_taken=''){
		$data = $this->get_ledap();
		$userTrack = array(
			'announcement_id'=>$primary_id,
			'emp_code'=>$data['user_ldamp']['employeeid'],
			'action_taken'=>$action_taken,
			'action_date'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("user_activity_track",$userTrack);
		return true;
	}
	//announcement end -----------------------------------------------------------------------------------------
	
	
//TV Mailer start 
	//announcement TV start ----------------------------------------------------------------------
	public function manage_announcements_tv(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['announcements_data'] = $this->Admin_model->get_all_announcements_tv();
		$data['include'] = 'backend_iconnect/announcements_tv';
		$data['action_tab'] = 'announcements_tv';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_announcements_tv(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id_tv($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page_tv',$data);
	}
	
	public function add_announcements_tv(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/announcements_page_tv',$data);
	}
	public function view_announcements_tv(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id_tv($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page_tv',$data);
	}
	
	public function status_announcements_tv(){
		$postData = $this->input->post();
		$data = $this->get_ledap();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'an_status'=>0
			);
			$this->track_user_activity_tv($postData['recd_id'], 'annuncement_tv_deactivated');
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'an_status'=>1
			);
			
			$this->track_user_activity_tv($postData['recd_id'], 'annuncement_tv_activated');
		}
		$this->db->where("an_id",$postData['recd_id']);
		$this->db->update("announcements_tv",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_announcement_tv(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement_tv/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$update_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->where("an_id",$postData['anno_id']);
			$this->db->update("announcements_tv",$update_ary);
			
			$this->track_user_activity_tv($postData['anno_id'], 'annuncement_tv_updated');
			
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_announcements_tv");
	}
	
	public function added_announcement_tv(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement_tv/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$insert_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->insert("announcements_tv",$insert_ary);
			
			//add user details - track who added mailer
			$annoucements_id =  $this->db->insert_id();
			$this->track_user_activity_tv($annoucements_id, 'announcement_tv_uploaded');
			
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_announcements_tv");
	}
	// to remove the annoucements
	public function remove_annoucement_tv(){
		   $postData = $this->input->post();
		   $data =  $this->Admin_model->get_announcements_by_id_tv($postData['recd_id']);
		  
		   if(count($data) > 0){
			//echo "<pre>";print_r($data);
			$ann_file = $data->an_file;
			//echo $ann_file;die;
			$path = 'uploads/announcement_tv/'.$ann_file;
			if(isset($path)){
				$delete_ary = array(
					'is_deleted'=>1,
					'an_status'=>0
				);
				$this->db->where("an_id",$postData['recd_id']);
		        $this->db->update("announcements_tv",$delete_ary);
				$this->track_user_activity($postData['recd_id'], 'announcement_tv_deleted');
			    $this->session->set_flashdata('message_submit', 'Record removed successfully');
			    $status = "success";
			}
			else
			{
				$status = "failed";
			}
		}
		else{
			$status = "failed";
		}
		echo $status;die;
	}
	
	function track_user_activity_tv($primary_id=0, $action_taken=''){
		$data = $this->get_ledap();
		$userTrack = array(
			'announcement_id'=>$primary_id,
			'emp_code'=>$data['user_ldamp']['employeeid'],
			'action_taken'=>$action_taken,
			'action_date'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("user_activity_track",$userTrack);
		return true;
	}
	//announcement TV end -----------------------------------------------------------------------------------------
	
	
	//thought of the day start --------------------------------------------------------------------------------
	public function manage_thought(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['thought_data'] = $this->Admin_model->get_all_thought();
		$data['include'] = 'backend_iconnect/thought_day';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_thought(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['thought_data'] = $this->Admin_model->get_thought_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/thought_day_page',$data);
	}
	
	public function add_thought(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/thought_day_page',$data);
	}
	public function view_thought(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['thought_data'] = $this->Admin_model->get_thought_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/thought_day_page',$data);
	}
	
	public function status_thought(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'thought_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'thought_is_active'=>1
			);
		}
		$this->db->where("thought_id",$postData['recd_id']);
		$this->db->update("iconnect_thought",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_thought(){
		$postData = $this->input->post();
		if($postData['thought_txt'] !=''){
			$updateAry = array(
				'thought_text'=>$postData['thought_txt']
			);
			$this->db->where("thought_id",$postData['thog_id']);
			$this->db->update("iconnect_thought",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_thought");
	}
	
	public function added_thought(){
		$postData = $this->input->post();
		if($postData['thought_txt'] !=''){
			$insertAry = array(
				'thought_text'=>$postData['thought_txt'],
				'thought_created'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("iconnect_thought",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_thought");
	}
	
	//thought of the day end 
	
	//knowledge base document start ------------------------------------------------------------------
	public function manage_archivefiles($archive_id = 0){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['archivefiles_data'] = $this->Admin_model->get_all_archivefiles($archive_id);
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($archive_id);
		$data['include'] = 'backend_iconnect/knowledgedocuments'; //
		$data['action_tab'] = 'archivefiles';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_archivefiles(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['archive_id']);
		$data['archivefiles_data'] = $this->Admin_model->get_archivefiles_by_id($postData['recd_id'],$postData['archive_id']);
		$this->load->view('backend_iconnect/knowledgedocuments_page',$data);
	}
	
	public function add_archivefiles(){
		$postData = $this->input->post();
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['archive_id']);
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/knowledgedocuments_page',$data);
	}
	public function view_archivefiles(){
		$postData = $this->input->post();
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['archive_id']);
		$data['action'] = 'view';
		$data['archivefiles_data'] = $this->Admin_model->get_archivefiles_by_id($postData['recd_id'],$postData['archive_id']);
		$this->load->view('backend_iconnect/knowledgedocuments_page',$data);
	}
	
	public function status_archivefiles(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'doc_file_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'doc_file_is_active'=>1
			);
		}
		$this->db->where("doc_file_id",$postData['recd_id']);
		$this->db->update("doc_files",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_archivefiles(){
		$postData = $this->input->post();
		$archivefiles_dir = 'uploads/archivefiles/';
		$file_name = '';
		$record_image = $_FILES['archivefiles_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['archivefiles_file']['name']);
			$file_archivefiles = "archivefile_" .$postData['archivefiles_name']. '_' .$ext[0]. "." . $ext[1];
			move_uploaded_file($_FILES['archivefiles_file']['tmp_name'], $archivefiles_dir . $file_archivefiles);
			$file_name = $file_archivefiles;
			
			$update_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_location'=>$file_name,
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where("doc_file_id",$postData['doc_file_id']);
			$this->db->update("doc_files",$update_ary);
		}else{
			$update_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where("doc_file_id",$postData['doc_file_id']);
			$this->db->update("doc_files",$update_ary);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_archivefiles/".$postData['archive_id']);
	}
	
	public function added_archivefiles(){
		$postData = $this->input->post();
		$archivefiles_dir = 'uploads/archivefiles/';
		$file_name = '';
		$record_image = $_FILES['archivefiles_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['archivefiles_file']['name']);
			//$file_archivefiles = "archivefiles_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			$file_archivefiles = "archivefile_" .$postData['archivefiles_name']. '_' .$ext[0]. "." . $ext[1];
			move_uploaded_file($_FILES['archivefiles_file']['tmp_name'], $archivefiles_dir . $file_archivefiles);
			$file_name = $file_archivefiles;
			
			$insert_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_location'=>$file_name,
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("doc_files",$insert_ary);
		}else{
			$insert_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("doc_files",$insert_ary);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_archivefiles/".$postData['archive_id']);
	}
	//knowledge base document end
	
	//knowledge archive start --------------------------------------------------------------------
	public function manage_archive(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['archive_data'] = $this->Admin_model->get_all_archive();
		$data['include'] = 'backend_iconnect/archive';
		$data['action_tab'] = 'archive';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_archive(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/archive_page',$data);
	}
	
	public function add_archive(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/archive_page',$data);
	}
	public function view_archive(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/archive_page',$data);
	}
	
	public function status_archive(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'doc_folder_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'doc_folder_is_active'=>1
			);
		}
		$this->db->where("doc_folder_id",$postData['recd_id']);
		$this->db->update("doc_folder",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_archive(){
		$postData = $this->input->post();
		if($postData['archive_txt'] !=''){
			$updateAry = array(
				'doc_folder_txt'=>$postData['archive_txt'],
				'doc_folder_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where("doc_folder_id",$postData['archive_id']);
			$this->db->update("doc_folder",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_archive");
	}
	
	public function added_archive(){
		$postData = $this->input->post();
		if($postData['archive_txt'] !=''){
			$insertAry = array(
				'doc_folder_txt'=>$postData['archive_txt'],
				'doc_folder_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("doc_folder",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_archive");
	}
	//knowledge archive end
	
//iconnect code end
	

//helpdesk code start 
	
	//guideline start --------------------------------------------------------------------------------
	public function manage_guideline(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['guideline_data'] = $this->Admin_model->get_all_guideline();
		$data['include'] = 'backend_iconnect/guideline_day';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_guideline(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['guideline_data'] = $this->Admin_model->get_guideline_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/guideline_page',$data);
	}
	
	public function add_guideline(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/guideline_page',$data);
	}
	public function view_guideline(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['guideline_data'] = $this->Admin_model->get_guideline_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/guideline_page',$data);
	}
	
	public function status_guideline(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'guideline_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'guideline_is_active'=>1
			);
		}
		$this->db->where("dept_guideline_hd_id",$postData['recd_id']);
		$this->db->update("dept_guideline_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_guideline(){
		$postData = $this->input->post();
		if($postData['guideline_txt'] !=''){
			$updateAry = array(
				'guideline_txt'=>$postData['guideline_txt']
			);
			$this->db->where("dept_guideline_hd_id",$postData['guid_id']);
			$this->db->update("dept_guideline_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_guideline");
	}
	
	public function added_guideline(){
		$postData = $this->input->post();
		if($postData['guideline_txt'] !=''){
			$insertAry = array(
				'guideline_txt'=>$postData['guideline_txt']
			);
			$this->db->insert("dept_guideline_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_guideline");
	}
	//guideline end 
	
	//dept manage start  -------------------------------------------------------------
	public function manage_department(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['department_data'] = $this->Admin_model->get_all_department();
		$data['include'] = 'backend_iconnect/department';
		$data['action_tab'] = 'department';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_department(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['department_data'] = $this->Admin_model->get_department_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/department_page',$data);
	}
	
	public function add_department(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/department_page',$data);
	}
	
	public function view_department(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['department_data'] = $this->Admin_model->get_department_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/department_page',$data);
	}
	
	public function status_department(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'dept_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'dept_is_active'=>1
			);
		}
		$this->db->where("dept_id",$postData['recd_id']);
		$this->db->update("dept_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_department(){
		$postData = $this->input->post();
		if($postData['department_txt'] !=''){
			$updateAry = array(
				'dept_name'=>$postData['department_txt']
			);
			$this->db->where("dept_id",$postData['dept_id']);
			$this->db->update("dept_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_department");
	}
	
	public function added_department(){
		$postData = $this->input->post();
		if($postData['department_txt'] !=''){
			$insertAry = array(
				'dept_name'=>$postData['department_txt']
			);
			$this->db->insert("dept_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_department");
	}
	//dept manage end
	
	//category manage start -----------------------------------------------------------
	public function manage_category(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['category_data'] = $this->Admin_model->get_all_category();
		$data['include'] = 'backend_iconnect/category';
		$data['action_tab'] = 'category';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_category(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['category_data'] = $this->Admin_model->get_category_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/category_page',$data);
	}
	
	public function add_category(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/category_page',$data);
	}
	
	public function view_category(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['category_data'] = $this->Admin_model->get_category_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/category_page',$data);
	}
	
	public function status_category(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'category_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'category_is_active'=>1
			);
		}
		$this->db->where("category_id",$postData['recd_id']);
		$this->db->update("category_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_category(){
		$postData = $this->input->post();
		if($postData['category_txt'] !=''){
			$updateAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_name'=>$postData['category_txt']
			);
			$this->db->where("category_id",$postData['cat_id']);
			$this->db->update("category_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_category");
	}
	
	public function added_category(){
		$postData = $this->input->post();
		if($postData['category_txt'] !=''){
			$insertAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_name'=>$postData['category_txt']
			);
			$this->db->insert("category_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_category");
	}
	//category end
	
	//sub category manage start -------------------------------------------------------
	public function manage_subcategory(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['subcategory_data'] = $this->Admin_model->get_all_subcategory();
		$data['include'] = 'backend_iconnect/subcategory';
		$data['action_tab'] = 'subcategory';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_subcategory(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['subcategory_data'] = $this->Admin_model->get_subcategory_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/subcategory_page',$data);
	}
	
	public function add_subcategory(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/subcategory_page',$data);
	}
	
	public function view_subcategory(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['subcategory_data'] = $this->Admin_model->get_subcategory_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/subcategory_page',$data);
	}
	
	public function status_subcategory(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'sub_category_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'sub_category_is_active'=>1
			);
		}
		$this->db->where("sub_category_id",$postData['recd_id']);
		$this->db->update("sub_category_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_subcategory(){
		$postData = $this->input->post();
		if($postData['subcategory_txt'] !=''){
			$updateAry = array(
				'category_id'=>$postData['cat_id'],
				'sub_category_name'=>$postData['subcategory_txt']
			);
			$this->db->where("sub_category_id",$postData['subcat_id']);
			$this->db->update("sub_category_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_subcategory");
	}
	
	public function added_subcategory(){
		$postData = $this->input->post();
		if($postData['subcategory_txt'] !=''){
			$insertAry = array(
				'category_id'=>$postData['cat_id'],
				'sub_category_name'=>$postData['subcategory_txt']
			);
			$this->db->insert("sub_category_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_subcategory");
	}
	//sub category manage end
	
	//items manage start --------------------------------------------------------------
	public function manage_item(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['item_data'] = $this->Admin_model->get_all_item();
		$data['include'] = 'backend_iconnect/item';
		$data['action_tab'] = 'item';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_item(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['item_data'] = $this->Admin_model->get_item_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/item_page',$data);
	}
	
	public function add_item(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/item_page',$data);
	}
	
	public function view_item(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['item_data'] = $this->Admin_model->get_item_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/item_page',$data);
	}
	
	public function status_item(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'item_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'item_is_active'=>1
			);
		}
		$this->db->where("items_id",$postData['recd_id']);
		$this->db->update("items_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_item(){
		$postData = $this->input->post();
		if($postData['item_txt'] !=''){
			$updateAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_id'=>$postData['cat_id'],
				'sub_category_id'=>$postData['subcat_id'],
				'item_name'=>$postData['item_txt']
			);
			$this->db->where("items_id",$postData['item_id']);
			$this->db->update("items_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_item");
	}
	
	public function added_item(){
		$postData = $this->input->post();
		if($postData['item_txt'] !=''){
			$insertAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_id'=>$postData['cat_id'],
				'sub_category_id'=>$postData['subcat_id'],
				'item_name'=>$postData['item_txt']
			);
			$this->db->insert("items_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_item");
	}
	//item manage end
	
	
	//quick link start --------------------------------------------------------------
	public function manage_quicklink(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['quicklink_data'] = $this->Admin_model->get_all_quicklink();
		$data['include'] = 'backend_iconnect/quicklink';
		$data['action_tab'] = 'quicklink';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_quicklink(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['quicklink_data'] = $this->Admin_model->get_quicklink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/quicklink_page',$data);
	}
	
	public function add_quicklink(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/quicklink_page',$data);
	}
	
	public function view_quicklink(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['quicklink_data'] = $this->Admin_model->get_quicklink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/quicklink_page',$data);
	}
	
	public function status_quicklink(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'quick_line_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'quick_line_is_active'=>1
			);
		}
		$this->db->where("quick_link_id",$postData['recd_id']);
		$this->db->update("quick_link_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_quicklink(){
		$postData = $this->input->post();
		if($postData['quicklink_name'] !=''){
			$updateAry = array(
				'quick_link_name'=>$postData['quicklink_name'],
				'quick_link_url'=>$postData['quicklink_url'],
				'quick_link_desc'=>$postData['quicklink_desc']
			);
			$this->db->where("quick_link_id",$postData['quicklink_id']);
			$this->db->update("quick_link_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_quicklink");
	}
	
	public function added_quicklink(){
		$postData = $this->input->post();
		if($postData['quicklink_name'] !=''){
			$insertAry = array(
				'quick_link_name'=>$postData['quicklink_name'],
				'quick_link_url'=>$postData['quicklink_url'],
				'quick_link_desc'=>$postData['quicklink_desc']
			);
			$this->db->insert("quick_link_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_quicklink");
	}
	//quick link end
	
	
	//tech groups start --------------------------------------------------------------
	public function manage_group(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['group_data'] = $this->Admin_model->get_all_group();
		$data['include'] = 'backend_iconnect/groups';
		$data['action_tab'] = 'groups';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_group(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['group_data'] = $this->Admin_model->get_group_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/group_page',$data);
	}
	
	public function add_group(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/group_page',$data);
	}
	
	public function view_group(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['group_data'] = $this->Admin_model->get_group_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/group_page',$data);
	}
	
	public function status_group(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'group_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'group_is_active'=>1
			);
		}
		$this->db->where("group_id",$postData['recd_id']);
		$this->db->update("groups_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_group(){
		$postData = $this->input->post();
		if($postData['group_txt'] !=''){
			$updateAry = array(
				'group_name'=>$postData['group_txt']
			);
			$this->db->where("group_id",$postData['group_id']);
			$this->db->update("groups_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_group");
	}
	
	public function added_group(){
		$postData = $this->input->post();
		if($postData['group_txt'] !=''){
			$insertAry = array(
				'group_name'=>$postData['group_txt']
			);
			$this->db->insert("groups_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_group");
	}
	//tech group end
	
	
	//add person to technician list start --------------------------------------------------------------
	public function manage_technician(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['technician_data'] = $this->Admin_model->get_all_technician();
		$data['include'] = 'backend_iconnect/technicians';
		$data['action_tab'] = 'technicians';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_technician(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['technician_data'] = $this->Admin_model->get_technician_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/technician_page',$data);
	}
	
	public function add_technician(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/technician_page',$data);
	}
	
	public function view_technician(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['technician_data'] = $this->Admin_model->get_technician_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/technician_page',$data);
	}
	
	public function status_technician(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'technician_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'technician_is_active'=>1
			);
		}
		$this->db->where("technician_id",$postData['recd_id']);
		$this->db->update("technician_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_technician(){
		$postData = $this->input->post();
		if($postData['tech_txt'] !=''){
			$techData = explode("~",$postData['tech_txt']);
			if($techData[2]!=''){
				$empCode = $techData[2];
			}else{
				$empCode = 'SGT'.rand(11,99);
			}
			$updateAry = array(
				'tech_code'=>$empCode,
				'technician_email'=>$techData[0],
				'tech_name'=>$techData[1]
			);
			$this->db->where("technician_id",$postData['tech_id']);
			$this->db->update("technician_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_technician");
	}
	
	public function added_technician(){
		$postData = $this->input->post();
		if($postData['tech_txt'] !=''){
			$techData = explode("~",$postData['tech_txt']);
			if($techData[2]!=''){
				$empCode = $techData[2];
			}else{
				$empCode = 'SGT'.rand(11,99);
			}
			$insertAry = array(
				'tech_code'=>$empCode,
				'technician_email'=>$techData[0],
				'tech_name'=>$techData[1]
			);
			$this->db->insert("technician_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_technician");
	}
	//add person to technician list end
	
	
	//tech group assign start --------------------------------------------------------------
	public function manage_assigntech(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['assigntech_data'] = $this->Admin_model->get_all_assigntech();
		$data['include'] = 'backend_iconnect/assigntech';
		$data['action_tab'] = 'assigntech';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_assigntech(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['assigntech_data'] = $this->Admin_model->get_assigntech_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assigntech_page',$data);
	}
	
	public function add_assigntech(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/assigntech_page',$data);
	}
	
	public function view_assigntech(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['assigntech_data'] = $this->Admin_model->get_assigntech_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assigntech_page',$data);
	}
	
	public function status_assigntech(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'technician_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'technician_is_active'=>1
			);
		}
		$this->db->where("technician_group_map_id",$postData['recd_id']);
		$this->db->update("technician_group_map_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_assigntech(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$updateAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->where("technician_group_map_id",$postData['assign_id']);
			$this->db->update("technician_group_map_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_assigntech");
	}
	
	public function added_assigntech(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$insertAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->insert("technician_group_map_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_assigntech");
	}
	//tech group assign end
	
	
	
	
	//important links start
	public function manage_implink(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['implink_data'] = $this->Admin_model->get_all_implink();
		$data['include'] = 'backend_iconnect/implink';
		$data['action_tab'] = 'implink';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_implink(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['implink_data'] = $this->Admin_model->get_implink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/implink_page',$data);
	}
	
	public function add_implink(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/implink_page',$data);
	}
	
	public function view_implink(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['implink_data'] = $this->Admin_model->get_implink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/implink_page',$data);
	}
	
	public function status_implink(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'implink_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'implink_is_active'=>1
			);
		}
		$this->db->where("implink_id",$postData['recd_id']);
		$this->db->update("implinks",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_implink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$updateAry = array(
				'implink_name'=>$postData['implink_name'],
				'implink_url'=>$postData['implink_url']
			);
			$this->db->where("implink_id",$postData['implink_id']);
			$this->db->update("implinks",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_implink");
	}
	
	public function added_implink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$insertAry = array(
				'implink_name'=>$postData['implink_name'],
				'implink_url'=>$postData['implink_url']
			);
			$this->db->insert("implinks",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_implink");
	}
	//important links end
	
	
	//group and item map start ----------------------------------------------------------------------------
	public function manage_assignitem(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['assignitem_data'] = $this->Admin_model->get_all_assignitem();
		$data['include'] = 'backend_iconnect/assignitem';
		$data['action_tab'] = 'assignitem';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_assignitem(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['assignitem_data'] = $this->Admin_model->get_assignitem_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assignitem_page',$data);
	}
	
	public function add_assignitem(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/assignitem_page',$data);
	}
	
	public function view_assignitem(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['assignitem_data'] = $this->Admin_model->get_assignitem_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assignitem_page',$data);
	}
	
	public function status_assignitem(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'group_item_map_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'group_item_map_is_active'=>1
			);
		}
		$this->db->where("group_item_map_id",$postData['recd_id']);
		$this->db->update("group_item_map_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_assignitem(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$updateAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->where("technician_group_map_id",$postData['assign_id']);
			$this->db->update("technician_group_map_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_assignitem");
	}
	
	public function added_assignitem(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$insertAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->insert("technician_group_map_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_assignitem");
	}
	//group and item map end ----------------------------------------------------------------------------
	
	
	//dept announcements start --------------------------------------------------------------------------
	public function manage_dept_announcement(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['deptannouncement_data'] = $this->Admin_model->get_all_deptannouncement();
		$data['include'] = 'backend_iconnect/deptannouncement';
		$data['action_tab'] = 'deptannouncement';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_dept_announcement(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['deptannouncement_data'] = $this->Admin_model->get_deptannouncement_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/deptannouncement_page',$data);
	}
	
	public function add_dept_announcement(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/deptannouncement_page',$data);
	}
	public function view_dept_announcement(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['deptannouncement_data'] = $this->Admin_model->get_deptannouncement_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/deptannouncement_page',$data);
	}
	
	public function status_dept_announcement(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'announcement_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'announcement_is_active'=>1
			);
		}
		$this->db->where("announcement_id",$postData['recd_id']);
		$this->db->update("announcements_hd;",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_dept_announcement(){
		$postData = $this->input->post();
		if($postData['announcement_name'] !='' && $postData['dept_id'] > 0){
			$updateAry = array(
				'dept_id'=>$postData['dept_id'],
				'announcement_name'=>$postData['announcement_name'],
				'announcement_desc'=>$postData['announcement_desc'],
				'announcements_attach'=>$attachName,
				'start_dt'=>date('Y-m-d H:i:s',strtotime($postData['st_date'])),
				'end_dt'=>date('Y-m-d H:i:s',strtotime($postData['ed_date']))
			);
			$this->db->where("announcement_id",$postData['announce_id']);
			$this->db->update("announcements_hd;",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_dept_announcement");
	}
	
	public function added_dept_announcement(){
		$postData = $this->input->post();
		if($postData['announcement_name'] !='' && $postData['dept_id'] > 0){
			$insertAry = array(
				'dept_id'=>$postData['dept_id'],
				'announcement_name'=>$postData['announcement_name'],
				'announcement_desc'=>$postData['announcement_desc'],
				'announcements_attach'=>$attachName,
				'start_dt'=>date('Y-m-d H:i:s',strtotime($postData['st_date'])),
				'end_dt'=>date('Y-m-d H:i:s',strtotime($postData['ed_date']))
			);
			$this->db->insert("announcements_hd;",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_dept_announcement");
	}
	//dept announcements end ----------------------------------------------------------------------------
	
	
	//IP links start
	public function manage_iplink(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['iplink_data'] = $this->Admin_model->get_all_iplink();
		$data['include'] = 'backend_iconnect/iplink';
		$data['action_tab'] = 'iplink';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function update_iplink(){
		$data = $this->get_ledap();
		$data['iplink_data'] = $this->Admin_model->get_all_iplink();
		$this->load->view('backend_iconnect/iplink_ajx',$data);
	}
	
	public function edit_iplink(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['iplink_data'] = $this->Admin_model->get_iplink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/iplink_page',$data);
	}
	
	public function add_iplink(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/iplink_page',$data);
	}
	
	public function view_iplink(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['iplink_data'] = $this->Admin_model->get_iplink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/iplink_page',$data);
	}
	
	public function status_iplink(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'ip_email_shoot'=>1
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'ip_email_shoot'=>0
			);
			$updateAry2 = array(
				'link_down_count'=>0
			);
			$this->db->where("ip_monitor_id",$postData['recd_id']);
			$this->db->update("ip_monitor",$updateAry2);
		}
		$this->db->where("ip_monitor_id",$postData['recd_id']);
		$this->db->update("ip_monitor",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_iplink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$updateAry = array(
				'ip_number'=>$postData['implink_name'],
				'ip_email_id'=>$postData['implink_url']
			);
			$this->db->where("ip_monitor_id",$postData['iplink_id']);
			$this->db->update("ip_monitor",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_iplink");
	}
	
	public function added_iplink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$insertAry = array(
				'ip_number'=>$postData['implink_name'],
				'ip_email_id'=>$postData['implink_url']
			);
			$this->db->insert("ip_monitor",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_iplink");
	}
	//IP links end
	
	
	
//helpdesk code end
	
	
	//ldap data 
	public function get_ledap(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		return $data;
	}
	
	//<<NEW CODE >> Twice a day 
	public function cron_job_update_ladp(){
		//ini_set('max_execution_time', 300000);
		$server = ldap_serverhost;
        $basedn = ldap_basedn;
		$handle = sysu;
		$password = sysp;
        $dn = ldap_basedn . "\\" . $handle;
        $connect = ldap_connect($server);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
		
		//var_dump(ldap_bind($connect, $dn, $password));
        if ($ds = @ldap_bind($connect, $dn, $password)) {
            //$filters = "(samaccountname=$handle)";
			
            $filters = "(cn=*)"; // get all results
            $results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
            $entries = ldap_get_entries($connect, $results);
			//echo '<pre>';
			//echo 'count of entities are '.count($entries);
			//exit; die;
            if(count($entries) > 0){
				for ($i = 0; $i < count($entries); $i++) {
					$ldap_userinfo = array(
						//'usnchanged' => $entries[$i]["usnchanged"][0],
						'samaccountname' => $entries[$i]["samaccountname"][0],
						'unique_id'=> base64_encode($entries[$i]["objectguid"][0]),
						'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
						'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
						'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
						'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
						'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
						'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
						'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
						'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
						'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
						'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
						'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
					);
					//print_r($ldap_userinfo);
					
					if($ldap_userinfo['mail'] !='' || $ldap_userinfo['employeeid'] !=''){
						$lognSql = "select * from ldapusers where unique_id='" .$ldap_userinfo['unique_id']. "' ";
						$local_result = $this->Base_model->run_query($lognSql);
						if (count($local_result) == 0) {
							$local_user_insert['unique_id'] = $ldap_userinfo['unique_id'];
							$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
							$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
							$local_user_insert['u_name'] = $ldap_userinfo['name'];
							$local_user_insert['u_email'] = $ldap_userinfo['mail'];
							$local_user_insert['u_designation'] = $ldap_userinfo['description'];
							$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
							$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
							
							$local_user_insert['u_image'] = NULL;
							$local_user_insert['u_cover_image'] = NULL;
							
							$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
							$local_user_insert['u_status'] = 1;
							$local_user_insert['u_role'] = 0;
							$this->Base_model->insert_operation($local_user_insert, 'ldapusers');
						} else {
							$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
							$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
							$local_user_insert['u_name'] = $ldap_userinfo['name'];
							$local_user_insert['u_email'] = $ldap_userinfo['mail'];
							$local_user_insert['u_designation'] = $ldap_userinfo['description'];
							$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
							$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
							
							$local_user_insert['u_image'] = NULL;
							$local_user_insert['u_cover_image'] = NULL;
							
							$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
							$local_user_insert['u_status'] = 1;
							$local_user_insert['u_role'] = 0;
							
							$logi_where['unique_id'] = $ldap_userinfo['unique_id'];
							$this->Base_model->update_operation($local_user_insert, 'ldapusers', $logi_where);
						}
						
					}
				}
			}
		}
	}
	
	//Outlook emails
	
	public function manage_outlookemail()
	{	
		$data = $this->get_ledap();
		$data['my_title']  = 'Outlook Emails';
		$data['emailData'] =  $this->Dailytask_model->get_outlookemail();
		$data['action_tab'] = 'Outlook_mails';
		$data['include'] = 'backend_iconnect/outlook_emails';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	
	// daily task code start************************************************
	public function manage_dailytask()
	{	
		$data = $this->get_ledap();
		$data['my_title']  = 'Daily task';
		$data['all_tasks'] =  $this->Dailytask_model->get_dailytask($this->user_ldamp);
		$data['action_tab'] = 'daily_task';
		$data['include'] = 'backend_iconnect/daily_task1';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function daily_task_report1(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'daily_task_report';
		$data['report_data'] = json_encode($this->Dailytask_model->get_employee_report(),true);
		$data['include'] = 'backend_iconnect/daily_task_report';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function daily_task_report_dep(){
		$data = $this->get_ledap();
		$postData = $this->input->post();
		$data['action_tab'] = 'daily_task_report';
		$data['post_type'] = (isset($postData['filter_type'])) ? $postData['filter_type'] : 'by_date';
		$data['filter_date'] = (isset($postData['filter_date'])) ? $postData['filter_date'] : date('d-m-Y');
		$data['report_data'] = json_encode($this->Dailytask_model->get_employee_report1($data['post_type'],$data['filter_date']),true);
		$data['include'] = 'backend_iconnect/daily_task_report_dup';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	/*
	public function downloadReport($customReport=''){
		$postData = $this->input->post();
	    $contents ='"Sr No","Employee Name","Employee Code","Project Name","Start Date","End Date","Task activity","Task priority","Task status","Working time","Remarks"';
	    $contents .="\n";
		$i = 1;
	    foreach($customReport as $taskCRecord){
			$all_tasks = $this->Dailytask_model->get_dailytask1($taskCRecord['emp_code'],$filter_date);
			foreach($all_tasks as $task){				
				$contents.='"'.$i.'","'.$taskCRecord['u_name'].'","'.$taskCRecord['emp_code'].'","'.$task['project_name'].'","'.date('d-m-Y',strtotime($task['start_date'])).'","'.date('d-m-Y',strtotime($task['end_date'])).'","'.ucfirst($task['task_activity']).'","'.ucfirst($task['task_priority']).'","'.ucfirst($task['remarks']).'"';
				$contents.="\n";
				$i++;
			}
	    }
	    $contents = strip_tags($contents);
	    Header("Content-Disposition: attachment;filename=dailytask_report.csv ");
	    print $contents;
	    exit;
	}
	*/
	
	public function get_employee_tasks($emp_code="",$filter_date=""){
		if ($this->input->is_ajax_request()) {
			if($filter_date !=''){
				$data['all_tasks'] =  $this->Dailytask_model->get_dailytask1($emp_code,$filter_date);
			}else{
				$data['all_tasks'] =  $this->Dailytask_model->get_dailytask1($emp_code);
			}
			$this->load->view('backend_iconnect/daily_task_report_ajax',$data);
		}
	}
	
	public function save_task()
	{
		$postData = $this->input->post();
		if(!empty($postData)){
			if($this->Dailytask_model->save_dailytask($this->user_ldamp)){
			
				$this->session->set_flashdata('msg','Task Saved Successfully.');
				redirect('admin/manage_dailytask');
			}
			else{
				$this->session->set_flashdata('error_msg','An error occured while Saving...');
				redirect('admin/manage_dailytask');
			}
		}
		else{
			$this->session->set_flashdata('error_msg','An error occured while Saving...');
			redirect('admin/manage_dailytask');
		}
	}
	
	public function sendDailyTaskEmail($fromName='',$toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);
		$mail = '';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name=$fromName;
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('ravi.kumar@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	// daily task code end************************************************	
	
	//yammar code start ***********************************
	public function ydashboard(){
		$data['yem'] = 1;
		$data['include'] = 'backend_iconnect/yammer/profile';
		$this->load->view('backend_iconnect/yammer/masterpage',$data);
	}
	
	public function testUpload(){
		$postData = $this->input->post();
		$returnAry = array(
			'fileName'=>$_FILES['file']['name']
		);
		echo json_encode($returnAry);
	}
	//yammar code end *************************************
	
	
}

