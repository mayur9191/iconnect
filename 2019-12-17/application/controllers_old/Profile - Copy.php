<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
		
		//phpinfo();
		
        $this->mssql = $this->load->database('mssql', TRUE);
		
        $this->user_ldamp = $this->session->userdata('user_ldamp');

        $lognSql = "select * from users where u_ldap_userid='" . $this->user_ldamp['samaccountname'] . "' ";
        $this->user_indo = $this->Base_model->run_query($lognSql);

        $mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        $mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        $this->hrdata_user_info = $mssql_user_info_query->result_array();
		//echo '<pre>';print_r($this->hrdata_user_info);exit;

        $mssql_cunnect_birthday_sql = "SELECT *   FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE DATE_OF_BIRTH = '" . date("Y-m-d") . "'";
        $mssql_cunnect_birthday_query = $this->mssql->query($mssql_cunnect_birthday_sql);
        $this->cunnect_birthdays = $mssql_cunnect_birthday_query->result_array();

        $mssql_upcoming_birthday_sql = "SELECT *   FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE  DATEPART(mm,DATE_OF_BIRTH) > '" . date("m") . "' AND DATEPART(dd,DATE_OF_BIRTH) > '" . date("d") . "'  ORDER BY DATEPART(mm,DATE_OF_BIRTH), DATEPART(dd,DATE_OF_BIRTH) ASC OFFSET 0 ROWS FETCH NEXT 6 ROWS ONLY";
        $mssql_upcoming_birthday_query = $this->mssql->query($mssql_upcoming_birthday_sql);
        $this->upcoming_birthdays = $mssql_upcoming_birthday_query->result_array();
		
		
		//RV
		//$mssql_upcoming_marriage_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE DATEPART(mm,WEDDING_DATE) > '" . date("m") . "' AND DATEPART(dd,WEDDING_DATE) > '" . date("d") . "'  ORDER BY DATEPART(mm,WEDDING_DATE), DATEPART(dd,WEDDING_DATE) ASC";
		
		$mssql_upcoming_marriage_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE DATEPART(mm,WEDDING_DATE)>='" . date("m") . "' AND DATEPART(dd,WEDDING_DATE) !='" . date("d") . "' ORDER BY DATEPART(mm,WEDDING_DATE) ASC OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";
		
		//$desc_query = "select * from DU_EMP_PERSONAL_DETAIL_UPD ";
		
        $mssql_upcoming_marriage_query = $this->mssql->query($mssql_upcoming_marriage_sql);
        $this->upcoming_marriage = $mssql_upcoming_marriage_query->result_array();
		
		
		  $wallSql = "select * from user_wall_updates AS w LEFT JOIN users AS u ON u.u_id=w.uw_user_id WHERE w.uw_status=0 GROUP BY uw_id ORDER BY uw_id DESC LIMIT 0, 20 ";
       $this->user_wall = $this->Base_model->run_query($wallSql);
		

//        phpinfo();


        if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }


    }

    public function index()
    {

//        $mssql= "SELECT *   FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE  DATEPART(mm,DATE_OF_BIRTH) > '".date("m")."' AND DATEPART(dd,DATE_OF_BIRTH) > '".date("d")."'  ORDER BY DATEPART(mm,DATE_OF_BIRTH), DATEPART(dd,DATE_OF_BIRTH) ASC ";
//        $mssql= "SELECT *   FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE MONTH(DATE_OF_BIRTH)  > '".date("m")."'  ORDER BY MONTH(DATE_OF_BIRTH) ASC ";
//        $mssql= "SELECT *   FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '".$this->user_ldamp['employeeid']."'";
//        $msdataquery = $this->mssql->query($mssql);
//        $row = $msdataquery->result_array();

//        echo $mssql;
//
//
//        echo '<pre>';
//        print_r($this->user_ldamp);
//        print_r($row);
//        exit();


        $data['titile'] = 'User Profile';

        $data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);

      


        $birt_commSql = "select * from birthday_comments AS w LEFT JOIN users AS u ON u.u_id=w.bc_userid GROUP BY w.bc_comment_group ORDER BY w.bc_id DESC LIMIT 0, 20 ";
        $data['birthday_comments'] = $this->Base_model->run_query($birt_commSql); 
		
		$birt_commSql = "select * from announcements AS w LEFT JOIN users AS u ON u.u_id=w.an_created_by GROUP BY w.an_id ORDER BY w.an_id DESC LIMIT 0, 20 ";
        $data['announcements'] = $this->Base_model->run_query($birt_commSql);
		

        $birt_mycommSql = "select * from birthday_comments AS w LEFT JOIN users AS u ON u.u_id=w.bc_userid WHERE w.bc_brith_uid=".$this->user_ldamp['user_id']." GROUP BY w.bc_comment_group ORDER BY w.bc_id DESC LIMIT 0, 20 ";
        $data['my_comments'] = $this->Base_model->run_query($birt_mycommSql);

        $birthday_wishes_Sql = "select * from birthday_wishes  ORDER BY bw_id DESC LIMIT 0, 20 ";
//        $wallSql = "select * from birthday_wishes AS w LEFT JOIN users AS u ON u.u_id=w.uw_user_id GROUP BY uw_id ORDER BY uw_id DESC LIMIT 0, 20 ";
        $data['birthday_wishes'] = $this->Base_model->run_query($birthday_wishes_Sql);

        $slides_Sql = "select * from profile_slides  ORDER BY sl_id DESC LIMIT 0, 10 ";
        $data['prifile_slides'] = $this->Base_model->run_query($slides_Sql);


//        phpinfo();
//        exit();


//        $data['msdataquery'] = $msdataquery;


        $this->load->view('profile', $data);
    }

    public function wishbirthday()
    {

        if ($this->input->post()) {
            $bc_comment = $this->input->post('birth_commet', TRUE);
            $bc_brith_uid = $this->input->post('brth_usrs', TRUE);
            $bc_brith_uids = explode(',', $bc_brith_uid);

            $bc_comment_group = time();

            if (count(array_filter($bc_brith_uids)) > 0) {
                foreach ($bc_brith_uids AS $bc_brith_user) {

                    $wish_birth_insert['bc_userid'] = $this->user_ldamp['user_id'];
                    $wish_birth_insert['bc_brith_uid'] = $bc_brith_user;
                    $wish_birth_insert['bc_comment_group'] = $bc_comment_group;
                    $wish_birth_insert['bc_comment'] = $bc_comment;
                    $this->Base_model->insert_operation($wish_birth_insert, 'birthday_comments');
                }
            }

        }


        redirect($this->agent->referrer());


    }

    public function birthdayLike(){
        $postData = $this->input->post();
        $like_user_id = $postData['like_user_id'];
        $birth_users = explode(",",$postData['birthday_user_ids']);
        foreach($birth_users as $birth_users_id){
            $birthdayLikeAry = array(
                'bl_buid'=>$like_user_id,
                'bl_user_id'=> $birth_users_id
            );
            $this->Base_model->insert_operation($birthdayLikeAry, 'birthday_likes');
        }
        echo $this->agent->referrer();
    }


    public function dpchange()
    {


        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
//            print_r($_FILES);


            $folderNew = 'uploads/users/profiles/';
            if (!file_exists($folderNew)) {
                mkdir($folderNew, 0777, true);
            }
            $folderNewthumb = 'uploads/users/profiles/thumb/';
            if (!file_exists($folderNewthumb)) {
                mkdir($folderNewthumb, 0777, true);
            }


            if ($this->user_indo[0]->u_image != '') {
                @unlink($folderNew . $this->user_indo[0]->u_image);
                @unlink($folderNewthumb . $this->user_indo[0]->u_image);
            }

            $Image = '';
            $record_image = $_FILES['image']['name'];
            if (!empty($record_image)) {
                $ext = explode('.', $_FILES['image']['name']);
                $Image = "profile_" . $this->user_indo[0]->u_id . '-' . random_string('alnum', 20) . "." . $ext[1];
                $update_dp_inputdata['u_image'] = $Image;

                move_uploaded_file($_FILES['image']['tmp_name'], $folderNew . $Image);

                $this->create_custom_image($folderNew . $Image, $folderNewthumb . $Image, 300, 300);
                $this->resize_image($folderNew . $Image, 920, 1087);


                $dp_where['u_id'] = $this->user_ldamp['user_id'];
                $this->Base_model->update_operation($update_dp_inputdata, 'users', $dp_where);

                redirect($this->agent->referrer());
            }


        } else {
            redirect($this->agent->referrer());
        }


    }

	public function announcement(){
		
		  if ($this->input->post()) {
			  //error_reporting(E_ALL);
			 //print_r($_POST);
			 
			// exit();
			  
			  
			$announcement_dir = 'uploads/announcement/';
            if (!file_exists($announcement_dir)) {
                mkdir($announcement_dir, 0777, true);
            }

			
			$file_name = '';

            $record_image = $_FILES['attachemnet']['name'];
            if (!empty($record_image)) {
                $ext = explode('.', $_FILES['attachemnet']['name']);
                $file_announcement = "announcement_" . '-' . random_string('alnum', 20) . "." . $ext[1];
                move_uploaded_file($_FILES['attachemnet']['tmp_name'], $announcement_dir . $file_announcement);

                //$this->create_custom_image($sliders_dir . $Image, 520, 300);              
				$file_name = $file_announcement ;
            }else{
				$file_name= '';
			}
			  
			    $slide_img_inputdata['an_file'] = $file_name;
                $slide_img_inputdata['an_text'] = $this->input->post('announcement_text');
                $slide_img_inputdata['an_created_by'] = $this->user_ldamp['user_id'];
                $slide_img_inputdata['an_date'] = date('Y-m-d');
                $this->Base_model->insert_operation($slide_img_inputdata, 'announcements');
			  
			   redirect($this->agent->referrer());
			  
			  
		  }
		
		
	}
	
    public function sliders($type='')
    {

        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

            $sliders_dir = 'uploads/sliders/';
            if (!file_exists($sliders_dir)) {
                mkdir($sliders_dir, 0777, true);
            }


            $record_image = $_FILES['image']['name'];
            if (!empty($record_image)) {
                $ext = explode('.', $_FILES['image']['name']);
                $Image = "slide_" . '-' . random_string('alnum', 20) . "." . $ext[1];
                move_uploaded_file($_FILES['image']['tmp_name'], $sliders_dir . $Image);

                //$this->create_custom_image($sliders_dir . $Image, 520, 300);


                $slide_img_inputdata['sl_image'] = $Image;
//                $birthday_img_inputdata['bw_post_user_id'] = $this->user_ldamp['user_id'];
                $this->Base_model->insert_operation($slide_img_inputdata, 'profile_slides');

              redirect($this->agent->referrer());
			 
			 exit();
            }


        }


    }

    public function bithdaypost($type)
    {

        switch ($type) {
            case 'post':

                $birthdir = 'uploads/birthdays/';
                if (!file_exists($birthdir)) {
                    mkdir($birthdir, 0777, true);
                }


                $record_image = $_FILES['image']['name'];
                if (!empty($record_image)) {
                    $ext = explode('.', $_FILES['image']['name']);
                    $Image = "profile_" . $this->user_indo[0]->u_id . '-' . random_string('alnum', 20) . "." . $ext[1];
                    move_uploaded_file($_FILES['image']['tmp_name'], $birthdir . $Image);
                    $this->create_custom_image($birthdir . $Image, 230, 270);


                    $birthday_img_inputdata['bw_image'] = $Image;
                    $birthday_img_inputdata['bw_post_user_id'] = $this->user_ldamp['user_id'];
                    $this->Base_model->insert_operation($birthday_img_inputdata, 'birthday_wishes');

                    redirect($this->agent->referrer());
                }


                print_r($_FILES);
                exit();
                break;
            case 'delete':
                break;
            default:
                break;
        }
    }

    public function wallpost()
    {
//        print_r($_POST);
//        print_r($this->user_ldamp);

        if ($this->input->post()) {
            $this->form_validation->set_rules('walltext', 'wall comment', 'trim|required');
            if ($this->form_validation->run() == TRUE) {

                $walltext = $this->input->post('walltext', TRUE);

                $local_user_insert['uw_user_id'] = $this->user_ldamp['user_id'];
                $local_user_insert['uw_wall_text'] = $walltext;
                $this->Base_model->insert_operation($local_user_insert, 'user_wall_updates');

                redirect($this->agent->referrer());

            }
        }


    }

    public function wall_update()
    {
//        print_r($_POST);
//        exit();
//        print_r($this->user_ldamp);

        if ($this->input->post()) {
            $this->form_validation->set_rules('edit_wall_text', 'wall comment', 'trim|required');
            $this->form_validation->set_rules('edit_wall_id', 'wall ID', 'trim|required');
            if ($this->form_validation->run() == TRUE) {

                $edit_wall_text = $this->input->post('edit_wall_text', TRUE);
                $edit_wall_id = $this->input->post('edit_wall_id', TRUE);

                $wall_update_inputdata['uw_wall_text'] = $edit_wall_text;
                $wall_update_where['uw_id'] = nl2br($edit_wall_id);
                $this->Base_model->update_operation($wall_update_inputdata, 'user_wall_updates', $wall_update_where);

                redirect($this->agent->referrer());

            }
        }


    }
	
	public function edit(){
		
		if ($this->input->post()) {
			  $this->form_validation->set_rules('EMP_STAFFID', 'Emp. Staffid', 'trim|required');
			  $this->form_validation->set_rules('ROW_ID','Unique id', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$postData = $this->input->post();
				
				//$date=date_create($postData['WEDDING_DATE']);//("16-03-2013");
				//echo date_format($date,"Y/m/d H:i:s");
				//echo 'ss'.date('Y/m/d',strtotime($postData['WEDDING_DATE']));exit();
				
				$ROW_ID = trim($postData['ROW_ID']);
				$EMP_STAFFID = trim($postData['EMP_STAFFID']);
				$SURNAME = trim($postData['SURNAME']);
				$FIRST_NAME = trim($postData['FIRST_NAME']);
				$MIDDLE_NAME = trim($postData['MIDDLE_NAME']);
				$KNOWN_NAME = trim($postData['KNOWN_NAME']);
				$OFFICIAL_NAME = trim($postData['OFFICIAL_NAME']);
				$DATE_OF_BIRTH = date('Y/m/d',strtotime($postData['DATE_OF_BIRTH']));
				$STATE_OF_BIRTH = trim($postData['STATE_OF_BIRTH']);
				$COUNTRY_OF_BIRTH = trim($postData['COUNTRY_OF_BIRTH']);
				$MARITAL_STATUS = trim($postData['MARITAL_STATUS']);
				$WEDDING_DATE = date('Y/m/d',strtotime($postData['WEDDING_DATE']));
				$NATIONALITY = trim($postData['NATIONALITY']);
				$PAN_NUMBER = trim($postData['PAN_NUMBER']);
				$PREVIOUS_NAME = trim($postData['PREVIOUS_NAME']);
				$LANGUAGES = trim($postData['LANGUAGES']);
				$GENDER = trim($postData['GENDER']);
				$BLOOD_GROUP = trim($postData['BLOOD_GROUP']);
				$AADHAR_NO = trim($postData['AADHAR_NO']);
				$ETHINIC = trim($postData['ETHINIC']);
				
				$profile_update_sql = "UPDATE DU_EMP_PERSONAL_DETAIL_UPD SET EMP_STAFFID='$EMP_STAFFID',SURNAME='$SURNAME',FIRST_NAME='$FIRST_NAME',MIDDLE_NAME='$MIDDLE_NAME',KNOWN_NAME='$KNOWN_NAME',OFFICIAL_NAME='$OFFICIAL_NAME',DATE_OF_BIRTH='$DATE_OF_BIRTH',STATE_OF_BIRTH='$STATE_OF_BIRTH',COUNTRY_OF_BIRTH='$COUNTRY_OF_BIRTH',MARITAL_STATUS='$MARITAL_STATUS',WEDDING_DATE='$WEDDING_DATE',NATIONALITY='$NATIONALITY',PAN_NUMBER='$PAN_NUMBER',PREVIOUS_NAME='$PREVIOUS_NAME',LANGUAGES='$LANGUAGES',BLOOD_GROUP='$BLOOD_GROUP',GENDER='$GENDER',AADHAR_NO='$AADHAR_NO',ETHINIC='$ETHINIC' WHERE ROW_ID='$ROW_ID'";
				$this->mssql->query($profile_update_sql);
			}
			redirect($this->agent->referrer());
			
		}else{
			 $birt_mycommSql = "select * from birthday_comments AS w LEFT JOIN users AS u ON u.u_id=w.bc_userid WHERE w.bc_brith_uid=".$this->user_ldamp['user_id']." GROUP BY w.bc_comment_group ORDER BY w.bc_id DESC LIMIT 0, 20 ";
			$data['my_comments'] = $this->Base_model->run_query($birt_mycommSql);
		
			$data['user_ldamp'] = $this->user_ldamp;
			$lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
			$data['user_indo'] = $this->Base_model->run_query($lognSql);
		
			$this->load->view('module/profile/edit',$data);
		}
		
	}

    public function logout()
    {

        $this->session->unset_userdata('user_ldamp');
        redirect('login', 'refresh');


//        $this->data['active_class'] = 'logout';
//        $this->data['title'] = 'logout';
//        $this->load->view('login');
    }
	
	//test query 
	public function testquery($employeeid=0){ //SG0155
		$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '".$employeeid."'";
        $mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        $this->hrdata_user_info = $mssql_user_info_query->result_array();
		echo '<pre>'; print_r($this->hrdata_user_info);
	}
}
