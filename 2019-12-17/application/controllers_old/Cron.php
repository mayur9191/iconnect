<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cron extends CI_Controller {
	
	public function __construct()
	{ 
		parent::__construct();
		include APPPATH . 'third_party/class.phpmailer.php';
	}
	
	public function test(){
		echo "Hiii".PHP_EOL;die;
	}
	
	//*********************************common function starts****************************************************
	//send email - SMTP
	public function sendOutlookEmail($toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);
		$cc = 'ravi.kumar@skeiron.com,Shaijad.Kureshi@skeiron.com,Johny.Anthony@skeiron.com';
		$mail = '';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name="IP Monitoring";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('ravi.kumar@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	
	//New app for ping IP start 
	public function pingUrl(){
		$qry = $this->db->query("SELECT * FROM ip_monitor WHERE ip_is_active=1");
		$ipData = $qry->result();
		foreach($ipData as $ipRecord){
			$domain = '';
			$domain = $ipRecord->ip_number;
			if($domain!=''){
				echo '<br><br>';
				$pingData = exec('ping -n 1 -w 1 '.$domain);
				$findme = 'Lost';
				$position = 0;
				$position = strpos($pingData, $findme);
				if($position > 0){
					//down
					echo '<br>IP is down IP - '.$url;
					//$email = 'network.support@skeiron.com';
					$email = 'avinash.kawre@skeiron.com';
					//$email = 'ravi.kumar@skeiron.com';
					$subject = 'The IP '.$domain.' is DOWN - '.$ipRecord->ip_email_id;
					$msg = 'Hi <br> The IP '.$domain.' is down please check and do the needful to resolve the issue. <br> Please contact the below technicians for the same <br><br><br><br>Shaijad kureshi :- 9970567872<br>Avinash Kawre :- 7350389754<br>Johny Anthony :- 9819618486';
					/*
						Dear Sir,
						 The IP '.$domain.' is down. <br>We are working on the issue, once it get resolved, we will update you for the same. <br><br><br><br> Please feel free to contact the below engineers for any futher assitance <br><br><br><br>Shaijad Kureshi :- 9970567872<br>Avinash Kawre :- 7350389754<br>Johny Anthony :- 9819618486'
					*/
					$linkCount = 1;
					if($ipRecord->ip_email_shoot==0 && $ipRecord->link_down_count>=10){
						$this->sendOutlookEmail($email,$subject,$msg);
						$updateAry = array(
							'ip_email_shoot'=>1
						);						
						$this->updateIPData($updateAry,$ipRecord->ip_monitor_id);
					}
					
					$linkCount += $ipRecord->link_down_count;
					$updateAry1 = array(
						'link_down_count'=>$linkCount
					);
					$this->updateIPData($updateAry1,$ipRecord->ip_monitor_id);
				}else{
					$updateAry2 = array(
						'link_down_count'=>0
					);
					$this->updateIPData($updateAry2,$ipRecord->ip_monitor_id);
					//up and running
					if($ipRecord->ip_email_shoot==1 && $ipRecord->link_down_count==0){
						$email1 = 'avinash.kawre@skeiron.com';
						$subject1 = 'The IP '.$domain.' is UP now - '.$ipRecord->ip_email_id;
						$msg1 = 'Hi <br> The IP '.$domain.' is up no need to check. <br> Please contact the below technicians for the same <br><br><br><br>Shaijad kureshi :- 9970567872<br>Avinash Kawre :- 7350389754<br>Johny Anthony :- 9819618486';
						$this->sendOutlookEmail($email1,$subject1,$msg1);
						$updateAry = array(
							'ip_email_shoot'=>0
						);						
						$this->updateIPData($updateAry,$ipRecord->ip_monitor_id);
					}
					
					echo '<br>IP up and running IP - '.$domain;
				}
				echo '(IP - '.$domain.' - position '.$position.'. Status '.$pingData.')';
			}
		}
	}
	
	public function updateIPData($updateAry, $tablId){
		$this->db->where('ip_monitor_id',$tablId);
		$this->db->update('ip_monitor',$updateAry);
		return true;
	}
	//New app for ping IP end
	
}