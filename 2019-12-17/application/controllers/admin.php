<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller {
	public function __construct(){
		
		parent::__construct();
		include APPPATH . 'third_party/class.phpmailer.php';
		//$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->is_admin = is_Admin($this->user_ldamp["employeeid"]);
		//if(!$this->is_admin){
		//	redirect("profile");
		//}
		$lognSql = "select * from users where u_ldap_userid='" . $this->user_ldamp['samaccountname'] . "' ";
        $this->user_indo = $this->Base_model->run_query($lognSql);
		$this->load->model('Admin_model'); 
		$this->load->model('Dailytask_model'); 
		
		//$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        //$mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        //$this->hrdata_user_info = $mssql_user_info_query->result_array();
		
	}
	
	public function index(){
		
		$data = $this->get_ledap();
		$data['action_tab'] = 'dashboard';
		$data['include'] = 'backend_iconnect/homepage';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function admin_dashboard(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/homepage';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
//iconnect code start 
	//announcement start ----------------------------------------------------------------------
	public function manage_announcements(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['announcements_data'] = $this->Admin_model->get_all_announcements();
		$data['include'] = 'backend_iconnect/announcements';
		$data['action_tab'] = 'manage_announcements';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page',$data);
	}
	
	public function add_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/announcements_page',$data);
	}
	public function view_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page',$data);
	}
	
	public function status_announcements(){
		$postData = $this->input->post();
		$data = $this->get_ledap();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'an_status'=>0
			);
			
			$this->track_user_activity($postData['recd_id'], 'annuncement_deactivated');
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'an_status'=>1
			);
			
			$this->track_user_activity($postData['recd_id'], 'annuncement_activated');
		}
		$this->db->where("an_id",$postData['recd_id']);
		$this->db->update("announcements",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_announcement(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$update_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->where("an_id",$postData['anno_id']);
			$this->db->update("announcements",$update_ary);
			
			$this->track_user_activity($postData['anno_id'], 'annuncement_updated');
			
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_announcements");
	}
	
	public function added_announcement(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$insert_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->insert("announcements",$insert_ary);
			
			//add user details - track who added mailer
			$annoucements_id =  $this->db->insert_id();
			$this->track_user_activity($annoucements_id, 'announcement_uploaded');
			
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_announcements");
	}
	// to remove the annoucements
	public function remove_annoucement(){
		   $postData = $this->input->post();
		   $data =  $this->Admin_model->get_announcements_by_id($postData['recd_id']);
		  
		   if(count($data) > 0){
			//echo "<pre>";print_r($data);
			$ann_file = $data->an_file;
			//echo $ann_file;die;
			$path = 'uploads/announcement/'.$ann_file;
			if(isset($path)){
				$delete_ary = array(
					'is_deleted'=>1,
					'an_status'=>0
				);
				$this->db->where("an_id",$postData['recd_id']);
		        $this->db->update("announcements",$delete_ary);
				$this->track_user_activity($postData['recd_id'], 'announcement_deleted');
			    $this->session->set_flashdata('message_submit', 'Record removed successfully');
			    $status = "success";
			}
			else
			{
				$status = "failed";
			}
		}
		else{
			$status = "failed";
		}
		echo $status;die;
	}
	
	function track_user_activity($primary_id=0, $action_taken=''){
		$data = $this->get_ledap();
		$userTrack = array(
			'announcement_id'=>$primary_id,
			'emp_code'=>$data['user_ldamp']['employeeid'],
			'action_taken'=>$action_taken,
			'action_date'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("user_activity_track",$userTrack);
		return true;
	}
	//announcement end -----------------------------------------------------------------------------------------
	
	
//TV Mailer start 
	//announcement TV start ----------------------------------------------------------------------
	public function manage_announcements_tv(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['announcements_data'] = $this->Admin_model->get_all_announcements_tv();
		$data['include'] = 'backend_iconnect/announcements_tv';
		$data['action_tab'] = 'announcements_tv';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_announcements_tv(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id_tv($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page_tv',$data);
	}
	
	public function add_announcements_tv(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/announcements_page_tv',$data);
	}
	public function view_announcements_tv(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['announcements_data'] = $this->Admin_model->get_announcements_by_id_tv($postData['recd_id']);
		$this->load->view('backend_iconnect/announcements_page_tv',$data);
	}
	
	public function status_announcements_tv(){
		$postData = $this->input->post();
		$data = $this->get_ledap();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'an_status'=>0
			);
			$this->track_user_activity_tv($postData['recd_id'], 'annuncement_tv_deactivated');
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'an_status'=>1
			);
			
			$this->track_user_activity_tv($postData['recd_id'], 'annuncement_tv_activated');
		}
		$this->db->where("an_id",$postData['recd_id']);
		$this->db->update("announcements_tv",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_announcement_tv(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement_tv/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$update_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->where("an_id",$postData['anno_id']);
			$this->db->update("announcements_tv",$update_ary);
			
			$this->track_user_activity_tv($postData['anno_id'], 'annuncement_tv_updated');
			
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_announcements_tv");
	}
	
	public function added_announcement_tv(){
		$postData = $this->input->post();
		$announcement_dir = 'uploads/announcement_tv/';
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			$file_announcement = "announcement_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$insert_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>1,
				'an_status'=>1
			);
			$this->db->insert("announcements_tv",$insert_ary);
			
			//add user details - track who added mailer
			$annoucements_id =  $this->db->insert_id();
			$this->track_user_activity_tv($annoucements_id, 'announcement_tv_uploaded');
			
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_announcements_tv");
	}
	// to remove the annoucements
	public function remove_annoucement_tv(){
		   $postData = $this->input->post();
		   $data =  $this->Admin_model->get_announcements_by_id_tv($postData['recd_id']);
		  
		   if(count($data) > 0){
			//echo "<pre>";print_r($data);
			$ann_file = $data->an_file;
			//echo $ann_file;die;
			$path = 'uploads/announcement_tv/'.$ann_file;
			if(isset($path)){
				$delete_ary = array(
					'is_deleted'=>1,
					'an_status'=>0
				);
				$this->db->where("an_id",$postData['recd_id']);
		        $this->db->update("announcements_tv",$delete_ary);
				$this->track_user_activity($postData['recd_id'], 'announcement_tv_deleted');
			    $this->session->set_flashdata('message_submit', 'Record removed successfully');
			    $status = "success";
			}
			else
			{
				$status = "failed";
			}
		}
		else{
			$status = "failed";
		}
		echo $status;die;
	}
	
	function track_user_activity_tv($primary_id=0, $action_taken=''){
		$data = $this->get_ledap();
		$userTrack = array(
			'announcement_id'=>$primary_id,
			'emp_code'=>$data['user_ldamp']['employeeid'],
			'action_taken'=>$action_taken,
			'action_date'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("user_activity_track",$userTrack);
		return true;
	}
	//announcement TV end -----------------------------------------------------------------------------------------
	
	
	//thought of the day start --------------------------------------------------------------------------------
	public function manage_thought(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['thought_data'] = $this->Admin_model->get_all_thought();
		$data['include'] = 'backend_iconnect/thought_day';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_thought(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['thought_data'] = $this->Admin_model->get_thought_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/thought_day_page',$data);
	}
	
	public function add_thought(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/thought_day_page',$data);
	}
	public function view_thought(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['thought_data'] = $this->Admin_model->get_thought_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/thought_day_page',$data);
	}
	
	public function status_thought(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'thought_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'thought_is_active'=>1
			);
		}
		$this->db->where("thought_id",$postData['recd_id']);
		$this->db->update("iconnect_thought",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_thought(){
		$postData = $this->input->post();
		if($postData['thought_txt'] !=''){
			$updateAry = array(
				'thought_text'=>$postData['thought_txt']
			);
			$this->db->where("thought_id",$postData['thog_id']);
			$this->db->update("iconnect_thought",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_thought");
	}
	
	public function added_thought(){
		$postData = $this->input->post();
		if($postData['thought_txt'] !=''){
			$insertAry = array(
				'thought_text'=>$postData['thought_txt'],
				'thought_created'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("iconnect_thought",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_thought");
	}
	
	//thought of the day end 
	
	//knowledge base document start ------------------------------------------------------------------
	public function manage_archivefiles($archive_id = 0){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['archivefiles_data'] = $this->Admin_model->get_all_archivefiles($archive_id);
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($archive_id);
		$data['include'] = 'backend_iconnect/knowledgedocuments'; //
		$data['action_tab'] = 'archivefiles';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_archivefiles(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['archive_id']);
		$data['archivefiles_data'] = $this->Admin_model->get_archivefiles_by_id($postData['recd_id'],$postData['archive_id']);
		$this->load->view('backend_iconnect/knowledgedocuments_page',$data);
	}
	
	public function add_archivefiles(){
		$postData = $this->input->post();
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['archive_id']);
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/knowledgedocuments_page',$data);
	}
	public function view_archivefiles(){
		$postData = $this->input->post();
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['archive_id']);
		$data['action'] = 'view';
		$data['archivefiles_data'] = $this->Admin_model->get_archivefiles_by_id($postData['recd_id'],$postData['archive_id']);
		$this->load->view('backend_iconnect/knowledgedocuments_page',$data);
	}
	
	public function status_archivefiles(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'doc_file_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'doc_file_is_active'=>1
			);
		}
		$this->db->where("doc_file_id",$postData['recd_id']);
		$this->db->update("doc_files",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_archivefiles(){
		$postData = $this->input->post();
		$archivefiles_dir = 'uploads/archivefiles/';
		$file_name = '';
		$record_image = $_FILES['archivefiles_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['archivefiles_file']['name']);
			$file_archivefiles = "archivefile_" .$postData['archivefiles_name']. '_' .$ext[0]. "." . $ext[1];
			move_uploaded_file($_FILES['archivefiles_file']['tmp_name'], $archivefiles_dir . $file_archivefiles);
			$file_name = $file_archivefiles;
			
			$update_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_location'=>$file_name,
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where("doc_file_id",$postData['doc_file_id']);
			$this->db->update("doc_files",$update_ary);
		}else{
			$update_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where("doc_file_id",$postData['doc_file_id']);
			$this->db->update("doc_files",$update_ary);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_archivefiles/".$postData['archive_id']);
	}
	
	public function added_archivefiles(){
		$postData = $this->input->post();
		$archivefiles_dir = 'uploads/archivefiles/';
		$file_name = '';
		$record_image = $_FILES['archivefiles_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['archivefiles_file']['name']);
			//$file_archivefiles = "archivefiles_" . '_' . random_string('alnum', 20) . "." . $ext[1];
			$file_archivefiles = "archivefile_" .$postData['archivefiles_name']. '_' .$ext[0]. "." . $ext[1];
			move_uploaded_file($_FILES['archivefiles_file']['tmp_name'], $archivefiles_dir . $file_archivefiles);
			$file_name = $file_archivefiles;
			
			$insert_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_location'=>$file_name,
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("doc_files",$insert_ary);
		}else{
			$insert_ary = array(
				'doc_folder_id'=>$postData['archive_id'],
				'doc_file_name'=>$postData['archivefiles_name'],
				'doc_file_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("doc_files",$insert_ary);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_archivefiles/".$postData['archive_id']);
	}
	//knowledge base document end
	
	//knowledge archive start --------------------------------------------------------------------
	public function manage_archive(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['archive_data'] = $this->Admin_model->get_all_archive();
		$data['include'] = 'backend_iconnect/archive';
		$data['action_tab'] = 'archive';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_archive(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/archive_page',$data);
	}
	
	public function add_archive(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/archive_page',$data);
	}
	public function view_archive(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['archive_data'] = $this->Admin_model->get_archive_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/archive_page',$data);
	}
	
	public function status_archive(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'doc_folder_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'doc_folder_is_active'=>1
			);
		}
		$this->db->where("doc_folder_id",$postData['recd_id']);
		$this->db->update("doc_folder",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_archive(){
		$postData = $this->input->post();
		if($postData['archive_txt'] !=''){
			$updateAry = array(
				'doc_folder_txt'=>$postData['archive_txt'],
				'doc_folder_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where("doc_folder_id",$postData['archive_id']);
			$this->db->update("doc_folder",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_archive");
	}
	
	public function added_archive(){
		$postData = $this->input->post();
		if($postData['archive_txt'] !=''){
			$insertAry = array(
				'doc_folder_txt'=>$postData['archive_txt'],
				'doc_folder_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->insert("doc_folder",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_archive");
	}
	//knowledge archive end
	
//iconnect code end
	

//helpdesk code start 
	
	//guideline start --------------------------------------------------------------------------------
	public function manage_guideline(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['guideline_data'] = $this->Admin_model->get_all_guideline();
		$data['include'] = 'backend_iconnect/guideline_day';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_guideline(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['guideline_data'] = $this->Admin_model->get_guideline_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/guideline_page',$data);
	}
	
	public function add_guideline(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/guideline_page',$data);
	}
	public function view_guideline(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['guideline_data'] = $this->Admin_model->get_guideline_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/guideline_page',$data);
	}
	
	public function status_guideline(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'guideline_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'guideline_is_active'=>1
			);
		}
		$this->db->where("dept_guideline_hd_id",$postData['recd_id']);
		$this->db->update("dept_guideline_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_guideline(){
		$postData = $this->input->post();
		if($postData['guideline_txt'] !=''){
			$updateAry = array(
				'guideline_txt'=>$postData['guideline_txt']
			);
			$this->db->where("dept_guideline_hd_id",$postData['guid_id']);
			$this->db->update("dept_guideline_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_guideline");
	}
	
	public function added_guideline(){
		$postData = $this->input->post();
		if($postData['guideline_txt'] !=''){
			$insertAry = array(
				'guideline_txt'=>$postData['guideline_txt']
			);
			$this->db->insert("dept_guideline_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_guideline");
	}
	//guideline end 
	
	//dept manage start  -------------------------------------------------------------
	public function manage_department(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['department_data'] = $this->Admin_model->get_all_department();
		$data['include'] = 'backend_iconnect/department';
		$data['action_tab'] = 'department';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_department(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['department_data'] = $this->Admin_model->get_department_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/department_page',$data);
	}
	
	public function add_department(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/department_page',$data);
	}
	
	public function view_department(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['department_data'] = $this->Admin_model->get_department_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/department_page',$data);
	}
	
	public function status_department(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'dept_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'dept_is_active'=>1
			);
		}
		$this->db->where("dept_id",$postData['recd_id']);
		$this->db->update("dept_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_department(){
		$postData = $this->input->post();
		if($postData['department_txt'] !=''){
			$updateAry = array(
				'dept_name'=>$postData['department_txt']
			);
			$this->db->where("dept_id",$postData['dept_id']);
			$this->db->update("dept_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_department");
	}
	
	public function added_department(){
		$postData = $this->input->post();
		if($postData['department_txt'] !=''){
			$insertAry = array(
				'dept_name'=>$postData['department_txt']
			);
			$this->db->insert("dept_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_department");
	}
	//dept manage end
	
	//category manage start -----------------------------------------------------------
	public function manage_category(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['category_data'] = $this->Admin_model->get_all_category();
		$data['include'] = 'backend_iconnect/category';
		$data['action_tab'] = 'category';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_category(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['category_data'] = $this->Admin_model->get_category_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/category_page',$data);
	}
	
	public function add_category(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/category_page',$data);
	}
	
	public function view_category(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['category_data'] = $this->Admin_model->get_category_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/category_page',$data);
	}
	
	public function status_category(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'category_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'category_is_active'=>1
			);
		}
		$this->db->where("category_id",$postData['recd_id']);
		$this->db->update("category_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_category(){
		$postData = $this->input->post();
		if($postData['category_txt'] !=''){
			$updateAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_name'=>$postData['category_txt']
			);
			$this->db->where("category_id",$postData['cat_id']);
			$this->db->update("category_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_category");
	}
	
	public function added_category(){
		$postData = $this->input->post();
		if($postData['category_txt'] !=''){
			$insertAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_name'=>$postData['category_txt']
			);
			$this->db->insert("category_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_category");
	}
	//category end
	
	//sub category manage start -------------------------------------------------------
	public function manage_subcategory(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['subcategory_data'] = $this->Admin_model->get_all_subcategory();
		$data['include'] = 'backend_iconnect/subcategory';
		$data['action_tab'] = 'subcategory';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_subcategory(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['subcategory_data'] = $this->Admin_model->get_subcategory_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/subcategory_page',$data);
	}
	
	public function add_subcategory(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/subcategory_page',$data);
	}
	
	public function view_subcategory(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['subcategory_data'] = $this->Admin_model->get_subcategory_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/subcategory_page',$data);
	}
	
	public function status_subcategory(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'sub_category_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'sub_category_is_active'=>1
			);
		}
		$this->db->where("sub_category_id",$postData['recd_id']);
		$this->db->update("sub_category_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_subcategory(){
		$postData = $this->input->post();
		if($postData['subcategory_txt'] !=''){
			$updateAry = array(
				'category_id'=>$postData['cat_id'],
				'sub_category_name'=>$postData['subcategory_txt']
			);
			$this->db->where("sub_category_id",$postData['subcat_id']);
			$this->db->update("sub_category_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_subcategory");
	}
	
	public function added_subcategory(){
		$postData = $this->input->post();
		if($postData['subcategory_txt'] !=''){
			$insertAry = array(
				'category_id'=>$postData['cat_id'],
				'sub_category_name'=>$postData['subcategory_txt']
			);
			$this->db->insert("sub_category_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_subcategory");
	}
	//sub category manage end
	
	//items manage start --------------------------------------------------------------
	public function manage_item(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['item_data'] = $this->Admin_model->get_all_item();
		$data['include'] = 'backend_iconnect/item';
		$data['action_tab'] = 'item';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_item(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['item_data'] = $this->Admin_model->get_item_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/item_page',$data);
	}
	
	public function add_item(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/item_page',$data);
	}
	
	public function view_item(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['item_data'] = $this->Admin_model->get_item_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/item_page',$data);
	}
	
	public function status_item(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'item_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'item_is_active'=>1
			);
		}
		$this->db->where("items_id",$postData['recd_id']);
		$this->db->update("items_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_item(){
		$postData = $this->input->post();
		if($postData['item_txt'] !=''){
			$updateAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_id'=>$postData['cat_id'],
				'sub_category_id'=>$postData['subcat_id'],
				'item_name'=>$postData['item_txt']
			);
			$this->db->where("items_id",$postData['item_id']);
			$this->db->update("items_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_item");
	}
	
	public function added_item(){
		$postData = $this->input->post();
		if($postData['item_txt'] !=''){
			$insertAry = array(
				'dept_id'=>$postData['dept_id'],
				'category_id'=>$postData['cat_id'],
				'sub_category_id'=>$postData['subcat_id'],
				'item_name'=>$postData['item_txt']
			);
			$this->db->insert("items_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_item");
	}
	//item manage end
	
	
	//quick link start --------------------------------------------------------------
	public function manage_quicklink(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['quicklink_data'] = $this->Admin_model->get_all_quicklink();
		$data['include'] = 'backend_iconnect/quicklink';
		$data['action_tab'] = 'quicklink';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_quicklink(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['quicklink_data'] = $this->Admin_model->get_quicklink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/quicklink_page',$data);
	}
	
	public function add_quicklink(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/quicklink_page',$data);
	}
	
	public function view_quicklink(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['quicklink_data'] = $this->Admin_model->get_quicklink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/quicklink_page',$data);
	}
	
	public function status_quicklink(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'quick_line_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'quick_line_is_active'=>1
			);
		}
		$this->db->where("quick_link_id",$postData['recd_id']);
		$this->db->update("quick_link_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_quicklink(){
		$postData = $this->input->post();
		if($postData['quicklink_name'] !=''){
			$updateAry = array(
				'quick_link_name'=>$postData['quicklink_name'],
				'quick_link_url'=>$postData['quicklink_url'],
				'quick_link_desc'=>$postData['quicklink_desc']
			);
			$this->db->where("quick_link_id",$postData['quicklink_id']);
			$this->db->update("quick_link_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_quicklink");
	}
	
	public function added_quicklink(){
		$postData = $this->input->post();
		if($postData['quicklink_name'] !=''){
			$insertAry = array(
				'quick_link_name'=>$postData['quicklink_name'],
				'quick_link_url'=>$postData['quicklink_url'],
				'quick_link_desc'=>$postData['quicklink_desc']
			);
			$this->db->insert("quick_link_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_quicklink");
	}
	//quick link end
	
	
	//tech groups start --------------------------------------------------------------
	public function manage_group(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['group_data'] = $this->Admin_model->get_all_group();
		$data['include'] = 'backend_iconnect/groups';
		$data['action_tab'] = 'groups';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_group(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['group_data'] = $this->Admin_model->get_group_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/group_page',$data);
	}
	
	public function add_group(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/group_page',$data);
	}
	
	public function view_group(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['group_data'] = $this->Admin_model->get_group_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/group_page',$data);
	}
	
	public function status_group(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'group_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'group_is_active'=>1
			);
		}
		$this->db->where("group_id",$postData['recd_id']);
		$this->db->update("groups_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_group(){
		$postData = $this->input->post();
		if($postData['group_txt'] !=''){
			$updateAry = array(
				'group_name'=>$postData['group_txt']
			);
			$this->db->where("group_id",$postData['group_id']);
			$this->db->update("groups_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_group");
	}
	
	public function added_group(){
		$postData = $this->input->post();
		if($postData['group_txt'] !=''){
			$insertAry = array(
				'group_name'=>$postData['group_txt']
			);
			$this->db->insert("groups_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_group");
	}
	//tech group end
	
	
	//add person to technician list start --------------------------------------------------------------
	public function manage_technician(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['technician_data'] = $this->Admin_model->get_all_technician();
		$data['include'] = 'backend_iconnect/technicians';
		$data['action_tab'] = 'technicians';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_technician(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['technician_data'] = $this->Admin_model->get_technician_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/technician_page',$data);
	}
	
	public function add_technician(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/technician_page',$data);
	}
	
	public function view_technician(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['technician_data'] = $this->Admin_model->get_technician_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/technician_page',$data);
	}
	
	public function status_technician(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'technician_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'technician_is_active'=>1
			);
		}
		$this->db->where("technician_id",$postData['recd_id']);
		$this->db->update("technician_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_technician(){
		$postData = $this->input->post();
		if($postData['tech_txt'] !=''){
			$techData = explode("~",$postData['tech_txt']);
			if($techData[2]!=''){
				$empCode = $techData[2];
			}else{
				$empCode = 'SGT'.rand(11,99);
			}
			$updateAry = array(
				'tech_code'=>$empCode,
				'technician_email'=>$techData[0],
				'tech_name'=>$techData[1]
			);
			$this->db->where("technician_id",$postData['tech_id']);
			$this->db->update("technician_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_technician");
	}
	
	public function added_technician(){
		$postData = $this->input->post();
		if($postData['tech_txt'] !=''){
			$techData = explode("~",$postData['tech_txt']);
			if($techData[2]!=''){
				$empCode = $techData[2];
			}else{
				$empCode = 'SGT'.rand(11,99);
			}
			$insertAry = array(
				'tech_code'=>$empCode,
				'technician_email'=>$techData[0],
				'tech_name'=>$techData[1]
			);
			$this->db->insert("technician_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_technician");
	}
	//add person to technician list end
	
	
	//tech group assign start --------------------------------------------------------------
	public function manage_assigntech(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['assigntech_data'] = $this->Admin_model->get_all_assigntech();
		$data['include'] = 'backend_iconnect/assigntech';
		$data['action_tab'] = 'assigntech';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_assigntech(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['assigntech_data'] = $this->Admin_model->get_assigntech_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assigntech_page',$data);
	}
	
	public function add_assigntech(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/assigntech_page',$data);
	}
	
	public function view_assigntech(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['assigntech_data'] = $this->Admin_model->get_assigntech_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assigntech_page',$data);
	}
	
	public function status_assigntech(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'technician_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'technician_is_active'=>1
			);
		}
		$this->db->where("technician_group_map_id",$postData['recd_id']);
		$this->db->update("technician_group_map_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_assigntech(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$updateAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->where("technician_group_map_id",$postData['assign_id']);
			$this->db->update("technician_group_map_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_assigntech");
	}
	
	public function added_assigntech(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$insertAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->insert("technician_group_map_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_assigntech");
	}
	//tech group assign end
	
	
	
	
	//important links start
	public function manage_implink(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['implink_data'] = $this->Admin_model->get_all_implink();
		$data['include'] = 'backend_iconnect/implink';
		$data['action_tab'] = 'implink';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_implink(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['implink_data'] = $this->Admin_model->get_implink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/implink_page',$data);
	}
	
	public function add_implink(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/implink_page',$data);
	}
	
	public function view_implink(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['implink_data'] = $this->Admin_model->get_implink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/implink_page',$data);
	}
	
	public function status_implink(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'implink_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'implink_is_active'=>1
			);
		}
		$this->db->where("implink_id",$postData['recd_id']);
		$this->db->update("implinks",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_implink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$updateAry = array(
				'implink_name'=>$postData['implink_name'],
				'implink_url'=>$postData['implink_url']
			);
			$this->db->where("implink_id",$postData['implink_id']);
			$this->db->update("implinks",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_implink");
	}
	
	public function added_implink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$insertAry = array(
				'implink_name'=>$postData['implink_name'],
				'implink_url'=>$postData['implink_url']
			);
			$this->db->insert("implinks",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_implink");
	}
	//important links end
	
	
	//group and item map start ----------------------------------------------------------------------------
	public function manage_assignitem(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['assignitem_data'] = $this->Admin_model->get_all_assignitem();
		$data['include'] = 'backend_iconnect/assignitem';
		$data['action_tab'] = 'assignitem';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_assignitem(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['assignitem_data'] = $this->Admin_model->get_assignitem_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assignitem_page',$data);
	}
	
	public function add_assignitem(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/assignitem_page',$data);
	}
	
	public function view_assignitem(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['assignitem_data'] = $this->Admin_model->get_assignitem_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/assignitem_page',$data);
	}
	
	public function status_assignitem(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'group_item_map_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'group_item_map_is_active'=>1
			);
		}
		$this->db->where("group_item_map_id",$postData['recd_id']);
		$this->db->update("group_item_map_hd",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_assignitem(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$updateAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->where("technician_group_map_id",$postData['assign_id']);
			$this->db->update("technician_group_map_hd",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_assignitem");
	}
	
	public function added_assignitem(){
		$postData = $this->input->post();
		if($postData['group_id'] !='' && $postData['tech_id'] !=''){
			$insertAry = array(
				'technician_id'=>$postData['tech_id'],
				'group_id'=>$postData['group_id']
			);
			$this->db->insert("technician_group_map_hd",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_assignitem");
	}
	//group and item map end ----------------------------------------------------------------------------
	
	
	//dept announcements start --------------------------------------------------------------------------
	public function manage_dept_announcement(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['deptannouncement_data'] = $this->Admin_model->get_all_deptannouncement();
		$data['include'] = 'backend_iconnect/deptannouncement';
		$data['action_tab'] = 'deptannouncement';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function edit_dept_announcement(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['deptannouncement_data'] = $this->Admin_model->get_deptannouncement_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/deptannouncement_page',$data);
	}
	
	public function add_dept_announcement(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/deptannouncement_page',$data);
	}
	public function view_dept_announcement(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['deptannouncement_data'] = $this->Admin_model->get_deptannouncement_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/deptannouncement_page',$data);
	}
	
	public function status_dept_announcement(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'announcement_is_active'=>0
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'announcement_is_active'=>1
			);
		}
		$this->db->where("announcement_id",$postData['recd_id']);
		$this->db->update("announcements_hd;",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_dept_announcement(){
		$postData = $this->input->post();
		if($postData['announcement_name'] !='' && $postData['dept_id'] > 0){
			$updateAry = array(
				'dept_id'=>$postData['dept_id'],
				'announcement_name'=>$postData['announcement_name'],
				'announcement_desc'=>$postData['announcement_desc'],
				'announcements_attach'=>$attachName,
				'start_dt'=>date('Y-m-d H:i:s',strtotime($postData['st_date'])),
				'end_dt'=>date('Y-m-d H:i:s',strtotime($postData['ed_date']))
			);
			$this->db->where("announcement_id",$postData['announce_id']);
			$this->db->update("announcements_hd;",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_dept_announcement");
	}
	
	public function added_dept_announcement(){
		$postData = $this->input->post();
		if($postData['announcement_name'] !='' && $postData['dept_id'] > 0){
			$insertAry = array(
				'dept_id'=>$postData['dept_id'],
				'announcement_name'=>$postData['announcement_name'],
				'announcement_desc'=>$postData['announcement_desc'],
				'announcements_attach'=>$attachName,
				'start_dt'=>date('Y-m-d H:i:s',strtotime($postData['st_date'])),
				'end_dt'=>date('Y-m-d H:i:s',strtotime($postData['ed_date']))
			);
			$this->db->insert("announcements_hd;",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_dept_announcement");
	}
	//dept announcements end ----------------------------------------------------------------------------
	
	
	//IP links start
	public function manage_iplink(){
		$data = $this->get_ledap();
		$data['msg'] = $this->session->flashdata('message_submit');
		$data['iplink_data'] = $this->Admin_model->get_all_iplink();
		$data['include'] = 'backend_iconnect/iplink';
		$data['action_tab'] = 'iplink';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function update_iplink(){
		$data = $this->get_ledap();
		$data['iplink_data'] = $this->Admin_model->get_all_iplink();
		$this->load->view('backend_iconnect/iplink_ajx',$data);
	}
	
	public function edit_iplink(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['iplink_data'] = $this->Admin_model->get_iplink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/iplink_page',$data);
	}
	
	public function add_iplink(){
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/iplink_page',$data);
	}
	
	public function view_iplink(){
		$postData = $this->input->post();
		$data['action'] = 'view';
		$data['iplink_data'] = $this->Admin_model->get_iplink_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/iplink_page',$data);
	}
	
	public function status_iplink(){
		$postData = $this->input->post();
		if($postData['status'] == 'deactive'){
			$update_ary = array(
				'ip_email_shoot'=>1
			);
		}
		if($postData['status'] == 'active'){
			$update_ary = array(
				'ip_email_shoot'=>0
			);
			$updateAry2 = array(
				'link_down_count'=>0
			);
			$this->db->where("ip_monitor_id",$postData['recd_id']);
			$this->db->update("ip_monitor",$updateAry2);
		}
		$this->db->where("ip_monitor_id",$postData['recd_id']);
		$this->db->update("ip_monitor",$update_ary);
		echo 'Status changed successfully';
	}
	
	public function edited_iplink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$updateAry = array(
				'ip_number'=>$postData['implink_name'],
				'ip_email_id'=>$postData['implink_url']
			);
			$this->db->where("ip_monitor_id",$postData['iplink_id']);
			$this->db->update("ip_monitor",$updateAry);
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/manage_iplink");
	}
	
	public function added_iplink(){
		$postData = $this->input->post();
		if($postData['implink_name'] !=''){
			$insertAry = array(
				'ip_number'=>$postData['implink_name'],
				'ip_email_id'=>$postData['implink_url']
			);
			$this->db->insert("ip_monitor",$insertAry);
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/manage_iplink");
	}
	//IP links end
	
	
	
//helpdesk code end
	
	
	//ldap data 
	public function get_ledap(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		return $data;
	}
	
	//<<NEW CODE >> Twice a day 
	public function cron_job_update_ladp(){
		//ini_set('max_execution_time', 300000);
		$server = ldap_serverhost;
        $basedn = ldap_basedn;
		$handle = sysu;
		$password = sysp;
        $dn = ldap_basedn . "\\" . $handle;
        $connect = ldap_connect($server);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
		
		//var_dump(ldap_bind($connect, $dn, $password));
        if ($ds = @ldap_bind($connect, $dn, $password)) {
            //$filters = "(samaccountname=$handle)";
			
            $filters = "(cn=*)"; // get all results
            $results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
            $entries = ldap_get_entries($connect, $results);
			//echo '<pre>';
			//echo 'count of entities are '.count($entries);
			//exit; die;
            if(count($entries) > 0){
				for ($i = 0; $i < count($entries); $i++) {
					$ldap_userinfo = array(
						//'usnchanged' => $entries[$i]["usnchanged"][0],
						'samaccountname' => $entries[$i]["samaccountname"][0],
						'unique_id'=> base64_encode($entries[$i]["objectguid"][0]),
						'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
						'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
						'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
						'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
						'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
						'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
						'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
						'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
						'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
						'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
						'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
					);
					//print_r($ldap_userinfo);
					
					if($ldap_userinfo['mail'] !='' || $ldap_userinfo['employeeid'] !=''){
						$lognSql = "select * from ldapusers where unique_id='" .$ldap_userinfo['unique_id']. "' ";
						$local_result = $this->Base_model->run_query($lognSql);
						if (count($local_result) == 0) {
							$local_user_insert['unique_id'] = $ldap_userinfo['unique_id'];
							$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
							$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
							$local_user_insert['u_name'] = $ldap_userinfo['name'];
							$local_user_insert['u_email'] = $ldap_userinfo['mail'];
							$local_user_insert['u_designation'] = $ldap_userinfo['description'];
							$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
							$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
							
							$local_user_insert['u_image'] = NULL;
							$local_user_insert['u_cover_image'] = NULL;
							
							$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
							$local_user_insert['u_status'] = 1;
							$local_user_insert['u_role'] = 0;
							$this->Base_model->insert_operation($local_user_insert, 'ldapusers');
						} else {
							$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
							$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
							$local_user_insert['u_name'] = $ldap_userinfo['name'];
							$local_user_insert['u_email'] = $ldap_userinfo['mail'];
							$local_user_insert['u_designation'] = $ldap_userinfo['description'];
							$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
							$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
							
							$local_user_insert['u_image'] = NULL;
							$local_user_insert['u_cover_image'] = NULL;
							
							$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
							$local_user_insert['u_status'] = 1;
							$local_user_insert['u_role'] = 0;
							
							$logi_where['unique_id'] = $ldap_userinfo['unique_id'];
							$this->Base_model->update_operation($local_user_insert, 'ldapusers', $logi_where);
						}
						
					}
				}
			}
		}
	}
	
	//Outlook emails
	
	public function manage_outlookemail()
	{	
		$data = $this->get_ledap();
		$data['my_title']  = 'Outlook Emails';
		$data['emailData'] =  $this->Dailytask_model->get_outlookemail();
		$data['action_tab'] = 'Outlook_mails';
		$data['include'] = 'backend_iconnect/outlook_emails';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	
	// daily task code start************************************************
	public function manage_dailytask()
	{	
		$data = $this->get_ledap();
		$data['my_title']  = 'Daily task';
		$data['all_tasks'] =  $this->Dailytask_model->get_dailytask($this->user_ldamp);
		$data['action_tab'] = 'daily_task';
		$data['include'] = 'backend_iconnect/daily_task1';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function daily_task_report1(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'daily_task_report';
		$data['report_data'] = json_encode($this->Dailytask_model->get_employee_report(),true);
		$data['include'] = 'backend_iconnect/daily_task_report';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function daily_task_report_dep(){
		$data = $this->get_ledap();
		$postData = $this->input->post();
		$data['action_tab'] = 'daily_task_report';
		$data['post_type'] = (isset($postData['filter_type'])) ? $postData['filter_type'] : 'by_date';
		$data['filter_date'] = (isset($postData['filter_date'])) ? $postData['filter_date'] : date('d-m-Y');
		$data['report_data'] = json_encode($this->Dailytask_model->get_employee_report1($data['post_type'],$data['filter_date']),true);
		$data['include'] = 'backend_iconnect/daily_task_report_dup';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	/*
	public function downloadReport($customReport=''){
		$postData = $this->input->post();
	    $contents ='"Sr No","Employee Name","Employee Code","Project Name","Start Date","End Date","Task activity","Task priority","Task status","Working time","Remarks"';
	    $contents .="\n";
		$i = 1;
	    foreach($customReport as $taskCRecord){
			$all_tasks = $this->Dailytask_model->get_dailytask1($taskCRecord['emp_code'],$filter_date);
			foreach($all_tasks as $task){				
				$contents.='"'.$i.'","'.$taskCRecord['u_name'].'","'.$taskCRecord['emp_code'].'","'.$task['project_name'].'","'.date('d-m-Y',strtotime($task['start_date'])).'","'.date('d-m-Y',strtotime($task['end_date'])).'","'.ucfirst($task['task_activity']).'","'.ucfirst($task['task_priority']).'","'.ucfirst($task['remarks']).'"';
				$contents.="\n";
				$i++;
			}
	    }
	    $contents = strip_tags($contents);
	    Header("Content-Disposition: attachment;filename=dailytask_report.csv ");
	    print $contents;
	    exit;
	}
	*/
	
	public function get_employee_tasks($emp_code="",$filter_date=""){
		if ($this->input->is_ajax_request()) {
			if($filter_date !=''){
				$data['all_tasks'] =  $this->Dailytask_model->get_dailytask1($emp_code,$filter_date);
			}else{
				$data['all_tasks'] =  $this->Dailytask_model->get_dailytask1($emp_code);
			}
			$this->load->view('backend_iconnect/daily_task_report_ajax',$data);
		}
	}
	
	public function save_task()
	{
		$postData = $this->input->post();
		if(!empty($postData)){
			if($this->Dailytask_model->save_dailytask($this->user_ldamp)){
			
				$this->session->set_flashdata('msg','Task Saved Successfully.');
				redirect('admin/manage_dailytask');
			}
			else{
				$this->session->set_flashdata('error_msg','An error occured while Saving...');
				redirect('admin/manage_dailytask');
			}
		}
		else{
			$this->session->set_flashdata('error_msg','An error occured while Saving...');
			redirect('admin/manage_dailytask');
		}
	}
	
	public function sendDailyTaskEmail($fromName='',$toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);
  C:\Users\Administrator\Desktop\Mayur\Iconnect\iconnect_bk\2019-12-17\application\controllers\admin.php (91 hi
		$mail = '';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name=$fromName;
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('ravi.kumar@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	// daily task code end************************************************	
	
	
	//inventory code start ***********************************************
	public function asset_lob_report(){
		$data = $this->get_ledap();
		$data['LOB_data'] = getReportData_new('getLOB');
		
		$data['asset_data'] = getReportData_new('getAssetype');
		
		$data['include'] = 'backend_iconnect/inventory/asset_lob_report';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function showform(){
		$data['include'] = 'backend_iconnect/inventory_angulr';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function add_asset(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/inventory/add_new_asset';
		$data['asset_code'] = date('dmYImi');
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function edit_asset($assetId=0){
		$data = $this->get_ledap();
		$data['asset_data'] = $this->Admin_model->get_asset_by_id($assetId);
		$data['include'] = 'backend_iconnect/inventory/edit_new_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function addAsset(){
		$this->load->model('Inventory_model');
		$postData = $this->input->post();
		$assetAry = array(
			'asset_type'=>$postData['asset_type'],
			'system_ref_code'=>$postData['system_ref_code'],
			'asset_system_generated_code'=>$postData['system_generated_code'],
			'asset_name'=>$postData['asset_name'],
			'present_asset_code'=>$postData['present_asset_code'],
			'nav_code'=>$postData['nav_asset_code'],
			'procured_date'=>date('Y-m-d I:m:s',strtotime($postData['procured_dt'])),
			'received_date'=>date('Y-m-d I:m:s',strtotime($postData['received_dt'])),
			'approved_lob_hod_emp_code'=>$postData['approved_hod'],
			'procured_lob_emp_code'=>$postData['procured_lob'],
			'procured_company'=>$postData['procured_company'],
			'procured_state'=>$postData['procured_state'],
			'procured_location'=>$postData['procured_location'],
			'manufacturer'=>$postData['manufacturer'],
			'modal_no'=>$postData['model_no'],
			'expiry_date'=>date('Y-m-d I:m:s',strtotime($postData['expiry_dt'])),
			'serial_number'=>$postData['serial_no'],
			'warranty_expiry_no_month'=>$postData['warranty_expire_month'],
			'warranty_type'=>$postData['warranty_type'],
			'description'=>$postData['description'],
			'warranty_expiry_date'=>date('Y-m-d I:m:s',strtotime($postData['warranty_expire_dt'])),
			'asset_created_date'=>date('Y-m-d I:m:s'),
			'asset_remark'=>$postData['remark'],
			'asset_stock_type'=>'Stock',
			'po_created'=>date('Y-m-d H:i:s',strtotime($postData['po_created'])),
			'po_numr'=>$postData['po_number'],
			'vendor_name'=>$postData['po_name']
		);
		if($this->Admin_model->add_asset($assetAry)){
			//for sub inventory items
			$insert_id = $this->db->insert_id();
			if(count($postData['key_id'])> 0){
				for($i=0;$i<count($postData['key_id']);$i++){
					$sub_inv_ary = array(
						'asset_id'=>$insert_id,
						'key_id'=>$postData['key_id'][$i],
						'processor'=>$postData['processor'][$i],
						'ram'=>$postData['ram_id'][$i],
						'hdd'=>$postData['hdd_id'][$i],
						'os_name'=>$postData['os_id'][$i],
						'os_key'=>$postData['os_key'][$i],
						'ms_office_name'=>$postData['msoffice_id'][$i],
						'ms_office_key'=>$postData['msoffice_key'][$i],
						'po_created'=>date('Y-m-d',strtotime($postData['po_created'][$i])),
						'po_number'=>$postData['po_number'][$i],
						'po_name'=>$postData['po_name'][$i],
					);
					$this->db->insert('inv_sub_asset',$sub_inv_ary);
					$insert_subasset_id = $this->db->insert_id();
					$sub_asset_history = array(
						'inv_sub_asset'=>$insert_subasset_id,
						'asset_id'=>$insert_id,
						'key_id'=>$postData['key_id'][$i],
						'processor'=>$postData['processor'][$i],
						'ram'=>$postData['ram_id'][$i],
						'hdd'=>$postData['hdd_id'][$i],
						'os_name'=>$postData['os_id'][$i],
						'os_key'=>$postData['os_key'][$i],
						'ms_office_name'=>$postData['msoffice_id'][$i],
						'ms_office_key'=>$postData['msoffice_key'][$i],
						'po_created'=>date('Y-m-d',strtotime($postData['po_created'][$i])),
						'po_number'=>$postData['po_number'][$i],
						'po_name'=>$postData['po_name'][$i],
					);
					$this->Inventory_model->add_inv_sub_asset_history($sub_asset_history);
				}
			}
			$this->session->set_flashdata('msg','Asset added successfully');
			 
	# Add history in inv_add_asset_history table 
			$add_history = array(
			      'asset_id'=>$insert_id,
				  'action_user_code'=>$this->user_ldamp['employeeid'],
				  'action_type'=>'ADD',
				  'asset_type'=>$postData['asset_type'],
				  'asset_system_generated_code'=>$postData['system_generated_code'],
				  'system_ref_code'=>$postData['system_ref_code'],
				  'present_asset_code'=>$postData['present_asset_code'],
				  'nav_code'=>$postData['nav_asset_code'],
				  'asset_name'=>$postData['asset_name'],
				  'procured_date'=>date('Y-m-d I:m:s',strtotime($postData['procured_dt'])),
				  'received_date'=>date('Y-m-d I:m:s',strtotime($postData['received_dt'])),
				  'approved_lob_hod_emp_code'=>$postData['approved_hod'],
				  'procured_lob_emp_code'=>$postData['procured_lob'],
				  'procured_company'=>$postData['procured_company'],
				  'procured_state'=>$postData['procured_state'],
				  'procured_location'=>$postData['procured_location'],
				  'manufacturer'=>$postData['manufacturer'],
				  'modal_no'=>$postData['model_no'],
				  'expiry_date'=>date('Y-m-d I:m:s',strtotime($postData['expiry_dt'])),
				  'serial_number'=>$postData['serial_no'],
				  'warranty_expiry_no_month'=>$postData['warranty_expire_month'],
				  'warranty_type'=>$postData['warranty_type'],
				  'description'=>$postData['description'],
				  'warranty_expiry_date'=>date('Y-m-d I:m:s',strtotime($postData['warranty_expire_dt'])),
				  'asset_remark'=>$postData['remark'],
				  'asset_created_date'=>date('Y-m-d I:m:s'),
				  'asset_stock_type'=>'Stock',
				  'po_created'=>date('Y-m-d H:i:s',strtotime($postData['po_created'])),
				  'po_numr'=>$postData['po_number'][$i],
				  'vendor_name'=>$postData['po_name']
			);
			
			//$this->dd($add_history); 
			
			$this->Inventory_model->add_inv_asset_history($add_history);
			
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manage_asset');
	}
	
	public function editAsset($assetId=0){
		$this->load->model('Inventory_model');
		$postData = $this->input->post();
		if($postData['asset_stock_location']!=''){
			$stock_location = $postData['asset_stock_location'];
		}else{
			$stock_location = '';
		}
		if($postData['custodian_name']!=''){
			$custodian_name = $postData['custodian_name'];
		}else{
			$custodian_name = '';
		}
		$assetAry = array(
			'asset_type'=>$postData['asset_type'],
			//'asset_system_generated_code'=>$postData['system_generated_code'],
			'asset_name'=>$postData['asset_name'],
			'present_asset_code'=>$postData['present_asset_code'],
			'nav_code'=>$postData['nav_asset_code'],
			'procured_date'=>date('Y-m-d H:i:s',strtotime($postData['procured_dt'])),
			'received_date'=>date('Y-m-d H:i:s',strtotime($postData['received_dt'])),
			'approved_lob_hod_emp_code'=>$postData['approved_hod'],
			'procured_lob_emp_code'=>$postData['procured_lob'],
			'procured_company'=>$postData['procured_company'],
			'procured_state'=>$postData['procured_state'],
			'procured_location'=>$postData['procured_location'],
			'manufacturer'=>$postData['manufacturer'],
			'modal_no'=>$postData['model_no'],
			'expiry_date'=>date('Y-m-d H:i:s',strtotime($postData['expiry_dt'])),
			'serial_number'=>$postData['serial_no'],
			'warranty_expiry_no_month'=>$postData['warranty_expire_month'],
			'warranty_type'=>$postData['warranty_type'],
			'description'=>$postData['description'],
			'warranty_expiry_date'=>date('Y-m-d H:i:s',strtotime($postData['warranty_expire_dt'])),
			'asset_remark'=>$postData['remark'],
			'po_created'=>date('Y-m-d H:i:s',strtotime($postData['po_created'])),
			'po_numr'=>$postData['po_number'],
			'vendor_name'=>$postData['po_name'],
			'asset_stock_location'=>$stock_location,
			'custodian_name'=>$custodian_name
		);
		if($this->Admin_model->edit_asset($assetAry,$assetId)){
			$deactive_sub_ast = array(
				'inv_sub_asset_is_active'=>0
			);
			$this->db->where('asset_id', $assetId);
			$this->db->update('inv_sub_asset',$deactive_sub_ast);
			if(count($postData['key_id'])>0 || count($postData['processor'])> 0 || count($postData['ram_id'])>0 || count($postData['hdd_id'])>0 || count($postData['os_id'])>0 || count($postData['os_key'])>0 || count($postData['msoffice_id'])>0 || count($postData['msoffice_key'])>0){
				for($i=0;$i<count($postData['key_id']);$i++){
					$sub_inv_ary = array(
						'asset_id'=>$assetId,
						'key_id'=>$postData['key_id'][$i],
						'processor'=>$postData['processor'][$i],
						'ram'=>$postData['ram_id'][$i],
						'hdd'=>$postData['hdd_id'][$i],
						'os_name'=>$postData['os_id'][$i],
						'os_key'=>$postData['os_key'][$i],
						'ms_office_name'=>$postData['msoffice_id'][$i],
						'ms_office_key'=>$postData['msoffice_key'][$i],
						'inv_sub_asset_is_active'=>1,
						'po_created'=>date('Y-m-d'),
						'po_number'=>0,
						'po_name'=>0
					);
					if($postData['asset_sub_id'][$i] > 0){
						$this->db->where('inv_sub_asset',$postData['asset_sub_id'][$i]);
						$this->db->update('inv_sub_asset',$sub_inv_ary);
						
						
						$add_sub_asset_history = array(
						'asset_id'=>$assetId,
						'inv_sub_asset'=>$postData['asset_sub_id'][$i],
						'key_id'=>$postData['key_id'][$i],
						'processor'=>$postData['processor'][$i],
						'ram'=>$postData['ram_id'][$i],
						'hdd'=>$postData['hdd_id'][$i],
						'os_name'=>$postData['os_id'][$i],
						'os_key'=>$postData['os_key'][$i],
						'ms_office_name'=>$postData['msoffice_id'][$i],
						'ms_office_key'=>$postData['msoffice_key'][$i],
						'inv_sub_asset_is_active'=>1,
						'po_created'=>date('Y-m-d'),
						'po_number'=>0,
						'po_name'=>0
						);
						//$this->dd($add_sub_asset_history);
						$this->db->insert('inv_sub_asset_history',$add_sub_asset_history);
						
						
					}else{
						$this->db->insert('inv_sub_asset',$sub_inv_ary);
						$insert_subasset_id = $this->db->insert_id();
						
						$add_sub_asset_history = array(
						'asset_id'=>$assetId,
						'inv_sub_asset'=>$insert_subasset_id,
						'key_id'=>$postData['key_id'][$i],
						'processor'=>$postData['processor'][$i],
						'ram'=>$postData['ram_id'][$i],
						'hdd'=>$postData['hdd_id'][$i],
						'os_name'=>$postData['os_id'][$i],
						'os_key'=>$postData['os_key'][$i],
						'ms_office_name'=>$postData['msoffice_id'][$i],
						'ms_office_key'=>$postData['msoffice_key'][$i],
						'inv_sub_asset_is_active'=>1,
						'po_created'=>date('Y-m-d'),
						'po_number'=>0,
						'po_name'=>0
						);
						//$this->dd($add_sub_asset_history);
						$this->db->insert('inv_sub_asset_history',$add_sub_asset_history);
						
						
					}
				}
				$this->db->where('inv_sub_asset_is_active', 0);
				$this->db->delete('inv_sub_asset');
			}
			$this->session->set_flashdata('msg','Asset edited successfully');
			
			# Edit asset : Enrty in inv_add_asset_history table
			$edit_asset_history = array(
			      'asset_id'=>$assetId,
				  'action_user_code'=>$this->user_ldamp['employeeid'],
				  'action_type'=>'EDIT',
				  'asset_type'=>$postData['asset_type'],
				  //'system_ref_code'=>$postData['system_ref_code'],
				  'present_asset_code'=>$postData['present_asset_code'],
				  'nav_code'=>$postData['nav_asset_code'],
				  'asset_name'=>$postData['asset_name'],
				  'procured_date'=>date('Y-m-d I:m:s',strtotime($postData['procured_dt'])),
				  'received_date'=>date('Y-m-d I:m:s',strtotime($postData['received_dt'])),
				  'approved_lob_hod_emp_code'=>$postData['approved_hod'],
				  'procured_lob_emp_code'=>$postData['procured_lob'],
				  'procured_company'=>$postData['procured_company'],
				  'procured_state'=>$postData['procured_state'],
				  'procured_location'=>$postData['procured_location'],
				  'manufacturer'=>$postData['manufacturer'],
				  'modal_no'=>$postData['model_no'],
				  'expiry_date'=>date('Y-m-d I:m:s',strtotime($postData['expiry_dt'])),
				  'serial_number'=>$postData['serial_no'],
				  'warranty_expiry_no_month'=>$postData['warranty_expire_month'],
				  'warranty_type'=>$postData['warranty_type'],
				  'description'=>$postData['description'],
				  'warranty_expiry_date'=>date('Y-m-d I:m:s',strtotime($postData['warranty_expire_dt'])),
				  'asset_remark'=>$postData['remark'],
				  'asset_created_date'=>date('Y-m-d I:m:s'),
				  //'asset_stock_type'=>'NEW',
				  'po_created'=>date('Y-m-d H:i:s',strtotime($postData['po_created'])),
				  'po_numr'=>$postData['po_number'],
				  'vendor_name'=>$postData['po_name']
			);
			$this->Inventory_model->edit_asset_history($edit_asset_history);
			
			
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manage_asset');
	}
	
	public function manage_asset(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'manage_asset';
		$data['asset_data'] = $this->Admin_model->get_all_asset();
		$data['include'] = 'backend_iconnect/inventory/manage_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	
	//assign asset management
	public function asset_assign_mgnt(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'manage_assign_asset';
		$data['assign_asset_data'] = $this->Admin_model->get_assign_data();
		$data['include'] = 'backend_iconnect/inventory/manage_assign_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	//assign asset 
	public function assign_asset(){
		$data = $this->get_ledap();
		$data['action_tab'] = 'manage_asset';
		$data['assetData'] = json_encode($this->Admin_model->getAssetData());
		$data['include'] = 'backend_iconnect/inventory/assign_new_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	//assign asset -------- TESTING
	public function assign_asset_TESTING(){
		$data['action_tab'] = 'manage_asset';
		$data['assetData'] = json_encode($this->Admin_model->getAssetData());
		$data['include'] = 'backend_iconnect/inventory/assign_new_asset_TESTING';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	//ajax asset for assign
	public function ajx_assign_asset($assetId = 0){
		if($assetId == 0){
			$postData = $this->input->post();
			$data['asset_data'] = $this->Admin_model->get_asset_by_id($postData['assetId']);
		}else{
			$data['asset_data'] = $this->Admin_model->get_asset_by_id($assetId);
		}
		$data['assign_asset_id'] = 0;
		$this->load->view('backend_iconnect/inventory/ajx_new_asset',$data);
	}
	//
	public function submit_add_assign_asset(){
		$this->load->model('Inventory_model');
		$rk = 0;
		$postData = $this->input->post();
		$companyId = $postData['emp_company_name'];
		$locationId = $postData['assign_location'];
		$assign_asset_usr_ary = array(
			'asset_id'=>$postData['asset_assign_id'],
			'emp_type'=>$postData['emp_type'],
			'emp_name'=>$postData['emp_name'],
			'emp_email'=>$postData['emp_email'],
			'emp_code'=>$postData['emp_code'],
			'assign_lob_name'=>$postData['procured_lob'],
			//'assign_hod_name'=>$postData['approved_hod'],
			'emp_company'=>$postData['emp_company_name'],
			'emp_dept'=>$postData['emp_dept_name'],
			'assign_emp_state'=>$postData['assign_state'],
			'assign_emp_location'=>$postData['assign_location'],
			'approved_by'=>$postData['approved_by'],
			'assign_by'=>$postData['assigned_by'],
			'assign_dt'=>date('Y-m-d',strtotime($postData['assigned_date'])),
			'assign_asset_remark'=>$postData['remark_assign_asset'],
			'assign_created_dt'=>date('Y-m-d')
		);
		
		$this->db->insert(inv_assign_asset,$assign_asset_usr_ary);
		$assign_asset_usr_id = $this->db->insert_id();
		$assignAssetId = explode(',',$postData['asset_assign_id']);
		for($i=0;$i<count($assignAssetId);$i++){
			$assign_assset_ary = array(
				'assign_asset_usr_id'=>$assign_asset_usr_id,
				'assign_asset_id'=>$assign_asset_usr_id,
				'asset_id'=>$assignAssetId[$i],
				'emp_code'=>$postData['emp_code'],
				'assign_map_created_dt'=>date('Y-m-d'),
				'assign_map_is_active'=>1
			);
			$this->db->insert(inv_assign_asset_map,$assign_assset_ary);
			
			
			
			
			//change asset new to assign in asset
			/*OLD CODE
			$assetAry = array(
				'asset_stock_type'=>'assign'
			);
			*/
			$getNewAssetAry = getAssignAssetCode($assignAssetId[$i],$companyId,$locationId);
			$assetAry = array(
				'asset_stock_type'=>'Assign',
				'system_ref_code'=>$getNewAssetAry['systemRefCode'],
				//'present_asset_code'=>$getNewAssetAry['newPresentAssetCode'],  /*Change told by Shaijad*/
				'asset_stock_location'=>0,  //reset asset stock location
				'custodian_name'=>''
			);
			$this->db->where('asset_id',$assignAssetId[$i]);
			$this->db->update(add_inventory,$assetAry);
			$rk = 1;
		}
		
		$this->db->insert(inv_assign_asset_new,$assign_asset_usr_ary); //Added by bh for testing report
		
	# add email acceptance
		$subject = 'IT Asset allocation with ';
		foreach(getAssignAsset($assign_asset_usr_id) as $assignKeyAsset){
				$assetData = getAssetData($assignKeyAsset->asset_id);
				$subject .='[Serial no-'.$assetData->serial_number.' and asset tag-'.$assetData->present_asset_code.']';
		}
		$emailTemplte = getAcceptaceEmailTempl('accept',$assign_asset_usr_id);
		$acceptAry = array(
			'inv_assign_asset_emp_id'=>$assign_asset_usr_id,
			'inv_emp_email'=>$postData['emp_email'],
			'inv_emp_code'=>$postData['emp_code'],
			'acceptance_email_templ_one'=>$emailTemplte
		);
		$this->db->insert(inv_asset_accept,$acceptAry);
		
		$this->sendOutlookEmail($postData['emp_email'],$subject,$emailTemplte);
		$this->session->set_flashdata('msg','Asset assigned successfully');
		
		/*
		if($rk > 0){
	     # send email for acceptance -- not implemented yet
			
			$this->sendOutlookEmail($postData['emp_email'],$subject,$emailTemplte);
			$this->session->set_flashdata('msg','Asset assigned successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}*/
		
		
	# Add history for assign asset in inv_assign_asset_for_emp_history	
		$add_assign_asset_his = array(
			'action_type'=>'Assign',
			'assign_asset_emp_id'=>$assign_asset_usr_id,
			'asset_id'=>$postData['asset_assign_id'],
			'asset_code'=>$postData['asset_assign_id'],
			'emp_type'=>$postData['emp_type'],
			'emp_name'=>$postData['emp_name'],
			'emp_email'=>$postData['emp_email'],
			'emp_code'=>$postData['emp_code'],
			'assign_lob_name'=>$postData['procured_lob'],
			'assign_hod_name'=>$postData['approved_hod'],
			'emp_company'=>$postData['emp_company_name'],
			'emp_dept'=>$postData['emp_dept_name'],
			'assign_emp_state'=>$postData['assign_state'],
			'assign_emp_location'=>$postData['assign_location'],
			'assign_dt'=>date('Y-m-d',strtotime($postData['assigned_date'])),
			'assign_created_dt'=>date('Y-m-d'),
			'assign_asset_remark'=>$postData['remark_assign_asset'],
			'approved_by'=>$postData['approved_by'],
			'assign_by'=>$postData['assigned_by'],
			'assign_is_active'=>1,
			'action_user_code'=>$this->user_ldamp['employeeid']
		);
		$this->Inventory_model->add_inv_assign_asset_for_emp_history($add_assign_asset_his);
		 
		redirect('admin/asset_assign_mgnt');
	}
	
	
	
	public function edit_assign_asset(int $assignAssetId = 0){
		$data = $this->get_ledap();
		$data['action_tab']='edit_assign_asset';
		$data['assetData'] = json_encode($this->Admin_model->getAssetData());
		$data['include']= 'backend_iconnect/inventory/edit_assign_asset';
		$data['edit_assign_data'] = $this->Admin_model->get_assgin_asset_by_id($assignAssetId);
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	# assign asset edit
	public function ajx_assign_edit_asset($assetId = 0,$assign_asset_id=0){
		$data['assign_asset_id'] = $assign_asset_id;
		$data['asset_data'] = $this->Admin_model->get_asset_by_id($assetId);
		$data['history_data'] = $this->Admin_model->get_asset_history($assetId);
		$data['recevied_history_data'] = $this->Admin_model->get_asset_received_history($assetId);
		//var_dump($data['history_data']);
		$this->load->view('backend_iconnect/inventory/ajx_new_asset_edit',$data);
	}
	
	public function submit_edit_assign_asset(){
		$this->load->model('Inventory_model');
		$rk = 0;
		$postData = $this->input->post();
		$assignEmpAssetId = $postData['emp_assign_id'];
		$companyId = $postData['emp_company_name'];
		$locationId = $postData['assign_location'];
		$assign_asset_usr_ary = array(
			'asset_id'=>$postData['asset_assign_id'],
			'emp_type'=>$postData['emp_type'],
			'emp_name'=>$postData['emp_name'],
			'emp_email'=>$postData['emp_email'],
			'emp_code'=>$postData['emp_code'],
			'assign_lob_name'=>$postData['procured_lob'],
			//'assign_hod_name'=>$postData['approved_hod'],
			'emp_company'=>$postData['emp_company_name'],
			'emp_dept'=>$postData['emp_dept_name'],
			'assign_emp_state'=>$postData['assign_state'],
			'assign_emp_location'=>$postData['assign_location'],
			'approved_by'=>$postData['approved_by'],
			'assign_by'=>$postData['assigned_by'],
			'assign_dt'=>date('Y-m-d',strtotime($postData['assigned_date'])),
			'assign_asset_remark'=>$postData['remark_assign_asset'],
			'assign_created_dt'=>date('Y-m-d')
		);
		//$this->dd($postData);
	# asset assign to user 
		$this->db->where('assign_asset_for_emp_id',$assignEmpAssetId);
		$this->db->update(inv_assign_asset,$assign_asset_usr_ary);
		
		
		//BH update inv_assign_asset_for_emp_new table
		$this->db->where('asset_id',$postData['asset_assign_id']);
		$this->db->update('inv_assign_asset_for_emp_new',$assign_asset_usr_ary);
		//BH update inv_assign_asset_for_emp_new table done
		
	# deactive previous assign asset
		$deactiveAssginAsset = array(
			'assign_map_is_active'=>0
		);
		$this->db->where('assign_asset_usr_id',$assignEmpAssetId);
		$this->db->update(inv_assign_asset_map,$deactiveAssginAsset);
		
		/***for deactivate excel assign_asset_usr_id***/ ##BH
		$deactiveAssginAssetforexcel = array(
			'assign_map_is_active'=>0
			
		);
		$this->db->where('assign_asset_usr_id','1');
		$this->db->where('assign_asset_id',$assignEmpAssetId);
		$this->db->update(inv_assign_asset_map,$deactiveAssginAssetforexcel);
		/***for deactivate excel assign_asset_usr_id DONE***/
		
		
		$rk = 1;
	# assign New asset eachtime user update
		if($postData['asset_assign_id'] !=''){
			$assignAssetId = explode(',',$postData['asset_assign_id']);
			for($i=0;$i<count($assignAssetId);$i++){
				$assign_assset_ary = array(
					'assign_asset_usr_id'=>$assignEmpAssetId,
					'assign_asset_id'=>$assignEmpAssetId,
					'asset_id'=>$assignAssetId[$i],
					'emp_code'=>$postData['emp_code'],
					'assign_map_created_dt'=>date('Y-m-d'),
					'assign_map_is_active'=>1
				);
				$this->db->insert(inv_assign_asset_map,$assign_assset_ary);
				
				
			
			
				
	# change asset new to assign in asset
				/*$assetAry = array(
					'asset_stock_type'=>'assign'
				);
				*/
				$getNewAssetAry = getAssignAssetCode($assignAssetId[$i],$companyId,$locationId);
				$assetAry = array(
					'asset_stock_type'=>'Assign',
					'system_ref_code'=>$getNewAssetAry['systemRefCode']
					//'present_asset_code'=>$getNewAssetAry['newPresentAssetCode']
				);
				$this->db->where('asset_id',$assignAssetId[$i]);
				$this->db->update(add_inventory,$assetAry);
			}
		}
		
		$add_assign_asset_his = array(
			'action_type'=>'EDIT',
			'assign_asset_emp_id'=>$assignEmpAssetId,
			'asset_id'=>$postData['asset_assign_id'],
			'asset_code'=>$postData['asset_assign_id'],
			'emp_type'=>$postData['emp_type'],
			'emp_name'=>$postData['emp_name'],
			'emp_email'=>$postData['emp_email'],
			'emp_code'=>$postData['emp_code'],
			'assign_lob_name'=>$postData['procured_lob'],
			'assign_hod_name'=>$postData['approved_hod'],
			'emp_company'=>$postData['emp_company_name'],
			'emp_dept'=>$postData['emp_dept_name'],
			'assign_emp_state'=>$postData['assign_state'],
			'assign_emp_location'=>$postData['assign_location'],
			'assign_dt'=>date('Y-m-d',strtotime($postData['assigned_date'])),
			'assign_created_dt'=>date('Y-m-d'),
			'assign_asset_remark'=>$postData['remark_assign_asset'],
			'approved_by'=>$postData['approved_by'],
			'assign_by'=>$postData['assigned_by'],
			'assign_is_active'=>1,
			'action_user_code'=>$this->user_ldamp['employeeid']
		);
		$this->Inventory_model->add_inv_assign_asset_for_emp_history($add_assign_asset_his);
		
		
		# Add history in inv_assign_asset_emp_map_history table
		$assign_assset_history_array = array(
				'assign_asset_usr_id'=>$assignEmpAssetId,
				'assign_asset_id'=>$assignEmpAssetId,
				'asset_id'=>$postData['asset_assign_id'],
				'emp_code'=>$postData['emp_code'],
				'assign_map_created_dt'=>date('Y-m-d'),
				'assign_map_is_active'=>1,
				'action_user_code'=>$postData['emp_code'],
				'action_type'=>'EDIT'
			);
			//$this->dd($assign_assset_history_array);
			
			$this->db->insert(inv_assign_asset_emp_map_history,$assign_assset_history_array);
		
		if($rk > 0){
			$this->session->set_flashdata('msg','Asset assigned updated successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/asset_assign_mgnt');
	}
	
	#manage received asset
	public function asset_received(){
		$data['action_tab'] = 'asset_received';
		$data['include'] = 'backend_iconnect/inventory/asset_received';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	#add received asset
	public function add_received_asset(int $assignAssetId = 0){
		$data['action_tab']='edit_assign_asset';
		$data['assetData'] = json_encode($this->Admin_model->getAssetData());
		$data['include']= 'backend_iconnect/inventory/edit_assign_asset_receive';
		$data['edit_assign_data'] = $this->Admin_model->get_assgin_asset_by_id($assignAssetId);
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	#submit_receive_asset
	public function submit_receive_asset(){
		$return_status = (int)0;
		$postData = $this->input->post();
		$id_key = explode("_",$postData['asset_id']);
		$asset_id = $id_key[0];
		$asset_map_id = $id_key[1];
		$remark_ary = array(
			'asset_received_remark'=>$postData['asset_receive_remark'],
			'asset_received_status'=>1,
			'assign_map_is_active'=>0,
			'asset_received_date'=>date('Y-m-d')
		);
	#update on receive map
		$this->db->where('assign_asset_emp_map_id',$asset_map_id);
		$this->db->update(inv_assign_asset_map,$remark_ary);
		
	#update main asset as received 
		$asset_in_stock_ary = array(
			'asset_stock_type'=>'Stock'
		);
		$this->db->where('asset_id',$asset_id);
		if($this->db->update(add_inventory,$asset_in_stock_ary)){
			
	#reset the array 
			$assign_ast_ary = array(
				'asset_id'=>$postData['prasent_ary_list'],
				'assign_is_active'=>0
			);
			//$this->db->where('assign_asset_for_emp_id',$asset_id);   --- OLD
			$this->db->where('asset_id',$asset_id);  // Change on 2nd feb
			$this->db->update(inv_assign_asset,$assign_ast_ary);
			
			
			//BH for inv_assign_asset_for_emp_new table
			$assign_ast_ary_new = array(
				'asset_id'=>$postData['prasent_ary_list'],
				'assign_is_active'=>0
			);
			$this->db->where('asset_id',$asset_id);
			$this->db->update('inv_assign_asset_for_emp_new',$assign_ast_ary_new);
			
			//BH for inv_assign_asset_for_emp_new table done
			
			//update by BH to add asset stock location 
			if($postData['asset_stock_location'] != ''){
				$asset_stock_loc = array(
					'asset_stock_location'=>$postData['asset_stock_location']
				);
				$this->db->where('asset_id',$asset_id);
				$this->db->update(add_inventory,$asset_stock_loc);
			}
			if($postData['custodian'] != ''){
				$custodian_name = array(
					'custodian_name'=>$postData['custodian']
				);
				$this->db->where('asset_id',$asset_id);
				$this->db->update(add_inventory,$custodian_name);
			}
			//update by BH to add asset stock location  done
			
			$return_status = (int)$asset_id;
		}
		echo json_encode(array('return_ary'=>$return_status));
	}
	
	#submit add received asset
	public function submit_add_received_asset(){
		
	}
	
	#edit received asset
	public function edit_received_asset(){
		
	}
	
	#submit edit received asset
	public function submit_edit_received_asset(){
		
	}
	
	public function manageNonUsr(){
		$data['action_tab'] = 'manage_non_usr';
		$data['non_usr_data'] = $this->Admin_model->getNonUsrData();
		$data['include'] = 'backend_iconnect/inventory/manage_non_usr';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function add_non_usr_form(){
		$data['empCode'] = 'SGNAST'.rand(1111,9999);
		$data['action_tab'] = 'add_non_usr';
		$data['include'] = 'backend_iconnect/inventory/non_usr_form';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	
	public function edit_non_usr_form(int $nonUsrId = 0){
		$data['action_tab'] = 'edit_non_usr';
		$data['non_user_data'] = $this->Admin_model->getNonUserDataById($nonUsrId);
		$data['include'] = 'backend_iconnect/inventory/edit_usr_form';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function add_non_usr(){
		$postData = $this->input->post();
		$non_usr_form_ary = array(
			'user_name'=>$postData['non_usr_name'],
			'user_code'=>$postData['non_usr_code'],
			'non_user_state'=>$postData['non_usr_state'],
			'non_user_location'=>$postData['non_usr_location'],
			'user_address'=>$postData['non_usr_address'],
			'non_user_created_dt'=>date('Y-m-d I:m:s')
		);
		if($this->Admin_model->add_non_usr($non_usr_form_ary)){
			$this->session->set_flashdata('msg','Non user added successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manageNonUsr');
	}
	
	public function edit_non_usr(){
		$postData = $this->input->post();
		$non_usr_form_ary = array(
			'user_name'=>$postData['non_usr_name'],
			'non_user_state'=>$postData['non_usr_state'],
			'non_user_location'=>$postData['non_usr_location'],
			'user_address'=>$postData['non_usr_address'],
			'non_user_created_dt'=>date('Y-m-d I:m:s')
		);
		if($this->Admin_model->edit_non_usr($non_usr_form_ary,$postData['non_usr_id'])){
			$this->session->set_flashdata('msg','Non user edited successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manageNonUsr');
	}
	
	public function ajax_handover(){
		$this->load->view("backend_iconnect/inventory/asset_handover");
	}
	
	public function ajax_call(){
		$postData = $this->input->post();
		switch($postData['type']){
			case 'search_asset':
				$getAssetData = $this->Admin_model->getAssetDataByKey($postData['asset_search']);
				
				$returnData = '<thead>
				  <tr>
					<th>Asset Name</th>
					<th>Asset Serial No.</th>
					<th>Present Asset Code</th>
				  </tr>
				</thead>
				<tbody>';
				/*$returnData .='<tr class="Rkhover" onclick="onSerchSug(this)">
							<td data-key="0">Test 1</td>
							<td>123456789</td>
							<td>ABC1234</td>
						</tr>
						<tr class="Rkhover" onclick="onSerchSug(this)">
							<td data-key="0">Test 2</td>
							<td>123456789</td>
							<td>XYZ1234</td>
						</tr>';
				*/
				if(count($getAssetData) > 0){
					foreach($getAssetData as $assetRecord){
						$returnData .='<tr class="Rkhover" onclick="onSerchSug(this)">
							<td data-key="'.$assetRecord->asset_id.'">'.$assetRecord->asset_name.'</td>
							<td>'.$assetRecord->serial_number.'</td>
							<td>'.$assetRecord->present_asset_code.'</td>
						</tr>';
					}
				}else{
					$returnData.='<tr colspan="3"><td colspan="3">No record found</td></tr>';
				}
				$returnData .= '</tbody>';
			break;
		}
		echo $returnData;
	}
	
	//send email - SMTP
	public function sendOutlookEmail($toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);		
		$smu =smu;
		$smp=smp;
		$mail = '';
		$account=$smu;
		$password=$smp;
		$from=$smu;
		$to=$toEmail;
		$cc="bhagyashree.holey@skeiron.com,Vijay.Maurya@skeiron.com,Shaijad.Kureshi@skeiron.com";
		$from_name="Helpdesk";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
			//$mail->AddCC('bhagyashree.holey@skeiron.com'); // for testing
		}
		
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		//$this->dd();
		return true;
	}
	
//************************** ASSIGN ASSET PDF GANERATION CODE START *********************************************************
	# PDF
	public function ajx_assign_edit_assetPDF($assetId = 0,$assign_asset_id=0){
		$data['assign_asset_id'] = $assign_asset_id;
		$data['asset_data'] = $this->Admin_model->get_asset_by_id($assetId);
		$this->load->view('backend_iconnect/inventory/ajx_new_asset_edit_pdf',$data);
	}
	
	#add received asset
	public function assign_asset_acceptance(int $assignAssetId = 0){
		/* FOR DEMO
		$subject = 'Asset acceptace confirmation';
		$emailTemplte = getAcceptaceEmailTempl('accept',$assignAssetId);
		$this->sendOutlookEmail('ravi.kumar@skeiron.com',$subject,$emailTemplte);
		acceptAsset();
		*/
		
		$data['action_tab']='edit_assign_asset';
		$data['assetData'] = json_encode($this->Admin_model->getAssetData());
		$data['edit_assign_data'] = $this->Admin_model->get_assgin_asset_by_id($assignAssetId);
		
		//$this->acceptAsset($assignAssetId);
		
		$this->load->view('backend_iconnect/inventory/edit_assign_asset_pdf',$data);
	}
	
	#function for accept the accept and sent email confirmation for via email 
	function acceptAsset($assetAssignEmpId = 0){
		$postData = $this->input->post();
		$acceptData = checkIsAcceptedByEmail($assetAssignEmpId);
		
		
		if($acceptData > 0){
			$subject = 'Asset acceptace confirmation';
			$emailTemplte = getAcceptaceEmailTempl('confirmation',$assetAssignEmpId);
			$acceptAry = array(
				'acceptance_email_templ_two'=>$emailTemplte,
				'accepted_date'=>date('Y-m-d H:i:s'),
				'is_accepted'=>1
			);
			$this->db->where('inv_asset_accept_id',$postData['accept_id']);
			$this->db->update(inv_asset_accept,$acceptAry);
			
			#send email confirmation email -- not implemented yet
			$this->sendOutlookEmail($acceptData->inv_emp_email,$subject,$emailTemplte);
			$this->session->set_flashdata('msg','Thank you for accepting the asset');
		}else{
			$this->session->set_flashdata('error_msg','Error occured while accepting... Please try again');
		}
		//redirect('admin/assign_asset_acceptance/'.$assetAssignEmpId);
	}


public function downloadPDFV2($gateway_pass_id=0){
		
		ob_start();
		
		$this->load->library('PDF_CON_RV/tcpdf');
		header("Content-Encoding: None", true);
		ini_set('max_execution_time', 1000);
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// add a page
		//$pdf->AddPage();
		$pdf->AddPage('L', 'A2');
		
		//with rates
		$passingData = getMaterialMasterDataById($gateway_pass_id);
		$approvalData = getMaterialApproval($passingData->material_master_id);
		$contents =<<<EOF
		
EOF;
		//$pdf->writeHTML($contents, true, false, true, false, '');
		$pdf->Write(0, $contents, '', 0, 'L', true, 0, false, false, 0);
		//Close and output PDF document
		$pdf->lastPage();
		//ob_end_clean();
		
		$pdf->Output('gateway_pass.pdf','I');
		exit;
	}

//************************** ASSIGN ASSET PDF GANERATION CODE END ***********************************************************
# New MIS Code  START *******************************************************************
	public function reportPage_new(){
		$data = $this->get_ledap();
		$this->load->model('Helpdesk_model');
		$data['departments'] = $this->Helpdesk_model->get_departments(); 	
		$data['asset_wise_stock'] = getReportData_new('getAssetWise',array('assetType'=>'Stock'));
		$data['asset_wise_in_use'] = getReportData_new('getAssetWise',array('assetType'=>'Assign'));
		$data['company_wise'] = getReportData_new('getCompanyWise');
		$data['LOB_data'] = getReportData_new('getLOB');
		$data['location_data'] = getReportData_new('getLocation');
		$data['asset_stock_location_data'] = getReportData_new('getAssetStockLocation');
		$data['stock_data'] = getReportData_new('getStock');
		$data['action_tab'] = 'asset_report';
		$data['include'] = 'backend_iconnect/inventory/reportPage_new';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function ajx_mis_report_new() {
		$postData = $this->input->post();
		switch($postData['type']){
			case 'asset_wise':
			
				//BH$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add LEFT JOIN inv_assign_asset_emp_map as inv_emp_asign_map ON (inv_emp_asign_map.asset_id = inv_add.asset_id) LEFT JOIN inv_assign_asset_for_emp as inv_emp_asig  ON(inv_emp_asign_map.asset_id = inv_emp_asig.assign_asset_for_emp_id) WHERE inv_add.asset_type = '".$postData['key_search_primary']."' AND inv_add.asset_stock_type='".$postData['key_search_secondary']."' AND inv_add.asset_is_active='1'");
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add LEFT JOIN inv_assign_asset_emp_map as inv_emp_asign_map ON (inv_emp_asign_map.assign_asset_id = inv_add.asset_id) LEFT JOIN inv_assign_asset_for_emp as inv_emp_asig  ON(inv_emp_asign_map.assign_asset_id = inv_emp_asig.assign_asset_for_emp_id) WHERE inv_add.asset_type = '".$postData['key_search_primary']."' AND inv_add.asset_stock_type='".$postData['key_search_secondary']."' AND inv_add.asset_is_active='1' AND inv_emp_asign_map.assign_map_is_active=1");
				
				
				$resultArry = $qry->result();
			break;
			
			case 'company_wise':
				$qry = $this->db->query("SELECT * FROM inv_assign_asset_for_emp as inv_emp_asig LEFT JOIN inv_assign_asset_emp_map as inv_emp_asign_map ON(inv_emp_asign_map.assign_asset_id = inv_emp_asig.assign_asset_for_emp_id) LEFT JOIN inv_add_asset as inv_add ON (inv_emp_asign_map.assign_asset_id = inv_add.asset_id) WHERE inv_emp_asig.emp_company ='".$postData['key_search_primary']."' AND inv_emp_asign_map.assign_map_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'lob_asset_wise':
			     /**query by BH**/
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add WHERE inv_add.procured_lob_emp_code ='".$postData['key_search_secondary']."' AND inv_add.asset_type='".$postData['key_search_primary']."' AND inv_add.asset_stock_type = '".$postData['stockType']."' AND inv_add.asset_is_active='1'");
				/**query by BH done**/
				
				
				//below are previous 
				//$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add LEFT JOIN inv_assign_asset_emp_map as inv_emp_asign_map ON (inv_emp_asign_map.assign_asset_id = inv_add.asset_id) LEFT JOIN inv_assign_asset_for_emp as inv_emp_asig ON (inv_emp_asign_map.assign_asset_id = inv_emp_asig.assign_asset_for_emp_id) WHERE inv_add.procured_lob_emp_code ='".$postData['key_search_secondary']."' AND inv_add.asset_type='".$postData['key_search_primary']."' AND inv_add.asset_stock_type = '".$postData['stockType']."' AND inv_add.asset_is_active='1'");
				
				$resultArry = $qry->result();
			break;
			
			case 'location_asset_wise':
				  /**query by BH**/
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add WHERE inv_add.procured_location = '".$postData['key_search_primary']."' AND inv_add.asset_is_active='1'");
				/**query by BH done**/
				
				//below are previous 
				//$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add  LEFT JOIN inv_assign_asset_emp_map as inv_emp_asign_map ON (inv_emp_asign_map.assign_asset_id = inv_add.asset_id) LEFT JOIN inv_assign_asset_for_emp as inv_emp_asig ON (inv_emp_asign_map.assign_asset_id = inv_emp_asig.assign_asset_for_emp_id) WHERE inv_add.procured_location ='".$postData['key_search_primary']."' AND inv_add.asset_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'stock_wise':
				/**query by BH**/
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add WHERE inv_add.asset_stock_type='".$postData['key_search_primary']."' AND inv_add.asset_is_active='1'");
				/**query by BH done**/
				
				//below are previous 
				//$qry = $this->db->query("SELECT * FROM inv_assign_asset_for_emp as inv_emp_asig LEFT JOIN inv_assign_asset_emp_map as inv_emp_asign_map ON (inv_emp_asign_map.assign_asset_id = inv_emp_asig.assign_asset_for_emp_id) LEFT JOIN inv_add_asset as inv_add ON (inv_emp_asign_map.assign_asset_id = inv_add.asset_id) WHERE inv_add.asset_stock_type='".$postData['key_search_primary']."' AND asset_is_active='1'");
				$resultArry = $qry->result();
			break;	

			case 'asset_count':
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add WHERE inv_add.asset_type='".$postData['key_search_primary']."' AND inv_add.asset_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'stock_asset_count':
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add LEFT JOIN inv_assign_asset_for_emp as asset_emp ON (asset_emp.asset_id=inv_add.asset_id) WHERE inv_add.asset_type='".$postData['key_search_primary']."' AND inv_add.asset_stock_type='Stock' AND inv_add.asset_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'nav_asset_count':
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add LEFT JOIN inv_assign_asset_for_emp as asset_emp ON (asset_emp.asset_id=inv_add.asset_id) WHERE inv_add.asset_type='".$postData['key_search_primary']."' AND inv_add.nav_code!='NA' AND inv_add.asset_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'total_asset_count':
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add WHERE inv_add.asset_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'total_asset_countinv':
				$qry = $this->db->query("SELECT * FROM inv_assign_asset_for_emp as inv_add WHERE inv_add.assign_is_active='1'");
				$resultArry = $qry->result();
			break;
			
			case 'stock_asset_location':
				$qry = $this->db->query("SELECT * FROM inv_add_asset as inv_add WHERE inv_add.asset_stock_location='".$postData['key_search_primary']."' AND inv_add.asset_is_active='1'");
				$resultArry = $qry->result();
			break;
		}
		//echo $this->db->last_query(); die;
	
			if($postData['type'] == 'total_asset_count' || $postData['type'] == 'lob_asset_wise' || $postData['type'] == 'asset_count' || $postData['type'] == 'location_asset_wise' || $postData['type'] == 'stock_asset_location' || $postData['type'] == 'stock_wise'){
			foreach($resultArry as $recordData){
				$returnAry = array(
					'procure_lob'=>getNameByCodeId('getLobName',$recordData->procured_lob_emp_code),
					'procure_company'=>getNameByCodeId('getCompanyName',$recordData->procured_company),
					'user_lob'=>getNameByCodeId('getLobName',getNameByCodeId('getLobNameINC',$recordData->asset_id)),
					'user_company'=>getNameByCodeId('getCompanyName',getNameByCodeId('getCompanyNameINC',$recordData->asset_id)),
					'user_name'=>getNameByCodeId('getAssignEmpName',$recordData->asset_id),
					'user_dept'=>getNameByCodeId('getDeptName',getNameByCodeId('getDeptNameINC',$recordData->asset_id)),
					'user_location'=>getNameByCodeId('getLocationName',getNameByCodeId('getLocationNameINC',$recordData->asset_id)),
					'asset_type'=>getNameByCodeId('getAssetTypeName',$recordData->asset_type),
					'asset_tag'=>$recordData->present_asset_code,
					'manufacture'=>$recordData->manufacturer,
					'processor'=>getNameByCodeId('getProcessorName',$recordData->asset_id),
					'model_no'=>$recordData->modal_no,
					'serial_no'=>$recordData->serial_number,
					'nav_code'=>$recordData->nav_code,
					'po_numr'=>$recordData->po_numr,
					'warranty_expire_in_monthe'=>$recordData->warranty_expiry_no_month .' Months',
					'warranty_expire_in_type'=>$recordData->warranty_type,
					'warranty_expiry_date'=>$recordData->warranty_expiry_date,
					'stock_type_status'=>$recordData->asset_stock_type,
					'custodian_name'=>$recordData->custodian_name,
					'stock_location_name'=>getNameByCodeId('getLocationName',$recordData->asset_stock_location),
					'procure_date'=>$recordData->procured_date,
					'allocation_date'=>getNameByCodeId('getAssigndateINC',$recordData->asset_id)
				);
				$returnAryO[] = $returnAry;
			}
		}else{
			foreach($resultArry as $recordData){
				$returnAry = array(
					'procure_lob'=>getNameByCodeId('getLobName',$recordData->procured_lob_emp_code),
					'procure_company'=>getNameByCodeId('getCompanyName',$recordData->procured_company),
					'user_lob'=>getNameByCodeId('getLobName',$recordData->assign_lob_name),
					'user_company'=>getNameByCodeId('getCompanyName',$recordData->emp_company),
					'user_name'=>$recordData->emp_name,
					'user_dept'=>getNameByCodeId('getDeptName',$recordData->emp_dept),
					'user_location'=>getNameByCodeId('getLocationName',$recordData->assign_emp_location),
					'asset_type'=>getNameByCodeId('getAssetTypeName',$recordData->asset_type),
					'asset_tag'=>$recordData->present_asset_code,
					'manufacture'=>$recordData->manufacturer,
					'processor'=>getNameByCodeId('getProcessorName',$recordData->asset_id),
					'model_no'=>$recordData->modal_no,
					'serial_no'=>$recordData->serial_number,
					'nav_code'=>$recordData->nav_code,
					'po_numr'=>$recordData->po_numr,
					'warranty_expire_in_monthe'=>$recordData->warranty_expiry_no_month .' Months',
					'warranty_expire_in_type'=>$recordData->warranty_type,
					'warranty_expiry_date'=>$recordData->warranty_expiry_date,
					'stock_type_status'=>$recordData->asset_stock_type,
					'custodian_name'=>$recordData->custodian_name,
					'stock_location_name'=>getNameByCodeId('getLocationName',$recordData->asset_stock_location),
					'procure_date'=>$recordData->procured_date,
					'allocation_date'=>$recordData->assign_dt
				);
				$returnAryO[] = $returnAry;
			}
		}
		$data['mis_data'] = $returnAryO;
		$this->load->view('backend_iconnect/inventory/report_ajax_sys_new',$data);
	}
	# New MIS Code  END *******************************************************************
	
	
//mis report ajax start 
	public function reportPage(){
		$data = $this->get_ledap();
		$this->load->model('Helpdesk_model');
		$data['departments'] = $this->Helpdesk_model->get_departments(); 
		
		$data['asset_wise_stock'] = getReportData('getAssetWise',array('assetType'=>'In_Stock'));
		$data['asset_wise_in_use'] = getReportData('getAssetWise',array('assetType'=>'Out_Stock'));
		$data['company_wise'] = getReportData('getCompanyWise');
		$data['LOB_data'] = getReportData('getLOB');
		$data['location_data'] = getReportData('getLocation');
		$data['stock_data'] = getReportData('getStock');
		$data['action_tab'] = 'asset_report';
		$data['include'] = 'backend_iconnect/inventory/reportPage';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function ajx_mis_report(){
		$postData = $this->input->post();
		switch($postData['type']){
			case 'asset_wise':
				$qry = $this->db->query("SELECT * FROM inv_demo_mis WHERE asset_type='".$postData['key_search_primary']."' AND stock_type_status='".$postData['key_search_secondary']."' AND inv_demo_is_active=1");
				$resultArry = $qry->result();
			break;
			
			case 'company_wise':
				$qry = $this->db->query("SELECT * FROM inv_demo_mis WHERE user_company='".$postData['key_search_primary']."' AND inv_demo_is_active=1");
				$resultArry = $qry->result();
			break;
			
			case 'lob_asset_wise':
				$qry = $this->db->query("SELECT * FROM inv_demo_mis WHERE asset_type='".$postData['key_search_primary']."' AND user_lob='".$postData['key_search_secondary']."' AND stock_type_status = '".$postData['stockType']."' AND inv_demo_is_active=1");
				$resultArry = $qry->result();
			break;
			
			case 'location_asset_wise':
				$qry = $this->db->query("SELECT * FROM inv_demo_mis WHERE user_location='".$postData['key_search_primary']."' AND inv_demo_is_active=1");
				$resultArry = $qry->result();
			break;
			
			case 'stock_wise':
				$qry = $this->db->query("SELECT * FROM inv_demo_mis WHERE stock_type_status='".$postData['key_search_primary']."' AND inv_demo_is_active=1");
				$resultArry = $qry->result();
			break;
			
			
			
		}
		$data['mis_data'] = $resultArry;
		$this->load->view('backend_iconnect/inventory/report_ajax_sys',$data);
	}
//mis report ajax end 	

//angular code start 
	public function add_asset_ang(){
		$data['include'] = 'backend_iconnect/inventory/angular/add_new_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function manage_asset1(){
		$data['asset_data'] = $this->Admin_model->get_all_asset();
		$data['include'] = 'backend_iconnect/inventory/manage_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
		
	public function getObject(){
		$postData = $this->input->post();
		$returnData = array();
		$objCode = $postData['objCode'];
		switch($objCode){
			case 'getExpiryDate':
				if($postData['warnty_month']!=''){
					$monthCount = $postData['warnty_month'];
				}else{
					$monthCount = 0;
				}
				$returnData['objData'] = date('d-m-Y', strtotime("+".$monthCount." months", strtotime($postData['procur_dt'])));
			break;
			case 'getLocation':
				$returnData['objData'] = inv_location($postData['stateId']);
			break;
			case 'getHod':
				$returnData['objData'] = inv_hod($postData['lobId']);
			break;
			case 'getAsset':
				$returnData['objData'] = $this->Admin_model->getAssetData();
			break;
		}
		echo json_encode($returnData);
	}
	
	//initialize 
	public function getPrefilled($stateId=0){
		$prefilledData = array(
			'assetType'=>inv_assetType(),
			'preDefind'=>date('dMYImS'),
			'hod'=>inv_hod(),
			'lob'=>inv_lob(),
			'state'=>inv_states(),
			'locations'=>inv_location($stateId)
		);
		echo json_encode($prefilledData);
	}
//angular code end

	//yammar code start ***********************************
	public function ydashboard(){
		$data['yem'] = 1;
		$data['include'] = 'backend_iconnect/yammer/profile';
		$this->load->view('backend_iconnect/yammer/masterpage',$data);
	}
	
	public function testUpload(){
		$postData = $this->input->post();
		$returnAry = array(
			'fileName'=>$_FILES['file']['name']
		);
		echo json_encode($returnAry);
	}
	//yammar code end *************************************
	
	public function changestocktonew(){
		$this->db->where('asset_stock_type', 'stock');
		$this->db->update("inv_add_asset", array('asset_stock_type' => 'New'));
		
		if($this->db->affected_rows()>0){
			echo "success-inv_add_asset";
		}
		
	}
	
	public function view_assign_asset($assetid){
		//echo $assetid;
		$data = $this->get_ledap();
		$data['action_tab']='view_assign_asset';
		$data['asset_assign_data'] = $this->Admin_model->get_assgin_asset_by_id($assetid);
		$this->load->view('backend_iconnect/inventory/assign_assign_asset_view',$data);
		
	}
	public function download_IT_MIS(){
		$this->load->model('Admin_model');
		$data = $this->get_ledap();
		$data['action_tab']='download_IT_MIS';
		$data['asset_data'] = $this->Admin_model->get_all_asset();
		$this->load->view('backend_iconnect/inventory/download_IT_MIS',$data);
	}
	
	public function getReportDownloadV2(){
		ob_start();
		$postData = $this->input->post();
		if($postData['download_btn'] == 'Download xls'){
			$this->downloadReportV2();
		}
		
	}
	
	public function downloadReportV2(){
		$postData = $this->input->post();
		$customReport = $this->Admin_model->get_all_asset();
	    $contents ='"Sr No","Procure LOB","Procure company","User LOB","User Company","User Dept","User Name","User Email","User Code","User Type","User Location","Asset Type","Serial No","Nav Code","Manufacture","Processor","Model No","Procure Date","Allocation Date","Status"';
	    $contents .="\n";
		$i = 1;
	foreach($customReport as $recordData){
		if($recordData->asset_stock_type == 'Assign'){
			$stockStatus = 'In Use';
		}
		if($recordData->asset_stock_type == 'Stock'){
			$stockStatus = 'In Stock';
		}else{
			$stockStatus = $recordData->asset_stock_type;
		}
		$contents.='"'.$i.'","'.getNameByCodeId('getLobName',$recordData->procured_lob_emp_code).'","'.getNameByCodeId('getCompanyName',$recordData->procured_company).'","'.getNameByCodeId('getLobName',getNameByCodeId('getLobNameINC',$recordData->asset_id)).'","'.getNameByCodeId('getCompanyName',getNameByCodeId('getCompanyNameINC',$recordData->asset_id)).'","'.getNameByCodeId('getDeptName',getNameByCodeId('getDeptNameINC',$recordData->asset_id)).'","'.getNameByCodeId('getAssignEmpName',$recordData->asset_id).'","'.getNameByCodeId('getAssignEmpEmail',$recordData->asset_id).'","'.getNameByCodeId('getAssignEmpCode',$recordData->asset_id).'","'.getNameByCodeId('getAssignEmpType',$recordData->asset_id).'","'.getNameByCodeId('getLocationName',getNameByCodeId('getLocationNameINC',$recordData->asset_id)).'","'.getNameByCodeId('getAssetTypeName',$recordData->asset_type).'","'.$recordData->serial_number.'","'.$recordData->nav_code.'","'.$recordData->manufacturer.'","'.getNameByCodeId('getProcessorName',$recordData->asset_id).'","'.$recordData->modal_no.'","'.date('Y-m-d',strtotime($recordData->procured_date)).'","'.date('Y-m-d',strtotime(getNameByCodeId('getAssigndateINC',$recordData->asset_id))).'","'.$stockStatus.'"';
		$contents.="\n";
		$i++;
	}
	    $contents = strip_tags($contents);
	    Header("Content-Disposition: attachment;filename=IT_assets.csv ");
	    print $contents;
	    exit;
	}
	
	public function send_reminder_mail(){
		$data = $this->get_ledap();
		
		$data['non_accepted_user'] = $this->Admin_model->non_accept_asset_users();
		$data['include'] = 'backend_iconnect/inventory/send_remainder_mail';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function send_IT_bulkemail(){
		$data = $this->get_ledap();
		$assign_asset_emp_id = $_POST['checklist'];
		foreach ($assign_asset_emp_id as $assign_asset_emp_id=>$assetval) {
			$nonAcpusers = $this->Admin_model->get_non_accepted_assets_user($assetval);
			
            foreach ($nonAcpusers as $users){
				$email= $users->emp_email;
				$emailSubj ='Reminder';
				$emailMsg = 'Dear ' .$users->emp_name.',<br/><br/>';
				$emailMsg .= 'With referance to asset-'.getNameByCodeId('getAssetTypeName',getNameByCodeId('getAssetType',$users->asset_id)) . 
				' with [Serial no-'.getNameByCodeId('getSerialNumber',$users->asset_id).' and 
				asset tag-'.getNameByCodeId('getAssetCode',$users->asset_id).'] has been already assigned to you 
				on date ' .date('d-m-Y',strtotime($users->assign_dt)).'.<br/>
				We wish to remind you that the same asset has not been accepted by you.kindly accept the same.';
				
				$emailMsg .= '<p class="fontSmall"><b>Kindly Note:-</b>Make sure you are logged in to http://intranet.skeiron.com Before clicking on accept button.</p>
	<p class="fontSmall"><a href="'.base_url().'admin/assign_asset_acceptance/'.$users->assign_asset_for_emp_id.'" class="btn btn-primary">I accept</a>
	<p class="fontSmall">Additionally you can reach out to ITSS by logging an online complaint on http://intranet.skeiron.com/helpdesk or on a landline no 020-66278111 and Ext no 8111.</p>
	<br>
	<p class="fontSmall">This is System generated email, hence please do not reply.</p>';
				$this->sendOutlookEmail($email,$emailSubj,$emailMsg);
			
			}
			
        }
		$this->session->set_flashdata('msg','Email sent successfully');
		redirect('admin/send_remainder_mail');
	}
	
	/*********Admin asset managment start*********/
	
	public function manage_asset_admin(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['action_tab'] = 'manage_asset_admin';
		$data['asset_data'] = $this->Admin_Inventory_model->get_all_adminasset();
		$data['include'] = 'backend_iconnect/admin_inventory/manage_asset_admin';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function editadmin_asset($assetId){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['asset_data'] = $this->Admin_Inventory_model->get_adminasset_by_id($assetId);
		$data['include'] = 'backend_iconnect/admin_inventory/editadmin_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function admin_assign_asset($assetId){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['action_tab'] = 'admin_assign_asset';
		$data['asset_data'] = $this->Admin_Inventory_model->get_asset($assetId);
		$data['include'] = 'backend_iconnect/admin_inventory/admin_assign_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function AssignAdminAsset($assetId){
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		
		$assetAry = array(
		    'procured_date'=>date('Y-m-d',strtotime($postData['procured_date'])),
			'user'=>$postData['user'],
			'emp_code'=>$postData['emp_code'],
			'email'=>$postData['email'],
			'grade'=>$postData['grade'],
			'user_location'=>$postData['user_location'],
			'user_company'=>$postData['user_company'],
			'user_company_brand'=>$postData['user_lob'],
			'user_lob_id'=>getLOBID($postData['user_lob']),
			'monthly_limit'=>$postData['monthly_limit'],
			'issued_on'=>date('Y-m-d',strtotime($postData['issued_on'])),
			'availablity_status'=>'assign',
			'assign_remark'=>$postData['assign_remark']
			
		);
		
			$lob = $postData['user_lob'];
			$data['LobHod'] = $this->Admin_Inventory_model->getLOB_HODByName($lob);
			$hod_email = $data['LobHod']->lob_hod;
		if($this->Admin_Inventory_model->admin_asset_assign($assetAry,$assetId)){
			/*Send email start*/
			
			$subject = 'Admin Asset Allocation';
			$emailTemplte = getAssignEmailTempl('assign',$assetId);
		    $this->sendAdminAssignAssetEmail('Admin Inventory',$postData['email'],$subject,$emailTemplte,$hod_email);
			
			$assetassignAry = array(
			'asset_id'=>$assetId,
			'emp_name'=>$postData['user'],
			'emp_email'=>$postData['email'],
			'emp_code'=>$postData['emp_code'],
			'assign_email'=>$postData['email'],
			'assign_date'=>date('Y-m-d'),
			'accept'=>0
			);
			
			$this->db->insert("admin_asset_acept",$assetassignAry);
			/*Send email end*/
			$this->session->set_flashdata('msg','Asset assigned successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manage_asset_admin');
	}
	
	public function admin_assign_asset_acceptance(int $assignAssetId = 0){
			$this->load->model('Admin_Inventory_model');
			$postData = $this->input->post();
			$data['action_tab']='edit_assign_asset';
			$data['edit_assign_data'] = $this->Admin_Inventory_model->get_adminasset_by_id($assignAssetId);
			
			$this->adminacceptAsset($assignAssetId);
			
			$this->load->view('backend_iconnect/admin_inventory/assign_asset_pdf',$data);
		
	}
	
	function adminacceptAsset($assetAssignId = 0){
		$this->load->model('Admin_Inventory_model'); 
		$postData = $this->input->post();
		$acceptData = admincheckIsAcceptedByEmail($assetAssignId);
			if($acceptData > 0){
			$subject = 'Asset acceptace confirmation';
			//$emailTemplte = getAssignEmailTempl('confirmation',$assetAssignId);
			$acceptAry = array(
				'accept_date'=>date('Y-m-d'),
				'accept'=>1
			);
			$this->db->where('asset_id',$acceptData->asset_id);
			$this->db->update('admin_asset_acept',$acceptAry);
			
			
			$asset_assign_data = $this->Admin_Inventory_model->get_adminasset_by_id($assetAssignId);
			$emailSubj ='Assign Asset confirmation';
				$emailMsg = '<p>Dear '.$acceptData->emp_name.',</p>
				<p>Kindly note that below asset has been issued to you today by Central Admin.This is to be used under your 
				ownership and not transferrable to any other employee directly without approval from Central Admin.</p>
				

				<div class="container">
					<h3><center id="assetFrmName">Asset Allocation</center></h3>
						<table border=1>
							<tr>
								<td><b>Asset Type </b></td> 
								<td><b>Service Provider </b></td> 
								<td><b>Connection number </b></td> 
								<td><b>Current rental plan </b></td> 
								<td><b>Rental Amount </b></td> 
								<td><b>Monthly Limit </b></td> 
							</tr>
							<tr>
								<td>'.$asset_assign_data->asset_type.'</td> 
								<td>'.$asset_assign_data->service_provider.'</td> 
								<td>'.$asset_assign_data->connection_no.'</td> 
								<td>'.$asset_assign_data->current_rental_plan.'</td> 
								<td>'.$asset_assign_data->rental_amount.'</td> 
								<td>'.$asset_assign_data->monthly_limit.'</td> 
							</tr>
						</table>
				</div>		
				
				<br/>
				<p>For any further query, please write to HO-Admin@skeiron.com or Asha.Bhandwalkar@skeiron.com</p>
				<p>This is System generated email, Please do not replay.</p>';
				
				
				//$this->sendAdminMISExceedEmail('Admin Inventory',$acceptData->emp_email,$emailSubj,$emailMsg);
			
			
			
		}else{
			$this->session->set_flashdata('error_msg','Error occured while accepting... Please try again');
		}
		
	}
	
	public function edit_Admin_Asset($assetId){
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		//var_dump($postData);exit;
		$assetAry = array(
			'asset_type'=>$postData['asset_type'],
			'service_provider'=>$postData['service_provider'],
			'connection_no'=>$postData['connection_no'],
			'procured_date'=>date('Y-m-d',strtotime($postData['procured_date'])),
			'purchased_LOB_id'=>$postData['purchased_lob'],
			'purchased_LOB'=>getUserLOBNameBYId($postData['purchased_lob']),
			'purchased_company'=>$postData['purchased_company'],
			'purchased_company_name'=>getPurchasedCompny($postData['purchased_company']),
			'issued_on'=>date('Y-m-d',strtotime($postData['issued_on'])),
			'asset_location'=>$postData['asset_location'],
			'current_rental_plan'=>$postData['current_rental_plan'],
			'rental_amount'=>$postData['rental_amount'],
			'monthly_limit'=>$postData['monthly_limit'],
			'sim_no'=>$postData['sim_no'],
			'account_no'=>$postData['account_no'],
			'availablity_status'=>$postData['availablity_status'],
			'asset_is_active'=>$postData['working_status']
			
		);
		//var_dump($assetAry);exit;
		if($this->Admin_Inventory_model->edit_admin_asset($assetAry,$assetId)){
			$this->session->set_flashdata('msg','Asset edited successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manage_asset_admin');
	}
	public function admin_asset_list(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['action_tab'] = 'admin_asset_list';
		$data['asset_data'] = $this->Admin_Inventory_model->get_all_adminassignasset();
		$data['include'] = 'backend_iconnect/admin_inventory/admin_asset_list';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function admin_add_asset(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/admin_inventory/admin_add_new_asset';
		$data['asset_code'] = date('dmYImi');
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function addAdminAsset(){
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		
		$assetAry = array(
			'asset_type'=>$postData['asset_type'],
			'procured_date'=>date('Y-m-d',strtotime($postData['procured_date'])),
			'purchased_company'=>$postData['purchased_company'],
			'purchased_company_name'=>getPurchasedCompny($postData['purchased_company']),
			'purchased_LOB'=>$postData['purchased_lob'],
			'purchased_LOB_id'=>getLOBID($postData['purchased_lob']),
			'service_provider'=>$postData['service_provider'],
			'connection_no'=>$postData['connection_no'],
			'wef'=>date('Y-m-d',strtotime($postData['wef'])),
			'asset_location'=>$postData['asset_location'],
			'current_rental_plan'=>$postData['current_rental_plan'],
			'rental_amount'=>$postData['rental_amount'],
			'monthly_limit'=>$postData['monthly_limit'],
			'sim_no'=>$postData['sim_no'],
			'account_no'=>$postData['account_no'],
			'asset_is_active'=>'Active',
			'availablity_status'=>'stock'
		);
		if($this->Admin_Inventory_model->add_asset($assetAry)){
			//for sub inventory items
			$insert_id = $this->db->insert_id();
			
			$this->session->set_flashdata('msg','Asset added successfully');
			 
	
			
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/manage_asset_admin');
		
	}
	
	public function admin_edit_asset($assetId=0){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['asset_data'] = $this->Admin_Inventory_model->get_adminasset_by_id($assetId);
		$data['include'] = 'backend_iconnect/admin_inventory/admin_edit_new_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function editAdminAsset($assetId){
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		$assetAry = array(
			'asset_type'=>$postData['asset_type'],
			'service_provider'=>$postData['service_provider'],
			'connection_no'=>$postData['connection_no'],
			'user'=>$postData['user'],
			'emp_code'=>$postData['emp_code'],
			'email'=>$postData['email'],
			'user_location'=>$postData['user_location'],
			'user_company'=>$postData['user_company'],
			'user_company_brand'=>getUserLOBNameBYId($postData['user_lob']),
			'user_lob_id'=>$postData['user_lob'],
			'procured_date'=>date('Y-m-d',strtotime($postData['procured_date'])),
			'purchased_company'=>$postData['purchased_company'],
			'purchased_company_name'=>getPurchasedCompny($postData['purchased_company']),
			'issued_on'=>date('Y-m-d',strtotime($postData['issued_on'])),
			'asset_location'=>$postData['asset_location'],
			'current_rental_plan'=>$postData['current_rental_plan'],
			'rental_amount'=>$postData['rental_amount'],
			'monthly_limit'=>$postData['monthly_limit'],
			'sim_no'=>$postData['sim_no'],
			'account_no'=>$postData['account_no']
			
		);
		if($this->Admin_Inventory_model->edit_admin_asset($assetAry,$assetId)){
			$this->session->set_flashdata('msg','Asset edited successfully');
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('admin/admin_asset_list');
	}
	
	public function admin_return_asset($assetID){
		$data = $this->get_ledap();
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		$data['asset_data'] = $this->Admin_Inventory_model->get_adminasset_by_id($assetID);
		$data['include'] = 'backend_iconnect/admin_inventory/admin_return_asset';
		$this->load->view('backend_iconnect/masterpage',$data);
		
		
	}
	
	public function returnAdminAsset($assetID){
		$data = $this->get_ledap();
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		//var_dump($postData);exit;
		
		$retrunAssetarray = array(
			'asset_id'=>$assetID,
			'asset_type'=>$postData['asset_type'],
			'user'=>$postData['user'],
			'emp_code'=>$postData['emp_code'],
			'email'=>$postData['email'],
			'user_location'=>$postData['user_location'],
			'user_company'=>$postData['user_company'],
			'return_by'=>$data['user_ldamp']['name'],
			'return_date'=>date('Y-m-d'),
			'return_remark'=>$postData['asset_received_remark']
		);
		$this->Admin_Inventory_model->addReturn_admin_asset($retrunAssetarray);
		
		
		$assetAry = array(
			'user'=>'',
			'emp_code'=>'',
			'email'=>'',
			'user_location'=>'',
			'user_company'=>'',
			'user_company_brand'=>'',
			'user_lob_id'=>'',
			'issued_on'=>'',
			'grade'=>''
		);
		$this->Admin_Inventory_model->edit_admin_asset($assetAry,$assetID);
		
		$this->db->where('id', $assetID);
		$this->db->update("e_admin_inventory", array('return_remark' => $postData['asset_received_remark'],'availablity_status'=>'stock'));
		
		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('Asset received successfully');
		}
		redirect('admin/admin_asset_list');
	}
	public function view_asset_details($assetId = 0,$assign_asset_id=0){
		$this->load->model('Admin_Inventory_model');
		$data['assign_asset_id'] = $assign_asset_id;
		$data['asset_data'] = $this->Admin_Inventory_model->get_asset($assetId);
		$data['return_history'] = $this->Admin_Inventory_model->get_asset_history($assetId);
		$this->load->view('backend_iconnect/admin_inventory/view_asset_details',$data);
	}
	
	public function admin_lob_report(){
		$data = $this->get_ledap();
		$this->load->model('Admin_Inventory_model');
		$data['LOB_data'] = getAdminReportData_new('getLOB');
		$data['asset_data'] = getAdminReportData_new('getAssetype');
		$data['CountMSC'] = getAssetCountMSC();
		$data['CountMVTS'] = getAssetCountMVTS();
		$data['CountDC'] = getAssetCountDC();
		$data['CountTF'] = getAssetCountTF();
		$data['include'] = 'backend_iconnect/admin_inventory/admin_lob_report';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	
	public function admin_asset_dashboard(){
		$data = $this->get_ledap();
		
		$data['asset_wise_stock'] = getAdminReportData_new('getAssetWise',array('assetType'=>'stock'));
		$data['asset_wise_in_use'] = getAdminReportData_new('getAssetWise',array('assetType'=>'assign'));
		
		
		$data['LOB_data'] = getAdminReportData_new('getLOB');
		
		$data['company_wise_MVTS'] = getAdminReportData_new('getCompanyWise',array('assetType'=>'Mobile (VTS)'));
		$data['company_wise_MSC'] = getAdminReportData_new('getCompanyWise',array('assetType'=>'Mobile SIM card'));
		$data['company_wise_TF'] = getAdminReportData_new('getCompanyWise',array('assetType'=>'Toll free'));
		$data['company_wise_WDC'] = getAdminReportData_new('getCompanyWise',array('assetType'=>'Data Card'));
		
		$data['action_tab'] = 'asset_report';
		$data['include'] = 'backend_iconnect/admin_inventory/reportPage_new';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function ajx_admin_mis_report_new() {
		$postData = $this->input->post();
		//print_r($postData);exit;
		//echo $postData['key_search_secondary'];
		switch($postData['type']){
			case 'asset_wise':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE asset_type = '".$postData['key_search_primary']."' AND availablity_status='".$postData['key_search_secondary']."'");
				$resultArry = $qry->result();
			break;
			
			case 'stock_wise':
				$this->db->query("SELECT * FROM e_admin_inventory WHERE asset_type = '".$postData['key_search_primary']."' AND availablity_status='".$postData['key_search_secondary']."'");
				$resultArry = $qry->result();
			break;

			case 'company_wise':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_company ='".$postData['key_search_primary']."' AND asset_type='".$postData['key_search_secondary']."'");
				$resultArry = $qry->result();
			break;
			
			case 'lob_asset_wise':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."' AND asset_type='".$postData['key_search_secondary']."' AND availablity_status='".$postData['stockType']."'");
				$resultArry = $qry->result();
			break;
			
			case 'lob_compny_asset_wise':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."' AND purchased_company='".$postData['key_search_secondary']."' AND asset_type='".$postData['stockType']."'");
				$resultArry = $qry->result();
				//echo $this->db->last_query(); die;
			break;
			case 'lob_asset':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."'");
				$resultArry = $qry->result();
				//echo $this->db->last_query(); die;
			break;
			
			case 'total_assets':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE asset_type ='".$postData['stockType']."'");
				$resultArry = $qry->result();
				//echo $this->db->last_query(); die;
			break;
		}
		
			foreach($resultArry as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryO[] = $returncompanyAry;
			}
				$data['mis_data'] = $returnAryO;
				$this->load->view('backend_iconnect/admin_inventory/report_ajax_sys_new',$data);
				//echo $this->db->last_query(); die;
	}
	
	public function ajx_admin_mis_report_new1() {
		$postData = $this->input->post();
		//print_r($postData);exit;
		//echo $postData['key_search_secondary'];
		switch($postData['type']){
		
			case 'lob_asset':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."'");
				$resultArry = $qry->result();
				//echo $this->db->last_query(); die;
			break;
			
			
		}
		
			foreach($resultArry as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryO[] = $returncompanyAry;
			}
			
			
				
				$data['LOB']=$postData['key_search_primary'];
				
				/// Mobile Sim card
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."' AND asset_type='Mobile SIM card'");
				$resultArryMSC = $qry->result();
				foreach($resultArryMSC as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryOMSC[] = $returncompanyAry;
				}
				
				$data['mis_dataMSC'] = $returnAryOMSC;
				
				/// Mobile Sim card end
				
				/// Mobile VTS
				
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."' AND asset_type='Mobile (VTS)'");
				$resultArryMVTS = $qry->result();
				foreach($resultArryMVTS as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryOMVTS[] = $returncompanyAry;
				}
				
				$data['mis_dataMVTS'] = $returnAryOMVTS;
				
				/// Mobile VTS end
				
				/// Toll free
				
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."' AND asset_type='Toll free'");
				$resultArryTF = $qry->result();
				foreach($resultArryTF as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryOTF[] = $returncompanyAry;
				}
				
				$data['mis_dataTF'] = $returnAryOTF;
				
				/// Toll free end
				
				/// Data card
				
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE purchased_LOB ='".$postData['key_search_primary']."' AND asset_type='Data Card'");
				$resultArryDC = $qry->result();
				foreach($resultArryDC as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryODC[] = $returncompanyAry;
				}
				
				$data['mis_dataDC'] = $returnAryODC;
				
				/// Data card end
				
				
				$data['mis_data'] = $returnAryO;
				$this->load->view('backend_iconnect/admin_inventory/report_ajax_sys_new1',$data);
				//echo $this->db->last_query(); die;
	}
	
	public function ajx_admin_mis_report_new_user(){
		$postData = $this->input->post();
		//print_r($postData);exit;
		//echo $postData['key_search_secondary'];
		switch($postData['type']){
		
			case 'user_lob_asset':
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE user_company_brand ='".$postData['key_search_primary']."'");
				$resultArry = $qry->result();
				//echo $this->db->last_query(); die;
			break;
			
			
		}
		
			foreach($resultArry as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryO[] = $returncompanyAry;
			}
			
			
				
				$data['LOB']=$postData['key_search_primary'];
				
				/// Mobile Sim card
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE user_company_brand ='".$postData['key_search_primary']."' AND asset_type='Mobile SIM card'");
				$resultArryMSC = $qry->result();
				foreach($resultArryMSC as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryOMSC[] = $returncompanyAry;
				}
				
				$data['mis_dataMSC'] = $returnAryOMSC;
				
				/// Mobile Sim card end
				
				/// Mobile VTS
				
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE user_company_brand ='".$postData['key_search_primary']."' AND asset_type='Mobile (VTS)'");
				$resultArryMVTS = $qry->result();
				foreach($resultArryMVTS as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryOMVTS[] = $returncompanyAry;
				}
				
				$data['mis_dataMVTS'] = $returnAryOMVTS;
				
				/// Mobile VTS end
				
				/// Toll free
				
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE user_company_brand ='".$postData['key_search_primary']."' AND asset_type='Toll free'");
				$resultArryTF = $qry->result();
				foreach($resultArryTF as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryOTF[] = $returncompanyAry;
				}
				
				$data['mis_dataTF'] = $returnAryOTF;
				
				/// Toll free end
				
				/// Data card
				
				$qry = $this->db->query("SELECT * FROM e_admin_inventory WHERE user_company_brand ='".$postData['key_search_primary']."' AND asset_type='Data Card'");
				$resultArryDC = $qry->result();
				foreach($resultArryDC as $recordData){
					$returncompanyAry = array(
						'purchased_LOB'=>$recordData->purchased_LOB,
						'purchased_company_name'=>$recordData->purchased_company_name,
					    'asset_type'=>$recordData->asset_type,
						'service_provider'=>$recordData->service_provider,
						'connection_no'=>$recordData->connection_no,
						'procured_date'=>$recordData->procured_date,
						'current_rental_plan'=>$recordData->current_rental_plan,
						'rental_amount'=>$recordData->rental_amount,
						'user'=>$recordData->user,
						'email'=>$recordData->email,
						'user_company'=>$recordData->user_company,
						'status'=>$recordData->availablity_status
					);
					$returnAryODC[] = $returncompanyAry;
				}
				
				$data['mis_dataDC'] = $returnAryODC;
				
				/// Data card end
				
				
				$data['mis_data'] = $returnAryO;
				$this->load->view('backend_iconnect/admin_inventory/report_ajax_sys_new_user',$data);
				//echo $this->db->last_query(); die;
		
	}
	public function upload_excel(){
		//echo "text";
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/admin_inventory/upload_excel_form';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public	function ExcelDataAdd()	{ 
	$data = $this->get_ledap();	
	$this->load->library('excel');
	//echo $_FILES["file"]["name"];
	 if(isset($_FILES["file"]["name"]))
		  {
		   $path = $_FILES["file"]["tmp_name"];
		   $object = PHPExcel_IOFactory::load($path);
		   foreach($object->getWorksheetIterator() as $worksheet)
		   {
			$highestRow = $worksheet->getHighestRow();
			//echo $highestRow;
			$highestColumn = $worksheet->getHighestColumn();
			//echo $highestColumn;
			for($row=2; $row<=$highestRow; $row++)
			{

			// $id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
			 $entry_date = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
			 $company = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
			 $email = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
			 $mobile_number = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
			 $asset_type = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
			 $user_name = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
			 $service_provider = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
			 $emp_code = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
			 $user_company = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
			 $location = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
			 $brand = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
			 $period = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
			 $monthly_limit = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
			 $bill_amount = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
			 $late_fee = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
			 $claimed_amount = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
			 $excess = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
			 $deduction_to_be_done = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
			 $account_no = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
			 $entry_in_Deduction_list = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
			 $bill_no = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
			 $bill_date = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
			 $consumption_period = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
			 
			
				 $insert_ary = array(
				  'entry_date'  => $entry_date,
				  'company'   => $company,
				  'email'    => $email,
				  'mobile_number'  => $mobile_number,
				  'asset_type'   => $asset_type,
				  'user_name' => $user_name,
				  'service_provider'  => $service_provider,
				  'emp_code'   => $emp_code,
				  'user_company'    => $user_company,
				  'location'  => $location,
				  'brand'   => $brand,
				  'period' => $period,
				  'monthly_limit'  => $monthly_limit,
				  'bill_amount'   => $bill_amount,
				  'late_fee'    => $late_fee,
				  'claimed_amount'  => $claimed_amount,
				  'excess'   => $excess,
				  'deduction_to_be_done' => $deduction_to_be_done,
				  'account_no'  => $account_no,
				  'entry_in_Deduction_list'   => $entry_in_Deduction_list,
				  'bill_no'    => $bill_no,
				  'bill_date'  => $bill_date,
				  'consumption_period'   => $consumption_period,
				  'email_sent' => 0
				);
			$this->db->insert("e_admin_inv_email_trigger",$insert_ary);
			
			}
			 
		   }
			$this->session->set_flashdata('msg','Data Imported successfully');
			redirect("admin/upload_excel");
		  } 
		 
     }
	 
	public function clear_previous_data(){
		$data = $this->get_ledap();
		$this->db->empty_table("e_admin_inv_email_trigger");
		$qry = $this->db->query("SELECT * FROM e_admin_inv_email_trigger");
		$resultArry = $qry->result();
		if(count($resultArry) == 0){
			$this->session->set_flashdata('msg','Previous data has been deleted');
			redirect("admin/upload_excel");
		}
	}
	public function exceed_limit(){
		$qry = $this->db->query("SELECT * FROM e_admin_inv_email_trigger");
		$resultArry = $qry->result();
		foreach($resultArry as $data){
			//$exceed = $data->bill_amount - $data->monthly_limit;
			$excess = str_replace(array( '(' , ')' ), '', $data->excess);
			if($excess > 0){
				$emailSubj ='Intimation - Mobile Excess Usage Deduction';
				$emailMsg = 'Dear '.ucfirst($data->user_name).',<br>
				<p>Please be inform that we are sharing your Sim Card ( SIM mobile no ) usage of mobile and data card allotted to you.<p>
				<p>Given below are the details excess usage of your mobile / data card processed recently which is exceeding grade wise limit.</p>
                <br/>
				
						<table border=1>
							<tr>
								<td><b>User Name</b></td> 
								<td><b>User Company</b></td> 
								<td><b>LOB</b></td> 
								<td><b>Mobile Number</b></td> 
								<td><b>Service Provider </b></td> 
								<td><b>Bill Period</b></td> 
								<td><b>Total montly limit </b></td> 
								<td><b>Amount </b></td> 
								<td><b>Excess </b></td> 
							</tr>
							<tr>
								<td>'.$data->user_name.'</td> 
								<td>'.$data->user_company.'</td> 
								<td>'.$data->brand.'</td> 
								<td>'.$data->mobile_number.'</td> 
								<td>'.$data->service_provider.'</td> 
								<td>'.$data->period.'</td> 
								<td align="right">'.$data->monthly_limit.'</td> 
								<td align="right">'.$data->bill_amount.'</td> 
								<td align="right">'.$data->excess.'</td> 
							</tr>
						</table>
				</div>		
				<p>Pls note that the amount mentioned as Excess shall be deducted and adjusted in your monthly payroll salary.</p>
				<p>If you have any queries please get InTouch with central Admin at HO.</p>
				<p>HO-Admin@skeiron.com or Asha.Bhandwalkar@skeiron.com</p>
				
				
				<p>	*** This is an automatically generated email, please do not reply *** </p><br>';
				
				
				//$this->sendAdminMISExceedEmail('Admin Inventory',$data->email,$emailSubj,$emailMsg);
				
				/*
				$update_array = array('email_sent'=>1);
				$this->db->where('id',$data->id);
				$this->db->update('e_admin_inv_email_trigger',$update_array);
				*/
				echo "email send to user ".$data->email;
				echo "<br/>";
				
				
			}
			
		}
		
	}
	public function sendAdminAssignAssetEmail($fromName='',$toEmail='', $subject='',$mesge='',$cc){
		ob_start();
		error_reporting(0);		
		$smu =smu;
		$smp=smp;
		$mail = '';
		$account=$smu;
		$password=$smp;
		$from=$smu;
		$to=$toEmail;
		$from_name="Skeiron Admin";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		$ccmail = 'Ho-Admin@skeiron.com,Vijay.Maurya@skeiron.com,';
		$addcc = $ccmail.$cc;
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $addcc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($addcc);
		}
		
		if(!$mail->send()){
			echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			echo "<br>E-Mail has been sent";
		}
		return true;
	}
	public function sendAdminMISExceedEmail($fromName='',$toEmail='', $subject='',$mesge=''){
		ob_start();
		error_reporting(0);		
		$smu =smu;
		$smp=smp;
		$mail = '';
		$account=$smu;
		$password=$smp;
		$from=$smu;
		$to=$toEmail;
		$from_name="Skeiron Admin";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		$cc = 'Ho-Admin@skeiron.com,Vijay.Maurya@skeiron.com';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	public function download_Admin_MIS(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['action_tab']='download_Admin_MIS';
		$data['asset_data'] = $this->Admin_Inventory_model->get_all_adminasset();
		$data['admin_LOB'] = $this->Admin_Inventory_model->get_admin_LOB();
		$data['admin_assets'] = $this->Admin_Inventory_model->get_admin_asset_type();
		$this->load->view('backend_iconnect/admin_inventory/download_Admin_MIS',$data);
	}
	
	public function getAdminReportDownloadV2(){
		ob_start();
		$postData = $this->input->post();
		if($postData['download_btn'] == 'Download xls'){
			$this->downloadAdminReportV2();
		}
		
	}
	
	public function downloadAdminReportV2(){
		$this->load->model('Admin_Inventory_model');
		$postData = $this->input->post();
		$customReport = $this->Admin_Inventory_model->get_all_adminasset();
	    $contents ='"Sr No","Procure Date","Purchased LOB","Purchased company","Asset Type","Service Provider","Connection Number","Account Number","User Name","Email","Emp Code","Grade","User LOB","User Company","User Location","Issued on","Current rental plan","Rental Amount","Monthly Limit","Status","Working Status"';
	    $contents .="\n";
		$i = 1;
	foreach($customReport as $recordData){
		
		$contents.='"'.$i.'","'.$recordData->procured_date.'","'.$recordData->purchased_LOB.'","'.$recordData->purchased_company_name.'","'.$recordData->asset_type.'","'.$recordData->service_provider.'","'.$recordData->connection_no.'","'.$recordData->account_no.'","'.$recordData->user.'","'.$recordData->email.'","'.$recordData->emp_code.'","'.$recordData->grade.'","'.$recordData->user_company_brand.'","'.$recordData->user_company.'","'.$recordData->user_location.'","'.$recordData->issued_on.'","'.$recordData->current_rental_plan.'","'.$recordData->rental_amount.'","'.$recordData->monthly_limit.'","'.$recordData->availablity_status.'","'.$recordData->asset_is_active.'"';
		$contents.="\n";
		$i++;
	}
	    $contents = strip_tags($contents);
	    Header("Content-Disposition: attachment;filename=Admin_assets.csv ");
	    print $contents;
	    exit;
	}
	
	
	public function non_accepted_assets_user(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/admin_inventory/non_accepted_assets_user';
		$data['non_accepted_user'] = $this->Admin_Inventory_model->non_accepted_assets_user();
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function non_accepted_email_send($asset_id){
		$data = $this->get_ledap();
		$this->load->model('Admin_Inventory_model');
		$users = $this->Admin_Inventory_model->get_user_email_byassetID($asset_id);
		$email= $users->emp_email;
		
				$emailSubj ='Reminder';
				$emailMsg = 'Dear '.$users->emp_name.',<br>
				<p>With reference to below asset already issued to you on date - '.date('d-m-Y',strtotime($users->assign_date)).', we wish to remind you that the same has not been accepted by you yet.<p>
				<p><b>Kindly note that the services of the asset will be disconnected in case of further non-acceptance from your side.</b></p>
				<p>Hence request you to kindly accept the same by clicking on the hyperlink below.</p>
				<div class="container">
					<h3><center id="assetFrmName">Asset Allocation</center></h3>
						<table border=1>
							<tr>
								<td><b>Asset Type: </b></td> 
								<td><b>Service Provider: </b></td> 
								<td><b>Connection number: </b></td> 
								<td><b>Current rental plan: </b></td> 
								<td><b>Rental Amount: </b></td> 
								<td><b>Monthly Limit: </b></td> 
							</tr>
							<tr>
								<td>'.$users->asset_type.'</td> 
								<td>'.$users->service_provider.'</td> 
								<td>'.$users->connection_no.'</td> 
								<td>'.$users->current_rental_plan.'</td> 
								<td>'.$users->rental_amount.'</td> 
								<td>'.$users->monthly_limit.'</td> 
							</tr>
						</table>
				</div>	
				<p>Please accept the asset by clicking below <a href="'.base_url().'admin/admin_assign_asset_acceptance/'.$users->asset_id.'" class="btn btn-primary"> I accept </a>link.</p>
				<p>Kindly Note:-Make sure you are logged in to http://intranet.skeiron.com Before clicking on accept button.</p>
				<p><b>Please note that the asset will be made Active for use only after your acceptance.</b></p>
				<p>For any further query, please write to HO-Admin@skeiron.com or Asha.Bhandwalkar@skeiron.com.</p>
				
				<p>	*** This is an auto generated email, please do not reply *** </p><br>';
				if($this->sendAdminMISExceedEmail('Admin Inventory',$email,$emailSubj,$emailMsg)){
					$this->session->set_flashdata('msg','Email sent successfully');
					redirect('admin/non_accepted_assets_user');
				}
		
	}
	
	public function send_admin_bulkemail(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$asset_id = $_POST['checklist'];

		foreach ($asset_id as $asset_id=>$assetval) {
			$nonAcpusers = $this->Admin_Inventory_model->get_non_accepted_assets_user($assetval);
            foreach ($nonAcpusers as $users){
				$email= $users->emp_email;
				$emailSubj ='Remainder';
				$emailMsg = 'Dear '.$users->emp_name.',<br>
				<p>With reference to below asset already issued to you on date - '.date('d-m-Y',strtotime($users->assign_date)).', we wish to remind you that the same has not been accepted by you yet.<p>
				<p><b>Kindly note that the services of the asset will be disconnected in case of further non-acceptance from your side.</b></p>
				<p>Hence request you to kindly accept the same by clicking on the hyperlink below.</p>
				<div class="container">
					<h3><center id="assetFrmName">Asset Allocation</center></h3>
						<table border=1>
							<tr>
								<td><b>Asset Type: </b></td> 
								<td><b>Service Provider: </b></td> 
								<td><b>Connection number: </b></td> 
								<td><b>Current rental plan: </b></td> 
								<td><b>Rental Amount: </b></td> 
								<td><b>Monthly Limit: </b></td> 
							</tr>
							<tr>
								<td>'.$users->asset_type.'</td> 
								<td>'.$users->service_provider.'</td> 
								<td>'.$users->connection_no.'</td> 
								<td>'.$users->current_rental_plan.'</td> 
								<td>'.$users->rental_amount.'</td> 
								<td>'.$users->monthly_limit.'</td> 
							</tr>
						</table>
				</div>	
				<p>Please accept the asset by clicking below <a href="'.base_url().'admin/admin_assign_asset_acceptance/'.$users->asset_id.'" class="btn btn-primary"> I accept </a>link.</p>
				<p>Kindly Note:-Make sure you are logged in to http://intranet.skeiron.com Before clicking on accept button.</p>
				<p><b>Please note that the asset will be made Active for use only after your acceptance.</b></p>
				<p>For any further query, please write to HO-Admin@skeiron.com or Asha.Bhandwalkar@skeiron.com.</p>
				
				<p>	*** This is an auto generated email, please do not reply *** </p><br>';
				if($this->sendAdminMISExceedEmail('Admin Inventory',$email,$emailSubj,$emailMsg)){
					$this->session->set_flashdata('msg','Email sent successfully');
					redirect('admin/non_accepted_assets_user');
				}
			
			}
        }
		
		
	}
	/*
	public function LOB_MIS(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS';
		$data['all_LOB']=$this->Admin_Inventory_model->get_admin_LOB();
		$this->load->view('backend_iconnect/masterpage',$data);
	}*/
	public function getLOB_MIS($lobID){
		$postData = $this->input->post();
		
		$this->load->model('Admin_Inventory_model');
		$customReport = $this->Admin_Inventory_model->getLOB_MIS($lobID);
		$lob_hod = $this->Admin_Inventory_model->getLOB_HOD($lobID);
		$hod = $lob_hod->lob_hod;
		$lob_name = $this->Admin_Inventory_model->get_admin_LOB_by_ID($lobID);
		$lobName = $lob_name->lob_name;
		
		$contents ='"Sr No","Procure Date","Purchased LOB","Purchased company","Asset Type","Service Provider","Connection Number","Account Number","User Name","Email","User LOB","User Company","Issued on","Current rental plan","Rental Amount","Monthly Limit","Status"';
	    $contents .="\n";
		$i = 1;
		foreach($customReport as $recordData){
			
			$contents.='"'.$i.'","'.$recordData->procured_date.'","'.$recordData->purchased_LOB.'","'.$recordData->purchased_company_name.'","'.$recordData->asset_type.'","'.$recordData->service_provider.'","'.$recordData->connection_no.'","'.$recordData->account_no.'","'.$recordData->user.'","'.$recordData->email.'","'.$recordData->user_company_brand.'","'.$recordData->user_company.'","'.$recordData->issued_on.'","'.$recordData->current_rental_plan.'","'.$recordData->rental_amount.'","'.$recordData->monthly_limit.'","'.$recordData->availablity_status.'"';
			$contents.="\n";
			$i++;
		}
	    $contents = strip_tags($contents);
		//$mail->AddAttachment(FCPATH."uploads/helpdesk_daily/".$fileName);
	    Header("Content-Disposition: attachment;filename=LOB-".$lobName."-MIS.csv");
		print $contents;
		
		//$this->sendAttachEmail($lobID,$lobName,$hod);
	    exit;
	}
	
	public function sendAttachEmail($lobID,$lobName,$hod){
		ob_start();
		error_reporting(0);
		$customReport = $this->Admin_Inventory_model->getLOB_MIS($lobID);
		$lob_hod = $this->Admin_Inventory_model->getLOB_HOD($lobID);
		$hod = $lob_hod->lob_hod;
		$subject = 'Data Card/SIM Card MIS Report for '.$lobName;
		$mesge = 'Dear Sir / Madam,<br><br>
				Please find Admin Inventory MIS report.<br><br>';
		$mesge .= '<table cellpadding="1" cellspacing="1" border="1">';
		$mesge .= '<tr>
				  <th>Procured Date</th>
				  <th>Asset Type</th>
				  <th>Service Provider</th>
				  <th>Connection Number</th>
				  <th>User</th>
				  <th>User Company</th>
				  <th>User location</th>
				  <th>Issued date</th>
				  
				  </tr>';
		
		foreach($customReport as $recordData){
			
			$mesge.='<tr>
			<td>'.$recordData->procured_date.'</td>
			<td>'.$recordData->asset_type.'</td>
			<td>'.$recordData->service_provider.'</td>
			<td>'.$recordData->connection_no.'</td>
			<td>'.$recordData->user.'</td>
			<td>'.$recordData->user_company.'</td>
			<td>'.$recordData->user_location.'</td>
			<td>'.$recordData->issued_on.'</td>
			</tr>';
			
		}
		$mesge .='</table>';
	    $mesge .= '<p>	*** This is an automatically generated email, please do not reply *** </p><br>';
		$mail = '';
		
		$toEmail = $hod;
		
		$cc = 'Ho-Admin@skeiron.com,Vijay.Maurya@skeiron.com,bhagyashree.holey@skeiron.com';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name="Helpdesk";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		//$header = "Content-Disposition: attachment;filename=LOB-".$lobName."-MIS.csv";
		//$mail->AddAttachment("LOB-".$lobName."-MIS.csv");
		if(!$mail->send()){
			echo "<br>Mailer Error: " . $mail->ErrorInfo;
			return false;
		}else{
			echo "<br>E-Mail has been sent";
			return true;
		}
	}
	public function sendEmailtoHOD($lobID){
		$postData = $this->input->post();
		
		$this->load->model('Admin_Inventory_model');
		ob_start();
		error_reporting(0);
		$customReport = $this->Admin_Inventory_model->getLOB_MIS($lobID);
		$lob_hod = $this->Admin_Inventory_model->getLOB_HOD($lobID);
		$hod = $lob_hod->lob_hod;
		$lob_name = $this->Admin_Inventory_model->get_admin_LOB_by_ID($lobID);
		$lobName = $lob_name->lob_name;
		
		$subject = 'Data Card/SIM Card MIS Report for '.$lobName;
		$mesge = 'Dear Sir / Madam,<br><br>
				Please find Admin Inventory MIS report.<br><br>';
		$mesge .= '<table cellpadding="1" cellspacing="1" border="1">';
		$mesge .= '<tr>
				  <th>Procured Date</th>
				  <th>Asset Type</th>
				  <th>Service Provider</th>
				  <th>Connection Number</th>
				  <th>Current Rental Plan</th>
				  <th>User</th>
				  <th>User Company</th>
				  <th>User location</th>
				  <th>Issued date</th>
				  
				  </tr>';
		
		foreach($customReport as $recordData){
			
			$mesge.='<tr>
			<td>'.$recordData->procured_date.'</td>
			<td>'.$recordData->asset_type.'</td>
			<td>'.$recordData->service_provider.'</td>
			<td>'.$recordData->connection_no.'</td>
			<td>'.$recordData->current_rental_plan.'</td>
			<td>'.$recordData->user.'</td>
			<td>'.$recordData->user_company.'</td>
			<td>'.$recordData->user_location.'</td>
			<td>'.$recordData->issued_on.'</td>
			</tr>';
			
		}
		$mesge .='</table>';
	    $mesge .= '<p>	*** This is an automatically generated email, please do not reply *** </p><br>';
		$mail = '';
		
		$toEmail = $hod;
		
		//$cc = 'Ho-Admin@skeiron.com,Vijay.Maurya@skeiron.com,bhagyashree.holey@skeiron.com';
		$cc = 'bhagyashree.holey@skeiron.com';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name="Helpdesk";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
			if(!$mail->send()){
			echo "<br>Mailer Error: " . $mail->ErrorInfo;
			
			return false;
		}else{
			$this->session->set_flashdata('msg','E-Mail has been sent');
			redirect("admin/LOB_MIS_Report");
			return true;
		}
	}
	
	public function LOB_MIS_Report(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_Report';
		$data['all_LOB']=$this->Admin_Inventory_model->get_admin_LOB();
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	/*
	public function LOB_MIS_Infrastructures(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_Infrastructures';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function LOB_MIS_Logistics(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_Logistics';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function LOB_MIS_Projects(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_Projects';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function LOB_MIS_Renewables(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_Renewables';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function LOB_MIS_Group(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_Group';
		
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function LOB_MIS_ProjectsANDGroups(){
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/admin_inventory/LOB_MIS_ProjectsANDGroups';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	*/
	/*********Admin asset managment end*********/
	public function share_drive(){
		
		$this->load->model('Admin_Inventory_model');
		$data = $this->get_ledap();
		$data['share_drive_data'] = $this->Admin_model->get_all_share_documents();
		$data['include'] = 'backend_iconnect/share_drive/share_drive';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function share_drive_document()
	{	
		$data = $this->get_ledap();
		$data['my_title']  = 'Upload Document';
		$data['share_drive_data'] = $this->Admin_model->get_all_share_documents();
		$data['latest_share_drive_data'] = $this->Admin_model->get_all_share_documentsForAWeek();
		$data['action_tab'] = 'share_drive_document';
		$data['include'] = 'backend_iconnect/share_drive/share_drive_document';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	public function upload_documents(){
		$this->load->helper(array('form', 'url'));
		$postData = $this->input->post();
		$data['action'] = 'add';
		$this->load->view('backend_iconnect/share_drive/upload_documents',$data);
	}
	
	public function added_share_document(){
		$postData = $this->input->post();
		$folder = $postData['folderName'];
		$directoryName = 'uploads/announcement_tv/share/'.$folder.'/';
 
		//Check if the directory already exists.
		if(!is_dir($directoryName)){
			//Directory does not exist, so lets create it.
			mkdir($directoryName, 0777, true);
		}
		//echo $folder;exit;
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['ldamp_name'] = $data['user_ldamp']['name'];
		$announcement_dir = $directoryName;
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			//$file_announcement = "share" . '_' . random_string('alnum', 20) . "." . $ext[1];
			$file_announcement = $_FILES['announcement_file']['name'];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$insert_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>$data['ldamp_name'],
				'an_status'=>1,
				'folder_name'=>$folder
			);
			$this->db->insert("documents",$insert_ary);
			
			//add user details - track who added mailer
			$annoucements_id =  $this->db->insert_id();
			//$this->track_user_activity_tv($annoucements_id, 'announcement_tv_uploaded');
			
			$this->session->set_flashdata('message_submit', 'Record inserted successfully');
		}
		redirect("admin/share_drive_document");
	}
	
	public function edit_upload_announcements(){
		$postData = $this->input->post();
		$data['action'] = 'edit';
		$data['announcements_data'] = $this->Admin_model->get_share_documents_by_id($postData['recd_id']);
		$this->load->view('backend_iconnect/share_drive/edit_upload_announcements',$data);
	}
	
	public function edited_share_announcement(){
		$postData = $this->input->post();
		$folder = $postData['folder_name'];
		$directoryName = 'uploads/announcement_tv/share/'.$folder.'/';
 
		
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['ldamp_name'] = $data['user_ldamp']['name'];
		$announcement_dir = $directoryName;
		$file_name = '';
		$record_image = $_FILES['announcement_file']['name'];
		if (!empty($record_image)) {
			$ext = explode('.', $_FILES['announcement_file']['name']);
			//$file_announcement = "share" . '_' . random_string('alnum', 20) . "." . $ext[1];
			$file_announcement = $_FILES['announcement_file']['name'];
			move_uploaded_file($_FILES['announcement_file']['tmp_name'], $announcement_dir . $file_announcement);
			$file_name = $file_announcement;
			
			$update_ary = array(
				'an_file'=>$file_name,
				'an_text'=>'',
				'an_date'=>date('Y-m-d'),
				'an_created_by'=>$data['ldamp_name'],
				'an_status'=>1,
				'folder_name'=>$folder
			);
			$this->db->where("an_id",$postData['anno_id']);
			$this->db->update("documents",$update_ary);
			
			//$this->track_user_activity($postData['anno_id'], 'annuncement_updated');
			
			$this->session->set_flashdata('message_submit', 'Record updated successfully');
		}
		redirect("admin/share_drive_document");
		
	}
	
	public function remove_share_drive_doc(){
		   $postData = $this->input->post();
		   $data =  $this->Admin_model->get_share_documents_by_id($postData['recd_id']);
		  
		   if(count($data) > 0){
			//echo "<pre>";print_r($data);
			$ann_file = $data->an_file;
			$folder_name = $data->folder_name;
			//echo $ann_file;die;
			$path = 'uploads/announcement_tv/share/'.$folder_name.'/'.$ann_file;
			if(isset($path)){
				$delete_ary = array(
					'is_deleted'=>1,
					'an_status'=>0
				);
				$this->db->where("an_id",$postData['recd_id']);
		        $this->db->update("documents",$delete_ary);
				///unlink($path);
				//$this->track_user_activity($postData['recd_id'], 'announcement_tv_deleted');
			    $this->session->set_flashdata('message_submit', 'Record removed successfully');
			    $status = "success";
			}
			else
			{
				$status = "failed";
			}
		}
		else{
			$status = "failed";
		}
		echo $status;die;
	}
	
	public function update_admin_asset(){
		$assetAry = array(
			'emp_code'=>'SG0555',
			'user_company'=>'Shivam Filaments Pvt Ltd'
		);
		$assetId = '9370954829';
		$this->Admin_model->update_asset($assetAry,$assetId);
		
	}
	
	
	public function updateSGPPLtoSRLSG0211(){
		//admin_hd start
		$this->db->where('admin_emp_code', 'SG0211');
		$this->db->update("admin_hd", array('admin_emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-admin_hd";echo "<br/>";
		}
		//admin_hd ends
		
		//corp_suggestion start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("corp_suggestion", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-corp_suggestion";echo "<br/>";
		}
		//corp_suggestion ends
	
		//daily_task start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("daily_task", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-daily_task";echo "<br/>";
		}
		//daily_task ends
		
		
		//emp_hr_data start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("emp_hr_data", array('emp_code' => 'SG0672','emp_company' => 'Sarjan Realities Limited'));
		
		if($this->db->affected_rows()>0){
			echo "success-emp_hr_data";echo "<br/>";
		}
		//emp_hr_data ends
		
		
		//escalated_task start
		$this->db->where('escalated_empcode', 'SG0211');
		$this->db->update("escalated_task", array('escalated_empcode' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-escalated_task";echo "<br/>";
		}
		//escalated_task ends
		
		
		//escalation_level_hd start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("escalation_level_hd", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-escalation_level_hd";echo "<br/>";
		}
		//escalation_level_hd ends
		
		
		//hod_in start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("hod_in", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-hod_in";echo "<br/>";
		}
		//hod_in ends
		
		//inv_asset_accept start
		$this->db->where('inv_emp_code', 'SG0211');
		$this->db->update("inv_asset_accept", array('inv_emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-inv_asset_accept";echo "<br/>";
		}
		//inv_asset_accept ends
		
		
		//inv_assign_asset_emp_map start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("inv_assign_asset_emp_map", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-inv_assign_asset_emp_map";echo "<br/>";
		}
		//inv_assign_asset_emp_map ends
		
		
		
		//technician_hd start
		$this->db->where('tech_code', 'SG0211');
		$this->db->update("technician_hd", array('tech_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-technician_hd";echo "<br/>";
		}
		//technician_hd ends
		
		
		//user_activity_track start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("user_activity_track", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_activity_track";echo "<br/>";
		}
		//user_activity_track ends
		
		
		//user_admin start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("user_admin", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_admin";echo "<br/>";
		}
		//user_admin ends
		
		
		//user_event_log start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("user_event_log", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_event_log";echo "<br/>";
		}
		//user_event_log ends
		
		
		//user_login_count start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("user_login_count", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_login_count";echo "<br/>";
		}
		//user_login_count ends
		
		
		//user_request_hd start
		$this->db->where('user_code', 'SG0211');
		$this->db->update("user_request_hd", array('user_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_request_hd";echo "<br/>";
		}
		//user_request_hd ends
		
		
		//user_request_notes_hd start
		$this->db->where('note_user_id', 'SG0211');
		$this->db->update("user_request_notes_hd", array('note_user_id' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_request_notes_hd";echo "<br/>";
		}
		//user_request_notes_hd ends
		
		
		//user_roles start
		$this->db->where('user_emp_code', 'SG0211');
		$this->db->update("user_roles", array('user_emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-user_roles";echo "<br/>";
		}
		//user_roles ends
		
		
		//users start
		$this->db->where('u_employeeid', 'SG0211');
		$this->db->update("users", array('u_employeeid' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-users";echo "<br/>";
		}
		//users ends
		
		
		//usr_module_permission start
		$this->db->where('emp_code', 'SG0211');
		$this->db->update("usr_module_permission", array('emp_code' => 'SG0672'));
		
		if($this->db->affected_rows()>0){
			echo "success-usr_module_permission";echo "<br/>";
		}
		//usr_module_permission ends
	
	}
}

