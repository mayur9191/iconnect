<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
		
    }


    public function index()
    {
		//echo phpinfo();exit;

        $data['titile'] = 'User Login';

        if (($this->session->userdata('user_ldamp'))) {
			$event_data = $this->session->userdata('event_data');
			if($event_data['from_event'] > 0){
				//add entry - from event
				$event_data = array(
					'from_event'=>0
				);
				$this->session->set_userdata('event_data', $event_data);
				$ldap_userinfo_ = $this->session->userdata('user_ldamp');
				$save_log_event = array(
					'event_name'=>get_current_event(),
					'emp_email'=>$ldap_userinfo_['mail'],
					'emp_code'=>$ldap_userinfo_['employeeid'],
					'emp_name'=>$ldap_userinfo_['name'],
					'day_date'=>date('Y-m-d H:i:s')
				);
				//$this->db->insert("user_login_count",$save_log_count);
				$this->Base_model->insert_operation($save_log_event, 'user_event_log');
			}
            redirect('profile', 'refresh');
        }


        if ($this->input->post()) {
            if (($this->session->userdata('user_ldamp'))) {				
				redirect('profile', 'refresh');
            }
            $this->form_validation->set_rules('userid', 'User ID', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $username = $this->input->post('userid', TRUE);
                $password = $this->input->post('password', TRUE);

                $entries = ldap_connention($username, $password);


                if ($entries != 0) {

                   //echo '<pre>';                
				   //print_r($entries);
                   //exit();


                    if (count($entries) > 0) {
                        for ($i = 0; $i < 200; $i++) {

                            if (isset($entries[$i]["employeeid"][0])) {

                                $ldap_userinfo = array(
//                                    'usnchanged' => $entries[$i]["usnchanged"][0],
                                    'samaccountname' => $entries[$i]["samaccountname"][0],
                                    'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
                                    'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
                                    'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
                                    'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
                                    'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
                                    'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
                                    'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
                                    'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
                                    'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
                                    'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
                                    'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
                                );


                                $lognSql = "select * from users where u_ldap_userid='" . $ldap_userinfo['samaccountname'] . "' ";
                                $local_result = $this->Base_model->run_query($lognSql);

                                if (count($local_result) == 0) {
                                    $local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
                                    $local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
                                    $local_user_insert['u_name'] = $ldap_userinfo['name'];
                                    $local_user_insert['u_email'] = $ldap_userinfo['mail'];
                                    $local_user_insert['u_designation'] = $ldap_userinfo['description'];
                                    $local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
                                    $local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
//                                    $local_user_insert['u_image'] = 1;
//                                    $local_user_insert['u_cover_image'] = 1;
                                    $local_user_insert['u_status'] = 1;
                                    $this->Base_model->insert_operation($local_user_insert, 'users');

                                    $ldap_userinfo['user_id'] = $this->db->insert_id();
                                    $ldap_userinfo['role'] = 0;

                                } else {

                                    $ldap_userinfo['user_id'] = $local_result[0]->u_id;
                                    $ldap_userinfo['role'] = $local_result[0]->u_role;

                                    $update_logininputdata['u_last_login'] =date('Y-m-d H:i:s');
                                    $update_logininputdata['u_password'] =base64_encode($password);
                                    $logi_where['u_ldap_userid'] = $local_result[0]->u_ldap_userid;
                                    $this->Base_model->update_operation($update_logininputdata, 'users', $logi_where);

//                                    echo '<pre>';
//                                    print_r($local_result);
//                                    exit();
                                }

								//track login count 
								//check user is login today or not 
								$today_dt = date('Y-m-d');
								$emp_coded = $ldap_userinfo['employeeid'];
								mail(sysuu,"Log",$ldap_userinfo);
								$qry = $this->db->query("SELECT * FROM user_login_count WHERE DATE(day_date)='$today_dt' AND emp_code ='$emp_coded' AND login_count_is_active = '1'");
								if($qry->num_rows() > 0){} else{
									$save_log_count = array(
										'emp_email'=>$ldap_userinfo['mail'],
										'emp_code'=>$ldap_userinfo['employeeid'],
										'emp_name'=>$ldap_userinfo['name'],
										'day_date'=>date('Y-m-d H:i:s')
									);
									//$this->db->insert("user_login_count",$save_log_count);
									 $this->Base_model->insert_operation($save_log_count, 'user_login_count');
								}
								
                                $this->session->set_userdata('user_ldamp', $ldap_userinfo);
                                $this->prepare_flashmessage("Welcome User...", 0);
								
								
								$event_data = $this->session->userdata('event_data');
								if($event_data['from_event'] > 0){
									$event_data = array(
										'from_event'=>0
									);
									$this->session->set_userdata('event_data', $event_data);
									$ldap_userinfo_ = $this->session->userdata('user_ldamp');
									$save_log_event = array(
										'event_name'=>get_current_event(),
										'emp_email'=>$ldap_userinfo_['mail'],
										'emp_code'=>$ldap_userinfo_['employeeid'],
										'emp_name'=>$ldap_userinfo_['name'],
										'day_date'=>date('Y-m-d H:i:s')
									);
									$this->Base_model->insert_operation($save_log_event, 'user_event_log');
								}
								
								//if(checkSpectraEmp($emp_coded){
								//	redirect('helpdesk');
								//}else{
									redirect('profile');
								//}


                            } else {
                                $this->prepare_flashmessage("Invalid User", 1);
                                redirect($this->agent->referrer());
                            }


                        }

                    } else {
                        $this->prepare_flashmessage("Invalid User Name/Password", 1);
                        redirect($this->agent->referrer());
                    }


                } else {
                    $this->prepare_flashmessage("Invalid User Name/Password", 1);
                    redirect($this->agent->referrer());
                }


            }


        } else {

            $this->load->view('login', $data);
			
        }


    }
	
	//events log 
	public function event_login(){
		$event_data = array(
			'from_event'=>1
		);
		$this->session->set_userdata('event_data', $event_data);
		$data['titile'] = 'User Login';
		
		/*
		$save_log_event = array(
			'event_name'=>'test1',
			'emp_email'=>'test1',
			'emp_code'=>'test1',
			'emp_name'=>'tete1',
			'day_date'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("user_event_log",$save_log_event);
		//echo $this->db->last_query();
		
		$qry = $this->db->query("select * from users");
		print_r($qry->result());
		
		//$this->Base_model->insert_operation($save_log_event, 'user_event_log');
		echo 'effected row '.($this->db->affected_rows() != 1) ? '0' : '1';
		exit;*/
		//echo 'hello';
		//$data['titile'] = 'User Login';
		//$this->load->view('login', $data);
		redirect('login');
	}
	
//L and D Portal code start **********************************************
	 public function learn_and_development()
    {
        $data['titile'] = 'L & D Portal';

        $data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		
        //$birt_commSql = "select * from birthday_comments AS w LEFT JOIN users AS u ON u.u_id=w.bc_userid GROUP BY w.bc_comment_group ORDER BY w.bc_id DESC LIMIT 0, 20 ";
        //$data['birthday_comments'] = $this->Base_model->run_query($birt_commSql); 
		
		//$birt_commSql = "select * from announcements AS w LEFT JOIN users AS u ON u.u_id=w.an_created_by GROUP BY w.an_id ORDER BY w.an_id DESC LIMIT 0, 20 ";
        //$data['announcements'] = $this->Base_model->run_query($birt_commSql);

        //$birt_mycommSql = "select * from birthday_comments AS w LEFT JOIN users AS u ON u.u_id=w.bc_userid WHERE w.bc_brith_uid=".$this->user_ldamp['user_id']." GROUP BY w.bc_comment_group ORDER BY w.bc_id DESC LIMIT 0, 20 ";
        //$data['my_comments'] = $this->Base_model->run_query($birt_mycommSql);

        //$birthday_wishes_Sql = "select * from birthday_wishes  ORDER BY bw_id DESC LIMIT 0, 20 ";
        //$data['birthday_wishes'] = $this->Base_model->run_query($birthday_wishes_Sql);

        //$slides_Sql = "select * from profile_slides WHERE is_active=1 ORDER BY sl_id DESC LIMIT 0, 10 ";
        //$data['prifile_slides'] = $this->Base_model->run_query($slides_Sql);
		
		//$qry = "SELECT emp_hr.*,u.u_image FROM emp_hr_data emp_hr LEFT JOIN users u ON(u.u_employeeid = emp_hr.emp_code) WHERE emp_hr.emp_is_active = '1' AND emp_hr.emp_status='Active' AND emp_hr.emp_date_birth !='0000-00-00 00:00:00' AND DATE_FORMAT(emp_hr.emp_date_birth,'%m-%d') = DATE_FORMAT('".date('Y-m-d')."', '%m-%d')";
		//$data['birthday_list'] = $this->Base_model->run_query($qry);
		
        $this->load->view('l_and_d/l_and_d_copy', $data);
    }
	
	public function documents($url=''){
		switch($url){
			case 'L_and_D_Calendars':
				$url_folder = 'L_and_D_Offerings/L_and_D_Calendars';
				$folder_name = 'L & D Calenders';
			break;
			
			case 'Programmes_Offerred':
				$url_folder = 'L_and_D_Offerings/Programmes_Offerred';
				$folder_name = 'Programmes & Offerings';
			break;
			
			case 'Forms':
				$url_folder = 'L_and_D_Procedures/Forms';
				$folder_name = 'Forms';
			break;
			
			case 'Processes':
				$url_folder = 'L_and_D_Procedures/Processes';
				$folder_name = 'Processes';
			break;
			
			case 'Templates':
				$url_folder = 'L_and_D_Procedures/Templates';
				$folder_name = 'Templates';
			break;
			
			case 'Mis':
				$url_folder = 'L_and_D_Dashboard';
				$folder_name = 'MIS';
			break;
			
			case 'Presentation_of_Trainings':
				$url_folder = 'Virtual_Learning/Presentation_of_Trainings';
				$folder_name = 'Presentation of Trainings';
			break;
			
		}
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		$this->load->view('l_and_d/l_and_d_doc',$data);
	}
	
	public function training_presentation(){
		
		$this->load->view('l_and_d/training_presentation');
	}
	
	public function technical_traning(){
		$this->load->view('l_and_d/technical_traning');
	}
	
	public function functional_traning(){
		$url_folder = 'Functional';
		$folder_name = 'Functional';
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		
		$this->load->view('l_and_d/functional_traning',$data);
	}
	
	public function personal_traning(){
		$url_folder = 'Personal';
		$folder_name = 'Personal';
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		
		$this->load->view('l_and_d/personal_traning',$data);
		
	}
	public function personal_sub($url=''){
		switch($url){
			case 'LDP':
				$url_folder = 'Personal/LDP';
				$folder_name = 'LDP';
			break;
			
			case 'MDP':
				$url_folder = 'Personal/MDP';
				$folder_name = 'MDP';
			break;
			
			case 'Behavioural':
				$url_folder = 'Personal/Behavioural';
				$folder_name = 'Behavioural';
			break;
			
		}
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		$this->load->view('l_and_d/personal_traning_nested',$data);
		
	}
	public function qhse_traning(){
		$url_folder = 'QHSE';
		$folder_name = 'QHSE';
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		
		$this->load->view('l_and_d/qhse_traning',$data);
		
	}
	public function induction_traning(){
		$url_folder = 'Induction';
		$folder_name = 'Induction';
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		
		$this->load->view('l_and_d/induction_traning',$data);
		
	}
	
	public function technical($url=''){
		switch($url){
			case 'electrical_technical_traning':
				$url_folder = 'Technical/Electrical';
				$folder_name = 'Electrical Traning';
			break;
			
			case 'mech_technical_traning':
				$url_folder = 'Technical/Mechanical';
				$folder_name = 'Mechanical Traning';
			break;
			
			case 'electronics_technical_traning':
				$url_folder = 'Technical/Electronics';
				$folder_name = 'Electronics Traning';
			break;
			
			case 'civil_technical_traning':
				$url_folder = 'Technical/Civil';
				$folder_name = 'Civil Traning';
			break;
			
			case 'renewable_energy_traning':
				$url_folder = 'Technical/Renewable';
				$folder_name = 'Renewable Traning';
			break;
			
		}
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		$this->load->view('l_and_d/technical_traning_nested',$data);
	}
	
	
	/*
	public function electrical_technical_traning(){
		$url_folder = 'Technical/Electrical';
		$folder_name = 'Electrical Traning';
		$data['folder_name'] = $folder_name;
		$data['image_list'] = $this->get_folder_img($url_folder);
		$data['image_url'] = base_url().'uploads/l_and_d/'.$url_folder.'/';
		
		$this->load->view('l_and_d/electrical_technical_traning',$data);
	}
	public function mech_technical_traning(){
		$this->load->view('l_and_d/mech_technical_traning');
	}
	public function electronics_technical_traning(){
		$this->load->view('l_and_d/electronics_technical_traning');
	}
	public function civil_technical_traning(){
		$this->load->view('l_and_d/civil_technical_traning');
	}*/
	
	function get_folder_img($folder_name=''){
		$dir = "uploads/l_and_d/";
		if($folder_name !=''){
			$get_img_dir = $dir.$folder_name;
			$fileImg = scandir($get_img_dir, 1);
			$count = count($fileImg);
			for($i=0;$i<$count;$i++){
				if($fileImg[$i]=="." || $fileImg[$i]==".."){ }else{
					$folderImg['imglist'][] = $fileImg[$i];
				}
			}
		}
		return $folderImg;
	}
	
	public function about_us(){
		$this->load->view('l_and_d/about');
	}
	public function key_info(){	
		$this->load->view('l_and_d/key_info');
	}
	public function roles(){	
		$this->load->view('l_and_d/role');
	}
	
	//L and D Portal code end **********************************************
	
	
}
