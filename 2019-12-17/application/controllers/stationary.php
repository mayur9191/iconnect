<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Stationary extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->is_admin = is_Admin($this->user_ldamp["employeeid"]);
		$lognSql = "select * from users where u_ldap_userid='" . $this->user_ldamp['samaccountname'] . "' ";
        $this->user_indo = $this->Base_model->run_query($lognSql);
		$this->load->model('Stationary_model');
		$this->load->library('excel');
		
		include APPPATH . 'third_party/class.phpmailer.php';
		
	}
	
	public function get_ledap(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		return $data;
	}
	
	public function add_stationary(){
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['stationary_data']=$this->Stationary_model->getStationary_master_list();
		$data['include'] = 'backend_iconnect/stationary/add_stationary';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function item_purchased_mis($item_code){
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['MIS']=$this->Stationary_model->getItemPuchasedMIS($item_code);
		$data['include'] = 'backend_iconnect/stationary/item_purchased_mis';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function add_stationary_item(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/stationary/add_stationary_item';
		$LastEnterItemCode = $this->Stationary_model->getLastEnterItemCode();
		$data['newItemCode'] = ($LastEnterItemCode->item_code)+1;
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	public function addNewStationaryItem(){
		$postData = $this->input->post();
		$insertArray = array(
		   'item_code' => $postData['item_code'],
		   'item_name' => $postData['item_name'],
		   'brand' => $postData['brand'],
		   'unit' => $postData['unit'],
		   'sb_rate' => $postData['sb_rate'],
		   'HSN' => $postData['HSN'],
		   'HSN_code' => $postData['HSN_code'],
		   'GST' => $postData['GST'],
		   'quantity' => $postData['quantity'],
		   'total_purchased_quantity'=>$postData['quantity']
		
		);
		$item_mis = array(
		   'item_code' => $postData['item_code'],
		   'item_name' => $postData['item_name'],
		   'quantity' => $postData['quantity'],
		   'quantity_update' =>date('Y-m-d')
		);
		
		if($this->Stationary_model->add_StationaryItem($insertArray)){
			$this->db->insert('stationary_item_date_quantity',$item_mis); // Add for MIS data
			$this->session->set_flashdata('msg','Item added successfully');
			 
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('stationary/add_stationary');
	}
	
	public function update_item($id){
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['stationary_data']=$this->Stationary_model->getStationary_item($id);
		$data['include'] = 'backend_iconnect/stationary/update_item';
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	public function updateStationaryItem($itemid){
		$data = $this->get_ledap();
		$postData = $this->input->post();
		$total = $this->Stationary_model->getStationary_totalPurchasedQuantity($itemid);
		
		$quantity = $postData['quantity'] + $postData['quantity_to_add'] ;
		$total = ($total->total_purchased_quantity)+ $postData['quantity_to_add'];
		
		$updateArray = array(
		   'item_code' => $postData['item_code'],
		   'item_name' => $postData['item_name'],
		   'brand' => $postData['brand'],
		   'unit' => $postData['unit'],
		   'sb_rate' => $postData['sb_rate'],
		   'HSN' => $postData['HSN'],
		   'HSN_code' => $postData['HSN_code'],
		   'GST' => $postData['GST'],
		   'quantity' => $quantity,
		   'total_purchased_quantity' =>$total,
		   'quantity_update_date'=>date('Y-m-d')
		
		);
		if($postData['quantity_to_add'] != ''){
			$insertArray = array(
				'item_code' => $postData['item_code'],
				'item_name' => $postData['item_name'],
				'quantity' =>$postData['quantity_to_add'],
				'quantity_update'=>date('Y-m-d')
			);
			$this->db->insert('stationary_item_date_quantity',$insertArray);
		}
		if($this->Stationary_model->update_StationaryItem($updateArray,$itemid)){
			$this->session->set_flashdata('msg','Item added successfully');
			 
		}else{
			$this->session->set_flashdata('error_msg','An error occured while saving... Please try again');
		}
		redirect('stationary/add_stationary');
	}
	
	public function assign_stationary_userList(){
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/stationary/assign_stationary_userList';
		$data['assign_stationary_items']=$this->Stationary_model->assign_stationary_userList();
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function assign_stationary_FromList(){
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/stationary/assign_stationary_FromList';
		$data['stationary_items']=$this->Stationary_model->getStationary_master_list();
		$this->load->view('backend_iconnect/masterpage',$data);
		
	}
	
	public function assign_stationary(){
		$data = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/stationary/assign_stationary';
		$data['stationary_items']=$this->Stationary_model->getStationaryitems();
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function AssignStationary(){
		$item_id = $_POST['checklist'];
		$postData = $this->input->post();
		$assign_quan = $_POST['assign_quan'];
		$items = array();
		
		if(count($_POST['checklist'])> 0){
				for($i=0;$i<count($_POST['checklist']);$i++){
					
					$sub_inv_ary = array(
					    'id' =>$_POST['checklist'][$i],
						'emp_name'=>$postData['user_name'],
						'emp_code'=>$postData['user_empcode'],
						'emp_email'=>$postData['user_email'],
						'emp_lob'=>$postData['user_lob'],
						//'item_code'=>$postData['item_code'][$i],
						//'item_name'=>$postData['item_name'][$i],
						'quantity'=>$_POST['assign_quan'][$i],
						'assigned_date'=>date('Y-m-d')
					);
					
					echo "<pre>";
					print_r($sub_inv_ary);
					//$this->db->insert('stationary_assign_item',$sub_inv_ary);
					//$items[] = "<b>Items-</b>" .$postData['item_name'][$i]." <b>Quantity-</b> " .$postData['quantity'][$i]."<br/>";
					
				}
		}
		
	}
	public function AssignStationaryItems(){
		$udata = $this->get_ledap();
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$postData = $this->input->post();
		$items = array();
		$array = $postData['item_name'];
		
		if(count($postData['item_name'])> 0){
				for($i=0;$i<count($postData['item_name']);$i++){
					$item_name = $this->Stationary_model->getStationary_itemBYCode($postData['item_name'][$i]);
						$sub_inv_ary = array(
							'emp_name'=>$postData['user_name'],
							'emp_code'=>$postData['user_empcode'],
							'emp_email'=>$postData['user_email'],
							'emp_lob'=>$postData['user_lob'],
							'item_code'=>$postData['item_name'][$i],
							'item_name'=>$item_name->item_name,
							'quantity'=>$postData['quantity'][$i],
							'assigned_date'=>$postData['assign_date'][$i],
							'assigned_by'=>$udata['user_ldamp']['name']
						);
					if($postData['item_name'][$i] == ''){
						break;
					}
					//echo "<pre>";
					//print_r($sub_inv_ary);
					
					$this->db->insert('stationary_assign_item',$sub_inv_ary);
					//echo $postData['item_code'][$i];echo $postData['quantity'][$i];echo"<br/>"; 
					
					$data['stationary_items']=$this->Stationary_model->getStationary_itemBYCode($postData['item_name'][$i]);
					
				    $stock = ($data['stationary_items']->quantity - $postData['quantity'][$i]);
					$assigned_qun = ($data['stationary_items']->assign_quantity + $postData['quantity'][$i]);
					
					//echo "Stock".$stock;
					//echo "Assi".$assigned_qun;
					$update_array = array(
						'quantity'=>$stock,
						'assign_quantity'=>$assigned_qun
					);
					$this->db->where('item_code', $postData['item_name'][$i]);
					$this->db->update('stationary_master_list',$update_array);
					$items[] .="<tr><td>".$item_name->item_name."</td><td>".$postData['quantity'][$i]."</td><td>".$data['stationary_items']->unit."</td><td>".$postData['assign_date'][$i]."</td></tr>";
					
				}
				
		}
		
		$email = $postData['user_email'];
		$emailSubj = 'Stationary issuance confirmation';
		$emailMsg = 'Dear Sir/Madam,<br>
				<p>Stationary has been assigned to you.<p>
				<p>Please find details.</p>
				<table cellpadding="1" cellspacing="1" border="1">
				<tr><td><b>Item:</b></td><td><b>Quantity:</b></td><td><b>Unit:</b></td><td><b>Assigned Date:</b></td></tr>';
				foreach($items as $item){
					
				$emailMsg .=$item;	
				}
		$emailMsg .='</table>';		
		$emailMsg .='<p>*** This is an auto generated email, please do not reply *** </p><br>';
		if($this->sendAssignItemEmail('Admin Stationary',$email,$emailSubj,$emailMsg)){
					$this->session->set_flashdata('msg','Email sent successfully');
					redirect('stationary/assign_stationary_userList');
		}
	}
	
	public function sendAssignItemEmail($fromName='',$toEmail='', $subject='',$mesge=''){
		ob_start();
		error_reporting(0);		
		$smu =smu;
		$smp=smp;
		$mail = '';
		$account=$smu;
		$password=$smp;
		$from=$smu;
		$to=$toEmail;
		$from_name="Skeiron Admin";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		$cc = 'Asha.Bhandwalkar@skeiron.com,bhagyashree.holey@skeiron.com';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	
	public function autocomplete(){
		$data['ldamp_usercode'] = $data['user_ldamp']['mail'];
		$data['include'] = 'backend_iconnect/stationary/autocomplete';
		$data['stationary_items']=$this->Stationary_model->getStationaryitems();
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function TriggerEmailLowQuantity(){
		
		$data['items'] = $this->Stationary_model->getStationary_master_list();
		$email = 'Asha.Bhandwalkar@skeiron.com';
		$emailSubj = 'Need to order for following stationary items.';
		
		$mesge = 'Dear Sir / Madam,<br><br>
				Please find following Admin Stationary Items. Available stock quantity is very less.Need to order for same. <br><br>';
		$mesge .= '<table cellpadding="1" cellspacing="1" border="1">';
		$mesge .= '<tr>
				  <th>Item Code</th>
				  <th>Item Name</th>
				  <th>Brand</th>
				  <th>Unit</th>
				  <th>Rate</th>
				  <th>HSN</th>
				  <th>HSN Code</th>
				  <th>GST</th>
				  <th>Stock Quantity</th>
				  <th>Margin Value</th>
				  </tr>';
		
		foreach($data['items'] as $items){
			if(($items->quantity) < ($items->quantity_margin_value)){
			$mesge.='<tr>
					<td>'.$items->item_code.'</td>
					<td>'.$items->item_name.'</td>
					<td>'.$items->brand.'</td>
					<td>'.$items->unit.'</td>
					<td>'.$items->sb_rate.'</td>
					<td>'.$items->HSN.'</td>
					<td>'.$items->HSN_code.'</td>
					<td>'.$items->GST.'</td>
					<td>'.$items->quantity.'</td>
					<td>'.$items->quantity_margin_value.'</td>
					</tr>';
			}
			
		}
		$mesge .='</table>';
	    $mesge .= '<p>*** This is an automatically generated email, please do not reply *** </p><br>';
		//echo $mesge;
	    $this->sendTriggerEmail('Admin Stationary',$email,$emailSubj,$mesge);
	}
	
	public function getObject(){
		$postData = $this->input->post();
		$returnData = array();
		$objCode = $postData['objCode'];
		switch($objCode){
			
			case 'getUnit':
				$returnData['objData'] = get_item_unitBYitem_code($postData['itemCode']);
			break;
			
		}
		echo json_encode($returnData);
	}
	
	public function sendTriggerEmail($fromName='',$toEmail='', $subject='',$mesge=''){
		ob_start();
		error_reporting(0);		
		$smu =smu;
		$smp=smp;
		$mail = '';
		$account=$smu;
		$password=$smp;
		$from=$smu;
		$to=$toEmail;
		$from_name="Skeiron Admin";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		$cc = 'Asha.Bhandwalkar@skeiron.com,bhagyashree.holey@skeiron.com';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
}

