<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdesk extends MY_Controller {
	public function __construct()
	{ 
		parent::__construct();
		$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->model('Survey_model');
		$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        $mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        $this->hrdata_user_info = $mssql_user_info_query->result_array();
	}
	
	//user panel start 
	public function index(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->view('helpdesk/homepage',$data);
	}
	
	//user panel end
	
	//admin panel start 
	public function admin(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->view('helpdesk/homepageadmin',$data);
	}
	
	public function open_request(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		
		$this->load->view('helpdesk/open_request',$data);
	}
	
	public function open_request_details(){
		$data['user_ldamp'] = $this->user_ldamp;
		$lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		
		$this->load->view('helpdesk/open_request_details',$data);
	}
	//admin panel end
	
	public function testpage(){
		$this->load->view('helpdesk/permission_page');
	}
	public function new_request()
	{
		$data['user_ldamp'] = $this->user_ldamp;
		$this->load->view('helpdesk/new_request',$data);
	}
}
