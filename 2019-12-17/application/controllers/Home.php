<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct()
	{ 
		parent::__construct();
		$this->mssql = $this->load->database('mssql', TRUE);
		$this->empCode = '';
		if (empty($this->session->userdata('user_ldamp'))) {
            redirect('login', 'refresh');
        }
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->model('Survey_model');
		$mssql_user_info_sql = "SELECT * FROM DU_EMP_PERSONAL_DETAIL_UPD WHERE EMP_STAFFID = '" . $this->user_ldamp['employeeid'] . "'";
        $mssql_user_info_query = $this->mssql->query($mssql_user_info_sql);
        $this->hrdata_user_info = $mssql_user_info_query->result_array();
		
	}
	
	public function index()
	{
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		
		$data['user0'] = 0;
		$data['user1'] = 1;
		$data['user2'] = 2;
		$data['user3'] = 3;
		$this->user_ldamp = $this->session->userdata('user_ldamp');
		$this->load->view('home/_form1',$data);
	}
	
	public function report(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		
		$data['passingData'] = getMaterialAllMaster();
		$this->load->view('home/report',$data);
	}
	
	public function material_gateway_form($gateway_pass_id=0){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		
		$data['passingData'] = getMaterialMasterDataById($gateway_pass_id);
		//$data['passingData'] = getMaterialMaster();
		$this->load->view('home/_form1_preview',$data);
	}
	
	//
	public function approval_process($gateway_pass_id=0){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		
		$empId = $this->user_ldamp['employeeid'];
		
		$user0Ary = array('SG0308'); //Prepared by
		$user1Ary = array('SG0300'); //Checked by
		$user2Ary = array('SG0282'); //Approved by
		$user3Ary = array('SG0263'); //Approved by
		
		$data['user0'] = (in_array($empId,$user0Ary)) ? 4 : 0; //0;
		$data['user1'] = (in_array($empId,$user1Ary)) ? 4 : 1; //1;
		$data['user2'] = (in_array($empId,$user2Ary)) ? 4 : 2; //2;
		$data['user3'] = (in_array($empId,$user3Ary)) ? 4 : 3; //4;
		
		$data['passingData'] = getMaterialMasterDataById($gateway_pass_id);
		$this->load->view('home/_form_edit',$data);
	}
	
	//gateway form submit
	public function submitGateway(){
		$empId = $this->user_ldamp['employeeid'];
		$postData = $this->input->post();
		$material_master_id = 0;
		
		if(isset($postData['company_check1'])){
			$company_check1 = 1;
		}else{
			$company_check1 = 0;
		}
		
		if(isset($postData['company_check2'])){
			$company_check2 = 1;
		}else{
			$company_check2 = 0;
		}
		
		$material_master = array(
			'top_number'=>fillterInput($postData['top_number']),
			'top_date'=>date('Y-m-d',strtotime($postData['top_date'])),
			'company_check1'=>$company_check1,
			'company_check2'=>$company_check2,
			'location'=>fillterInput($postData['location']),
			'sender_name_contact'=>fillterInput($postData['sender_name_contact']),
			'source_address'=>fillterInput($postData['source_address']),
			'recipient_name_contact'=>fillterInput($postData['recipient_name_contact']),
			'destination_address'=>fillterInput($postData['destination_address']),
			'purpose'=>fillterInput($postData['purpose']),
			'material_went_out_date'=>date('Y-m-d',strtotime($postData['material_went_out_date'])),
			'material_went_out_vehicle'=>fillterInput($postData['material_went_out_vehicle']),
			'name_of_recipient'=>fillterInput($postData['name_of_recipient']),
			'contact_no'=>fillterInput($postData['contact_no']),
			'security_signature'=>fillterInput($postData['security_signature']),
			'created_date'=>date('Y-m-d'),
			'material_created_by'=>$empId
		);
		
		$this->db->insert('material_master',$material_master);
		$material_master_id = $this->db->insert_id();
		
		for($i=0;$i<count($postData['sl_no']); $i++){
			$material_product = array(
				'material_master_id'=>$material_master_id,
				'product_sl_no'=>fillterInput($postData['sl_no'][$i]),
				'product_item'=>fillterInput($postData['item'][$i]),
				'description'=>fillterInput($postData['description'][$i]),
				'product_nob'=>fillterInput($postData['product_nob'][$i]),
				'product_unit'=>fillterInput($postData['product_unit'][$i]),
				'returnable_status'=>fillterInput($postData['returnable_status'][$i]),
				'remark'=>fillterInput($postData['remark'][$i])
			);
			$this->db->insert('material_product',$material_product);
		}
		
		for($j=0;$j<count($postData['department']);$j++){
			$material_approval = array(
				'material_master_id'=>$material_master_id,
				'department'=>fillterInput($postData['department'][$j]),
				'person_name'=>fillterInput($postData['person_name'][$j]),
				'designation'=>fillterInput($postData['designation'][$j]),
				'signature_upload'=>fillterInput($postData['signature_upload'][$j]),
				'digital_signature'=>'',
				'approval_sque_id'=>$j,
				'approved_date'=>date('Y-m-d',strtotime($postData['approved_date'][$j]))
			);
			$this->db->insert('material_approval',$material_approval);
		}
		redirect(base_url().'home/report');
	}
	
	public function submitEditGateway(){
		$empId = $this->user_ldamp['employeeid'];
		$postData = $this->input->post();
		$material_master_id = $postData['material_id'];
		if(isset($postData['company_check1'])){
			$company_check1 = 1;
		}else{
			$company_check1 = 0;
		}
		
		if(isset($postData['company_check2'])){
			$company_check2 = 1;
		}else{
			$company_check2 = 0;
		}
		
		$material_master = array(
			'top_number'=>fillterInput($postData['top_number']),
			'top_date'=>date('Y-m-d',strtotime($postData['top_date'])),
			'company_check1'=>$company_check1,
			'company_check2'=>$company_check2,
			'location'=>fillterInput($postData['location']),
			'sender_name_contact'=>fillterInput($postData['sender_name_contact']),
			'source_address'=>fillterInput($postData['source_address']),
			'recipient_name_contact'=>fillterInput($postData['recipient_name_contact']),
			'destination_address'=>fillterInput($postData['destination_address']),
			'purpose'=>fillterInput($postData['purpose']),
			'material_went_out_date'=>date('Y-m-d',strtotime($postData['material_went_out_date'])),
			'material_went_out_vehicle'=>fillterInput($postData['material_went_out_vehicle']),
			'name_of_recipient'=>fillterInput($postData['name_of_recipient']),
			'contact_no'=>fillterInput($postData['contact_no']),
			'security_signature'=>fillterInput($postData['security_signature']),
			'created_date'=>date('Y-m-d'),
			'material_created_by'=>0
		);
		$this->db->where('material_master_id',$material_master_id);
		$this->db->update('material_master',$material_master);
		
		
		deactiveMaterialProducts($material_master_id);
		
		for($i=0;$i<count($postData['sl_no']); $i++){
			$material_product = array(
				'material_master_id'=>$material_master_id,
				'product_sl_no'=>fillterInput($postData['sl_no'][$i]),
				'product_item'=>fillterInput($postData['item'][$i]),
				'description'=>fillterInput($postData['description'][$i]),
				'product_nob'=>fillterInput($postData['product_nob'][$i]),
				'product_unit'=>fillterInput($postData['product_unit'][$i]),
				'returnable_status'=>fillterInput($postData['returnable_status'][$i]),
				'remark'=>fillterInput($postData['remark'][$i])
			);
			$this->db->insert('material_product',$material_product);
		}
		
		if($postData['user0']== 4){
			$j=0;
			$material_approval_id = getApprovalId($material_master_id,$j);
		}
		
		if($postData['user1'] == 4){
			$j=1;
			$material_approval_id = getApprovalId($material_master_id,$j);
		}
		
		if($postData['user2'] == 4){
			$j=2;
			$material_approval_id = getApprovalId($material_master_id,$j);
		}
		
		if($postData['user3'] == 4){
			$j = 3;
			$material_approval_id = getApprovalId($material_master_id,$j);
		}
		
		$material_approval = array(
			'material_master_id'=>$material_master_id,
			'department'=>fillterInput($postData['department'][$j]),
			'person_name'=>fillterInput($postData['person_name'][$j]),
			'designation'=>fillterInput($postData['designation'][$j]),
			'signature_upload'=>fillterInput($postData['signature_upload'][$j]),
			'digital_signature'=>fillterInput($postData['signature_upload'][$j]),
			'approved_date'=>date('Y-m-d',strtotime($postData['approved_date'][$j])),
			'emp_id'=>$empId
		);
		$this->db->where('material_approval_id',$material_approval_id);
		$this->db->update('material_approval',$material_approval);
		//echo $this->db->last_query();exit;
		redirect(base_url().'home/report');
	}
	
	public function downloadPDFV2($gateway_pass_id=0){
		
		ob_start();
		
		$this->load->library('PDF_CON_RV/tcpdf');
		header("Content-Encoding: None", true);
		ini_set('max_execution_time', 1000);
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// add a page
		//$pdf->AddPage();
		$pdf->AddPage('L', 'A2');
		
		//with rates
		$passingData = getMaterialMasterDataById($gateway_pass_id);
		$approvalData = getMaterialApproval($passingData->material_master_id);
		$contents =<<<EOF
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
   <?php include APPPATH . "views/includs/hedder_code.php" ?>
<style>
.alg-center{
	text-align:center;
}
input[type="checkbox"]{ width: 20px; height: 20px;}
</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<?php $approvalData = getMaterialApproval($passingData->material_master_id); ?>
<br><br><br>
 <div class="container" style="border: 2px groove;">
	<div class="row pull-right">
		<table class="table">
			<tbody>
			  <tr>
				<th>No. : </th>
				<td><?php _echo ($passingData->top_number !='')? $passingData->top_number:''; ?></td>
				<th>Date : </th>
				<td><?php echo ($passingData->top_date !='')? date('d/m/Y',strtotime($passingData->top_date)):''; ?></td>
			  </tr>
			</tbody>
		</table>
	</div>
    
   <br><br><br>
   <center><h3>MATERIAL GATE PASS </h3></center>
   <br>
   
	<div class="col-lg-12 ">
	<div class="row">
		<form>
			<div class="col-lg-12">
				<div class="row">
				<table class="table" >
					<tbody id="adrow">
						<tr>
							<td><b>Skeiron Green Power PRIVATE LIMITED</b> <input type="checkbox" <?php echo ($passingData->company_check1 == 1)? 'checked="checked" disabled="disabled"':'disabled="disabled"'; ?> /></td>
							<td><b>Aspen INFRASTRUCTURES PRIVATE LIMITED</b> <input type="checkbox" <?php echo ($passingData->company_check2 == 1)? 'checked="checked" disabled="disabled"':'disabled="disabled"'; ?> /></td>
						</tr>
						<tr>
							<td colspan="2"><b>Location :</b> <?php echo ($passingData->location !='')? $passingData->location:''; ?> </td>
						</tr>
						<tr>
							<td colspan="2">The following material may please be allowed to go out of our office on date  <?php echo ($approvalData[3]->approved_date !='')? date('d/m/Y',strtotime($approvalData[3]->approved_date)):''; ?>.</td>
						</tr>
					</tbody>
				</table>
			</div>	
			
              <div class="row">
                
                  <table class="table" border="">
                    <thead>
                      <tr>
                        <th class="alg-center">S. No.</th>
                        <th class="alg-center">Item</th>
                        <th class="alg-center">Description</th>
                        <th class="alg-center">No</th>
                        <th class="alg-center">Unit</th>
                        <th class="alg-center">Returnable / Non-returnable</th>
                        <th class="alg-center">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="adrow">
					<?php $materialData = getMterialProduct($passingData->material_master_id);
						foreach($materialData as $materialRecord) { ?>
							<tr>
								<td><?php echo ($materialRecord->product_sl_no !='')? $materialRecord->product_sl_no:''; ?></td>
								<td><?php echo ($materialRecord->product_item !='')? $materialRecord->product_item:''; ?></td>
								<td><?php echo ($materialRecord->description !='')? $materialRecord->description:''; ?></td>
								<td><?php echo ($materialRecord->product_nob !='')? $materialRecord->product_nob:''; ?></td>
								<td><?php echo ($materialRecord->product_unit !='')? $materialRecord->product_unit:''; ?></td>
								<td><?php echo ($materialRecord->returnable_status !='')? $materialRecord->returnable_status:''; ?></td>
								<td><?php echo ($materialRecord->remark !='')? $materialRecord->remark:''; ?></td>
							</tr>
					<?php } ?>
                    </tbody>
                </table>
              </div>
			  
              <div class="row">
			   <table class="table">
					<tbody>
                      <tr>
                        <th>Sender Name & Contact No. : </th>
                        <td><?php echo ($passingData->sender_name_contact !='')? $passingData->sender_name_contact:''; ?></td>
                      </tr>
					  
					  <tr>
                        <th>Source Address : </th>
                        <td><?php echo ($passingData->source_address !='')? $passingData->source_address:''; ?></td>
                      </tr>
					  
					   <tr>
                        <th>Recipient Name & Contact No. : </th>
                        <td><?php echo ($passingData->recipient_name_contact !='')? $passingData->recipient_name_contact:''; ?></td>
                      </tr>
					  
					  <tr>
                        <th>Destination Address : </th>
                        <td><?php echo ($passingData->destination_address !='')? $passingData->destination_address:''; ?></td>
                      </tr>
					  
					   <tr>
                        <th>Purpose : </th>
                        <td><?php echo ($passingData->purpose !='')? $passingData->purpose:''; ?></td>
                      </tr>
					  
					</tbody>
				</table>
			  </div>
			  
              <div class="row">
                
                  <table class="table" border="">
                    <thead>
                      <tr>
                        <th></th>
                        <th class="alg-center">Prepared by</th>
                        <th class="alg-center">Checked by</th>
                        <th class="alg-center">Approved by</th>
                        <th class="alg-center">Approved by</th>
                      </tr>
                    </thead>
                    <tbody>
					
                      <tr>
                        <th>Department</th>
                        <td><?php echo ($approvalData[0]->department !='')? $approvalData[0]->department:''; ?></td>
                        <td><?php echo ($approvalData[1]->department !='')? $approvalData[1]->department:''; ?></td>
                        <td><?php echo ($approvalData[2]->department !='')? $approvalData[2]->department:''; ?></td>
                        <td><?php echo ($approvalData[3]->department !='')? $approvalData[3]->department:''; ?></td>
                      </tr>
                      <tr>
                        <th>Name</th>
                        <td><?php echo ($approvalData[0]->person_name !='')? $approvalData[0]->person_name:''; ?></td>
                        <td><?php echo ($approvalData[1]->person_name !='')? $approvalData[1]->person_name:''; ?></td>
                        <td><?php echo ($approvalData[2]->person_name !='')? $approvalData[2]->person_name:''; ?></td>
                        <td><?php echo ($approvalData[3]->person_name !='')? $approvalData[3]->person_name:''; ?></td>
                      </tr>
                       <tr>
                        <th>Designation</th>
                        <td><?php echo ($approvalData[0]->designation !='')? $approvalData[0]->designation:''; ?></td>
                        <td><?php echo ($approvalData[1]->designation !='')? $approvalData[1]->designation:''; ?></td>
                        <td><?php echo ($approvalData[2]->designation !='')? $approvalData[2]->designation:''; ?></td>
                        <td><?php echo ($approvalData[3]->designation !='')? $approvalData[3]->designation:''; ?></td>
                      </tr>
                      <tr>
                        <th>Signature</th>
						<td><?php echo ($approvalData[0]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
                        <td><?php echo ($approvalData[1]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
                        <td><?php echo ($approvalData[2]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
                        <td><?php echo ($approvalData[3]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
						
                        <!--<td><?php echo ($approvalData[0]->digital_signature =='approve')? 'Approve':''; ?></td>
                        <td><?php echo ($approvalData[1]->digital_signature =='approve')? 'Approve':''; ?></td>
                        <td><?php echo ($approvalData[2]->digital_signature =='approve')? 'Approve':''; ?></td>
                        <td><?php echo ($approvalData[3]->digital_signature =='approve')? 'Approve':''; ?></td>-->
                      </tr>
                      <tr>
                        <th>Date</th>
                        <td><?php echo ($approvalData[0]->approved_date !='')? date('d/m/Y',strtotime($approvalData[0]->approved_date)):''; ?></td>
                        <td><?php echo ($approvalData[1]->approved_date !='')? date('d/m/Y',strtotime($approvalData[1]->approved_date)):''; ?></td>
                        <td><?php echo ($approvalData[2]->approved_date !='')? date('d/m/Y',strtotime($approvalData[2]->approved_date)):''; ?></td>
                        <td><?php echo ($approvalData[3]->approved_date !='')? date('d/m/Y',strtotime($approvalData[3]->approved_date)):''; ?></td>
                      </tr>
					 
                    </tbody>
                </table>
              </div>
			  
            <div class="row">
              <div class="col-sm-12 form-group">
                <label>Remarks of Security : </label>
              </div>
            </div>
			
			<div class="row">
			   <table class="table">
					<tbody>
                      <tr>
                        <th>1. Material went out on date: </th>
                        <td>_________________________ Time: ______________ AM /PM</td>
                      </tr>
					  
					  <tr>
                        <th>2. by which vehicle :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					   <tr>
                        <th>3. Name of Recipient :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					  <tr>
                        <th>4.Contact Number :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					   <tr>
                        <th>5. Signature :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					</tbody>
				</table>
				<br>
				<span class="pull-right" style="margin-right: 10%;"><b>Signature of Security Officer</b></span>
				<br>
				<br>
				
			</div>
			</div>
		</form> 
		</div>
	</div>
	</div>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
EOF;
		
		
		
//http://jsfiddle.net/5ud8jkvf/

//https://jsfiddle.net/enf0gy85/


		//$pdf->writeHTML($contents, true, false, true, false, '');
		$pdf->Write(0, $contents, '', 0, 'L', true, 0, false, false, 0);
		//Close and output PDF document
		$pdf->lastPage();
		//ob_end_clean();
		
		$pdf->Output('gateway_pass.pdf','I');
		exit;
	}
	public function getPage($gateway_pass_id){
		
		return 0;
	}
	
	//permission module start 
	public function addpermission(){
		$data['user_ldamp'] = $this->user_ldamp;
        $lognSql = "select * from users where u_ldap_userid='" . $data['user_ldamp']['samaccountname'] . "' ";
        $data['user_indo'] = $this->Base_model->run_query($lognSql);
		
		
		$data['userList'] = getUserList();
		$this->load->view('home/permission_page',$data);
	}
	public function submitPermission(){
		$postData = $this->input->post();
		
		$permission_column1 = 1; //prepared by
		$permission_column2 = 2; //checked by
		$permission_column3 = 3; //approved1 by
		$permission_column4 = 4; //approved2 by 
		foreach($postData['prepared_by'] as $permissionRecord){
			$insetAry = array(
				'user_role_id'=>$permissionRecord,
				'permission_column'=>$permission_column1
			);
			$this->db->insert("gate_pass_permission",$insetAry);
		}
		
		foreach($postData['checked_by'] as $permissionRecord){
			$insetAry = array(
				'user_role_id'=>$permissionRecord,
				'permission_column'=>$permission_column2
			);
			$this->db->insert("gate_pass_permission",$insetAry);
		}
		
		foreach($postData['approve1_by'] as $permissionRecord){
			$insetAry = array(
				'user_role_id'=>$permissionRecord,
				'permission_column'=>$permission_column3
			);
			$this->db->insert("gate_pass_permission",$insetAry);
		}
		
		
		foreach($postData['approve2_by'] as $permissionRecord){
			$insetAry = array(
				'user_role_id'=>$permissionRecord,
				'permission_column'=>$permission_column4
			);
			$this->db->insert("gate_pass_permission",$insetAry);
		}
		
	}
	//permission module end
	// 4430345335
}
