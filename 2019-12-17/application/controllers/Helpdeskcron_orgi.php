<?php

 defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdeskcron extends CI_Controller {
	
	public $request_date_time;
    public $current_date_time;
    public $sla_duration;
    public $work_strt_time;
    public $work_end_time;
	public function __construct()
	{ 
		parent::__construct();
		include APPPATH . 'third_party/class.phpmailer.php';
		$work_time = get_work_start_end('1');
		$work_strt_time = $work_time->start_time;
		$work_end_time  = $work_time->end_time;
        $this->current_date_time = date("Y-m-d H:i:s");
        $this->work_strt_time = date("Y-m-d").$work_strt_time;
        $this->work_end_time =  date("Y-m-d").$work_end_time;
		$this->load->model('Helpdesk_model');
		$this->load->model('Dailytask_model'); 
	}
	public function test()
	{
		echo "Hiii".PHP_EOL;die;
	}
	//-- SLA Code starts-------------------------------------------------
	public function  technician_escalation_email($user_request_id)
	{
		$user_req = getReqInfoByUserRequestId($user_request_id);
		//echo "<pre>";print_r($user_req);exit();
		$email = $user_req["technician_Email"];
		$technician_name = $user_req["technician_name"];
		$subject = "Request ID: ".getRequestCode($user_request_id)." has been Escalated ";
		$msg ='<p><b>Dear '.$technician_name.',</b></p>
			<p> The Request ID '.getRequestCode($user_request_id).' has been escalated due to SLA violation</p>
			<p> The subject of the request is : '.ucfirst($user_req["request_subject"]).' </p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>
		';
		$this->sendOutlookEmail($email,$subject,$msg);
		return;
	}
	public function  user_escalation_email($user_request_id)
	{
		$user_req = getReqInfoByUserRequestId($user_request_id);
		//echo "<pre>";print_r($user_req);exit();
		$email = $user_req["requester_email"];
		$user_name = $user_req["requester_name"];
		$technician_name = $user_req["technician_name"];
		$subject = "Request ID ".getRequestCode($user_request_id)."  has been Escalated ";
		$msg ='<p><b>Dear '.$user_name.',</b></p>
			<p> The Request ID :'.getRequestCode($user_request_id).' assigned to '.$technician_name.' has been escalated. </p>
			<p> The subject of the request is : '.ucfirst($user_req["request_subject"]).' </p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>
		';
		// to save the system emails before sending the user emails
		$sys_email_ary = array(
				'to_emails'=>$email ,
				'subject'=>$subject,
				'description'=>$msg,
				'created_date'=>date('Y-m-d H:i:s'),
				'user_request_id'=>$user_request_id
			);
		$this->db->insert('system_emails',$sys_email_ary);
		$this->sendOutlookEmail($email,$subject,$msg);
		return;
	}
	
	public function manager_escalation_email($user_request_id)
	{
		$user_req = getReqInfoByUserRequestId($user_request_id);
		//************************************************************
		
		$emp_code = get_escalation_emp_code($user_request_id);
		if(!empty($emp_code)){
		$email_info = get_email_by_emp_code($emp_code);
		}
		if(!empty($email_info)){
		   $email = $email_info[0]->u_email;
		}
		else{
			$email = "Dev@skeiron.com";
		}
		//**************************************************************
		$technician_name = $user_req["technician_name"];
		$subject = "Request ID: ".getRequestCode($user_request_id)." has been Escalated ";
		$msg ='<p><b>Dear Sir / Madam,</b></p>
			<p> The Request ID '.getRequestCode($user_request_id).' assigned to '.$technician_name.' has been escalated to you due to SLA Violation. </p>
			<p> The subject of the request is : '.ucfirst($user_req["request_subject"]).' </p>
			<p>Requester : '.ucfirst($user_req["requester_name"]).'</p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>
		';
		//echo $email;die;
		$this->sendOutlookEmail($email,$subject,$msg);
		return;
	}
	/** gets the sla duration of item and converts it into seconds*****
	/*public function sla_duration_item($get_sla)
	{	
		$sla_duration = 0;
		if(!empty($get_sla))
		 {
			 //echo "<pre>";print_r($user_req);exit();
			 
			  $sla_duration      =  $get_sla->sla_duration;
			  $sla_duration_unit = $get_sla->sla_duration_unit;
			  $sla_duration = get_sla_duration_from_type($get_sla->sla_duration_unit,'Sec',$sla_duration);
			 
		 }
		return $sla_duration; 
	}*/
	/*
	 *  Accepts the request id 
     *	returns the Remaining time before the SLA is reached or sends email in case sla is reached 
	 */
		
	public function sla_check($request_id)
    {	
	     $user_req = getReqInfoByUserRequestId($request_id);
		 //echo "<pre>";print_r($user_req);exit();
		 $req_date_time = $user_req['request_assign_person_dt'];
		 $technician_id = $user_req['technician_id'];
	     $this->request_date_time = $req_date_time; 
		 //Given in seconds.This should come from the table as per SLA matrix
		 $get_sla = get_item_sla_duration($user_req["item_id"]);
		 
		 $this->sla_duration = sla_duration_item($get_sla);
		  
		$working_days = $this->working_days_in_month();
		/*if(is_queried_by_tech($request_id)){
			continue;
		}*/
		if(in_array(date("Y-m-d",strtotime($this->current_date_time)),$working_days))
        if(strtotime($this->work_strt_time) <= strtotime($this->request_date_time))
		{
            //CURRENT TIME AFTER OR AT work start time
            $result = $this->sla_result($request_id,$technician_id);
            //echo $result; 
    
        }
        else if(date("Y-m-d",strtotime($this->work_strt_time)) == date("Y-m-d",strtotime($this->request_date_time)))
        {    //CURRENT TIME BEFORE work start time or AFTER work end time  
            $this->request_date_time = $this->work_strt_time;
            $result = $this->sla_result($request_id,$technician_id);
            //echo $result;
        }
        else
        {
            $this->request_date_time = $this->work_strt_time;
            $result = $this->sla_result($request_id,$technician_id);
           // echo $result;
        }
		return;
    }
	
	public function get_querytouserData($request_id=0){
		$qry = $this->db->query("SELECT SUM(clock_duration) AS time_spent1 FROM clock_request_hd WHERE user_request_id = $request_id GROUP By user_request_id");
		if($qry->num_rows() > 0){
			$time_data = $qry->row();
			return $time_data->time_spent1;
		}else{
			return 0;
		}
	}
	
	public function getRequestStatus($request_id=0){
		$qry = $this->db->query("SELECT * FROM request_assign_person_hd WHERE user_request_id='$request_id' AND request_assign_person_is_active='1'");
		$result_row = $qry->row();
		return $result_row;
	}
	
	public function sla_result($request_id,$tech_id)
    {
        $sla_duration = $this->sla_duration;
		//echo $sla_duration;die;
        //CURRENT TIME BEFORE OR AT 6:00
		
        if((strtotime($this->current_date_time)>= strtotime($this->work_strt_time))&&(strtotime($this->current_date_time)<= strtotime($this->work_end_time)))
            {   
				//new code start 
				$requestData = $this->getRequestStatus($request_id);
				$qry = $this->db->query("SELECT * FROM request_resolution_hd WHERE user_request_id='$request_id' AND resolution_is_active=1");
				$resolutionData = $qry->row();
					
				if($requestData->task_status == 'query_to_user' || $requestData->task_status == 'hold'){
					//save hold and query to user status time
					
					$time_spent_ = $this->get_querytouserData($request_id);	
					if($time_spent_ == 0){
						$time_spent_ +=  (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
					}else{
						$time_spent_1 = (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
						$time_spent_ = $time_spent_+$time_spent_1;
					}
					
					//$time_spent_ += (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
					
					$unworkTime = array(
						'clock_duration'=>$time_spent_,
						'clock_duration_date'=>date('Y-m-d',strtotime($this->current_date_time)),
						'user_request_id'=>$request_id,
						'technician_id'=>$tech_id,
						'request_status_code'=>$requestData->task_status
					);
					$this->sla_time_spent_save($request_id,$unworkTime,$tech_id,'spacial');
				}else{
							
							
					if(getClockTime_today($request_id,$tech_id)){
						
						$time_spent = get_sla_time_spent($request_id,$tech_id);
						if($time_spent == 0){
							$time_spent += (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
						}else{
							$time_spentk = $time_spent;
							$time_spent_temp = (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
							$time_spent5 = 60 + $time_spentk;
							$time_spent = 0;
							$time_spent = $time_spent5;
						}
						
					}else{
						
						$time_spent = get_sla_time_spent($request_id,$tech_id);
						//echo $time_spent;echo "<pre>";print_r($resolutionData);die;
						//$last_date_update =  (!empty($resolutionData))? strtotime($resolutionData->resolution_created_dt):strtotime($this->request_date_time); 
						$last_date_update =  (!empty($resolutionData) && ((($requestData->task_status=='open'|| $requestData->task_status == 'wip')&& date("Y-m-d",strtotime($resolutionData->resolution_created_dt))==date("Y-m-d",strtotime($this->current_date_time))))) ? strtotime($resolutionData->resolution_created_dt): strtotime($this->request_date_time); 
						if($time_spent == 0){
							$time_spent += (strtotime($this->current_date_time) - $last_date_update);
						}else{
							$time_spent = (strtotime($this->current_date_time) - $last_date_update);  
						}
					}
					
					
				//new code end
				//check the time spent and save/update the request id
					$data = array("time_spent" => $time_spent);
					$this->sla_time_spent_save($request_id,$data,$tech_id,'regular');
					$time_spent = sla_time_spent($request_id,$tech_id);
					$result = $this->sla_return_text($time_spent,$sla_duration,$request_id);
				}
            }
			else{
				$time_spent = sla_time_spent($request_id,$tech_id);
				$result = $this->sla_return_text($time_spent,$sla_duration,$request_id);
			}
		return;	
    }
	// copied sla_result to sla_result1
	public function sla_result_org($request_id,$tech_id)
    {
        $sla_duration = $this->sla_duration;
		//echo $sla_duration;die;
        //CURRENT TIME BEFORE OR AT 6:00
		
        if(strtotime($this->current_date_time)<= strtotime($this->work_end_time))
            {   
				//new code start 
				$requestData = $this->getRequestStatus($request_id);
				$qry = $this->db->query("SELECT * FROM request_resolution_hd WHERE user_request_id='$request_id' AND resolution_is_active=1");
				$resolutionData = $qry->row();
					
				if($requestData->task_status == 'query_to_user' || $requestData->task_status == 'hold'){
					//save hold and query to user status time
					
					$time_spent_ = $this->get_querytouserData($request_id);	
					if($time_spent_ == 0){
						$time_spent_ +=  (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
					}else{
						$time_spent_1 = (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
						$time_spent_ = $time_spent_+$time_spent_1;
					}
					
					//$time_spent_ += (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
					
					$unworkTime = array(
						'clock_duration'=>$time_spent_,
						'clock_duration_date'=>date('Y-m-d',strtotime($this->current_date_time)),
						'user_request_id'=>$request_id,
						'technician_id'=>$tech_id,
						'request_status_code'=>$requestData->task_status
					);
					$this->sla_time_spent_save($request_id,$unworkTime,$tech_id,'spacial');
				}else{
					if(getClockTime($request_id,$tech_id)){
						$time_spent = sla_time_spent($request_id,$tech_id);
						if($time_spent == 0){
							$time_spent += (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
						}else{
							$time_spentk = $time_spent;
							$time_spent_temp = (strtotime($this->current_date_time) - strtotime($resolutionData->resolution_created_dt));
							$time_spent5 = 60 + $time_spentk;
							$time_spent = 0;
							$time_spent = $time_spent5;
						}
						/*echo 'time spent ('.$time_spent.') '.$time_spent_temp .'='.(strtotime($this->current_date_time).'('.$this->current_date_time.')' .'-'. strtotime($resolutionData->resolution_created_dt)).'('.$resolutionData->resolution_created_dt.')';
						echo '<br> sum val ='.$time_spent/60; */
					}else{
						$time_spent = sla_time_spent($request_id,$tech_id);
						if($time_spent == 0){
							$time_spent += (strtotime($this->current_date_time) - strtotime($this->request_date_time));
						}else{
							$time_spent = (strtotime($this->current_date_time) - strtotime($this->request_date_time));  
						}
					}
				//new code end
				//check the time spent and save/update the request id
					$data = array("time_spent" => $time_spent);
					$this->sla_time_spent_save($request_id,$data,$tech_id,'regular');
					$time_spent = sla_time_spent($request_id,$tech_id);
					$result = $this->sla_return_text($time_spent,$sla_duration,$request_id);
				}
            }
			else{
				$time_spent = sla_time_spent($request_id,$tech_id);
				$result = $this->sla_return_text($time_spent,$sla_duration,$request_id);
			}
		return;	
    }
	// this function return text if sla is not reached else ensures email are send to users
	
	public function sla_return_text($time_spent,$sla_duration,$request_id)
    {
		$user_req      = getReqInfoByUserRequestId($request_id);
		$group_id      = $user_req["group_id"];
		$technician_id = $user_req["technician_id"];
		if((strtotime($this->current_date_time)>= strtotime($this->work_strt_time))&&(strtotime($this->current_date_time)<= strtotime($this->work_end_time))){
        if($time_spent > $sla_duration){
			
			// shoot an email if the sla is reached to the respective technician  and the next level 
			if(is_request_escalated($request_id,'task_escalated')==0){
				
				//saving the escalation info to the request escalation hd
				$this->save_escalation_hd($request_id);
				$data = array(
				   "request_id" => $request_id
				);
				$this->technician_escalation_email($request_id);
				$this->user_escalation_email($request_id);
				$this->manager_escalation_email($request_id);
				
				// save to history the sla 
				add_history('ReqSlaAdded',$data);
			}
			//$result = "SLA REACHED";
			
			// escalated level =1
			if(is_request_escalated($request_id,'task_escalated_level_one')==0 && is_request_escalated($request_id,'task_escalated') == 1){
				//get escalataion level one
				$qry = $this->db->query("SELECT request_escalation_dt FROM  request_escalation_hd where user_request_id = $request_id");
				if($qry->num_rows()>0){
					$result = $qry->row();
					$escalation_dt = $result->request_escalation_dt;
					$escalation_emp_code = get_escalation_emp_code($request_id);
					if(!empty($escalation_emp_code)){
					$qry_escal = $this->db->query("SELECT * FROM escalation_level_hd where emp_code = '$escalation_emp_code' AND is_active = 1");
					$qry_escal_data = $qry_escal->row();
					// calculate the escalation reached at the level two
					$escalataion_level_one = $sla_duration + ($qry_escal_data->escalation_time*60);
					if($time_spent > $escalataion_level_one){
						// update the user request hd table for the escalation
						$this->db->where("user_request_id",$request_id);
						$this->db->update("user_request_hd",array("task_escalated_level_one" => '1'));
						//insert the esclation_level
						$data = array (
							  "group_id"           =>    $group_id,
							  "technician_id"      =>    $technician_id,
							  "esclation_level"	   =>    $qry_escal_data->esc_level,
							  "user_request_id"    =>    $request_id,
							  "request_escalation_dt" => date("Y-m-d H:i:s"),
							  "emp_code"               =>   $escalation_emp_code
						);
						$this->db->insert("request_escalation_hd",$data);
						//shoot email after the task_escalated_level_one is set as one
						$this->manager_escalation_email($request_id);
					}
				 }	
					
				}
				
			}
			
			// escalated level =2
			if(is_request_escalated($request_id,'task_escalated_level_two')==0 && is_request_escalated($request_id,'task_escalated_level_one')==1 && is_request_escalated($request_id,'task_escalated')==1)
			{
				
				$qry = $this->db->query("SELECT request_escalation_dt FROM  request_escalation_hd where user_request_id = $request_id");
				if($qry->num_rows()>0){
					$result = $qry->row();
					$escalation_dt = $result->request_escalation_dt;
					$escalation_emp_code = get_escalation_emp_code($request_id);
					if(!empty($escalation_emp_code)){
					$qry_escal = $this->db->query("SELECT * FROM escalation_level_hd where emp_code = '$escalation_emp_code' AND is_active = 1");
					$qry_escal_data = $qry_escal->row();
					// calculate the escalation reached at the level two
					$escalataion_level_one = $sla_duration + ($qry_escal_data->escalation_time*60);
					if($time_spent > $escalataion_level_one){
						// update the user request hd table for the escalation
						$this->db->where("user_request_id",$request_id);
						$this->db->update("user_request_hd",array("task_escalated_level_two" => '1'));
						//insert the esclation_level
						$data = array (
							  "group_id"           =>    $group_id,
							  "technician_id"      =>    $technician_id,
							  "esclation_level"	   =>    $qry_escal_data->esc_level,
							  "user_request_id"    =>    $request_id,
							  "request_escalation_dt" => date("Y-m-d H:i:s"),
							  "emp_code"              =>   $escalation_emp_code
						);
						$this->db->insert("request_escalation_hd",$data);
						//shoot email after the task_escalated_level_two is set as one
						$this->manager_escalation_email($request_id);
					}
				  }
				}
				//echo 'escalate level 2';die();
			}
			
        }
        else
        {    
           //$time_remaining = $sla_duration - $time_spent;
            //$result = "Time Remaining :".gmdate("H:i:s",$time_remaining);
        }
       // return $result;
	 }
	     return;
    }
	
   /* //this function is moved to helper for quick reference
   public function sla_time_spent($id)
    {
        $result = 0;
       // $qry = $this->db->select('*')->from('sla_time_log_hd')->where('user_request_id',$id)->get();
	   $qry = $this->db->query("SELECT SUM(time_spent) AS time_spent FROM `sla_time_log_hd` where user_request_id = $id group by user_request_id");
        if($qry->num_rows() > 0){
            $row  = $qry->row();
            $result = $row->time_spent;
        }
        return $result;
    }*/
  
	public function sla_time_spent_save($request_id,$data,$tech_id,$save_code)
    {   
		switch($save_code){
			case 'regular':
				$qry = $this->db->select('*')->from('sla_time_log_hd')->where('user_request_id',$request_id)->where('technician_id',$tech_id)->where('time_spent_date',date('Y-m-d',strtotime($this->current_date_time)))->get();
				if($qry->num_rows() > 0){
					//update the record 
					$this->db->where("user_request_id",$request_id);
					$this->db->where("technician_id",$tech_id);
					$this->db->where("time_spent_date",date('Y-m-d',strtotime($this->current_date_time)));
					$this->db->update("sla_time_log_hd",$data);
				}
				else{
					//save the record
					$data["time_spent_date"] = date('Y-m-d',strtotime($this->current_date_time));
					$data["user_request_id"] = $request_id;
					$data["technician_id"] = $tech_id;
					$this->db->insert("sla_time_log_hd",$data);
				}
			break;
			
			case 'spacial':
				$qry = $this->db->select('*')->from('clock_request_hd')->where('user_request_id',$request_id)->where('technician_id',$tech_id)->where('clock_duration_date',date('Y-m-d',strtotime($this->current_date_time)))->get();
				if($qry->num_rows() > 0){
					//update the record 
					$clockRequestData = $qry->row();
					$this->db->where('id_clock_request',$clockRequestData->id_clock_request);
					$this->db->update('clock_request_hd',$data);
				}else{
					//save the record
					$this->db->insert('clock_request_hd',$data);
				}
			break;
		}
		return TRUE;
    }
	
	public function save_escalation_hd($user_request_id)
	{	$user_req      = getReqInfoByUserRequestId($user_request_id);
		$group_id      = $user_req["group_id"];
		$technician_id = $user_req["technician_id"];
		$user_request_id = $user_req["request_id"];
		// update the user request hd table for the escalation
		$this->db->where("user_request_id",$user_request_id);
		$this->db->update("user_request_hd",array("task_escalated" => '1'));
		
		// update the user request person hd table for the escalation
		$this->db->where("user_request_id",$user_request_id);
		$this->db->update("request_assign_person_hd",array("is_escalated" => '1',"escalated_dt" =>date("Y-m-d H:i:s")));
		
	    //echo "<pre>";print_r($user_req);exit(); 
		$escalation_level = get_escalation_level($user_request_id);
		$escalation_emp_code = get_escalation_emp_code($user_request_id);
		$data = array(
		      "group_id"               =>    $group_id,
			  "technician_id"          =>    $technician_id,
			  "user_request_id"        =>    $user_request_id,
			  "request_escalation_dt"  => date("Y-m-d H:i:s"),
			  "esclation_level"        =>   $escalation_level,
			  "emp_code"               =>   $escalation_emp_code
		    );
		$this->db->insert("request_escalation_hd",$data);
		
		//save to escalated_task record 
		$data_escalated = array( 
			"group_id"                 => $group_id,
			"technician_id"            => $technician_id,
			"user_request_id"          => $user_request_id,
			"request_assign_person_dt" => date("Y-m-d H:i:s"),
			"escalation_status"        => '1',
			"escalation_level_id"      => $escalation_level,
			"escalated_empcode"        => $escalation_emp_code
		);
		$this->db->insert("escalated_task",$data_escalated);
		//echo "testing";
		return;
	}
	public function working_days_in_month()
    {	//echo "<pre>"; print_r(get_holidays_for_month());exit();
        $start_date = new Datetime(date("Y-m-01"));
		$start_date_copy = new Datetime(date("Y-m-01")); 
		$date_next_month = date_add($start_date,date_interval_create_from_date_string("1 month"));
        $end_date = new Datetime($date_next_month->Format("Y-m-d"));
        $holiday = get_holidays_for_month();// get from database table  //"2017-08-07","2017-08-15","2017-08-25"
       // print_r($holiday);die;
	   
		$interval = new DateInterval('P1D');
        $daterange = new DatePeriod($start_date_copy,$interval,$end_date );
		
	   //weeks we have holiday will be same 
        $weeks_holidays = array(2,4);
			//echo "<pre>";print_r($daterange);die;
		foreach($daterange as $date){
			
		  if(!empty($holiday))
		  {	
           if($date->Format("N")<6  && !in_array($date->Format("Y-m-d"),$holiday) ||
            ($date->Format("N")==6 && !in_array(ceil( date( 'j', strtotime( $date->Format("Y-m-d") ) ) / 7 ),$weeks_holidays))) 
            {
              $result[] = $date->Format("Y-m-d");
            }
		  }
		  else if($date->Format("N")<6 || ($date->Format("N")==6 && !in_array(ceil( date( 'j', strtotime( $date->Format("Y-m-d") ) ) / 7 ),$weeks_holidays)))
		  {	
			$result[] = $date->Format("Y-m-d"); 
		  }		
        }
		//echo "<pre>";print_r($result);
		return $result;
        
        
    }
	public function test_script()
	{
		//var_dump(is_queried_by_tech(103));
		
		$time_in_sec =sla_time_spent(104,15)-request_sla_stopped(104,15);echo format_time($time_in_sec) ;exit();
	}
	public function test_duration()
	{	$user_request_id =74;
		//$emp_code = "SG0360";
		$emp_code = get_escalation_emp_code($user_request_id);
		echo $emp_code;
		//echo get_escalation_emp_code($user_request_id);exit();
		$email = get_email_by_emp_code($emp_code);
		if(!empty($email)){
		 echo $email[0]->u_email;die;
		}
		
	}
	// sla cron to check the request which are not complete and has request active with no escalation
	public function sla_cron()
	{
		$requests = get_all_request();
		//echo "<pre>"; print_r($requests);exit();
		foreach($requests as $request){
		   $this->sla_check($request);
		   echo $request.PHP_EOL;
		}
		
	}
	public function test_func_cron()
	{
		$this->load->view("backend/cron_file");
	}
	public function get_elapse_time()
	{
		$postData = $this->input->post();
		$request_id = $postData['request_id'];
		$user_req = getReqInfoByUserRequestId($request_id);
		$technician_id = $user_req['technician_id'];
		$timespent = sla_time_spent($request_id,$technician_id);
		$time_format = format_time($timespent);
		echo $time_format;
		
	}
	//*********************************common function starts****************************************************
	//send email - SMTP
	public function sendOutlookEmail($toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);
		$mail = '';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name="Helpdesk";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('Nelson.komathan@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	//******************************common function ends********************************************************
	
	
	//*************** LDAP Sync Start*******************************************
	//<<NEW CODE >> Twice a day 
	public function cron_job_update_ladp(){
		$executeTime = date('H:i');
		//if($executeTime == '23:00'|| $executeTime == '23:02'){
			set_time_limit(600);
			$server = ldap_serverhost;
			$basedn = ldap_basedn;
			$handle = sysu;
			$password = sysp;
			$dn = ldap_basedn . "\\" . $handle;
			$connect = ldap_connect($server);
			ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
			
			//var_dump(ldap_bind($connect, $dn, $password));
			if ($ds = @ldap_bind($connect, $dn, $password)) {
				//$filters = "(samaccountname=$handle)";
				//$filters = "(samaccountname='Ravi.Arsid')";
				$filters = "(samaccountname=*)";
				//$filters = "(cn=*)"; // get all results
				$results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
				$entries = ldap_get_entries($connect, $results);
				echo '<pre>'.count($entries);
				//print_r($entries);
				//exit;
				if(count($entries) > 0){
					for ($i = 0; $i < count($entries); $i++) {
						$ldap_userinfo = array(
							//'usnchanged' => $entries[$i]["usnchanged"][0],
							'samaccountname' => $entries[$i]["samaccountname"][0],
							'unique_id'=> base64_encode($entries[$i]["objectguid"][0]),
							'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
							'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
							'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
							'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
							'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
							'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
							'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
							'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
							'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
							'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
							'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
						);
						//print_r($entries[$i]);
						//echo '<br>';
						//echo ' - '.$i;
						echo '<hr>';
						//print_r($ldap_userinfo);
						
						if($ldap_userinfo['employeeid'] !='' || $ldap_userinfo['mail']!=''){
							$lognSql = "select * from ldapusers where unique_id='" .$ldap_userinfo['unique_id']. "' ";
							$local_result = $this->Base_model->run_query($lognSql);
							if (count($local_result) == 0) {
								$local_user_insert['unique_id'] = $ldap_userinfo['unique_id'];
								$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
								$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
								$local_user_insert['u_name'] = $ldap_userinfo['name'];
								$local_user_insert['u_email'] = $ldap_userinfo['mail'];
								$local_user_insert['u_designation'] = $ldap_userinfo['description'];
								$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
								$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
								
								$local_user_insert['u_image'] = NULL;
								$local_user_insert['u_cover_image'] = NULL;
								
								$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
								//$local_user_insert['u_status'] = 1;
								$local_user_insert['u_role'] = 0;
								$this->Base_model->insert_operation($local_user_insert, 'ldapusers');
								echo 'swwwq<br>';
								print_r($ldap_userinfo);
							} else {
								$local_user_insert['u_ldap_userid'] = $ldap_userinfo['samaccountname'];
								$local_user_insert['u_employeeid'] = $ldap_userinfo['employeeid'];
								$local_user_insert['u_name'] = $ldap_userinfo['name'];
								$local_user_insert['u_email'] = $ldap_userinfo['mail'];
								$local_user_insert['u_designation'] = $ldap_userinfo['description'];
								$local_user_insert['u_mobile'] = $ldap_userinfo['mobile'];
								$local_user_insert['u_streetaddress'] = $ldap_userinfo['streetaddress'];
								
								$local_user_insert['u_image'] = NULL;
								$local_user_insert['u_cover_image'] = NULL;
								
								$local_user_insert['u_create_date'] = date('Y-m-d H:i:s');
								//$local_user_insert['u_status'] = 1;
								$local_user_insert['u_role'] = 0;
								
								$logi_where['unique_id'] = $ldap_userinfo['unique_id'];
								$this->Base_model->update_operation($local_user_insert, 'ldapusers', $logi_where);
								echo 'swwwq<br>';
								print_r($ldap_userinfo);
							}
						}
					}
				}
			}
		//}
	}
	
	
	//public function logintimeupdate(){
	//	$qry = $this->db->query("UPDATE users_new SET u_status=1 WHERE u_role='0'");
	//	echo $this->db->affected_rows();
	//	echo $this->db->last_query();
	//}
	
	public function resetCount(){
		$executeTime = date('H:i');
		if($executeTime == '23:00' || $executeTime == '23:05'){
			$qry= $this->db->query("UPDATE track_assign_count_hd SET task_assign_count='0' WHERE 1");
			echo $this->db->affected_rows();
		}
	}
	
	//*************** LDAP Sync End*******************************************
	
	//*************** Email Sync Start*******************************************
	//retrive email tickets start <<< NEW CODE >>> CRON JOB TO FETCH data from outlook **
	public function getOutlookmail(){
		// get emails 
		$emailList = $this->receiveOutlookEmail();
		//print_r($emailList); die; exit;
		//insert the email into the database
		foreach($emailList['emailsss'] as $emailRecord){
			$emailExists = $this->Helpdesk_model->checkEmailExsitsByUniqueId($emailRecord['email_unique_id']);
			$outlookEmail = array();
			$emailId = 0;
			if(count($emailExists) > 0){} else {
				//insert email to database 
				$outlookEmail = array(
					'email_unique_id'=>$emailRecord['email_unique_id'],
					'email_from'=>$emailRecord['email_from'],
					'email_subject'=>$emailRecord['email_subject'],
					'email_body'=>$emailRecord['email_body'],
					'email_created'=>date('Y-m-d H:i:s')
				);
				$this->db->insert('outlook_email',$outlookEmail);
				$emailId = $this->db->insert_id();
				$postData = getEmpByEmail1($emailRecord['email_from']);
				$behalf_of = $emailRecord['email_from'];
				$request_type = 'Self';
				
				if($postData->u_name !=''){
					$data = array(
						'email_id'=>$emailId,
						'user_id'=>$postData->u_id,
						'user_code'=>$postData->u_employeeid,
						'request_type'=>'email', 
						'behalf_of'=>$behalf_of,
						'service_request_type'=>$request_type,
						'request_priority'=>'High',
						'requester_name'=>$postData->u_name,
						'requester_location_id'=>1,
						'state_id'=>7,
						'requester_location_name'=>$postData->u_streetaddress,
						'dept_id'=>5,
						'category_id'=>62,
						'subcategory_id'=>292, 
						'item_id'=>301, 
						'request_subject'=>$emailRecord['email_subject'], 
						'request_desc'=>$emailRecord['email_body'],
						'request_created_dt'=>date('Y-m-d H:i:s')
					);
					// filter and insert into database 
					if($this->Helpdesk_model->save_user_request_outlook($data)){
						$requestId = $this->db->insert_id();
						$tech_id = 7; 	//get the technician id and assign him to task
						$item_id = 301;
						$this->Helpdesk_model->assign_request($tech_id,$requestId,$item_id);
						$this->sendEmailTemple($requestId,2,$tech_id,$behalf_of,$postData->u_name);
						// assign technician
						//$this->cron_job_new(1);
					}
				}
			}
		}
	}
	
	//for testing use this ********************************************
	public function getOutlookmail_test(){
		// get emails 
		$emailList = $this->receiveOutlookEmail();
		echo '<pre>';
		//print_r($emailList); die; exit;
		//insert the email into the database
		$outlookEmail = array();
		$emailId = 0;
		foreach($emailList['emailsss'] as $emailRecord){
			$emailExists = $this->Helpdesk_model->checkEmailExsitsByUniqueId($emailRecord['email_unique_id']);
			$outlookEmail = array();
			echo '<br>------------------------------------------------------------<br><hr>';
			$outlookEmail = array(
					'email_unique_id'=>$emailRecord['email_unique_id'],
					'email_from'=>$emailRecord['email_from'],
					'email_subject'=>$emailRecord['email_subject'],
					'email_body'=>$emailRecord['email_body'],
					'email_created'=>date('Y-m-d H:i:s')
				);
			print_r($outlookEmail);
			if(count($emailExists) > 0){} else { break;
				//insert email to database 
				$outlookEmail = array(
					'email_unique_id'=>$emailRecord['email_unique_id'],
					'email_from'=>$emailRecord['email_from'],
					'email_subject'=>$emailRecord['email_subject'],
					'email_body'=>$emailRecord['email_body'],
					'email_created'=>date('Y-m-d H:i:s')
				);
				print_r($outlookEmail);
				//$this->db->insert('outlook_email',$outlookEmail);
				//$emailId = $this->db->insert_id();
				$emailId = 0;
				$postData = getEmpByEmail1($emailRecord['email_from']);
				$behalf_of = $emailRecord['email_from'];
				$request_type = 'Self';
				$data = array(
					'email_id'=>$emailId,
					'user_id'=>$postData->u_id,
					'user_code'=>$postData->u_employeeid,
					'request_type'=>'email', 
					'behalf_of'=>$behalf_of,
					'service_request_type'=>$request_type,
					'request_priority'=>'High',
					'requester_name'=>$postData->u_name,
					'requester_location_id'=>1,
					'state_id'=>7,
					'requester_location_name'=>$postData->u_streetaddress,
					'dept_id'=>5,
					'category_id'=>62,
					'subcategory_id'=>292, 
					'item_id'=>301, 
					'request_subject'=>$emailRecord['email_subject'], 
					'request_desc'=>$emailRecord['email_body'],
					'request_created_dt'=>date('Y-m-d H:i:s')
				);
				// filter and insert into database 
				/*if($this->Helpdesk_model->save_user_request_outlook($data)){
					$requestId = $this->db->insert_id();
					$tech_id = 7; 	//get the technician id and assign him to task
					$item_id = 301;
					$this->Helpdesk_model->assign_request($tech_id,$requestId,$item_id);
					$this->sendEmailTemple($requestId,2,$tech_id,$behalf_of,$postData->u_name);
					// assign technician
					//$this->cron_job_new(1);
				}*/
			}
		}
	}
	
	//****************************************************************************DEL
	public function sendEmailTemple($request_id=0,$temp_code=0,$tech_id=0,$u_email='',$u_name='',$frm_email = 0){
		if($frm_email == 0){
			$qry_1 = $this->db->query("SELECT * FROM users u LEFT JOIN user_request_hd ur ON (u.u_employeeid = ur.user_code) WHERE ur.user_request_id='$request_id'");
			$empData = $qry_1->row();
		}else{
			$qry_1 = $this->db->query("SELECT *,behalf_of as u_email FROM user_request_hd WHERE user_request_id='$request_id'");
			$empData = $qry_1->row();
		}
		
		if($u_name !=''){
			$displayname = $u_name;
		}else{
			$displayname = $empData->requester_name;
		}
		switch($temp_code){
			case 11:
				//The status of the request can be tracked at http://helpdesk.skeiron.com .
				$subject="Your request has been logged with request ID ".getRequestCode($request_id)." ";
				$msg='<h3></h3>
					Dear '.ucfirst($displayname).',

					<p style="margin-left:25px">This is an acknowledgement mail for your request.<br><br>

					Your request has been created with Ticket ID '.getRequestCode($request_id).'.<br><br>

					The subject of the request is : '.$empData->request_subject.'. <br><br>

					</p>
					<br><br>
				<p>	*** This is an automatically generated email, please do not reply *** </p><br>
			<p>
			Regards,<br>
			HelpDesk,<br>
			Skeiron Group.
			</p>';
			break;
			
			case 2:
				$qry_2 = $this->db->query("SELECT * FROM technician_hd WHERE technician_id = '$tech_id' AND technician_is_active = '1'");
				$tech_data = $qry_2->row();
				
				//The status of the request can be tracked at http://helpdesk.skeiron.com .
				
				$subject="Your request has been logged with request ID ".getRequestCode($request_id)." ";
				$msg='<h3></h3>
						Dear '.ucfirst($displayname).',

						<p style="margin-left:25px">This is an acknowledgement mail for your request.<br><br>

						Your request has been created with Ticket ID '.getRequestCode($request_id).'.<br><br>
						
						Your request with id '.getRequestCode($request_id).' has been assigned to '.ucfirst($tech_data->tech_name).'. <br><br>

						The subject of the request is : '.$empData->request_subject.'. <br><br>

						</p>
						<br><br>
					<p>	*** This is an automatically generated email, please do not reply *** </p><br>
				<p>
				Regards,<br>
				HelpDesk,<br>
				Skeiron Group.
				</p>';
			
				$msgSubject= "Created by: ".ucfirst($displayname)." on ".std_time();
				$messagebody = "Operation :CREATE ,performed by:".ucfirst($displayname).".<br/>";
				$messagebody .= "From Host/IP Address:".$_SERVER["REMOTE_ADDR"]."<br/>";
				// add history 
				$postData = array(
				    "request_id"=>$request_id
			    );
		        add_history('ReqCreated',$postData);
				if($tech_data->technician_email !=''){
					$tech_email = $tech_data->technician_email; //uncomment this line
					//$tech_email = 'Dev@SKEIRON.COM';
				}else{
					$tech_email = 'Dev@SKEIRON.COM'; // NEED TO Change Before live
				}
				
			break;
		}
		
		$tech_subject = 'Request ID ##'.getRequestCode($request_id).'## has been assigned to you.';
		$tech_email_temp = $this->sendemail_technician($request_id,$displayname,$empData->request_created_dt,$empData->item_id,$empData->dept_id,$empData->request_subject, $empData->request_desc);
		
		
		if($u_email !=''){
			$empDt = getEmpByEmail($u_email);
			
			//echo 'email : '.$u_email;
			$this->sendOutlookEmail($u_email,$subject,$msg);
			
			//send email to technician
			$this->sendOutlookEmail($tech_email,$tech_subject,$tech_email_temp);
			
			$sys_email_ary = array(
				'to_emails'=>$u_email,
				'subject'=>$subject,
				'description'=>$msg,
				'created_date'=>date('Y-m-d H:i:s'),
				'user_request_id'=>$request_id
			);
			$this->db->insert('system_emails',$sys_email_ary);
			$technician_id = $tech_id;
		   // savehistory($history_type="0",$request_id,$technician_id,$empDt->u_employeeid,$u_email,$msgSubject,$messagebody);
			
		}else{
			//echo 'email : '.$empData->u_email;
			$empDt = getEmpByEmail($u_email);
			
			$this->sendOutlookEmail($empData->u_email,$subject,$msg);
			
			//send email to technician
			$this->sendOutlookEmail($tech_email,$tech_subject,$tech_email_temp);
			
			$sys_email_ary = array(
				'to_emails'=>$empData->u_email,
				'subject'=>$subject,
				'description'=>$msg,
				'created_date'=>date('Y-m-d H:i:s'),
				'user_request_id'=>$request_id
			);
			$this->db->insert('system_emails',$sys_email_ary);
			$technician_id = $tech_id;
		   // savehistory($history_type="0",$request_id,$technician_id,$empDt->u_employeeid,$empData->u_email,$msgSubject,$messagebody);
		}
	
		return true;
	}
	
	//** Email to technician
	public function sendemail_technician($request_code = '',$requester_name='', $request_created_date='',$item_id='',$dept_id='',$request_subj='', $request_desc=''){
		$dept_data = get_record_by_id_code('dept',$dept_id);
		$dept_name = $dept_data->dept_name;
		
		$item_data = get_record_by_id_code('item',$item_id);
		$item_name = $item_data->item_name;
		
		$request_due_date = date('M d ,Y h:i:s A',strtotime('+2 hours',strtotime($request_created_date)));
		$tech_msg ='<p><b>Request details are :</b></p>
			<p><b>Requested by :</b>'.$requester_name.'</p>
			<!--<p><b>Created by :</b> Ravi Arsid</p>-->
			<!--<p><b>Due by date :</b> '.$request_due_date.'</p>-->
			<p><b>Department :</b> '.$dept_name.'</p>
			<!--<p><b>Category :</b> '.$dept_name.'</p>-->
			<p><b>Item :</b> '.$item_name.'</p>
			<p><b>Subject :</b> '.$request_subj.'</p>
			<p><b>Description :</b></p>
			<p>'.$request_desc.'</p>
			<br><br><br><br>
			<p>*** This is an automatically generated email, please do not reply *** </p>
			<p>Regards,<br>
			HelpDesk,<br>
			Skeiron Group.<br></p>
		';
		return $tech_msg;
	}
	//****************************************************************************DEL END
	
	
	
	
	public function receiveOutlookEmail(){
		$emailReciAry = array();
		$imap = imap_open("{outlook.office365.com:143}", smu,smp);
		$num = imap_num_msg($imap); 
		$num_retrive = $num;// - 10;
		//print_r($num_retrive);
		//echo '<table border="1" style="border-color: red;border-width: 4px;">';
		//echo '<pre>';
		//$str_out .='<tr><th> Subject </th><th> Body </th></tr>';
		for($i=1;$i<=$num;$i++){
			$attachments = imap_fetchstructure($imap, $i);
			$unique_id = imap_uid($imap, $i);
			$headerData = imap_headerinfo ($imap ,$i);			
			echo '<br><hr><br><br><br>--------------------Header------------------------------';
			echo '<br><b>From id :</b>'.$headerData->from[0]->mailbox.'@'.$headerData->from[0]->host.' -- <b>Unique Id</b> '.$unique_id.' <br>';
			echo '<b>Subject :</b>'.$headerData->subject.'<br>';
			echo '--------------------Body------------------------------';
			echo '<br><br>';			
			//print_r($attachments);echo 'attachhhhhment<br>';
			//$body = imap_qprint(imap_body($imap, $i));
			//$body =  imap_fetch_overview($imap, $i);
			$body =  imap_fetchstructure($imap, $i);
			//print_r($body);
			$structure = imap_fetchstructure($imap, $i);
			$flattenedParts = $this->flattenParts($structure->parts);
			foreach($flattenedParts as $partNumber => $part) {
				$message = '';
				//print_r($part);exit;
				switch($part->type) {
					case 0:
						// the HTML or plain text part of the email
						$message = $this->getPart($imap, $i, $partNumber, $part->encoding);
						//echo $message;
						// now do something with the message, e.g. render it
					break;
				
					case 1:
						// multi-part headers, can ignore
					break;
					case 2:
						// attached message headers, can ignore
					break;
				
					case 3: // application
					case 4: // audio
					case 5: // image
					case 6: // video
					case 7: // other
						$filename = $this->getFilenameFromPart($part);
						if($filename) {
							// it's an attachment
							$attachment = $this->getPart($imap, $i, $partNumber, $part->encoding);
							//echo '<pre> attachment <br>';print_r($attachment);
							// now do something with the attachment, e.g. save it somewhere
						} else {
							// don't know what it is
						}
					break;
				}
			}
			//$str_out .='<tr><td> '.$headerData->subject.' </td><td>'.$body .'</td></tr>';
			$emailReciAry['emailsss'][] = array(
				'email_unique_id' =>$unique_id,
				'email_from' =>$headerData->from[0]->mailbox.'@'.$headerData->from[0]->host,
				'email_subject'=>$headerData->subject,
				'email_body'=>$message
			);			
			print_r($emailReciAry);
		}
		imap_close($imap);
		//echo $str_out;
		//echo '</table>';
		
		return $emailReciAry;
	}
	
	//IMPA SUPPORT FUNCTION  START 
	public function flattenParts($messageParts, $flattenedParts=array(), $prefix = '', $index = 1, $fullPrefix = true) {
		foreach($messageParts as $part) {
			$flattenedParts[$prefix.$index] = $part;
			if(isset($part->parts)) {
				if($part->type == 2) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.', 0, false);
				}
				elseif($fullPrefix) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.');
				}
				else {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix);
				}
				unset($flattenedParts[$prefix.$index]->parts);
			}
			$index++;
		}
		return $flattenedParts;
	}
	
	public function getPart($connection, $messageNumber, $partNumber, $encoding) {
		$data = '';
		$data = imap_fetchbody($connection, $messageNumber, $partNumber);
		switch($encoding) {
			case 0: return $data; // 7BIT
			case 1: return $data; // 8BIT
			case 2: return $data; // BINARY
			case 3: return base64_decode($data); // BASE64
			case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
			case 5: return $data; // OTHER
		}	
	}

	public function getFilenameFromPart($part) {
		$filename = '';
		if($part->ifdparameters) {
			foreach($part->dparameters as $object) {
				if(strtolower($object->attribute) == 'filename') {
					$filename = $object->value;
				}
			}
		}
		if(!$filename && $part->ifparameters) {
			foreach($part->parameters as $object) {
				if(strtolower($object->attribute) == 'name') {
					$filename = $object->value;
				}
			}
		}
		return $filename;
	}
	//IMPA SUPPORT FUNCTION  END 
	
	
	//*************** Email Sync End*******************************************
	
		//New app for ping IP start 
	public function pingUrl12(){
		$qry = $this->db->query("SELECT * FROM ip_monitor WHERE ip_is_active=1");
		$ipData = $qry->result();
		foreach($ipData as $ipRecord){
			$domain = '';
			$domain = $ipRecord->ip_number;
			if($domain!=''){
				echo '<br><br>';
				$pingData = exec('ping -n 1 -w 1 '.$domain);
				$findme = 'Lost';
				$position = 0;
				$position = strpos($pingData, $findme);
				if($position > 0){
					//down
					echo '<br>IP is down IP - '.$url;
					//$email = 'network.support@skeiron.com';
					$email = 'avinash.kawre@skeiron.com';
					$subject = 'The IP '.$domain.' is down - '.$ipRecord->ip_email_id;
					$msg = 'Hi <br> The IP '.$domain.' is down please check and do the needful to resolve the issue. <br> Please contact the below technicians for the same <br><br><br><br>Shaijad kureshi :- 9970567872<br>Avinash Kawre :- 7350389754<br>johny Anthony :- 9819618486';
					/*
						Dear Sir,
						 The IP '.$domain.' is down. <br>We are working on the issue, once it get resolved, we will update you for the same. <br><br><br><br> Please feel free to contact the below engineers for any futher assitance <br><br><br><br>Shaijad Kureshi :- 9970567872<br>Avinash Kawre :- 7350389754<br>Johny Anthony :- 9819618486'
					*/
					$linkCount = 1;
					if($ipRecord->ip_email_shoot==0 && $ipRecord->link_down_count>=5){
						$this->sendOutlookEmail($email,$subject,$msg);
						$updateAry = array(
							'ip_email_shoot'=>1
						);						
						$this->updateIPData($updateAry,$ipRecord->ip_monitor_id);
					}
					$linkCount += $ipRecord->link_down_count;
					$updateAry1 = array(
						'link_down_count'=>$linkCount
					);
					$this->updateIPData($updateAry1,$ipRecord->ip_monitor_id);
				}else{
					$updateAry2 = array(
						'link_down_count'=>0
					);
					$this->updateIPData($updateAry2,$ipRecord->ip_monitor_id);
					//up and running
					echo '<br>IP up and running IP - '.$domain;
				}
				echo '(IP - '.$domain.' - position '.$position.'. Status '.$pingData.')';
			}
		}

	
		
		
		/*$starttime = microtime(true);
			$file      = fsockopen ($domain, 80, $errno, $errstr, 10);
			$stoptime  = microtime(true);
			$status    = 0;
			//print_r($file);
			if (!$file){
				$status = -1;  // Site is down
				//echo '<br>IP is down '.$status.' IP - '.$domain;
			} else {
				fclose($file);
				$status = ($stoptime - $starttime) * 1000;
				$status = floor($status);
				//echo '<br>IP is UP and running '.$status.' IP - '.$domain;
			}
		*/
		
		
		/*$qry = $this->db->query("SELECT * FROM ip_monitor WHERE ip_is_active=1 AND ip_email_shoot=0");
		$ipData = $qry->result();
		foreach($ipData as $ipRecord){
			$url = '';
			$url = $ipRecord->ip_number;
			if($url == NULL) return false;  
			$ch = curl_init($url);  
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);  
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
			$data = curl_exec($ch);  
			print_r($data);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
			echo '<br>';
			print_r($httpcode);
			echo '<br>';
			curl_close($ch);  
			if($httpcode>=200 && $httpcode<415){  
				echo '<br>IP up and running '.$httpcode.' IP - '.$url;
			} else {  
				//return false;  
				echo '<br>IP is down '.$httpcode.' IP - '.$url;
				$email = 'avinash.kawre@skeiron.com';
				$subject = 'The IP '.$url.' is down';
				$msg = 'Hi <br> The IP '.$url.' is down please check and do the needful to resolve the issue';
				//$this->sendOutlookEmail($email,$subject,$msg);
				$updateAry = array(
					'ip_email_shoot'=>1
				);
				//$this->db->where('ip_monitor_id',$ipRecord->ip_monitor_id);
				//$this->db->update('ip_monitor',$updateAry);
			}  
		}
		return true;
		*/
	}
	
	public function updateIPData($updateAry, $tablId){
		$this->db->where('ip_monitor_id',$tablId);
		$this->db->update('ip_monitor',$updateAry);
		return true;
	}
	//New app for ping IP end

	// system report code start
	public function downloadPDFV2(){
		//ob_start();
		//echo FCPATH."uploads/Signature_LOGO.png"; die;
		ini_set('max_execution_time', 5000);
		$this->load->library('PDF_CON_RV/tcpdf');
		
		// create new PDF document
		$date = date('d-m-Y'); //'07-09-2017';
		
		foreach(get_dept() as $deptData){
			$contents = '';
			$contents_status = '';
			$dept_id = $deptData->dept_id;
			$dept_name = $deptData->dept_name;
			$pdfrk = $dept_id.'a';
			$pdfrk = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			// add a page
			$pdfrk->AddPage('L', 'A3');
			
			$assign_tech_list = get_request_by_date(date('Y-m-d',strtotime($date)),$dept_id);
			
			$request_counter = get_request_count_all($dept_id,date('Y-m-d',strtotime($date)));
			$inprocess = 0;
			$onhold = 0;
			$completed = 0;
			$inprocess = ($request_counter['inprocess'] !='') ? $request_counter['inprocess'] : 0;
			$onhold = ($request_counter['hold']!='') ? $request_counter['hold']: 0;
			$completed = ($request_counter['completed']!='') ? $request_counter['completed'] : 0;
			
			
			//with rates
			$contents.='<div><center><img src="http://intranet.skeiron.com/assets/images/logo/Logo.png" style="width:120px;margin-left:100px;"></center></div>';
			
			$contents.='Helpdesk - Ticket report for '.$deptData->dept_name.' <br>Generated by System for date : '.date('d-m-Y').'<br>Total records : '.count($assign_tech_list).'<br>Created Time : '.date('M d, Y h:i:s A').'.<br><hr><br><br>';
			
			$contents.= '<br><br><div><br><br><img src="http://chart.apis.google.com/chart?cht=p3&chd=t:'.$inprocess.','.$onhold.','.$completed.'&chco=ffeb3b|f89406|4fa928&chs=300x100&chl=Inprocess('.$inprocess.')|Onhold('.$onhold.')|Completed('.$completed.')" /></div>';
			
			$contents.='<table cellpadding="1" cellspacing="1" border="1">'; 
			
			$contents.='<tr style="font-weight:bold"><th style="font-family:Times New Roman;width:70px;">  Sr No  </th><th style="font-family:Times New Roman">  ID  </th><th style="font-family:Times New Roman">  Subject  </th><th style="font-family:Times New Roman">  Requester Name  </th><th style="font-family:Times New Roman">  Technician  </th><th style="font-family:Times New Roman">  Status </th><th style="font-family:Times New Roman">  Created date  </th><th style="font-family:Times New Roman">  Site  </th><th style="font-family:Times New Roman">  Priority  </th><th style="font-family:Times New Roman">  Group  </th></tr>';
			//--------------------------------------------------------------------------------------------------
			//$groups = get_groups(); 
			//$group_sr ='<option  value="">Not Assigned</option>'; 
			//foreach($groups as $group): 
			//	$group_sr .= '<option   value="'. $group->group_id.'">'.  $group->group_name .'</option>'; 
			//endforeach; 
						
			$i=1; 
			foreach($assign_tech_list as $reportRecord){ 
				
				$dept_data = get_record_by_id_code('dept',$dept_id);
				$category_data = get_record_by_id_code('category',$reportRecord->category_id);
				$subcategory_data = get_record_by_id_code('subcategory',$reportRecord->subcategory_id);
				$item_data = get_record_by_id_code('item',$reportRecord->item_id);
				$contents.='<tr><td style="font-family:Times New Roman;width:70px;">  '.$i.'  </td><td style="font-family:Times New Roman">  '.getRequestCode($reportRecord->ur_user_request_id).'  </td><td style="font-family:Times New Roman">'.ucfirst($reportRecord->request_subject).'  </td><td style="font-family:Times New Roman">  '.ucfirst($reportRecord->requester_name).'  </td><td>';
				$contents.=($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : 'Not assign';
				
				$contents.='</td><td >'; 
				if($reportRecord->request_assign_person_id != NULL){
					if($reportRecord->is_request_completed == 1){  
						$contents_status=' <span style="background-color:green;color:#fff;border-radius: 10px;">&nbsp;Completed&nbsp;&nbsp;</span> ';
					}else if($reportRecord->is_request_completed == 0 && $reportRecord->task_status == 'hold'){
						$contents_status=' <span style="background-color:#F89406;color:#fff;border-radius: 10px;">&nbsp;On Hold&nbsp;&nbsp;</span> ';
					} else {
						$contents_status=' <span style="background-color:#ffeb3b;color:#1c6b94;border-radius: 10px;">&nbsp;Inprocess&nbsp;&nbsp;</span> ';
					}
					if($reportRecord->is_request_completed == 0 && $reportRecord->task_status == 'query_to_user'){
						$contents_status=' <span style="background-color:#F89406;color:#fff;border-radius: 10px;">&nbsp;Query to user&nbsp;</span> ';
					}
				}else{
					$contents_status=' <span style="background-color:orange;color:#fff;border-radius: 10px;">&nbsp;Not assign&nbsp;&nbsp;</span> ';
				}
				$contents.= $contents_status;
				$contents.='</td><td style="font-family:Times New Roman"> '.date('d-m-Y h:i:s A',strtotime($reportRecord->request_created_dt)).' </td><td style="font-family:Times New Roman">'.ucfirst($reportRecord->requester_location_name).' </td><td style="font-family:Times New Roman"> <span class="square '.ucfirst($reportRecord->request_priority).'"></span>'.ucfirst($reportRecord->request_priority).' </td>'; 
				
				$contents.='<td style="font-family:Times New Roman"> ';
				$contents.= ($reportRecord->group_name !=NULL)? ucfirst($reportRecord->group_name): 'Not assign';
				$contents.=' </td>';			
				$contents.='</tr>';
				
				$i++; 
			}  
			//--------------------------------------------------------------------------------------------------
			
			$contents .='</table>';
			
			$pdfrk->writeHTML($contents, true, false, true, false, '');
			
			//print_r($contents);
			//Close and output PDF document
			$pdfrk->lastPage();
			
			$filename= date('d_M_Y')."_".$dept_id.".pdf";
			$filelocation = "C:\\inetpub\\wwwroot\\iconnect\\uploads\\helpdesk_daily";//windows
			$fileNL = $filelocation."\\".$filename;//Windows
			$pdfrk->Output($fileNL,'F');
			if($this->sendAttachEmail($filename,$dept_name,$dept_id)){
				echo '<br>Mail sent successfully';
			}else{
				echo '<br>Error while email sent';
			}
			
		}
		////$pdf->Output('ticket_report.pdf','I');
		exit;
		////$pdf->Output($filename,'F');
	}
	
	
	public function sendAttachEmail($fileName,$deptName,$dept_id){
		ob_start();
		error_reporting(0);
		$mail = '';
		$emp_head_data = get_head_email_by_dept_id($dept_id);
		$subject = 'Daily Helpdesk Report '.date('d-m-Y').' for '.$deptName;
		$mesge = 'Dear Sir / Madam,<br><br>Please find the attached helpdesk report for '.date('d-m-Y').' of '.$deptName.'.<br><br><p>	*** This is an automatically generated email, please do not reply *** </p><br>';
		
		//$toEmail = 'ravi.kumar@skeiron.com';
		//if($dept_id == 5){
			$toEmail = $emp_head_data;
		//}else{
			//$toEmail = 'ravi.kumar@skeiron.com';
		//}
		//$toEmail = 'Shaijad.Kureshi@skeiron.com';
		//$cc = 'Vijay.Maurya@skeiron.com';
		$cc = 'ravi.kumar@skeiron.com';
		$account=smu;
		$password=smp;
		$from=smu;
		$to=$toEmail;
		$from_name="Helpdesk";
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('ravi.kumar@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		$mail->AddAttachment(FCPATH."uploads/helpdesk_daily/".$fileName);
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
			return false;
		}else{
			//echo "<br>E-Mail has been sent";
			return true;
		}
	}
	// system report code end
	
	//daily task code start 
	public function get_helpdesk_rest_data(){
		ini_set('max_execution_time', 3000);
		$ticketDataAryComb = array();
		$deptData = get_dept();
		$taskStatus = array('inprocess','onhold','completed');
		foreach($deptData as $deptRecord){
			for($rk=0;$rk<count($taskStatus);$rk++){
				$ticketData = get_request_all($taskStatus[$rk],$deptRecord->dept_id);
				foreach($ticketData as $reportRecord){
					
					if($reportRecord->request_assign_person_id != NULL){
						if($reportRecord->is_request_completed == 1){  
							$taskStst = 'Completed';
						}else if($reportRecord->is_request_completed == 0 && $reportRecord->task_status == 'hold'){
							$taskStst = 'On Hold';
						}
						else{
							$taskStst = 'Inprocess';
						}
					}else{
						$taskStst = 'Not assign';
					}
					
					$user_req = getReqInfoByUserRequestId($reportRecord->ur_user_request_id);
					$get_sla = get_item_sla_duration($user_req["item_id"]);
					$sla = $get_sla->sla_duration.''.$get_sla->sla_duration_unit;
					
					$technician_id = $user_req['technician_id'];
					$time_spent = sla_time_spent($reportRecord->ur_user_request_id,$technician_id);
					$time_elapse = format_time($time_spent);
					
					$time_spent1 = sla_time_spent($reportRecord->ur_user_request_id,$technician_id);
					$get_sla1 = get_item_sla_duration($user_req["item_id"]);
					$is_sla_violated = ($time_spent1 >sla_duration_item($get_sla1))? 'Escalated': 'In time';
					
					$ticketDataAry = array(
						'DeptName'=>$deptRecord->dept_name,
						'ID'=>getRequestCode($reportRecord->ur_user_request_id),
						'Subject'=>ucfirst($reportRecord->request_subject),
						'Request Desc'=>($reportRecord->request_desc !='') ? ucfirst(strip_tags($reportRecord->request_desc)) : 'Not available',
						'Requester Name'=>ucfirst($reportRecord->requester_name),
						'Technician'=>($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : 'Not assign',
						'Status'=>$taskStst,
						'Created date'=>date('d-m-Y',strtotime($reportRecord->request_created_dt)),
						'Site'=>ucfirst($reportRecord->requester_location_name),
						'Priority'=>ucfirst($reportRecord->request_priority),
						'Group'=>($reportRecord->group_name !=NULL) ? ucfirst($reportRecord->group_name) : 'Not assign',
						'SLA'=>$sla,
						'Time Elapse'=>$time_elapse,
						'Escalation Status'=>$is_sla_violated,
						'total_count'=>1
					);
					$ticketDataAryComb[] = $ticketDataAry;
				}
			}
		}
		echo json_encode($ticketDataAryComb);
	}
	
	public function get_helpdesk_rest_data_for_testing_only(){
		ini_set('max_execution_time', 3000);
		$ticketDataAryComb12 = array();
		$deptData = get_dept();
		$taskStatus = array('inprocess','onhold','completed');
		
		foreach($deptData as $deptRecord){
			for($rk=0;$rk<count($taskStatus);$rk++){
				$ticketData = get_request_all($taskStatus[$rk],$deptRecord->dept_id);
				foreach($ticketData as $reportRecord){
					if($reportRecord->request_assign_person_id != NULL){
						if($reportRecord->is_request_completed == 1){  
							$taskStst = 'Completed';
						}else if($reportRecord->is_request_completed == 0 && $reportRecord->task_status == 'hold'){
							$taskStst = 'On Hold';
						}
						else{
							$taskStst = 'Inprocess';
						}
					}else{
						$taskStst = 'Not assign';
					}
					
					$ticketDataAry = array(
						'DeptName'=>$deptRecord->dept_name,
						'ID'=>getRequestCode($reportRecord->ur_user_request_id),
						'Subject'=>ucfirst(strip_tags(stripslashes($reportRecord->request_subject))),
						'Request Desc'=>($reportRecord->request_desc !='') ? ucfirst(strip_tags(stripslashes($reportRecord->request_desc))) : "Not available",
						'Requester Name'=>ucfirst($reportRecord->requester_name),
						'Technician'=>($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : "Not assign",
						'Status'=>$taskStst,
						'Created date'=>date('d-m-Y',strtotime($reportRecord->request_created_dt)),
						'Site'=>ucfirst($reportRecord->requester_location_name),
						'Priority'=>ucfirst($reportRecord->request_priority),
						'Group'=>($reportRecord->group_name !=NULL) ? ucfirst($reportRecord->group_name) : "Not assign",
						'total_count'=>1
					);
					//if($reportRecord->ur_user_request_id !='443'){
						$ticketDataAryComb12[] = $ticketDataAry;
					//}
				}
			}
		}
		$jsonData = json_encode($ticketDataAryComb12,true);
		echo $jsonData;
	}
	
	public function rest_data(){
		$data['post_type'] = (isset($postData['filter_type'])) ? $postData['filter_type'] : 'by_date';
		$data['filter_date'] = (isset($postData['filter_date'])) ? $postData['filter_date'] : date('d-m-Y');
		$report_data = json_encode($this->Dailytask_model->get_employee_report1($data['post_type'],$data['filter_date']),true);
		$all_tasks =  $this->Dailytask_model->get_all_emp_data();
		$tasksDate = $this->Dailytask_model->get_dates();
		$array1 = array();
		$taskRecordAry1 = array();
		foreach($tasksDate as $taskDt){
			$array2 = array();
			foreach($all_tasks as $taskRecord){
				if($taskDt->start_date == $taskRecord["start_date"]){
					$taskRecordAry = array(
						"u_name"=>$taskRecord["u_name"],
						"task_id"=>$taskRecord["task_id"],
						"emp_code"=>$taskRecord["emp_code"],
						"project_name"=>$taskRecord["project_name"],
						"start_date"=>$taskRecord["start_date"],
						"end_date"=>$taskRecord["end_date"],
						"task_priority"=>$taskRecord["task_priority"],
						"task_activity"=>$taskRecord["task_activity"],
						"task_status"=>$taskRecord["task_status"],
						"remarks"=>$taskRecord["remarks"],
						"created_date"=>$taskRecord["created_date"],
						"task_is_active"=>$taskRecord["task_is_active"],
						"task_hrs"=>$taskRecord["task_hrs"],
						"task_min"=>$taskRecord["task_min"]
					);
					$array2[] = $taskRecord["emp_code"];
					$taskRecordAry1[] = $taskRecordAry;
				}else{
					/*$array1 = array('SG0271','SG0272','SG0282','SG0283','SG0300','SG0308','SG0360','SG0361','SG0365','SG0355');
					$result = array_diff($array1, $array2);
					foreach($result as $emp_code_){
						$emp_data = get_all_details_by_emp_code($emp_code_);
						$taskRecordAry = array(
							"u_name"=>$emp_data->u_name,
							"task_id"=>0,
							"emp_code"=>$emp_code_,
							"project_name"=>"",
							"start_date"=>$taskDt->start_date,
							"end_date"=>"",
							"task_priority"=>"",
							"task_activity"=>"",
							"task_status"=>"",
							"remarks"=>"",
							"created_date"=>"",
							"task_is_active"=>0,
							"task_hrs"=>0,
							"task_min"=>0
						);
						//print_r($taskRecordAry);
					}*/
				}
			}
			
			//$array1 = array('SG0271','SG0272','SG0282','SG0283','SG0300','SG0308','SG0360','SG0361','SG0365','SG0355');
			$array1 = array('SG0271','SG0282','SG0283','SG0308','SG0360','SG0361','SG0365','SG0355');
			
			$result = array_diff($array1, $array2);
			foreach($result as $emp_code_){
				$emp_data = get_all_details_by_emp_code($emp_code_);
				$taskRecordAry = array(
					"u_name"=>$emp_data->u_name,
					"task_id"=>0,
					"emp_code"=>$emp_code_,
					"project_name"=>"",
					"start_date"=>$taskDt->start_date,
					"end_date"=>"",
					"task_priority"=>"",
					"task_activity"=>"",
					"task_status"=>"",
					"remarks"=>"",
					"created_date"=>"",
					"task_is_active"=>0,
					"task_hrs"=>0,
					"task_min"=>0
				);
				$taskRecordAry1[] = $taskRecordAry;
			}
		}
		echo json_encode($taskRecordAry1);
	}
	public function check_user_not_filled(){
		echo 'working';
		$data['post_type'] = (isset($postData['filter_type'])) ? $postData['filter_type'] : 'by_date';
		$data['filter_date'] = (isset($postData['filter_date'])) ? $postData['filter_date'] : date('d-m-Y');
		$report_data = json_encode($this->Dailytask_model->get_employee_report1($data['post_type'],$data['filter_date']),true);
		$record = json_decode($report_data);
		$person_ary = array();
		$count_ary = array();
		foreach($record as $rec){
			$person_ary[] = $rec->u_name.'('.$rec->count.')';
			$count_ary[] = $rec->count;
			$array2[] = $rec->emp_code;
		}
		//$array1 = array('SG0271','SG0272','SG0282','SG0283','SG0300','SG0308','SG0360','SG0361','SG0365','SG0355');
		$array1 = array('SG0271','SG0282','SG0283','SG0308','SG0360','SG0361','SG0365','SG0355');
		$result = array_diff($array1, $array2);
		echo '<br>Employee who are not filled the daily task are';
		$ik = 1;
		foreach($result as $emp_code_){
			$emailSubj = '';
			$emailMsg = '';
			$emp_data = get_all_details_by_emp_code($emp_code_);
			$notfilledPersons .= '<br>'.$ik.') '.$emp_data->u_name;
			$emailSubj = 'Daily task reminder';
			$emailMsg = 'Dear '.ucfirst($emp_data->u_name).',<br><p>You have not filled the daily task for the date : '.date('d-m-Y').'.</p><br><p>Please fill the daily task ASAP. </p><br><p>	*** This is an automatically generated email, please do not reply *** </p><br>';
			$this->sendDailyTaskEmail('Daily Task',$emp_data->u_email,$emailSubj,$emailMsg,'Vijay.Maurya@skeiron.com');
			$this->sendDailyTaskEmail('Daily Task','ravi.kumar@skeiron.com',$emailSubj,$emailMsg);
			$ik++;
		}
		$eSubj ='Daily task not filled person names';
		$eMsg = 'Dear Sir,<br> <p>The below list of persons, who are not filled the daily task.</p>'.$notfilledPersons.'<br><p>	*** This is an automatically generated email, please do not reply *** </p><br>';
		$this->sendDailyTaskEmail('Daily Task','Vijay.Maurya@skeiron.com',$eSubj,$eMsg);
		$this->sendDailyTaskEmail('Daily Task','ravi.kumar@skeiron.com',$eSubj,$eMsg);
	}
	
	public function sendDailyTaskEmail($fromName='',$toEmail='', $subject='',$mesge='',$cc=''){
		ob_start();
		error_reporting(0);
		$mail = '';
		$account=smu1;
		$password=smp1;
		$from=smu1;
		$to=$toEmail;
		$from_name=$fromName;
		$subject=$subject;
		$msg = $mesge;
		$mail = new PHPMailer();
		$mail->SMTPDebug = 0;
		$mail->do_debug = 0;
		$mail->IsSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = "smtp.office365.com";
		$mail->SMTPAuth= true;
		$mail->Port = 587;
		$mail->Username= $account;
		$mail->Password= $password;
		$mail->SMTPSecure = 'tls';
		$mail->From = $from;
		$mail->FromName= $from_name;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->AltBody = '';
		
		//Explode by comma so that we get an array of emails.
        $emailsExploded = explode(",", $to);
		//If the array isn't empty, loop through
		if(!empty($emailsExploded)){    
			foreach($emailsExploded as $emailAddress){
				$mail->AddAddress(trim($emailAddress));
			}
		} else{
			$mail->AddAddress($to);
		}
		
		$ccExploded = explode(",", $cc);
		if(!empty($ccExploded)){    
			foreach($ccExploded as $ccAddress){
				$mail->AddCC(trim($ccAddress));
			}
		} else{
			$mail->AddCC($cc);
		}
		
		//$mail->AddCC('ravi.kumar@skeiron.com');// added to check if user on cc receives email 
		
	    //$mail->AddAttachment(FCPATH."uploads/Signature_LOGO.png");
		if(!$mail->send()){
			//echo "<br>Mailer Error: " . $mail->ErrorInfo;
		}else{
			//echo "<br>E-Mail has been sent";
		}
		return true;
	}
	public function findUser(){
		$userData = exec('whoami');
		$userLog = array(
			'log_info'=>$userData,
			'login_date_time'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("user_log_in",$userLog);
		$eSubj = 'Server log and event log';
		$eMsg = 'Hi Ravi,<br> User info '.$userData.' is event happen on the server 192.168.20.21 please take an action to the user <br><p>	*** This is an automatically generated email, please do not reply *** </p><br>';
		$this->sendDailyTaskEmail('Server event','ravi.kumar@skeiron.com',$eSubj,$eMsg);
	}
	//daily task code end
	
}