<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Api_model'); 
	}
	
	public function index(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/homepage';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function admin_dashboard(){
		$data = $this->get_ledap();
		$data['include'] = 'backend_iconnect/homepage';
		$data['action_tab'] = 'dashboard';
		$this->load->view('backend_iconnect/masterpage',$data);
	}
	
	public function get_tv_images(){
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$tv_data = $this->Api_model->get_all_announcements_tv_api();
			
			foreach($tv_data as $tv_record){
				$mailerAry = array(
					'mailer_id'=>$tv_record->an_id,
					'image_url'=>base_url().'uploads/announcement_tv/'.$tv_record->an_file
				);
				$mailer_list[] = $mailerAry;
			}
			echo json_encode($mailer_list);
		}else{
			echo 'Invalid Request';
		}
	}
	
}

