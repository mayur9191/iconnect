<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['event_login'] = 'login/event_login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



$route['learn_and_development'] = 'login/learn_and_development';
$route['documents/Programmes_Offerred'] = 'login/documents/Programmes_Offerred';
$route['documents/L_and_D_Calendars'] = 'login/documents/L_and_D_Calendars';
$route['documents/Forms'] = 'login/documents/Forms';
$route['documents/Processes'] = 'login/documents/Processes';
$route['documents/Templates'] = 'login/documents/Templates';
$route['documents/Mis'] = 'login/documents/Mis';
$route['documents/Presentation_of_Trainings'] = 'login/documents/Presentation_of_Trainings';
$route['aboutus'] = 'login/about_us';
$route['key_info'] = 'login/key_info';
$route['roles'] = 'login/roles';