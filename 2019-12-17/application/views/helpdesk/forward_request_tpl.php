<!DOCTYPE html>
<html>
  <head>
    <title>Forward Request</title>
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  table tr td{
    padding:3px;
  }
   .forwardrequest{
  	width:750px;
  	float:left;
  }
  </style>
  </head>
  <body>
  	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  		<form>
	  		<tbody>
	            <tr>
	                <td>Forward Request</td>
	                <td>
                        <span class="mandatory">* </span>
                        <span style="font:normal 12px Arial, Helvetica, sans-serif">Mandatory Field</span>
	                </td>

	            </tr>
	            <tr>
	            	<td>To</td>
	            	<td>
	            		<input type="email"  class="forwardrequest" required name="ëmail" id="email">
	            	</td>
	            </tr>
	            <tr>
	            	<td>cc</td>
	            	<td>
	            		<input type="email" class="forwardrequest"  required name="cc" id="cc">
	            	</td>
	            </tr>
	            <tr>
	            	<td>Subject</td>
	            	<td>
	            		<input type="email"  class="forwardrequest" required name="subject" id="subject">
	            	</td>
	            </tr>
	            <tr>
	            	<td>Description</td>
	            	<td>
	            		<textarea class="forwardrequest" ></textarea>
	            	</td>
	            </tr>
	            <tr>
	            	<td>Attachment</td>
	            	<td>
	            		 <input type="file"   name="attachfile" id="attachfile">
	            	</td>
	            </tr>
	            <tr>
	            	
	            	<td>
	            		 <button type="submit" class="btn btn-primary btn-sm">Send</button>
			             <button type="submit" class="btn btn-default btn-sm">Save</button>
			             <button type="submit" class="btn btn-default btn-sm">Send for Review</button>
			             <button type="button" onclick="window.close();" class="btn btn-default btn-sm">Cancel</button>
	            	</td>
	            </tr>
	        </tbody>    
	  	</form>	
  	</table>
  	<!--<div class="container">
        <h3>Forward Request</h3>

	    <form>
			<div class="form-group">
			    <label for="email">To:</label>
			    <input type="email" class="form-control" required name="ëmail" id="email">
			</div>
			<div class="form-group">
			    <label for="cc">cc:</label>
			    <input type="email" class="form-control" name="cc" id="cc">
			</div>
			<div class="form-group">
			    <label for="subject">Subject:</label>
			    <input type="email" class="form-control" required  name="subject" id="subject">
			</div> 
			<div class="form-group">
			    <label for="description"></label>
			    <textarea class="form-control"></textarea>
			</div>  
			<div class="form-group">
			    <label for="pwd">Attach File:</label>
			    <input type="file" class="form-control"  name="attachfile" id="attachfile">
			</div> 
			  <button type="submit" class="btn btn-primary btn-sm">Send</button>
			  <button type="submit" class="btn btn-default btn-sm">Save</button>
			  <button type="submit" class="btn btn-default btn-sm">Send for Review</button>
			  <button type="button" onclick="window.close();" class="btn btn-default btn-sm">Cancel</button>
	    </form>
	</div>-->    
  </body>
</html>