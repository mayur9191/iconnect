<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />

   <?php //include APPPATH . "views/includs/hedder_code.php" ?>
<style>
body{margin-top:20px;
background:#fff;
}


.inbox .inbox-menu ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .inbox-menu ul li {
    height: 30px;
    padding: 5px 15px;
    position: relative
}

.inbox .inbox-menu ul li:hover,
.inbox .inbox-menu ul li.active {
    background: #e4e5e6
}

.inbox .inbox-menu ul li.title {
    margin: 20px 0 -5px 0;
    text-transform: uppercase;
    font-size: 10px;
    color: #d1d4d7
}

.inbox .inbox-menu ul li.title:hover {
    background: 0 0
}

.inbox .inbox-menu ul li a {
    display: block;
    width: 100%;
    text-decoration: none;
    color: #3d3f42
}

.inbox .inbox-menu ul li a i {
    margin-right: 10px
}

.inbox .inbox-menu ul li a .label {
    position: absolute;
    top: 10px;
    right: 15px;
    display: block;
    min-width: 14px;
    height: 14px;
    padding: 2px
}

.inbox ul.messages-list {
    list-style: none;
    margin: 15px -15px 0 -15px;
    padding: 15px 15px 0 15px;
    border-top: 1px solid #d1d4d7
}

.inbox ul.messages-list li {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    cursor: pointer;
    margin-bottom: 10px;
    padding: 10px
}

.inbox ul.messages-list li a {
    color: #3d3f42
}

.inbox ul.messages-list li a:hover {
    text-decoration: none
}

.inbox ul.messages-list li.unread .header,
.inbox ul.messages-list li.unread .title {
    font-weight: 700
}

.inbox ul.messages-list li:hover {
    background: #e4e5e6;
    border: 1px solid #d1d4d7;
    padding: 9px
}

.inbox ul.messages-list li:hover .action {
    color: #d1d4d7
}

.inbox ul.messages-list li .header {
    margin: 0 0 5px 0
}

.inbox ul.messages-list li .header .from {
    width: 49.9%;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .header .date {
    width: 50%;
    text-align: right;
    float: right
}

.inbox ul.messages-list li .title {
    margin: 0 0 5px 0;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .description {
    font-size: 12px;
    padding-left: 29px
}

.inbox ul.messages-list li .action {
    display: inline-block;
    width: 16px;
    text-align: center;
    margin-right: 10px;
    color: #d1d4d7
}

.inbox ul.messages-list li .action .fa-check-square-o {
    margin: 0 -1px 0 1px
}

.inbox ul.messages-list li .action .fa-square {
    float: left;
    margin-top: -16px;
    margin-left: 4px;
    font-size: 11px;
    color: #fff
}

.inbox ul.messages-list li .action .fa-star.bg {
    float: left;
    margin-top: -16px;
    margin-left: 3px;
    font-size: 12px;
    color: #fff
}

.inbox .message .message-title {
    margin-top: 30px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px
}

.inbox .message .header {
    margin: 20px 0 30px 0;
    padding: 10px 0 10px 0;
    border-top: 1px solid #d1d4d7;
    border-bottom: 1px solid #d1d4d7
}

.inbox .message .header .avatar {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    height: 34px;
    width: 34px;
    float: left;
    margin-right: 10px
}

.inbox .message .header i {
    margin-top: 1px
}

.inbox .message .header .from {
    display: inline-block;
    width: 50%;
    font-size: 12px;
    margin-top: -2px;
    color: #d1d4d7
}

.inbox .message .header .from span {
    display: block;
    font-size: 14px;
    font-weight: 700;
    color: #3d3f42
}

.inbox .message .header .date {
    display: inline-block;
    width: 29%;
    text-align: right;
    float: right;
    font-size: 12px;
    margin-top: 18px
}

.inbox .message .attachments {
    border-top: 3px solid #e4e5e6;
    border-bottom: 3px solid #e4e5e6;
    padding: 10px 0;
    margin-bottom: 20px;
    font-size: 12px
}

.inbox .message .attachments ul {
    list-style: none;
    margin: 0 0 0 -40px
}

.inbox .message .attachments ul li {
    margin: 10px 0
}

.inbox .message .attachments ul li .label {
    padding: 2px 4px
}

.inbox .message .attachments ul li span.quickMenu {
    float: right;
    text-align: right
}

.inbox .message .attachments ul li span.quickMenu .fa {
    padding: 5px 0 5px 25px;
    font-size: 14px;
    margin: -2px 0 0 5px;
    color: #d1d4d7
}

.inbox .contacts ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .contacts ul li {
    height: 30px;
    padding: 5px 15px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis!important;
    position: relative;
    cursor: pointer
}

.inbox .contacts ul li .label {
    display: inline-block;
    width: 6px;
    height: 6px;
    padding: 0;
    margin: 0 5px 2px 0
}

.inbox .contacts ul li:hover {
    background: #e4e5e6
}


.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}


.panel-success>.panel-heading {background-image:linear-gradient(to bottom,#c9da2c 0,#cbdb2b 100%)}
.with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {color:#ffffff}
.with-nav-tabs.panel-success .nav-tabs > li > a:hover{background-color:#ffffff; color:#1c6b94;}
.backtab {background-color:#1c6b94; color:#ffffff;}
.backtab:hover {background-color:#1c6b94; color:#ffffff;}

.backtab1 {background-color:#cada2b; color:#1c6b94;}
.backtab1:hover {background-color:#cada2b; color:#1c6b94;}
#accordion .panel .panel-heading{
	padding-top: 10px;
    padding-bottom: 10px
}
.margin-top-10 { margin-top: 1.0em;margin-left: 12px; }
#update_request,#reset_request,#cancel_request,#update_requester,#reset_requester,#cancel_requester{
    display:none;
}

</style>
</head>
<body>
<?php //include APPPATH . "views/includs/top_navbar.php" ?>
<?php //$approvalData = getMaterialApproval($passingData->material_master_id); ?>
<br><br><br>
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
<script src="https://select2.github.io/dist/js/select2.full.js"></script>
<div class="container">

	<div class="row">
		<div class="col-md-12">
            <div class="panel with-nav-tabs panel-success">
			<div class="row inbox">
				<div class="col-md-13">
                    <?php //echo "<pre>";print_r($request_details); ?>
                    <?php //echo "<pre>";print_r($request_more_details); ?>
				<span class="btn backtab btn-block"><b>Request ID :<?php echo $request_details->user_request_id; ?>	</b></span>
					<div class="panel panel">
						<div class="panel-body message">
							<p><b><?php echo $request_details->request_subject; ?>.</b></p>
							<p><b>By <?php echo  $request_details->requester_name; ?></b> on  <?php echo date('M d,Y h:i:s A',strtotime($request_details->request_created_dt)); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Due Date :<?php $duedate = date('M d,Y h:i:s A',strtotime('+2 hours',strtotime($request_details->request_created_dt))); echo $duedate; ?> </p>
						</div>	
					</div>
				</div><!--/.col-->		
			</div>
                <div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1success" data-toggle="tab" id="tab1">&nbsp;&nbsp;&nbsp;&nbsp;Request&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
						<li><a href="#tab2success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;Resolution&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
						<li><a href="#tab3success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;History&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
					</ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1success">
							<div class="row inbox">
								<div class="col-md-13">
									<div class="panel panel-default">
										<span class="btn backtab btn-block"><b>Description</b></span><br>
										<div class="panel-body message">
											<p><?php echo $request_details->request_desc; ?></p>

											

										</div>	
									</div>
								</div><!--/.col-->	
                                <div class="col-md-13">
                                 Requester Conversations   |  [View All Conversations]
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        <i class="glyphicon glyphicon-lock" style="margin-left:1.0em"></i>
                                                        <i class="glyphicon glyphicon-envelope" style="margin-left:3.0em"></i>
                                                            <span style="margin-left:3.0em">System</span>
                                                            <span style="margin-left:0.5em"> on  May 6, 2017 02:15 PM</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <!-- use in class to open collapse div -->
                                                <div id="collapseOne" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>text body goes here.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                        <i class="glyphicon glyphicon-lock" style="margin-left:1.0em"></i>
                                                        <i class="glyphicon glyphicon-envelope" style="margin-left:3.0em"></i>
                                                            <span style="margin-left:3.0em">System</span>
                                                            <span style="margin-left:0.5em"> on  May 6, 2017 02:15 PM</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p><b>To</b>:xyz@skeiron.com</p>
                                                        <p><b>Summary</b></p>
                                                        <p>Your request with id ##1500## has been assigned to Rahul Survase</p>
                                                        <p><b>Description</b></p>
                                                        <p>Dear IT Team,</p>

                                                        <p>we received an request from GRT sir that he is facing some slowness issue in his iMac after software update.</p> 

                                                        <p>request you to kindly check & resolve. </p>

                                                        <p>Thanks,</p>
                                                        <p>Shaijad Kureshi.</p>
                                                        <p><button  onClick="window.open('<?php echo base_url(); ?>test/forward_request','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-primary btn-sm">Forward Request</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                        <i class="glyphicon glyphicon-lock" style="margin-left:1.0em"></i>
                                                        <i class="glyphicon glyphicon-envelope" style="margin-left:3.0em"></i>
                                                            <span style="margin-left:3.0em">System</span>
                                                            <span style="margin-left:0.5em"> on  May 6, 2017 02:15 PM</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p>text body goes here.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
        
                                    </div>
                                </div>    
                                <div class="col-md-13">
                                    <div class="row margin-top-10">
                                        <b>Request Details</b> <button id="request_edit" name="request_edit"> Edit</button>
                                    </div>
                                    <form method="post" action="abc">
                                
                                
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <b>Status</b>
                                                </div>
                                                <div class="col-sm-3">
                                                  <span><?php echo ucfirst($request_more_details->task_status); ?></span>
                                                    <span style="display:none;">
                                                        <select name="status" id="status">
                                                            <option>Select</option>
                                                            <option<?php if($request_more_details->task_status=='close') echo 'selected'; ?>>Closed</option>
                                                            <option <?php if($request_more_details->task_status=='hold') echo 'selected'; ?>>On Hold</option>
                                                            <option <?php if($request_more_details->task_status=='open') echo 'selected'; ?>>Open</option>
                                                            <option <?php if($request_more_details->task_status=='resolve') echo 'selected'; ?>>Resolved</option>
                                                            <option <?php if($request_more_details->task_status=='wip') echo 'selected'; ?>>WIP</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Workstation Number</b></div>
                                                <div class="col-sm-3">
                                                <span>-</span>
                                                <span style="display:none;"><input type="text" id="work_Station" name="work_Station"/></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Mode</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo ucfirst($request_details->request_type); ?></span>
                                                    <span style="display:none;">
                                                        <select name="mode" id="mode">
                                                            <option>Select</option>
                                                            <!--<option>E-mail</option>
                                                            <option>Phone Call</option>
                                                            <option>Web Form</option>-->
                                                            <?php foreach($request_mode as $mode): ?>
                                                            <option <?php echo ($request_details->request_type==$mode->request_type)?'selected':''; ?> value="<?php echo $mode->request_type ?>"><?php echo ucfirst($mode->request_type); ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Priority</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></span>
                                                    <span style="display:none;">
                                                        <select name="priority" id="priority">
                                                            <option>Select</option>
                                                            <option <?php echo ($request_details->request_priority=="High") ?'selected':''; ?>>High</option>
                                                            <option <?php echo ($request_details->request_priority=="Low") ?'selected':''; ?>>Low</option>
                                                            <option <?php echo ($request_details->request_priority=="Low") ?'selected':''; ?>>Medium</option>
                                                            <option <?php echo ($request_details->request_priority=="Normal") ?'selected':''; ?>>Normal</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Level</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="level" id="level">
                                                            <option>Select</option>
                                                            <option>Tier1</option>
                                                            <option>Tier2</option>
                                                            <option>Tier3</option>
                                                            <option>Tier4</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Category</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php $category = get_record_by_id_code('category',$request_details->category_id);echo $category->category_name;  ?></span>
                                                    <span style="display:none;">
                                                        
                                                        <select name="category" id="category">
                                                            <option>Select</option>
                                                            <?php $categories = get_category();?>
                                                            <?php foreach ($categories as  $value) {?>
                                                            <option <?php echo ($request_details->category_id == $value->category_id )?'selected':''; ?> value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Site</b></div>
                                                <div class="col-sm-3">
                                                    <?php //print_r($site_location_hd); ?>
                                                    <span>Pune</span>
                                                    <span style="display:none;">
                                                        <select name="site" id="site">
                                                            <option>Select</option>
                                                            <?php foreach($site_location_hd as $location): ?>
                                                            <option <?php echo ( $request_details->requester_location_name==$location->site_location_name)?'selected':''; ?> value="<?php echo $location->site_location_id; ?>"><?php echo $location->site_location_name;   ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Subcategory</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php $subcategory = get_record_by_id_code('subcategory',$request_details->subcategory_id);echo $subcategory->sub_category_name;  ?></span>
                                                    <span style="display:none;">
                                                        <select name="subactegory" id="subactegory">
                                                            <option>Select</option>
                                                            <?php $subcategories = get_sub_category();?>
                                                            <?php foreach ($subcategories as  $value) {?>
                                                            <option <?php echo ($request_details->subcategory_id == $value->sub_category_id )?'selected':''; ?>  value="<?php echo $value->sub_category_id; ?>"><?php echo $value->sub_category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Group</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span>L1 Desktop Team</span>
                                                    <span style="display:none;">
                                                        <select name="group" id="group">
                                                            <option>Select</option>
                                                            <?php foreach ($groups_hd as  $value) {?>
                                                            <option <?php echo ($value->group_id == $request_more_details->group_id)?'selected':''; ?> value="<?php echo $value->group_id; ?>"><?php echo $value->group_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Item</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span><?php $item = get_record_by_id_code('item',$request_details->item_id);echo $item->item_name;  ?></span>
                                                    <span style="display:none;">
                                                        <?php $items = get_item();  ?>
                                                        <select name="item" id="item">
                                                            <option>Select</option>
                                                            
                                                            <?php foreach($items as $value): ?>
                                                            <option <?php echo ($request_details->item_id== $value->items_id) ? 'selected':''; ?> value=""><?php echo $value->item_name; ?></option>
                                                           <?php endforeach; ?>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Technician</b></div>
                                                <div class="col-sm-3">
                                                 
                                                    <span><?php $technician = get_record_by_id_code('technician',$request_more_details->technician_id);  echo  $technician->tech_name; ?></span>
                                                    <span style="display:none;">
                                                        <select name="technician" id="technician">
                                                            <option>Select</option>
                                                            <?php foreach($technicians_hd as $tech): ?>
                                                            <option <?php echo ($request_more_details->technician_id==$tech->tech_id)?'selected':''; ?> value="<?php echo $tech->tech_id;  ?>"><?php echo $tech->tech_name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Created By</b></div>
                                                <div class="col-sm-3">Abhijeet Susladkar </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Service Category</b></div>
                                                <div class="col-sm-3">
                                                     
                                                     <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="service_category" id="service_category">
                                                            <option>-----Choose-----</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>SLA</b></div>
                                                <div class="col-sm-3">- </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Asset(s)</b></div>
                                                <div class="col-sm-3">
                                                     
                                                    <span>-</span>
                                                    <span style="display:none;">
                                                        <textarea name="assets" id="assets"></textarea>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Created Date</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo date('M d,Y h:i:s A',strtotime($request_details->request_created_dt)); ?></span>
                                                    <span style="display:none;"><input type="text" readonly value="<?php echo date('M d,Y h:i:s A',strtotime($request_details->request_created_dt)); ?>" id="create_date" name="create_date"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Department</b></div>
                                                 <div class="col-sm-3">  
                                                    <span><?php $dept = get_record_by_id_code('dept',$request_more_details->dept_id); echo $dept->dept_name; ?></span>
                                                    <span style="display:none;">
                                                    <select name="dept" id="dept">
                                                        <option>Select</option>
                                                        <?php foreach ($depts_hd as  $value) {?>
                                                        <option <?php echo ($value->dept_id == $request_more_details->dept_id)?'selected':''; ?> value="<?php echo $value->dept_id; ?>"><?php echo $value->dept_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    </span>              

                                                </div>
                                                <div class="col-sm-3"><b>DueBy Date</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo $duedate; ?></span>
                                                    <span style="display:none;"><input type="text" value="<?php echo $duedate; ?>" id="dueby_date" name="dueby_date"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Template</b></div>
                                                <div class="col-sm-3">Default Request </div>
                                                <div class="col-sm-3"><b>Response DueBy Time</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span>-</span>
                                                    <span style="display:none;"><input type="text" value="" id="response_dueby" name="response_dueby"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Resolved Date</b></div>
                                                <div class="col-sm-3">- </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Time Elapsed</b></div>
                                                <div class="col-sm-3">
                                    <?php 
                                        $currentTime = strtotime(date('Y-m-d h:i:s'));
                                        $systemtTime  = strtotime($request_details->request_created_dt);

                                        $timeelapsed = $currentTime - $systemtTime;

                                        echo date('h',strtotime($timeelapsed)).'Hrs '.date('i',strtotime($timeelapsed)).' min';

                                    ?></div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Last Update Time</b></div>
                                                <div class="col-sm-3">May 6, 2017 02:16 PM</div>
                                            </div>
                                        </div>
                                            <button type="submit" id="update_request" name="update_request">Update</button>
                                            <button type="reset" id="reset_request" name="reset_request">Reset</button>
                                            <button type="button" id="cancel_request" name="cancel_request">Cancel</button>
                                    </form>
                                </div>
                                <div class="col-md-13">
                                       <div class="row margin-top-10">
                                         <b>Requester Details</b> <button id="requester_edit" name="requester_edit"> Edit</button>
                                       </div>
                                    <form method="post" action="abc1">
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Requester Name</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span><?php echo $request_details->requester_name; ?></span>
                                                    <span style="display:none;"><input type="text" value="<?php echo $request_details->requester_name; ?>" id="requester_name" name="requester_name"/> </span>
                                                </div>
                                                <div class="col-sm-3"><b>E-mail Address</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span>Asha.Bhandwalkar@skeiron.com</span>
                                                    <span style="display:none;"><input type="email" value="Asha.Bhandwalkar@skeiron.com" id="email_address" name="email_address"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Contact number</b></div>
                                                <div class="col-sm-3">
                                                     <span>020 66278036</span>
                                                    <span style="display:none;"><input type="text" value="020 66278036" id="contact_no" name="contact_no"/> </span>
                                                </div>
                                                <div class="col-sm-3"><b>Mobile number</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span>8805009266</span>
                                                    <span style="display:none;"><input type="text" value="8805009266" id="mobile_no" name="mobile_no"/> </span>
                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Department</b></div>
                                                <div class="col-sm-3">
                                                    <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="depts" id="depts">
                                                            <option>-----Choose-----</option>
                                                            <option>Skeiron Group</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Business Impact</b></div>
                                                <div class="col-sm-3">
                                                    <span>-</span>
                                                    <span style="display:none;">
                                                        <select name="business_impact" id="business_impact">
                                                            <option>-----Choose-----</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" id="update_requester" name="update_requester">Update</button>
                                        <button type="reset" id="reset_requester" name="reset_requester">Reset</button>
                                        <button type="button" id="cancel_requester" name="cancel_requester">Cancel</button> 
                                    </form>
                                </div>	
							</div>
						</div>
						
						
						
                        <div class="tab-pane fade" id="tab2success">
							<div class="row inbox">
								<div class="col-md-12">
								
									<form class="form-horizontal" role="form">
										<div class="form-group">
											<div class="col-sm-12">
												  <textarea class="form-control select2-offscreen" id="editor"></textarea>
											</div>
										</div>
										
										<div class="form-group ">
											<label for="cc" class="col-sm-2 control-label">Update request status to</label>
											<div class="col-sm-3">
												<select name="statuschange" class="form-control select2-offscreen">
													<option value="">Select Status</option>
													<option value="open">Open</option>
													<option value="wip">WIP</option>
													<option value="on_hold">On Hold</option>
													<option value="closed">Close</option>
													<option value="resolved">Resolved</option>
												</select>
											</div>
										</div>
										<button type = "button" class="btn btn-success" > Submit </button>
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Open Modal</button>
									</form>
								</div><!--/.col-->
							</div>
							
							
							<!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Status change comment</h4>
								  </div>
								  <div class="modal-body">
									<textarea class="form-control select2-offscreen" placeholder="Comment mandatory"></textarea>
								  </div>
								  <div class="modal-footer">
									<button type = "button" class="btn btn-success" > Submit </button>
								  </div>
								</div>
								
							  </div>
							</div>
							
							
							
							
						</div>
						 
						
						 <div class="tab-pane fade" id="tab3success">
							<div class="row inbox">
								<div class="col-md-13">
									<div class="panel panel-default">
										<span class="btn backtab btn-block"><b>Request History</b></span><br>
										<div class="panel-body message">
											<p class="backtab1">Created by Shaijad Kureshi on Apr 3, 2017 07:42 PM</p>
											<p>Operation : CREATE , Performed by : Shaijad Kureshi</p>
											<p>From Host/IP Address: 192.168.22.189</p>
											<p>Technician assigned to request through Tech Auto Assign</p>
											<p  class="backtab1">Updated by Shaijad Kureshi on Apr 3, 2017 07:43 PM</p>
											<p>Request Updated by Shaijad Kureshi</p>
											<p>Group changed from L1 Desktop Team to None</p>
											<p>Technician changed from Rahul Survase to Shaijad Kureshi</p>
											<p>Time of technician assignment changed from Apr 3, 2017 07:42 PM to Apr 3, 2017 07:43 PM</p>
										</div>	
									</div>
								</div><!--/.col-->	

							</div>
						</div>
						
                    </div>
                </div>
            </div>
        </div>
	</div>


</div>


<!-- email modal start -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">replay</button>
<INPUT type="button" value="New Window!" onClick="window.open('<?php echo base_url()."helpdesk/emailcomposer";?>','mywindow','width=1100,height=620,left=110,top=20')" /> 

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- email modal end -->





	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
  
<script>
	var trVal = 1;
	$(document).ready(function(){
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
		$("select[name='statuschange']").change(function(){
			var statusVal = $(this).val();
			if(statusVal == 'open' || statusVal == ''){
			}else{
				$("#myModal").modal('show');
			}
		});
		
		
		$("#tab1").click(function(){
			$(this).attr('aria-expanded',true);
			$(this).find('li').addClass('active');
            //alert("hello");
			$("#tab4success").addClass('active');
            //$("#tab5success").addClass('active in');

			$("#tab2").attr('aria-expanded',false);
			$("#tab2").find('li').removeClass('active');
		});
		
		$("#tab21").click(function(){
			$("#tab1").attr('aria-expanded',false);
			$("#tab1").parent('li').removeClass('active');
			
			$("#tab2").attr('aria-expanded',true);
			$("#tab2").parent('li').addClass('active');
			
			$("#tab2success").addClass('active in');
			$("#tab1success").removeClass('active in');
		});
		
		
		
		
		/*var myTable = 
		$('#dynamic-table').DataTable( {
			bAutoWidth: true,
			"aoColumns": [null, null,null,null, null,null,null, null,null,null],
			"aaSorting": [],
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
			//"bProcessing": true,
			//"bServerSide": true,
			//"sAjaxSource": "http://127.0.0.1/table.php"	,

			//,
			//"sScrollY": "200px",
			//"bPaginate": false,

			//"sScrollX": "100%",
			//"sScrollXInner": "120%",
			"bScrollCollapse": true,
			//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
			//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
			//"iDisplayLength": 50
			select: {
				style: 'multi'
			}
		});*/
        /****request edit button click code starts here****/
        $("#request_edit").on("click",function(){
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().prev().css("display","none");
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().css("display","block"); 
           
           // button set visible here 
           $("#update_request,#reset_request,#cancel_request").css("display","inline-block");

        });
		/****requester edit button click code starts here****/
        $("#requester_edit").on("click",function(){

            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().prev().css("display","none");
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().css("display","block"); 
         // button  set visible here
         $("#update_requester,#reset_requester,#cancel_requester").css("display","inline-block");

        });
        
        /***cancel request*******/
        $("#cancel_request").on("click",function(){
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().prev().css("display","block");
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().css("display","none"); 
            // button  set hidden here
         $("#update_request,#reset_request,#cancel_request").css("display","none");
        });
        /***cancel requester*******/
        $("#cancel_requester").on("click",function(){
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().prev().css("display","block");
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().css("display","none"); 
            // button  set hidden here
         $("#update_requester,#reset_requester,#cancel_requester").css("display","none");
        });
	});
	
	CKEDITOR.replace( 'editor', {
		plugins: 'wysiwygarea,sourcearea,basicstyles,toolbar,undo',
		on: {
			instanceReady: function() {
				// Show textarea for dev purposes.
				//this.element.show();
			},
			change: function() {
				// Sync textarea.
				this.updateElement();    
				// Fire keyup on <textarea> here?
			}
		}
	});
	
	
</script>
</body>
</html>


