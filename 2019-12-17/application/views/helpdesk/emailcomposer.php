<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" />

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
<!--<link href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css" />-->

<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.5.0/summernote.css" />
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.5.0/summernote-bs3.css" />
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/autocomple/easy-autocomplete.css'; ?>" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  

<style>
.mand{color:red;}
.header-r{background-color:grey; }

.eac-square input {
  background-image: url("images/icon_search.png");
  background-repeat: no-repeat;
  background-position: right 10px center;
}
</style>
</head>
<body>
	
	<div class="col-md-12">
	  <div class="box box-primary">
		<div class="box-header with-border"><h4 class="pull-left"><Span style="font: bold 13px Arial, Verdana, Helvetica, sans-serif!important;"><?php echo ($request_type=='REPLY') ? "Mail to Requester":"Forward Request"; ?></span></h4> <h5 class="pull-right"><span class="mand">*</span> Mandatory Field</h5></div>
		
		
		<!-- /.box-header -->
		<div class="box-body">
		<br><br>
		<?php if($this->session->flashdata('msg')): ?>
		<div class="alert alert-success alert-dismissable">
		 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		 <strong>Success!</strong>
		 <?php echo $this->session->flashdata('msg'); ?>
		</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('error_msg')): ?>
		<div class="alert alert-danger alert-dismissable">
		 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		 <strong>Error!</strong>
		 <?php echo $this->session->flashdata('error_msg'); ?>
		</div>
		<?php endif; ?>
		<form method="post" action="<?php echo base_url() ?>/helpdesk/sendRequest" enctype="multipart/form-data">
		  <input type="hidden" name="request_id" id="request_id" value="<?php echo $request_details->user_request_id; ?>" />
		  <input type="hidden" name="request_type" id="request_type" value="<?php echo $request_type; ?>" />
		  <div class="form-group ui-widget">
			<span class="mand">*</span>To:<input class="form-control" name="suggest" placeholder="To:" type="text" id="suggest" value="<?php echo ($request_type=='REPLY')? rtrim($emails,","):''; ?>" />
			<ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all" role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1; top: 20px; left: 0px; display: none; width: 167px;" id="suggestion">
				<li class="ui-menu-item" role="menuitem"><a class="ui-corner-all" tabindex="-1">ask</a></li>
			</ul>
		  </div>
		  <div class="form-group">
			<span class="mand"></span>cc:<input type="text" class="form-control" value="" id="cc" name="cc" placeholder="cc:" />
			<ul class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all" role="listbox" aria-activedescendant="ui-active-menuitem" style="z-index: 1; top: 20px; left: 0px; display: none; width: 167px;" id="suggestion1">
				<li class="ui-menu-item" role="menuitem"><a class="ui-corner-all" tabindex="-1">ask</a></li>
			</ul>
		  </div>
		  <div class="form-group">
			<span class="mand">*</span>Subject:<input class="form-control" value="<?php echo ($request_type=='FORWARD')?"Fwd:":"Re:";echo$request_details->request_subject; ?>" name="subject" placeholder="Subject:" />
		  </div>
		    <div class="form-group">
				<span class="mand">*</span>Body : 
				<textarea id="summernote" name="summernote" class="form-control" style="height: 300px">
				<?php  $duedate = date('M d,Y h:i:s A',strtotime('+2 hours',strtotime($request_details->request_created_dt))); ?>
				    <ul style="list-style-type:none;padding-left:10px;">
				       <?php if($request_type=='FORWARD'): ?>
						<li>		
						   <?php  echo "Requester:". $request_details->requester_name; ?>	
						</li>
						<li><?php  echo "Due by time:".$duedate; ?></li>
					   <?php endif; ?>	
						<li><?php $category = get_record_by_id_code('category',$request_details->category_id);echo "Category:". $category->category_name;  ?></li>
						
						<li><?php  echo "Description:". $request_details->request_desc; ?></li>
                     						  
				    </ul> 
				</textarea>
				<!--<h1><u>Heading Of Message</u></h1>
				  <h4>Subheading</h4>
				  <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain
					was born and I will give you a complete account of the system, and expound the actual teachings
					of the great explorer of the truth, the master-builder of human happiness. No one rejects,
					dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know
					how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again
					is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,
					but because occasionally circumstances occur in which toil and pain can procure him some great
					pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,
					except to obtain some advantage from it? But who has any right to find fault with a man who
					chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that
					produces no resultant pleasure? On the other hand, we denounce with righteous indignation and
					dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so
					blinded by desire, that they cannot foresee</p>
				  <ul>
					<li>List item one</li>
					<li>List item two</li>
					<li>List item three</li>
					<li>List item four</li>
				  </ul>
				  <p>Thank you,</p>
				  <p>John Doe</p>-->
				<?php //echo "<pre>";print_r($request_details);die; ?>
		    </div>
		  <?php if($ticket_status): ?>
		    <div class="form-group col-md-6">
				<span class="mand">*</span>Status
				<select class="form-control col-md-6" name="task_status" id="task_status">
					<option value="wip">WIP</option> 
					<option value="open">Opened</option> 
					<option value="close">Closed</option> 
				</select>
			</div><br/>
		  <?php endif; ?>
			<div class="form-group col-md-6">
				<input  type="file" name="request_attach[]" class="form-control select2-offscreen multi" placeholder="attachment" />
			</div>
		</div>
		<!-- /.box-body -->
		<div class="box-footer col-md-12 text-center">
		    <div class="pull-right">
			
		    </div>
		</div>
		<div class="col-md-12 text-center"> 
			<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send </button>
			<button type="submit" class="btn btn-primary" onclick="window.close();"><i class="fa fa-trash-o"></i> Discard </button>
			<br><br>
		</div>
		
		</form>	
		<!-- /.box-footer -->
	  </div>
	  <!-- /. box -->
	</div>
	
</body>
 
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.5.0/summernote.min.js" ></script>
<script src="<?php echo base_url() ?>assets/js/jQuery.MultiFile.min.js" type="text/javascript" language="javascript"></script>
 <!--<script src="<?php //echo base_url().'assets/autocomple/jquery.easy-autocomplete.js'; ?>" ></script>-->
<script src="<?php echo base_url().'assets/autocomple/nicEdit.js'; ?>" ></script>
<script>
var availableTags = [];
function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
function auto_complete(id)
{
	$( "#"+id)
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
	
}

$(document).ready(function(){
	var searchphrase;
	if($("#suggest").val()!=""){
	    searchphrase = 	$("#suggest").val();
	}
    if($("#cc").val()!=""){
	    searchphrase = 	$("#cc").val();
	}
    
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "<?php echo base_url().'helpdesk/getEmail/filter'; ?>",
		dataType: "json",
		data: "{'phrase':'" + searchphrase + "'}",
		success: function(data) {
		 
		 for(var i = 0;i<data.length;i++){
			 availableTags[i]= data[i].name;
		 }
		},
		error: function(result) {
		alert("Error");
		}
	});
	nicEditors.allTextAreas();
	/*$('#summernote').summernote({
	   height: 150,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }
	});*/
	auto_complete("suggest");
	auto_complete("cc");
    
	/*var options = {
	  url: function(phrase) {
		return "<?php echo base_url().'helpdesk/getEmail/filter'; ?>";
	  },
	  getValue: function(element) {
		return element.name;
	  },
	  ajaxSettings: {
		dataType: "json",
		method: "POST",
		data: {
		  dataType: "json"
		}
	  },
	  preparePostData: function(data) {
		data.phrase = $("#suggest").val();
		return data;
	  },
	  requestDelay: 400
	};
	$("#suggest").easyAutocomplete(options);
	*/
    /*$("#suggest").autocomplete({
		 source: function( request, response ) {
			$.ajax({
			  url: "<?php echo base_url().'helpdesk/getEmail/filter'; ?>",
			  dataType: "jsonp",
			  data: {
				q: request.term
			  },
			  success: function( data ) {
				console.log(data)
				response( data[1] );
			  }
			});
		  }
    });*/
	/*$("#suggest").autocomplete({
		suggestURL = "<?php echo base_url().'helpdesk/getEmail/filter'; ?>?q="+qrys;
		$.ajax({
			method: 'GET',
			url: suggestURL
		}).success(function(data){
			console.log(data);
			source: data;
		});
    });*/
	/*$("#suggest").autocomplete({
        delay: 100,
        source: function (request, response) {
            // Suggest URL
            var suggestURL = "<?php echo base_url().'helpdesk/getEmail/filter' ?>?q=%QUERY";
            suggestURL = suggestURL.replace('%QUERY', request.term);
            
            // JSONP Request
            $.ajax({
                method: 'GET',
                dataType: 'jsonp',
                //jsonpCallback: 'jsonCallback',
                url: suggestURL
            })
            .success(function(data){
                response(data[1]);
            });
        }
    });*/
	
});
</script>
</html>

