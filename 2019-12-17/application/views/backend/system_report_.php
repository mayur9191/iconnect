<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	     table tbody tr td:nth-child(3) a.description .tooltiptext {
			display: none;
			width: 430px;
			background-color:#ccc
			color:black;
			text-align: left;
			border-radius: 6px;
			padding: 5px 0;
			position: absolute;
			z-index: 1;
			border:1px solid black;
			
			
		}
		table tbody tr td:nth-child(3) a.description:hover .tooltiptext {
			display: block;
			color:black;
			padding-left:12px;
			word-wrap: break-word;
		}
		table tbody tr td:nth-child(3) a.description{
			color:#333;
		}
		table tbody tr td:nth-child(3) a.description:hover{
			color:#337ab7;
		}
		span.tooltiptext ul li{
			list-style-type:none;
		} 
		.square {
		float: left;
		width: 10px;
		height: 10px;
		margin: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		}

		.Normal {
		  background: #006600;
		}

		.Medium {
		  background: #ff6600;
		}

		.High {
		  background: #ff0000;
		}
		#dynamic-table{
			width:100% !important;
			font-size:12px !important;
		}
	</style>
</head>
<body>

	<div class="tab-content">
		<div class="tab-pane fade in active" id="tab2success">
			<div class="row inbox">
				<div class="container-fluid col-md-12" style="background-color: white;">
			<div class="page-header"></div>
			<h4>Technician assigned request</h4>
			<form name="tech_report" id="tech_report" onsubmit="return checkReport();">
				<table class="table">
					<tr>
						<td>Technician : </td>
						<td>
							<select name="techid" >
								<option value="">Select Technician</option>
								<?php 
									foreach($technician_list as $techRecord){
										echo '<option value="'.$techRecord->tech_code.'_'.$techRecord->technician_id.'">'.$techRecord->tech_name.'</value>';
									}
								?>
							</select>
						</td>
						<td>From Date : </td>
						<td>
							<input type="date" name="from_date" />
						</td>
						<td>To Date : </td>
						<td>
							<input type="date" name="to_date" />
						</td>
						
						<td>
							<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
						</td>
					</tr>
				</table>
			</form>
			
		</div> <!-- ./container -->
		
		</div><!--/.col-->		
	</div>
</div>
</body>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script>

function checkReport(){
	var form =$("#tech_report");
	$.ajax({
        type:"POST",
        url:'<?php echo base_url(); ?>helpdesk/get_report',
        data:form.serialize(),
        success: function(response){
			if(response === 1){
				//load chech.php file  
			}  else {
				//show error
			}
        }
    });
	return false;
}

var request_idaa = 0;
/* function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coor = "X coords: " + x + ", Y coords: " + y;
	$("#printTxt").css({'top':y,'left':x,'display':'block'}).show(); 
}
 */
function get_technician(group_id,tech_id)
{
	        $.ajax({
				url : "<?php echo base_url() ?>"+ "helpdesk/get_technician",
				type:"POST",
				data: {group_id:group_id},
				success:function(response){
					var str = '<option value=""> --- Choose --- </option>';
					var obj  =  JSON.parse(response);
					var selectop = '';
					$(obj).each(function(index,value){
						var sel = (tech_id == obj[index].tech_id)? 'selected':'';
						str += '<option '+sel+' id="'+obj[index].tech_id+'" value="'+obj[index].tech_id+'">'+obj[index].tech_name+'</option>';	
					});
					
					$("#technicians").html(str);
					//document.getElementById(tech_id).selected = "true";
					//$("#tech_id").prop("selected",true);
				},
				error:function(response){
					alert("error occured"+response)
				}
			});
	
}

$(document).ready(function(){
	
	//reassing technician 
	/*$("#button1id").on("click",function(){
		requtVal = false;
		var group_id = $("#groups").val();
		var technician_id = $("#technicians").val();
		console.log('reassing groupId = '+ group_id + ' and technician id = '+technician_id);
		if(group_id > 0){
			requtVal = true;
		}else{
			alert('Please select the group');
			requtVal = false;
		}
		
		if(technician_id > 0){
			requtVal = true;
		}else{
			alert('Please select the technician_id');
			requtVal = false;
		}
			
		if(requtVal){
			$.ajax({
				url: "<?php echo base_url() ?>/helpdesk/reassign_technician",
				type: "POST",
				data:{group_id:group_id,technician_id:technician_id},
				success:function(resp){
					console.log(resp);
				},error:function(resp){
					alert("error occured please try again later");
					console.log(resp);
				}
			});
		}
	});*/
	
	//reassing technician 
	$("#reassign_task").validate({
            rules: {
                groups: { required: true },
				technicians : { required : true }
            },
            messages: {
				groups: { required : "Please select group" },
                technician : { required : "Please select technician" }
            },
            submitHandler: function (form) {
				var group_id = $("#groups").val();
				var technician_id = $("#technicians").val();
				console.log('reassing groupId = '+ group_id + ' and technician id = '+technician_id);
				$.ajax({
					url: "<?php echo base_url() ?>/helpdesk/reassign_technician",
					type: "POST",
					data:{group_id:group_id,technician_id:technician_id,request_id:request_idaa},
					success:function(resp){
						console.log(resp);
						console.log('request_id '+request_idaa);
						if(resp > 0 && resp !=2){
							location.reload();
						}else if(resp == 2){
							alert("You can not re-assign to same technician");
						}else{
							alert("error occured please try again later");
						}
					},error:function(resp){
						alert("error occured please try again later");
						console.log(resp);
					}
				});
                return false;
            }
        });

	
	
	
	
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": [null, null,null,null, null,null,null, null,null,null],
		"aaSorting": [],
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
	//Handles menu drop down
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
		
    });
	$('#dynamic-table').on('click',' tbody >tr>td>.dropdown>a.dropdown-toggle.ab',function(e){
		//alert(($("#printTxt").position().left));
		
		var positionX;
		var positionY;
		var group_id = $(this).attr('data-gid');
		var tech_id  = $(this).attr('data-tid');
		request_idaa = $(this).attr('data-rid');
		positionX = e.clientX-45 + 'px',
		positionY = $(this).offset().top-250+'px';
		$("#groups").val('');
		$("#groups").val(group_id);
		//alert($(this).offset().top);
		//$("#request_gp").css({'top':positionY,'left':'75%','display':'block'}).show();
		//$("#request_gp").css({'top':e.clientY,'left':e.clientX,'display':'block'}).show();
		$("#printTxt").css({'top':positionY,'left':positionX,'display':'block'});
		// code to select technician
		get_technician(group_id,tech_id);
		$("#request_gp").show();
		
		
		///code to select group 
		/*
		$("#groups option").each(function(index){
			if(index == group_id){
				$(this).attr("selected","selected");
				
			}
			else{
				$(this).removeAttr("selected");
			}
			
				
			
		});*/
		
	});
	
	
	 // on menu selection
	 $( ".dropdown-menu" ).draggable();
	 
	 //on group selection
	$("#groups").on("change",function(){
		var group_id = $(this).find("option:selected").val();
		if(group_id == "")
		{
			alert("please select the correct group");
			
		}
		else
		{
			get_technician(group_id,tech_id =0);
		}
		
	});
	
	$(".close.pop").on("click",function(){
		$("#request_gp").hide();
	});
	$(".btn.btn-default.pop").on("click",function(){
		$("#request_gp").hide();
	});
	
});
</script>