<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />

   <?php include APPPATH . "views/includs/hedder_code.php" ?>
<style>
body{margin-top:20px;
background:#fff;
}


.inbox .inbox-menu ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .inbox-menu ul li {
    height: 30px;
    padding: 5px 15px;
    position: relative
}

.inbox .inbox-menu ul li:hover,
.inbox .inbox-menu ul li.active {
    background: #e4e5e6
}

.inbox .inbox-menu ul li.title {
    margin: 20px 0 -5px 0;
    text-transform: uppercase;
    font-size: 10px;
    color: #d1d4d7
}

.inbox .inbox-menu ul li.title:hover {
    background: 0 0
}

.inbox .inbox-menu ul li a {
    display: block;
    width: 100%;
    text-decoration: none;
    color: #3d3f42
}

.inbox .inbox-menu ul li a i {
    margin-right: 10px
}

.inbox .inbox-menu ul li a .label {
    position: absolute;
    top: 10px;
    right: 15px;
    display: block;
    min-width: 14px;
    height: 14px;
    padding: 2px
}

.inbox ul.messages-list {
    list-style: none;
    margin: 15px -15px 0 -15px;
    padding: 15px 15px 0 15px;
    border-top: 1px solid #d1d4d7
}

.inbox ul.messages-list li {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    cursor: pointer;
    margin-bottom: 10px;
    padding: 10px
}

.inbox ul.messages-list li a {
    color: #3d3f42
}

.inbox ul.messages-list li a:hover {
    text-decoration: none
}

.inbox ul.messages-list li.unread .header,
.inbox ul.messages-list li.unread .title {
    font-weight: 700
}

.inbox ul.messages-list li:hover {
    background: #e4e5e6;
    border: 1px solid #d1d4d7;
    padding: 9px
}

.inbox ul.messages-list li:hover .action {
    color: #d1d4d7
}

.inbox ul.messages-list li .header {
    margin: 0 0 5px 0
}

.inbox ul.messages-list li .header .from {
    width: 49.9%;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .header .date {
    width: 50%;
    text-align: right;
    float: right
}

.inbox ul.messages-list li .title {
    margin: 0 0 5px 0;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .description {
    font-size: 12px;
    padding-left: 29px
}

.inbox ul.messages-list li .action {
    display: inline-block;
    width: 16px;
    text-align: center;
    margin-right: 10px;
    color: #d1d4d7
}

.inbox ul.messages-list li .action .fa-check-square-o {
    margin: 0 -1px 0 1px
}

.inbox ul.messages-list li .action .fa-square {
    float: left;
    margin-top: -16px;
    margin-left: 4px;
    font-size: 11px;
    color: #fff
}

.inbox ul.messages-list li .action .fa-star.bg {
    float: left;
    margin-top: -16px;
    margin-left: 3px;
    font-size: 12px;
    color: #fff
}

.inbox .message .message-title {
    margin-top: 30px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px
}

.inbox .message .header {
    margin: 20px 0 30px 0;
    padding: 10px 0 10px 0;
    border-top: 1px solid #d1d4d7;
    border-bottom: 1px solid #d1d4d7
}

.inbox .message .header .avatar {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    height: 34px;
    width: 34px;
    float: left;
    margin-right: 10px
}

.inbox .message .header i {
    margin-top: 1px
}

.inbox .message .header .from {
    display: inline-block;
    width: 50%;
    font-size: 12px;
    margin-top: -2px;
    color: #d1d4d7
}

.inbox .message .header .from span {
    display: block;
    font-size: 14px;
    font-weight: 700;
    color: #3d3f42
}

.inbox .message .header .date {
    display: inline-block;
    width: 29%;
    text-align: right;
    float: right;
    font-size: 12px;
    margin-top: 18px
}

.inbox .message .attachments {
    border-top: 3px solid #e4e5e6;
    border-bottom: 3px solid #e4e5e6;
    padding: 10px 0;
    margin-bottom: 20px;
    font-size: 12px
}

.inbox .message .attachments ul {
    list-style: none;
    margin: 0 0 0 -40px
}

.inbox .message .attachments ul li {
    margin: 10px 0
}

.inbox .message .attachments ul li .label {
    padding: 2px 4px
}

.inbox .message .attachments ul li span.quickMenu {
    float: right;
    text-align: right
}

.inbox .message .attachments ul li span.quickMenu .fa {
    padding: 5px 0 5px 25px;
    font-size: 14px;
    margin: -2px 0 0 5px;
    color: #d1d4d7
}

.inbox .contacts ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .contacts ul li {
    height: 30px;
    padding: 5px 15px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis!important;
    position: relative;
    cursor: pointer
}

.inbox .contacts ul li .label {
    display: inline-block;
    width: 6px;
    height: 6px;
    padding: 0;
    margin: 0 5px 2px 0
}

.inbox .contacts ul li:hover {
    background: #e4e5e6
}


.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}


.panel-success>.panel-heading {background-image:linear-gradient(to bottom,#c9da2c 0,#cbdb2b 100%)}
.with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {color:#ffffff}
.with-nav-tabs.panel-success .nav-tabs > li > a:hover{background-color:#ffffff; color:#1c6b94;}
.backtab {background-color:#1c6b94; color:#ffffff;}
.backtab:hover {background-color:#1c6b94; color:#ffffff;}
.mand{color:red;}
.dropdown-thin {
    height: 25px;
    padding-top: 2px;
}

.dep-btn {
	background-color: #c9da2b;
    color: #3c763d;
    float: left;
    margin-bottom: 10px;
    margin-top: 10px;
    margin-right: 10px;
}
</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<br><br><br>
<?php $approvalData = getMaterialApproval($passingData->material_master_id); ?>
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
<script src="https://select2.github.io/dist/js/select2.full.js"></script>
<div class="container">
	
  

<!-- Modal for Contact US -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:green;">Contacts <i  class="fa fa-envelope" style="font-size:24px;color:green;"></i></h4>
      </div>
	  
	  
	  
      <div class="modal-body" style="height:410px;overflow-y:scroll;">
		<table class="col-md-12">
			<tr>
				<td colspan="2"><h4>ITFM</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Rahul Survase</span><br/>
						<span>Rahul.Survase@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>Shaijad Kureshi</span><br/>
						<span>Shaijad.Kureshi@skeiron.com</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>HR</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Rudramahi Wadje</span><br/>
						<span>Rudramahi.Wadje@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>Amish Bhatt</span><br/>
						<span>Amish.Bhatt@skeiron.com</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Server Support</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Sunil Nale</span><br/>
						<span>Sunil.Nale@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Nav Support</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Shekhar Deshmukh</span><br/>
						<span>Shekhar.Deshmukh@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>Ajay Yadav</span><br/>
						<span>Ajay.Yadav@skeiron.com</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Network Support</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Johny Anthony</span><br/>
						<span>Johny.Anthony@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>New Purchase</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Nelson Komathan</span><br/>
						<span>Nelson.Komathan@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Admin</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Asha Bhandwalkar</span><br/>
						<span>Asha.Bhandwalkar@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
		</table>
      </div>
      
    </div>

  </div>
</div>
	<div class="row">
		<div class="col-md-12">
            <div class="panel with-nav-tabs panel-success">
                <div class="panel-heading">
				     
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1success" data-toggle="tab" id="tab1">Skeiron Help Desk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                            <!--<li><a href="#tab2success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>-->
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1success">
							<div class="row inbox">
								<div class="col-md-3">
									<div class="panel panel-default">
									<span class="btn backtab btn-block">Request Summary </span>
										<div class="panel-body inbox-menu">	
											
											<ul>
												<li>
													<a href="#"> In process <span style="background-color:yellow;color:#1c6b94;" class="label label-primary" >4</span></a>
												</li>
												
												<li>
													<a href="#"> On hold <span style="color:#1c6b94;" class="label label-warning">4</span></a>
												</li>
												
												<li>
													<a href="#"> Completed <span  class="label label-success">4</span></a>
												</li>
												
												<li>
													<a href="#"> Total <span  class="label label-primary">12</span></a>
												</li>
												
											</ul>
										</div>	
									</div>
									
									<div class="panel panel-default">
									<a href="#" class="btn backtab btn-block"> Important Links </a>
										<div class="panel-body contacts">
											
											<ul>
												<!--<li><a href="http://www.skeiron.com" target="_blank" title="Skeiron.com"> <span class="label label-danger"></span>Skeiron.com </a></li>-->
												<li><a href="https://apps.seshipping.net/tms" target="_blank" title="Transport Management System"><span class="label label-danger"></span>Transport Management System </a></li>
												<li><a href="http://outlook.office365.com/" target="_blank" title="Skeiron Webmail"><span class="label label-danger"></span>Skeiron Webmail</a></li>
											</ul>
										
										</div>
									
									</div>			
									
								</div><!--/.col-->
								
								<div class="col-md-9">
									<div class="panel panel-default">
										<span class="btn backtab btn-block">Raise Request</span>
										<div class="panel-body message">
											<p class="text-center">
											
												<button onclick="window.location='<?php echo base_url().'helpdesk/new_request' ?>'" type = "button" class="btn label-success dep-btn" id="tab21">Admin</button>
												
												
												<button onclick="window.location='<?php echo base_url().'helpdesk/new_request' ?>'" type = "button" class="btn label-success dep-btn" id="tab22">Corporate Communication</button>
												
												<button onclick="window.location='<?php echo base_url().'helpdesk/new_request' ?>'" type = "button" class="btn label-success dep-btn" id="tab23">Finance</button>
												
												<button onclick="window.location='<?php echo base_url().'helpdesk/new_request' ?>'" type = "button" class="btn label-success dep-btn" id="tab24">HR</button>
												
												<button onclick="window.location='<?php echo base_url().'helpdesk/new_request' ?>'" type = "button" class="btn label-success dep-btn" id="tab24">IT</button>
												
												<button onclick="window.location='<?php echo base_url().'helpdesk/new_request' ?>'" type = "button" class="btn label-success dep-btn" id="tab26">Legal</button>
												
												
											
											</p>
										</div>	
									</div>
									<div class="panel panel-default">
										<span class="btn backtab btn-block">Annoucements
										  <div  class="dropdown pull-right">

											<button class="btn btn-default dropdown-toggle dropdown-thin" type="button" data-toggle="dropdown">Department
                                          <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                              <li><a href="#">Admin</a></li>
                                              <li><a href="#">Corporate Communication</a></li>
                                              <li><a href="#">Finance</a></li>
											  <li><a href="#">HR</a></li>
											  <li><a href="#">IT</a></li>
											  <li><a href="#">Legal</a></li>
                                            </ul>
										</div>
												
										</span>
										<div class="panel-body message">
											<ul>
												<li><a href="#">No Annoucements Yet </a></li>
												<!--<li><a href="#">Skeiron webmail</a></li>
												<li><a href="#">Skeiron.com</a></li>-->
											</ul>
										</div>	
									</div>
									<div class="panel panel-default">
									<span class="btn backtab btn-block">Guideline
									
									<div  class="dropdown pull-right">

											<button class="btn btn-default dropdown-toggle dropdown-thin" type="button" data-toggle="dropdown">Department
                                          <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                              <li><a href="#">Admin</a></li>
                                              <li><a href="#">Corporate Communication</a></li>
                                              <li><a href="#">Finance</a></li>
											  <li><a href="#">HR</a></li>
											  <li><a href="#">IT</a></li>
											  <li><a href="#">Legal</a></li>
                                            </ul>
										</div>
									</span>
										<div class="panel-body message">
											<p class="text-left">Regularly scan your computer with certified, up-to-date anti-virus / anti-spyware software.</p>
											<p class="text-left"> Your password are most common way to prove your identity when using websites, email accounts and your computer itself(via User Accounts).</p>
											<p class="text-left">The use of strong password is therefore essential in order to protect your security and identity.</p>
										</div>	
									</div>	
								</div><!--/.col-->		
							</div>
						</div>
						
						
						<!-- second div goes here -->
                        
                    </div>
                </div>
            </div>
        </div>
	</div>


</div>

	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>

<!-- editor -->
 
   <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/editr/jquery-te-1.4.0.css">	
<script type="text/javascript" src="<?php echo base_url() ?>assets/editr/jquery-te-1.4.0.min.js" charset="utf-8"></script>
<!-- editor -->

<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
  
<script>
	var trVal = 1;
	$(document).ready(function(){
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
		
		$("#tab1").click(function(){
			
			$(this).attr('aria-expanded',true);
			$(this).find('li').addClass('active');
			
			//$("#tab2").attr('aria-expanded',false);
			//$("#tab2").find('li').removeClass('active');
		});
		$('.jqte-test').jqte();
		
		// settings of status
		//var jqteStatus = true;
		/*$(".status").click(function(){
			jqteStatus = jqteStatus ? false : true;
			$('.jqte-test').jqte({"status" : jqteStatus})
		});*/
		
		/*$("#tab21").click(function(){
			$("#tab1").attr('aria-expanded',false);
			$("#tab1").parent('li').removeClass('active');
			
			$("#tab2").attr('aria-expanded',true);
			$("#tab2").parent('li').addClass('active');
			
			$("#tab2success").addClass('active in');
			$("#tab1success").removeClass('active in');
		});*/
		$("#btnSubmit").click(function(){
			$("#errMsg").text('');
			return true;
		});
		 $.validator.addMethod("regex", function (value, element, regexpr) {
			return regexpr.test(value);
		}, "Please enter a valid name.");

		  $.validator.setDefaults({
			 
			errorPlacement: function(error, element) {
				error.appendTo('#errMsg');
				$("#errMsg").append('<br>');
			}
		});
		
		$("#service_request_form").validate({
		 
			rules: {
				request_description:{
					required:true,
					regex: /^[A-Za-z]+$/
				},
				request_type: {required:true},
				priority: {required:true},
				request_name:{required:true},
				request_location:{required:true},
				category: {required:true},
				sub_category: {required:true},
				item: {required:true},
				request_subject: {required:true}
				
				
			},
			 
			messages: {
				
				"request_type": {
					required: "Please select one request type"                
				},
				"priority": {
					required: "Please select one priority item"                
				},
				"request_name": {
					required: "Please enter requester name"                
				},
				"request_location": {
					required: "Please enter requester location"                
				},
				"category": {
					required: "Please select category"                
				},
				"sub_category": {
					required: "Please select sub-category"                
				},
				"item": {
					required: "Please select item"                
				},
				"request_subject": {
					required: "Please enter subject"                
				},
				"request_description": {
					required: "Please enter the Description"                
				}
				
				
			},
			
			
			submitHandler: function (form) { 
				
				alert('valid form submitted'); 
				return false; 
			}
			
			
			
		});
		
	});
	function enableTab(tabid)
	{
		$("#tab2success").addClass('active in');
	    $("#tab1success").removeClass('active in');
		
	}
	/*CKEDITOR.replace( 'editor', {
		//removed "sourcearea"
		plugins: 'wysiwygarea,basicstyles,toolbar,undo',
		on: {
			instanceReady: function() {
				// Show textarea for dev purposes.
				//this.element.show();
			},
			change: function() {
				// Sync textarea.
				this.updateElement();    
				// Fire keyup on <textarea> here?
			}
		}
	});*/
	
	
	 
	
	
</script>
<h4>Redirect Links</h4>
<a href="<?php echo base_url().'helpdesk/'; ?>">Home</a><br>
<a href="<?php echo base_url().'helpdesk/open_request'; ?>">open request</a><br>
<a href="<?php echo base_url().'helpdesk/admin'; ?>">admin</a><br>
<a href="<?php echo base_url().'helpdesk/open_request_details'; ?>">request details</a><br>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-sm pull-right" style="margin-right:18px;" data-toggle="modal" data-target="#myModal">Contact Us</button>
</body>
</html>
