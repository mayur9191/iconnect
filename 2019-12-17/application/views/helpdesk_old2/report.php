<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	 <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<br><br>
	
	<div class="container col-md-12" style="background-color: white;">
		<div class="page-header">
			<center>
				<!--<h3>Skeiron Group</h3>-->
				<img src="<?= base_url().'assets/images/logo/Logo.png' ?>" style="width: 15%;" />
				<h4></h4>
				<h4></h4>
			</center>
		</div>
		<h4>MATERIAL GATE PASS REPORT</h4>
		<div class="col-xs-12" style="font-size: 10px;font-family: inherit;">
			<a href="<?php echo base_url().'home/'; ?>" class="btn btn-md btn-info pull-right">Create Gateway Pass</a>
				
			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
				<thead>
					<tr style="font-weight:bold">
						<th>Sr No</th>
						<th>Date</th>
						<th>Skeiron Green Power PRIVATE LIMITED</th>
						<th>Aspen INFRASTRUCTURES PRIVATE LIMITED</th>
						<th>Location</th>
						<th>Sender name</th>
						<th>Source address</th>
						<th>Recipient name</th>
						<th>Destination address</th>
						<th>Purpose</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
				<?php $i=1; foreach($passingData as $reportRecord){ ?>
					<tr>
						<td><?= $reportRecord->top_number; ?></td>
						<td><?= date('d-m-Y',strtotime($reportRecord->top_date)); ?></td>
						<td><?php echo ($reportRecord->company_check1 == 1)? 'Checked':''; ?></td>
						<td><?php echo ($reportRecord->company_check2 == 1)? 'Checked':''; ?></td>
						<td><?= $reportRecord->location; ?></td>
						<td><?= $reportRecord->sender_name_contact; ?></td>
						<td><?= $reportRecord->source_address; ?></td>
						<td><?= $reportRecord->recipient_name_contact; ?></td>
						<td><?= $reportRecord->destination_address; ?></td>
						<td><?= $reportRecord->purpose; ?></td>
						<td>
						<?php 
						$approvalData = getMaterialApproval($reportRecord->material_master_id);
						if($approvalData[0]->digital_signature=='approve' && $approvalData[1]->digital_signature=='approve' && $approvalData[2]->digital_signature=='approve' && $approvalData[3]->digital_signature=='approve'){ ?>
						
							<a href="<?php echo base_url().'home/material_gateway_form/'.$reportRecord->material_master_id; ?>" target="_blank" class="btn btn-md btn-info">Print Pass</a>
						<?php }else{ ?>
							<a href="<?php echo base_url().'home/approval_process/'.$reportRecord->material_master_id; ?>" target="_blank" class="btn btn-md btn-info">View & approve</a>
						<?php } ?>
						
						</td>
					</tr>
				<?php $i++; }  ?>
			</tbody>
			</table>
		</div>
	</div> <!-- ./container -->
</body>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": [null, null,null,null, null,null,null, null,null,null],
		"aaSorting": [],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
});
</script>