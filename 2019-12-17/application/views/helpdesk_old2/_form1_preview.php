<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
   <?php include APPPATH . "views/includs/hedder_code.php" ?>
<style>
.alg-center{
	text-align:center;
}
input[type="checkbox"]{ width: 20px; height: 20px;}
</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<?php $approvalData = getMaterialApproval($passingData->material_master_id); ?>
<br><br><br>
 <div class="container" style="border: 2px groove;">
	<div class="row pull-right">
		<table class="table">
			<tbody>
			  <tr>
				<th>No. : </th>
				<td><?php echo ($passingData->top_number !='')? $passingData->top_number:''; ?></td>
				<th>Date : </th>
				<td><?php echo ($passingData->top_date !='')? date('d/m/Y',strtotime($passingData->top_date)):''; ?></td>
			  </tr>
			</tbody>
		</table>
	</div>
    
   <br><br><br>
   <center><h3>MATERIAL GATE PASS </h3></center>
   <br>
   
	<div class="col-lg-12 ">
	<div class="row">
		<form>
			<div class="col-lg-12">
				<div class="row">
				<table class="table" >
					<tbody id="adrow">
						<tr>
							<td><b>Skeiron Green Power PRIVATE LIMITED</b> <input type="checkbox" <?php echo ($passingData->company_check1 == 1)? 'checked="checked" disabled="disabled"':'disabled="disabled"'; ?> /></td>
							<td><b>Aspen INFRASTRUCTURES PRIVATE LIMITED</b> <input type="checkbox" <?php echo ($passingData->company_check2 == 1)? 'checked="checked" disabled="disabled"':'disabled="disabled"'; ?> /></td>
						</tr>
						<tr>
							<td colspan="2"><b>Location :</b> <?php echo ($passingData->location !='')? $passingData->location:''; ?> </td>
						</tr>
						<tr>
							<td colspan="2">The following material may please be allowed to go out of our office on date  <?php echo ($approvalData[3]->approved_date !='')? date('d/m/Y',strtotime($approvalData[3]->approved_date)):''; ?>.</td>
						</tr>
					</tbody>
				</table>
			</div>	
			
              <div class="row">
                
                  <table class="table" border="">
                    <thead>
                      <tr>
                        <th class="alg-center">S. No.</th>
                        <th class="alg-center">Item</th>
                        <th class="alg-center">Description</th>
                        <th class="alg-center">No</th>
                        <th class="alg-center">Unit</th>
                        <th class="alg-center">Returnable / Non-returnable</th>
                        <th class="alg-center">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="adrow">
					<?php $materialData = getMterialProduct($passingData->material_master_id);
						foreach($materialData as $materialRecord) { ?>
							<tr>
								<td><?php echo ($materialRecord->product_sl_no !='')? $materialRecord->product_sl_no:''; ?></td>
								<td><?php echo ($materialRecord->product_item !='')? $materialRecord->product_item:''; ?></td>
								<td><?php echo ($materialRecord->description !='')? $materialRecord->description:''; ?></td>
								<td><?php echo ($materialRecord->product_nob !='')? $materialRecord->product_nob:''; ?></td>
								<td><?php echo ($materialRecord->product_unit !='')? $materialRecord->product_unit:''; ?></td>
								<td><?php echo ($materialRecord->returnable_status !='')? $materialRecord->returnable_status:''; ?></td>
								<td><?php echo ($materialRecord->remark !='')? $materialRecord->remark:''; ?></td>
							</tr>
					<?php } ?>
                    </tbody>
                </table>
              </div>
			  
              <div class="row">
			   <table class="table">
					<tbody>
                      <tr>
                        <th>Sender Name & Contact No. : </th>
                        <td><?php echo ($passingData->sender_name_contact !='')? $passingData->sender_name_contact:''; ?></td>
                      </tr>
					  
					  <tr>
                        <th>Source Address : </th>
                        <td><?php echo ($passingData->source_address !='')? $passingData->source_address:''; ?></td>
                      </tr>
					  
					   <tr>
                        <th>Recipient Name & Contact No. : </th>
                        <td><?php echo ($passingData->recipient_name_contact !='')? $passingData->recipient_name_contact:''; ?></td>
                      </tr>
					  
					  <tr>
                        <th>Destination Address : </th>
                        <td><?php echo ($passingData->destination_address !='')? $passingData->destination_address:''; ?></td>
                      </tr>
					  
					   <tr>
                        <th>Purpose : </th>
                        <td><?php echo ($passingData->purpose !='')? $passingData->purpose:''; ?></td>
                      </tr>
					  
					</tbody>
				</table>
			  </div>
			  
              <div class="row">
                
                  <table class="table" border="">
                    <thead>
                      <tr>
                        <th></th>
                        <th class="alg-center">Prepared by</th>
                        <th class="alg-center">Checked by</th>
                        <th class="alg-center">Approved by</th>
                        <th class="alg-center">Approved by</th>
                      </tr>
                    </thead>
                    <tbody>
					
                      <tr>
                        <th>Department</th>
                        <td><?php echo ($approvalData[0]->department !='')? $approvalData[0]->department:''; ?></td>
                        <td><?php echo ($approvalData[1]->department !='')? $approvalData[1]->department:''; ?></td>
                        <td><?php echo ($approvalData[2]->department !='')? $approvalData[2]->department:''; ?></td>
                        <td><?php echo ($approvalData[3]->department !='')? $approvalData[3]->department:''; ?></td>
                      </tr>
                      <tr>
                        <th>Name</th>
                        <td><?php echo ($approvalData[0]->person_name !='')? $approvalData[0]->person_name:''; ?></td>
                        <td><?php echo ($approvalData[1]->person_name !='')? $approvalData[1]->person_name:''; ?></td>
                        <td><?php echo ($approvalData[2]->person_name !='')? $approvalData[2]->person_name:''; ?></td>
                        <td><?php echo ($approvalData[3]->person_name !='')? $approvalData[3]->person_name:''; ?></td>
                      </tr>
                       <tr>
                        <th>Designation</th>
                        <td><?php echo ($approvalData[0]->designation !='')? $approvalData[0]->designation:''; ?></td>
                        <td><?php echo ($approvalData[1]->designation !='')? $approvalData[1]->designation:''; ?></td>
                        <td><?php echo ($approvalData[2]->designation !='')? $approvalData[2]->designation:''; ?></td>
                        <td><?php echo ($approvalData[3]->designation !='')? $approvalData[3]->designation:''; ?></td>
                      </tr>
                      <tr>
                        <th>Signature</th>
						<td><?php echo ($approvalData[0]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
                        <td><?php echo ($approvalData[1]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
                        <td><?php echo ($approvalData[2]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
                        <td><?php echo ($approvalData[3]->digital_signature =='approve')? '<img src="'.base_url().'uploads/Signature_LOGO.png" style="width: 100px;" />':''; ?></td>
						
                        <!--<td><?php echo ($approvalData[0]->digital_signature =='approve')? 'Approve':''; ?></td>
                        <td><?php echo ($approvalData[1]->digital_signature =='approve')? 'Approve':''; ?></td>
                        <td><?php echo ($approvalData[2]->digital_signature =='approve')? 'Approve':''; ?></td>
                        <td><?php echo ($approvalData[3]->digital_signature =='approve')? 'Approve':''; ?></td>-->
                      </tr>
                      <tr>
                        <th>Date</th>
                        <td><?php echo ($approvalData[0]->approved_date !='')? date('d/m/Y',strtotime($approvalData[0]->approved_date)):''; ?></td>
                        <td><?php echo ($approvalData[1]->approved_date !='')? date('d/m/Y',strtotime($approvalData[1]->approved_date)):''; ?></td>
                        <td><?php echo ($approvalData[2]->approved_date !='')? date('d/m/Y',strtotime($approvalData[2]->approved_date)):''; ?></td>
                        <td><?php echo ($approvalData[3]->approved_date !='')? date('d/m/Y',strtotime($approvalData[3]->approved_date)):''; ?></td>
                      </tr>
					 
                    </tbody>
                </table>
              </div>
			  
            <div class="row">
              <div class="col-sm-12 form-group">
                <label>Remarks of Security : </label>
              </div>
            </div>
			
			<div class="row">
			   <table class="table">
					<tbody>
                      <tr>
                        <th>1. Material went out on date: </th>
                        <td>_________________________ Time: ______________ AM /PM</td>
                      </tr>
					  
					  <tr>
                        <th>2. by which vehicle :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					   <tr>
                        <th>3. Name of Recipient :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					  <tr>
                        <th>4.Contact Number :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					   <tr>
                        <th>5. Signature :</th>
                        <td>_____________________________________________</td>
                      </tr>
					  
					</tbody>
				</table>
				<br>
				<span class="pull-right" style="margin-right: 10%;"><b>Signature of Security Officer</b></span>
				<br>
				<br>
				
			</div>
			</div>
		</form> 
		</div>
	</div>
	</div>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
<script>
	var trVal = 1;
	$(document).ready(function(){
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
	});
	
	function addRow(){
		var rowHtm = '<tr id="tr_'+trVal+'"><td><input type="text" placeholder="S.No." class="form-control" value="1" ></td><td><input type="text" placeholder="Item" class="form-control"></td><td><input type="text" placeholder="Description" class="form-control"></td><td><input type="text" placeholder="No" class="form-control"></td><td><input type="text" placeholder="Unit" class="form-control"></td><td><input type="text" placeholder="Returnable / Non-returnable" class="form-control"></td><td><input type="text" placeholder="Remark" class="form-control"><button type="button" class="btn btn-sm btn-info" onclick="removeRow('+trVal+')">-</button></td></tr>';
		$("#adrow").append(rowHtm);
		trVal++;
		console.log($("#adrow").find('> tr').length);	
		var lengthSn = $("#adrow").find('> tr').length;
		var j = 1;
		for(var i=0;i<=lengthSn;i++){
			$("#adrow").find('> tr').eq(i).find('> td').find('> input').eq(0).val(j);
			j++;
		}
	}
	
	function removeRow(val){
		console.log('the remove val is '+val);
		$("#tr_"+val).remove();
		var lengthSn = $("#adrow").find('> tr').length;
		var j = 1;
		for(var i=0;i<=lengthSn;i++){
			$("#adrow").find('> tr').eq(i).find('> td').find('> input').eq(0).val(j);
			j++;
		}
	}
</script>
</body>
</html>
