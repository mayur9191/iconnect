<nav class="navbar navbar-default navbar-inverse navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
	<span class="pull-right" style="margin-top:4px; margin-left:4px;">Welcome <br><?= ucwords(substr($user_ldamp['name'], 0, 12)) ?></span>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
			
            <a class="navbar-brand" href="<?= base_url() ?>">
                <img alt="alternative text" src="<?= base_url() . 'assets/' ?>images/iconnect-logo.png"/>
            </a>
        </div>
        <div class=" animated fadeIn" id="bs-example-navbar-collapse-1">
            <!--        <div class="collapse navbar-collapse animated fadeIn" id="bs-example-navbar-collapse-1">-->
            <center>
                <ul class="nav navbar-nav animated fadeIn text16">
                    <!--<li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/survey-icon.png" alt="menu-icons" class="img-responsive" style="width: 30px;" />
							</span>
						</a>
					</li>
					
					<li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/1.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/2.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/3.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/4.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/5.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/6.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/7.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/8.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/9.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/10.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>-->
                </ul>
                <!--<ul class="col-md-5">
                    <li class="input-group">-->
<!--                        <div class="input-group-btn search-panel">-->
<!--                            <button type="button" class="btn btn-default btn-iconbg dropdown-toggle"-->
<!--                                    data-toggle="dropdown" style="margin: 0px; padding: 7px;">-->
<!--                                <span id="search_concept" class="glyphicon glyphicon-menu-hamburger"></span> <span-->
<!--                                    class="caret"></span>-->
<!--                            </button>-->
<!--                            <ul class="dropdown-menu" role="menu">-->
<!--                                <li><a href="#contains">Example 1</a></li>-->
<!--                                <li><a href="#its_equal">Example 2</a></li>-->
<!--                                <li><a href="#greather_than">Example 3</a></li>-->
<!--                                <li><a href="#less_than">Example 4</a></li>-->
<!--                                <li class="divider"></li>-->
<!--                                <li><a href="#all">All</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
                       <!-- <input type="hidden" name="search_param" value="all" id="search_param">
                        <input type="text" class="form-control controlform" name="x" placeholder="Search">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-iconbg" type="button" style="margin: 0px; padding: 10px;"><span
                            class="glyphicon glyphicon-search"></span></button>
                </span>
                    </li>
                </ul>-->
                <!--<ul class="nav navbar-nav animated fadeIn text16">
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/11.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/12.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/13.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/14.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                    <li><a href="#"><span> <img src="<?= base_url() . 'assets/' ?>images/icons/mini/15.png"
                                                alt="menu-icons" class="img-responsive"/></span></a></li>
                </ul>-->
            </center>
			<?php

			//    print_r($CI->user_indo);
			//echo $CI->user_indo[0]->u_image;

			if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
				$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
			} else {

				$dp_image = base_url() . 'assets/images/45.png';
			}
			?>
            <ul class="nav navbar-nav navbar-right nopadding" style="margin-right:0px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $dp_image; ?>" style="width: 30px;border-radius: 20px;"  alt="profile"
                             class="img-responsive"/></a>
                    <ul class="dropdown-menu animated flipInX" role="menu">

                        <?php

                        if ($logindata['role'] == 1) {


                            ?>
                            
                            <li>
                                <a href="#" data-toggle="modal" data-target="#profile_slide">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>

                                    Banner Slider
                                </a>
                            </li>
                            <?php
                        }
                        ?>
						 <li><a href="<?= base_url() . 'profile/edit' ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i> Edit Profile </a></li>

                        <!--                        <li><a href="#"><span class="glyphicon glyphicon-list-alt"></span> First Click</a></li>-->
                        <!--                        <li><a href="#"><span class="glyphicon glyphicon-road"></span> Second Click</a></li>-->
                        <!--                        <li><a href="#"><span class="glyphicon glyphicon-barcode"></span> Third Click</a></li>-->
                        <li class="divider"></li>
                        <li><a href="#">Welcome <?= ucwords($user_ldamp['name']) ?></a></li>
                        <li class="divider"></li>
                        <li><a href="<?= base_url() . 'profile/logout' ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<?php

if ($logindata['role'] == 1) {
    ?>

    <div id="profile_slide" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="post" enctype="multipart/form-data" class="bithdaypost"
                      action="<?= base_url() . 'profile/sliders' ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Profile Slide</h4>
                    </div>
                    <div class="modal-body">
                        <div id="wrapper" style="margin-top: 20px;">
                            <div id="image-holder" class="col-md-12 nopad"  style="display: flex; max-height:250px; ">
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <input class="fileUpload" name="image" class="" type="file"/>
                                <i><b>Note:</b> Upload image size in 520 x 200 </i>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-primary" type="submit" value="Upload Now"/>

                    </div>
                </form>
            </div>

        </div>
    </div>



    <?php
}
?>
