<div class="col-xs-3 col-md-3 asdf-icon" data-step="1" data-intro="Profile picture upload">

    <?php

    //    print_r($CI->user_indo);
    //echo $CI->user_indo[0]->u_image;

    if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
        $dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
    } else {

        $dp_image = base_url() . 'assets/images/45.png';
    }

    ?>
    <div class="col-md-12 nopad" data-toggle="modal" data-target="#update_dp" style="width: 100px;display: flex; ">
        <img src="<?= $dp_image; ?>" style="margin: 5px;margin: 5px;max-width: 80px;max-height: 80px" alt="client-user" clas="img-responsive"/>
    </div>
    <div class="clear"></div>





    <div id="update_dp" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="post" enctype="multipart/form-data" class="changedp" action="<?= base_url() . 'profile/dpchange' ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Chenge Prifile</h4>
                    </div>
                    <div class="modal-body">
                        <div id="wrapper" style="margin-top: 20px;">
                            <div id="image-holder" class="col-md-12 nopad" style="width: 250px;display: flex; ">
                                <img src="<?= $dp_image ?>" alt="client-user" clas="img-responsive"/>
                            </div>

                            <div class="form-group">
                                <input class="fileUpload" name="image" class="" type="file"/>
                            </div>
                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="Update Now"/>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<div class="col-xs-8 col-md-9" style="padding-left: 30px; padding-top: 10px; line-height: 20px;">
    <b class="clientuser-name"> <?= ucfirst($CI->hrdata_user_info[0]['FIRST_NAME'] ? $CI->hrdata_user_info[0]['FIRST_NAME'] :'') ?> </b>
<!--    <b class="clientuser-name1 dsp1">  </b> <br>-->
    <div class="clear"></div>
    <span class="clientuser-name2">
        <?= ucfirst($user_ldamp['description']) ?>
    </span>
</div>


<!--<div class="iconnect-ps ps11">-->
<!--    <a href="#">-->
<!--        <h5 class="iconnect-psh5">-->
<!--            <b class="ps-tag" style="padding-left: 8px;">Connect</b>-->
<!--                        <span-->
<!--                            class="glyphicon glyphicon-plus navdown-list22"></span>-->
<!--        </h5>-->
<!--    </a>-->
<!--</div>-->
<div class="clear"></div>


<div class="clear"></div>

<div class="sidebox community_update" >
    <div class="iconnect-ps">
        <a href="#">
            <h5 class="iconnect-psh5">
                <b class="ps-tag" style="padding-left: 8px;">Quick links</b>

                        <!--<span class="glyphicon glyphicon-refresh navdown-list22"></span>-->
            </h5>
        </a>
    </div>
    <ul class="list-group">
	<li class="list-group-item" data-step="2" data-intro="Survey link">
		<a href="<?= base_url().'survey' ?>"  target="_blank">	
			<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			TNI Survey FY 2017-18
		</a>
	</li>
	<li class="list-group-item">
		<a href="http://helpdesk.skeiron.com" target="_blank">	
		<i class="fa fa-cog" aria-hidden="true"></i>
			IT Help Desk
		</a>
	</li>
	<li class="list-group-item">
		<a href="http://iconnect.skeiron.com/adrenalin"  target="_blank">	
		<i class="fa fa-comments-o" aria-hidden="true"></i>
			iconnect
		</a>
	</li>
	<li class="list-group-item">
		<a href="https://login.microsoftonline.com"  target="_blank">	
			<i class="fa fa-envelope" aria-hidden="true"></i>
			Mail
		</a>
	</li>
	<li class="list-group-item">
		<a href="https://mypayroll.paysquare.com"  target="_blank">	
			<i class="fa fa-money" aria-hidden="true"></i>
			My Payroll
		</a>
	</li>
	</ul>
</div>

<div class="sidebox community_update"style=""  data-step="3" data-intro="Community updates">
    <div class="iconnect-ps">
        <a href="#">
            <h5 class="iconnect-psh5">
                <b class="ps-tag" style="padding-left: 8px;">My Community Updates</b> 
				<!--<span class="glyphicon glyphicon-menu-hamburger navdown-list22"></span>-->
            </h5>
        </a>
    </div>
 
 
    <div class="three-search col-md-12">
                <form action="<?= base_url() . 'profile/wallpost' ?>" method="post" id="wallpostForm">
                    <div class="form-group">
                        <textarea rows="2" class="form-control walltext" id="walltext" placeholder="what's on your mind"
                                  name="walltext"></textarea>

                    </div>
                    <div class="form-group">
                        <input type="submit" value="post" class="btn btn-sm btn-primary pull-right"/>
                    </div>
                </form>


                <!--                <input type="text" class="form-control" placeholder="what's on your mind"/>-->
            </div>
            <div class="clear"></div>

            <h4 style="color: #1C6B94; font-size: 13px; margin-left: 12px;"> </h4>

            <?php
            //            echo '<pre>';
            //            print_r($user_wall);
            //            echo '</pre>';
            ?>

           <ul class="list-group" style="height:100px; overflow:auto">
                <?php
                if (count($CI->user_wall) > 0) {
                    foreach ($CI->user_wall AS $witem) {
                        ?>
                        <li class="list-group-item wallitem_<?= $witem->uw_id ?>">

                            <?php
                            //echo '<pre>';
                            //print_r($witem);
                            //echo '</pre>';
                            ?>

                            <div class="row">
                                <div class="col-xs-2 col-md-2 nopad">
                                    <img src="<?= base_url() . 'assets/' ?>images/50.png" alt="client-user"
                                         clas="img-responsive"/>
                                </div>
                                <div class="col-xs-10 col-md-10" style="padding-left: 15px; padding-top: 4px;">
                                    <div class="" style="font-size: 10px; font-weight: bold;">
                                        <span style="color: #1c6b94;">
                                            <?= $witem->u_name !='' ? $witem->u_name : 'noname'  ?>
                                        </span>


                                        <?php
                                        if ($logindata['user_id'] == $witem->uw_user_id) {
                                            ?>
                                            <div class="btn-group pull-right ">
                                                <button title="Edit" type="button" data-toggle="modal"
                                                        data-target="#wallmodel_<?= $witem->uw_id ?>"
                                                        class="btn btn-primary btn-xs">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button type="button" title="Remove" wallid="<?= $witem->uw_id ?>"
                                                        class="btn btn-primary btn-xs delete_wall_item wall_item_<?= $witem->uw_id ?>">
                                                    <i class="fa fa-remove"></i>
                                                </button>

                                            </div>


                                            <div id="wallmodel_<?= $witem->uw_id ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;</button>
                                                        </div>

                                                        <div class="modal-body">
                                                            <form method="post"
                                                                  action="<?= base_url() . 'profile/wall_update' ?>">
                                                                <input type="hidden" name="edit_wall_id"
                                                                       value="<?= $witem->uw_id ?>"/>
                                                                <div class="form-group">
                                                                    <textarea style="font-weight: normal" rows="3"
                                                                              cols="" class="form-control"
                                                                              name="edit_wall_text"><?= $witem->uw_wall_text ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="submit" class="btn btn-primary"
                                                                           value="Update Now"/>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>


                                    </div>
                                    <br>

                                    <div class="post">
                                        <?= stripcslashes($witem->uw_wall_text) ?>
                                    </div>
                                    <!--  <a href="#">View more..</a>-->

                                </div>
                            </div>
                        </li>
                        <?php
                    }
                }else{
					echo '<center>No updates</center>';
				}
                ?>


            </ul>
 

</div>


<?php
//echo '<pre>';
//print_r($my_comments);
//echo '</pre>';

if (count($my_comments) > 0) {

    ?>
    <div class="sidebox community_update">
        <div class="iconnect-ps" style="/*margin-top: 3px;*/">
            <a href="#">
                <h5 class="iconnect-psh5">
                    <b class="ps-tag" style="padding-left: 8px;">Comments</b>

                        <!--<span class="glyphicon glyphicon-refresh navdown-list22"> </span>-->
                </h5>
            </a>
        </div>
        <div class="col-md-12">
            <ul class="list-group" style="height:100px;overflow:none;">
			<marquee  direction="up" scrollamount="3" onMouseOver="this.stop();" onMouseOut="this.start();">
                <?php
                foreach ($my_comments AS $birthday_comment) {
                    ?>
			
                    <li class="list-group-item">
                        <div class="col-xs-1 col-md-1 asdf-icon">
                            <img src="<?= base_url() . 'assets/' ?>images/50.png" alt="client-user"
                                 clas="img-responsive"
                                 style="margin-top: 5px; margin-left: 5px; margin-right: 5px;"/>
                        </div>
                        <div class="col-xs-11 col-md-11" style="padding-left: 45px; padding-top: 4px;">
                            <p class="ptag1" style="font-size: 10px; font-weight: bold;">

                                <br/>
                                <?php
                                echo $birthday_comment->bc_comment;

                                ?>
                                <br />

                                <?php echo timeAgo($birthday_comment->bc_create); ?>  <span style="color: #1c6b94;">
                               <?= $birthday_comment->u_name ?>
                            </span>

                            </p>
                        </div>
                        <div class="clear"></div>
                    </li>

                    <?php
                }
                ?>
				</marquee>
            </ul>
        </div>
    </div>
    <?php
}




?>
