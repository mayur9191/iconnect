<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <?php include APPPATH . "views/includs/hedder_code.php" ?>

</head>

<body id="login-bgImg">

<div class="container">
    <div class="row" id="login-page-details">
        <div class="col-md-12">
            <div class="col-md-6">
                <img src="<?= base_url() . 'assets/' ?>images/login-msg.png" alt="login-msg" class="img-responsive login-msg" />
            </div>
            <div class="col-md-1 hidden-xs">&nbsp;</div>
            <div class="col-md-4">
                <div class="">
                    <div class="card card-container">
                        <h4 class="login-info">Login Information</h4>
                        <div class="infoMessage">
                            <?php echo $this->session->flashdata('message')?>
                        </div>

                        <form class="form-signin" method="post">
                            <span id="reauth-email" class="reauth-email"></span>
                            <input type="text" id="inputEmail" class="form-control" name="userid" placeholder="User ID" required autofocus>
                            <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
                            <div id="remember" class="checkbox">
                                <label>
                                    <input type="checkbox" value="remember-me"> Save Login Details
                                </label>
                            </div>
                            <button class="btn btn-primary btn-block btn-signin" type="submit">Login</button>
                        </form><!-- /form -->
                        <!--<a href="#" class="forgot-password">
                            Forgot the password ?
                        </a>-->
                    </div><!-- /card-container -->
                </div><!-- /container -->
            </div>
        </div>
    </div>
</div>


<?php include APPPATH . "views/includs/common_scripts.php" ?>


</body>
</html>