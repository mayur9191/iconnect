
		<thead>
			<tr>
				<th>Sr No</th>
				<th>Employee Name</th>
				<th>Employee Code</th>
				<th>Location</th>
				<th>Name of Business</th>
				<th>Name of Reporting Manager</th>
				<th>Name of HOD</th>
				<th>Technical / Functional Training Need 1</th>
				<th>Technical / Functional Training Need 2</th>
				<th>Behavioral / Soft Skills Training Need 1</th>
				<th>Behavioral / Soft Skills Training Need 2</th>
				<th>QHSE Training Need 1</th>
				<th>QHSE Training Need 2</th>
				<th>Tr Need 1</th>
				<th>Tr Need 2</th>
			</tr>
		</thead>

		<tbody>
			<?php $i=1; 
				if(count($surveyReportData) > 0) {
					foreach($surveyReportData as $reportRecord){ ?>
					<tr>
						<th><?= $i ?></th>
						<th><?= $reportRecord->employee_name ?></th>
						<th><?= $reportRecord->employee_code ?></th>
						<th><?= $reportRecord->location ?></th>
						<th><?= $reportRecord->name_of_business ?></th>
						<th><?= $reportRecord->name_of_reporting_manager ?></th>
						<th><?= $reportRecord->name_of_hod ?></th>
						<th><?= getTechnicalName($reportRecord->tech_func_training_need_one) ?></th>
						<th><?= getTechnicalName($reportRecord->tech_func_training_need_two) ?></th>
						<th><?= getBehsoftName($reportRecord->beh_soft_skills_training_need_one) ?></th>
						<th><?= getBehsoftName($reportRecord->beh_soft_skills_training_need_two) ?></th>
						<th><?= getQhseName($reportRecord->qhse_training_need_one) ?></th>
						<th><?= getQhseName($reportRecord->qhse_training_need_two) ?></th>
						<th><?= $reportRecord->tr_need_one ?></th>
						<th><?= $reportRecord->tr_need_two ?></th>
					</tr>
			<?php $i++; } 
				}else{ 
			?>
					<tr><th colspan="14"><center>No data found</center></th></tr>
			<?php } ?>
		</tbody>