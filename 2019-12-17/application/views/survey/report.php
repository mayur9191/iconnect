<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	 <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>


	<div class="container col-md-12" style="background-color: white;">
		<div class="page-header">
			<center>
				<!--<h3>Skeiron Group</h3>-->
				<img src="<?= base_url().'assets/images/logo/Logo.png' ?>" style="width: 15%;" />
				<h4>Learning & Development</h4>
				<h4>Individual Training Needs Identification Survey FY 2017 - 2018</h4>
			</center>
		</div>
		<h4>Survey Report</h4><br>
		<div class="col-xs-12" style="overflow-x:scroll;font-size: 10px;font-family: inherit;">
			<form action="<?= base_url().'survey/getReportDownload' ?>" method="POST" >
				<table>
					<tr>
					<td><h5>Custom Search : </h5></td>
					<td>
						<select class="form-control" name="filter_field" id="filter_field">
							<option value="">Select field</option>
							<option value="employee_name">Employee Name</option>
							<option value="employee_code">Employee Code</option>
							<option value="location">Location</option>
							<option value="name_of_business">Name of Business</option>
							<option value="name_of_reporting_manager">Name of Reporting Manager</option>
							<option value="name_of_hod">Name of HOD</option>
						</select>
					</td>
					<td>
						 &nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td>
						<h5>Search text :</h5>
					</td>
					<td>
						<input type="text" class="form-control" name="search_field" id="search_field" />
					</td>
					<td>
						&nbsp;&nbsp;
					</td>
					<td>
						<a href="#" class="btn btn-primary" name="search_submit" id="search_submit" style="border-radius: 30px;" >Submit</a>
					</td>
					<td>
						&nbsp;&nbsp;
					</td>
					<td>
						<input type="submit" class="btn btn-primary" name="download_btn" value="Download xls" style="border-radius: 30px;" />
						<input type="submit" class="btn btn-primary" name="download_btn" value="Download PDF" style="border-radius: 30px;" />
					</td>
				</table>
			</form>
			
			<br><hr>
			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
				<thead>
					<tr style="font-weight:bold">
						<th>Sr No</th>
						<th>Employee Name</th>
						<th>Employee Code</th>
						<th>Location</th>
						<th>Name of Business</th>
						<th>Name of Reporting Manager</th>
						<th>Name of HOD</th>
						<th>Technical / Functional Training Need 1</th>
						<th>Technical / Functional Training Need 2</th>
						<th>Behavioral / Soft Skills Training Need 1</th>
						<th>Behavioral / Soft Skills Training Need 2</th>
						<th>QHSE Training Need 1</th>
						<th>QHSE Training Need 2</th>
						<th>Tr Need 1</th>
						<th>Tr Need 2</th>
					</tr>
				</thead>

				<tbody>
				<?php $i=1; foreach($surveyReportData as $reportRecord){ ?>
					<tr>
						<th><?= $i ?></th>
						<th><?= $reportRecord->employee_name ?></th>
						<th><?= $reportRecord->employee_code ?></th>
						<th><?= $reportRecord->location ?></th>
						<th><?= $reportRecord->name_of_business ?></th>
						<th><?= $reportRecord->name_of_reporting_manager ?></th>
						<th><?= $reportRecord->name_of_hod ?></th>
						<th><?= getTechnicalName($reportRecord->tech_func_training_need_one) ?></th>
						<th><?= getTechnicalName($reportRecord->tech_func_training_need_two) ?></th>
						<th><?= getBehsoftName($reportRecord->beh_soft_skills_training_need_one) ?></th>
						<th><?= getBehsoftName($reportRecord->beh_soft_skills_training_need_two) ?></th>
						<th><?= getQhseName($reportRecord->qhse_training_need_one) ?></th>
						<th><?= getQhseName($reportRecord->qhse_training_need_two) ?></th>
						<th><?= $reportRecord->tr_need_one ?></th>
						<th><?= $reportRecord->tr_need_two ?></th>
					</tr>
				<?php $i++; }  ?>
			</tbody>
			</table>
		</div>
	</div> <!-- ./container -->
</body>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": [null, null,null,null, null,null,null, null,null,null, null,null,null, null,null],
		"aaSorting": [],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		//"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
	
	//custom search 
	$("#search_submit").on("click", function(){
		var filter_field = $("#filter_field").val();
		var search_field = $("#search_field").val();
		var filterstatus = true;
		if(filterstatus){
			 $.ajax({
                type: "POST",
                url: "<?= base_url().'survey/customReport' ?>",
                data: {filter_field:filter_field,search_field:search_field},
                cache: false,
                success: function (data) {
                    //console.log(data);
                    if (data){
                        $("#dynamic-table").html('');
                        $("#dynamic-table").html(data);
                    }
                }
            });
		}
	});
	
	/*$("#download_pdf").on("click", function(){
		var filter_field = $("#filter_field").val();
		var search_field = $("#search_field").val();
		var filterstatus = true;
		if(filterstatus){
			 $.ajax({
                type: "POST",
                url: "<?= base_url().'survey/downloadPDF' ?>",
                data: {filter_field:filter_field,search_field:search_field},
                cache: false,
                success: function (data) {
                    //console.log(data);
                    if (data){
                        //$("#dynamic-table").html('');
                        //$("#dynamic-table").html(data);
                    }
                }
            });
		}
	});*/
	/*
	Here, I am attaching the required joining documents (pfa). 
	In my previous company, I don't have PF account. Need to create a PF account in this company.
	
	If any mistake or unfilled in the attached documents please do let me know.
	*/
	
	
});
</script>