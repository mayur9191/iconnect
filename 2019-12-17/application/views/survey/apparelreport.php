<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	 <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>


	<div class="container col-md-12" style="background-color: white;">
		<div class="page-header">
			<center>
				<!--<h3>Skeiron Group</h3>-->
				<img src="<?= base_url().'assets/images/logo/Logo.png' ?>" style="width: 15%;" />
				<h4> Apparel Sizes -- Report</h4>
			</center>
		</div>
		<h4>Survey Report</h4><br>
		
		<div class="col-xs-12" style="overflow-x:scroll;font-size: 10px;font-family: inherit;">
			<a href="<?php echo base_url().'survey/apparel_downloadReport'; ?>" class="btn btn-sm btn-primary pull-right" >Export CSV</a>
			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
				<thead>
					<tr style="font-weight:bold">
						<th>Sr No</th>
						<th>Employee Code</th>
						<th>Employee Name</th>
						<th>Location</th>
						<th>Shirt Size</th>
						<th>Shirt Collar size</th>
						<th>T-shirt size</th>
					</tr>
				</thead>

				<tbody>
				<?php $i=1; foreach($surveyApparelReportData as $reportRecord){ ?>
					<tr>
						<th><?= $i ?></th>
						<th><?= $reportRecord->emp_code ?></th>
						<th><?= $reportRecord->full_name ?></th>
						<th><?= $reportRecord->location ?></th>
						<th><?= $reportRecord->shirt_size ?></th>
						<th><?= $reportRecord->shirt_collar_size ?></th>
						<th><?= $reportRecord->t_shirt_size ?></th>
					</tr>
				<?php $i++; }  ?>
			</tbody>
			</table>
		</div>
	</div> <!-- ./container -->
</body>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": [null, null,null,null, null,null,null],
		"aaSorting": [],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		//"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
	
	//custom search 
	$("#search_submit").on("click", function(){
		var filter_field = $("#filter_field").val();
		var search_field = $("#search_field").val();
		var filterstatus = true;
		if(filterstatus){
			 $.ajax({
                type: "POST",
                url: "<?= base_url().'survey/customReport' ?>",
                data: {filter_field:filter_field,search_field:search_field},
                cache: false,
                success: function (data) {
                    //console.log(data);
                    if (data){
                        $("#dynamic-table").html('');
                        $("#dynamic-table").html(data);
                    }
                }
            });
		}
	});
	
	/*$("#download_pdf").on("click", function(){
		var filter_field = $("#filter_field").val();
		var search_field = $("#search_field").val();
		var filterstatus = true;
		if(filterstatus){
			 $.ajax({
                type: "POST",
                url: "<?= base_url().'survey/downloadPDF' ?>",
                data: {filter_field:filter_field,search_field:search_field},
                cache: false,
                success: function (data) {
                    //console.log(data);
                    if (data){
                        //$("#dynamic-table").html('');
                        //$("#dynamic-table").html(data);
                    }
                }
            });
		}
	});*/
	/*
	Here, I am attaching the required joining documents (pfa). 
	In my previous company, I don't have PF account. Need to create a PF account in this company.
	
	If any mistake or unfilled in the attached documents please do let me know.
	*/
	
	
});
</script>