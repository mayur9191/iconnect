<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'theme/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'theme/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'theme/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'theme/' ?>bootstrap/js/bootstrap.min.js"></script>
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body style="background-color: lightgray;">
	<div class="col-md-1"></div>
	<div class="container col-md-10" style="background-color: white;">
		<div class="page-header">
			<center>
				<h3>Skeiron Group</h3>
				<h4>Learning & Development</h4>
				<h4>Individual Training Needs Identification Survey FY 2017 - 2018</h4>
			</center>
		</div>
		<!--<div class="col-md-2"></div>-->
		<form class="form-horizontal col-md-12" role="form" action="<?= 'Survey/submit_survey' ?>" method="POST" >		
			<div class="form-group col-md-12">
				<h6 class="col-sm-6" style="margin-left:20px;color:red;">	Note - All fields marked * are mandatory.</h6>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of Employee <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_name" value="<?= $empData['employee_name'] ? $empData['employee_name'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of Business <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="business_name" value="<?= $empData['name_of_business'] ? $empData['name_of_business'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Employee Code <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_code" value="<?= $empData['employee_code'] ? $empData['employee_code'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Location <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_location" value="<?= $empData['location'] ? $empData['location'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of Reporting Manager <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_reporting_manager" value="<?= $empData['name_of_reporting_manager'] ? $empData['name_of_reporting_manager'] :'' ;  ?>" placeholder="" required="required" class="form-control" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of HOD <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_hod" value="<?= $empData['name_of_hod'] ? $empData['name_of_hod'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			<br>
			<div style="margin-left:20px;">
				<div class="form-group col-md-12">
					<h6 class="col-sm-8">Please fill in the prioritised Training Needs in the below options </h6>
				</div>
				
				<div class="col-md-1"></div>
				<div class="form-group col-md-10">
					<label class="control-label">1. Please select the titles of your Top Two Technical / Functional Training Needs</label>
					<div class="col-sm-9">
					</div>
				</div>
				
				<div class="form-group col-md-10">
					<label class="col-sm-6 control-label"><h5>Technical / Functional Training Need 1</h5></label>
					<div class="col-sm-5">
						<select id="tech_func_1" name="tech_func_1" class="form-control">
							<option value="">Please select </option>
							<?php foreach($get_tec_func_data as $tech_func_record){ ?>
									<option <?= $empData['tech_func_training_need_one']==$tech_func_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $tech_func_record->tbl_primary_id ?>"><?= $tech_func_record->title ?></option>
							<?php } ?>
						</select>
						<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
					</div>
				</div>
				
				<div class="form-group col-md-10">
					<label class="col-sm-6 control-label"><h5>Technical / Functional Training Need 2</h5></label>
					<div class="col-sm-5">
						<select id="tech_func_2" name="tech_func_2" class="form-control">
							<option value="">Please select </option>
							<?php foreach($get_tec_func_data as $tech_func_record){ ?>
									<option <?= $empData['tech_func_training_need_two']==$tech_func_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $tech_func_record->tbl_primary_id ?>"><?= $tech_func_record->title ?></option>';
							<?php } ?>
						</select>
						<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
					</div>
				</div>
				
				<div class="form-group col-md-8"  style="margin-left: 60px;">
					<label class="control-label">2.Please select the titles of your Top Two Behavioral / Soft Skills Training Needs</label>
					<div class="col-sm-9"></div>
				</div>
				
				
				<div class="form-group col-md-10">
					<label class="col-sm-6 control-label"><h5>Behavioral / Soft SKills Training Need 1</h5></label>
					<div class="col-sm-5">
						<select id="soft_skill_1" name="soft_skill_1" class="form-control">
							<option value="">Please select </option>
							<?php foreach($behave_soft_skils_list_data as $soft_skill_record){ ?>
								<option <?= $empData['beh_soft_skills_training_need_one']==$soft_skill_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $soft_skill_record->tbl_primary_id ?>"><?= $soft_skill_record->title ?></option>
							<?php } ?>
						</select>
						<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
					</div>
				</div>
				
				<div class="form-group col-md-10">
					<label class="col-sm-6 control-label"><h5>Behavioral / Soft SKills Training Need 2</h5></label>
					<div class="col-sm-5">
						<select id="soft_skill_2" name="soft_skill_2" class="form-control">
							<option value="">Please select </option>
							<?php foreach($behave_soft_skils_list_data as $soft_skill_record){ ?>
								<option <?= $empData['beh_soft_skills_training_need_two']==$soft_skill_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $soft_skill_record->tbl_primary_id ?>"><?= $soft_skill_record->title ?></option>
							<?php } ?>
						</select>
						<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
					</div>
				</div>
				
				
				<div class="form-group col-md-8"  style="margin-left: 60px;">
					<label class="control-label">3. Please select the titles of your Top Two QHSE Training Need.</label>
					<div class="col-sm-9"></div>
				</div>
				
				
				<div class="form-group col-md-10">
					<label class="col-sm-6 control-label"><h5>QHSE Training Need 1</h5></label>
					<div class="col-sm-5">
						<select id="qhse_1" name="qhse_1" class="form-control">
							<option value="">Please select </option>
							<?php foreach($get_qhse_list_data as $qhse_record){ ?>
								<option <?= $empData['qhse_training_need_one']==$qhse_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $qhse_record->tbl_primary_id ?>"><?= $qhse_record->title ?></option>
							<?php } ?>
						</select>
						<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
					</div>
				</div>
				
				<div class="form-group col-md-10">
					<label class="col-sm-6 control-label"><h5>QHSE Training Need 2</h5></label>
					<div class="col-sm-5">
						<select id="qhse_2" name="qhse_2" class="form-control">
							<option value="">Please select </option>
							<?php foreach($get_qhse_list_data as $qhse_record){ ?>
								<option <?= $empData['qhse_training_need_two']==$qhse_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $qhse_record->tbl_primary_id ?>"><?= $qhse_record->title ?></option>
							<?php } ?>
						</select>
						<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
					</div>
				</div>
				
				
				<div class="form-group col-md-8"  style="margin-left: 60px;">
					<label class="control-label">4. Please insert maximum two training needs not available in the Drilldown list</label>
					<div class="col-sm-9"></div>
				</div>
				
				<div class="col-md-3"></div>
				<div class="form-group col-md-5" style="margin-left: 100px;">
					<label class="col-sm-6 control-label"><h5>Tr Need 1</h5></label>
					<div class="col-sm-10">
						<textarea rows="5" name="tr_need_1" maxlength="100" class="form-control" ><?= $empData['tr_need_one'] ? $empData['tr_need_one'] :'' ;  ?></textarea>
					</div>
				</div>
				
				<div class="form-group col-md-5">
					<label class="col-sm-6 control-label"><h5>Tr Need 2</h5></label>
					<div class="col-sm-10">
						<textarea rows="5" name="tr_need_2" maxlength="100" class="form-control" ><?= $empData['tr_need_two'] ? $empData['tr_need_two'] :'' ;  ?></textarea>
					</div>
				</div>
				
				<?php if($empData['is_submit'] == 0){ ?>
					<div class="form-group" style="margin-left: 20%;">
						<div class="form-group col-md-6">
							<div class="col-sm-4 col-sm-offset-3">
								<input type="submit" class="btn btn-primary btn-block" name="submit" value="Save" />
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="col-sm-4">
								<input type="submit" class="btn btn-primary btn-block" name="submit" value="Submit" />
							</div>
						</div>
					</div>
				<?php } ?>
				
				</div>
			</div>
		</form> <!-- /form -->
	</div> <!-- ./container -->
</body>
</html>

<script>
$(document).ready(function(){
	
});
</script>