<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= 'L&D Portal'; //isset($titile)?$titile." Portal":'Portal'?></title>

	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/bootstrap.min.css"  />
	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/style.css"  />

	<link rel="stylesheet" href="<?=base_url().'assets/'?>css/fontawesome.css" />

	<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>intro/introjs.css" />
	<script type="text/javascript" src="<?= base_url().'assets/' ?>intro/intro.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php
	$CI = & get_instance();
	$logindata = $this->session->userdata('user_ldamp');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" media="screen">
<style>
.panel-body.a{ padding:0px 0px 0px 0px;}
.panel-default>.panel-heading {
    color: #f8ffff;
    background-color: #8fb729;
    border-color: #8fb729;
}
.list-group-item{
	border: 1px solid #8fb729;
}
.sidebox{
	border: none;
}
</style>	 
</head>
<body style="/*background-color: #dff0d8;*/">
<nav class="navbar navbar-default navbar-inverse navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid ">
    <div class=" topbar">
	<span class="pull-right hidden-xs" style="margin-top:4px; margin-left:4px;">
	Welcome <br><?= (isset($logindata['displayname']))? ucwords($logindata['displayname']) : ''; ?>
	</span>
		
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
			
            <a class="navbar-brand" href="<?= base_url() ?>" style="font-size: 22px;font-weight: 700;">
                <img alt="alternative text" src="<?= base_url() . 'assets/' ?>images/iconnect-logo.png"/>
            </a>
        </div>
        <div class="animated fadeIn" id="bs-example-navbar-collapse-1">
            <!--<div class="collapse navbar-collapse animated fadeIn" id="bs-example-navbar-collapse-1">-->
            <center>
                <ul class="nav navbar-nav animated fadeIn text16">
                  
                </ul>
               
            </center>
			<?php

			if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
				$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
			} else {

				$dp_image = base_url() . 'assets/images/45.png';
			}
			?>
            <ul class="nav navbar-nav navbar-right nopadding" style="margin-right:0px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $dp_image; ?>" style="width: 30px;border-radius: 20px;"  alt="profile"
                             class="img-responsive hidden-xs"/>
                    </a>
                    <ul class="dropdown-menu animated flipInX" role="menu">

                        <?php

                        if ($logindata['role'] == 1) {
                            ?>
                            
                            <li>
                                <a href="#" data-toggle="modal" data-target="#profile_slide">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                    Banner Slider
                                </a>
                            </li>
                            <?php
                        }
                        ?>
						<!--<li><a href="<?= base_url() . 'profile/edit' ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i> Edit Profile </a></li>
                        <li class="divider"></li>
                        <li><a href="#">Welcome <?= ucwords($user_ldamp['name']) ?></a></li>
                        <li class="divider"></li>-->
                        <li><a href="<?= base_url() . 'profile/logout' ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </div><!-- /.container-fluid -->
</nav>

<?php

if ($logindata['role'] == 1) {
    ?>

    <div id="profile_slide" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="post" enctype="multipart/form-data" class="bithdaypost"
                      action="<?= base_url() . 'profile/sliders' ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Profile Slide</h4>
                    </div>
                    <div class="modal-body">
                        <div id="wrapper" style="margin-top: 20px;">
                            <div id="image-holder" class="col-md-12 nopad"  style="display: flex; max-height:250px; ">
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <input class="fileUpload" name="image" class="" type="file"/>
                                <i><b>Note:</b> Upload image size in 520 x 200 </i>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-primary" type="submit" value="Upload Now"/>

                    </div>
                </form>
            </div>

        </div>
    </div>



    <?php
}
?>
<style>
	.galimg{
		max-width:22%; margin-top:22px;
	}
	.thumbnail{
		/*margin:2px 2px 2px 2px;*/
		margin:2px 15px 10px 10px
	}
	.thumbnail:hover{
		border: 1px solid #1c6b94;
	}
	body {
		color: #6d7679;
		font-family: arial;
		font-weight: normal;
		line-height: 1.5;
	}
	
	h3 {
		font-size: 1.5rem;
	}
	h3 {
		font-size: 1.3125rem;
		line-height: 1.2;
		margin-bottom: 1rem;
		margin-top: 1rem;
	}
	section .column, section .columns {
		margin-bottom: 1rem;
	}
	.container-fluid.navbar-inverse{
		 
	}
	#footer {
		position: absolute;
		bottom: 0;
		width: 644px;
		height: 20px;
		margin: 0 auto;
		display:none;
	}
</style>



<div class="navbar-inverse" style="margin-top:-8px;">
	<div class="col-md-12 col-xs-12" style="background-color:#8fb729;color:#FFF;margin-bottom: 20px;padding:2px">
		<span class="pull-left" style="color:#FFF;padding-left:10px;"><h3><b><?php echo $folder_name; ?></b></h3></span>
		<a href="<?php echo base_url().'learn_and_development'; ?>" class="btn btn-default pull-right">Back</a>
	</div>
</div>



<div class="container-fluid main">
<div class="row">
    <!--<div class="col-md-12">
		<div class="container-fluid main" style="margin-top:20px;">-->
   
	<section class="col-md-12">
		<div class="row grid1">
			<?php 
				foreach($image_list['imglist'] as $img_record){
					if($img_record !=''){
						$image_urls = $image_url.$img_record;
						
						if(strpos($img_record, '.xlsx') != false || strpos($img_record, '.xls') != false){
							$icon_image_urls = base_url().'uploads/l_and_d/icons/excel.png';
						}
						
						if(strpos($img_record, '.doc') != false || strpos($img_record, '.docx') !=false){
							$icon_image_urls = base_url().'uploads/l_and_d/icons/word.png';
						}
						
						if(strpos($img_record, '.ppt') != false || strpos($img_record, '.pptx') !=false){
							$icon_image_urls = base_url().'uploads/l_and_d/icons/ppt.png';
						}
						
						if(strpos($img_record, '.pdf') != false){
							$icon_image_urls = base_url().'uploads/l_and_d/icons/pdf.png';
						}
						
						if($icon_image_urls == ''){
							$icon_image_urls = base_url().'uploads/l_and_d/icons/default.png';
						}
						
						//echo '<span><a href="'.$image_urls.'" data-lightbox="example-set"><img class="col-md-3 col-sm-12 thumbnail" src="'.$icon_image_urls.'" class="" alt="" style="height: 200px;width: 200px;" /></a>'.$img_record.'</span>';
						
						//$fileName = substr($img_record,0,21);
						$fileName = $img_record;
						echo '<div class="col-xs-12 col-md-1 col-sm-12 thumbnail" style="height: 140px;" title="'.$img_record.'"><center><img src="'.$icon_image_urls.'" alt="" class="img-responsive" style="height:60px;width:60px;"><p><a href="https://view.officeapps.live.com/op/view.aspx?src='.$image_urls.'" data-lightbox="example-set" title="'.$img_record.'" style="font-size:11px;" target="_blank">'.$fileName.'</a></p></center></div>';
						
						/*
						echo '<div class="col-xs-12 col-md-1 col-sm-12 thumbnail" style="height: 140px;" title="'.$img_record.'"><center><img src="'.$icon_image_urls.'" alt="" class="img-responsive" style="height:60px;width:60px;"><p><a href="'.$image_urls.'" data-lightbox="example-set" title="'.$img_record.'" style="font-size:11px;">'.$fileName.'</a></p></center></div>';
						*/
					}
				}
			?>
        </div>
	</section>
	<!--
	<div class="row">
		<div class="col-md-12 grid" id="containerss">
			 <link rel="stylesheet" href="<?php echo base_url().'assets/sliderjs/dist/css/lightbox.min.css'; ?>">
				<?php 
					foreach($image_list['imglist'] as $img_record){
						if($img_record !=''){
							$image_urls = $image_url.$img_record;
							
							if(strpos($img_record, '.xlsx') != false || strpos($img_record, '.xls') != false){
								$icon_image_urls = base_url().'uploads/l_and_d/icons/excel.png';
							}
							
							if(strpos($img_record, '.doc') != false || strpos($img_record, '.docx') !=false){
								$icon_image_urls = base_url().'uploads/l_and_d/icons/word.png';
							}
							
							if(strpos($img_record, '.ppt') != false || strpos($img_record, '.pptx') !=false){
								$icon_image_urls = base_url().'uploads/l_and_d/icons/ppt.png';
							}
							
							if(strpos($img_record, '.pdf') != false){
								$icon_image_urls = base_url().'uploads/l_and_d/icons/pdf.png';
							}
							
							if($icon_image_urls == ''){
								$icon_image_urls = base_url().'uploads/l_and_d/icons/default.png';
							}
							
							//https://view.officeapps.live.com/op/view.aspx?src=
							
							/*
							echo '<span><a href="'.$image_urls.'" data-lightbox="example-set">
							<img class="col-md-3 col-sm-12 thumbnail" src="'.$icon_image_urls.'" class="" alt="" style="height: 200px;width: 200px;" /></a>'.$img_record.'</span>';
							*/
							
						}
					}
				?>
		</div>
	</div>
	
</div>
-->

<?php include APPPATH . "views/includs/common_scripts.php" ?>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/masonry/3.1.5/masonry.pkgd.min.js"></script>
<script>
$( function() {
	//alert();  
    var m = new Masonry($('.grid').get()[0], {
        itemSelector: ".thumbnail"
    });	
	
	setInterval(function(){ 
		var m = new Masonry($('.grid').get()[0], {
			itemSelector: ".thumbnail"
		});	
	}, 3000);
});
</script>
		

    </div>
</div>

</div>



<?php include APPPATH . "views/includs/common_scripts.php" ?>

<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<!--<script type="text/javascript" src="--><? //= base_url() . 'assets/' ?><!--croppic/croppic.min.js"></script>-->

<div id="myModal" class="modal middle-slid" style="display: none;margin-top:80px;">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <img src="<?= base_url().'uploads/announcement/'.$witem->an_file;?>" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>
  </div>
</div>

<!-- <script src="<?= base_url().'assets/' ?>dist/js/lightbox-plus-jquery.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js"></script>


<style>
<!-- Tree view code  -->

</style>
</body>
</html>
