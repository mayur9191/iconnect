<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= 'L&D Portal'; //isset($titile)?$titile." Portal":'Portal'?></title>

	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/bootstrap.min.css"  />
	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/style.css"  />

	<link rel="stylesheet" href="<?=base_url().'assets/'?>css/fontawesome.css" />

	<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>intro/introjs.css" />
	<script type="text/javascript" src="<?= base_url().'assets/' ?>intro/intro.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php
	$CI = & get_instance();
	$logindata = $this->session->userdata('user_ldamp');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" media="screen">
<style>
.panel-body.a{ padding:0px 0px 0px 0px;}
.panel-default>.panel-heading {
    color: #f8ffff;
    background-color: #8fb729;
    border-color: #8fb729;
}
.list-group-item{
	border: 1px solid #8fb729;
}
.sidebox{
	border: none;
}
</style>	 
</head>
<body style="/*background-color: #dff0d8;*/">
<nav class="navbar navbar-default navbar-inverse navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid ">
    <div class=" topbar">
	<span class="pull-right hidden-xs" style="margin-top:4px; margin-left:4px;">
	Welcome <br><?= (isset($logindata['displayname']))? ucwords($logindata['displayname']) : ''; ?>
	</span>
		
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
			
            <a class="navbar-brand" href="<?= base_url() ?>" style="font-size: 22px;font-weight: 700;">
                <img alt="alternative text" src="<?= base_url() . 'assets/' ?>images/iconnect-logo.png"/>
            </a>
        </div>
        <div class="animated fadeIn" id="bs-example-navbar-collapse-1">
            <!--<div class="collapse navbar-collapse animated fadeIn" id="bs-example-navbar-collapse-1">-->
            <center>
                <ul class="nav navbar-nav animated fadeIn text16">
                  
                </ul>
               
            </center>
			<?php

			if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
				$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
			} else {

				$dp_image = base_url() . 'assets/images/45.png';
			}
			?>
            <ul class="nav navbar-nav navbar-right nopadding" style="margin-right:0px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $dp_image; ?>" style="width: 30px;border-radius: 20px;"  alt="profile"
                             class="img-responsive hidden-xs"/>
                    </a>
                    <ul class="dropdown-menu animated flipInX" role="menu">

                        <?php

                        if ($logindata['role'] == 1) {
                            ?>
                            
                            <li>
                                <a href="#" data-toggle="modal" data-target="#profile_slide">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                    Banner Slider
                                </a>
                            </li>
                            <?php
                        }
                        ?>
						<!--<li><a href="<?= base_url() . 'profile/edit' ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i> Edit Profile </a></li>
                        <li class="divider"></li>
                        <li><a href="#">Welcome <?= ucwords($user_ldamp['name']) ?></a></li>
                        <li class="divider"></li>-->
                        <li><a href="<?= base_url() . 'profile/logout' ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </div><!-- /.container-fluid -->
</nav>

<?php

if ($logindata['role'] == 1) {
    ?>

    <div id="profile_slide" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
               
            </div>

        </div>
    </div>



    <?php
}
?>



<div class="container-fluid main">
<div class="row">
    <div class="col-md-12">

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
						
			
			<div class="col-xs-12 col-md-12 asdf-icon" data-step="1" data-intro="Profile picture upload">

				<?php
				if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
					$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
				} else {

					$dp_image = base_url() . 'assets/images/45.png';
				}
				?>
				<div class="col-md-12 nopad" data-toggle="modal" data-target="#update_dp" style="display: flex;color: #8fb729;font-weight:700;">
					<a href="<?php echo base_url().'learn_and_development';?>"><h3>Learning & Development</h3></a>
				</div>
				<div class="clear"></div>
			</div>


				<div class="col-xs-8 col-md-9" style="padding-left: 30px; padding-top: 10px; line-height: 20px;">
					<div class="clear"></div>
					<span class="clientuser-name2">
					</span>
				</div>
			<div class="clear"></div>


			<div class="clear"></div>

			<!------------- Start  ------------------------------------>
				 <!-- first column-->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopad">
				
					<div class="panel-group sidebox community_update" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h6 class="panel-title iconnect-psh5">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										About L & D
									</a>
								</h6>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'aboutus';?>">About Us</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'key_info';?>">Key Information At L & D</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'roles';?>">Role & Responsibilities</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										L & D Offerings
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Programmes_Offerred';?>" target="_blank">Programmes & Offerings</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/L_and_D_Calendars';?>" target="_blank">L & D Calenders</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Virtual Learning
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item">E Learning</li>
										<li class="list-group-item">Presentation of Trainings</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFour">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
										L & D Procedures
									</a>
								</h4>
							</div>
							<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Forms';?>" target="_blank">Forms</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Processes';?>" target="_blank">Processs</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Templates';?>" target="_blank">Templates</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFive">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
										L & D Dashboards
									</a>
								</h4>
							</div>
							<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Mis';?>" target="_blank">MIS</a></li>
										<li class="list-group-item">Locational Training Data</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!------------- END    ------------------------------------>
				

        </div>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox" style="text-align: justify;text-justify: inter-word;">
	   
		
		
		<div class="row">
		
            <div style="height:345px;overflow-y:scroll;padding: 5px 5px 5px 5px;">
				<h2 style="color: #8fb729;">Learning and Development at Skeiron</h2>

				<h5 style="color: #8fb729;"><b>INTRODUCTION</b></h5>
				<h5 style="color: #8fb729;"><b>About L & D Skeiron</b></h5>

				<p>L&D is established to operate as a Centre of Excellence under the HR, at Group Level and is responsible for providing value added training interventions to the entire Skeiron value chain.</p>

				<p>In the year 2016, Skeiron set on a mission to establish a Learning and Development function which will cater to its envisaged growth and ever-growing need of human capability development. While this has addressed one area which strongly emerged in the Employee Engagement Survey, it was the need of the hour too, to have a full-fledged function at the Group level, A multi-disciplinary L & D unit which is responsible for rolling out learning interventions across various business of Skeiron. Types of trainings conducted are Technical, Behavioral, Functional and Induction.</p>

				<h5 style="color: #8fb729;"><b>L & D Vision</b></h5>
				To be a partner in the Organizational Growth by establishing as an Integrated L& D Function.
				Establishing partnership with all internal and external stakeholders to transform Skeiron into a Learning Organization.

				<h5 style="color: #8fb729;"><b>L & D Mission</b></h5>
				To drive business results by establishing a competitive advantage for Skeiron as the best place for RE Community to learn and develop, by delivering the highest quality learning and organizational development initiatives, to meet the prioritized needs of Today and Tomorrow.

				<h5 style="color: #8fb729;"><b>L & D Total Value Proposition</b><h5>
				<ul>
					<li>Strong business Alignment to realise Organizational Vision and Mission</li>
					<li>Learning Solutions to address Business Drivers - e. g. Execution Excellence, Technology Leadership, Strengthening People & Structure, etc.</li>
					<li>Support Performance Management and Improvement</li>
					<li>Catalyze Skill building through Customized Trainings</li>
					<li>Effect Knowledge Management</li>
					<li>Facilitate Organizational Development</li>
					<li>Achieve Standardization thru’ a Process Driven Approach w. r. t. L & D</li>
					<li>Developing employees with necessary skills and build competence to meet their growth aspirations</li>
					<li>One Window solution for Recruitment, Orientation and Induction of Fresh recruits to Offer Best in Class Experience</li>
					<li>Structured Induction and On Boarding Programme for Lateral Joinees for a Competitive Experience</li>
				</ul>

				<h5 style="color: #8fb729;"><b>L & D – The 3 S Approach</b></h5>
				 Strategize - Alignment, Execution, Tracking, Improvement
				 Structurize -  Hub and Spoke structure, Reporting, 
				 Strengthen - Standardization, Process Orientation, L & D Audits
			</div>
			
            </div>
			<!-- image slide code end -->
			
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
				<div class="clear"></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
				
				<div class="col-md-6" style="padding: 0px 8px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Image Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
									<img src="<?php echo base_url().'uploads/corpcom/Synergy Projects Banglore/Slide9.JPG'; ?>" class="all studio img_width"/>
									<img src="<?php echo base_url().'uploads/corpcom/Synergy Projects Banglore/Slide10.JPG'; ?>" class="all studio img_width"/>
									<img src="<?php echo base_url().'uploads/corpcom/Synergy Projects Banglore/Pic 8.jpg'; ?>" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="col-md-6" style="padding: 0px 0px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Video Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/video_gallery'; ?>" target="_blank">
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_170500909.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_171408479.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_172338706.JPG" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>	
			</div>
			
		
		
 
        </div>
		
		
		
		
			
		
		
		
		
		

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
            
			<!----------------- ----------------------------->
	
	<link href="<?php echo base_url().'assets/img_library/_css/Icomoon/style.css';?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url().'assets/img_library/_css/main.css'; ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url().'assets/img_library/_css/sim-prev-anim.css'; ?>" rel="stylesheet" type="text/css" />
<style>
.iconnect-ps .active .ps-tag{
	color:#000;
}
body{
	background-color:#fff !important;
}

</style>
<style>
.sim-anim-7 {
    position: absolute; 
}

.img_width{
	max-width: 95%;
	
}
</style>
<div class="">
</div>

<div class="clear"></div>
<div class="clear"></div>


<div class="clear"></div>

<!-- events start -->
<div class="sidebox community_update">
		<div class="panel-group sidebox community_update" id="accordion" role="tablist" aria-multiselectable="true">
				 <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingfourty">
						<h6 class="panel-title iconnect-psh5">
							<a role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseFourty" aria-expanded="true" aria-controls="collapseFourty">
								Know SKEIRON
							</a>
						</h6>
					</div>
					<div id="collapseFourty" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfourty">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">About Skeiron</li>
								<li class="list-group-item">Vision</li>
								<li class="list-group-item">Mission</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingten">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseTen" aria-expanded="false" aria-controls="headingten">
								Polls & Survey
							</a>
						</h4>
					</div>
					<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingten">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">TNI Surveys</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingthirty">
						<h6 class="panel-title iconnect-psh5">
							<a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapsethirty" aria-expanded="true" aria-controls="collapsethirty">
								Food for Brain
							</a>
						</h6>
					</div>
					<div id="collapsethirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthirty">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">Good Articles</li>
								<li class="list-group-item">Quizzer & Teasers</li>
							</ul>
						</div>
					</div>
				</div>
				
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingEighty">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseEighty" aria-expanded="false" aria-controls="collapseEighty">
								Quick Links
							</a>
						</h4>
					</div>
					<div id="collapseEighty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighty">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">Connect With Us</li>
								<li class="list-group-item">Help Desk</li>
							</ul>
						</div>
					</div>
				</div> 
			</div>
			
			
			<div class="panel panel-default" style="margin-top:-5px;">
				<div class="panel-heading" role="tab" id="headingfourty">
					<h6 class="panel-title iconnect-psh5">
						<a role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseFourty" aria-expanded="true" aria-controls="collapseFourty">
							Trainings
						</a>
					</h6>
				</div>
				<div id="collapseFourty" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfourty">
					<div class="panel-body a">
						<ul class="list-group">
							<marquee  behavior="scroll" direction="up" height="160" scrolldelay="100" scrollamount="2" onMouseOver="this.stop()" onMouseOut="this.start()">
								<li class="list-group-item" style="border-top: 1px solid #8fb729;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">Event 1</li>
								<li class="list-group-item" style="border-top: 1px solid #8fb729;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">Event 2</li>
								<li class="list-group-item" style="border-top: 1px solid #8fb729;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">Event 3</li>
								<li class="list-group-item" style="border-top: 1px solid #8fb729;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">Event 4</li>
							</marquee>
						</ul>
					</div>
				</div>
			</div>
	</div>
<!-- events end -->
<div class="clearfix"></div>
<!-- events start -->

<div class="clearfix"></div>
<!-- events end -->

			
			<!----------------- ----------------------------->
			
						
			
        </div>

    </div>
</div>

</div>



<?php include APPPATH . "views/includs/common_scripts.php" ?>

<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<!--<script type="text/javascript" src="--><? //= base_url() . 'assets/' ?><!--croppic/croppic.min.js"></script>-->

<div id="myModal" class="modal middle-slid" style="display: none;margin-top:80px;">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <img src="<?= base_url().'uploads/announcement/'.$witem->an_file;?>" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>
  </div>
</div>

<!-- <script src="<?= base_url().'assets/' ?>dist/js/lightbox-plus-jquery.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js"></script>

<div class="container-fluid navbar-inverse " style="margin-top:10px;">
    <div class="container footer">
        <div class="col-md-12 col-xs-12">
            <span class="pull-left" style="color: darkgray;font-size:10px;"> © <?= date('Y') ?> ALL RIGHTS RESERVED BY <b>SKEIRON IT</b> </span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 <!--<a href="javascript:void(0);" onclick="javascript:introJs().start();" class="pull-right" style="color:white;font-size: 12px;margin-left: 10px;">Website tour</a>-->
            <?= getLastLogin($CI->user_ldamp['employeeid']) ?>
        </div>
    </div>
</div>
<style>
<!-- Tree view code  -->

</style>
</body>
</html>
