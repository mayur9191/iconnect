<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Iconnect</title>

<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/bootstrap.min.css"  />
<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/style.css"  />

<link rel="stylesheet" href="<?=base_url().'assets/'?>css/fontawesome.css" />

<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>intro/introjs.css" />
<script type="text/javascript" src="<?= base_url().'assets/' ?>intro/intro.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="icon" href="<?php echo base_url().'uploads/favicon.png'; ?>" sizes="192x192" />


<!--https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css-->

<?php
$CI = & get_instance();
$logindata = $this->session->userdata('user_ldamp');
?>