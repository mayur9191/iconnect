<div class="container-fluid navbar-inverse " style="margin-top:10px;">
    <div class="container footer">
        <div class="col-md-12 col-xs-12">
            <span class="pull-left" style="color: darkgray;font-size:10px;"> © <?= date('Y') ?> ALL RIGHTS RESERVED BY <b>SKEIRON IT TEAM</b> </span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 <!--<a href="javascript:void(0);" onclick="javascript:introJs().start();" class="pull-right" style="color:white;font-size: 12px;margin-left: 10px;">Website tour</a>-->
            <?= getLastLogin($CI->user_ldamp['employeeid']) ?>
        </div>
    </div>
</div>

<script>
//script for tree view
$.fn.extend({
    treed: function (o) {
      
      var openedClass = ' fa fa-folder fa-b ';
      var closedClass = ' fa fa-folder fa-b ';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='fa fa-folder" + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();

$('#tree1 .branch').each(function(){

var icon = $(this).children('i:first');
icon.toggleClass('fa fa-folder fa-b fa fa-folder fa-b');
//$(this).children().children().toggle();
$(".list-group-item.branch").css("color","#337ab7");
});

</script>
<style>
<!-- Tree view code  -->

</style>