<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/bootstrap.min.js"></script>


<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/jquery.validate.min.js"></script>
<!--<script type="text/javascript" src="--><?//= base_url() . 'assets/' ?><!--js/additional-methods.min.js"></script>-->


<script type="text/javascript" src="<?= base_url() . 'assets/' ?>js/main.js"></script>
<script src="https://use.fontawesome.com/0fda1d5bfd.js"></script>
<script type="text/javascript"
        src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script type="text/javascript"
        src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
</script>

<script type="text/javascript">
    $(function (e) {
        $('.search-panel .dropdown-menu').find('a').click(function (e) {
            e.preventDefault();
            var param = $(this).attr("href").replace("#", "");
            var concept = $(this).text();
            $('.search-panel span#search_concept').text(concept);
            $('.input-group #search_param').val(param);
        });
    });
</script>