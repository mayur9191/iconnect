<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <?php include APPPATH . "views/includs/hedder_code.php" ?>
<!-- light box code start -->
<style>
body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}
</style>
<!-- light box code end   -->
	
</head>

<body style="/*background-color: #dff0d8;*/">
<?php include APPPATH . "views/includs/top_navbar.php" ?>


<div class="container-fluid main">
<div class="row">
    <div class="col-md-12">

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
            <?php include APPPATH . "views/module/profile/common/left_box.php" ?>
        </div>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox">
	   
		 
            
			<?php
			
			if ($logindata['role'] == 1) {
				
				?>
				 <div class="three-search">
                <form action="<?= base_url() . 'profile/announcement' ?>" enctype="multipart/form-data" method="post" id="wallpostForm">
					<div class="form-group" style="display:none;">
                        <textarea rows="4" class="form-control walltext" id="editor" placeholder="Enter Announcement"
                                  name="announcement_text"></textarea>

                    </div>
					 <div class="form-group">
                        <input type="file" name="attachemnet"  />

                    </div>
                    <div class="form-group">
                        <input type="submit" value="post" class="btn btn-sm btn-primary pull-right"/>
                    </div>
                </form>


                <!--  <input type="text" class="form-control" placeholder="what's on your mind"/>-->
            </div>
				<?php
				
			}
			
			?>
			
         
            <div class="clear"></div>
			<div class="iconnect-ps">
				<a href="#">
					<h5 class="iconnect-psh5">
						<b class="ps-tag" style="padding-left: 8px;">Announcements</b> 
						<!--<span class="glyphicon glyphicon-globe navdown-list22"></span>-->
					</h5>
				</a>
			</div>


            <?php
            //            echo '<pre>';
            //            print_r($user_wall);
            //            echo '</pre>';
            ?>

            <ul class="nopad list-group" style="list-style:none">
                <?php
                if (count($announcements) > 0) {
                    foreach ($announcements AS $witem) {
                        ?>
                        <li class="wallitem_<?= $witem->an_id ?> nopad list-group-item" >

                            <?php
                            //                            echo '<pre>';
                            //                            print_r($witem);
                            //                            echo '</pre>';
                            ?>

                            <div class="row">
                              <!--  <div class="col-xs-1 col-md-1 nopad">
                                    <img src="<?= base_url() . 'assets/' ?>images/50.png" alt="client-user"
                                         clas="img-responsive"/>
                                </div> -->
                                <div class="col-xs-12 col-md-12 nopad">
                                    <div class="ptag1" style="font-size: 10px; font-weight: bold; display:none;">
                                        <span style="color: #1c6b94; display:none">
                                            <?= ucfirst($witem->u_name) ?>
                                        </span>


                                        <?php
                                        if ($logindata['user_id'] == $witem->an_created_by) {
                                            ?>
                                            <div class="btn-group pull-right " style="display:none">
                                                <button title="Edit" type="button" data-toggle="modal"
                                                        data-target="#wallmodel_<?= $witem->an_id ?>"
                                                        class="btn btn-primary btn-xs">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button type="button" title="Remove" wallid="<?= $witem->an_id ?>"
                                                        class="btn btn-primary btn-xs delete_wall_item wall_item_<?= $witem->an_id ?>">
                                                    <i class="fa fa-remove"></i>
                                                </button>

                                            </div>


                                            <div id="wallmodel_<?= $witem->an_id ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;</button>
                                                        </div>

                                                        <div class="modal-body">
                                                            <form method="post"
                                                                  action="<?= base_url() . 'profile/wall_update' ?>">
                                                                <input type="hidden" name="edit_wall_id"
                                                                       value="<?= $witem->an_id ?>"/>
                                                                <div class="form-group">
                                                                    <textarea style="font-weight: normal" rows="3"
                                                                              cols="" class="form-control"
                                                                              name="edit_wall_text"><?= $witem->an_text ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="submit" class="btn btn-primary"
                                                                           value="Update Now"/>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>


                                    </div>
                                   

                                    <div class="post" style="text-align:justify;">
                                        <p style="text-align:justify;"><?= stripcslashes($witem->an_text) ?></p>
                                    </div>
								<?php if($witem->an_file !=''){ ?>
									<div class="" style="text-align:center;">
										<img src="<?= base_url().'uploads/announcement/'.$witem->an_file;?>" class="nopad" style="max-width:100%;max-height:408px;" onclick="openModal();currentSlide(1)" />
										
                                    </div>
								<?php } ?>
                                </div>
                            </div>
                        </li>
                        <?php
						break;
                    }
                }
                ?>


            </ul>

			
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
			
			<div class="clear"></div>
			<div class="iconnect-ps">
			<a href="#">
				<h5 class="iconnect-psh5">
					<b class="ps-tag" style="padding-left: 8px;">Events</b> 
				</h5>
			</a>
		</div>
		
		<?php
		 if (count($prifile_slides) > 0) {
			 ?>
			 <div class="row">


                <div id="profile_sliders" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php
                        if (count($prifile_slides) > 0) {
                            $ii = 0;
                            foreach ($prifile_slides AS $wish_item) {
                                if ($ii == 0) {
                                    $active = 'active';
                                } else {
                                    $active = '';
                                }

                                ?>
                                <li data-target="#profile_sliders" data-slide-to="<?= $ii ?>"
                                    class="<?= $active ?>"></li>
                                <?php
                                $ii++;
                            }
                        }
                        ?>

                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox" >

                        <?php
                        if (count($prifile_slides) > 0) {
                            $iim = 0;
                            foreach ($prifile_slides AS $wish_item) {
                                if ($iim == 0) {
                                    $active = 'active';
                                } else {
                                    $active = '';
                                }


                                ?>
                                <div class="item <?= $active ?>">
                                    <img src="<?= base_url() . 'uploads/sliders/' . $wish_item->sl_image ?>"
                                         alt="Birthday Wishes">
                                </div>

                                <?php
                                $iim++;
                            }
                        }
                        ?>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#profile_sliders" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#profile_sliders" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>


            </div>
			 <?php
		 }
		?>

			
			</div>
			
			
			<div class="col-md-6 col-sm-6 col-xs-12  videoiframe">
				<div class="iconnect-ps">
					<a href="#">
						<h5 class="iconnect-psh5">
							<b class="ps-tag" style="padding-left: 8px;">Videos</b> 
						</h5>
					</a>
				</div>
				<iframe style="height:140px;width:100%;" class="carousel slide" src="https://www.youtube.com/embed/qS3CtSX8Eck" frameborder="0" allowfullscreen></iframe>
				</div>
			
			
			
			
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
            <?php include APPPATH . "views/module/profile/common/right_box.php" ?>
        </div>

    </div>
</div>

<!--<div class="row">
    <a href="#" class="nextpage-arrow"><span class="glyphicon glyphicon-arrow-right"></span></a>
</div>-->
</div>



<?php include APPPATH . "views/includs/common_scripts.php" ?>

<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<!--<script type="text/javascript" src="--><? //= base_url() . 'assets/' ?><!--croppic/croppic.min.js"></script>-->



<div id="myModal" class="modal middle-slid" style="display: none;margin-top:80px;">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <img src="<?= base_url().'uploads/announcement/'.$witem->an_file;?>" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>
  </div>
</div>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

<script type="text/javascript">

CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;

};
CKEDITOR.replace('editor');


    $(function () {
		
		//$('#editor').ckeditor();
		
	
        $("#likeId").on("click", function () {
            var birthdayVal = $(this).attr("data-birthdays");
            var currntUid = $(this).attr("data-current");
            $.post('<?php echo base_url();?>Profile/birthdayLike', {
                    birthday_user_ids: birthdayVal,
                    like_user_id: currntUid
                },
                function (rps) {
                    console.log(rps);
                    $("#likeId").html('Liked');
                    $("#likeId").removeAttr("id");
                });
        });


        $("#wallpostForm").validate({
            rules: {
                walltext: {
                    required: true
//                    email: true
                }
            },
            messages: {
                walltext: {
                    required: " is required"
//                    email: "Enter Valid Email"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $(".changedp").validate({
            rules: {
                image: {
                    required: true
//                    email: true
                }
            },
            messages: {
                image: {
                    required: " is required"
//                    email: "Enter Valid Email"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $('.delete_wall_item').on('click', function () {
            console.log($(this).attr('wallid'))

            var wallid = $(this).attr('wallid');
            var wallcalss = $(this).attr('wallid');

            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'ajax/wallitemremove' ?>",
                data: {wallid: wallid},
                cache: false,
                success: function (data) {
                    console.log(data);

                    if (data == 1) {
                        $('.wallitem_' + wallid).slideUp();
                    }

//                    $("#resultarea").text(data);
                }
            });


        })

        $(".fileUpload").on('change', function () {
            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof(FileReader) != "undefined") {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "img-responsive"
                            }).appendTo(image_holder);
                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Pls select only images");
            }
        });


    });


</script>
<?php include APPPATH . "views/includs/footer.php" ?>
</body>
</html>
