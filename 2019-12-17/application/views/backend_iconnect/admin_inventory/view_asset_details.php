<div class="main-content" data-key="<?php echo $asset_data->asset_id; ?>" data-key-receive="<?php echo $asset_data->asset_id.'_'.$assign_asset_id; ?>" id="add_more_div_<?php echo $asset_data->asset_id; ?>">
	<div class="main-content-inner">
		
		<div class="page-content" style="border:1px solid red;margin-top:5px;">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<span class="pull-right remove_ajx_asset" style="margin-right:20px;margin-top: 3px;" title="Remove Asset" data-key="<?php echo $asset_data->asset_id; ?>" onclick="remove_option('add_more_div_<?php echo $asset_data->asset_id; ?>')" ><i class="ace-icon fa fa-times-circle bigger-250"></i></span>
						
						<span class="pull-right return_ajx_asset" style="margin-right:20px;margin-top: 3px;" title="Return Asset" data-key="<?php echo $asset_data->asset_id.''.$assign_asset_id; ?>" onclick="remove_option_receive('add_more_div_<?php echo $asset_data->asset_id; ?>')" ><i class="ace-icon fa fa-share-square bigger-250"></i></span>
						
					</div>
					
					<div>
						
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type" readonly="readonly">
										<option value="">Please select</option>
										<?php foreach(inv_admin_asset() as $adminasset){
										if($asset_data->asset_type == $adminasset->asset_type_name){
											echo '<option value="'.$adminasset->asset_type_name.'" data-shortCode="'.$adminasset->asset_short_code.'" selected>'.$adminasset->asset_type_name.'</option>';
										}else{
											echo '<option value="'.$adminasset->asset_type_name.'" data-shortCode="'.$adminasset->asset_short_code.'">'.$adminasset->asset_type_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="service_provider">Service Provider:</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->service_provider; ?>" name="service_provider" id="service_provider" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="connection_no">Connection number :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->connection_no; ?>" name="connection_no" id="connection_no" readonly="readonly"/>
								</div>
								
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_date">Procured Date :</label>
									<input type="text" required="required" value="<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>" class="form-control" name="procured_date" id="procured_date" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user">User :</label>
									<input type="text" required="required" value="<?php echo $asset_data->user; ?>" class="form-control" name="user" id="user" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="email">User Email :</label>
									<input type="text" required="required" value="<?php echo $asset_data->email; ?>" class="form-control" name="email" id="email" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_company">User Company :</label>
									<input type="text" required="required" value="<?php echo $asset_data->user_company; ?>" class="form-control" name="user_company" id="user_company" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_location">User Location :</label>
									<input type="text" required="required" value="<?php echo $asset_data->user_location; ?>" class="form-control" name="user_location" id="user_location" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="issued_on">Issued On :</label>
									<input type="text" required="required" value="<?php echo date('d-m-Y',strtotime($asset_data->issued_on)); ?>" class="form-control" name="issued_on" id="issued_on" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="purchased_company">Purchased company :</label>
									<select class="form-control" required="required" name="purchased_company" id="purchased_company" readonly="readonly">
									<option value="">Select Company</option>
									<?php foreach(inv_admin_company() as $companyRecord){
										if($asset_data->purchased_company == $companyRecord->company_id){
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'" selected>'.$companyRecord->company_name.'</option>';
										}else{
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
							
								
								<!-- new fields -->
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_location">Asset Location :</label>
									<input type="text" required="required" value="<?php echo $asset_data->asset_location; ?>" class="form-control" name="asset_location" id="asset_location" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="current_rental_plan">Current Plan :</label>
									<input type="text" required="required" value="<?php echo $asset_data->current_rental_plan; ?>" class="form-control" name="current_rental_plan" id="current_rental_plan" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="rental_amount">Rental Amount :</label>
									<input type="text" required="required" value="<?php echo $asset_data->rental_amount; ?>" class="form-control" name="rental_amount" id="rental_amount" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="monthly_limit">Monthly Limit :</label>
									<input type="text" required="required" value="<?php echo $asset_data->monthly_limit; ?>" class="form-control" name="monthly_limit" id="monthly_limit" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="sim_no">Sim Number :</label>
									<input type="text" required="required" value="<?php echo $asset_data->sim_no; ?>" class="form-control" name="sim_no" id="sim_no" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="account_no">Account Number :</label>
									<input type="text" required="required" value="<?php echo $asset_data->account_no; ?>" class="form-control" name="account_no" id="account_no" readonly="readonly"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="availablity_status">Availablity status :</label>
									<select class="form-control" required="required" name="availablity_status" id="availablity_status" readonly="readonly">
										<option value="">Please select</option>
										<option value="stock" <?php if($asset_data->availablity_status=='stock'){ ?> selected <?php } ?>>Stock</option>
										<option value="assign" <?php if($asset_data->availablity_status=='assign'){ ?> selected <?php } ?>>Assign</option>
										<option value="Destroyed" <?php if($asset_data->availablity_status=='Destroyed'){ ?> selected <?php } ?>>Destroyed</option>
									</select>
								</div>
								
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
								<label data-lblname="ownership_history">History :</label>
								<textarea required="required" class="form-control" name="ownership_history" rows="4" id="ownership_history" readonly="readonly">
								<?php echo $asset_data->ownership_history; ?>
								</textarea>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
								<label data-lblname="assign_remark">Assign Remark :</label>
								<textarea required="required" class="form-control" name="assign_remark" rows="4" id="assign_remark" readonly="readonly">
								<?php echo $asset_data->assign_remark; ?>
								</textarea>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
								<label data-lblname="return_remark">Return Remark :</label>
								<textarea required="required" class="form-control" name="return_remark" rows="4" id="return_remark" readonly="readonly">
								<?php echo $asset_data->return_remark; ?>
								</textarea>
								</div>
								<?php foreach($return_history as $history){
										//echo "User "$history->user;
										//echo $history->return_date;
										echo "<b>User</b> " .$history->user. " <b>Return date</b> " .$history->return_date; echo "<br/>";

								}?>
							</div>
								
							
							
							
							
							
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

