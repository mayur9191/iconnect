<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Edit Asset</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Edit Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Edit Asset
						<a href="<?php echo base_url().'admin/admin_asset_list';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'admin/editAdminAsset/'.$asset_data->id; ?>" method="POST" id="edit_admin_asset" name="myform" novalidate>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type">
										<option value="">Please select</option>
										<?php foreach(inv_admin_asset() as $adminasset){
										if($asset_data->asset_type == $adminasset->asset_type_name){
											echo '<option value="'.$adminasset->asset_type_name.'" data-shortCode="'.$adminasset->short_code.'" selected>'.$adminasset->asset_type_name.'</option>';
										}else{
											echo '<option value="'.$adminasset->asset_type_name.'" data-shortCode="'.$adminasset->short_code.'">'.$adminasset->asset_type_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="service_provider">Service Provider<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->service_provider; ?>" name="service_provider" id="service_provider" />
								</div>-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="service_provider">Service Provider<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="service_provider" id="service_provider">
										<option value="">Please select</option>
										<?php foreach(inv_admin_serviceProvider() as $serviceProvider){
										if($asset_data->service_provider == $serviceProvider->name){
											echo '<option value="'.$serviceProvider->name.'" data-shortCode="'.$serviceProvider->name.'" selected>'.$serviceProvider->name.'</option>';
										}else{
											echo '<option value="'.$serviceProvider->name.'" data-shortCode="'.$serviceProvider->name.'">'.$serviceProvider->name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="connection_no">Connection number<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->connection_no; ?>" name="connection_no" id="connection_no"/>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user">User <span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->user; ?>" class="form-control" name="user" id="user"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_code">Emp Code<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->emp_code; ?>" class="form-control" name="emp_code" id="emp_code" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="email">Email<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->email; ?>" class="form-control" name="email" id="email" />
								</div>
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_location">User Location <span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->user_location; ?>" class="form-control" name="user_location" id="user_location" />
								</div>	-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_location">User Location<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="user_location" id="user_location">
										<option value="">Please select</option>
										<?php foreach(inv_admin_UserLocation() as $userLocation){
										if($asset_data->user_location == $userLocation->location_name){
											echo '<option value="'.$userLocation->location_name.'" data-shortCode="'.$userLocation->location_name.'" selected>'.$userLocation->location_name.'</option>';
										}else{
											echo '<option value="'.$userLocation->location_name.'" data-shortCode="'.$userLocation->location_name.'">'.$userLocation->location_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_company">User Company<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->user_company; ?>" class="form-control" name="user_company" id="user_company"/>
									
									
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">User LOB<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="user_lob" id="user_lob" />
									<option value="">Select User LOB</option>
									<?php foreach(inv_admin_LOB() as $lob){
										if($asset_data->user_lob_id == $lob->lob_id){
											echo '<option value="'.$lob->lob_id.'" data-shortCode="'.$lob->lob_id.'" selected>'.$lob->lob_name.'</option>';
										}else{
											echo '<option value="'.$lob->lob_id.'" data-shortCode="'.$lob->lob_id.'">'.$lob->lob_name.'</option>';
										}
									?>
									<?php } ?>
									
									
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_date">Procured Date<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>" class="form-control" name="procured_date" id="procured_date" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="purchased_company">Purchased company<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="purchased_company" id="purchased_company" >
									<option value="">Select Company</option>
									<?php foreach(inv_admin_company() as $companyRecord){
										if($asset_data->purchased_company == $companyRecord->company_id){
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'" selected>'.$companyRecord->company_name.'</option>';
										}else{
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="issued_on">Issued On<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo date('d-m-Y',strtotime($asset_data->issued_on)); ?>" class="form-control" name="issued_on" id="issued_on" />
								</div>
								
								
								<!-- new fields -->
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_location">Asset Location<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->asset_location; ?>" class="form-control" name="asset_location" id="asset_location" />
								</div>-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_location">Asset Location<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_location" id="asset_location" >
									<option value="">Select Location</option>
									<?php foreach(inv_admin_AssetLocation() as $assetLocation){
										if($asset_data->asset_location == $assetLocation->asset_loction){
											echo '<option value="'.$assetLocation->asset_loction.'" data-shortCode="'.$assetLocation->asset_loction.'" selected>'.$assetLocation->asset_loction.'</option>';
										}else{
											echo '<option value="'.$assetLocation->asset_loction.'" data-shortCode="'.$assetLocation->asset_loction.'">'.$assetLocation->asset_loction.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="current_rental_plan">Current Plan<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->current_rental_plan; ?>" class="form-control" name="current_rental_plan" id="current_rental_plan" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="rental_amount">Rental Amount<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->rental_amount; ?>" class="form-control" name="rental_amount" id="rental_amount" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="monthly_limit">Monthly Limit<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->monthly_limit; ?>" class="form-control" name="monthly_limit" id="monthly_limit" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="sim_no">Sim Number<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->sim_no; ?>" class="form-control" name="sim_no" id="sim_no" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="account_no">Account Number<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->account_no; ?>" class="form-control" name="account_no" id="account_no" />
								</div>
								
								
								
								
							</div>
							
							
							
							
							
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#procured_date').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>');
	$('#issued_on').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->issued_on)); ?>');
	
	$("#edit_admin_asset").validate({
		rules:{
			asset_type: {required:true},
			service_provider: {required:true},
			connection_no: {required:true},
			user: {required:true},
			emp_code: {required:true},
			email: {required:true,email:true},
			user_location: {required:true},
			user_company: {required:true},
			user_lob: {required:true},
			procured_date: {required:true},
			purchased_company: {required:true},
			issued_on: {required:true},
			asset_location: {required:true},
			current_rental_plan: {required:true},
			rental_amount: {required:true},
			monthly_limit: {required:true},
			sim_no: {required:true},
			account_no: {required:true}
			
		},
		messages: {
			"asset_type": { required: "Please select one asset type" },
			"service_provider": { required: "Please enter service provider" },
			"connection_no": { required: "Please enter connection no" },
			"user": { required: "Please enter user" },
			"emp_code": { required: "Please enter emp code" },
			"email": { required: "Please enter email" },
			"user_location": { required: "Please enter User location" },
			"user_company": { required: "Please enter User company" },
			"user_lob": { required: "Please enter User LOB" },
			"procured_date": { required: "Please enter procured date" },
			"purchased_company": { required: "Please select purchased company" },
			"issued_on": { required: "Please enter issued on date" },
			"asset_location": { required: "Please select asset location" },
			"current_rental_plan": { required: "Please enter current rental plan" },
			"rental_amount": { required: "Please enter rental amount" },
			"monthly_limit": { required: "Please enter monthly limit" },
			"sim_no": { required: "Please enter sim no" },
			"account_no": { required: "Please enter account no" }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
	
	
});








//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>