
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Admin Assets</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">	
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Assets</h3>
					<?php if($this->session->flashdata('msg')): ?>
						<div class="alert alert-success alert-dismissable">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?> 
						</div>
					<?php endif; ?>
					<?php if($this->session->flashdata('error_msg')): ?>
						<div class="alert alert-danger alert-dismissable">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong> <?php echo $this->session->flashdata('error_msg'); ?> 
						</div>
					<?php endif; ?> 
					
					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					
					<div>
					
					<form action="<?= base_url().'admin/getLOB_MIS' ?>" method="POST" >
						<table id="myTable" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>LOB</th>
									<th>HOD</th>
									<th>Download MIS</th>
								
								</tr>
							</thead>

							<tbody>
							
							
								<tr>
									<td><?php echo "Group"; ?></td>
									<td><?php echo "Parag.Sawant@skeiron.com"; ?></td>
									<td>
									<a href="<?php echo base_url().'admin/getLOB_MIS/5/Group'; ?>">
												Download MIS
										</a>
									</td>
									
								</tr>
							

							</tbody>
						</table>
					</form>
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- Modal 2 start -->
  <div class="modal fade" id="myModal_viewasset" role="dialog">
	<div class="modal-dialog" style="width:80%;">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Asset Details</h4>
		  </div>
		  <div class="modal-body" id="myModal_viewasset1">
		  
		  </div>
		  <div class="modal-footer">
		  </div>
		</div>
	</div>
  </div>
<!-- Modal 2 end -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!--datatable js--->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">



function view_record(recd_id){
	$('#myModal_viewasset1').html('');
	console.log('record Id '+recd_id);
	$("#myModal_viewasset").modal('show');
	$.ajax({
		url: "<?php echo base_url() ?>admin/view_asset_details/"+recd_id,
		type:"post",
		//data:{"assetId":recd_id},
		success:function(r){
			$('#myModal_viewasset1').html(r);
			$(".pull-right").attr("style","display:none");
			$(".page-content").css('border','none');
			$('#myModal_viewasset').modal('show');
		},error:function(){}
	});
	 return false;
}

function edit_record(recd_id){
	$('#myModal').html('');
	console.log('record Id '+recd_id);
	$("#myModal").modal('show');
	 $.ajax({
		 url: "<?php echo base_url() ?>admin/edit_category",
		 type:"post",
		 data:{"recd_id":recd_id},
		  success:function(r){
			$('#myModal').html(r);
			$('#myModal').modal('show');
		  },error:function(){
		  }
	 });
	 return false;
}

function add_record(){
	$('#myModal').html('');
	$("#myModal").modal('show');
	 $.ajax({
		 url: "<?php echo base_url() ?>admin/add_category",
		 type:"post",
		  success:function(r){
			$('#myModal').html(r);
			$('#myModal').modal('show');
		
		  },error:function(){
			  
		  }
	 });
	 return false;
}



jQuery(function($) {
	
	$('#myTable').DataTable({
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]]
	});
	
	setTimeout(function(){
		$(".alert").remove();
	},5000);
	
	$(".red").on("click",function(){
		console.log($(this).attr("data-val"));
		recd_id = $(this).attr("data-val")
		var status_val = $(this).find('i').attr("class");
		if(status_val == 'ace-icon fa fa-toggle-off bigger-130'){
			$(this).find('i').removeClass("ace-icon fa fa-toggle-off bigger-130");
			$(this).find('i').addClass("ace-icon fa fa-toggle-on bigger-130");
			status = 'deactive';
			$("#stat_"+recd_id).text('De-Active');
		}
		if(status_val == 'ace-icon fa fa-toggle-on bigger-130'){
			$(this).find('i').removeClass("ace-icon fa fa-toggle-on bigger-130");
			$(this).find('i').addClass("ace-icon fa fa-toggle-off bigger-130");
			status = 'active';
			$("#stat_"+recd_id).text('Active');
		}
		 $.ajax({
			 url: "<?php echo base_url() ?>admin/status_category",
			 type:"post",
			 data:{"recd_id":recd_id,"status":status},
			  success:function(r){
					$("#success_msg").html(r);
					$(".alert.alert-success.alert-dismissable.a").show();
					setTimeout(function(){ 
						$(".alert.alert-success.alert-dismissable.a").hide();
					}, 3000);
			  },error:function(){
			  }
		 });
		 return false;
	});
	
	//initiate dataTables plugin
	var myTable = 
	$('#dynamic-table')
	//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
	.DataTable( {
		bAutoWidth: false,
		"aoColumns": [
		  { "bSortable": false },
		  null, null,null, null, null,
		  { "bSortable": false }
		],
		"aaSorting": [],
		
		
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		//"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50


		select: {
			style: 'multi'
		}
	} );

	
	
	$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
	
	new $.fn.dataTable.Buttons( myTable, {
		buttons: [
		  {
			"extend": "colvis",
			"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
			"className": "btn btn-white btn-primary btn-bold",
			columns: ':not(:first):not(:last)'
		  },
		  {
			"extend": "copy",
			"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "csv",
			"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "excel",
			"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "pdf",
			"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "print",
			"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
			"className": "btn btn-white btn-primary btn-bold",
			autoPrint: false,
			message: 'This print was produced using the Print button for DataTables'
		  }		  
		]
	} );
	myTable.buttons().container().appendTo( $('.tableTools-container') );
	
	//style the message box
	var defaultCopyAction = myTable.button(1).action();
	myTable.button(1).action(function (e, dt, button, config) {
		defaultCopyAction(e, dt, button, config);
		$('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
	});
	
	
	var defaultColvisAction = myTable.button(0).action();
	myTable.button(0).action(function (e, dt, button, config) {
		
		defaultColvisAction(e, dt, button, config);
		
		
		if($('.dt-button-collection > .dropdown-menu').length == 0) {
			$('.dt-button-collection')
			.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
			.find('a').attr('href', '#').wrap("<li />")
		}
		$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
	});

	////

	setTimeout(function() {
		$($('.tableTools-container')).find('a.dt-button').each(function() {
			var div = $(this).find(' > div').first();
			if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
			else $(this).tooltip({container: 'body', title: $(this).text()});
		});
	}, 500);
	
	
	
	
	
	myTable.on( 'select', function ( e, dt, type, index ) {
		if ( type === 'row' ) {
			$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
		}
	} );
	myTable.on( 'deselect', function ( e, dt, type, index ) {
		if ( type === 'row' ) {
			$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
		}
	} );




	/////////////////////////////////
	//table checkboxes
	$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
	
	//select/deselect all rows according to table header checkbox
	$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
		var th_checked = this.checked;//checkbox inside "TH" table header
		
		$('#dynamic-table').find('tbody > tr').each(function(){
			var row = this;
			if(th_checked) myTable.row(row).select();
			else  myTable.row(row).deselect();
		});
	});
	
	//select/deselect a row when the checkbox is checked/unchecked
	$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
		var row = $(this).closest('tr').get(0);
		if(this.checked) myTable.row(row).deselect();
		else myTable.row(row).select();
	});



	$(document).on('click', '#dynamic-table .dropdown-toggle', function(e) {
		e.stopImmediatePropagation();
		e.stopPropagation();
		e.preventDefault();
	});
	
	
	
	//And for the first simple table, which doesn't have TableTools or dataTables
	//select/deselect all rows according to table header checkbox
	var active_class = 'active';
	$('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
		var th_checked = this.checked;//checkbox inside "TH" table header
		
		$(this).closest('table').find('tbody > tr').each(function(){
			var row = this;
			if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
			else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
		});
	});
	
	//select/deselect a row when the checkbox is checked/unchecked
	$('#simple-table').on('click', 'td input[type=checkbox]' , function(){
		var $row = $(this).closest('tr');
		if($row.is('.detail-row ')) return;
		if(this.checked) $row.addClass(active_class);
		else $row.removeClass(active_class);
	});

	

	/********************************/
	//add tooltip for small view action buttons in dropdown menu
	$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
	
	//tooltip placement on right or left
	function tooltip_placement(context, source) {
		var $source = $(source);
		var $parent = $source.closest('table')
		var off1 = $parent.offset();
		var w1 = $parent.width();

		var off2 = $source.offset();
		//var w2 = $source.width();

		if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
		return 'left';
	}
	
	
	
	
	/***************/
	$('.show-details-btn').on('click', function(e) {
		e.preventDefault();
		$(this).closest('tr').next().toggleClass('open');
		$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
	});
	/***************/
	
	
	
	
	
	
})
</script>