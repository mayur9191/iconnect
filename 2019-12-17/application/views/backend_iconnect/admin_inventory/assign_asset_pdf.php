<?php $backendURL = base_url().'assets/backend/'; ?>

	<head>
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/bootstrap.min.css" />
		
		<style>
		/* html *
		{
		   font-size: 12px;
		}*/
		.fontSmall{
			font-size:9px;
		}
		</style>
	</head>
	<body>
	<!-- table asset generation code start -->
	<?php if($this->session->flashdata('msg')): ?>
		<div class="alert alert-success alert-dismissable">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?> 
		</div>
	<?php endif; ?>
	<?php if($this->session->flashdata('error_msg')): ?>
		<div class="alert alert-danger alert-dismissable">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Danger!</strong> <?php echo $this->session->flashdata('error_msg'); ?> 
		</div>
	<?php endif; ?> 
	<?php //var_dump($edit_assign_data); ?>
	<div class="container">
					<h3><center id="assetFrmName">Asset Allocation</center></h3>
						<table border=1>
							<tr>
								<td><b>Asset Type: </b></td> 
								<td><b>Service Provider: </b></td> 
								<td><b>Connection number: </b></td> 
								<td><b>Current rental plan: </b></td> 
								<td><b>Rental Amount: </b></td> 
								<td><b>Monthly Limit: </b></td> 
							</tr>
							<tr>
								<td><?php echo $edit_assign_data->asset_type; ?></td> 
								<td><?php echo $edit_assign_data->service_provider; ?></td> 
								<td><?php echo $edit_assign_data->connection_no; ?></td> 
								<td><?php echo $edit_assign_data->current_rental_plan; ?></td> 
								<td><?php echo $edit_assign_data->rental_amount; ?></td> 
								<td><?php echo $edit_assign_data->monthly_limit; ?></td> 
							</tr>
						</table>
				</div>		
	<!-- table asset generation code end -->
	
	<!-- email template code start -->
	
	
	<!-- email template code end -->
	