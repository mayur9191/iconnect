<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Edit Asset</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Edit Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Edit Asset
						<a href="<?php echo base_url().'admin/manage_asset_admin';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'admin/edit_Admin_Asset/'.$asset_data->id; ?>" method="POST" id="edit_admin_asset" name="myform" novalidate>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type">
										<option value="">Please select</option>
										<?php foreach(inv_admin_asset() as $adminasset){
										if($asset_data->asset_type == $adminasset->asset_type_name){
											echo '<option value="'.$adminasset->asset_type_name.'" data-shortCode="'.$adminasset->asset_short_code.'" selected>'.$adminasset->asset_type_name.'</option>';
										}else{
											echo '<option value="'.$adminasset->asset_type_name.'" data-shortCode="'.$adminasset->asset_short_code.'">'.$adminasset->asset_type_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="service_provider">Service Provider<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="service_provider" id="service_provider">
										<option value="">Please select</option>
										<?php foreach(inv_admin_serviceProvider() as $serviceProvider){
										if($asset_data->service_provider == $serviceProvider->name){
											echo '<option value="'.$serviceProvider->name.'" data-shortCode="'.$serviceProvider->name.'" selected>'.$serviceProvider->name.'</option>';
										}else{
											echo '<option value="'.$serviceProvider->name.'" data-shortCode="'.$serviceProvider->name.'">'.$serviceProvider->name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="service_provider">Service Provider<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->service_provider; ?>" name="service_provider" id="service_provider" />
								</div>-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="connection_no">Connection number<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->connection_no; ?>" name="connection_no" id="connection_no"/>
								</div>
								
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_date">Procured Date<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>" class="form-control" name="procured_date" id="procured_date" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="purchased_lob">Purchased LOB<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="purchased_lob" id="purchased_lob" >
									<option value="">Select Company</option>
									<?php foreach(inv_admin_LOB() as $lobRecord){
										if($asset_data->purchased_LOB_id == $lobRecord->lob_id){
											echo '<option value="'.$lobRecord->lob_id.'" selected>'.$lobRecord->lob_name.'</option>';
										}else{
											echo '<option value="'.$lobRecord->lob_id.'">'.$lobRecord->lob_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="purchased_company">Purchased company<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="purchased_company" id="purchased_company" >
									<option value="">Select Company</option>
									<?php foreach(inv_admin_company() as $companyRecord){
										if($asset_data->purchased_company == $companyRecord->company_id){
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'" selected>'.$companyRecord->company_name.'</option>';
										}else{
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
							
								
								<!-- new fields -->
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_location">Asset Location<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->asset_location; ?>" class="form-control" name="asset_location" id="asset_location" />
								</div>-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_location">Asset Location<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_location" id="asset_location" >
									<option value="">Select Location</option>
									<?php foreach(inv_admin_AssetLocation() as $assetLocation){
										if($asset_data->asset_location == $assetLocation->asset_loction){
											echo '<option value="'.$assetLocation->asset_loction.'" data-shortCode="'.$assetLocation->asset_loction.'" selected>'.$assetLocation->asset_loction.'</option>';
										}else{
											echo '<option value="'.$assetLocation->asset_loction.'" data-shortCode="'.$assetLocation->asset_loction.'">'.$assetLocation->asset_loction.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="current_rental_plan">Current Plan<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->current_rental_plan; ?>" class="form-control" name="current_rental_plan" id="current_rental_plan" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="rental_amount">Rental Amount<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->rental_amount; ?>" class="form-control" name="rental_amount" id="rental_amount" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="monthly_limit">Monthly Limit<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->monthly_limit; ?>" class="form-control" name="monthly_limit" id="monthly_limit" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="sim_no">Sim Number<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->sim_no; ?>" class="form-control" name="sim_no" id="sim_no" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="account_no">Account Number<span class="mand">*</span> :</label>
									<input type="text" required="required" value="<?php echo $asset_data->account_no; ?>" class="form-control" name="account_no" id="account_no" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="availablity_status">Availablity status<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="availablity_status" id="availablity_status">
										<option value="">Please select</option>
										<option value="stock" <?php if($asset_data->availablity_status=='stock'){ ?> selected <?php } ?>>Stock</option>
										<option value="assign" <?php if($asset_data->availablity_status=='assign'){ ?> selected <?php } ?>>Assign</option>
										<option value="Destroyed" <?php if($asset_data->availablity_status=='Destroyed'){ ?> selected <?php } ?>>Destroyed</option>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="working_status">Working status<span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="working_status" id="working_status">
										<option value="">Please select</option>
										<option value="Deactivated" <?php if($asset_data->asset_is_active=='Deactivated'){ ?> selected <?php } ?>>Deactivated</option>
										<option value="Active" <?php if($asset_data->asset_is_active=='Active'){ ?> selected <?php } ?>>Active</option>
										<option value="safe custody" <?php if($asset_data->asset_is_active=='safe custody'){ ?> selected <?php } ?>>safe custody</option>
									</select>
								</div>
								
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
								<label data-lblname="ownership_history">History<span class="mand"></span> :</label>
								<textarea required="required" class="form-control" name="ownership_history" rows="4" id="ownership_history">
								<?php echo $asset_data->ownership_history; ?>
								</textarea>
							</div>-->
								
							</div>
							
							
							
							
							
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#procured_date').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>');
	
	$("#edit_admin_asset").validate({
		rules:{
			asset_type: {required:true},
			service_provider: {required:true},
			connection_no: {required:true},
			procured_date: {required:true},
			purchased_lob: {required:true},
			purchased_company: {required:true},
			asset_location: {required:true},
			current_rental_plan: {required:true},
			rental_amount: {required:true},
			monthly_limit: {required:true},
			sim_no: {required:true},
			account_no: {required:true},
			availablity_status: {required:true},
			working_status:{required:true}
			
		},
		messages: {
			"asset_type": { required: "Please select one asset type" },
			"service_provider": { required: "Please enter service provider" },
			"connection_no": { required: "Please enter connection no" },
			"procured_date": { required: "Please enter procured date" },
			"purchased_lob": {required: "Please select purchased LOB"},
			"purchased_company": { required: "Please select purchased company" },
			"asset_location": { required: "Please select asset location" },
			"current_rental_plan": { required: "Please enter current rental plan" },
			"rental_amount": { required: "Please enter rental amount" },
			"monthly_limit": { required: "Please enter monthly limit" },
			"sim_no": { required: "Please enter sim no" },
			"account_no": { required: "Please enter account no" },
			"availablity_status": { required: "Please select availablity status" },
			"working_status": { required: "Please select working status" }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
	
	
});








//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>