<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!--<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	-->
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	     table tbody tr td:nth-child(3) a.description .tooltiptext {
			display: none;
			width: 430px;
			background-color:#ccc
			color:black;
			text-align: left;
			border-radius: 6px;
			padding: 8px 0;
			position: absolute;
			z-index: 1;
			border:1px solid black;
			
			
		}
		table tbody tr td:nth-child(3) a.description:hover .tooltiptext {
			display: block;
			color:black;
			padding-left:12px;
			word-wrap: break-word;
		}
		table tbody tr td:nth-child(3) a.description{
			color:#333;
		}
		table tbody tr td:nth-child(3) a.description:hover{
			color:#337ab7;
		}
		span.tooltiptext ul li{
			list-style-type:none;
		} 
		.square {
		float: left;
		width: 10px;
		height: 10px;
		margin: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		}

		.Normal {
		  background: #006600;
		}
		.Low {
		  background: #e4af6d;
		}

		.Medium {
		  background: #ff6600;
		}

		.High {
		  background: #ff0000;
		}
		#dynamic-table{
			width:100% !important;
			font-size:12px !important;
		}
	</style>
</head>
<body>
<?php //var_dump(count($mis_dataMSC)); ?>
<a href="#" style="text-decoration:none;"><span style="background-color:#ffeb3b;color:#fff;padding: 4px;border-radius: 10px;" onclick="show_MVTS();">Mobile (VTS)</span></a>
<a href="#" style="text-decoration:none;"><span style="background-color:#4fa928;color:#fff;padding: 4px;border-radius: 10px;" onclick="show_MSC();">Mobile SIM Card</span></a>
<a href="#" style="text-decoration:none;"><span style="background-color:blueviolet;color:#fff;padding: 4px;border-radius: 10px;" onclick="show_TF();">Toll free</span></a>
<a href="#" style="text-decoration:none;"><span style="background-color:#f89406;color:#fff;padding: 4px;border-radius: 10px;" onclick="show_DC();">Data Card</span></a>
	<br/><br/>		
	<div class="col-md-12">
		<div  id="tab2success">
			<div class="row inbox">
				<div class="container-fluid col-md-12" style="background-color: white;">
			<!--<div class="page-header"></div>-->
			<?php $CI = & get_instance();  ?>
			<div class="col-md-12 col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Purchased LOB</th>
							<th>Purchased Company</th>
							<th>Asset Type</th>
							<th>Service Provider</th>
							<th>Connection number</th>
							<th>Procured date</th>
							<th>Current rental plan</th>
							<th>Rental amount</th>
							<th>Status</th>
							<th>User</th>	
							<th>User email</th>	
							<th>User Company</th>								
							
						</tr>
					</thead>

					<tbody>
					<?php $i=1; foreach($mis_data as $reportRecord){ 							
							
					?>
						<tr>
							<td><?= $i;?></td>
							<td><?php echo $reportRecord['purchased_LOB'] ; ?></td>
							<td><?php echo $reportRecord['purchased_company_name'] ; ?></td>
							<td><?php echo $reportRecord['asset_type'] ; ?></td>
							<td><?php echo $reportRecord['service_provider'] ; ?></td>
							<td><?php echo $reportRecord['connection_no'] ; ?></td>
							<td><?php echo $reportRecord['procured_date'] ; ?></td>
							<td><?php echo $reportRecord['current_rental_plan'] ; ?></td>
							<td><?php echo $reportRecord['rental_amount'] ; ?></td>
							<td><?php //echo $reportRecord['status'] ; 
								if($reportRecord['status'] == 'stock'){
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:green;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}else{
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:red;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}
										echo $asset_stock_type;
							
							?></td>
									<td><?php echo $reportRecord['user'] ; ?></td>
									<td><?php echo $reportRecord['email'] ; ?></td>	
									<td><?php echo $reportRecord['user_company'] ; ?></td>									
								
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
			</div>
			
			
			<div class="col-md-12 col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-tableMSC" style="display:none;" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Purchased LOB</th>
							<th>Purchased Company</th>
							<th>Asset Type</th>
							<th>Service Provider</th>
							<th>Connection number</th>
							<th>Procured date</th>
							<th>Current rental plan</th>
							<th>Rental amount</th>
							<th>Status</th>
							<th>User</th>	
							<th>User email</th>	
							<th>User Company</th>								
							
						</tr>
					</thead>

					<tbody>
					<?php $i=1; foreach($mis_dataMSC as $reportRecord){ 							
							
					?>
						<tr style="font-size:12px;">
							<td><?= $i;?></td>
							<td><?php echo $reportRecord['purchased_LOB'] ; ?></td>
							<td><?php echo $reportRecord['purchased_company_name'] ; ?></td>
							<td><?php echo $reportRecord['asset_type'] ; ?></td>
							<td><?php echo $reportRecord['service_provider'] ; ?></td>
							<td><?php echo $reportRecord['connection_no'] ; ?></td>
							<td><?php echo $reportRecord['procured_date'] ; ?></td>
							<td><?php echo $reportRecord['current_rental_plan'] ; ?></td>
							<td><?php echo $reportRecord['rental_amount'] ; ?></td>
							<td><?php //echo $reportRecord['status'] ; 
								if($reportRecord['status'] == 'stock'){
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:green;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}else{
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:red;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}
										echo $asset_stock_type;
							
							?></td>
									<td><?php echo $reportRecord['user'] ; ?></td>
									<td><?php echo $reportRecord['email'] ; ?></td>	
									<td><?php echo $reportRecord['user_company'] ; ?></td>									
								
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
			</div>
			
			<div class="col-md-12 col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-tableMVTS" style="display:none;" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Purchased LOB</th>
							<th>Purchased Company</th>
							<th>Asset Type</th>
							<th>Service Provider</th>
							<th>Connection number</th>
							<th>Procured date</th>
							<th>Current rental plan</th>
							<th>Rental amount</th>
							<th>Status</th>
							<th>User</th>	
							<th>User email</th>	
							<th>User Company</th>								
							
						</tr>
					</thead>

					<tbody style="font-size:12px;">
					<?php $i=1; foreach($mis_dataMVTS as $reportRecord){ 							
							
					?>
						<tr>
							<td><?= $i;?></td>
							<td><?php echo $reportRecord['purchased_LOB'] ; ?></td>
							<td><?php echo $reportRecord['purchased_company_name'] ; ?></td>
							<td><?php echo $reportRecord['asset_type'] ; ?></td>
							<td><?php echo $reportRecord['service_provider'] ; ?></td>
							<td><?php echo $reportRecord['connection_no'] ; ?></td>
							<td><?php echo $reportRecord['procured_date'] ; ?></td>
							<td><?php echo $reportRecord['current_rental_plan'] ; ?></td>
							<td><?php echo $reportRecord['rental_amount'] ; ?></td>
							<td><?php //echo $reportRecord['status'] ; 
								if($reportRecord['status'] == 'stock'){
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:green;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}else{
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:red;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}
										echo $asset_stock_type;
							
							?></td>
									<td><?php echo $reportRecord['user'] ; ?></td>
									<td><?php echo $reportRecord['email'] ; ?></td>	
									<td><?php echo $reportRecord['user_company'] ; ?></td>									
								
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
			</div>
			
			
			<div class="col-md-12 col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-tableTF" style="display:none;" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Purchased LOB</th>
							<th>Purchased Company</th>
							<th>Asset Type</th>
							<th>Service Provider</th>
							<th>Connection number</th>
							<th>Procured date</th>
							<th>Current rental plan</th>
							<th>Rental amount</th>
							<th>Status</th>
							<th>User</th>	
							<th>User email</th>	
							<th>User Company</th>								
							
						</tr>
					</thead>

					<tbody style="font-size:12px;">
					<?php $i=1; foreach($mis_dataTF as $reportRecord){ 							
							
					?>
						<tr>
							<td><?= $i;?></td>
							<td><?php echo $reportRecord['purchased_LOB'] ; ?></td>
							<td><?php echo $reportRecord['purchased_company_name'] ; ?></td>
							<td><?php echo $reportRecord['asset_type'] ; ?></td>
							<td><?php echo $reportRecord['service_provider'] ; ?></td>
							<td><?php echo $reportRecord['connection_no'] ; ?></td>
							<td><?php echo $reportRecord['procured_date'] ; ?></td>
							<td><?php echo $reportRecord['current_rental_plan'] ; ?></td>
							<td><?php echo $reportRecord['rental_amount'] ; ?></td>
							<td><?php //echo $reportRecord['status'] ; 
								if($reportRecord['status'] == 'stock'){
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:green;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}else{
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:red;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}
										echo $asset_stock_type;
							
							?></td>
									<td><?php echo $reportRecord['user'] ; ?></td>
									<td><?php echo $reportRecord['email'] ; ?></td>	
									<td><?php echo $reportRecord['user_company'] ; ?></td>									
								
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
			</div>
			
			
			
			<div class="col-md-12 col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-tableDC" style="display:none;" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Purchased LOB</th>
							<th>Purchased Company</th>
							<th>Asset Type</th>
							<th>Service Provider</th>
							<th>Connection number</th>
							<th>Procured date</th>
							<th>Current rental plan</th>
							<th>Rental amount</th>
							<th>Status</th>
							<th>User</th>	
							<th>User email</th>	
							<th>User Company</th>								
							
						</tr>
					</thead>

					<tbody style="font-size:12px;">
					<?php $i=1; foreach($mis_dataDC as $reportRecord){ 							
							
					?>
						<tr>
							<td><?= $i;?></td>
							<td><?php echo $reportRecord['purchased_LOB'] ; ?></td>
							<td><?php echo $reportRecord['purchased_company_name'] ; ?></td>
							<td><?php echo $reportRecord['asset_type'] ; ?></td>
							<td><?php echo $reportRecord['service_provider'] ; ?></td>
							<td><?php echo $reportRecord['connection_no'] ; ?></td>
							<td><?php echo $reportRecord['procured_date'] ; ?></td>
							<td><?php echo $reportRecord['current_rental_plan'] ; ?></td>
							<td><?php echo $reportRecord['rental_amount'] ; ?></td>
							<td><?php //echo $reportRecord['status'] ; 
								if($reportRecord['status'] == 'stock'){
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:green;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}else{
										$asset_stock_type = '&nbsp;&nbsp;<span style="background-color:red;color:#fff;">&nbsp;&nbsp;'.ucfirst($reportRecord['status']).'&nbsp;&nbsp;</span>&nbsp;&nbsp;';
										}
										echo $asset_stock_type;
							
							?></td>
									<td><?php echo $reportRecord['user'] ; ?></td>
									<td><?php echo $reportRecord['email'] ; ?></td>	
									<td><?php echo $reportRecord['user_company'] ; ?></td>									
								
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
			</div>
			
		</div> <!-- ./container -->
		
		</div><!--/.col-->		
	</div>
</div>
</body>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script>

var request_idaa = 0;
/* function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coor = "X coords: " + x + ", Y coords: " + y;
	$("#printTxt").css({'top':y,'left':x,'display':'block'}).show(); 
}
 */
 function show_MSC(){
		document.getElementById("dynamic-tableMSC").style.display = "block";
		
		document.getElementById("dynamic-table").style.display = "none";
		document.getElementById("dynamic-tableMVTS").style.display = "none";
		document.getElementById("dynamic-tableDC").style.display = "none";
		document.getElementById("dynamic-tableTF").style.display = "none";
		
	}
function show_MVTS(){
		document.getElementById("dynamic-tableMVTS").style.display = "block";
		
		document.getElementById("dynamic-table").style.display = "none";
		document.getElementById("dynamic-tableMSC").style.display = "none";
		document.getElementById("dynamic-tableDC").style.display = "none";
		document.getElementById("dynamic-tableTF").style.display = "none";
}
function show_TF(){
		document.getElementById("dynamic-tableTF").style.display = "block";
		
		document.getElementById("dynamic-table").style.display = "none";
		document.getElementById("dynamic-tableMVTS").style.display = "none";
		document.getElementById("dynamic-tableMSC").style.display = "none";
		document.getElementById("dynamic-tableDC").style.display = "none";
}
function show_DC(){
		document.getElementById("dynamic-tableDC").style.display = "block";
		
		document.getElementById("dynamic-table").style.display = "none";
		document.getElementById("dynamic-tableMVTS").style.display = "none";
		document.getElementById("dynamic-tableMSC").style.display = "none";
		document.getElementById("dynamic-tableTF").style.display = "none";
}

</script>
<script>

var request_idaa = 0;
/* function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coor = "X coords: " + x + ", Y coords: " + y;
	$("#printTxt").css({'top':y,'left':x,'display':'block'}).show(); 
}
 */
function get_technician(group_id,tech_id)
{
	        $.ajax({
				url : "<?php echo base_url() ?>"+ "helpdesk/get_technician",
				type:"POST",
				data: {group_id:group_id},
				success:function(response){
					var str = '<option value=""> --- Choose --- </option>';
					var obj  =  JSON.parse(response);
					var selectop = '';
					$(obj).each(function(index,value){
						var sel = (tech_id == obj[index].tech_id)? 'selected':'';
						str += '<option '+sel+' id="'+obj[index].tech_id+'" value="'+obj[index].tech_id+'">'+obj[index].tech_name+'</option>';	
					});
					
					$("#technicians").html(str);
					//document.getElementById(tech_id).selected = "true";
					//$("#tech_id").prop("selected",true);
				},
				error:function(response){
					alert("error occured"+response)
				}
			});
	
}

$(document).ready(function(){
	
	
	
	//reassing technician 
	$("#reassign_task").validate({
            rules: {
                groups: { required: true },
				technicians : { required : true }
            },
            messages: {
				groups: { required : "Please select group" },
                technician : { required : "Please select technician" }
            },
            submitHandler: function (form) {
				var group_id = $("#groups").val();
				var technician_id = $("#technicians").val();
				console.log('reassing groupId = '+ group_id + ' and technician id = '+technician_id);
				$.ajax({
					//url: "<?php echo base_url() ?>/helpdesk/reassign_technician",
					url: "<?php echo base_url() ?>/helpdesk/tech_reassign_ajx",
					type: "POST",
					data:{group_id:group_id,technician_id:technician_id,request_id:request_idaa},
					
					success:function(resp){
						console.log(resp);
						console.log('response is = '+resp);
						//$("#spinner").show();
						console.log('request_id '+request_idaa);
						if(resp > 0 && resp !=2){
							location.reload();
						}else if(resp == 2){
							alert("You can not re-assign to same technician");
						}else{
							alert("error occured please try again later");
						}
					},error:function(resp){
						alert("Error occured please try again later");
						console.log(resp);
					}
				});
                return false;
            }
        });

	
	
	
	<?php if(check_technician($CI->user_ldamp["mail"],$CI->user_ldamp["employeeid"])){ ?>
		var nulval = [null,null,null,null, null,null,null, null,null,null,null,null, null,null,null];
	<?php }else{ ?>
		var nulval = [null,null,null,null, null,null,null, null,null,null,null, null,null,null];
	<?php } ?>
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": nulval,
		"aaSorting": [],
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
	//Handles menu drop down
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
		
    });
	$('#dynamic-table').on('click',' tbody >tr>td>.dropdown>a.dropdown-toggle.ab',function(e){
		//alert(($("#printTxt").position().left));
		
		var positionX;
		var positionY;
		var group_id = $(this).attr('data-gid');
		var tech_id  = $(this).attr('data-tid');
		request_idaa = $(this).attr('data-rid');
		positionX = e.clientX-45 + 'px',
		positionY = $(this).offset().top-250+'px';
		$("#groups").val('');
		$("#groups").val(group_id);
		//alert($(this).offset().top);
		//$("#request_gp").css({'top':positionY,'left':'75%','display':'block'}).show();
		//$("#request_gp").css({'top':e.clientY,'left':e.clientX,'display':'block'}).show();
		$("#printTxt").css({'top':positionY,'left':positionX,'display':'block'});
		// code to select technician
		get_technician(group_id,tech_id);
		$("#request_gp").show();
		
		
		///code to select group 
		/*
		$("#groups option").each(function(index){
			if(index == group_id){
				$(this).attr("selected","selected");
				
			}
			else{
				$(this).removeAttr("selected");
			}
			
				
			
		});*/
		
	});
	
	
	 // on menu selection
	 $( ".dropdown-menu" ).draggable();
	 
	 //on group selection
	$("#groups").on("change",function(){
		var group_id = $(this).find("option:selected").val();
		if(group_id == "")
		{
			alert("please select the correct group");
			
		}
		else
		{
			get_technician(group_id,tech_id =0);
		}
		
	});
	
	$(".close.pop").on("click",function(){
		$("#request_gp").hide();
	});
	$(".btn.btn-default.pop").on("click",function(){
		$("#request_gp").hide();
	});
	
});
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>