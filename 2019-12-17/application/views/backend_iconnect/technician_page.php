<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Technician</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_technician'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>Technician Name : </b></label>
				<input type="hidden" name="tech_id" value="<?php echo $technician_data->technician_id; ?>" />
				<select class="form-control" name="tech_txt">
					<?php echo get_user_list($technician_data->technician_email); ?>
				</select>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Technician</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_technician'; ?>" method="POST">
			  <div class="form-group">
				<label><b>Technician Name : </b></label>
				<select class="form-control" name="tech_txt">
					<?php echo get_user_list($technician_data->technician_email); ?>
				</select>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Technician</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Technician Name : </b></label>
			<span><?php echo $technician_data->tech_name; ?></span>
		  </div>
		  
		  <div class="form-group">
			<label ><b>Technician Email : </b></label>
			<span><?php echo $technician_data->technician_email; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

