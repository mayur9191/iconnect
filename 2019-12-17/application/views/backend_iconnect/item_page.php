<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Sub-Category</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_item'; ?>" method="POST">
			  
			  <div class="form-group">
				<label ><b>Department Name : </b></label>
				<select class="form-control" name="dept_id">
					<?php 
						foreach(get_all_record_by_code('dept') as $dept_record){
							if($dept_record->dept_id == $item_data->dept_id){
								echo '<option value="'.$dept_record->dept_id.'" selected>'.ucfirst($dept_record->dept_name).'</option>';
							}else{
								echo '<option value="'.$dept_record->dept_id.'">'.ucfirst($dept_record->dept_name).'</option>';
							}
						}
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Category Name : </b></label>
				<select class="form-control" name="cat_id">
					<?php 
						foreach(get_all_record_by_code('category') as $cat_record){
							if($cat_record->category_id == $item_data->category_id){
								echo '<option value="'.$cat_record->category_id.'" selected>'.ucfirst($cat_record->category_name).'</option>';
							}else{
								echo '<option value="'.$cat_record->category_id.'">'.ucfirst($cat_record->category_name).'</option>';
							}
						}
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Sub-Category Name : </b></label>
				<select class="form-control" name="subcat_id">
					<?php 
						foreach(get_all_record_by_code('subcategory') as $subcat_record){
							if($subcat_record->sub_category_id == $item_data->sub_category_id){
								echo '<option value="'.$subcat_record->sub_category_id.'" selected>'.ucfirst($subcat_record->sub_category_name).'</option>';
							}else{
								echo '<option value="'.$subcat_record->sub_category_id.'">'.ucfirst($subcat_record->sub_category_name).'</option>';
							}
						}
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Item Name : </b></label>
				<input type="hidden" name="item_id" value="<?php echo $item_data->items_id; ?>" />
				<textarea  name="item_txt" class="form-control"><?php echo $item_data->item_name; ?></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Sub-Category</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_item'; ?>" method="POST">
			 <div class="form-group">
				<label ><b>Department Name : </b></label>
				<select class="form-control" name="dept_id">
					<?php 
						foreach(get_all_record_by_code('dept') as $dept_record){
							echo '<option value="'.$dept_record->dept_id.'">'.ucfirst($dept_record->dept_name).'</option>';
						}
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Category Name : </b></label>
				<select class="form-control" name="cat_id">
					<?php 
						foreach(get_all_record_by_code('category') as $cat_record){
							echo '<option value="'.$cat_record->category_id.'">'.ucfirst($cat_record->category_name).'</option>';
						}
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Sub-Category Name : </b></label>
				<select class="form-control" name="subcat_id">
					<?php 
						foreach(get_all_record_by_code('subcategory') as $subcat_record){
							echo '<option value="'.$subcat_record->sub_category_id.'">'.ucfirst($subcat_record->sub_category_name).'</option>';
						}
					?>
				</select>
			  </div>
			  <div class="form-group">
				<label><b>Item Name : </b></label>
				<textarea  name="item_txt" class="form-control"></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Sub-Category</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Department Name : </b></label>
			<span><?php 
					$deptData = get_record_by_id_code('dept',$item_data->dept_id);
					$deptName = $deptData->dept_name;
					echo $deptName;
				?></span>
		  </div>
		  
		   <div class="form-group">
			<label ><b>Category Name : </b></label>
			<span><?php 
					$categoryData = get_record_by_id_code('category',$item_data->category_id);
					$categoryName = $categoryData->category_name;
					echo $categoryName;
				?></span>
		  </div>
		  
		   <div class="form-group">
			<label ><b>Sub-Category Name : </b></label>
			<span><?php 
					$subcategoryData = get_record_by_id_code('subcategory',$item_data->category_id);
					$subcategoryName = $subcategoryData->sub_category_name;
					echo $subcategoryName;
				?></span>
		  </div>
		  
		  
		  <div class="form-group">
			<label ><b>Item Name : </b></label>
			<span><?php echo $item_data->item_name; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

