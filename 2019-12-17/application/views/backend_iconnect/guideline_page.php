<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Guideline</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_guideline'; ?>" method="POST">
				<?php 
					$deptData = get_record_by_id_code('dept',$guideline_data->dept_id);
					$deptName = $deptData->dept_name;
				?>
				<div class="form-group">
				<label ><b>Department : </b></label>
				<span><?php echo $deptName; ?></span>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Guideline : </b></label>
				<input type="hidden" name="guid_id" value="<?php echo $guideline_data->dept_guideline_hd_id; ?>" />
				<textarea  name="guideline_txt" class="form-control"><?php echo $guideline_data->guideline_txt; ?></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Guideline</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_guideline'; ?>" method="POST">
			  <div class="form-group">
				<label><b>Guideline </b></label>
				<textarea  name="guideline_txt" class="form-control"></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Guideline</h3>
		</div>
		<div class="modal-body">
		
			<?php 
				$deptData = get_record_by_id_code('dept',$guideline_data->dept_id);
				$deptName = $deptData->dept_name;
			?>
		  <div class="form-group">
			<label ><b>Department : </b></label>
			<span><?php echo $deptName; ?></span>
		  </div>
		  
		  <div class="form-group">
			<label ><b>Guideline : </b></label>
			<span><?php echo $guideline_data->guideline_txt; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

