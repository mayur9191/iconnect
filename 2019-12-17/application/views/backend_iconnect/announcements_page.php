<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.21/themes/base/jquery-ui.css" />
<style>
.ui-datepicker td>a.ui-state-active { color:#000 !important; }
</style>
<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Announcement</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_announcement'; ?>" method="POST" enctype="multipart/form-data">
			  <div class="form-group">
				<img src="<?php echo base_url().'uploads/announcement/'.$announcements_data->an_file; ?>" width="30%" height="40%" />
			  </div>
			  <div class="form-group">
				<label for="exampleInputFile">File input</label>
				<input type="hidden" name="anno_id" value="<?php echo $announcements_data->an_id; ?>" />
				<input type="file" name="announcement_file" id="exampleInputFile">
			  </div>
			  <!--<div class="form-group">
				<small> Announcement display </small><br>
				<label >From </label>
				<input type="text" name="frm_dt" id="from" class="form-control" />
			   </div>
			   <div class="form-group">
				<label for="exampleInputFile">To</label>
				<input type="text" name="to_dt" id="to" class="form-control" />
			   </div>-->
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Announcement</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_announcement'; ?>" method="POST" enctype="multipart/form-data">
			  <div class="form-group">
				<label for="exampleInputFile">File input</label>
				<input type="file" name="announcement_file" id="exampleInputFile">
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>			
		<center>
			<img src="<?php echo base_url().'uploads/announcement/'.$announcements_data->an_file; ?>" width="100%" />
		</center>
		</div>
		
	</div>
</div>

<?php } ?>

