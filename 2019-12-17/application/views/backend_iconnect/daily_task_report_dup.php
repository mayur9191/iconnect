<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Daily task report</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="col-xs-12">
				<div class="clearfix">
					<div class="pull-right tableTools-container"></div>
				</div>
				<div class="table-header">
					Overall Daily Task Report 
				</div>
				<?php $array2 = array(); ?>
				<div class="row">
					<div class="col-md-3">
						<div class="panel panel-primary" style="position:relative;">
							<span class="btn backtab btn-block">Daily Task Report Summary  </span>
							<div class="panel-body inbox-menu">
								<ul style="list-style-type:none;">
								<?php $record = json_decode($report_data);
									  $person_ary = array();
									  $count_ary = array();
									  foreach($record as $rec){
										$person_ary[] = $rec->u_name.'('.$rec->count.')';
										$count_ary[] = $rec->count;
								?>
									<li style="width:180px;padding:5px;">
									<a href="javascript:void(0)" onclick="ajax_get_result('<?php echo $rec->emp_code; ?>','<?php echo$rec->u_name; ?>')"><?php echo $rec->u_name; ?>
									<span class="label label-primary pull-right" ><?php echo $rec->count; ?></span></a></li> 
								<?php 
									$array2[] = $rec->emp_code;
								?>
								<?php } ?>
								
								</ul>	  
							</div>
							
							<span class="btn backtab btn-block">Task not filled</span>
							<div class="panel-body inbox-menu">
								<ul style="list-style-type:none;">
								<?php
								$array1 = array('SG0524','SG0360','SG0532','SG0365','SG0533','SG0534');
								$result = array_diff($array1, $array2);
									foreach($result as $emp_code_){
									$emp_data = get_all_details_by_emp_code($emp_code_);
								?>
									<li style="width:180px;padding:5px;">
									<a href="javascript:void(0)" ><?php echo $emp_data->u_name; ?>
									<span class="label label-primary pull-right" >0</span></a></li> 
								<?php } ?>
								</ul>	  
							</div>
							
						</div>
					</div>
					<div class="col-md-3">
						
						<?php 
							$color_ary = array('dc3912','3366cc','ff9900','990099','0099c6','dd4477','85C1E9','EDBB99','F9E79F','109618','FAD7A0','73C6B6','A9DFBF','ABEBC6');
							$color_ary1 = array('85C1E9','EDBB99','F9E79F','FAD7A0','73C6B6','A9DFBF','ABEBC6');
							//$count_ary = array(1,2,3,4,5,6,7);
							$color_str = implode("|",$color_ary);
							$color_str1 = implode("|",$color_ary1);
							$person_str = implode("|",$person_ary);
							$count_str = implode(",",$count_ary);
						?>
						<br><br>
						<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:<?php echo $count_str; ?>&chco=<?php echo $color_str; ?>&chs=500x155&chl=<?php echo $person_str; ?>" />
					</div>
					
					<div class="col-md-3 pull-right">
						<div class="panel panel-primary" >
							<span class="btn backtab btn-block">Filters</span>
							<div class="col-md-12">
								<form action="#" method="POST">
									<br>
										<input type="radio" name="filter_type" value="all" class="filter_type" id="filter_by_all" /> All &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="filter_type" value="by_date" class="filter_type" id="filter_by_date" /> By Date
									<br><br>
									<span id="by_date" style="display:none;">
										Filter Date : <input id="thedate" type="text" name="filter_date" /><br>
									</span>
									<input id="thesubmit" type="submit" value="Submit" />
								</form>
							</div>
							<div class="panel-body inbox-menu"></div>
						</div>
					</div>
					
					
				</div>
			</div>
		
			<div id="myModal_div" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"></h4>
				  </div>
				  <div class="modal-body">
				  </div>
				  <div class="modal-footer" style="border-top:none;">
				   <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				  </div>
				</div>

			  </div>
			</div> 
			
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.21/themes/base/jquery-ui.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
		
		<script>
		var date_val = '';
		$(document).ready(function(){
			var post_type = '<?php echo $post_type; ?>';
			if(post_type !=''){
				if(post_type == 'all'){
					$("#filter_by_all").attr("checked",true);
				}
				if(post_type == 'by_date'){
					$("#filter_by_date").attr("checked",true);
					date_val = '<?php echo $filter_date; ?>';
					set_date = '<?php echo date('Y-m-d',strtotime($filter_date)); ?>';
					$("#thedate").val(date_val);
					$("#by_date").show();
				}
			}
			
			$(".filter_type").on("change",function(){
				var filter_option = $(this).val();
				console.log(filter_option);
				if(filter_option == 'all'){
					$("#by_date").hide();
				}else{
					$("#by_date").show();
				}
			});
		});
		
		$('#thedate').datepicker({
			dateFormat: 'dd-mm-yy',
			altField: '#thealtdate',
			altFormat: 'yy-mm-dd',
			format: "dd-mm-yyyy"
		}).datepicker('setDate','now');
		
		</script>
		
		<script>
			function ajax_get_result(emp_code,emp_name){
				$.get("<?php echo base_url() ?>admin/get_employee_tasks/"+emp_code+"/"+date_val,function(data, status){
					$("#myModal_div").find(".modal-title").html(emp_name);
					$("#myModal_div").find(".modal-body").html(data);
					$("#myModal_div").modal('show');
				});
				return false;
			}	
		</script>
		
	</div>
</div><!-- /.main-content -->

</div>

<br>
<?php 

//Array ( [0] => SG0271 [1] => SG0272 [2] => SG0282 [3] => SG0283 [5] => SG0308 [6] => SG0360 [7] => SG0361 )
//Array ( [4] => SG0300 [8] => SG0365 [9] => SG0355 )
//print_r($result);
?>

</body>