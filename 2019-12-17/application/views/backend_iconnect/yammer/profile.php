
<div class="main-content">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.backwall{
	height:200px;
	width:100%;
}
.walllogo{
	height:100px;
	position:absolute;
	padding: 10px 10px 10px 10px;
	margin: 20px 20px 20px 25px;
}
.group_n{
	float: right;
    position: absolute;
    color: #fff;
    font-weight: bold;
    margin: 30px 140px 0px;
}
.men_tab{
	position: absolute;
    margin-top: -40px;
}
.right_div{
	float:right !important
}
.member_profile > ul > li > img{
	border-color: #1c6b94;
    margin: 0px 0px 5px 0px;
    border-radius: 40px;
    height: 38px;
}
.border_bottom{
	border-bottom: #d4cfcf solid 1px;
    padding: 5px 5px 7px 2px;
}
.list_itm{
	list-style-type:none;
}
.list_itm > li{
	margin: 10px 10px 10px -20px;
}
.profile_img_radius{
	border-radius:100px;
	height:80px;
}
.profile_n{
	float: right;
    position: relative;
    color: #000;
    margin: 30px 110px 0px;
}

.profile_n_comment{
	float: left;
    position: relative;
    color: #000;
    margin: 12px 20px 0px;	
}
.walllogocomment{
	height:70px;
	position:absolute;
	padding: 10px 10px 10px 10px;
	margin: 20px 20px 20px 25px;
}
.profile_upload_img > img{
	width:80%;
}
.tab-content{
	border:none;
}
.social_link a:hover{
	color: #23527c;
    text-decoration: none;
}
.comment_div{
	position:relative;
}
.prof_post{
	border-top: #d4cfcf solid 1px;
	margin-top:20px;
}
.post_comment{
	background-color:#edeff2;
}

/*Google comment start */
@import url(http://fonts.googleapis.com/css?family=Roboto:400,700);

.img-circle{
	border-radius:100px;
	height:50px;
}

.img-circle1{
	border-radius:100px;
	height:40px;
}

.panel-google-plus.panel-google-plus-show-comment > .panel-footer > .img-circle1 {
    display: none;   
}

.panel-google-plus > .panel-footer > .btn {
    float: left;
    margin-right: 8px;
}
.panel-google-plus > .panel-footer > .input-placeholder {
    display: block;
    margin-left: 98px;
    color: rgb(153, 153, 153);
    font-size: 12px;
    font-weight: 400;
    padding: 8px 6px 7px;
    border: 1px solid rgb(217, 217, 217);
    border-radius: 2px;
    box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 0px 0px;
}
.panel-google-plus.panel-google-plus-show-comment > .panel-footer > .input-placeholder {
    display: none;   
}
.panel-google-plus > .panel-google-plus-comment {
    display: none;
    padding: 10px 20px 15px;
    border-top: 1px solid rgb(229, 229, 229);
    background-color: rgb(245, 245, 245);
}
.panel-google-plus.panel-google-plus-show-comment > .panel-google-plus-comment {
    display: block;
}
/*.panel-google-plus > .panel-google-plus-comment > img {
    float: left;   
}*/
.panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea {
    float: right;
    width: calc(100% - 56px);
}
.panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > textarea {
    display: block;
    /*margin-left: 60px;
    width: calc(100% - 56px);*/
    width: 100%;
    background-color: rgb(255, 255, 255);
    border: 1px solid rgb(217, 217, 217);
    box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 0px 0px;
    resize: vertical;
}
.panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > .btn {
    margin-top: 10px;
    margin-right: 8px;
    width: 100%;
}
@media (min-width: 992px) {
    .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > .btn {
        width: auto;
    }    
}

/*Google comment end */

/* file upload start */
.fileUpload {
	position: relative;
	overflow: hidden;
}
.fileUpload input {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	cursor: pointer;
	opacity: 0;
}
.progress {
	margin-bottom: 0;
}
/* file upload end */
</style>
	<div class="main-content-inner">
		<!-- <div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Add Asset</li>
			</ul>
			
			
			example file upload code start
				<form>
					<span class="fileUpload btn btn-default">
						<span class="glyphicon glyphicon-upload"></span> Upload file
						<input type="file" id="uploadFile" />
					</span>
				</form>
					
				<p id="fileUploadError" class="text-danger hide"></p>

				<div class="list-group" id="files"></div>

				<script id="fileUploadProgressTemplate" type="text/x-jquery-tmpl">
					<div class="list-group-item">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-info" style="width: 0%;"></div>
						</div>
					</div>
				</script>

				<script id="fileUploadItemTemplate" type="text/x-jquery-tmpl">
					<div class="list-group-item">
						<button type="button" class="close rkupload" onclick="rkupload(event);">&times;</button>
						<span class="glyphicon glyphicon-file"></span> ${rkFilename} 
					</div>
				</script>
			example file upload code start
			
			
			
		</div> -->
		
		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-md-12 col-sm-12">
						<div>
						<span><img src="http://www.businessintelligence.com.s3.amazonaws.com/wp-content/uploads/2013/01/IT-logo.png" class="walllogo" />
						</span>
						<span class="group_n">
							<h3>IT Shared Services</h3>
							<h5>Information Technology !!</h5>
						</span>
						</div>
						<img src="https://wallpaperscraft.com/image/wall_brick_structure_surface_27260_1920x1200.jpg" class="backwall" />
						<div class="row">
							<div class="col-md-9 col-sm-12 men_tab">
								 <ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#home" onclick="changeTab('home')">NEW CONVERSATIONS</a></li>
									<li><a data-toggle="tab" href="#menu1" onclick="changeTab('menu1')">ALL CONVERSATIONS</a></li>
									<li><a data-toggle="tab" href="#menu2" onclick="changeTab('menu2')">FILES</a></li>
									<li><a data-toggle="tab" href="#menu3" onclick="changeTab('menu3')">NOTES</a></li>
								  </ul>

								  <div class="col-md-12 col-sm-12 tab-content">
									<div id="home" class="fade in active">
										<div class="row">
											<h4>Update</h4>
											<form action="#">
												<div class="form-group">
													<textarea row="5" class="form-control"></textarea>
												</div>
												<input type="submit" value="Post" class="btn btn-primary pull-right" style="margin:0px 10px 10px 10px" />
												<!-- example file upload code start -->
													<span class="fileUpload btn btn-primary pull-right">
														<span class="glyphicon glyphicon-upload"></span> Upload file
														<input type="file" id="uploadFile" />
													</span>
												<!-- example file upload code start -->
											</form>
										</div>	
										<div class="row">
											<p id="fileUploadError" class="text-danger hide"></p>

											<div class="list-group" id="files"></div>

											<script id="fileUploadProgressTemplate" type="text/x-jquery-tmpl">
												<div class="list-group-item">
													<div class="progress progress-striped active">
														<div class="progress-bar progress-bar-info" style="width: 0%;"></div>
													</div>
												</div>
											</script>

											<script id="fileUploadItemTemplate" type="text/x-jquery-tmpl">
												<div class="list-group-item">
													<button type="button" class="close rkupload" onclick="rkupload(event);">&times;</button>
													<span class="glyphicon glyphicon-file"></span> ${rkFilename} <!-- File name (type, date, etc)-->
												</div>
											</script>
										</div>
										<div class="row prof_post">											
											<span>
												<img class="profile_img_radius walllogo" src="http://192.168.20.20/assets/images/45.png" alt="user image" />
											</span>
											<div class="profile_n">
												<span ><b>Ravi kumar Arsid </b> -  November 10 at 11:25am</span>
												<h6 style="color: #b5adad;">Feeling happy!!</h6>
												<p>
													Whether the purpose of your site is to convince people to do something, to buy something, or simply to inform, testing only whether they can find information or complete transactions is a missed opportunity: Is the content appropriate for the audience? Can they read and understand what you’ve written
												</p>
												<span class="profile_upload_img">
													<img src="https://static.pexels.com/photos/33109/fall-autumn-red-season.jpg"  />
												</span>
												<p>
													<ul class="list-inline social_link">
														<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
														<li><a href="#"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a></li>
														<li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
													</ul>
												</p>
											</div>
										</div>
										<div class="row post_comment">
											<span class="comment_div">
												<img class="profile_img_radius walllogocomment" src="http://192.168.20.20/assets/images/45.png" alt="user image" />
												<span class="profile_n">
												<span><b>Ravi kumar Arsid </b> -  November 10 at 11:25am</span>
													<h6  style="color: #b5adad;"></h6>
													<p>
														Whether the purpose of your site is to convince people to do something, to buy something, or simply to inform, testing only whether they can find information or complete transactions is a missed opportunity: Is the content appropriate for the audience? Can they read and understand what you’ve written
													</p>
													<p>
														<ul class="list-inline social_link">
															<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
															<li><a href="#"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a></li>
															<li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
														</ul>
													</p>
													
													
													 <div class="row">
														<div class="[ col-xs-12 col-sm-12 ]">
															<div class="[ panel panel-default ] panel-google-plus">
																<div class="panel-footer">
																<img class="img-circle1 pull-left" src="http://192.168.20.20/assets/images/45.png" alt="User Image" />
																	<div class="input-placeholder">Add a comment...</div>
																</div>
																<div class="panel-google-plus-comment">
																	<img class="img-circle" src="http://192.168.20.20/assets/images/45.png" alt="User Image" />
																	<div class="panel-google-plus-textarea">
																		<textarea rows="4"></textarea>
																		<button type="submit" class="[ btn btn-success disabled ]">Post comment</button>
																		<button type="reset" class="[ btn btn-default ]">Cancel</button>
																	</div>
																	<div class="clearfix"></div>
																</div>
															</div>
														</div>
													</div>
													
													
												</span>
											</span>
										</div>
										
										
										<div class="row prof_post">											
											<span>
												<img class="profile_img_radius walllogo" src="http://192.168.20.20/assets/images/45.png" alt="user image" />
											</span>
											<span class="profile_n">
												<span ><b>Ravi kumar Arsid </b> -  November 10 at 11:25am</span>
												<h6  style="color: #b5adad;">Information Technology !!</h6>
												<p>
													Whether the purpose of your site is to convince people to do something, to buy something, or simply to inform, testing only whether they can find information or complete transactions is a missed opportunity: Is the content appropriate for the audience? Can they read and understand what you’ve written
												</p>
												<p>
													<ul class="list-inline social_link">
														<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
														<li><a href="#"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a></li>
														<li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
													</ul>
												</p>
											</span>
										</div>
										<div class="row post_comment">
											<div class="row">
												<div class="[ col-xs-12 col-sm-12 ]">
													<div class="[ panel panel-default ] panel-google-plus">
														<div class="panel-footer">
														<img class="img-circle1 pull-left" src="http://192.168.20.20/assets/images/45.png" alt="User Image" />
															<div class="input-placeholder">Add a comment...</div>
														</div>
														<div class="panel-google-plus-comment">
															<img class="img-circle" src="http://192.168.20.20/assets/images/45.png" alt="User Image" />
															<div class="panel-google-plus-textarea">
																<textarea rows="4"></textarea>
																<button type="submit" class="[ btn btn-success disabled ]">Post comment</button>
																<button type="reset" class="[ btn btn-default ]">Cancel</button>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										
										
										
										<div class="row prof_post">											
											<span>
												<img class="profile_img_radius walllogo" src="http://192.168.20.20/assets/images/45.png" alt="user image" />
											</span>
											<span class="profile_n">
												<span ><b>Ravi kumar Arsid </b> -  November 10 at 11:25am</span>
												<h6  style="color: #b5adad;">Information Technology !!</h6>
												<p>
													Whether the purpose of your site is to convince people to do something, to buy something, or simply to inform, testing only whether they can find information or complete transactions is a missed opportunity: Is the content appropriate for the audience? Can they read and understand what you’ve written
												</p>
												<p>
													<ul class="list-inline social_link">
														<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</a></li>
														<li><a href="#"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a></li>
														<li><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
													</ul>
												</p>
											</span>
										</div>
										<div class="row post_comment">
											<div class="row">
												<div class="[ col-xs-12 col-sm-12 ]">
													<div class="[ panel panel-default ] panel-google-plus">
														<div class="panel-footer">
														<img class="img-circle1 pull-left" src="http://192.168.20.20/assets/images/45.png" alt="User Image" />
															<div class="input-placeholder">Add a comment...</div>
														</div>
														<div class="panel-google-plus-comment">
															<img class="img-circle" src="http://192.168.20.20/assets/images/45.png" alt="User Image" />
															<div class="panel-google-plus-textarea">
																<textarea rows="4"></textarea>
																<button type="submit" class="[ btn btn-success disabled ]">Post comment</button>
																<button type="reset" class="[ btn btn-default ]">Cancel</button>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
									
									<div id="menu1" class="tab-pane fade">
										<div class="row">
										  <h3>ALL CONVERSATIONS</h3>
										  <p></p>
										</div>
									</div>
									
									<div id="menu2" class="tab-pane fade">
									  <h3>FILES</h3>
									  <table id="example" class="display" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Name</th>
												<th>Type</th>
												<th>Last Updated By</th>
												<th>Last Updated</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Image 1</td>
												<td>jpeg</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar</td>
												<td>November 22</td>
												
											</tr>
											<tr>
												<td>Image 2</td>
												<td>jpeg</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar</td>
												<td>November 22</td>
											</tr>
											<tr>
												<td>Image 3</td>
												<td>jpeg</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar</td>
												<td>November 21</td>
											</tr>
											<tr>
												<td>Image 4</td>
												<td>jpeg</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar</td>
												<td>November 17</td>
											</tr>
											
											
										</tbody>
									</table>
									</div>
									
									<div id="menu3" class="tab-pane fade">
									  <h3>NOTES</h3>
									  <input type="submit" value="Add Note" class="btn btn-primary pull-right" style="margin:0px 10px 10px 10px" data-toggle="modal" data-target="#myModal" onclick="modalClick()" />
									  
									   <table id="example1" class="display" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Name</th>
												<th>Last Published By</th>
												<th>Last Published On</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Note 1</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar </td>
												<td>November 22</td>
												
											</tr>
											<tr>
												<td>Note 2</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar </td>
												<td>November 22</td>
											</tr>
											<tr>
												<td>Note 3</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar </td>
												<td>November 21</td>
											</tr>
											<tr>
												<td>Note 4</td>
												<td><img class="profile_img_radius" style="height:30px;" src="http://192.168.20.20/assets/images/45.png" alt="user image" /> Ravikumar </td>
												<td>November 16</td>
											</tr>
											
											
										</tbody>
									</table>
									</div>
								  </div>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-12 right_div">
							<div class="member_profile">
								<span ><h5 class="border_bottom"><b>MEMBERS (11)</b></h5></span>
								<ul class="list-inline">
									<li><img class="nav-user-photo" src="http://192.168.20.20/assets/images/45.png" alt="user image"></li>
									<li><img class="nav-user-photo" src="http://192.168.20.20/assets/images/45.png" alt="user image"></li>
									<li><img class="nav-user-photo" src="http://192.168.20.20/assets/images/45.png" alt="user image"></li>
									<li><img class="nav-user-photo" src="http://192.168.20.20/assets/images/45.png" alt="user image"></li>
								</ul>
							</div>
							<div class="member_profile">
								<span ><h5 class="border_bottom"><b>GROUP ACTIONS</b></h5></span>
								<span><i class="fa fa-bar-chart" aria-hidden="true"></i> View Group Insights</span>
							</div>
							
							<div class="member_profile">
								<span ><h5 class="border_bottom"><b>ACCESS OPTIONS</b></h5></span>
								<span>
									<ul class="list_itm">
										<li><i class="fa fa-square-o" aria-hidden="true"></i> Subscribe to this group by email</li>
										<li><i class="fa fa-envelope" aria-hidden="true"></i> Post to this group by email</li>
										<li><i class="fa fa-chevron-left" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i> Embed this feed in your site</li>
									</ul>
								</span>
							</div>
						</div>
						<!-- tabs start  -->
						<!-- Modal -->
						  <div class="modal" id="myModal" role="dialog">
							<div class="modal-dialog">
							
							  <!-- Modal content-->
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4 class="modal-title">Add note</h4>
								</div>
								<div class="modal-body">
								  <p><textarea name="textara" row="5" id="mytextarea"></textarea></p>
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
								</div>
							  </div>
							  
							</div>
						  </div>
						<!-- Modal end -->
					
						<!-- tabs end -->
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->

<!-- Modal end -->





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://tinymce.cachefly.net/4.2/tinymce.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	//datatable code start 
	$('#example').DataTable({
		order: [[ 3, 'desc' ], [ 0, 'asc' ]]
	});
	$('#example1').DataTable({
		order: [[ 2, 'desc' ], [ 0, 'asc' ]]
	});
	//datatable code end
	
	//file upload code start 
	$("#uploadFile").change(function() {
		var formData = new FormData();
		formData.append('file', this.files[0]);
		
		$("#files").append($("#fileUploadProgressTemplate").tmpl());
		$("#fileUploadError").addClass("hide");
		
		$.ajax({
			url: '<?php echo base_url();?>admin/testUpload',
			type: 'POST',
			xhr: function() {
				var xhr = $.ajaxSettings.xhr();
				if (xhr.upload) {
					xhr.upload.addEventListener('progress', function(evt) {
						var percent = (evt.loaded / evt.total) * 100;
						$("#files").find(".progress-bar").width(percent + "%");
					}, false);
				}
				return xhr;
			},
			success: function(data) {
				obj = JSON.parse(data);
				$("#files").children().last().remove();
				returnData = 
				$("#files").append($("#fileUploadItemTemplate").tmpl({rkFilename:obj.fileName}));
				//$("#fileUploadError").removeClass("hide").text("An error occured!");
				$("#uploadFile").closest("form").trigger("reset");
				console.log(obj);
			},
			/*
			error: function() {
				$("#fileUploadError").removeClass("hide").text("An error occured!");
				$("#files").children().last().remove();
				$("#uploadFile").closest("form").trigger("reset");
			},*/
			data: formData,
			cache: false,
			contentType: false,
			processData: false
		}, 'json');
	});
	//file upload code end
	
	
	
	
	//Google comment start 
	 $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function(event) {
			var $panel = $(this).closest('.panel-google-plus');
				$comment = $panel.find('.panel-google-plus-comment');
				
			$comment.find('.btn:first-child').addClass('disabled');
			$comment.find('textarea').val('');
			
			$panel.toggleClass('panel-google-plus-show-comment');
			
			if ($panel.hasClass('panel-google-plus-show-comment')) {
				$comment.find('textarea').focus();
			}
	   });
	   $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
			var $comment = $(this).closest('.panel-google-plus-comment');
			
			$comment.find('button[type="submit"]').addClass('disabled');
			if ($(this).val().length >= 1) {
				$comment.find('button[type="submit"]').removeClass('disabled');
			}
	   });
	//Google comment end
	
});
tinymce.init({
    selector: "#mytextarea"
});
	
	

function rkupload(e){
	console.log('clicked');
	e.path[1].remove();
	console.log(e.path[1].remove());
}

function changeTab(hideId){
	var tabAry = ["home","menu1","menu2","menu3"];
	for(var i=0;i<=tabAry.length;i++){
		if(tabAry[i] == hideId){
			$("#"+tabAry[i]).show();
		}else{
			$("#"+tabAry[i]).hide();
		}
	}
}

function modalClick(){
	console.log('Clicked');
	setTimeout(
		function(){ 
		$(".modal-backdrop").remove(); 
	}, 1000);	
}
</script>

<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>