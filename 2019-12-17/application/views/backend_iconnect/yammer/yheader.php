<?php $backendURL = base_url().'assets/backend/'; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Admin</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="<?php echo $backendURL; ?>assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?php echo $backendURL; ?>assets/js/html5shiv.min.js"></script>
		<script src="<?php echo $backendURL; ?>assets/js/respond.min.js"></script>
		<![endif]-->
		<link rel="icon" href="https://cdn4.iconfinder.com/data/icons/Isloo_icons_by_IconFinder/128/internet_intranet.png" sizes="192x192" />
		<!--datatable css--->
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" >

		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />

		<style>
		.mand{color:red;}
		.error {
			color: red;
			font-size: 12px;
		}
		</style>
		
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a data-toggle="dropdown" href="<?php echo base_url(); ?>" class="dropdown-toggle">
						<img class="nav-user-photo" src="<?php echo base_url().'assets/images/iconnect-logo.png';?>" alt="iconnect" style="border-color:#1c6b94;">
					</a>

				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<?php 
						if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
							$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
						} else {
							$dp_image = base_url() . 'assets/images/45.png';
						}
						?>
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo $dp_image;?>" alt="user image" style="border-color:#1c6b94;margin:0px 0px 0 0;" />
								<span class="user-info">
									<small>Welcome,</small>
									<?= ucwords($user_ldamp['name']) ?>
								</span>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo base_url(); ?>profile/edit">
										<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
										 Edit Profile 
									</a>
								</li>

								<li>
									<a href="<?php echo base_url(); ?>profile/logout">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
						
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="<?php echo ($action_tab == 'dashboard')? 'active' : '' ;?>">
						<a href="#">
							<span class="menu-text">SKEIRON GROUPS </span>
							<b class="arrow fa fa-plus"></b>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						<a href="#" class="dropdown-toggle">
							<span class="menu-text">
								IT Shared Services
							</span>
						</a>
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						<a href="#" class="dropdown-toggle">
							<span class="menu-text">
								GM Office (All Employees)
							</span>
						</a>
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						<a href="#" class="dropdown-toggle">
							<span class="menu-text">
								All Company
							</span>
						</a>
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						<a href="#" class="dropdown-toggle">
							<span class="menu-text">
								<i class="fa fa-plus" aria-hidden="true"></i>
								Create a group
							</span>
						</a>
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						<a href="#" class="dropdown-toggle">
							<span class="menu-text">
								<i class="fa fa-users" aria-hidden="true"></i>
								Discover more groups
							</span>
						</a>
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						
					</li>
					
					<li class="<?php echo ($action_tab == 'dashboard')? 'active' : '' ;?>">
						<a href="#">
							<span class="menu-text">Private Messages</span>
							<b class="arrow fa fa-plus"></b>
							
						</a>
						<b class="arrow"></b>
						Create your first private message by clicking the + button next to this tip.
					</li>
					
					<li class="<?php echo (in_array($action_tab,array('thought','announcements','archive')))? 'active' : '' ;?>">
						<a href="#" class="dropdown-toggle">
							<span class="menu-text">
								<i class="fa fa-plus" aria-hidden="true"></i>
								Create a message
							</span>
						</a>
					</li>
					
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>
			