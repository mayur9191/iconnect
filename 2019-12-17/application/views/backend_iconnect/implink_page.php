<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Important link</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_implink'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>Link Name : </b></label>
				<input type="hidden" name="implink_id" value="<?php echo $implink_data->implink_id; ?>" />
				<textarea  name="implink_name" class="form-control"><?php echo $implink_data->implink_name; ?></textarea>
			  </div>
			  <div class="form-group">
				<label ><b>Link URL : </b></label>
				<textarea  name="implink_url" class="form-control"><?php echo $implink_data->implink_url; ?></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Important link</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_implink'; ?>" method="POST">
			
			  <div class="form-group">
				<label ><b>Link Name : </b></label>
				<textarea  name="implink_name" class="form-control"></textarea>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Link URL : </b></label>
				<textarea  name="implink_url" class="form-control"></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Important link</h3>
		</div>
		<div class="modal-body">
		  
		  <div class="form-group">
			<label ><b>Link Name : </b></label>
			<span><?php echo $implink_data->implink_name; ?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Link URL : </b></label>
			<span><?php echo $implink_data->implink_url; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

