
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Update Stationary</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Update Stationary Item</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Update Stationary Item
						<a href="<?php echo base_url().'stationary/add_stationary';?>" class="btn btn-success pull-right" title="Back to Stationary List">Back</a>
					</div>
					<?php //var_dump($stationary_data); ?> 
					<div>
						<form action="<?php echo base_url().'stationary/updateStationaryItem/'.$stationary_data->id; ?>" method="POST" id="update_stationary_item" name="myform" novalidate>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="item_code">Item Code:</label>
									
									<input type="text" class="form-control" name="item_code" id="item_code" value="<?php echo $stationary_data->item_code; ?>" readonly/>
									<input type="hidden" name="item_code" id="item_code" value="<?php echo $stationary_data->item_code; ?>">
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="item_name">Item Name<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="item_name" id="item_name" value="<?php echo $stationary_data->item_name; ?>"/>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="brand">Brand:</label>
									<input type="text" class="form-control" name="brand" id="brand" value="<?php echo $stationary_data->brand;?>"/>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="unit">Unit<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="unit" id="unit" value="<?php echo $stationary_data->unit;?>"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="sb_rate">Rate<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="sb_rate" id="sb_rate" value="<?php echo $stationary_data->sb_rate;?>"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="HSN">HSN:</label>
									<input type="text" class="form-control" name="HSN" id="HSN" value="<?php echo $stationary_data->HSN;?>"/>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="HSN_code">HSN code<span class="mand"></span> :</label>
									<input type="text" class="form-control" name="HSN_code" id="HSN_code" value="<?php echo $stationary_data->HSN_code;?>"/>
								</div>
								
							
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="GST">GST:</label>
									<input type="text" class="form-control" name="GST" id="GST" value="<?php echo $stationary_data->GST;?>"/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="quantity">Quantity[Stock]<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="quantity" id="quantity" value="<?php echo $stationary_data->quantity;?>" readonly/>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="quantity">Quantity to Add:</label>
									<input type="text" required="required"  class="form-control" name="quantity_to_add" id="quantity_to_add"/>
								</div>
								
								
							</div>
							
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#update_stationary_item").validate({
		rules:{
			item_name: {required:true},
			unit: {required:true},
			sb_rate: {required:true}
			
		},
		messages: {
			"item_name": { required: "Please add Item Name" },
			"unit": { required: "Please enter Unit" },
			"sb_rate":{required: "Please enter SB Rate"}
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
});

</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>