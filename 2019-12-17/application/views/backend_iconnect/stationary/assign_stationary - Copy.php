
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Assign Stationary</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Assign Stationary</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Assign Stationary
						<a href="<?php echo base_url().'stationary/assign_stationary_userList';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'stationary/AssignStationaryItems'; ?>" method="POST" id="assignStationary" name="myform" novalidate>
							<div class="row">
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_name">User Name<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="user_name" id="user_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_empcode">User Empcode<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="user_empcode" id="user_empcode" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_email">User Email<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="user_email" id="user_email"/>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_lob">User LOB<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="user_lob" id="user_lob" />
								</div>
								
								
							</div>
						
							
							<div class="row">
								<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Other item" onclick="add_more()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
							</div>
							
							<div id="add_mor_div">
								
							</div>
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	console.log('working...');
	//$('#procured_dt').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	$('#procured_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#received_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#warranty_expire_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#po_created').datepicker({format:"dd-mm-yyyy"}).datepicker('setDate','now');
	
	$("#assignStationary").validate({
		rules:{
			user_name: {required:true},
			user_empcode: {required:true},
			user_email: {required:true},
			user_lob: {required:true},
			number:{required:true}
		},
		messages: {
			"user_name": { required: "Please enter user name." },
			"user_empcode": { required: "Please enter emp code." },
			"user_email": { required: "Please enter user email." },
			"user_lob": { required: "Please enter user LOB." },
			"number": { required: "Please enter number." }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
});


var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>Items</b>&nbsp;&nbsp;&nbsp;&nbsp;<div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="number">Sr.Number :</label><input type="text" class="form-control" name="number[]" id="number" required="required"/></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="item_code">Item Code :</label><input type="text" class="form-control" name="item_code[]" id="item_code"/></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="item_name">Item name :</label><input type="text" class="form-control" name="item_name[]" id="item_name"/></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="quantity">Quantity :</label><input type="text" class="form-control" name="quantity[]" id="quantity" /></div><br></div>';
	$("#add_mor_div").append(add_more_str);
	add_more_count++;
}

function remove_option(div_id){
	$("#"+div_id).remove();
}

function getLocation(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#procured_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'" data-shortCode="'+rkObj.objData[rk].city_code+'">'+rkObj.objData[rk].site_location_name+'</option>';
			}
			$("#procured_location").html(rkHtm);
		}
	});
}

function getHod(){
	$.ajax({
		data:"objCode=getHod&lobId="+$("#procured_lob").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select HOD</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'">'+rkObj.objData[rk].hod_name+'</option>';
			}
			$("#approved_hod").html(rkHtm);
		}
	});
}

function getExpiryDt(){
	$.ajax({
		data:"objCode=getExpiryDate&warnty_month="+$("#warranty_expire_month").val()+"&procur_dt="+$("#procured_dt").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			console.log(rkObj);
			$("#expiry_dt").val(rkObj.objData);
		}
	});
}

//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>