
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Assign Stationary</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Assign Stationary</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Assign Stationary
						<a href="<?php echo base_url().'stationary/assign_stationary_userList';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'stationary/AssignStationaryItems'; ?>" method="POST" id="assignStationary" name="myform" novalidate>
							<div class="row">
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_name">User Name<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="user_name" id="user_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_empcode">User Empcode<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="user_empcode" id="user_empcode" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_email">User Email<span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="user_email" id="user_email"/>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="user_lob">User LOB<span class="mand">*</span> :</label>
									<!--<input type="text" required="required" class="form-control" name="user_lob" id="user_lob" />-->
									<select id="user_lob" name="user_lob" class="form-control select2-offscreen">
											<option value="">Please select LOB</option>
											<?php $AllLob = get_master_LOB(); foreach($AllLob as $lob){ ?>
												<option value="<?php echo $lob->lob_name; ?>"><?php echo $lob->lob_name; ?></option>
											<?php } ?>
									</select>
								</div>
								
								
							</div>
							
							<div class="row">
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="number">Sr. No.<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="number[]" id="number" value="1"/>
								</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">Item Name<span class="mand">*</span> :</label>
									
										 <select id="item_name" name="item_name[]" class="form-control select2-offscreen" onchange="mainInfoone(this.value);">
											<option value="">Please select</option>
											<?php $item_name = get_item_name(); foreach($item_name as $items){ ?>
												<option value="<?php echo $items->item_code; ?>"><?php echo $items->item_name; ?></option>
											<?php } ?>
										 </select>
										
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="unit">Unit<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="unit1" id="unit1" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">quantity<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="quantity[]" id="quantity" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="assign_date">Assign date<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="assign_date[]" id="assign_date1" />
							</div>
							</div>
						
							
							<div class="row">
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="number">Sr. No.<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="number[]" id="number" value="2"/>
								</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">Item Name<span class="mand">*</span> :</label>
									
										 <select id="item_name" name="item_name[]" class="form-control select2-offscreen" onchange="mainInfotwo(this.value);">
											<option value="">Please select</option>
											<?php $item_name = get_item_name(); foreach($item_name as $items){ ?>
												<option value="<?php echo $items->item_code; ?>"><?php echo $items->item_name; ?></option>
											<?php } ?>
										 </select>
										
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="unit">Unit<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="unit2" id="unit2" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">quantity<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="quantity[]" id="quantity" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="assign_date">Assign date<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="assign_date[]" id="assign_date2" />
							</div>
							</div>
							<div class="row">
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="number">Sr. No.<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="number[]" id="number" value="3"/>
								</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">Item Name<span class="mand">*</span> :</label>
									
										 <select id="item_name" name="item_name[]" class="form-control select2-offscreen" onchange="mainInfothree(this.value);">
											<option value="">Please select</option>
											<?php $item_name = get_item_name(); foreach($item_name as $items){ ?>
												<option value="<?php echo $items->item_code; ?>"><?php echo $items->item_name; ?></option>
											<?php } ?>
										 </select>
										
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="unit">Unit<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="unit3" id="unit3" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">quantity<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="quantity[]" id="quantity" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="assign_date">Assign date<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="assign_date[]" id="assign_date3" />
							</div>
							</div>
							<div class="row">
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="number">Sr. No.<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="number[]" id="number" value="4"/>
								</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">Item Name<span class="mand">*</span> :</label>
									
										 <select id="item_name" name="item_name[]" class="form-control select2-offscreen" onchange="mainInfofour(this.value);">
											<option value="">Please select</option>
											<?php $item_name = get_item_name(); foreach($item_name as $items){ ?>
												<option value="<?php echo $items->item_code; ?>"><?php echo $items->item_name; ?></option>
											<?php } ?>
										 </select>
										
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="unit">Unit<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="unit4" id="unit4" />
							</div>
							
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">quantity<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="quantity[]" id="quantity" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="assign_date">Assign date<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="assign_date[]" id="assign_date4" />
							</div>
							</div>
						    <div class="row">
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="number">Sr. No.<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="number[]" id="number" value="5"/>
								</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">Item Name<span class="mand">*</span> :</label>
									
										 <select id="item_name" name="item_name[]" class="form-control select2-offscreen" onchange="mainInfofive(this.value);">
											<option value="">Please select</option>
											<?php $item_name = get_item_name(); foreach($item_name as $items){ ?>
												<option value="<?php echo $items->item_code; ?>"><?php echo $items->item_name; ?></option>
											<?php } ?>
										 </select>
										
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="unit">Unit<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="unit5" id="unit5" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="user_lob">quantity<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="quantity[]" id="quantity" />
							</div>
							<div class="form-group col-xs-12 col-sm-12 col-md-2">
									<label data-lblname="assign_date">Assign date<span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" name="assign_date[]" id="assign_date5" />
							</div>
							</div>
							<!--
							<div class="row">
								<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Other item" onclick="add_more()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
							</div>
							
							<div id="add_mor_div">
								
							</div>-->
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	console.log('working...');
	//$('#procured_dt').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	$('#assign_date1').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#assign_date2').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#assign_date3').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#assign_date4').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#assign_date5').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	
	$('#received_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#warranty_expire_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#po_created').datepicker({format:"dd-mm-yyyy"}).datepicker('setDate','now');
	
	$("#assignStationary").validate({
		rules:{
			user_name: {required:true},
			user_empcode: {required:true},
			user_email: {required:true,email:true},
			user_lob: {required:true},
			number: {required:true}
		},
		messages: {
			"user_name": { required: "Please enter user name." },
			"user_empcode": { required: "Please enter emp code." },
			"user_email": { required: "Please enter user email." },
			"user_lob": { required: "Please enter user LOB." },
			"number": { required: "Please enter number." }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
});


var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>Items</b>&nbsp;&nbsp;&nbsp;&nbsp;<div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="number">Sr.Number :</label><input type="text" class="form-control" name="number[]" id="number" required="required"/></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="item_code">Item Code :</label><input type="text" class="form-control" name="item_code[]" id="item_code"/></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="item_name">Item name :</label><input type="text" class="form-control" name="item_name[]" id="item_name"/></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="quantity">Quantity :</label><input type="text" class="form-control" name="quantity[]" id="quantity" /></div><br></div>';
	$("#add_mor_div").append(add_more_str);
	add_more_count++;
}

function remove_option(div_id){
	$("#"+div_id).remove();
}
function mainInfoone(id) {
	  $.ajax({
        //data: "ItemCode ="+ id,
		data:"objCode=getUnit&itemCode="+id,
		url:"<?php echo base_url().'stationary/'; ?>getObject",
		type:"POST",
        success: function(resp) {
			rkObj = JSON.parse(resp);
			//console.log(rkObj.objData);
			$("#unit1").val(rkObj.objData.unit);
        }
    });
}

function mainInfotwo(id) {
	  $.ajax({
        //data: "ItemCode ="+ id,
		data:"objCode=getUnit&itemCode="+id,
		url:"<?php echo base_url().'stationary/'; ?>getObject",
		type:"POST",
        success: function(resp) {
			rkObj = JSON.parse(resp);
			//console.log(rkObj.objData);
			$("#unit2").val(rkObj.objData.unit);
        }
    });
}
function mainInfothree(id) {
	  $.ajax({
        //data: "ItemCode ="+ id,
		data:"objCode=getUnit&itemCode="+id,
		url:"<?php echo base_url().'stationary/'; ?>getObject",
		type:"POST",
        success: function(resp) {
			rkObj = JSON.parse(resp);
			//console.log(rkObj.objData);
			$("#unit3").val(rkObj.objData.unit);
        }
    });
}
function mainInfofour(id) {
	  $.ajax({
        //data: "ItemCode ="+ id,
		data:"objCode=getUnit&itemCode="+id,
		url:"<?php echo base_url().'stationary/'; ?>getObject",
		type:"POST",
        success: function(resp) {
			rkObj = JSON.parse(resp);
			//console.log(rkObj.objData);
			$("#unit4").val(rkObj.objData.unit);
        }
    });
}
function mainInfofive(id) {
	  $.ajax({
        //data: "ItemCode ="+ id,
		data:"objCode=getUnit&itemCode="+id,
		url:"<?php echo base_url().'stationary/'; ?>getObject",
		type:"POST",
        success: function(resp) {
			rkObj = JSON.parse(resp);
			//console.log(rkObj.objData);
			$("#unit5").val(rkObj.objData.unit);
        }
    });
}

//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>