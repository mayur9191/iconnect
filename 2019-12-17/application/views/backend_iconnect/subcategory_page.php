<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Sub-Category</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_subcategory'; ?>" method="POST">
			  
			  <div class="form-group">
				<label ><b>Category Name : </b></label>
				<select class="form-control" name="cat_id">
					<?php 
						foreach(get_all_record_by_code('category') as $cat_record){
							if($cat_record->category_id == $subcategory_data->category_id){
								echo '<option value="'.$cat_record->category_id.'" selected>'.ucfirst($cat_record->category_name).'</option>';
							}else{
								echo '<option value="'.$cat_record->category_id.'">'.ucfirst($cat_record->category_name).'</option>';
							}
						}
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Sub-Category Name : </b></label>
				<input type="hidden" name="subcat_id" value="<?php echo $subcategory_data->sub_category_id; ?>" />
				<textarea  name="subcategory_txt" class="form-control"><?php echo $subcategory_data->sub_category_name; ?></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Sub-Category</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_subcategory'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>Category Name : </b></label>
				<select class="form-control" name="cat_id">
					<?php 
						foreach(get_all_record_by_code('category') as $cat_record){
							echo '<option value="'.$cat_record->category_id.'">'.ucfirst($cat_record->category_name).'</option>';
						}
					?>
				</select>
			  </div>
			  <div class="form-group">
				<label><b>Sub-Category Name : </b></label>
				<textarea  name="category_txt" class="form-control"></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Sub-Category</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Category Name : </b></label>
			<span><?php 
					$categoryData = get_record_by_id_code('category',$subcategory_data->category_id);
					$categoryName = $categoryData->category_name;
					echo $categoryName;
				?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Sub-Category Name : </b></label>
			<span><?php echo $subcategory_data->sub_category_name; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

