<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Daily task</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Daily task</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<?php if($this->session->flashdata('msg')): ?>
						<div class="alert alert-success alert-dismissable">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?> 
						</div>
						<?php endif; ?>
						<?php if($this->session->flashdata('error_msg')): ?>
							<div class="alert alert-danger alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> <?php echo $this->session->flashdata('error_msg'); ?> 
							</div>
						  <?php endif; ?>
					<div class="table-header">
						<?php echo $my_title; ?>
						<span class="pull-right" style="margin-right: 10px;margin-top: 3px;">
						<?php todayWorkingHrs(); ?>
						</span>
						<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Add Task" onclick="clear_modal()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
					</div>
					
					<div>
						<table  id="myTable" class="table table-striped table-bordered table-hover">
						<thead>
						  <tr>
							<th>Sr no</th>
							<th>Project name</th>
							<th>Start date</th>
							<th>End date</th>
							<th>Task activity</th>
							<th>Task priority</th>
							<th>Task status</th>
							<th>Working Time</th>
							<th>Remarks</th>
							<th>Action</th>
						  </tr>
						</thead>

						<tbody>
							<?php $i=1; 
								foreach($all_tasks as $task): ?>
								  <tr>
									<td><?php echo $i; ?></td>
									<td><?php echo ucfirst($task['project_name']); ?></td>
									<td><?php echo date('d-m-Y',strtotime($task['start_date'])); ?></td>
									<td><?php echo date('d-m-Y',strtotime($task['end_date'])); ?></td>
									<td><?php echo ucfirst($task['task_activity']); ?></td>
									<td><?php echo ucfirst($task['task_priority']); ?></td>
									<td>
									<?php 
											$task_status='';
											switch($task['task_status']){
											case 'not_started':
												$task_status = 'Not Started';
											break;
											case 'in_progress':
												$task_status = 'In-Process';
											break;
											case 'completed':
												$task_status = 'Completed';
											break;
											
									} echo $task_status; ?>
									</td>
									<td><?php echo str_pad($task['task_hrs'], 2, '0', STR_PAD_LEFT).':'.str_pad($task['task_min'], 2, '0', STR_PAD_LEFT); ?></td>
									<td><?php echo $task['remarks']; ?></td>
									<td>
									<a href="javascript:void(0)" data-taskstatus="<?php echo $task['task_status']; ?>" data-taskpriority="<?php echo $task['task_priority']; ?>" data-dailytask="<?php echo $task['task_id']; ?>" onclick="display_modal(this)" id="edit_<?php echo $i; ?>"><span class="glyphicon glyphicon-edit"></span></a>
									</td>
									<?php $i++; ?>
								  </tr>
								 <?php endforeach; ?> 

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	
	   <form action="<?php echo base_url() ?>admin/save_task" id="task_form" enctype="multipart/form-data" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			 <div class="modal-header" style="border-bottom:none;border-top:none;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			  </div>
			  <div class="modal-body" style="">
					<span  class="mand pull-right">All fields marked are Mandatory (*)&nbsp;&nbsp;&nbsp;&nbsp;<br></span>
					<input type="hidden" id="task_id" name="task_id">
					<div class="form-group col-sm-12 col-md-12">
						<label for="inputsm">Project name<span class="mand">*</span></label>
						<input class="form-control input-sm" name="project_name" id="project_name" type="text">
					</div>
					<div class="form-group col-sm-12 col-md-6">
						<label for="inputsm">Start date<span class="mand">*</span></label>
						<input class="form-control input-sm" name="start_date"  id="start_date" type="text">
					</div>
					<div class="form-group col-sm-12 col-md-6">
						<label for="inputsm">End date<span class="mand">*</span></label>
						<input class="form-control input-sm" name="end_date"  id="end_date" type="text">
					</div>
					<div class="form-group col-sm-12 col-md-12">
						<label for="inputsm">Task activity<span class="mand">*</span></label>
						<textarea class="form-control input-sm" name="task_activity"  id="task_activity"></textarea>
					</div>
					<div class="form-group col-sm-12 col-md-6">
						<label for="inputsm">Task priority<span class="mand">*</span></label>
						<select class="form-control input-sm" name="task_priority"  id="task_priority">
							<option value="">Please select</option>
							<option value="low">Low</option>
							<option value="medium">Medium</option>
							<option value="high">High</option>
						</select>
					</div>
					<div class="form-group col-sm-12 col-md-6">
						<label for="inputsm">Task status<span class="mand">*</span></label>
						<select class="form-control input-sm" name="task_status"  id="task_status">
							<option value="">Please select</option>
							<option value="not_started">Not Started</option>
							<option value="in_progress">In-Process</option>
							<option value="completed">Completed</option>
						</select>
					</div>
					
					
					
					<div class="form-group col-sm-12 col-md-6" id="hrs_v" style="display:none;">
						<label for="inputsm">Task working in hours<span class="mand">*</span></label>
						<input class="form-control input-sm" name="hrs_time" value="00" maxlength="3" id="hrs_time" type="number" style="width: 80px;" />
					</div>
					
					<div class="form-group col-sm-12 col-md-6" id="min_v" style="display:none;">
						<label for="inputsm">Task working in minutes<span class="mand">*</span></label>
						<input class="form-control input-sm" name="min_time" value="00" maxlength="3" id="min_time" type="number" style="width: 80px;" />
					</div>
					

					
					<div class="form-group col-sm-12 col-md-12">
						<label for="inputsm">Remarks</label>
						<textarea class="form-control input-sm" name="remarks"  id="remarks"></textarea>
					</div>
			  </div>
			  <div class="modal-footer">
				<input type="submit" value="Submit" name="submit_task" class="btn btn-primary">
			  </div>
			</div>
		  </div>
		</form>
	</div>


  <div class="modal-dialog" style="display:none;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
	  <form action="<?php echo base_url() ?>admin/save_task" id="task_form" enctype="multipart/form-data" method="post">
	 
      <div class="modal-body">
		<span  class="mand pull-right">All fields marked are Mandatory (*)&nbsp;&nbsp;&nbsp;&nbsp;</span>	
		<input type="hidden" id="task_id" name="task_id">
        <div class="form-group col-sm-12 col-md-12">
			<label for="inputsm">Project name<span class="mand">*</span></label>
			<input class="form-control input-sm" name="project_name" id="project_name" type="text">
		</div>
		
		<div class="form-group col-sm-12 col-md-6">
			<label for="inputsm">Start date<span class="mand">*</span></label>
			<input class="form-control input-sm" name="start_date" value="<?php echo date('d-m-Y'); ?>" id="start_date" type="text">
		</div>
		
		<div class="form-group col-sm-12 col-md-6">
			<label for="inputsm">End date<span class="mand">*</span></label>
			<input class="form-control input-sm" name="end_date" value="<?php echo date('d-m-Y'); ?>"  id="end_date" type="text">
		</div>
		
		<div class="form-group col-sm-12 col-md-12">
			<label for="inputsm">Task activity<span class="mand">*</span></label>
			<textarea class="form-control input-sm" name="task_activity"  id="task_activity"></textarea>
		</div>
		
		<div class="form-group col-sm-12 col-md-6">
			<label for="inputsm">Task priority<span class="mand">*</span></label>
			<select class="form-control input-sm" name="task_priority"  id="task_priority">
				<option value="">Please select</option>
				<option value="low">Low</option>
				<option value="medium">Medium</option>
				<option value="high">High</option>
			</select>
		</div>
		<div class="form-group col-sm-12 col-md-6">
			<label for="inputsm">Task status<span class="mand">*</span></label>
			<select class="form-control input-sm" name="task_status"  id="task_status">
				<option value="">Please select</option>
				<option value="not_started">Not Started</option>
				<option value="in_progress">In-Process</option>
				<option value="completed">Completed</option>
			</select>
		</div>
		
		<div class="form-group col-sm-12 col-md-6" id="hrs_v" style="display:none;">
			<label for="inputsm">Task working in hours<span class="mand">*</span></label>
			<input class="form-control input-sm" name="hrs_time" value="00" maxlength="3" id="hrs_time" type="number" style="width: 80px;" />
		</div>
		
		<div class="form-group col-sm-12 col-md-6" id="min_v" style="display:none;">
			<label for="inputsm">Task working in minutes<span class="mand">*</span></label>
			<input class="form-control input-sm" name="min_time" value="00" maxlength="3" id="min_time" type="number" style="width: 80px;" />
		</div>
		
		<div class="form-group col-sm-12 col-md-6">
			<label for="inputsm">Remarks</label>
			<textarea class="form-control input-sm" name="remarks"  id="remarks"></textarea>
		</div>
		<div class="form-group">
			<div class="col-sm-8">
			<input type="submit"value="Submit" name="submit_task" class="btn btn-primary center-block">
			</div>
		</div>
			
      </div>
    </form>  
    </div>
</div>


</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--datatable js--->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script>
function display_modal(obj)
{
	var o = $(obj).parent().parent();
	var project_name = $(o).find("td").eq('1').text();
	var start_date  =  $(o).find("td").eq('2').text();
	var end_date  =  $(o).find("td").eq('3').text();
	var task_activity = $(o).find("td").eq('4').text();
	var task_priority = $(obj).data('taskpriority');
	var task_status = $(obj).data('taskstatus');
	var remarks = $(o).find("td").eq('8').text();
	var work_time = $(o).find("td").eq('7').text();
	var res = work_time.split(":");
	var task_id = $(obj).data('dailytask');
	//alert(task_id);
	//alert(project_name+ start_date+end_date+task_activity+task_priority+task_status+remarks);
	$('#myModal').modal('show');
	$("#project_name").val(project_name);
	$("#start_date").val(start_date);
	$("#end_date").val(end_date);
	$("#task_activity").val(task_activity);
	$("#task_priority").val(task_priority);
	$("#task_status").val(task_status);
	$("#remarks").val(remarks);
	$("#task_id").val(task_id);
	$("#hrs_time").val();
	$("#min_time").val();
	$('.modal-title').html('Edit task');
	if(task_status =='completed'){
		$("#hrs_v").show();
		$("#min_v").show();
		$("#hrs_time").val(parseInt(res[0]));
		$("#min_time").val(parseInt(res[1]));
		console.log('work time = '+parseInt(res[0])+' - '+parseInt(res[1]));
	}
}
function clear_modal()
{
	$('#myModal').modal('show');
	$("#project_name").val('');
	$("#start_date").val('');
	$("#end_date").val('');
	$("#task_activity").val('');
	$("#task_priority").val('');
	$("#task_status").val('');
	$("#remarks").val('');
	$("#task_id").val('');
	$('#myModal').find('label,input').removeClass('error');
	$('.modal-title').html('Add task');
	
	$('#start_date').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	$('#start_date').val($.datepicker.formatDate("dd-mm-yy", new Date()));
	$('#end_date').datepicker({format: "dd-mm-yyyy",startDate:'now',endDate:'+1m'}).datepicker('setDate','now');
	$('#end_date').val($.datepicker.formatDate("dd-mm-yy", new Date())); 
}
$(document).ready(function(){
	$('#start_date').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	$('#end_date').datepicker({format: "dd-mm-yyyy",startDate:'now',endDate:'+1m'}).datepicker('setDate','now');
	
    // for datatable code goes here
	$('#myTable').DataTable({
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]]
	});
	// for validation code starts here
	$("#task_form").validate({
		 
			rules: {
				project_name:{
					required:true
					
				},
				start_date: {required:true},
				end_date: {required:true},
				task_activity:{required:true},
				task_priority:{required:true},
				task_status:{required:true},
				hrs_time:{required:true},
				min_time:{required:true}
			},
			 
			messages: {
				
				"project_name": {
					required: "Please enter the project name"                
				},
				"start_date": {
					required: "Please enter the start date"                
				},
				"end_date": {
					required: "Please enter the end date"                
				},
				"task_activity": {
					required: "Please enter the task activity"                
				},
				"task_priority": {
					required: "Please select the task priority"                
				},
				"task_status": {
					required: "Please select the task status"                
				},
				"hrs_time":{
					required: "Please add hrs",
				},
				"min_time":{
					required: "Please add min"
				}
				
			},
			submitHandler: function (form) { 
				form.submit();
			}
			
		});

	
		$("#task_status").on("change",function(){
			var vales = $("#task_status").val();
			if(vales =='completed'){
				$("#hrs_v").show();
				$("#min_v").show();
			}else{
				$("#hrs_v").hide();
				$("#min_v").hide();
			}
		});
        
});
</script>