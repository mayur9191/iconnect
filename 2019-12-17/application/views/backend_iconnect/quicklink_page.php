<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Quicklink</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_quicklink'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>Quicklink Name : </b></label>
				<input type="hidden" name="quicklink_id" value="<?php echo $quicklink_data->quick_link_id; ?>" />
				<textarea  name="quicklink_name" class="form-control"><?php echo $quicklink_data->quick_link_name; ?></textarea>
			  </div>
			  <div class="form-group">
				<label ><b>Quicklink URL : </b></label>
				<textarea  name="quicklink_url" class="form-control"><?php echo $quicklink_data->quick_link_url; ?></textarea>
			  </div>
			  <div class="form-group">
				<label ><b>Quicklink Description : </b></label>
				<textarea  name="quicklink_desc" class="form-control"><?php echo $quicklink_data->quick_link_desc; ?></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Quicklink</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_quicklink'; ?>" method="POST">
			
			  <div class="form-group">
				<label ><b>Quicklink Name : </b></label>
				<textarea  name="quicklink_name" class="form-control"><?php echo $quicklink_data->quick_link_name; ?></textarea>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Quicklink URL : </b></label>
				<textarea  name="quicklink_url" class="form-control"><?php echo $quicklink_data->quick_link_url; ?></textarea>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Quicklink Description : </b></label>
				<textarea  name="quicklink_desc" class="form-control"><?php echo $quicklink_data->quick_link_desc; ?></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Quicklink</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Quicklink Name : </b></label>
			<span><?php echo $quicklink_data->quick_link_name; ?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Quicklink URL : </b></label>
			<span><?php echo $quicklink_data->quick_link_url; ?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Quicklink Description : </b></label>
			<span><?php echo $quicklink_data->quick_link_desc; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

