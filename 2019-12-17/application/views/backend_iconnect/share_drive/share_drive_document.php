<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Share Drive Documents</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Share Drive Documents</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<?php if($this->session->flashdata('msg')): ?>
						<div class="alert alert-success alert-dismissable">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?> 
						</div>
						<?php endif; ?>
						<?php if($this->session->flashdata('error_msg')): ?>
							<div class="alert alert-danger alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> <?php echo $this->session->flashdata('error_msg'); ?> 
							</div>
						  <?php endif; ?>
						  <div class="sidebox community_update" >
						<div class="iconnect-ps">
							<a href="#">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px; background-color: rgba(142, 183, 39, 0.99); color:#fff;">Latest Update's for Share drive</b>
								</h5>
							</a>
						</div>
	
	
						<div style="height:85px;overflow-y:hidden;">
						
							<marquee  behavior="scroll" direction="up" height="85" scrolldelay="100" scrollamount="2" onMouseOver="this.stop()" onMouseOut="this.start()">
								<table width="100%" >
								<?php foreach($latest_share_drive_data as $announcements_record) { ?>
								<tr>
								<td width="100%" style="padding: 5px 5px 5px 5px;"><a href="<?php echo base_url().'uploads/announcement_tv/share/'.$announcements_record->folder_name.'/'.$announcements_record->an_file; ?>" target="_blank" style="font-size:15px;" download> <?php echo $announcements_record->an_file; ?> </a></td>
								</tr>
								<?php } ?>	
								</table>
							</marquee>
						
						</div>
	
					</div>
					<div class="table-header">
						<?php echo $my_title; ?>
						
						<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Add Document" onclick="clear_modal()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
					</div>
					
					<div>
						<table  id="myTable" class="table table-striped table-bordered table-hover">
						<thead>
						  <tr>
							<tr>
									<th>Document</th>
									<th>Created by</th>
									<th>Date</th>
									<th></th>
									
								
						  </tr>
						</thead>

						<tbody>
							<?php foreach($share_drive_data as $announcements_record) { ?>
								<tr><!-- width="30%" -->
									<td><?php echo $announcements_record->folder_name; ?> >> <a href="<?php echo base_url().'uploads/announcement_tv/share/'.$announcements_record->folder_name.'/'.$announcements_record->an_file; ?>" target="_blank" download><?php echo $announcements_record->an_file; ?></td>
									<td>
										<?php echo $announcements_record->an_created_by; ?>
									</td>

									<td>
										<?php echo $announcements_record->an_date; ?>								
									</td>
									<td>
									<a class="green" href="#" onclick="edit_record('<?php echo $announcements_record->an_id; ?>');" title="Edit">
												<i class="ace-icon fa fa-pencil bigger-130"></i>
											</a>&nbsp;&nbsp;
									<a href="#" onclick="remove_record('<?php echo $announcements_record->an_id; ?>');" title="Remove Document">
												<i class="ace-icon fa fa-trash-o bigger-130"></i>
											</a>
									</td>
								</tr>
							<?php } ?>

						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	
	   <form action="<?php echo base_url() ?>admin/added_share_document" id="task_form" enctype="multipart/form-data" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			 <div class="modal-header" style="border-bottom:none;border-top:none;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			  </div>
			  <div class="modal-body" style="">
					<span  class="mand pull-right">All fields marked are Mandatory (*)&nbsp;&nbsp;&nbsp;&nbsp;<br></span>
					<div class="form-group col-sm-12 col-md-12">
						<label for="inputsm">Add Folder<span class="mand">*</span></label>
						<input class="form-control input-sm" name="folderName" id="folderName" type="text">
						
					</div>
					<div class="form-group col-sm-12 col-md-6">
						<label for="inputsm">Add File<span class="mand">*</span></label>
						<input type="file" name="announcement_file" id="announcement_file" style="display:inline;">
					</div>
				
			  </div>
			  <div class="modal-footer">
				<input type="submit" value="Submit" name="submit_task" class="btn btn-primary">
			  </div>
			</div>
		  </div>
		</form>
	</div>
	 <div class="modal fade" id="myEditModal" role="dialog">
	
  </div>


</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--datatable js--->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script>
function edit_record(recd_id){
	$('#myEditModal').html('');
	console.log('record Id '+recd_id);
	$("#myEditModal").modal('show');
	 $.ajax({
		 url: "<?php echo base_url() ?>admin/edit_upload_announcements",
		 type:"post",
		 data:{"recd_id":recd_id},
		  success:function(r){
			$('#myEditModal').html(r);
			$('#myEditModal').modal('show');
		  },error:function(){
		  }
	 });
	 return false;
}
function remove_record(recd_id){
	if(confirm("Are you sure you wish to delete"))
	{
	   $.ajax({
		   url: "<?php echo base_url() ?>admin/remove_share_drive_doc",
		   type:"post",
		   data:{"recd_id":recd_id},
		   dataType:'text',
		   success:function(r){
			   var response = $.trim(r);
			   //console.log(response);
			   if(response == "success"){
				   location.reload();
			   }
			   else{
				   alert("Failed to delete the Record");
			   }
		   },error:function(){
			   
		   }
	   });	
	}
	else
	return false;	
}
function clear_modal()
{
	$('#myModal').modal('show');
	$("#folderName").val('');
	$("#announcement_file").val('');
	$('#myModal').find('label,input').removeClass('error');
	$('.modal-title').html('Add Document');
	
	
}
$(document).ready(function(){
	
    // for datatable code goes here
	$('#myTable').DataTable({
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]]
	});
	// for validation code starts here
	$("#task_form").validate({
		 
			rules: {
				folderName:{
					required:true
					
				},
				announcement_file: {required:true}
				
			},
			 
			messages: {
				
				"folderName": {
					required: "Please add folder name"                
				},
				"announcement_file": {
					required: "Please select file"                
				}
				
			},
			submitHandler: function (form) { 
				form.submit();
			}
			
		});

	
		$("#task_status").on("change",function(){
			var vales = $("#task_status").val();
			if(vales =='completed'){
				$("#hrs_v").show();
				$("#min_v").show();
			}else{
				$("#hrs_v").hide();
				$("#min_v").hide();
			}
		});
        
});
</script>