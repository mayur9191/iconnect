<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Document</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_archivefiles'; ?>" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="archive_id" value="<?php echo $archive_data->doc_folder_id; ?>" />
				<input type="hidden" name="doc_file_id" value="<?php echo $archivefiles_data->doc_file_id; ?>" />
			  <div class="form-group">
				<label><b>Archive Name : </b></label>
				<?php echo $archive_data->doc_folder_txt; ?>
			  </div>
			   <div class="form-group">
				<label><b>Document name : </b></label>
				<input type="text" name="archivefiles_name" value="<?php echo $archivefiles_data->doc_file_name; ?>" class="form-control" />
			  </div>
			  <div class="form-group">
				<label><b>File input : </b></label>
				
				<input type="file" name="archivefiles_file"  class="form-control" />
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Document</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_archivefiles'; ?>" method="POST" enctype="multipart/form-data">
			   <div class="form-group">
				<label><b>Archive Name : </b></label>
				<?php echo $archive_data->doc_folder_txt; ?>
			  </div>
			   <div class="form-group">
				<label><b>File name : </b></label>
				<input type="text" name="archivefiles_name" class="form-control" />
			  </div>
			  <div class="form-group">
				<label><b>File input : </b></label>
				<input type="hidden" name="archive_id" value="<?php echo $archive_data->doc_folder_id; ?>" />
				<input type="file" name="archivefiles_file"  class="form-control" />
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Document</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label><b>Archive Name : </b></label>
			<?php echo $archive_data->doc_folder_txt; ?>
		  </div>
		   <div class="form-group">
			<label><b>Document name : </b></label>
			<?php echo $archivefiles_data->doc_file_name; ?>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

