<div class="main-content" data-key="<?php echo $asset_data->asset_id; ?>" data-key-receive="<?php echo $asset_data->asset_id.'_'.$assign_asset_id; ?>" id="add_more_div_<?php echo $asset_data->asset_id; ?>">
	<div class="main-content-inner">
		
		<div class="page-content" style="border:1px solid red;margin-top:5px;">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					
					<div>
						
							<div class="row">
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type" disabled="desabled" readonly="readonly" style="display:none;">
										<option value="">Please select</option>
										<?php foreach(inv_assetType() as $assetType){
											if($asset_data->asset_type == $assetType->asset_type_id){
												echo '<option value="'.$assetType->asset_type_id.'" selected>'.$assetType->asset_type_name.'</option>';
												$astName = $assetType->asset_type_name;
											}else{
												echo '<option value="'.$assetType->asset_type_id.'">'.$assetType->asset_type_name.'</option>';
											}
										} ?>
									</select>
									<input type="text" required="required"  class="form-control" value="<?php echo $astName; ?>" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="system_generated_code">System Generated Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->asset_system_generated_code; ?>" readonly="readonly" disabled="desabled" name="system_generated_code" id="system_generated_code" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="asset_name">Asset Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="asset_name" value="<?php echo $asset_data->asset_name; ?>" id="asset_name" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="present_asset_code">Present Asset Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="present_asset_code" value="<?php echo $asset_data->present_asset_code; ?>" id="present_asset_code" readonly="readonly" disabled="desabled" />
								</div>
							</div>
							
							
							<div class="row">
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="nav_asset_code">NAV Asset code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" value="<?php echo $asset_data->nav_code; ?>" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="procured_dt">Procured date <span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" onchange="getExpiryDt();" name="procured_dt" id="procured_dt" value="<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="warranty_expire_month">Warranty Expiry month <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getExpiryDt();" name="warranty_expire_month" id="warranty_expire_month" readonly="readonly" disabled="desabled" >
										<option value="">Please select</option>
										<option value="1" <?php echo ($asset_data->warranty_expiry_no_month == 1)? 'selected' : ''; ?> >1 Month</option>
										<option value="2" <?php echo ($asset_data->warranty_expiry_no_month == 2)? 'selected' : ''; ?> >2 Month</option>
										<option value="3" <?php echo ($asset_data->warranty_expiry_no_month == 3)? 'selected' : ''; ?> >3 Month</option>
										<option value="4" <?php echo ($asset_data->warranty_expiry_no_month == 4)? 'selected' : ''; ?> >4 Month</option>
										<option value="5" <?php echo ($asset_data->warranty_expiry_no_month == 5)? 'selected' : ''; ?> >5 Month</option>
										<option value="6" <?php echo ($asset_data->warranty_expiry_no_month == 6)? 'selected' : ''; ?> >6 Month</option>
										<option value="7" <?php echo ($asset_data->warranty_expiry_no_month == 7)? 'selected' : ''; ?> >7 Month</option>
										<option value="8" <?php echo ($asset_data->warranty_expiry_no_month == 8)? 'selected' : ''; ?> >8 Month</option>
										<option value="9" <?php echo ($asset_data->warranty_expiry_no_month == 9)? 'selected' : ''; ?> >9 Month</option>
										<option value="10" <?php echo ($asset_data->warranty_expiry_no_month == 10)? 'selected' : ''; ?> >10 Month</option>
										<option value="11" <?php echo ($asset_data->warranty_expiry_no_month == 11)? 'selected' : ''; ?> >11 Month</option>
										<option value="12" <?php echo ($asset_data->warranty_expiry_no_month == 12)? 'selected' : ''; ?> >1 Year</option>
										<option value="24" <?php echo ($asset_data->warranty_expiry_no_month == 24)? 'selected' : ''; ?> >2 Year</option>
										<option value="36" <?php echo ($asset_data->warranty_expiry_no_month == 36)? 'selected' : ''; ?> >3 Year</option>
										<option value="48" <?php echo ($asset_data->warranty_expiry_no_month == 48)? 'selected' : ''; ?> >4 Year</option>
										<option value="60" <?php echo ($asset_data->warranty_expiry_no_month == 60)? 'selected' : ''; ?> >5 Year</option>
										<option value="72" <?php echo ($asset_data->warranty_expiry_no_month == 72)? 'selected' : ''; ?> >6 Year</option>
									</select>
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="warranty_type">Warranty Type <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="warranty_type" value="<?php echo $asset_data->warranty_type; ?>" id="warranty_type" readonly="readonly" disabled="desabled" />
								</div>								
							</div>
							
							<div class="row">
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="expiry_dt">Expiry Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" value="<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>" readonly="readonly" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="received_dt">Received date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo date('d-m-Y',strtotime($asset_data->received_date)); ?>" name="received_dt" id="received_dt" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="procured_lob">Procured LOB <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getHod_ajx();" name="procured_lob" id="procured_lob" readonly="readonly" disabled="desabled" style="display:none;">
										<option value="">Select LOB</option>
										<?php foreach(inv_lob() as $invLob){
											if($invLob->lob_id == $asset_data->procured_lob_emp_code){
												echo '<option value="'.$invLob->lob_id.'" selected>'.$invLob->lob_name.'</option>';
												$compName = $invLob->lob_name;
											}else{												
												echo '<option value="'.$invLob->lob_id.'">'.$invLob->lob_name.'</option>';
											}
										} ?>
									</select>
									<input type="text" required="required"  class="form-control" value="<?php echo $compName; ?>" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="approved_hod">Approved LOB, HOD <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="approved_hod" id="approved_hod" readonly="readonly" disabled="desabled">
										<option value=""><?php echo getNameByCodeId('getHodName',$asset_data->approved_lob_hod_emp_code); ?></option>
									</select>
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3" disabled="desabled">
									<label data-lblname="procured_company">Procured Company <span class="mand">*</span> :</label>
									<!--<input type="text" required="required"  class="form-control" name="procured_company" value="<?php //echo $asset_data->procured_company; ?>" id="procured_company" readonly="readonly" disabled="desabled" />-->
									
									<select class="form-control" required="required" name="procured_company" id="procured_company" readonly="readonly" disabled="desabled" >
									<option value="">Select Company</option>
									<?php foreach(inv_company() as $companyRecord){
										if($asset_data->procured_company == $companyRecord->company_id){
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'" selected>'.$companyRecord->company_name.'</option>';
										}else{
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
										}
									?>
									<?php } ?>
									</select>
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="procured_state">Procured State <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getLocation_ajx();" name="procured_state" id="procured_state" readonly="readonly" disabled="desabled">
										<option value="">Select State</option>
										<?php foreach(inv_states() as $invState){
											if($invState->state_id == $asset_data->procured_state){
												echo '<option value="'.$invState->state_id.'" selected>'.$invState->state_name.'</option>';
											}else{
												echo '<option value="'.$invState->state_id.'">'.$invState->state_name.'</option>';
											}
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="procured_location">Procured Location <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="procured_location" id="procured_location" readonly="readonly" disabled="desabled">
										<option value=""><?php echo getNameByCodeId('getLocationName',$asset_data->procured_location); ?></option>
									</select>
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="manufacturer">Manufacturer <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="manufacturer" value="<?php echo $asset_data->manufacturer; ?>" id="manufacturer" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="model_no">Model No. <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="model_no" value="<?php echo $asset_data->modal_no; ?>" id="model_no" readonly="readonly" disabled="desabled" />
								</div>
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="serial_no">Serial Number <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="serial_no" value="<?php echo $asset_data->serial_number; ?>" id="serial_no" readonly="readonly" disabled="desabled" />
								</div>
								
								<div class="form-group col-xs-3 col-sm-3 col-md-3" style="display:none;">
									<label data-lblname="warranty_expire_dt">Warranty Expiry Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="warranty_expire_dt" value="<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>" id="warranty_expire_dt" readonly="readonly" disabled="desabled" />
								</div>
								<!-- new fields -->
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="po_created">PO Created <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>" name="po_created" id="po_created" readonly="readonly" disabled="desabled" />
								</div>
								
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="po_number">PO No. <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->po_numr; ?>" name="po_number" id="po_number" readonly="readonly" disabled="desabled" />
								</div>
								
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="po_name">Vendor Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->vendor_name; ?>" name="po_name" id="po_name" readonly="readonly" disabled="desabled" />
								</div>
								
								<div class="form-group col-xs-3 col-sm-3 col-md-3">
									<label data-lblname="description">Description <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="description" id="description" readonly="readonly" disabled="desabled"><?php echo $asset_data->description; ?></textarea>
								</div>
								
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-12">
									<label data-lblname="remark">Remark <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="remark" id="remark" readonly="readonly" disabled="desabled"><?php echo $asset_data->asset_remark; ?></textarea>
								</div>
							</div>
							<div class="row">
								
							</div>
							
							<div id="add_mor_div">
								<?php foreach(inv_sub_item($asset_data->asset_id) as $sub_invet){ ?>
									
									<div class="row" id="add_more_div_1111" style="border: 1px solid #393939;margin-top: 5px;">
									<input type="hidden" name="asset_sub_id[]" value="<?php echo ($sub_invet->inv_sub_asset !='')?$sub_invet->inv_sub_asset:''; ?>" />
										<!--
										<div class="row">
											<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option('add_more_div_1111')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span>
										</div>
										-->
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="key_id">Key :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->key_id !='')?$sub_invet->key_id:''; ?>"  name="key_id[]" id="key_id" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="processor">Processor :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->processor !='')?$sub_invet->processor:''; ?>" name="processor[]" id="processor" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="ram_id">RAM :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->ram !='')?$sub_invet->ram:''; ?>" name="ram_id[]" id="ram_id" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="hdd_id">HDD :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->hdd !='')?$sub_invet->hdd:''; ?>" name="hdd_id[]" id="hdd_id" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="os_id">OS :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->os_name !='')?$sub_invet->os_name:''; ?>" name="os_id[]" id="os_id" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="os_key">OS Key :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->os_key !='')?$sub_invet->os_key:''; ?>" name="os_key[]" id="os_key" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="msoffice_id">MS Office :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->ms_office_name !='')?$sub_invet->ms_office_name:''; ?>" name="msoffice_id[]" id="msoffice_id" readonly="readonly" disabled="desabled" />
										</div>
										<div class="form-group col-xs-3 col-sm-3 col-md-3">
											<label data-lblname="msoffice_key">MS Office Key :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->ms_office_key !='')?$sub_invet->ms_office_key:''; ?>" name="msoffice_key[]" id="msoffice_key" readonly="readonly" disabled="desabled" />
										</div>
									   <br>
									</div>
								<?php } ?>
								
							</div>
							
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

