
<div class="main-content">
<style>
.inbox .inbox-menu ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .inbox-menu ul li {
    height: 30px;
    padding: 5px 15px;
    position: relative
}

.inbox .inbox-menu ul li:hover,
.inbox .inbox-menu ul li.active {
    background: #e4e5e6
}

.inbox .inbox-menu ul li.title {
    margin: 20px 0 -5px 0;
    text-transform: uppercase;
    font-size: 10px;
    color: #d1d4d7
}

.inbox .inbox-menu ul li.title:hover {
    background: 0 0
}

.inbox .inbox-menu ul li a {
    display: block;
    width: 100%;
    text-decoration: none;
    color: #3d3f42
}

.inbox .inbox-menu ul li a i {
    margin-right: 10px
}

.inbox .inbox-menu ul li a .label {
    position: absolute;
    top: 10px;
    right: 15px;
    display: block;
    min-width: 14px;
    height: 14px;
    padding: 2px
}

.inbox ul.messages-list {
    list-style: none;
    margin: 15px -15px 0 -15px;
    padding: 15px 15px 0 15px;
    border-top: 1px solid #d1d4d7
}

.inbox ul.messages-list li {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    cursor: pointer;
    margin-bottom: 10px;
    padding: 10px
}

.inbox ul.messages-list li a {
    color: #3d3f42
}

.inbox ul.messages-list li a:hover {
    text-decoration: none
}

.inbox ul.messages-list li.unread .header,
.inbox ul.messages-list li.unread .title {
    font-weight: 700
}

.inbox ul.messages-list li:hover {
    background: #e4e5e6;
    border: 1px solid #d1d4d7;
    padding: 9px
}

.inbox ul.messages-list li:hover .action {
    color: #d1d4d7
}

.inbox ul.messages-list li .header {
    margin: 0 0 5px 0
}

.inbox ul.messages-list li .header .from {
    width: 49.9%;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .header .date {
    width: 50%;
    text-align: right;
    float: right
}

.inbox ul.messages-list li .title {
    margin: 0 0 5px 0;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .description {
    font-size: 12px;
    padding-left: 29px
}

.inbox ul.messages-list li .action {
    display: inline-block;
    width: 16px;
    text-align: center;
    margin-right: 10px;
    color: #d1d4d7
}

.inbox ul.messages-list li .action .fa-check-square-o {
    margin: 0 -1px 0 1px
}

.inbox ul.messages-list li .action .fa-square {
    float: left;
    margin-top: -16px;
    margin-left: 4px;
    font-size: 11px;
    color: #fff
}

.inbox ul.messages-list li .action .fa-star.bg {
    float: left;
    margin-top: -16px;
    margin-left: 3px;
    font-size: 12px;
    color: #fff
}

.inbox .message .message-title {
    margin-top: 30px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px
}

.inbox .message .header {
    margin: 20px 0 30px 0;
    padding: 10px 0 10px 0;
    border-top: 1px solid #d1d4d7;
    border-bottom: 1px solid #d1d4d7
}

.inbox .message .header .avatar {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    height: 34px;
    width: 34px;
    float: left;
    margin-right: 10px
}

.inbox .message .header i {
    margin-top: 1px
}

.inbox .message .header .from {
    display: inline-block;
    width: 50%;
    font-size: 12px;
    margin-top: -2px;
    color: #d1d4d7
}

.inbox .message .header .from span {
    display: block;
    font-size: 14px;
    font-weight: 700;
    color: #3d3f42
}

.inbox .message .header .date {
    display: inline-block;
    width: 29%;
    text-align: right;
    float: right;
    font-size: 12px;
    margin-top: 18px
}

.inbox .message .attachments {
    border-top: 3px solid #e4e5e6;
    border-bottom: 3px solid #e4e5e6;
    padding: 10px 0;
    margin-bottom: 20px;
    font-size: 12px
}

.inbox .message .attachments ul {
    list-style: none;
    margin: 0 0 0 -40px
}

.inbox .message .attachments ul li {
    margin: 10px 0
}

.inbox .message .attachments ul li .label {
    padding: 2px 4px
}

.inbox .message .attachments ul li span.quickMenu {
    float: right;
    text-align: right
}

.inbox .message .attachments ul li span.quickMenu .fa {
    padding: 5px 0 5px 25px;
    font-size: 14px;
    margin: -2px 0 0 5px;
    color: #d1d4d7
}

.inbox .contacts ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .contacts ul li {
    height: 30px;
    padding: 5px 15px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis!important;
    position: relative;
    cursor: pointer
}

.inbox .contacts ul li .label {
    display: inline-block;
    width: 6px;
    height: 6px;
    padding: 0;
    margin: 0 5px 2px 0
}

.inbox .contacts ul li:hover {
    background: #e4e5e6
}


.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}


.panel-success>.panel-heading {background-image:linear-gradient(to bottom,#c9da2c 0,#cbdb2b 100%)}
.with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {color:#ffffff}
.with-nav-tabs.panel-success .nav-tabs > li > a:hover{background-color:#ffffff; color:#1c6b94;}
.backtab {background-color:#1c6b94; color:#ffffff;}
.backtab:hover {background-color:#1c6b94; color:#ffffff;}
.mand{color:red;}
.dropdown-thin {
    height: 25px;
    padding-top: 2px;
}

.dep-btn {
	background-color: #c9da2b;
    color: #3c763d;
    float: left;
    margin-bottom: 10px;
    margin-top: 10px;
    margin-right: 10px;
}
.dep-btn_annou {
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
	font-size:12px;
}
.dep-btn_annou:hover{
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
}
.dep-btn_annou:active{
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
}
.dep-btn_annou:target{
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
}

.badge1 {
   position:relative;
}
.badge1[data-badge]:after {
   content:attr(data-badge);
   position:absolute;
   top:-10px;
   right:-9px;
   font-size:.7em;
   background:green;
   color:white;
   width:16px;height:16px;
   text-align:center;
   line-height:18px;
   border-radius:50%;
   box-shadow:0 0 1px #333;
}
.tooltiptext {
    display: none;
    width: 220px;
    background-color: #c9da2b;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 1s;
}
.badge1:hover .tooltiptext{
	 display: block;
     opacity: 1;
}
.panel-body{
	font-size:12px;
}
.left {
    float:left;
    width: 50%;
}
.left > ul {
	margin-left: 0px;
}
.right{
    float:right;
    width: 50%;
	height:140px;
}

#piechart1{
	height:140px;
}
</style>

	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Asset-LOB-Report</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					
					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Asset-LOB-Report
					</div>
					
					<div>
					
						
					<div class="col-md-3">
						<div class="panel panel-default">
						<span class="btn backtab btn-block">Total Asset</span>
						<div class="panel-body inbox-menu" style="font-size:13px;">	
						
						<div>
				
						
						<?php 
							
								echo "Total";echo"-";
								foreach(getReportData_new('getTotalAssetCount',array('')) as $TotalAssetCount){?>
								<a style="text-decoration:none;" onclick="ajax_get_result('total_asset_count','<?php echo $assetid['asset_type_id']; ?>','','')" href="#">
								<span><?php echo $TotalAssetCount['TotalAssetCount']; echo "&nbsp;";?></span>
								</a>
									
								<?php }
							
						?>
					</div>
				</div>
					</div>	
				</div>	
						
					
					<div class="col-md-3" style="width:2775px;">
						<div class="panel panel-default">
						<span class="btn backtab btn-block">Total Asset(LOB)</span>
						<div class="panel-body inbox-menu" style="font-size:13px;">	
						
						<div>
				
						<ul class="list-unstyled">
							<table style="float:right;margin-top:-96px;">
						<?php foreach($LOB_data as $lobRecod){ ?>
							
							
							<li style="width:633px;">
								<?php echo $lobRecod['lob_name'];

								?>
									<span class="pull-right">
										<tr>
										<?php foreach($asset_data as $asset){
											 foreach(getReportData_new('getLOBAssetCount',array('procure_lob'=>$lobRecod['lob_id'],'asset_type'=>$asset['asset_type_id'])) as $LOBAssetCount){
											?>
											<td>
										<!--<a style="text-decoration:none;" onclick="ajax_get_result('lob_asset','<?php echo $lobRecod['lob_name']; ?>','<?php echo $compnayID; ?>','<?php echo $assetType['asset_type_name']; ?>')" href="#">--> 	
										<span>&nbsp;<?php echo $asset['asset_short_code'];?>&nbsp;</span>
										<span style="background-color:#ffeb3b;color:#3c763d;border-radius:2px;" class="primary"><?php echo $LOBAssetCount['LOBAssetCount'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
										<!--</a>--> 
											</td>
										<?php }

											}?>
											</tr> 
									</span>
								
							</li>
						<?php } ?>
						</table>
						</ul>
						<?php 
							foreach($asset_data as $assetid){
								echo $assetid['asset_short_code'];echo"-";
								foreach(getReportData_new('getToalAssetCount',array('asset_type_id'=>$assetid['asset_type_id'])) as $TotalLobCount){?>
								<a style="text-decoration:none;" onclick="ajax_get_result('asset_count','<?php echo $assetid['asset_type_id']; ?>','','')" href="#">
								<span><?php echo $TotalLobCount['LOBTotalCount']; echo "&nbsp;";?></span>
								</a>
									
								<?php }
							}
						?>
					</div>
				</div>
					</div>	
				</div>
						<!-- Report page -->
						
				<div class="col-md-3" style="width:2701px;">
						<div class="panel panel-default">
						<span class="btn backtab btn-block">Total Asset(LOB)-Stock</span>
						<div class="panel-body inbox-menu" style="font-size:13px;">	
						
						<div>
				
						<ul class="list-unstyled">
							<table style="float:right;margin-top:-96px;">
						<?php foreach($LOB_data as $lobRecod){ ?>
							
							
							<li style="width:633px;">
								<?php echo $lobRecod['lob_name'];

								?>
									<span class="pull-right">
										<tr>
										<?php foreach($asset_data as $asset){
											 foreach(getReportData_new('getLOBAssetStockCount',array('procure_lob'=>$lobRecod['lob_id'],'asset_type'=>$asset['asset_type_id'])) as $LOBAssetStockCount){
											?>
											<td>
										<span>&nbsp;<?php echo $asset['asset_short_code'];?>&nbsp;</span>
										<span style="background-color:#ffeb3b;color:#3c763d;border-radius:2px;" class="primary"><?php echo $LOBAssetStockCount['LOBAssetStockCount'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
											</td>
										<?php }

											}?>
											</tr> 
									</span>
								
							</li>
						<?php } ?>
						</table>
						</ul>
						<?php 
							foreach($asset_data as $assetid){
								echo $assetid['asset_short_code'];echo"-";
								foreach(getReportData_new('getToalAssetStockCount',array('asset_type_id'=>$assetid['asset_type_id'])) as $TotalLobCount){?>
								<a style="text-decoration:none;" onclick="ajax_get_result('stock_asset_count','<?php echo $assetid['asset_type_id']; ?>','','')" href="#">
								<span><?php echo $TotalLobCount['LOBTotalStockCount']; echo "&nbsp;";?></span>
								</a>
									
								<?php }
							}
						?>
					</div>
				</div>
					</div>	
				</div>	
						
				<div class="col-md-3" style="width:2746px;display:none;">
						<div class="panel panel-default">
						<span class="btn backtab btn-block">Total Asset(LOB)-NAV Code</span>
						<div class="panel-body inbox-menu" style="font-size:13px;">	
						
						<div>
				
						<ul class="list-unstyled">
							<table style="float:right;margin-top:-96px;">
						<?php foreach($LOB_data as $lobRecod){ ?>
							
							
							<li style="width:633px;">
								<?php echo $lobRecod['lob_name'];

								?>
									<span class="pull-right">
										<tr>
										<?php foreach($asset_data as $asset){
											 foreach(getReportData_new('getLOBAssetNAVCodeCount',array('procure_lob'=>$lobRecod['lob_id'],'asset_type'=>$asset['asset_type_id'])) as $LOBAssetNAVCODECount){
											?>
											<td>
										<span>&nbsp;<?php echo $asset['asset_short_code'];?>&nbsp;</span>
										<span style="background-color:#ffeb3b;color:#3c763d;border-radius:2px;" class="primary"><?php echo $LOBAssetNAVCODECount['LOBAssetNAVCount'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
											</td>
										<?php }

											}?>
											</tr> 
									</span>
								
							</li>
						<?php } ?>
						</table>
						</ul>
						<?php 
							foreach($asset_data as $assetid){
								echo $assetid['asset_short_code'];echo"-";
								foreach(getReportData_new('getToalAssetNAVCount',array('asset_type_id'=>$assetid['asset_type_id'])) as $TotalNAVCount){?>
								<a style="text-decoration:none;" onclick="ajax_get_result('nav_asset_count','<?php echo $assetid['asset_type_id']; ?>','','')" href="#">
								<span><?php echo $TotalNAVCount['LOBTotalNAVCount']; echo "&nbsp;";?></span>
								</a>
									
								<?php }
							}
						?>
					</div>
				</div>
					</div>	
				</div>	
					
				
						
						
						<div class="col-md-3" style="display:none;">
						<div class="panel panel-default">
						<span class="btn backtab btn-block">Total Asset-inv add</span>
						<div class="panel-body inbox-menu" style="font-size:13px;">	
						
						<div>
				
						
						<?php 
							
								echo "Total";echo"-";
								foreach(getReportData_new('getTotalAssetCountinvadd',array('')) as $TotalAssetCount){?>
								<a style="text-decoration:none;" onclick="ajax_get_result('total_asset_countinv','<?php echo $assetid['asset_type_id']; ?>','','')" href="#">
								<span><?php echo $TotalAssetCount['TotalAssetCount']; echo "&nbsp;";?></span>
								</a>
									
								<?php }
							
						?>
					</div>
				</div>
					</div>	
				</div>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->


<!-- Modal -->
<div id="myModal_div" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width:100%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer" style="border-top:none;">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div>  


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--datatable js--->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	//console.log('working...');
	//$('#procured_dt').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	$('#procured_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#received_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#warranty_expire_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#po_created').datepicker({format:"dd-mm-yyyy"}).datepicker('setDate','now');
	
	$("#add_asset").validate({
		rules:{
			asset_type: {required:true},
			asset_name: {required:true},
			present_asset_code: {required:true},
			nav_asset_code: {required:true},
			procured_dt: {required:true},
			warranty_expire_month: {required:true},
			warranty_type: {required:true},
			received_dt: {required:true},
			procured_lob: {required:true},
			approved_hod: {required:true},
			procured_company: {required:true},
			procured_state: {required:true},
			procured_location: {required:true},
			manufacturer: {required:true},
			model_no: {required:true},
			serial_no: {required:true},
			po_created: {required:true},
			po_number: {required:true},
			po_name: {required:true},
			description: {required:true},
			remark: {required:true}
			
		},
		messages: {
			"asset_type": { required: "Please select one asset type" },
			"asset_name": { required: "Please enter asset name" },
			"present_asset_code": { required: "Please enter asset code" },
			"nav_asset_code": { required: "Please enter asset code" },
			"procured_dt": { required: "Please select procured date" },
			"warranty_expire_month": { required: "Please select warranty" },
			"warranty_type": { required: "Please enter warranty type" },
			"received_dt": { required: "Please select received date" },
			"procured_lob": { required: "Please select LOB" },
			"approved_hod": { required: "Please select HOD" },
			"procured_company": { required: "Please enter company" },
			"procured_state": { required: "Please select state" },
			"procured_location": { required: "Please select location" },
			"manufacturer": { required: "Please enter manufacturer" },
			"model_no": { required: "Please enter model no." },
			"serial_no": { required: "Please enter serial no." },
			"po_created": { required: "Please select PO created date" },
			"po_number": { required: "Please enter PO number" },
			"po_name": { required: "Please enter vendor name" },
			"description": { required: "Please enter description" },
			"remark": { required: "Please enter remark" }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
});
var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;"><div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="key_id">Key :</label><input type="text" class="form-control" name="key_id[]" id="key_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="processor">Processor :</label><input type="text" class="form-control" name="processor[]" id="processor" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="ram_id">RAM :</label><input type="text" class="form-control" name="ram_id[]" id="ram_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="hdd_id">HDD :</label><input type="text" class="form-control" name="hdd_id[]" id="hdd_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="os_id">OS :</label><input type="text" class="form-control" name="os_id[]" id="os_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="os_key">OS Key :</label><input type="text" class="form-control" name="os_key[]" id="os_key" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="msoffice_id">MS Office :</label><input type="text" class="form-control" name="msoffice_id[]" id="msoffice_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="msoffice_key">MS Office Key :</label><input type="text" class="form-control" name="msoffice_key[]" id="msoffice_key" /></div><br></div>';
	$("#add_mor_div").append(add_more_str);
	add_more_count++;
}

function remove_option(div_id){
	$("#"+div_id).remove();
}

function getLocation(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#procured_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'">'+rkObj.objData[rk].site_location_name+'</option>';
			}
			$("#procured_location").html(rkHtm);
		}
	});
}

function getHod(){
	$.ajax({
		data:"objCode=getHod&lobId="+$("#procured_lob").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select HOD</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'">'+rkObj.objData[rk].hod_name+'</option>';
			}
			$("#approved_hod").html(rkHtm);
		}
	});
}

function getExpiryDt(){
	$.ajax({
		data:"objCode=getExpiryDate&warnty_month="+$("#warranty_expire_month").val()+"&procur_dt="+$("#procured_dt").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			console.log(rkObj);
			$("#expiry_dt").val(rkObj.objData);
		}
	});
}

//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}


function ajax_get_result(TYPE,key_search_primary,key_search_secondary,stockType=0)
{
	$("#myModal_div").modal('show');
	$.ajax({
		data:"type="+TYPE+"&key_search_primary="+key_search_primary+"&key_search_secondary="+key_search_secondary+"&stockType="+stockType,
		url:"<?php echo base_url().'admin/'; ?>ajx_mis_report_new",
		type:"POST",
		success:function(resp){
			//$("#myModal_div").find(".modal-title").html(dept_name+" - "+TYPE);
			$("#myModal_div").find(".modal-body").html(resp);
			$("#myModal_div").modal('show');
		}
	});
	return false;
}

function ajax_get_totalresult(TYPE,key_search_primary,key_search_secondary,stockType=0)
{
	$("#myModal_div").modal('show');
	$.ajax({
		data:"type="+TYPE+"&key_search_primary="+key_search_primary+"&key_search_secondary="+key_search_secondary+"&stockType="+stockType,
		url:"<?php echo base_url().'admin/'; ?>ajx_admin_mis_report_new",
		type:"POST",
		success:function(resp){
			//$("#myModal_div").find(".modal-title").html(dept_name+" - "+TYPE);
			$("#myModal_div").find(".modal-body").html(resp);
			$("#myModal_div").modal('show');
		}
	});
	return false;
}


</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>