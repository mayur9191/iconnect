<?php $backendURL = base_url().'assets/backend/'; ?>

	<head>
		<link rel="stylesheet" href="<?php echo $backendURL; ?>assets/css/bootstrap.min.css" />
		
		<style>
		/* html *
		{
		   font-size: 12px;
		}*/
		.fontSmall{
			font-size:9px;
		}
		</style>
	</head>
	<body>
	<!-- table asset generation code start -->
	<?php if($this->session->flashdata('msg')): ?>
		<div class="alert alert-success alert-dismissable">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?> 
		</div>
	<?php endif; ?>
	<?php if($this->session->flashdata('error_msg')): ?>
		<div class="alert alert-danger alert-dismissable">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Danger!</strong> <?php echo $this->session->flashdata('error_msg'); ?> 
		</div>
	<?php endif; ?> 
	<div class="container">
		<h3><center id="assetFrmName">IT Asset Allocation</center></h3>
	  <table class="table table-bordered">
		<tr class="fontSmall">
			<td><b>Name: </b><?php echo $edit_assign_data->emp_name; ?></td> <td><b>Empl.Code: </b><?php echo $edit_assign_data->emp_code; ?></td> <td><b>Email: </b><?php echo $edit_assign_data->emp_email; ?></td><td ><b>Employment Type: </b><?php echo ($edit_assign_data->emp_type=='employee')? 'Permanent':''; ?><?php echo ($edit_assign_data->emp_type=='IT_asset')? 'IT Asset':''; ?><?php echo ($edit_assign_data->emp_type=='consultant')? 'Consultant':''; ?></td>
		</tr>
		
		<tr class="fontSmall">
			<td><b>LOB: </b><?php echo getNameByCodeId('getLobName',$edit_assign_data->assign_lob_name); ?></td><td><b>Company: </b><?php echo getNameByCodeId('getCompanyName',$edit_assign_data->emp_company); ?></td><td><b>Department: </b><?php echo getNameByCodeId('getDeptName',$edit_assign_data->emp_dept); ?></td><td><b>HOD: </b><?php echo getNameByCodeId('getHodName',$edit_assign_data->assign_hod_name); ?></td>
		</tr>
		
		<tr class="fontSmall">
			<td><b>State: </b><?php echo getNameByCodeId('getStateName',$edit_assign_data->assign_emp_state); ?></td><td><b>Location: </b><?php echo getNameByCodeId('getLocationName',$edit_assign_data->assign_emp_location); ?></td><td><b>Approved By: </b><?php echo $edit_assign_data->approved_by; ?></td><td><b>Assign By: </b><?php echo $edit_assign_data->assign_by; ?></td>
		</tr>
		
		<tr class="fontSmall">
			<td><b>Assign Date: </b><?php echo date('d-m-Y',strtotime($edit_assign_data->assign_dt)); ?></td><td colspan="3"><b>Remark: </b><?php echo $edit_assign_data->assign_asset_remark; ?></td>
		</tr>
		</table>
		
		<table class="table table-bordered">
		<tr><td colspan="5"><b><center>Asset Details</center></b></td></tr>
		<tr class="fontSmall">
			<td><center><b>Asset Type</b></center></td><td><center><b>Manufacturer</b></center></td><td><center><b>Model</b></center></td><td><center><b>Serial Number</b></center></td><td><center><b>Asset Tag</b></center></td></center>
		</tr>
		<?php 
			$rk = & get_instance();
			foreach(getAssignAsset($edit_assign_data->assign_asset_for_emp_id) as $assignKeyAsset){
				$assetData = getAssetData($assignKeyAsset->asset_id);
				if($assetData->serial_number !=''){
					echo '<tr class="fontSmall"><td>'.getAssetTypeName($assetData->asset_type).'</td><td>'.$assetData->manufacturer.'</td><td>'.$assetData->modal_no.'</td><td>'.$assetData->serial_number.'</td><td>'.$assetData->present_asset_code.'</td></tr>';
				}
			}
			
		#check for accepted or not
		$acceptData = checkIsAcceptedByEmail($edit_assign_data->assign_asset_for_emp_id);
		if(count($acceptData) > 0){
			
		?>
			<form action="<?php echo base_url().'admin/acceptAsset/'.$edit_assign_data->assign_asset_for_emp_id; ?>" method="POST" id="acceptFrm">
				<tr><td colspan="5"></td></tr>
				<input type="hidden" value="<?php echo $acceptData->inv_asset_accept_id; ?>" name="accept_id" />
				<tr><td colspan="5"><center><input type="submit" class="btn btn-primary" id="acceptAsset" value="I Accept" /></center></td></tr>
			</form>
		<?php } ?>
	  </table>
	</div>
	<!-- table asset generation code end -->
	
	<!-- email template code start -->
	
	
	<!-- email template code end -->
	