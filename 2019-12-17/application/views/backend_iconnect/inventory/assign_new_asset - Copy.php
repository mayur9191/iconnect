
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Assign Asset - (Development in-process)</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Assign Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Assign Asset
						<a href="<?php echo base_url().'admin/manage_asset';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="#" method="POST">
							<!--<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="employee">Employee :</label>
									<input type="radio" required="required" name="emp_type" id="employee" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="non_employee">Non Employee :</label>
									<input type="radio" required="required" name="emp_type" id="non_employee" />
								</div>
							</div>-->
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_type">Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="emp_type" id="emp_type">
										<option value="">Please select</option>
										<option value="">Employee</option>
										<option value="">Non-Employee</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_email">Employee Email <span class="mand">*</span> :</label>
									<input type="email" required="required"  class="form-control" name="emp_email" id="emp_email" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_code">Employee Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="emp_code" id="emp_code" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_name">Employee Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="emp_name" id="emp_name" />
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_lob">LOB <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getHod();" name="procured_lob" id="procured_lob">
										<option value="">Select LOB</option>
										<?php foreach(inv_lob() as $invLob){
											echo '<option value="'.$invLob->lob_id.'">'.$invLob->lob_name.'</option>';
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_hod">LOB, HOD <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="approved_hod" id="approved_hod">
										<option value="">Select HOD</option>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_company_name">Company Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="emp_company_name" id="emp_company_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_dept_name">Dept Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="emp_dept_name" id="emp_dept_name" />
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_state">Assign State <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getLocation();" name="assign_state" id="procured_state">
										<option value="">Select State</option>
										<?php foreach(inv_states() as $invState){
											echo '<option value="'.$invState->state_id.'">'.$invState->state_name.'</option>';
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_location">Assign Location <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="assign_location" id="procured_location">
										<option value="">Select Location</option>
									</select>
								</div>
						    </div>
						<div style="border:1px solid #393939;padding:10px;">
							<div class="row">
								<span class="pull-right" style="margin-right:10px;" title="remove_option" onclick="remove_option('add_more_div_2')"><i class="ace-icon fa fa-times-circle bigger-250"></i></span>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type">
										<option value="">Please select</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="system_generated_code">System Generated Code :</label>
									<input type="text" required="required"  class="form-control" name="system_generated_code" id="system_generated_code" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_name">Asset Name :</label>
									<input type="text" required="required"  class="form-control" name="asset_name" id="asset_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="present_asset_code">Present Asset Code :</label>
									<input type="text" required="required"  class="form-control" name="present_asset_code" id="present_asset_code" />
								</div>
							</div>
							
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="nav_asset_code">NAV Asset code :</label>
									<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_dt">Procured date :</label>
									<input type="date" required="required"  class="form-control" name="procured_dt" id="procured_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="received_dt">Received date :</label>
									<input type="date" required="required"  class="form-control" name="received_dt" id="received_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_hod">Approved LOB, HOD :</label>
									<select class="form-control" required="required" name="approved_hod" id="approved_hod">
										<option value="">Select HOD</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_lob">Procured LOB :</label>
									<select class="form-control" required="required" name="procured_lob" id="procured_lob">
										<option value="">Select LOB</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_company">Procured Company :</label>
									<input type="text" required="required"  class="form-control" name="procured_company" id="procured_company" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_state">Procured State :</label>
									<select class="form-control" required="required" name="procured_state" id="procured_state">
										<option value="">Select State</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_location">Procured Location :</label>
									<select class="form-control" required="required" name="procured_location" id="procured_location">
										<option value="">Select Location</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="manufacturer">Manufacturer :</label>
									<input type="text" required="required"  class="form-control" name="manufacturer" id="manufacturer" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="model_no">Model No. :</label>
									<input type="text" required="required"  class="form-control" name="model_no" id="model_no" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="expiry_dt">Expiry Date :</label>
									<input type="date" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="serial_no">Serial Number :</label>
									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_expire_month">Warranty Expiry month :</label>
									<input type="text" required="required"  class="form-control" name="warranty_expire_month" id="warranty_expire_month" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_type">Warranty Type :</label>
									<input type="text" required="required"  class="form-control" name="warranty_type" id="warranty_type" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="description">Description :</label>
									<textarea required="required"  class="form-control" name="description" id="description"></textarea>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_expire_dt">Warranty Expiry Date :</label>
									<input type="date" required="required"  class="form-control" name="warranty_expire_dt" id="warranty_expire_dt" />
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-12">
									<label data-lblname="remark">Remark :</label>
									<textarea required="required"  class="form-control" name="remark" id="remark"></textarea>
								</div>
							</div>
						</div>
						
							<div class="row">
								<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Add more option" onclick="add_more()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
							</div>
							
							<div id="add_mor_div">
								<div class="row" id="add_more_div_122" style="border: 1px solid #393939;margin-top:5px; display:none;">
									<div class="row">
										<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="remove_option" onclick="remove_option('add_more_div_2')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span>
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field1">Field1 :</label>
										<input type="text" required="required"  class="form-control" name="field1" id="field1" />
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field2">Field2 :</label>
										<input type="text" required="required"  class="form-control" name="field2" id="field2" />
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field3">Field3 :</label>
										<input type="text" required="required"  class="form-control" name="field3" id="field3" />
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field4">Field4 :</label>
										<input type="text" required="required"  class="form-control" name="field4" id="field4" />
									</div>
								</div>
							</div>
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	/*add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;"><div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field1">Field1 :</label><input type="text" required="required"  class="form-control" name="field1" id="field1" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field2">Field2 :</label><input type="text" required="required"  class="form-control" name="field2" id="field2" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field3">Field3 :</label><input type="text" required="required"  class="form-control" name="field3" id="field3" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field4">Field4 :</label><input type="text" required="required"  class="form-control" name="field4" id="field4" /></div><br></div>';*/
	var myvar = '<div style="border:1px solid #393939;padding:10px;" id="add_more_div_'+add_more_count+'" ><div class="row">'+
'								<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-times-circle bigger-250"></i></span>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="asset_type">Asset Type :</label>'+
'									<select class="form-control" required="required" name="asset_type" id="asset_type">'+
'										<option value="">Please select</option>'+
'									</select>'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="system_generated_code">System Generated Code :</label>'+
'									<input type="text" required="required"  class="form-control" name="system_generated_code" id="system_generated_code" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="asset_name">Asset Name :</label>'+
'									<input type="text" required="required"  class="form-control" name="asset_name" id="asset_name" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="present_asset_code">Present Asset Code :</label>'+
'									<input type="text" required="required"  class="form-control" name="present_asset_code" id="present_asset_code" />'+
'								</div>'+
'							</div>'+
'							'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="nav_asset_code">NAV Asset code :</label>'+
'									<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_dt">Procured date :</label>'+
'									<input type="date" required="required"  class="form-control" name="procured_dt" id="procured_dt" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="received_dt">Received date :</label>'+
'									<input type="date" required="required"  class="form-control" name="received_dt" id="received_dt" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="approved_hod">Approved LOB, HOD :</label>'+
'									<select class="form-control" required="required" name="approved_hod" id="approved_hod">'+
'										<option value="">Select HOD</option>'+
'									</select>'+
'								</div>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_lob">Procured LOB :</label>'+
'									<select class="form-control" required="required" name="procured_lob" id="procured_lob">'+
'										<option value="">Select LOB</option>'+
'									</select>'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_company">Procured Company :</label>'+
'									<input type="text" required="required"  class="form-control" name="procured_company" id="procured_company" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_state">Procured State :</label>'+
'									<select class="form-control" required="required" name="procured_state" id="procured_state">'+
'										<option value="">Select State</option>'+
'									</select>'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_location">Procured Location :</label>'+
'									<select class="form-control" required="required" name="procured_location" id="procured_location">'+
'										<option value="">Select Location</option>'+
'									</select>'+
'								</div>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="manufacturer">Manufacturer :</label>'+
'									<input type="text" required="required"  class="form-control" name="manufacturer" id="manufacturer" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="model_no">Model No. :</label>'+
'									<input type="text" required="required"  class="form-control" name="model_no" id="model_no" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="expiry_dt">Expiry Date :</label>'+
'									<input type="date" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="serial_no">Serial Number :</label>'+
'									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />'+
'								</div>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="warranty_expire_month">Warranty Expiry month :</label>'+
'									<input type="text" required="required"  class="form-control" name="warranty_expire_month" id="warranty_expire_month" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="warranty_type">Warranty Type :</label>'+
'									<input type="text" required="required"  class="form-control" name="warranty_type" id="warranty_type" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="description">Description :</label>'+
'									<textarea required="required"  class="form-control" name="description" id="description"></textarea>'+
'								</div>'+
'                               <div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'                                 <label data-lblname="warranty_expire_dt">Warranty Expiry Date:</label>'+
'                                   <input type="date" required="required"  class="form-control" name="warranty_expire_dt" id="warranty_expire_dt" />'+
'                               </div>'+
'                         </div>'+
'                         <div class="row">'+
'                            <div class="form-group col-xs-12 col-sm-12 col-md-12">'+
'                               <label data-lblname="remark">Remark :</label>'+
'                                  <textarea required="required"  class="form-control" name="remark" id="remark"></textarea>'+
'                            </div>'+
'                              </div>'+
'                                  </div><br>';								
	//$("#add_mor_div").append(add_more_str);
	$("#add_mor_div").append(myvar);
	
	add_more_count++;
}

function remove_option(div_id){
	$("#"+div_id).remove();
}

function getHod(){
	$.ajax({
		data:"objCode=getHod&lobId="+$("#procured_lob").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select HOD</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'">'+rkObj.objData[rk].hod_name+'</option>';
			}
			$("#approved_hod").html(rkHtm);
		}
	});
}

function getLocation(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#procured_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'">'+rkObj.objData[rk].site_location_name+'</option>';
			}
			$("#procured_location").html(rkHtm);
		}
	});
}
</script>