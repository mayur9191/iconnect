<div class="main-content" data-key="<?php echo $asset_data->asset_id; ?>" id="add_more_div_<?php echo $asset_data->asset_id; ?>">
	<div class="main-content-inner">
		
		<div class="page-content" style="border:1px solid red;margin-top:5px;">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="remove_option" data-key="<?php echo $asset_data->asset_id; ?>" onclick="remove_option('add_more_div_<?php echo $asset_data->asset_id; ?>')"><i class="ace-icon fa fa-times-circle bigger-250"></i></span>
					</div>
					
					<div>
						
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type" disabled="desabled" readonly="readonly">
										<option value="">Please select</option>
										<?php foreach(inv_assetType() as $assetType){
											if($asset_data->asset_type == $assetType->asset_type_id){
												echo '<option value="'.$assetType->asset_type_id.'" selected>'.$assetType->asset_type_name.'</option>';
											}else{
												echo '<option value="'.$assetType->asset_type_id.'">'.$assetType->asset_type_name.'</option>';
											}
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="system_generated_code">System Generated Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->asset_system_generated_code; ?>" readonly="readonly" name="system_generated_code" id="system_generated_code" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_name">Asset Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="asset_name" value="<?php echo $asset_data->asset_name; ?>" id="asset_name" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="present_asset_code">Present Asset Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="present_asset_code" value="<?php echo $asset_data->present_asset_code; ?>" id="present_asset_code" readonly="readonly" />
								</div>
							</div>
							
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="nav_asset_code">NAV Asset code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" value="<?php echo $asset_data->nav_code; ?>" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_dt">Procured date <span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" onchange="getExpiryDt();" name="procured_dt" id="procured_dt" value="<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_expire_month">Warranty Expiry month <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getExpiryDt();" name="warranty_expire_month" id="warranty_expire_month" readonly="readonly">
										<option value="">Please select</option>
										<option value="1" <?php echo ($asset_data->warranty_expiry_no_month == 1)? 'selected' : ''; ?> >1 Month</option>
										<option value="2" <?php echo ($asset_data->warranty_expiry_no_month == 2)? 'selected' : ''; ?> >2 Month</option>
										<option value="3" <?php echo ($asset_data->warranty_expiry_no_month == 3)? 'selected' : ''; ?> >3 Month</option>
										<option value="4" <?php echo ($asset_data->warranty_expiry_no_month == 4)? 'selected' : ''; ?> >4 Month</option>
										<option value="5" <?php echo ($asset_data->warranty_expiry_no_month == 5)? 'selected' : ''; ?> >5 Month</option>
										<option value="6" <?php echo ($asset_data->warranty_expiry_no_month == 6)? 'selected' : ''; ?> >6 Month</option>
										<option value="7" <?php echo ($asset_data->warranty_expiry_no_month == 7)? 'selected' : ''; ?> >7 Month</option>
										<option value="8" <?php echo ($asset_data->warranty_expiry_no_month == 8)? 'selected' : ''; ?> >8 Month</option>
										<option value="9" <?php echo ($asset_data->warranty_expiry_no_month == 9)? 'selected' : ''; ?> >9 Month</option>
										<option value="10" <?php echo ($asset_data->warranty_expiry_no_month == 10)? 'selected' : ''; ?> >10 Month</option>
										<option value="11" <?php echo ($asset_data->warranty_expiry_no_month == 11)? 'selected' : ''; ?> >11 Month</option>
										<option value="12" <?php echo ($asset_data->warranty_expiry_no_month == 12)? 'selected' : ''; ?> >1 Year</option>
										<option value="24" <?php echo ($asset_data->warranty_expiry_no_month == 24)? 'selected' : ''; ?> >2 Year</option>
										<option value="36" <?php echo ($asset_data->warranty_expiry_no_month == 36)? 'selected' : ''; ?> >3 Year</option>
										<option value="48" <?php echo ($asset_data->warranty_expiry_no_month == 48)? 'selected' : ''; ?> >4 Year</option>
										<option value="60" <?php echo ($asset_data->warranty_expiry_no_month == 60)? 'selected' : ''; ?> >5 Year</option>
										<option value="72" <?php echo ($asset_data->warranty_expiry_no_month == 72)? 'selected' : ''; ?> >6 Year</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_type">Warranty Type <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="warranty_type" value="<?php echo $asset_data->warranty_type; ?>" id="warranty_type" readonly="readonly" />
								</div>								
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="expiry_dt">Expiry Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" readonly="readonly" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="received_dt">Received date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo date('d-m-Y',strtotime($asset_data->received_date)); ?>" name="received_dt" id="received_dt" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_lob">Procured LOB <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getHod_ajx();" name="procured_lob" id="procured_lob" readonly="readonly">
										<option value="">Select LOB</option>
										<?php foreach(inv_lob() as $invLob){
											if($invLob->lob_id == $asset_data->procured_lob_emp_code){
												echo '<option value="'.$invLob->lob_id.'" selected>'.$invLob->lob_name.'</option>';
											}else{												
												echo '<option value="'.$invLob->lob_id.'">'.$invLob->lob_name.'</option>';
											}
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_hod">Approved HOD <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="approved_hod" id="approved_hod" readonly="readonly">
										<option value="">Select HOD</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_company">Procured Company <span class="mand">*</span> :</label>
									<!--<input type="text" required="required"  class="form-control" name="procured_company" value="<?php echo $asset_data->procured_company; ?>" id="procured_company" readonly="readonly" />-->
									
									<select class="form-control" required="required" name="procured_company" id="procured_company" readonly="readonly" >
									<option value="">Select Company</option>
									<?php foreach(inv_company() as $companyRecord):
										if($asset_data->procured_company == $companyRecord->company_id){
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'" selected>'.$companyRecord->company_name.'</option>';
										}else{
											echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
										}
									?>
									<?php endforeach;?>
									</select>
									
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_state">Procured State <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getLocation_ajx();" name="procured_state" id="procured_state" readonly="readonly">
										<option value="">Select State</option>
										<?php foreach(inv_states() as $invState){
											if($invState->state_id == $asset_data->procured_state){
												echo '<option value="'.$invState->state_id.'" selected>'.$invState->state_name.'</option>';
											}else{
												echo '<option value="'.$invState->state_id.'">'.$invState->state_name.'</option>';
											}
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_location">Procured Location <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="procured_location" id="procured_location" readonly="readonly">
										<option value="">Select Location</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="manufacturer">Manufacturer <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="manufacturer" value="<?php echo $asset_data->manufacturer; ?>" id="manufacturer" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="model_no">Model No. <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="model_no" value="<?php echo $asset_data->modal_no; ?>" id="model_no" readonly="readonly" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="serial_no">Serial Number <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="serial_no" value="<?php echo $asset_data->serial_number; ?>" id="serial_no" readonly="readonly" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3" style="display:none;">
									<label data-lblname="warranty_expire_dt">Warranty Expiry Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="warranty_expire_dt" value="<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>" id="warranty_expire_dt" readonly="readonly" />
								</div>
								<!-- new fields -->
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="po_created">PO Created <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>" name="po_created" id="po_created" readonly="readonly" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="po_number">PO No. <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->po_numr; ?>" name="po_number" id="po_number" readonly="readonly" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="po_name">Vendor Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_data->vendor_name; ?>" name="po_name" id="po_name" readonly="readonly" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="description">Description <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="description" id="description" readonly="readonly"><?php echo $asset_data->description; ?></textarea>
								</div>
								
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-12">
									<label data-lblname="remark">Remark <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="remark" id="remark" readonly="readonly"><?php echo $asset_data->asset_remark; ?></textarea>
								</div>
							</div>
							<div class="row">
								
							</div>
							
							<div id="add_mor_div">
								<?php foreach(inv_sub_item($asset_data->asset_id) as $sub_invet){ ?>
									
									<div class="row" id="add_more_div_1111" style="border: 1px solid #393939;margin-top: 5px;">
									<input type="hidden" name="asset_sub_id[]" value="<?php echo ($sub_invet->inv_sub_asset !='')?$sub_invet->inv_sub_asset:''; ?>" />
										<!--
										<div class="row">
											<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option('add_more_div_1111')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span>
										</div>
										-->
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="key_id">Key :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->key_id !='')?$sub_invet->key_id:''; ?>"  name="key_id[]" id="key_id" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="processor">Processor :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->processor !='')?$sub_invet->processor:''; ?>" name="processor[]" id="processor" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="ram_id">RAM :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->ram !='')?$sub_invet->ram:''; ?>" name="ram_id[]" id="ram_id" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="hdd_id">HDD :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->hdd !='')?$sub_invet->hdd:''; ?>" name="hdd_id[]" id="hdd_id" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="os_id">OS :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->os_name !='')?$sub_invet->os_name:''; ?>" name="os_id[]" id="os_id" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="os_key">OS Key :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->os_key !='')?$sub_invet->os_key:''; ?>" name="os_key[]" id="os_key" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="msoffice_id">MS Office :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->ms_office_name !='')?$sub_invet->ms_office_name:''; ?>" name="msoffice_id[]" id="msoffice_id" readonly="readonly" />
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-3">
											<label data-lblname="msoffice_key">MS Office Key :</label><input type="text" class="form-control" value="<?php echo ($sub_invet->ms_office_key !='')?$sub_invet->ms_office_key:''; ?>" name="msoffice_key[]" id="msoffice_key" readonly="readonly" />
										</div>
									   <br>
									</div>
								<?php } ?>
								
							</div>
							
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	console.log('working...ajax');
	//$('#procured_dt').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	
	/*$('#procured_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#received_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#warranty_expire_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#po_created').datepicker({format:"dd-mm-yyyy"}).datepicker('setDate','now');*/
	
	$('#procured_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->procured_date)); ?>');
	$('#received_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->received_date)); ?>');
	$('#warranty_expire_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>');
	$('#po_created').datepicker({format:"dd-mm-yyyy"}).datepicker('setDate','<?php echo date('d-m-Y',strtotime($asset_data->expiry_date)); ?>');
	
	
	/*
	$("#add_asset").validate({
		rules:{
			asset_type: {required:true},
			asset_name: {required:true},
			present_asset_code: {required:true},
			nav_asset_code: {required:true},
			procured_dt: {required:true},
			warranty_expire_month: {required:true},
			warranty_type: {required:true},
			received_dt: {required:true},
			procured_lob: {required:true},
			approved_hod: {required:true},
			procured_company: {required:true},
			procured_state: {required:true},
			procured_location: {required:true},
			manufacturer: {required:true},
			model_no: {required:true},
			serial_no: {required:true},
			po_created: {required:true},
			po_number: {required:true},
			po_name: {required:true},
			description: {required:true},
			remark: {required:true}
			
		},
		messages: {
			"asset_type": { required: "Please select one asset type" },
			"asset_name": { required: "Please enter asset name" },
			"present_asset_code": { required: "Please enter asset code" },
			"nav_asset_code": { required: "Please enter asset code" },
			"procured_dt": { required: "Please select procured date" },
			"warranty_expire_month": { required: "Please select warranty" },
			"warranty_type": { required: "Please enter warranty type" },
			"received_dt": { required: "Please select received date" },
			"procured_lob": { required: "Please select LOB" },
			"approved_hod": { required: "Please select HOD" },
			"procured_company": { required: "Please enter company" },
			"procured_state": { required: "Please select state" },
			"procured_location": { required: "Please select location" },
			"manufacturer": { required: "Please enter manufacturer" },
			"model_no": { required: "Please enter model no." },
			"serial_no": { required: "Please enter serial no." },
			"po_created": { required: "Please select PO created date" },
			"po_number": { required: "Please enter PO number" },
			"po_name": { required: "Please enter vendor name" },
			"description": { required: "Please enter description" },
			"remark": { required: "Please enter remark" }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
	*/
	getLocation_ajx();
	getHod_ajx();
});
var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;"><div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><input type="hidden" name="asset_sub_id[]" value="0" /><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="key_id">Key :</label><input type="text" class="form-control" name="key_id[]" id="key_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="processor">Processor :</label><input type="text" class="form-control" name="processor[]" id="processor" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="ram_id">RAM :</label><input type="text" class="form-control" name="ram_id[]" id="ram_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="hdd_id">HDD :</label><input type="text" class="form-control" name="hdd_id[]" id="hdd_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="os_id">OS :</label><input type="text" class="form-control" name="os_id[]" id="os_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="os_key">OS Key :</label><input type="text" class="form-control" name="os_key[]" id="os_key" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="msoffice_id">MS Office :</label><input type="text" class="form-control" name="msoffice_id[]" id="msoffice_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="msoffice_key">MS Office Key :</label><input type="text" class="form-control" name="msoffice_key[]" id="msoffice_key" /></div><br></div>';
	$("#add_mor_div").append(add_more_str);
	add_more_count++;
}

//function remove_option(div_id){
//	$("#"+div_id).remove();
//}

function getLocation_ajx(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#procured_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			locationId = <?php echo $asset_data->procured_location; ?>;
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				if(locationId == rkObj.objData[rk].site_location_id){
					rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'" selected>'+rkObj.objData[rk].site_location_name+'</option>';
				}else{
					rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'">'+rkObj.objData[rk].site_location_name+'</option>';
				}
			}
			$("#procured_location").html(rkHtm);
		}
	});
}

function getHod_ajx(){
	$.ajax({
		data:"objCode=getHod&lobId="+$("#procured_lob").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			approvedhodId = <?php echo $asset_data->approved_lob_hod_emp_code; ?>;
			var rkHtm = '<option value="">Select HOD</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				if(approvedhodId == rkObj.objData[rk].inv_hod_id){
					rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'" selected>'+rkObj.objData[rk].hod_name+'</option>';
				}else{
					rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'">'+rkObj.objData[rk].hod_name+'</option>';
				}
			}
			$("#approved_hod").html(rkHtm);
		}
	});
}

function getExpiryDt(){
	$.ajax({
		data:"objCode=getExpiryDate&warnty_month="+$("#warranty_expire_month").val()+"&procur_dt="+$("#procured_dt").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			console.log(rkObj);
			$("#expiry_dt").val(rkObj.objData);
		}
	});
}

//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>