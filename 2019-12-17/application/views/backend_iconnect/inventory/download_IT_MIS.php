<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	 <?php //include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body>
<?php //include APPPATH . "views/includs/top_navbar.php" ?>


	<div class="container col-md-12" style="background-color: white;">
		<div class="page-header">
			<center>
				<h4>IT Assets</h4>
				
			</center>
		</div>
		
		<div class="col-xs-12" style="overflow-x:scroll;font-size: 10px;font-family: inherit;">
			<form action="<?= base_url().'admin/getReportDownloadV2' ?>" method="POST" >
				<table>
					<tr>
					
					<td>
						<input type="submit" class="btn btn-primary" name="download_btn" value="Download xls" style="border-radius: 30px;" />
						
					</td>
					</tr>
				</table>
			</form>
			
			<br><hr>
			<table id="myTable" class="table table-striped table-bordered table-hover">
				<thead>
					<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Procure LOB</th>
							<th>Procure company</th>
							<th>User LOB</th>
							<th>User Company</th>
							<th>User Dept.</th>
							<th>User Name</th>
							<th>User Email</th>
							<th>User Code</th>
							<th>User Type</th>
							<th>User Location</th>
							<th>Asset Type</th>
							<th>Serial No.</th>
							<th>NAV Code</th>
							<th>Manufacture</th>
							<th>Processor</th>
							<th>Model No.</th>
							<th>Procure Date</th>
							<th>Allocation Date</th>
							<th style="width:127px;">Status</th>
							<!--<th>Asset Tag</th>
							
							<th>NAV Code</th>
							<th>PO number</th>
							<th>Warranty Expiry Date</th>-->
							
					</tr>
				</thead>

				<tbody>
				
					<?php $i=1; foreach($asset_data as $recordData){ 	?>
					
						<tr style="font-size:12px;">
							<td><?= $i; if($recordData->asset_stock_type == 'Assign'){
								$stockStatus = '<span style="Background-color:red;color:#fff;">&nbsp; In Use &nbsp;</span>';
							}
							if($recordData->asset_stock_type == 'Stock'){
								$stockStatus = '<span  style="Background-color:green;color:#fff;"> &nbsp;In Stock&nbsp;</span>';
							}else{
								$stockStatus = '<span style="Background-color:red;color:#fff;">&nbsp; '.$recordData->asset_stock_type.' &nbsp;</span>';
							}
										?></td>
							<td><?php echo getNameByCodeId('getLobName',$recordData->procured_lob_emp_code); ?></td>
							<td><?php echo getNameByCodeId('getCompanyName',$recordData->procured_company); ?></td>
							<td><?php echo getNameByCodeId('getLobName',getNameByCodeId('getLobNameINC',$recordData->asset_id)); ?></td>
							<td><?php echo getNameByCodeId('getCompanyName',getNameByCodeId('getCompanyNameINC',$recordData->asset_id)); ?></td>
							<td><?php echo getNameByCodeId('getDeptName',getNameByCodeId('getDeptNameINC',$recordData->asset_id)); ?></td>
							<td><?php echo getNameByCodeId('getAssignEmpName',$recordData->asset_id); ?></td>
							<td><?php echo getNameByCodeId('getAssignEmpEmail',$recordData->asset_id); ?></td>
							<td><?php echo getNameByCodeId('getAssignEmpCode',$recordData->asset_id); ?></td>
							<td><?php echo getNameByCodeId('getAssignEmpType',$recordData->asset_id); ?></td>
							<td><?php echo getNameByCodeId('getLocationName',getNameByCodeId('getLocationNameINC',$recordData->asset_id));?></td>
							<td><?php echo getNameByCodeId('getAssetTypeName',$recordData->asset_type); ?></td>
							<td><?php echo $recordData->serial_number;?></td>
							<td><?php echo $recordData->nav_code;?></td>
							<td><?php echo $recordData->manufacturer; ?></td>
							<td><?php echo getNameByCodeId('getProcessorName',$recordData->asset_id);?></td>
							<td><?php echo $recordData->modal_no;?></td>
							<td><?php echo date('Y-m-d',strtotime($recordData->procured_date)); ?></td>
							<td><?php echo date('Y-m-d',strtotime(getNameByCodeId('getAssigndateINC',$recordData->asset_id))); ?></td>
							<td><?php echo $stockStatus; ?></td>
							<!--
							<td><?php echo $recordData->present_asset_code; ?></td>
							
							<td><?php echo $recordData->nav_code; ?></td>
							<td><?php echo $recordData->po_numr; ?></td>
							<td><?php echo date('Y-m-d',strtotime($recordData->warranty_expiry_date)); ?></td>-->
							
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
			</table>
		</div>
	</div> <!-- ./container -->
</body>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

<script>
jQuery(function($) {
	
	
	$('#myTable').DataTable({
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]]
	});
	
	setTimeout(function(){
		$(".alert").remove();
	},5000);
	
	
	
	//initiate dataTables plugin
	var myTable = 
	$('#dynamic-table')
	//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
	.DataTable( {
		bAutoWidth: false,
		"aoColumns": [
		  { "bSortable": false },
		  null, null,null, null, null,
		  { "bSortable": false }
		],
		"aaSorting": [],
		
		
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		//"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50


		select: {
			style: 'multi'
		}
	} );

	
	
	$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
	
	new $.fn.dataTable.Buttons( myTable, {
		buttons: [
		  {
			"extend": "colvis",
			"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
			"className": "btn btn-white btn-primary btn-bold",
			columns: ':not(:first):not(:last)'
		  },
		  {
			"extend": "copy",
			"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "csv",
			"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "excel",
			"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "pdf",
			"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
			"className": "btn btn-white btn-primary btn-bold"
		  },
		  {
			"extend": "print",
			"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
			"className": "btn btn-white btn-primary btn-bold",
			autoPrint: false,
			message: 'This print was produced using the Print button for DataTables'
		  }		  
		]
	} );
	myTable.buttons().container().appendTo( $('.tableTools-container') );
	
	//style the message box
	var defaultCopyAction = myTable.button(1).action();
	myTable.button(1).action(function (e, dt, button, config) {
		defaultCopyAction(e, dt, button, config);
		$('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
	});
	
	
	var defaultColvisAction = myTable.button(0).action();
	myTable.button(0).action(function (e, dt, button, config) {
		
		defaultColvisAction(e, dt, button, config);
		
		
		if($('.dt-button-collection > .dropdown-menu').length == 0) {
			$('.dt-button-collection')
			.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
			.find('a').attr('href', '#').wrap("<li />")
		}
		$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
	});

	////

	setTimeout(function() {
		$($('.tableTools-container')).find('a.dt-button').each(function() {
			var div = $(this).find(' > div').first();
			if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
			else $(this).tooltip({container: 'body', title: $(this).text()});
		});
	}, 500);
	
	
	
	
	
	myTable.on( 'select', function ( e, dt, type, index ) {
		if ( type === 'row' ) {
			$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
		}
	} );
	myTable.on( 'deselect', function ( e, dt, type, index ) {
		if ( type === 'row' ) {
			$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
		}
	} );




	/////////////////////////////////
	//table checkboxes
	$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
	
	//select/deselect all rows according to table header checkbox
	$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
		var th_checked = this.checked;//checkbox inside "TH" table header
		
		$('#dynamic-table').find('tbody > tr').each(function(){
			var row = this;
			if(th_checked) myTable.row(row).select();
			else  myTable.row(row).deselect();
		});
	});
	
	//select/deselect a row when the checkbox is checked/unchecked
	$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
		var row = $(this).closest('tr').get(0);
		if(this.checked) myTable.row(row).deselect();
		else myTable.row(row).select();
	});



	$(document).on('click', '#dynamic-table .dropdown-toggle', function(e) {
		e.stopImmediatePropagation();
		e.stopPropagation();
		e.preventDefault();
	});
	
	
	
	//And for the first simple table, which doesn't have TableTools or dataTables
	//select/deselect all rows according to table header checkbox
	var active_class = 'active';
	$('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
		var th_checked = this.checked;//checkbox inside "TH" table header
		
		$(this).closest('table').find('tbody > tr').each(function(){
			var row = this;
			if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
			else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
		});
	});
	
	//select/deselect a row when the checkbox is checked/unchecked
	$('#simple-table').on('click', 'td input[type=checkbox]' , function(){
		var $row = $(this).closest('tr');
		if($row.is('.detail-row ')) return;
		if(this.checked) $row.addClass(active_class);
		else $row.removeClass(active_class);
	});

	

	/********************************/
	//add tooltip for small view action buttons in dropdown menu
	$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
	
	//tooltip placement on right or left
	function tooltip_placement(context, source) {
		var $source = $(source);
		var $parent = $source.closest('table')
		var off1 = $parent.offset();
		var w1 = $parent.width();

		var off2 = $source.offset();
		//var w2 = $source.width();

		if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
		return 'left';
	}
	
	
	
	
	/***************/
	$('.show-details-btn').on('click', function(e) {
		e.preventDefault();
		$(this).closest('tr').next().toggleClass('open');
		$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
	});
	/***************/
	
	
	
	
	
	/**
	//add horizontal scrollbars to a simple table
	$('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
	  {
		horizontal: true,
		styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
		size: 2000,
		mouseWheelLock: true
	  }
	).css('padding-top', '12px');
	*/


})
</script>