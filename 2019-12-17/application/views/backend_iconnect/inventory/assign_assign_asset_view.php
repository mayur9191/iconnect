<?php
//echo "<pre>";
//print_r($asset_assign_data);

?>

				<div class="col-xs-12">
					
					
				
						
							<div class="row">
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_type">Emp type</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->emp_type; ?>" readonly="readonly" disabled="desabled" name="emp_type" id="emp_type" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_email">Emp email</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->emp_email; ?>" readonly="readonly" disabled="desabled" name="emp_email" id="emp_email" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_code">Emp code</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->emp_code; ?>" readonly="readonly" disabled="desabled" name="emp_code" id="emp_code" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_name">Emp name</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->emp_name; ?>" readonly="readonly" disabled="desabled" name="emp_name" id="emp_name" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
								<label data-lblname="procured_lob">LOB</label>
									<select class="form-control" required="required" name="procured_lob" id="procured_lob" readonly="readonly" disabled="desabled">
										<option value="">Select LOB</option>
										<?php foreach(inv_lob() as $invLob){
											if($invLob->lob_id == $asset_assign_data->assign_lob_name){
												echo '<option value="'.$invLob->lob_id.'" selected>'.$invLob->lob_name.'</option>';
											}else{
												echo '<option value="'.$invLob->lob_id.'">'.$invLob->lob_name.'</option>';
											}
										} ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
								<label data-lblname="company">Company</label>
									<select class="form-control" required="required" name="company" id="company" readonly="readonly" disabled="desabled">
										<option value="">Select Company</option>
										<?php foreach(inv_company() as $company){
											if($company->company_id == $asset_assign_data->emp_company){
												echo '<option value="'.$company->company_id.'" selected>'.$company->company_name.'</option>';
											}else{
												echo '<option value="'.$company->company_id.'">'.$company->company_name.'</option>';
											}
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_by">Approved by</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->approved_by; ?>" readonly="readonly" disabled="desabled" name="approved_by" id="approved_by" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_by">Assign by</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->assign_by; ?>" readonly="readonly" disabled="desabled" name="assign_by" id="assign_by" />
								</div>
								
							</div>
							
							
							<div class="row">
								
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_dt">Assign date</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->assign_dt; ?>" readonly="readonly" disabled="desabled" name="assign_dt" id="assign_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_created_dt">Assign created date</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_assign_data->assign_created_dt; ?>" readonly="readonly" disabled="desabled" name="assign_created_dt" id="assign_created_dt" />
								</div>
								
								
							</div>
							
							
							<!-- Asset Return history ends->
							
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	
</div><!-- /.main-content -->

