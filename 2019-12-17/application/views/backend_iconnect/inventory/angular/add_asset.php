<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
<title>Add Asset</title>
</head>
<style>
body {
    margin-top:40px;
}
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>
<body>
	<div class="text-center">
		<p>Add Asset</p> 
	</div>
	<div class="container">
	<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
        <p>Step 4</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
        <p>Step 5</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
        <p>Step 6</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
        <p>Step 7</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-8" type="button" class="btn btn-default btn-circle" disabled="disabled">8</a>
        <p>Step 8</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-9" type="button" class="btn btn-default btn-circle" disabled="disabled">9</a>
        <p>Step 9</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-10" type="button" class="btn btn-default btn-circle" disabled="disabled">10</a>
        <p>Step 10</p>
      </div>
	  <div class="stepwizard-step">
        <a href="#step-11" type="button" class="btn btn-default btn-circle" disabled="disabled">11</a>
        <p>Step 11</p>
      </div>
    </div>
  </div>
	  <?php $companies  = get_record('company_in');
			$hods       =  get_hod();
			$lobs       =  get_record('lob_in');
			$depts      =  get_record('dept_in');
			$locations  =  get_record('site_locations_hd');
			$assets_types = get_record('asset_type_in');
			$processors = get_record('processor_in');
			$rams = get_record('storage_ram_in');
			$storages = get_record('storage_in');
			//$manufacturers = get_record('manufacturer_in');
			$os_types = get_record('operating_system_in');
			$ms_office = get_record('ms_office_in');
			//print_r($companies);	
					?>
	  <form role="form" action="<?php echo base_url() ?>inventory/save_asset" method="post">
		<div class="row setup-content" id="step-1">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
					<div class="form-group">
						<label for="empcode">Employee Code:</label>
						<input type="text" class="form-control" required="required" name="emp_code" id="emp_code">
					</div>
					<div class="form-group">
						<label for="empemail">Employee Email:</label>
						<input type="email" class="form-control" required="required" name="employee_email" id="employee_email">
					</div>
					<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			  </div>
            </div>
		</div>
		<div class="row setup-content" id="step-2">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="procuredate">Procured Date:</label>
					<input type="text" class="form-control" required="required" name="procured_date" id="procured_date">
				</div>
				<div class="form-group">
					<label for="procurecmpy">Procurement Company:</label>
					<select class="form-control" id="procurecmpy" required="required" name="procurecmpy">
					<option value="">Please select</option>
					<?php foreach($companies as $company): ?>
					<option value="<?php echo $company['company_id']; ?>"><?php echo $company['company_name'];; ?></option>
					<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<label for="allocationdate">Allocation date:</label>
					<input type="text" class="form-control" required="required" name="allocation_date" id="allocation_date">
				</div>
				<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div>	
		</div>
		<div class="row setup-content" id="step-3">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="oldassettag">Old Asset tag:</label>
					<input type="text" class="form-control" required="required" name="old_asset_tag" id="old_asset_tag">
				</div>
				<div class="form-group">
					<label for="newassettag">New Asset tag:</label>
					<input type="text" class="form-control" required="required" name="new_asset_tag" id="new_asset_tag">
				</div>
			   <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
			   <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div> 	
		</div>
		<div class="row setup-content" id="step-4">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="previoususername">Previous UserName:</label>
					<input type="text" class="form-control" required="required" name="previous_username" id="previous_username">
				</div>
				<div class="form-group">
					<label for="previoushod">Hod:</label>
					<select class="form-control" id="previous_hod" required="required" name="previous_hod">
						<option value="">Please select</option>
						<?php foreach($hods as $hod): ?>
						<option value="<?php echo $hod['u_id']; ?>"><?php echo $hod['u_name']; ?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<label for="usercmpy">User Company:</label>
					<select class="form-control" id="usercmpy" required="required" name="usercmpy">
					<option value="">Please select</option>
					<?php foreach($companies as $company): ?>
					<option value="<?php echo $company['company_id']; ?>"><?php echo $company['company_name'];; ?></option>
					<?php endforeach;?>
					</select>
				</div>
				<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div>	
		</div>
		<div class="row setup-content" id="step-5">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="currentusername">Current UserName:</label>
					<input type="text" class="form-control" required="required" name="current_username" id="current_username">
				</div>
				<div class="form-group">
					<label for="currenthod">Hod:</label>
					<select class="form-control" id="current_hod" required="required" name="current_hod">
						<option value="">Please select</option>
						<?php foreach($hods as $hod): ?>
						<option value="<?php echo $hod['u_id']; ?>"><?php echo $hod['u_name']; ?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<label for="lob">Lob:</label>
					<select class="form-control" id="lob" required="required" name="lob">
						<option value="">Please select</option>
						<?php foreach($lobs as $lob): ?>
						<option value="<?php echo $lob['lob_id']; ?>"><?php echo $lob['lob_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div>	
		</div>
		<div class="row setup-content" id="step-6">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="department">Department:</label>
					<select class="form-control" id="department" required="required" name="department">
						<option value="">Please select</option>
						<?php foreach($depts as $dept): ?>
						<option value="<?php echo $dept['dept_id']; ?>"><?php echo $dept['dept_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="location">Location:</label>
					<select class="form-control" id="location" required="required" name="location">
						<option value="">Please select</option>
						<?php foreach($locations as $location): ?>
						<option value="<?php echo $location['site_location_id']; ?>"><?php echo $location['site_location_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="asset_type">Asset type:</label>
					<select class="form-control" id="asset_type" required="required" name="asset_type">
						<option value="">Please select</option>
						<?php foreach($assets_types as $asset_type):?>
						<option value="<?php echo $asset_type['asset_type_id']; ?>"><?php echo $asset_type['asset_type']; ?></option>
						<?php endforeach;?>
					</select>
				</div>
				<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div> 		
		</div>
		<div class="row setup-content" id="step-7">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="manufacturer">Manufacturer:</label>
					<select class="form-control" id="manufacturer" required="required" name="manufacturer">
						<option value="">Please select</option>
						
					</select>
				</div>
				<div class="form-group">
					<label for="manufacturer">Model No:</label>
					<select class="form-control" id="model_no" required="required" name="model_no">
						<option value="">Please select</option>
					</select>
				</div>
				<div class="form-group">
					<label for="serialno">Serial no/ Express service tag:</label>
					<input type="text" class="form-control" required="required" name="serial_no" id="serial_no">
				</div>
				<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div>
		</div>
		<div class="row setup-content" id="step-8">
			<div class="col-xs-6 col-md-offset-3">
             <div class="col-md-12">
				<div class="form-group">
					<label for="processor">Processor:</label>
					<select class="form-control" id="processor" required="required" name="processor">
						<option value="">Please select</option>
						<?php foreach($processors as $processor): ?>
						<option value="<?php echo $processor['processor_id']; ?>"><?php echo $processor['processor_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="ram">RAM:</label>
					<select class="form-control" id="ram" required="required" name="ram">
						<option value="">Please select</option>
						<?php foreach($rams as $ram): ?>
						<option value="<?php echo $ram['ram_id']; ?>"><?php echo $ram['ram_size']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="hdd">HDD:</label>
					<select class="form-control" id="hdd" required="required" name="hdd">
						<option value="">Please select</option>
						<?php foreach($storages as $storage): ?>
						<option value="<?php echo $storage['hdd_id']; ?>"><?php echo $storage['hdd_size']; ?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<label for="cd_dvd">CD/DVD:</label>
					<select class="form-control" id="cd_dvd" required="required" name="cd_dvd">
						<option value="">Please select</option>
						<option value="yes">Yes</option>
						<option value="no">No</option>
					</select>
				</div>
				<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
				<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			</div>
		   </div>	
		</div>
		<div class="row setup-content" id="step-9">
		  <div class="col-xs-6 col-md-offset-3">
		   <div class="col-md-12">
			<div class="form-group">
				<label for="ostype">OS type:</label>
				<select class="form-control" id="os_types" required="required" name="os_types">
					<option value="">Please select</option>
					<?php foreach($os_types as $os_type): ?>
					<option value="<?php echo $os_type['os_id']; ?>"><?php echo $os_type['os_name'].' '.$os_type['os_bit']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group">
					<label for="oskey">OS key:</label>
					<input type="text" class="form-control" required="required" name="os_key" id="os_key">
			</div>
			<div class="form-group">
				<label for="msoffice">MS Office:</label>
				<select class="form-control" id="ms_office" required="required" name="ms_office">
					<option value="">Please select</option>
					<?php foreach($ms_office as $office): ?>
					<option value="<?php echo $office['ms_office_id']; ?>"><?php echo $office['ms_office_name']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<!--<div class="form-group">
				<button class="btn btn-primary  btn-sm pull-right" type="button">Add More</button>
				<br/>
			</div>-->
			<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
			<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
			
		  </div>
		 </div>	
		</div>
		<div class="row setup-content" id="step-10">
		  <div class="col-xs-6 col-md-offset-3">
		   <div class="col-md-12">
			<div class="form-group col-md-6">
					<label for="monitor">Monitor:</label>
					<input type="text" class="form-control" required="required" name="monitor" id="monitor">
			</div>
			<div class="form-group col-md-6">
					<label for="monitorsr">Monitor Serial:</label>
					<input type="text" class="form-control" required="required" name="monitor_sr" id="monitor_sr">
			</div>
			<div class="form-group">
					<label for="monitorasset">Monitor Asset Tag:</label>
					<input type="text" class="form-control" required="required" name="monitor_asset_tag" id="monitor_asset_tag">
			</div>
			<div class="form-group">
					<label for="keyboard">Keyboard:</label>
					<input type="text" class="form-control" required="required" name="keyboard" id="keyboard">
			</div>
			<div class="form-group">
					<label for="mouse">Mouse:</label>
					<input type="text" class="form-control" required="required" name="mouse" id="mouse">
			</div>
			<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
			<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
		   </div>
		  </div>
		</div>
		<div class="row setup-content" id="step-11">
		  <div class="col-xs-6 col-md-offset-3">
			<div class="col-md-12">
			<button class="btn btn-primary  btn-sm pull-right" id="add_more_software" type="button">Add More</button>
			<legend>Extra Software:</legend>
			<fieldset id="software_fieldset">
			<div class="row">
			<div class="form-group col-md-3">
					<input type="text" placeholder="software" class="form-control" required="required" name="software[]">
			</div>
			<div class="form-group col-md-3">
					<input type="text" placeholder="exp date" class="form-control" required="required" name="software_exp[]">
			</div>
			<div class="form-group col-md-3">
					<input type="text" placeholder="procure date" class="form-control" required="required" name="software_procure[]">
			</div>
			<div class="form-group col-md-3"><button class="btn btn-sm" onclick="remove_software(this)" >Remove</button>
			</div>
			</div>
			</fieldset>
			<button class="btn btn-primary  btn-sm pull-right" id="add_more_hardware" type="button">Add More</button>
			<legend>Extra Hardware:</legend>
			<fieldset id="hardware_fieldset">
			<div class="form-group col-sm-10">
					<input type="text" placeholder="Hardware" class="form-control" required="required" name="hardware[]">
			</div>
			</fieldset>
			<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
            <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
			</div>
		  </div>	
		</div>	
	  </form>	
	  
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script>
function remove_software(obj)
{
	//alert("hiii");
	$(obj).parent().prev('div').prev('div').prev('div').remove();
	$(obj).parent().prev('div').prev('div').remove();
	$(obj).parent().prev('div').remove();
	$(obj).parent().remove();
}
function remove_hardware(obj)
{
	$(obj).parent().prev('div').remove();
	$(obj).parent().remove();
}
/*$("span.glyphicon-trash").click(function(){
		
		$(this).parent().remove();
		$(this).parent().prev().remove();
		$(this).parent().prev().prev().remove();
	});*/
</script>
<script>
$( document ).ready(function() {
  
  // code to procured date and allocation date
  $('#procured_date').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#allocation_date').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
  $('input[name="software_exp[]"]').datepicker({format: "dd-mm-yyyy"}).datepicker();
  $('input[name="software_procure[]"]').datepicker({format: "dd-mm-yyyy"}).datepicker();
  // to get the model no on change of the manufacturer
	$("#manufacturer").change(function(){
		var id = $(this).val();
		//alert(id);
		if(id == ""){
			alert("Please select")
			$("#model_no").html('<option value="">please select</option>');
		}
		else{
			$.ajax({
				url:'<?php echo base_url() ?>inventory/get_model_no',
				type:'POST',
				data:{"manufacturer_id":id},
				datatype:'text',
				success:function(data){
					$("#model_no").html(data);
				},
			});
		}
	});
	
	// to get the manufacturer on change of the asset type
	$("#asset_type").change(function(){
		var id = $(this).val();
		$("#model_no").html('<option value="">please select</option>');
		//alert(id);
		if(id == ""){
			alert("Please select")
			$("#manufacturer").html('<option value="">please select</option>');
		}
		else{
			$.ajax({
				url:'<?php echo base_url() ?>inventory/get_manufacturer',
				type:'POST',
				data:{"asset_type_id":id},
				datatype:'text',
				success:function(data){
					$("#manufacturer").html(data);
				},
			});
		}
	});
	//add more software
	$("#add_more_software").click(function(){
		
		$("#software_fieldset").append('<div class="row"><div class="form-group col-md-3"><input type="text" placeholder="software" class="form-control" required="required" name="software[]"></div><div class="form-group col-md-3"><input type="text" placeholder="exp date" class="form-control" required="required" name="software_exp[]"></div><div class="form-group col-md-3"><input type="text" placeholder="procure date" class="form-control" required="required" name="software_procure[]"></div><div class="form-group col-md-3"><button  class="btn btn-sm" onclick="remove_software(this)" >Remove</button></div></div>');
		$('input[name="software_exp[]"]').datepicker({format: "dd-mm-yyyy"}).datepicker();
		$('input[name="software_procure[]"]').datepicker({format: "dd-mm-yyyy"}).datepicker();
	});
	//add more hardware
	$("#add_more_hardware").click(function(){
		$("#hardware_fieldset").append('<div class="form-group col-sm-10"><input type="text" placeholder="Hardware" class="form-control" required="required" name="hardware[]"></div><div class="form-group col-sm-2"><span class="glyphicon glyphicon-trash" onclick="remove_hardware(this)" ></span></div>');
	});
	
});
</script>
<script>
$(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn'),
  		  allPrevBtn = $('.prevBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });
  
  allPrevBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepWizard.removeAttr('disabled').trigger('click');
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],select"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>
</html>