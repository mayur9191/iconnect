<div class="main-content" ng-app="rkinventory">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Add Asset</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Add Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Add Asset
					</div>
					
					<div ng-init="init()" ng-controller="rkAddAsset">
						<form action="#" method="POST">
							<div class="row">
							
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type"  ng-modal="asset_type" id="asset_type" >
										<option value="">Please select asset type</option>
										<option ng-repeat="assettype in asset_type" value="{{assettype.asset_type_id}}">{{assettype.asset_type_name}}</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="system_generated_code">System Generated Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="system_generated_code" ng-modal="system_generated_code" id="system_generated_code" readonly="readonly" value="{{system_generated_code}}" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_name">Asset Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="asset_name" ng-modal="asset_name" id="asset_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="present_asset_code">Present Asset Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="present_asset_code" ng-modal="present_asset_code" id="present_asset_code" />
								</div>
							</div>
							
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="nav_asset_code">NAV Asset code :</label>
									<input type="text" required="required"  class="form-control" name="nav_asset_code" ng-modal="nav_asset_code" id="nav_asset_code" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_dt">Procured date :</label>
									<input type="date" required="required"  class="form-control" name="procured_dt" ng-modal="procured_dt" id="procured_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="received_dt">Received date :</label>
									<input type="date" required="required"  class="form-control" name="received_dt" ng-modal="received_dt" id="received_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_hod">Approved LOB, HOD :</label>
									<select class="form-control" required="required" name="approved_hod" ng-modal="approved_hod" id="approved_hod">
										<option value="">Select HOD</option>
										<option ng-repeat="approveHod in approved_hod" value="{{approveHod.inv_hod_id}}">{{approveHod.hod_name}}</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_lob">Procured LOB :</label>
									<select class="form-control" required="required" name="procured_lob" ng-modal="procured_lob" id="procured_lob">
										<option value="">Select LOB</option>
										<option ng-repeat="procuredLob in procured_lob" value="{{procuredLob.lob_id}}">{{procuredLob.lob_name}}</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_company">Procured Company :</label>
									<input type="text" required="required"  class="form-control" name="procured_company" ng-modal="procured_company" id="procured_company" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_state">Procured State :</label>
									<!-- ng-change="updateLocation(procured_state)" -->
									<select class="form-control" required="required" name="procured_state" ng-model="procured_state" id="procured_state">
										<option value="" selected="selected">Select State</option>
										<option  ng-repeat="procuredState in procured_state" value="{{procuredState.inv_state_id}}">{{procuredState.state_name}}</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_location">Procured Location :</label>
									<select class="form-control" required="required" name="procured_location" ng-modal="procured_location" id="procured_location">
										<option value="">Select Location</option>
										<option  ng-repeat="procuredLocation in procured_location | " value="{{procuredLocation.location_id}}">{{procuredLocation.location_name}}</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="manufacturer">Manufacturer :</label>
									<input type="text" required="required"  class="form-control" name="manufacturer" id="manufacturer" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="model_no">Model No. :</label>
									<input type="text" required="required"  class="form-control" name="model_no" id="model_no" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="expiry_dt">Expiry Date :</label>
									<input type="date" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="serial_no">Serial Number :</label>
									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_expire_month">Warranty Expiry month :</label>
									<input type="text" required="required"  class="form-control" name="warranty_expire_month" id="warranty_expire_month" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_type">Warranty Type :</label>
									<input type="text" required="required"  class="form-control" name="warranty_type" id="warranty_type" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="description">Description :</label>
									<textarea required="required"  class="form-control" name="description" id="description"></textarea>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_expire_dt">Warranty Expiry Date :</label>
									<input type="date" required="required"  class="form-control" name="warranty_expire_dt" id="warranty_expire_dt" />
								</div>
							</div>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-12">
									<label data-lblname="remark">Remark :</label>
									<textarea required="required"  class="form-control" name="remark" id="remark"></textarea>
								</div>
							</div>
							<div class="row">
								<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Add more option" onclick="add_more()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
							</div>
							
							<div id="add_mor_div">
								<div class="row" id="add_more_div_122" style="border: 1px solid #393939;margin-top:5px; display:none;">
									<div class="row">
										<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="remove_option" onclick="remove_option('add_more_div_2')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span>
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field1">Field1 :</label>
										<input type="text" required="required"  class="form-control" name="field1" id="field1" />
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field2">Field2 :</label>
										<input type="text" required="required"  class="form-control" name="field2" id="field2" />
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field3">Field3 :</label>
										<input type="text" required="required"  class="form-control" name="field3" id="field3" />
									</div>
									<div class="form-group col-xs-12 col-sm-12 col-md-3">
										<label data-lblname="field4">Field4 :</label>
										<input type="text" required="required"  class="form-control" name="field4" id="field4" />
									</div>
								</div>
							</div>
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;"><div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field1">Field1 :</label><input type="text" required="required"  class="form-control" name="field1" id="field1" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field2">Field2 :</label><input type="text" required="required"  class="form-control" name="field2" id="field2" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field3">Field3 :</label><input type="text" required="required"  class="form-control" name="field3" id="field3" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field4">Field4 :</label><input type="text" required="required"  class="form-control" name="field4" id="field4" /></div><br></div>';
	$("#add_mor_div").append(add_more_str);
	add_more_count++;
}

function remove_option(div_id){
	$("#"+div_id).remove();
}
</script>


<script src="<?php echo base_url(); ?>assets/js/angu/ang_contr.js"></script>