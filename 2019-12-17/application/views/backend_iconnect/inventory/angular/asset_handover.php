<div id="list_asset" style="border:1px solid #393939;padding:10px;">
	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="asset_type">Asset Type :</label>
			<select class="form-control" required="required" name="asset_type" id="asset_type">
				<option value="">Please select</option>
			</select>
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="system_generated_code">System Generated Code :</label>
			<input type="text" required="required"  class="form-control" name="system_generated_code" id="system_generated_code" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="asset_name">Asset Name :</label>
			<input type="text" required="required"  class="form-control" name="asset_name" id="asset_name" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="present_asset_code">Present Asset Code :</label>
			<input type="text" required="required"  class="form-control" name="present_asset_code" id="present_asset_code" />
		</div>
	</div>


	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="nav_asset_code">NAV Asset code :</label>
			<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="procured_dt">Procured date :</label>
			<input type="date" required="required"  class="form-control" name="procured_dt" id="procured_dt" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="received_dt">Received date :</label>
			<input type="date" required="required"  class="form-control" name="received_dt" id="received_dt" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="approved_hod">Approved LOB, HOD :</label>
			<select class="form-control" required="required" name="approved_hod" id="approved_hod">
				<option value="">Select HOD</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="procured_lob">Procured LOB :</label>
			<select class="form-control" required="required" name="procured_lob" id="procured_lob">
				<option value="">Select LOB</option>
			</select>
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="procured_company">Procured Company :</label>
			<input type="text" required="required"  class="form-control" name="procured_company" id="procured_company" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="procured_state">Procured State :</label>
			<select class="form-control" required="required" name="procured_state" id="procured_state">
				<option value="">Select State</option>
			</select>
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="procured_location">Procured Location :</label>
			<select class="form-control" required="required" name="procured_location" id="procured_location">
				<option value="">Select Location</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="manufacturer">Manufacturer :</label>
			<input type="text" required="required"  class="form-control" name="manufacturer" id="manufacturer" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="model_no">Model No. :</label>
			<input type="text" required="required"  class="form-control" name="model_no" id="model_no" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="expiry_dt">Expiry Date :</label>
			<input type="date" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="serial_no">Serial Number :</label>
			<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />
		</div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="warranty_expire_month">Warranty Expiry month :</label>
			<input type="text" required="required"  class="form-control" name="warranty_expire_month" id="warranty_expire_month" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="warranty_type">Warranty Type :</label>
			<input type="text" required="required"  class="form-control" name="warranty_type" id="warranty_type" />
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="description">Description :</label>
			<textarea required="required"  class="form-control" name="description" id="description"></textarea>
		</div>
		<div class="form-group col-xs-12 col-sm-12 col-md-3">
			<label data-lblname="warranty_expire_dt">Warranty Expiry Date :</label>
			<input type="date" required="required"  class="form-control" name="warranty_expire_dt" id="warranty_expire_dt" />
		</div>
	</div>

	<div class="row">
		<div class="form-group col-xs-12 col-sm-12 col-md-12">
			<label data-lblname="remark">Remark :</label>
			<textarea required="required"  class="form-control" name="remark" id="remark"></textarea>
		</div>
	</div>
</div>