
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Asset</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Asset
						<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Add Assign Technician" onclick="add_record()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="empcode">Employee Code:</label>
							<input type="text" required="required"  class="form-control" name="emp_code" id="emp_code">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="empemail">Employee Email:</label>
							<input type="email" required="required"  class="form-control" name="employee_email" id="employee_email">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="procuredate">Procured Date:</label>
							<input type="text" required="required"  class="form-control" name="procured_date" id="procured_date">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="procurecmpy">Procurement Company:</label>
							<select id="procurecmpy"  class="form-control" required="required" name="procurecmpy">
								<option value="">Please select</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="oldassettag">Old Asset tag:</label>
							<input type="text" required="required" class="form-control" name="old_asset_tag" id="old_asset_tag">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="allocationdate">Allocation Date:</label>
							<input type="text" required="required" class="form-control" name="allocation_date" id="allocation_date">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="procurecmpy">Procurement Company:</label>
							<select id="procurecmpy" class="form-control" required="required" name="procurecmpy">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="oldassettag">Old Asset tag:</label>
							<input type="text" class="form-control" required="required" name="old_asset_tag" id="old_asset_tag">
						</div>
					</div>
					
					<div class="row">
						
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="allocationdate">Allocation Date:</label>
							<input type="text" class="form-control" required="required" name="allocation_date" id="allocation_date">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="newassettag">New Asset tag:</label>
							<input type="text" class="form-control" required="required" name="new_asset_tag" id="new_asset_tag">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="currentusername">Current UserName:</label>
							<input type="text" required="required"  class="form-control" name="current_username" id="current_username">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="currenthod">Hod:</label>
							<select id="current_hod" class="form-control" required="required" name="current_hod">
								<option value="">Please select</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="previoususername">Previous UserName:</label>
							<input type="text" class="form-control" required="required" name="previous_username" id="previous_username">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="previoushod">Hod:</label>
							<select id="previous_hod" class="form-control" required="required" name="previous_hod">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="usercmpy">User Company:</label>
							<select id="usercmpy" class="form-control" required="required" name="usercmpy">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="lob">Lob:</label>
							<select id="lob" class="form-control" required="required" name="lob">
								<option value="">Please select</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="department">Department:</label>
							<select id="department" class="form-control" required="required" name="department">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="location">Location:</label>
							<select id="location" class="form-control" required="required" name="location">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="asset_type">Asset type:</label>
							<select id="asset_type" class="form-control" required="required" name="asset_type">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="manufacturer"  >Manufacturer:</label>
							<select id="manufacturer" class="form-control" required="required" name="manufacturer">
								<option value="">Please select</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="manufacturer">Model No:</label>
							<select id="model_no" class="form-control" required="required" name="model_no">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="serialno">Serial no:</label>
							<input type="text" class="form-control" required="required" name="serial_no" id="serial_no">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="processor">Processor:</label>
							<select id="processor" class="form-control" required="required" name="processor">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="ram">RAM:</label>
							<select id="ram" class="form-control" required="required" name="ram">
								<option value="">Please select</option>
							</select>
						</div>
					</div>
					
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="hdd"  >HDD:</label>
							<select id="hdd" class="form-control" required="required" name="hdd">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="cd_dvd"  >CD/DVD:</label>
							<select id="cd_dvd" class="form-control" required="required" name="cd_dvd">
								<option value="">Please select</option>
								<option value="yes">Yes</option>
								<option value="no">No</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="monitor" >Monitor:</label>
							<input type="text" class="form-control" required="required" name="monitor" id="monitor">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="monitorsr"  >Monitor Serial:</label>
							<input type="text" class="form-control" required="required" name="monitor_sr" id="monitor_sr">
						</div>
					</div>
					
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="monitorasset"  >Monitor Asset Tag:</label>
							<input type="text" class="form-control" required="required" name="monitor_asset_tag" id="monitor_asset_tag">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="keyboard"  >Keyboard:</label>
							<input type="text" class="form-control" required="required" name="keyboard" id="keyboard">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="mouse"  >Mouse:</label>
							<input type="text" class="form-control" required="required" name="mouse" id="mouse">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="ostype"  >OS type:</label>
							<select id="os_types" class="form-control" required="required" name="os_types">
								<option value="">Please select</option>
							</select>
						</div>
					</div>
					
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="oskey"  >OS key:</label>
							<input type="text" class="form-control" required="required" name="os_key" id="os_key">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="msoffice"  >MS Office:</label>
							<select id="ms_office" class="form-control" required="required" name="ms_office" id="ms_office">
								<option value="">Please select</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="msoffice"  >MS Office Key:</label>
							<input type="text" class="form-control" required="required" name="ms_office_key" id="ms_office_key">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="software"  >Additional S/W:</label>
							<input type="text" class="form-control" required="required" name="software">
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="expdate"  >Exp Date:</label>
							<input type="text" class="form-control" required="required" name="software_exp">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="expdate"  >Additional H/W:</label>
							<input type="text" class="form-control" required="required" name="hardware">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="remark"  >Remarks:</label>
							<input type="text" class="form-control" required="required" name="remark">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-3">
							<label for="remark">Warranty:</label>
							<input type="text" class="form-control" required="required" name="warranty">
						</div>
					</div>
					
					<div class="row">
						<div class="form-group col-xs-12 col-sm-12 col-md-4">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-4">
							<label for="remark"></label>
							<input type="submit" value="Submit" class="btn btn-sm btm-primary">
						</div>
						<div class="form-group col-xs-12 col-sm-12 col-md-4">
						</div>
					
					</div>
					
					
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
 <div class="modal fade" id="myModal" role="dialog">
	
 </div>
<!-- Modal end -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

</script>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
