
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Non User Form</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Non User Form</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Non User Form
						<a href="<?php echo base_url().'admin/manageNonUsr';?>" class="btn btn-success pull-right" title="Back to Non-User Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'admin/add_non_usr';?>" method="POST">
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-6">
									<label data-lblname="non_usr_name"> Name :</label>
									<input type="text" required="required"  class="form-control" name="non_usr_name" id="non_usr_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-6">
									<label data-lblname="non_usr_code"> Code :</label>
									<input type="text" required="required"  class="form-control" name="non_usr_code" id="non_usr_code" value="<?php echo $empCode; ?>" readonly />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-6">
									<label data-lblname="non_usr_state"> State :</label>
									<select class="form-control" required="required" onchange="getLocation();" name="non_usr_state" id="non_state">
										<option value="">Select State</option>
										<?php foreach(inv_states() as $invState){
											echo '<option value="'.$invState->state_id.'">'.$invState->state_name.'</option>';
										} ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-6">
									<label data-lblname="non_usr_location"> Location :</label>
									<select class="form-control" required="required" name="non_usr_location" id="non_usr_location">
										<option value="">Select Location</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-12">
									<label data-lblname="non_usr_address"> Address :</label>
									<textarea class="form-control" name="non_usr_address" id="non_usr_address" required="required"></textarea>
								</div>
								
							</div>
								
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

function getLocation(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#non_usr_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'">'+rkObj.objData[rk].site_location_name+'</option>';
			}
			$("#non_usr_location").html(rkHtm);
		}
	});
}
</script>