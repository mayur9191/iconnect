<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!--<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	-->
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	     table tbody tr td:nth-child(3) a.description .tooltiptext {
			display: none;
			width: 430px;
			background-color:#ccc
			color:black;
			text-align: left;
			border-radius: 6px;
			padding: 8px 0;
			position: absolute;
			z-index: 1;
			border:1px solid black;
			
			
		}
		table tbody tr td:nth-child(3) a.description:hover .tooltiptext {
			display: block;
			color:black;
			padding-left:12px;
			word-wrap: break-word;
		}
		table tbody tr td:nth-child(3) a.description{
			color:#333;
		}
		table tbody tr td:nth-child(3) a.description:hover{
			color:#337ab7;
		}
		span.tooltiptext ul li{
			list-style-type:none;
		} 
		.square {
		float: left;
		width: 10px;
		height: 10px;
		margin: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		}

		.Normal {
		  background: #006600;
		}
		.Low {
		  background: #e4af6d;
		}

		.Medium {
		  background: #ff6600;
		}

		.High {
		  background: #ff0000;
		}
		#dynamic-table{
			width:100% !important;
			font-size:12px !important;
		}
	</style>
</head>
<body>

	<div class="col-md-12">
		<div  id="tab2success">
			<div class="row inbox">
				<div class="container-fluid col-md-12" style="background-color: white;">
			<!--<div class="page-header"></div>-->
			<?php $CI = & get_instance();  ?>
			<div class="col-md-12 col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>Procure LOB</th>
							<th>Procure company</th>
							<th>User LOB</th>
							<th>User Company</th>
							<th>User Dept.</th>
							<th>User Name</th>
							<th>User Location</th>
							<th>Asset Type</th>
							<th>Asset Tag</th>
							<th>Manufacture</th>
							<th>Processor</th>
							<th>Model No.</th>
							<th>Serial No.</th>
							<th>NAV Code</th>
							<th>PO number</th>
							<th>Procure Date</th>
							<th>Allocation Date</th>
							<th>Warranty Expiry Date</th>
							<th style="width:127px;">Status</th>
							<th>Stock Location</th>
							<th>Custodian Name</th>
						</tr>
					</thead>

					<tbody>
					<?php $i=1; foreach($mis_data as $reportRecord){ 							
							if($reportRecord['stock_type_status'] == 'Assign'){
								$stockStatus = '<span style="Background-color:red;color:#fff;">&nbsp; In Use &nbsp;</span>';
							}
							if($reportRecord['stock_type_status'] == 'Stock'){
								$stockStatus = '<span  style="Background-color:green;color:#fff;"> &nbsp;In Stock&nbsp;</span>';
							}else{
								$stockStatus = $stockStatus = '<span style="Background-color:red;color:#fff;">&nbsp; '.$reportRecord['stock_type_status'].' &nbsp;</span>';
							}
					?>
					
						<tr>
							<td><?= $i; //echo "-"; $assetarray = explode(',', $reportRecord['asset_id']);
										//$first_asset = $assetarray [0];
										//$second_asset = $assetarray [1];
										// $countofasset = count($assetarray); echo $countofasset; 
										?></td>
							<td><?php echo ($reportRecord['procure_lob'] !='') ? $reportRecord['procure_lob'] :'NA'; ?></td>
							<td><?php echo ($reportRecord['procure_company'] !='') ? $reportRecord['procure_company'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['user_lob'] !='') ? $reportRecord['user_lob'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['user_company'] !='') ? $reportRecord['user_company'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['user_dept'] !='') ? $reportRecord['user_dept'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['user_name'] !='') ? $reportRecord['user_name'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['user_location'] !='') ? $reportRecord['user_location'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['asset_type'] !='') ? $reportRecord['asset_type'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['asset_tag'] !='') ? $reportRecord['asset_tag']: 'NA'; ?></td>
							<td><?php echo ($reportRecord['manufacture'] !='') ? $reportRecord['manufacture'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['processor'] !='') ? $reportRecord['processor'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['model_no'] !='') ? $reportRecord['model_no'] : 'NA'; ?></td>
							<td><?php echo ($reportRecord['serial_no'] !='') ? $reportRecord['serial_no'] : 'NA'; ?></td>
							<td><?php echo $reportRecord['nav_code']; ?></td>
							<td><?php echo $reportRecord['po_numr']; ?></td>
							<td><?php echo ($reportRecord['procure_date']=='0000-00-00')? 'NA' : date('Y-m-d',strtotime($reportRecord['procure_date'])); ?></td>
							<td><?php echo ($reportRecord['allocation_date'] !='')? date('Y-m-d',strtotime($reportRecord['allocation_date'])) : 'NA'; ?></td>
							<td><?php echo date('d-m-Y',strtotime($reportRecord['warranty_expiry_date']));?></td>
							<td><?php echo $stockStatus; ?></td>
							<td><?php echo $reportRecord['stock_location_name'];; ?></td>
							<td><?php echo $reportRecord['custodian_name']; ?></td>
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
			</div>
		</div> <!-- ./container -->
		
		</div><!--/.col-->		
	</div>
</div>
</body>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script>

var request_idaa = 0;
/* function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coor = "X coords: " + x + ", Y coords: " + y;
	$("#printTxt").css({'top':y,'left':x,'display':'block'}).show(); 
}
 */
function get_technician(group_id,tech_id)
{
	        $.ajax({
				url : "<?php echo base_url() ?>"+ "helpdesk/get_technician",
				type:"POST",
				data: {group_id:group_id},
				success:function(response){
					var str = '<option value=""> --- Choose --- </option>';
					var obj  =  JSON.parse(response);
					var selectop = '';
					$(obj).each(function(index,value){
						var sel = (tech_id == obj[index].tech_id)? 'selected':'';
						str += '<option '+sel+' id="'+obj[index].tech_id+'" value="'+obj[index].tech_id+'">'+obj[index].tech_name+'</option>';	
					});
					
					$("#technicians").html(str);
					//document.getElementById(tech_id).selected = "true";
					//$("#tech_id").prop("selected",true);
				},
				error:function(response){
					alert("error occured"+response)
				}
			});
	
}

$(document).ready(function(){
	
	
	
	//reassing technician 
	$("#reassign_task").validate({
            rules: {
                groups: { required: true },
				technicians : { required : true }
            },
            messages: {
				groups: { required : "Please select group" },
                technician : { required : "Please select technician" }
            },
            submitHandler: function (form) {
				var group_id = $("#groups").val();
				var technician_id = $("#technicians").val();
				console.log('reassing groupId = '+ group_id + ' and technician id = '+technician_id);
				$.ajax({
					//url: "<?php echo base_url() ?>/helpdesk/reassign_technician",
					url: "<?php echo base_url() ?>/helpdesk/tech_reassign_ajx",
					type: "POST",
					data:{group_id:group_id,technician_id:technician_id,request_id:request_idaa},
					
					success:function(resp){
						console.log(resp);
						console.log('response is = '+resp);
						//$("#spinner").show();
						console.log('request_id '+request_idaa);
						if(resp > 0 && resp !=2){
							location.reload();
						}else if(resp == 2){
							alert("You can not re-assign to same technician");
						}else{
							alert("error occured please try again later");
						}
					},error:function(resp){
						alert("Error occured please try again later");
						console.log(resp);
					}
				});
                return false;
            }
        });

	
	
	
	<?php if(check_technician($CI->user_ldamp["mail"],$CI->user_ldamp["employeeid"])){ ?>
		var nulval = [null,null,null,null, null,null,null, null,null,null,null,null, null,null,null];
	<?php }else{ ?>
		var nulval = [null,null,null,null, null,null,null, null,null,null,null, null,null,null];
	<?php } ?>
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": nulval,
		"aaSorting": [],
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
	//Handles menu drop down
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
		
    });
	$('#dynamic-table').on('click',' tbody >tr>td>.dropdown>a.dropdown-toggle.ab',function(e){
		//alert(($("#printTxt").position().left));
		
		var positionX;
		var positionY;
		var group_id = $(this).attr('data-gid');
		var tech_id  = $(this).attr('data-tid');
		request_idaa = $(this).attr('data-rid');
		positionX = e.clientX-45 + 'px',
		positionY = $(this).offset().top-250+'px';
		$("#groups").val('');
		$("#groups").val(group_id);
		//alert($(this).offset().top);
		//$("#request_gp").css({'top':positionY,'left':'75%','display':'block'}).show();
		//$("#request_gp").css({'top':e.clientY,'left':e.clientX,'display':'block'}).show();
		$("#printTxt").css({'top':positionY,'left':positionX,'display':'block'});
		// code to select technician
		get_technician(group_id,tech_id);
		$("#request_gp").show();
		
		
		///code to select group 
		/*
		$("#groups option").each(function(index){
			if(index == group_id){
				$(this).attr("selected","selected");
				
			}
			else{
				$(this).removeAttr("selected");
			}
			
				
			
		});*/
		
	});
	
	
	 // on menu selection
	 $( ".dropdown-menu" ).draggable();
	 
	 //on group selection
	$("#groups").on("change",function(){
		var group_id = $(this).find("option:selected").val();
		if(group_id == "")
		{
			alert("please select the correct group");
			
		}
		else
		{
			get_technician(group_id,tech_id =0);
		}
		
	});
	
	$(".close.pop").on("click",function(){
		$("#request_gp").hide();
	});
	$(".btn.btn-default.pop").on("click",function(){
		$("#request_gp").hide();
	});
	
});
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>