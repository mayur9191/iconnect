
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Assign Asset</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Assign Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Assign Asset
						<a href="<?php echo base_url().'admin/asset_assign_mgnt';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'admin/submit_add_assign_asset'; ?>" method="POST">
						
							<div class="row">
								<input type="hidden" name="asset_assign_id" value="" id="asset_assign_id" />
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_type">Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="emp_type" id="emp_type">
										<option value="">Please select</option>
										<option value="employee">Employee</option>
										<option value="IT_asset">IT Asset</option>
										<option value="consultant">Consultant</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_name">Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="emp_name" id="emp_name" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3" id="emp_email_div">
									<label data-lblname="emp_email">Email <span class="mand">*</span> :</label>
									<input type="email" required="required"  class="form-control" name="emp_email" id="emp_email" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_code">Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="emp_code" id="emp_code" />
								</div>
								
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_lob">LOB <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getHod();" name="procured_lob" id="procured_lob">
										<option value="">Select LOB</option>
										<?php foreach(inv_lob() as $invLob){
											echo '<option value="'.$invLob->lob_id.'">'.$invLob->lob_name.'</option>';
										} ?>
									</select>
								</div>
								<!--
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_hod">HOD <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="approved_hod" id="approved_hod">
										<option value="">Select HOD</option>
									</select>
								</div>-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_company_name">Company Name <span class="mand">*</span> :</label>
									<!--<input type="text" required="required"  class="form-control" name="emp_company_name" id="emp_company_name" />-->
									<select class="form-control" required="required" name="emp_company_name" id="emp_company_name" onchange="getAssetCode('company');" />
									<option value="">Select Company</option>
									<?php foreach(inv_company() as $companyRecord): 
										echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
									?>
									<?php endforeach;?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="emp_dept_name">Department Name <span class="mand">*</span> :</label>
									<!--<input type="text" required="required"  class="form-control" name="emp_dept_name" id="emp_dept_name" /> -->
									<select class="form-control" required="required" name="emp_dept_name" id="emp_dept_name">
										<option value="">Select Department</option>
										<?php foreach(inv_department() as $deptRecord):
											echo '<option value="'.$deptRecord->dept_id.'">'.$deptRecord->dept_name.'</option>';
										?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_state">Assign State <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getLocation();" name="assign_state" id="procured_state">
										<option value="">Select State</option>
										<?php foreach(inv_states() as $invState){
											echo '<option value="'.$invState->state_id.'">'.$invState->state_name.'</option>';
										} ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assign_location">Assign Location <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="assign_location" id="procured_location" onchange="getAssetCode('location');">
										<option value="">Select Location</option>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_by">Approved By <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="approved_by" id="approved_by" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assigned_by">Assign By <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="assigned_by" id="assigned_by" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="assigned_date">Assign Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="assigned_date" id="assigned_date" />
								</div>
								
								<div class="form-group col-xs-4 col-sm-12 col-md-6">
									<label data-lblname="remark">Remark <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="remark_assign_asset" id="remark_assign_asset"></textarea>
								</div>
								
								<!--<div class="form-group col-xs-4 col-sm-12 col-md-4">
									<label data-lblname="reason">Reason <sub>(Only if asset replace)</sub>  :</label>
									<textarea   class="form-control" name="reason_of_asset_replace" id="reason_of_asset_replace"></textarea>
								</div>-->
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3" style="display:none;">
									<label data-lblname="allocation_date">Allocation Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="allocation_date" id="allocation_date" />
								</div>
								
								
								<div class="form-group col-xs-12 col-sm-12 col-md-1">
									<label data-lblname="assign_location">&nbsp;<span class="mand">&nbsp;</span>&nbsp;&nbsp;</label>
									<a href="#" class="btn btn-primary" title="Add asset" id="add_asset" onclick="add_asset()">Add Asset</a>
								</div>
								
						    </div>
							
	<!-- ------------------------------------------------------------------------------------------------------------------------------------ -->
	
							<hr>
						<!-- here -->
						
							<div class="row" style="display:none;">
								<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Add more option" onclick="add_more()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
							</div>
							
							<div id="add_mor_div">
								
							</div>
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Select Asset</h4>
		  </div>
		  <div class="modal-body">
			<input type="text" id="search_asset" class="form-control" id="search_asset" />
			<input type="hidden" name="asset_id" id="asset_id" value="" />
			<!-- table data start -->
			<table class="table" style="position:absolute; background-color:beige; z-index:9999;width:95%;" id="search_suggestion">
			
			</table>
			<!-- table data end -->
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="asset_select()">Add Asset</button>
		  </div>
		</div>
	</div>
  </div>
<!-- Modal end -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--datatable js--->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
$("#search_suggestion").hide();
$(document).ready(function(){
	console.log('Working...');
	$('#allocation_date').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#assigned_date').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	
	$("#search_asset").on("keyup",function(){
		$("#search_suggestion").hide();
		var assetVal = $(this).val();
		if(assetVal !=''){
			$.ajax({
				url:'<?php echo base_url().'admin/ajax_call'; ?>',
				type:"POST",
				data:"asset_search="+assetVal+"&type=search_asset",
				success:function(resp){
					$("#search_suggestion").html(resp);
					$("#search_suggestion").show();
				}
			});
		}
	});
});
function onSerchSug(ths){
	var slection = $(ths).find('td').eq('0');
	var selectionOne = slection.text();
	var dataVal = slection.attr('data-key');
	$("#asset_id").val(dataVal);
	$("#search_asset").val(selectionOne);
	$("#search_suggestion").hide();
}
//----------------------------------------------------


var add_more_count = 1;
var addedAssetAry = [];
function add_more(){
	var add_more_str = '';
	/*add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;"><div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field1">Field1 :</label><input type="text" required="required"  class="form-control" name="field1" id="field1" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field2">Field2 :</label><input type="text" required="required"  class="form-control" name="field2" id="field2" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field3">Field3 :</label><input type="text" required="required"  class="form-control" name="field3" id="field3" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="field4">Field4 :</label><input type="text" required="required"  class="form-control" name="field4" id="field4" /></div><br></div>';*/
	
	var myvar = '<div style="border:1px solid #393939;padding:10px;" id="add_more_div_'+add_more_count+'" ><div class="row">'+
'								<span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-times-circle bigger-250"></i></span>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="asset_type">Asset Type :</label>'+
'									<select class="form-control" required="required" name="asset_type" id="asset_type">'+
'										<option value="">Please select</option>'+
'									</select>'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="system_generated_code">System Generated Code :</label>'+
'									<input type="text" required="required"  class="form-control" name="system_generated_code" id="system_generated_code" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="asset_name">Asset Name :</label>'+
'									<input type="text" required="required"  class="form-control" name="asset_name" id="asset_name" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="present_asset_code">Present Asset Code :</label>'+
'									<input type="text" required="required"  class="form-control" name="present_asset_code" id="present_asset_code" />'+
'								</div>'+
'							</div>'+
'							'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="nav_asset_code">NAV Asset code :</label>'+
'									<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_dt">Procured date :</label>'+
'									<input type="date" required="required"  class="form-control" name="procured_dt" id="procured_dt" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="received_dt">Received date :</label>'+
'									<input type="date" required="required"  class="form-control" name="received_dt" id="received_dt" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="approved_hod">Approved LOB, HOD :</label>'+
'									<select class="form-control" required="required" name="approved_hod" id="approved_hod">'+
'										<option value="">Select HOD</option>'+
'									</select>'+
'								</div>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_lob">Procured LOB :</label>'+
'									<select class="form-control" required="required" name="procured_lob" id="procured_lob">'+
'										<option value="">Select LOB</option>'+
'									</select>'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_company">Procured Company :</label>'+
'									<input type="text" required="required"  class="form-control" name="procured_company" id="procured_company" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_state">Procured State :</label>'+
'									<select class="form-control" required="required" name="procured_state" id="procured_state">'+
'										<option value="">Select State</option>'+
'									</select>'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="procured_location">Procured Location :</label>'+
'									<select class="form-control" required="required" name="procured_location" id="procured_location">'+
'										<option value="">Select Location</option>'+
'									</select>'+
'								</div>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="manufacturer">Manufacturer :</label>'+
'									<input type="text" required="required"  class="form-control" name="manufacturer" id="manufacturer" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="model_no">Model No. :</label>'+
'									<input type="text" required="required"  class="form-control" name="model_no" id="model_no" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="expiry_dt">Expiry Date :</label>'+
'									<input type="date" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="serial_no">Serial Number :</label>'+
'									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" />'+
'								</div>'+
'							</div>'+
'							'+
'							<div class="row">'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="warranty_expire_month">Warranty Expiry month :</label>'+
'									<input type="text" required="required"  class="form-control" name="warranty_expire_month" id="warranty_expire_month" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="warranty_type">Warranty Type :</label>'+
'									<input type="text" required="required"  class="form-control" name="warranty_type" id="warranty_type" />'+
'								</div>'+
'								<div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'									<label data-lblname="description">Description :</label>'+
'									<textarea required="required"  class="form-control" name="description" id="description"></textarea>'+
'								</div>'+
'                               <div class="form-group col-xs-12 col-sm-12 col-md-3">'+
'                                 <label data-lblname="warranty_expire_dt">Warranty Expiry Date:</label>'+
'                                   <input type="date" required="required"  class="form-control" name="warranty_expire_dt" id="warranty_expire_dt" />'+
'                               </div>'+
'                         </div>'+
'                         <div class="row">'+
'                            <div class="form-group col-xs-12 col-sm-12 col-md-12">'+
'                               <label data-lblname="remark">Remark :</label>'+
'                                  <textarea required="required"  class="form-control" name="remark" id="remark"></textarea>'+
'                            </div>'+
'                              </div>'+
'                                  </div><br>';								
	//$("#add_mor_div").append(add_more_str);
	$("#add_mor_div").append(myvar);
	
	add_more_count++;
}

function remove_option(div_id){
	removeItem($("#"+div_id).attr('data-key'));
	$("#"+div_id).remove();
}


function removeItem(assetId = 0){
	returnStatus = true;
	for(var i=0;i<addedAssetAry.length;i++){
		if(addedAssetAry[i] == assetId){
			addedAssetAry.splice(i,1);
			returnStatus = i;
			$("#asset_assign_id").val(addedAssetAry);
		}
	}
	console.log('asset id is '+assetId+' removed id  '+returnStatus);
	return returnStatus;
}



function getHod(){
	$.ajax({
		data:"objCode=getHod&lobId="+$("#procured_lob").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select HOD</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'">'+rkObj.objData[rk].hod_name+'</option>';
			}
			$("#approved_hod").html(rkHtm);
		}
	});
}

function getLocation(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#procured_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'" data-shortCode="'+rkObj.objData[rk].city_code+'">'+rkObj.objData[rk].site_location_name+'</option>';
			}
			$("#procured_location").html(rkHtm);
		}
	});
}


$(document).ready(function(){
	$("#emp_type").on('change',function(){
		var emp_type = $(this).val();
		console.log(emp_type);
		if(emp_type !='IT_asset'){
			//$("#emp_email_div").show();
		}else{
			//$("#emp_email_div").hide();
		}
	});
	
	
	
});

function add_asset(){
	rkObj = [];
	rkObj = <?php echo $assetData; ?>;
	console.log(rkObj);
	var rkHtm = '<option value="">Select Asset</option>';
	for(var rk =0;rk<rkObj.length;rk++){
		rkHtm += '<option value="'+rkObj[rk].asset_id+'">'+rkObj[rk].asset_name+' - ('+rkObj[rk].present_asset_code+')</option>';
	}
	/*var htmlData = '<div class="modal-dialog">'+
			'<div class="modal-content">'+
			'  <div class="modal-header">'+
				'<button type="button" class="close" data-dismiss="modal">&times;</button>'+
				'<h4 class="modal-title">Select Asset</h4>'+
			  '</div>'+
			  '<div class="modal-body">'+
				'<select name="asset_id" id="asset_id" class="form-control" >'+rkHtm+'</select>'+
			  '</div>'+
			  '<div class="modal-footer">'+
				'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="asset_select()">Add Asset</button>'+
			  '</div>'+
			'</div>'+
		  '</div>';
	$("#myModal").html(htmlData);*/
	$("#search_asset").val('');
	$("#myModal").modal('show');
	return false;
}


function asset_select(){
	var asset_id = $("#asset_id").val();
	/*$.ajax({
		url:"<?php echo base_url().'admin/ajx_assign_asset'; ?>",
		data:"assetId="+asset_id,
		type:"POST",
		success:function(resp){
			if(checkAddAset(asset_id)){
				addedAssetAry.push(parseInt(asset_id));
				$("#add_mor_div").append(resp);
				$("#asset_assign_id").val(addedAssetAry);
			}else{
				alert('You have already added this item');
			}
		}
	});*/
	$.ajax({
		url:"<?php echo base_url().'admin/ajx_assign_edit_asset'; ?>/"+asset_id,
		type:"POST",
		success:function(resp){
			if(checkAddAset(asset_id)){
				addedAssetAry.push(parseInt(asset_id));
				$("#add_mor_div").append(resp);
				$("#asset_assign_id").val(addedAssetAry);
				$(".pull-right.return_ajx_asset").css('display','none');
			}else{
				alert('You have already added this item');
			}
		}
	});
	console.log(asset_id);
}

//generate asset code automatically start 
	var assetCode = '- - -<?php echo getAssetNum(); ?>';
	function getAssetCode(generateType){	
		assetCodeDecpt = assetCode.split("-");
		companyShort = assetCodeDecpt[0].trim();
		assetShort = assetCodeDecpt[1].trim();
		locationShort = assetCodeDecpt[2].trim();
		incrementShort  = assetCodeDecpt[3].trim();
		switch(generateType){
			case 'company':
				companyShort = $("#procured_company").find(":selected").attr('data-shortCode');
			break;
			case 'assttype':
				assetShort = $("#asset_type").find(":selected").attr('data-shortCode');
			break;
			case 'location':
				locationShort = $("#procured_location").find(":selected").attr('data-shortCode');
			break;
		}
		assetCode = companyShort+'-'+assetShort+'-'+locationShort+'-'+incrementShort;
		deAsstCode = assetCode.split("-");
		showAssetCode = deAsstCode[0].trim()+''+deAsstCode[1].trim()+''+deAsstCode[2].trim()+''+deAsstCode[3].trim();
		$("#present_asset_code").val(showAssetCode);
		$("#system_ref_code").val(assetCode);
		console.log(assetCode);
	}
//generate asset code automatically end

function checkAddAset(assetId = 0){
	returnStatus = true;
	for(var i=0;i<addedAssetAry.length;i++){
		if(addedAssetAry[i] == assetId){
			returnStatus = false;
		}
	}
	return returnStatus;
}


</script>