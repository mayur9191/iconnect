
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Add Asset</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Add Asset</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Add Asset
						<a href="<?php echo base_url().'admin/manage_asset';?>" class="btn btn-success pull-right" title="Back to Asset Management">Back</a>
					</div>
					
					<div>
						<form action="<?php echo base_url().'admin/addAsset'; ?>" method="POST" id="add_asset" name="myform" novalidate>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_type">Asset Type <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="asset_type" id="asset_type" onchange="getAssetCode('assttype');">
										<option value="">Please select</option>
										<?php foreach(inv_assetType() as $assetType){
											echo '<option value="'.$assetType->asset_type_id.'" data-shortCode="'.$assetType->asset_short_code.'">'.$assetType->asset_type_name.'</option>';
										} ?>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3" style="display:none;">
									<label data-lblname="system_generated_code">System Generated Code <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" value="<?php echo $asset_code; ?>" readonly="readonly" name="system_generated_code" id="system_generated_code" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="asset_name">Asset Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="asset_name" id="asset_name" />
								</div>
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="present_asset_code">Current Asset Tag <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="present_asset_code" id="present_asset_code" readonly />
								</div>
								<input type="hidden" name="system_ref_code" id="system_ref_code" value="" />
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_dt">Procured date <span class="mand">*</span> :</label>
									<input type="text" required="required" class="form-control" onchange="getExpiryDt();" name="procured_dt" id="procured_dt" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="received_dt">Received date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="received_dt" id="received_dt" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_lob">Procured LOB <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getHod();" name="procured_lob" id="procured_lob">
										<option value="">Select LOB</option>
										<?php foreach(inv_lob() as $invLob){
											echo '<option value="'.$invLob->lob_id.'">'.$invLob->lob_name.'</option>';
										} ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="approved_hod">Procured Approved HOD <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="approved_hod" id="approved_hod">
										<option value="">Select HOD</option>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_company">Procured Company <span class="mand">*</span> :</label>
									<!--<input type="text" required="required"  class="form-control" name="procured_company" id="procured_company" />-->
									<select class="form-control" required="required" onchange="getAssetCode('company');" name="procured_company" id="procured_company" />
									<option value="">Select Company</option>
									<?php foreach(inv_company() as $companyRecord): 
										echo '<option value="'.$companyRecord->company_id.'" data-shortCode="'.$companyRecord->short_code.'">'.$companyRecord->company_name.'</option>';
									?>
									<?php endforeach;?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_state">Procured State <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getLocation();" name="procured_state" id="procured_state">
										<option value="">Select State</option>
										<?php foreach(inv_states() as $invState){
											echo '<option value="'.$invState->state_id.'">'.$invState->state_name.'</option>';
										} ?>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="procured_location">Procured Location <span class="mand">*</span> :</label>
									<select class="form-control" required="required" name="procured_location" id="procured_location" onchange="getAssetCode('location');">
										<option value="">Select Location</option>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_expire_month">Warranty Expiry month <span class="mand">*</span> :</label>
									<select class="form-control" required="required" onchange="getExpiryDt();" name="warranty_expire_month" id="warranty_expire_month">
										<option value="">Please select</option>
										<option value="1">1 Month</option>
										<option value="2">2 Month</option>
										<option value="3">3 Month</option>
										<option value="4">4 Month</option>
										<option value="5">5 Month</option>
										<option value="6">6 Month</option>
										<option value="7">7 Month</option>
										<option value="8">8 Month</option>
										<option value="9">9 Month</option>
										<option value="10">10 Month</option>
										<option value="11">11 Month</option>
										<option value="12">1 Year</option>
										<option value="24">2 Year</option>
										<option value="36">3 Year</option>
										<option value="48">4 Year</option>
										<option value="60">5 Year</option>
										<option value="72">6 Year</option>
									</select>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="warranty_type">Warranty Type <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="warranty_type" id="warranty_type" />
								</div>	
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="expiry_dt">Expiry Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="expiry_dt" id="expiry_dt" readonly="readonly" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="manufacturer">Manufacturer <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="manufacturer" id="manufacturer" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="model_no">Model No. <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="model_no" id="model_no" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="serial_no">Serial Number <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="serial_no" id="serial_no" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3" style="display:none;">
									<label data-lblname="warranty_expire_dt">Warranty Expiry Date <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="warranty_expire_dt" id="warranty_expire_dt" />
								</div>
								
								<!-- new fields -->
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="po_created">PO Created <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="po_created" id="po_created" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="nav_asset_code">NAV Asset Tag <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="nav_asset_code" id="nav_asset_code" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="po_number">PO No. <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="po_number" id="po_number" />
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-3">
									<label data-lblname="po_name">Vendor Name <span class="mand">*</span> :</label>
									<input type="text" required="required"  class="form-control" name="po_name" id="po_name" />
								</div>
								
								
							</div>
							
							<div class="row">
								<div class="form-group col-xs-6 col-sm-12 col-md-6">
									<label data-lblname="description">Description <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="description" id="description"></textarea>
								</div>
								
								<div class="form-group col-xs-6 col-sm-12 col-md-6">
									<label data-lblname="remark">Remark <span class="mand">*</span> :</label>
									<textarea required="required"  class="form-control" name="remark" id="remark"></textarea>
								</div>
							</div>
							<div class="row">
								<span class="pull-right" style="margin-right: 10px;margin-top: 3px;" title="Other item" onclick="add_more()"><i class="ace-icon fa fa-plus-circle bigger-250"></i></span>
							</div>
							
							<div id="add_mor_div">
								
							</div>
							<div class="row" style="margin-top:10px;">
								<center>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</center>
							</div>
						</form>	
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	console.log('working...');
	//$('#procured_dt').datepicker({format: "dd-mm-yyyy",startDate: '-1m',endDate:'now'}).datepicker('setDate','now');
	$('#procured_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#received_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#warranty_expire_dt').datepicker({format: "dd-mm-yyyy"}).datepicker('setDate','now');
	$('#po_created').datepicker({format:"dd-mm-yyyy"}).datepicker('setDate','now');
	
	$("#add_asset").validate({
		rules:{
			asset_type: {required:true},
			asset_name: {required:true},
			present_asset_code: {required:true},
			nav_asset_code: {required:true},
			procured_dt: {required:true},
			warranty_expire_month: {required:true},
			warranty_type: {required:true},
			received_dt: {required:true},
			procured_lob: {required:true},
			approved_hod: {required:true},
			procured_company: {required:true},
			procured_state: {required:true},
			procured_location: {required:true},
			manufacturer: {required:true},
			model_no: {required:true},
			serial_no: {required:true},
			po_created: {required:true},
			po_number: {required:true},
			po_name: {required:true},
			description: {required:true},
			remark: {required:true}
			
		},
		messages: {
			"asset_type": { required: "Please select one asset type" },
			"asset_name": { required: "Please enter asset name" },
			"present_asset_code": { required: "Please enter asset code" },
			"nav_asset_code": { required: "Please enter asset code" },
			"procured_dt": { required: "Please select procured date" },
			"warranty_expire_month": { required: "Please select warranty" },
			"warranty_type": { required: "Please enter warranty type" },
			"received_dt": { required: "Please select received date" },
			"procured_lob": { required: "Please select LOB" },
			"approved_hod": { required: "Please select HOD" },
			"procured_company": { required: "Please enter company" },
			"procured_state": { required: "Please select state" },
			"procured_location": { required: "Please select location" },
			"manufacturer": { required: "Please enter manufacturer" },
			"model_no": { required: "Please enter model no." },
			"serial_no": { required: "Please enter serial no." },
			"po_created": { required: "Please select PO created date" },
			"po_number": { required: "Please enter PO number" },
			"po_name": { required: "Please enter vendor name" },
			"description": { required: "Please enter description" },
			"remark": { required: "Please enter remark" }
			
		},submitHandler: function (form) {
			form.submit();
			$("#spinner").show();
		}
	});
});

//generate asset code automatically start 
var assetCode = '- - -<?php echo getAssetNum(); ?>';
function getAssetCode(generateType){	
	assetCodeDecpt = assetCode.split("-");
	companyShort = assetCodeDecpt[0].trim();
	assetShort = assetCodeDecpt[1].trim();
	locationShort = assetCodeDecpt[2].trim();
	incrementShort  = assetCodeDecpt[3].trim();
	switch(generateType){
		case 'company':
			companyShort = $("#procured_company").find(":selected").attr('data-shortCode');
		break;
		case 'assttype':
			assetShort = $("#asset_type").find(":selected").attr('data-shortCode');
		break;
		case 'location':
			locationShort = $("#procured_location").find(":selected").attr('data-shortCode');
		break;
	}
	assetCode = companyShort+'-'+assetShort+'-'+locationShort+'-'+incrementShort;
	deAsstCode = assetCode.split("-");
	showAssetCode = deAsstCode[0].trim()+''+deAsstCode[1].trim()+''+deAsstCode[2].trim()+''+deAsstCode[3].trim();
	$("#present_asset_code").val(showAssetCode);
	$("#system_ref_code").val(assetCode);
	console.log(assetCode);
}
//generate asset code automatically end

var add_more_count = 1;
function add_more(){
	var add_more_str = '';
	add_more_str = '<div class="row" id="add_more_div_'+add_more_count+'" style="border: 1px solid #393939;margin-top: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>Other Items</b>&nbsp;&nbsp;&nbsp;&nbsp;<div class="row"><span class="pull-right" style="margin-right:20px;margin-top: 3px;" title="Remove_option" onclick="remove_option(\'add_more_div_'+add_more_count+'\')"><i class="ace-icon fa fa-minus-circle bigger-250"></i></span></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="key_id">Key :</label><input type="text" class="form-control" name="key_id[]" id="key_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="processor">Processor :</label><input type="text" class="form-control" name="processor[]" id="processor" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="ram_id">RAM :</label><input type="text" class="form-control" name="ram_id[]" id="ram_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="hdd_id">HDD :</label><input type="text" class="form-control" name="hdd_id[]" id="hdd_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="os_id">OS :</label><input type="text" class="form-control" name="os_id[]" id="os_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="os_key">OS Key :</label><input type="text" class="form-control" name="os_key[]" id="os_key" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="msoffice_id">MS Office :</label><input type="text" class="form-control" name="msoffice_id[]" id="msoffice_id" /></div><div class="form-group col-xs-12 col-sm-12 col-md-3"><label data-lblname="msoffice_key">MS Office Key :</label><input type="text" class="form-control" name="msoffice_key[]" id="msoffice_key" /></div><br></div>';
	$("#add_mor_div").append(add_more_str);
	add_more_count++;
}

function remove_option(div_id){
	$("#"+div_id).remove();
}

function getLocation(){
	$.ajax({
		data:"objCode=getLocation&stateId="+$("#procured_state").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select Location</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].site_location_id+'" data-shortCode="'+rkObj.objData[rk].city_code+'">'+rkObj.objData[rk].site_location_name+'</option>';
			}
			$("#procured_location").html(rkHtm);
		}
	});
}

function getHod(){
	$.ajax({
		data:"objCode=getHod&lobId="+$("#procured_lob").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			var rkHtm = '<option value="">Select HOD</option>';
			for(rk=0;rk<rkObj.objData.length;rk++){
				rkHtm += '<option value="'+rkObj.objData[rk].inv_hod_id+'">'+rkObj.objData[rk].hod_name+'</option>';
			}
			$("#approved_hod").html(rkHtm);
		}
	});
}

function getExpiryDt(){
	$.ajax({
		data:"objCode=getExpiryDate&warnty_month="+$("#warranty_expire_month").val()+"&procur_dt="+$("#procured_dt").val(),
		url:"<?php echo base_url().'admin/'; ?>getObject",
		type:"POST",
		success:function(resp){
			rkObj = [];
			rkObj = JSON.parse(resp);
			console.log(rkObj);
			$("#expiry_dt").val(rkObj.objData);
		}
	});
}

//function not userd yet
Date.prototype.addMonths = function (m) {
    var d = new Date($("#procured_dt").val());
	$('#expiry_dt').datepicker({format: "dd-mm-yyyy",startDate: '+5m',endDate:'now'}).datepicker('setDate','now');
	console.log('date ');
    var years = Math.floor(m / 12);
    var months = m - (years * 12);
    if (years) d.setFullYear(d.getFullYear() + years);
    if (months) d.setMonth(d.getMonth() + months);
    return d;
}
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>