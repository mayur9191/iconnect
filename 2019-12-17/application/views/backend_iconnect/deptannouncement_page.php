<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit announcement</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_dept_announcement'; ?>" method="POST" enctype="multipart/form-data">
			  <div class="form-group">
				<label ><b>Announcement Name : </b></label>
				<input type="hidden" name="announce_id" value="<?php echo $deptannouncement_data->announcement_id; ?>" />
				<input type="text" name="announcement_name" value="<?php echo $deptannouncement_data->announcement_name; ?>" class="form-control" />
			  </div>
			  
			  <div class="form-group">
				<label ><b>Announcement Description : </b></label>
				<textarea  name="announcement_desc" class="form-control"><?php echo $deptannouncement_data->announcement_desc; ?></textarea>
			  </div>
			  
			  <div class="form-group">
				<label><b> Announcement start date : </b></label>
				<input type="text" name="st_date" value="" class="form-control" />
			  </div>
			  
			  <div class="form-group">
				<label><b> Announcement end date : </b></label>
				<input type="text" name="ed_date" value="" class="form-control" />
			  </div>
			  
			  <div class="form-group">
				<label><b> Announcement Attachment : </b></label>
				<input type="file" name="annoucement_attach" value="" class="form-control" />
			  </div>
			  
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add announcement</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_dept_announcement'; ?>" method="POST" enctype="multipart/form-data">
			
			 <div class="form-group">
				<label ><b>Announcement Name : </b></label>
				<input type="text" name="announcement_name" value="" class="form-control" />
			  </div>
			  
			  <div class="form-group">
				<label ><b>Announcement Description : </b></label>
				<textarea  name="announcement_desc" value="" class="form-control"></textarea>
			  </div>
			  
			  <div class="form-group">
				<label><b> Announcement start date : </b></label>
				<input type="date" name="st_date" value="" class="form-control" />
			  </div>
			  
			  <div class="form-group">
				<label><b> Announcement end date : </b></label>
				<input type="text" name="ed_date" value="" class="form_datetime" />
			  </div>
			  
			  <div class="form-group">
				<label><b> Announcement Attachment : </b></label>
				<input type="file" name="annoucement_attach" value="" class="form-control" />
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
</script>  
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View announcement</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Quicklink Name : </b></label>
			<span><?php echo $quicklink_data->quick_link_name; ?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Quicklink URL : </b></label>
			<span><?php echo $quicklink_data->quick_link_url; ?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Quicklink Description : </b></label>
			<span><?php echo $quicklink_data->quick_link_desc; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

