<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Technician Assign</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_assigntech'; ?>" method="POST">
			  
			  <div class="form-group">
				<label ><b>Group Name : </b></label>
				<select class="form-control" name="group_id">
					<?php 
						foreach(get_all_record_by_code('group') as $group_record){
							if($group_record->group_id == $assigntech_data->group_id){
								echo '<option value="'.$group_record->group_id.'" selected>'.ucfirst($group_record->group_name).'</option>';
							}else{
								echo '<option value="'.$group_record->group_id.'">'.ucfirst($group_record->group_name).'</option>';
							}
						} 
					?>
				</select>
				<input type="hidden" name="assign_id" value="<?php echo $assigntech_data->technician_group_map_id; ?>" />
			  </div>
			  
			  
			  <div class="form-group">
				<label ><b>Technician Name : </b></label>
				<select class="form-control" name="tech_id">
					<?php 
						foreach(get_all_record_by_code('technician') as $tech_record){
							if($tech_record->technician_id == $assigntech_data->technician_id){
								echo '<option value="'.$tech_record->technician_id.'" selected>'.ucfirst($tech_record->tech_name).'</option>';
							}else{
								echo '<option value="'.$tech_record->technician_id.'">'.ucfirst($tech_record->tech_name).'</option>';
							}
						} 
					?>
				</select>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Assign Technician</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_assigntech'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>Group Name : </b></label>
				<select class="form-control" name="group_id">
					<?php 
						foreach(get_all_record_by_code('group') as $group_record){
							echo '<option value="'.$group_record->group_id.'">'.ucfirst($group_record->group_name).'</option>';
						} 
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Technician Name : </b></label>
				<select class="form-control" name="tech_id">
					<?php 
						foreach(get_all_record_by_code('technician') as $tech_record){
							echo '<option value="'.$tech_record->technician_id.'">'.ucfirst($tech_record->tech_name).'</option>';
						} 
					?>
				</select>
			  </div>
			 
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Group Assign</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Group Name : </b></label>
			<span><?php 
					$groupData = get_record_by_id_code('group',$assigntech_data->group_id);
					$groupName = $groupData->group_name;
					
					$technician_data = get_record_by_id_code('technician',$assigntech_data->technician_id);
					$technicianName = $technician_data->tech_name;
					echo $groupName;
				?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Technician Name : </b></label>
			<span><?php echo $technicianName; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

