<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
    <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />

<style>
.mandatory{
	color:RED;
}
</style>
	
</head>

<body >
<?php include APPPATH . "views/includs/top_navbar.php" ?>

<div class="container-fluid main">
	<div class="row">
		<div class="col-md-12">

			<div class="col-md-3 oym-iconnect nopad_left_right">


				<?php include APPPATH . "views/module/profile/common/left_box.php" ?>
			</div>

			<div class="col-md-8">

			
			<form action="" method="post" id="form">
			<h2>Edit Profile</h2>
			
			<table class="table table-border table hover">
			<tr><td>EMP STAFFID</td><td><input type="text" name="EMP_STAFFID" class="form-control" value="<?= $CI->user_ldamp['employeeid'] ? $CI->user_ldamp['employeeid'] :'' ;  ?>" readonly />
			
				<input type="hidden" name="ROW_ID" class="form-control" value="<?= $CI->hrdata_user_info['ROW_ID'] ? $CI->hrdata_user_info['ROW_ID'] :'' ;  ?>" />
			</td></tr>
			
			<tr><td>SURNAME <span class="mandatory">*</span></td><td><input type="text" name="SURNAME" class="form-control" value="<?= $CI->hrdata_user_info['SURNAME'] ? $CI->hrdata_user_info['SURNAME'] :'' ;  ?>" required /></td></tr>
			
			<tr><td>FIRST NAME <span class="mandatory">*</span></td><td><input type="text" name="FIRST_NAME" class="form-control" value="<?= $CI->hrdata_user_info['FIRST_NAME'] ? $CI->hrdata_user_info['FIRST_NAME'] :'' ;  ?>" required /></td></tr>
			
			<tr><td>MIDDLE NAME <span class="mandatory">*</span></td><td><input type="text" name="MIDDLE_NAME" class="form-control" value="<?= $CI->hrdata_user_info['MIDDLE_NAME'] ? $CI->hrdata_user_info['MIDDLE_NAME'] :'' ;  ?>" required /></td></tr>
			
			<tr><td>KNOWN NAME <span class="mandatory">*</span></td><td><input type="text" name="KNOWN_NAME" class="form-control" value="<?= $CI->hrdata_user_info['KNOWN_NAME'] ? $CI->hrdata_user_info['KNOWN_NAME'] :'' ;  ?>" required /></td></tr>
			
			<tr><td>OFFICIAL NAME <span class="mandatory">*</span></td><td><input type="text" name="OFFICIAL_NAME" class="form-control" value="<?= $CI->hrdata_user_info['OFFICIAL_NAME'] ? $CI->hrdata_user_info['OFFICIAL_NAME'] :'' ;  ?>" required /></td></tr>
			
			<tr><td>DATE OF BIRTH <span class="mandatory">*</span></td><td><input type="text" name="DATE_OF_BIRTH" class="form-control datepicker" value="<?= $CI->hrdata_user_info['DATE_OF_BIRTH'] ? date('d-m-Y',strtotime($CI->hrdata_user_info['DATE_OF_BIRTH'])) :'' ;  ?>" required /></td></tr>
			
			<tr><td>STATE OF BIRTH <span class="mandatory">*</span></td><td><input type="text" name="STATE_OF_BIRTH" class="form-control" value="<?= $CI->hrdata_user_info['STATE_OF_BIRTH'] ? $CI->hrdata_user_info['STATE_OF_BIRTH'] :'' ;  ?>" required /></td></tr>
			
			<tr><td>COUNTRY OF BIRTH <span class="mandatory">*</span></td><td><input type="text" name="COUNTRY_OF_BIRTH" class="form-control" value="<?= $CI->hrdata_user_info['COUNTRY_OF_BIRTH'] ? $CI->hrdata_user_info['COUNTRY_OF_BIRTH'] :'' ;  ?>" required /></td></tr>
			
			<?php if($CI->hrdata_user_info['WEDDING_DATE'] != '0000-00-00 00:00:00'){
					$WEDDING_DATE = date('d-m-Y',strtotime($CI->hrdata_user_info['WEDDING_DATE']));
				}else{
					$WEDDING_DATE = '';
				}
			?>
			<tr><td>MARITAL STATUS <span class="mandatory">*</span></td><td>
			<!--<input type="text" name="MARITAL_STATUS" class="form-control" value="<?= $CI->hrdata_user_info['MARITAL_STATUS'] ? $CI->hrdata_user_info['MARITAL_STATUS'] :'' ;  ?>" required />-->
			
			<select class="form-control" name="MARITAL_STATUS" id="mrgStatus" required >
				<option> Please Select marital status </option>
				<option <?= $CI->hrdata_user_info['MARITAL_STATUS']=='Married' ? 'selected=""' :'' ;  ?> value="Married">Married</option>
				<option <?= $CI->hrdata_user_info['MARITAL_STATUS']=='Un-married' ? 'selected=""' :'' ;  ?> value="Un-married">Un-married</option>
			</select>
			
			</td></tr>
			
			<tr><td>WEDDING DATE</td><td><input type="text" name="WEDDING_DATE" id="mrgStatusDt" class="form-control datepicker" value="<?= $WEDDING_DATE; ?>" <?= $CI->hrdata_user_info['MARITAL_STATUS']=='Un-married' ? 'disabled=""' :'' ;  ?> /></td></tr>
			
			<tr><td>NATIONALITY</td><td><input type="text" name="NATIONALITY" class="form-control" value="<?= $CI->hrdata_user_info['NATIONALITY'] ? $CI->hrdata_user_info['NATIONALITY'] :'' ;  ?>"  /></td></tr>
			
			<tr><td>PAN NUMBER <span class="mandatory">*</span></td><td><input type="text" name="PAN_NUMBER" class="form-control" value="<?= $CI->hrdata_user_info['PAN_NUMBER'] ? $CI->hrdata_user_info['PAN_NUMBER'] :'' ;  ?>"  required /></td></tr>
			
			<tr><td>PREVIOUS NAME</td><td><input type="text" name="PREVIOUS_NAME" class="form-control" value="<?= $CI->hrdata_user_info['PREVIOUS_NAME'] ? $CI->hrdata_user_info['PREVIOUS_NAME'] :'' ;  ?>"  /></td></tr>
			
			<tr><td>LANGUAGES</td><td><input type="text" name="LANGUAGES" class="form-control" value="<?= $CI->hrdata_user_info['LANGUAGES'] ? $CI->hrdata_user_info['LANGUAGES'] :'' ;  ?>"  /></td></tr>
			
			
			<tr><td>GENDER <span class="mandatory">*</span></td><td>
				<select class="form-control" name="GENDER" required >
					<option> Please Select Gender </option>
					<option <?= $CI->hrdata_user_info['GENDER']=='Male' ? 'selected=""' :'' ;  ?> value="Male">Male</option>
					<option <?= $CI->hrdata_user_info['GENDER']=='Female' ? 'selected=""' :'' ;  ?> value="Female">Female</option>
				</select>
			</td></tr>
			<tr><td>BLOOD GROUP</td><td>
				<select class="form-control" name="BLOOD_GROUP">
					<option> Please Select blood group </option>
					<option value="O+" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='O+' ? 'selected=""' :'' ;  ?>> O+ </option>
					<option value="O-" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='O-' ? 'selected=""' :'' ;  ?>> O- </option>
					<option value="A+" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='A+' ? 'selected=""' :'' ;  ?>> A+ </option>
					<option value="A-" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='A-' ? 'selected=""' :'' ;  ?>> A- </option>
					<option value="B+" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='B+' ? 'selected=""' :'' ;  ?>> B+ </option>
					<option value="B-" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='B-' ? 'selected=""' :'' ;  ?>> B- </option>
					<option value="AB+" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='AB+' ? 'selected=""' :'' ;  ?>> AB+ </option>
					<option value="AB-" <?= $CI->hrdata_user_info['BLOOD_GROUP']=='AB-' ? 'selected=""' :'' ;  ?>> AB- </option>
				</select>
			<!--<input type="text" name="BLOOD_GROUP" class="form-control" value="<?= $CI->hrdata_user_info['BLOOD_GROUP'] ? $CI->hrdata_user_info['BLOOD_GROUP'] :'' ;  ?>"  />-->
			</td></tr>
			
			
			
			<tr><td>AADHAR_NO</td><td><input type="text" name="AADHAR_NO" class="form-control" value="<?= $CI->hrdata_user_info['AADHAR_NO'] ? $CI->hrdata_user_info['AADHAR_NO'] :'' ;  ?>"  /></td></tr>
			
			<tr><td>ETHINIC</td><td><input type="text" name="ETHINIC" class="form-control" value="<?= $CI->hrdata_user_info['ETHINIC'] ? $CI->hrdata_user_info['ETHINIC'] :'' ;  ?>"  /></td></tr>
			
			<tr><td></td><td style="padding-left: 100px;"><input type="submit" name="submit" class="form-control btn btn-info" style="width: 100px;" value="Update" /></td></tr>
			
			</table>
			
			</form>
			  
			</div>

			<!--<div class="col-md-3 oym-iconnect nopad_left_right">
				<?php //include APPPATH . "views/module/profile/common/right_box.php" ?>
			</div>-->

		</div>
	</div>
</div>

<!--<div class="row">
    <a href="#" class="nextpage-arrow"><span class="glyphicon glyphicon-arrow-right"></span></a>
</div>-->



<?php include APPPATH . "views/includs/common_scripts.php" ?>

<!--<script type="text/javascript" src="--><? //= base_url() . 'assets/' ?><!--croppic/croppic.min.js"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>




<script type="text/javascript">
$(document).ready(function () {
    $("#form").validate({
        rules: {
            "PAN_NUMBER": {
                required: true,
                postalCode:true,
                minlength: 10
            },
			"SURNAME": {
                required: true,
                lettersonly:true
            },
			"FIRST_NAME": {
                required: true,
				lettersonly:true
            },
			"MIDDLE_NAME": {
                required: true,
                lettersonly:true
            },
			"KNOWN_NAME": {
                required: true,
				lettersonly:true
            },
			"OFFICIAL_NAME": {
                required: true,
                lettersonly:true
            },
        },
        messages: {
            "PAN_NUMBER": {
                required: "Please, enter a name",
                minlength:"Please, enter aleast 10 characters",
               // maxlength:"Please, enter not more than 10 characters"
            },
			"SURNAME": {
                required: "Please, enter a surname"
            },
			"FIRST_NAME": {
                required: "Please, enter a first name"
            },
			"MIDDLE_NAME": {
                required: "Please, enter a middle name"
            },
			"KNOWN_NAME": {
                required: "Please, enter a known name"
            },
			"OFFICIAL_NAME": {
                required: "Please, enter a official name"
            },
        },
        submitHandler: function (form) { 
            //alert('valid form submitted');
            return true;
        }
    });

});

$.validator.addMethod( "lettersonly", function( value, element ) {
    return this.optional( element ) || /^[a-z," "]+$/i.test( value );
}, "Letters only please" );


$.validator.addMethod('postalCode', function (value,element) { 
	var str = value.length;
	if(str >0 && str <= 5 )
	{
        if(/^[A-z]{5}$/.test(value) === true)
        {
            result = true;
            
        }
        else{
        	result = false;
            message = " Please, enter character now";
        }		
        
	}
	else if(str > 5 && str <=9)
	{
		if(/^[A-z]{5}\d{4}$/.test(value) === true)
        {
            result = true;
            
        }	
        else{
        	 result = false;
             message = " Please, enter digits now";
        }	
       
	}
	else if(str ==10)
	{
		if(/^[A-z]{5}\d{4}[A-z]{1}$/.test(value) === true)
        {
            result = true;
            
        }
        else
        {
        	result = false;
            message = " Please, enter character now";
        }
	}
    else{
          result = false;
          message = "Please, enter not more than 10 characters";
            
        }
	
	return this.optional(element) || result;
},function() { return message; });

</script>





<script type="text/javascript">


    $(function () {
		
		//code R
		$("#mrgStatus").on('change',function(){
			var val = $(this).val();
			if(val=='Un-married'){
				$("#mrgStatusDt").attr('readonly',true);
				$("#mrgStatusDt").prop('disabled',true);
			}else{
				$("#mrgStatusDt").attr('readonly',false);
				$("#mrgStatusDt").prop('disabled',false);
			}
		});
		
		
		
		$(".datepicker").datepicker(
		{
			 format: 'dd-mm-yyyy'
		}
		);

        $("#likeId").on("click", function () {
            var birthdayVal = $(this).attr("data-birthdays");
            var currntUid = $(this).attr("data-current");
            $.post('<?php echo base_url();?>Profile/birthdayLike', {
                    birthday_user_ids: birthdayVal,
                    like_user_id: currntUid
                },
                function (rps) {
                    console.log(rps);
                    $("#likeId").html('Liked');
                    $("#likeId").removeAttr("id");
                });
        });


        $("#wallpostForm").validate({
            rules: {
                walltext: {
                    required: true
//                    email: true
                }
            },
            messages: {
                walltext: {
                    required: " is required"
//                    email: "Enter Valid Email"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $(".changedp").validate({
            rules: {
                image: {
                    required: true
//                    email: true
                }
            },
            messages: {
                image: {
                    required: " is required"
//                    email: "Enter Valid Email"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $('.delete_wall_item').on('click', function () {
            console.log($(this).attr('wallid'))

            var wallid = $(this).attr('wallid');
            var wallcalss = $(this).attr('wallid');

            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'ajax/wallitemremove' ?>",
                data: {wallid: wallid},
                cache: false,
                success: function (data) {
                    console.log(data);

                    if (data == 1) {
                        $('.wallitem_' + wallid).slideUp();
                    }

//                    $("#resultarea").text(data);
                }
            });


        })

        $(".fileUpload").on('change', function () {
            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof(FileReader) != "undefined") {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "img-responsive"
                            }).appendTo(image_holder);
                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Pls select only images");
            }
        });


    });


</script>

</body>
</html>
