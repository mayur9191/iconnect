<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
   <?php include APPPATH . "views/includs/hedder_code.php" ?>
<style>
.alg-center{
	text-align:center;
}
input[type="checkbox"]{ width: 20px; height: 20px;}
</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<?php $approvalData = getMaterialApproval($passingData->material_master_id); ?>
<br><br><br>
 <div class="container" style="border: 2px groove;">
 <?php //echo '<pre>'; //print_r($userList); ?>
   <center><h3>MATERIAL GATE PASS PERMISSION</h3></center>
   <br>
	<div class="row">
	<form action="<?php echo base_url().'home/submitPermission'?>" method="POST">
	
	  <table class="table" border="">
		<thead>
		  <tr>
			<th class="alg-center">Prepared by</th>
			<th class="alg-center">Checked by</th>
			<th class="alg-center">Approved 1 by</th>
			<th class="alg-center">Approved 2 by</th>
		  </tr>
		</thead>
		<tbody>
		<?php foreach($userList as $userRecord) { ?>
		
		  <tr> 
			<td <?php echo (checkPermission($userRecord->user_role_id,1))? 'style="background-color: darkseagreen;"':''; ?>><input type="checkbox" name="prepared_by[]" value="<?php echo $userRecord->user_role_id ?>" <?php echo (checkPermission($userRecord->user_role_id,1))? 'checked="checked"':''; ?> />  <b><?php echo $userRecord->user_emp_code; ?> </b> -- <?php echo $userRecord->user_role_name; ?></td>
			
			<td <?php echo (checkPermission($userRecord->user_role_id,2))? 'style="background-color: darkseagreen;"':''; ?>><input type="checkbox" name="checked_by[]" value="<?php echo $userRecord->user_role_id; ?>" <?php echo (checkPermission($userRecord->user_role_id,2))? 'checked="checked"':''; ?> />  <b><?php echo $userRecord->user_emp_code; ?></b> -- <?php echo $userRecord->user_role_name; ?></td>
			
			<td <?php echo (checkPermission($userRecord->user_role_id,3))? 'style="background-color: darkseagreen;"':''; ?>><input type="checkbox" name="approve1_by[]" value="<?php echo $userRecord->user_role_id; ?>" <?php echo (checkPermission($userRecord->user_role_id,3))? 'checked="checked"':''; ?> />  <b><?php echo $userRecord->user_emp_code; ?></b> -- <?php echo $userRecord->user_role_name; ?></td>
			
			<td <?php echo (checkPermission($userRecord->user_role_id,4))? 'style="background-color: darkseagreen;"':''; ?>><input type="checkbox" name="approve2_by[]" value="<?php echo $userRecord->user_role_id ?>" <?php echo (checkPermission($userRecord->user_role_id,4))? 'checked="checked"':''; ?> />  <b><?php echo $userRecord->user_emp_code; ?></b> -- <?php echo $userRecord->user_role_name; ?></td>
		  </tr>
		  
		<?php } ?>
		</tbody>
	</table>
	<center><input type="submit" value="Submit" class="btn btn-lg btn-info" /></center>
		
	</form>
	</div>
	<br><br><br><br><br>
	
	</div>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
<script>
	var trVal = 1;
	$(document).ready(function(){
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
		$("input[type=checkbox]").change(function(){
			if($(this).is(":checked")){
				$(this).parent("td").attr('style','background-color: darkseagreen;');
			}else{
				$(this).parent("td").removeAttr('style');
			}
		});
	});
	
	function addRow(){
		var rowHtm = '<tr id="tr_'+trVal+'"><td><input type="text" placeholder="S.No." class="form-control" value="1" ></td><td><input type="text" placeholder="Item" class="form-control"></td><td><input type="text" placeholder="Description" class="form-control"></td><td><input type="text" placeholder="No" class="form-control"></td><td><input type="text" placeholder="Unit" class="form-control"></td><td><input type="text" placeholder="Returnable / Non-returnable" class="form-control"></td><td><input type="text" placeholder="Remark" class="form-control"><button type="button" class="btn btn-sm btn-info" onclick="removeRow('+trVal+')">-</button></td></tr>';
		$("#adrow").append(rowHtm);
		trVal++;
		console.log($("#adrow").find('> tr').length);	
		var lengthSn = $("#adrow").find('> tr').length;
		var j = 1;
		for(var i=0;i<=lengthSn;i++){
			$("#adrow").find('> tr').eq(i).find('> td').find('> input').eq(0).val(j);
			j++;
		}
	}
	
	function removeRow(val){
		console.log('the remove val is '+val);
		$("#tr_"+val).remove();
		var lengthSn = $("#adrow").find('> tr').length;
		var j = 1;
		for(var i=0;i<=lengthSn;i++){
			$("#adrow").find('> tr').eq(i).find('> td').find('> input').eq(0).val(j);
			j++;
		}
	}
</script>
</body>
</html>
