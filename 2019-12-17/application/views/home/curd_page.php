<?php
 error_reporting(E_ALL); 
?>
<?php 
 // Constant declaretion
$controllerName = 'SClocation'; 
$modelName = 'SClocation';
$folderName = 'SClocation';
$headerName = 'SC Location Management';
$msgPrefix = 'SC Location';
$functionPrefix = 'location';
$tableName = 'SCLocation';
$tableStatusKey = 'locationIsActive';

$buttonName ='Location';

$databaseTabelQryFieldListAry = array('locationId as lId','SCId as sId','zoneId as zId','locationName as lName','locationAddress as lAddress','locationLatLong as lLatlong','locationIsActive as lActive');
$frontEndViewFields = array('Location Name','Zone Name');
$frontJSViewFields = array('lName','zId');
$frontEditViewFields = array('locationId','zoneName','locationName');
$frontJSViewKeyId = 'lId';
?>

<br>Controller code ---------------------------------<br>
<textarea style="width:90%;height:400px;">
&lt;?php
class <?php echo $controllerName; ?> extends CI_Controller {
	private $SCId = 0;
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('<?php echo $modelName; ?>_model');
		$this->load->model('admin/Admin_model');
		$this->Admin_model->checkAdminLogin();
		$this->SCId = $this->Admin_model->getSCId();
	}
	/**
	* Function index
	* function load when class load
	*/
	public function <?php echo $functionPrefix;?>Index(){
		$data['breadcrum'] = array('<?php echo $headerName; ?>','1');
		$resultData = $this-><?php echo $modelName; ?>_model->get<?php echo $functionPrefix;?>AllDetails($this->SCId);
		$data['resultData'] = json_encode($resultData);
		$data['include'] = '<?php echo $folderName;?>/manage';
		$data['success_msg'] = $this->Admin_model->getFlash();
		$this->load->view('backend/container',$data);
	}
	/**
	* Function details
	* @params #id, #type
	* #get data from content_manage
	* #redirect url
	*/
	public function <?php echo $functionPrefix;?>Details($viewType=0,$keyId=0,$status=0){ 
		$data['breadcrum'] = array('<?php echo $headerName; ?>','0');
		switch($viewType){
			case 'view':
				$data = $this->get<?php echo $functionPrefix;?>View($keyId);
				$data['include']='<?php echo $folderName;?>/info';
			break;
			case 'edit':
				$data = $this->get<?php echo $functionPrefix;?>View($keyId);
				$data['action'] = 'edit';
				$data['SCId']=$this->SCId;
				$data['include']='<?php echo $folderName;?>/add';
			break;
			case 'status':
				$data = $this-><?php echo $functionPrefix;?>ChangeStatus($keyId,$status);
			break;
			case 'add':
				$data = $this->get<?php echo $functionPrefix;?>View();
				$data['action'] = 'add';
				$data['SCId']=$this->SCId;
				$data['include']='<?php echo $folderName;?>/add';
			break;
			case 'back':
				$this->_<?php echo $functionPrefix;?>Redirect();
			break;
		}
		$this->load->view('backend/container',$data);
	}
	/**
	* Function addNew
	* @params 
	* #new record inserted into Table
	* return success_msg else error_msg
	*/
	public function <?php echo $functionPrefix;?>Add(){
		$recordData = getSClocationArray('add',$this->SCId);
		if($this->Admin_model->addRecord(<?php echo $tableName; ?>,$recordData)){
			//success_msg
			$this->Admin_model->setFlash(1,'<?php echo $msgPrefix; ?> record Inserted Successfully');
			$this->_<?php echo $functionPrefix;?>Redirect();
		}else{
			//error_msg
			$this->Admin_model->setFlash(2,'Error while <?php echo $msgPrefix; ?> record insertion');
			$this->_<?php echo $functionPrefix;?>Redirect();
		}
	}
	/**
	* Function getEdit
	* @params id -- Table Id
	* #return to table details function 
	* return success_msg else error_msg
	*/
	public function <?php echo $functionPrefix;?>Edit(){
		if($this->input->post('keyId') > 0){
			$recordData = getSClocationArray('edit',$this->SCId);
			$keyId = $this->input->post('keyId');
			if($this->Admin_model->editRecord(<?php echo $tableName; ?>,$recordData,$keyId,'locationId')){
				//success msg here
				$this->Admin_model->setFlash(1,'<?php echo $msgPrefix; ?> record Updated Successfully');
				$this->_<?php echo $functionPrefix;?>Redirect();
			}else{
				//error msg here
				$this->Admin_model->setFlash(2,'Error while <?php echo $msgPrefix; ?> updating record');
				$this->_<?php echo $functionPrefix;?>Redirect();
			}
		}
	}
	/**
	* Function getView
	* @params id -- Table Id
	* #return to details function 
	* return user record
	*/
	public function get<?php echo $functionPrefix;?>View($keyId=0){
		if($keyId > 0){
			$data['resultData'] = $this-><?php echo $modelName; ?>_model->get<?php echo $functionPrefix;?>Info($keyId,$this->SCId);
		}else{
			$data['resultData'] = '';
		}
		$data['getRData'] = $this-><?php echo $modelName; ?>_model->getRData($this->SCId);
		return $data;
	}
	/**
	* Function changeStatus
	* @params #id
	* #change status true or false -- Table
	* return #redirect_url
	*/
	 
	public function <?php echo $functionPrefix;?>ChangeStatus(){
		$postData = $this->input->post();
		$keyId= $postData['mKey']; 
		$recordValue=$postData['mStatus'];
		if($keyId > 0){
			$statusAry = array(
			'<?php echo $tableStatusKey; ?>'=>$recordValue
			);
			if($this-><?php echo $modelName; ?>_model->status<?php echo $functionPrefix;?>Change($keyId,$statusAry,$this->SCId)){
				if($recordValue == 1){
					$returnAry = array('status'=>true,'valu'=>0,'contxt'=>'De-Active','msg'=>'Record Activated Successfully');
				}else{
					$returnAry = array('status'=>true,'valu'=>1,'contxt'=>'Active','msg'=>'Record De-Activated Successfully');
				}
			}else{
				$returnAry = array('status'=>false,'msg'=>'Error while data submission');
			}
		}else{
			$returnAry = array('status'=>false,'msg'=>'Invalid data request');
		}
		echo json_encode($returnAry);
	}
	/**
	* Function _redirect
	* #redirect page to home
	*/
	public function _<?php echo $functionPrefix;?>Redirect(){
		$url = '<?php echo $controllerName; ?>/'<?php echo $functionPrefix;?>Index; 
		redirect($url);
	}
	/**
	* Function bulidArray
	* @params $_POST data 
	* return build array to store into database
	*/
	/*public function bulidArray(){
		$data = array(
		'SCId' => $this->SCId,
		'subjectName' => $this->input->post('subjectName'),
		'subjectYear' => $this->input->post('subjectYear'),
		'subjectCreatedDate' => $this->input->post('subjectCreatedDate'),
		'subjectModifiedDate'=>$this->input->post('subjectModifiedDate')
		);
		return $data;
	}*/

}





//Designed and Developed by RaviArsid (ravi.arsid2012@gmail.com)

</textarea>


<br>Model code --------------------------------------<br>
<textarea style="width:90%;height:400px;">
&lt;?php
class <?php echo $modelName; ?>_Model extends CI_Model
{
	public function __construct(){ 
		parent::__construct();
	}
	 
	/***
	* Function getAllDetails
	* return - all details from Table Array
	*/
	public function get<?php echo $functionPrefix;?>AllDetails($SCId=0){
		$result1 = '';
		$this->db->select('locationId as lId,SCId as sId,zoneId as zId,locationName as lName,locationAddress as lAddress,locationLatLong as lLatlong,locationIsActive as lActive')->from(<?php echo $tableName; ?>)->where('SCId',$SCId);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result();
			foreach($result as $record){
				$rkReturnAry = array(
				'lId'=>$record->lId,
				'sId'=>$record->sId,
				'zId'=>getZoneNamefromId($record->zId),
				'lName'=>$record->lName,
				'lAddress'=>$record->lAddress,
				'lActive'=>$record->lActive,
				);
				$result1[] = $rkReturnAry;
			}		 
		}
		return $result1;
	}
	/**
	* Function getInfo
	* @params #id -- From Table
	* return users single record 
	*/
	public function get<?php echo $functionPrefix;?>Info($keyId=0,$SCId=0){
		$result = array();
		$this->db->select('*')->from(<?php echo $tableName; ?>)->where('locationId',$keyId)->where('SCId',$SCId);
		$query=$this->db->get();
		if($query->num_rows() > 0){
			$result = $query->row(); 
		}
		return $result;
	}
	/** 
	* Function statuschange
	* @params #id, #value
	* update statuschange -- From Table 
	* return if success true else false 
	*/
	public function status<?php echo $functionPrefix;?>Change($keyId,$recordAry=0,$SCId=0){
		$this->db->where('locationId',$keyId);
		$this->db->where('SCId',$SCId);
		if($this->db->update(<?php echo $tableName; ?>,$recordAry)){
			return true;
		}else{
			return false;
		}
	}
	public function getRData($SCId=0){
		$result = '';
		$this->db->select('locationId as lId,SCId as sId,zoneId as zId,locationName as lName,locationAddress as lAddress,locationLatLong as lLatlong,locationIsActive as lActive')->from(<?php echo $tableName; ?>)->where('SCId',$SCId);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result();
			return $result;
		}
		return $result;
	}

}






//Designed and Developed by RaviArsid (ravi.arsid2012@gmail.com)
</textarea>

<br>View code ---------------------------------------<br>
<br> Manage Code ------------------------------------<br>
<textarea style="width:90%;height:400px;">

<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <a href="&lt;?php schUrl('addlocation'); ?>" class="btn btn-info pull-right" >Add <?php echo $buttonName; ?></a>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                  <?php for($i=0;$i<count($frontEndViewFields);$i++){
                    echo '<th>'.$frontEndViewFields[$i].'</th>';
                  }?><th>Action</th>
                  </tr>
                </thead>
                <tbody id="rkDataPop">
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!--/.col (left) -->
      </div>   <!-- /.row -->
<script>

$(document).ready(function(){
var item = <?php echo $resultData; ?>;
var trHTML = '';
        for (var key=0, size=item.length; key<size; key++){
        <?php $str = '';
        for($i=0;$i<count($frontJSViewFields);$i++){
            $str .="<td>' +item[key].".$frontJSViewFields[$i]."+'</td>";
          }
          ?>
            trHTML +='<tr>'<?php echo $str; ?>'<td>'+addAction(item[key].<?php echo $frontJSViewKeyId; ?>,item[key].sActive)+'</td></tr>';
        }
        $('#rkDataPop').append(trHTML);
});

function changesStatus(key,status){
alertify.success('Your request is received. Please wait...');
var urlStr1 = '&lt;?php echo base_url();?>'<?php echo $controllerName; ?>'/'<?php echo $functionPrefix;?>'ChangeStatus';
$.post(urlStr1, {mStatus:status,mKey:key}, function(result){
var json = $.parseJSON(result);
if(json.status){
    $('#stat_'+key).html(json.contxt);
    $('#stat_'+key).attr('onclick','changesStatus('+key+','+json.valu+')');
           alertify.success(json.msg);
}else{
   alertify.error(json.msg);
}
    });
}
function addAction(key,status){
//var key= encript(key);
var urlStr = '&lt;?php echo base_url();?><?php echo $controllerName; ?>/<?php echo $functionPrefix;?>Details/edit/'+key;
var str = 'Active';
if(status == 1){
str = 'De-Active';
urlStr1 = '&lt;?php echo base_url();?><?php echo $controllerName; ?>/active/'+key;
status = 0;
}else{
status = 1;
}
   var actionStr = '<div class="btn-group"><button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>Action</span></button><ul class="dropdown-menu"><li><a href="'+urlStr+'">Edit Record</a></li><li><a id="stat_'+key+'" href="#" onClick="changesStatus('+key+','+status+')" >'+str+'</a></li></ul></div>';
return actionStr;
}
</script> 

</textarea>
<br> Add/Edit Code -----------------------------------<br>


<textarea style="width:90%;height:400px;">
<script src="&lt;?php echo base_url(); ?>themes/plugins/select2/select2.full.min.js"></script>
 
&lt;?php if($action=='add'){ ?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add <?php echo $buttonName; ?></h3>
                  <a href="&lt;?php echo base_url().'<?php echo $controllerName; ?>'; ?>" class="btn btn-primary pull-right" >Back</a>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="formdata" method="POST" action="&lt;?php echo base_url(); ?><?php echo $controllerName; ?>/<?php echo $functionPrefix;?>Add">
                  <div class="box-body">
					<?php for($i=0;$i<count($frontEndViewFields); $i++) { 
						$fieldName ='';
						$fieldTitle = $frontEndViewFields[$i];
						$fieldData = explode(" ",$frontEndViewFields[$i]);
						for($rk=0;$rk<count($fieldData);$rk++){
							if($rk == 0){
								$fieldName .= strtolower($fieldData[$rk]);
							}else{
								$fieldName .= $fieldData[$rk];
							}
						}
					?>
					<div class="form-group ">
					  <label for="exampleInputEmail1"><?php echo $frontEndViewFields[$i]; ?></label>
					  <input type="text" class="form-control" name="<?php echo $fieldName; ?>" placeholder="Enter <?php echo $fieldTitle; ?>">
					</div>
					<?php } ?>
					
                  </div><!-- /.box-body -->

                  <div class="box-footer">
    <input type="submit" class="btn btn-primary" value="Submit" />
    <input type="reset" class="btn btn-default" value="Reset" />
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-2" style="padding-left: 0px;">
              <!-- Horizontal Form -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>
&lt;?php } 
if($action=='edit'){ ?>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit <?php echo $buttonName; ?></h3>
                  <a href="&lt;?php echo base_url().'<?php echo $controllerName; ?>'; ?>" class="btn btn-primary pull-right" >Back</a>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="formdata" method="POST" action="&lt;?php echo base_url(); ?><?php echo $controllerName; ?>/<?php echo $controllerName; ?>Edit">
                  <div class="box-body">
				  
					<?php for($i=0;$i<count($frontEndViewFields); $i++) { 
						$fieldName ='';
						$fieldTitle = $frontEndViewFields[$i];
						$fieldData = explode(" ",$frontEndViewFields[$i]);
						for($rk=0;$rk<count($fieldData);$rk++){
							if($rk == 0){
								$fieldName .= strtolower($fieldData[$rk]);
							}else{
								$fieldName .= $fieldData[$rk];
							}
						}
					?>
					<div class="form-group ">
					  <label for="exampleInputEmail1"><?php echo $frontEndViewFields[$i]; ?></label>
					  <input type="text" class="form-control" name="<?php echo $fieldName; ?>" value="&lt;?php echo $resultData-><?php echo $frontEditViewFields[$i+1]; ?>; ?>" placeholder="Enter <?php echo $fieldTitle; ?>">
					</div>
					<?php } ?>
				   <input type="hidden" value="&lt;?php echo $resultData-><?php echo $frontEditViewFields[0]; ?>; ?>" name="keyId" />
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
				<input type="submit" class="btn btn-primary" value="Submit" />
				<input type="reset" class="btn btn-default" value="Reset" />
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-2" style="padding-left: 0px;">
              <!-- Horizontal Form -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section>

&lt;?php } ?>
<style>
.error{
    color: red;
    font-weight: normal;
}
</style>
<script>
$("#formdata").validate({
        rules:{  
			<?php $totalCount = count($frontEndViewFields);
			$totalCount = $totalCount -1;
			for($i=0;$i<count($frontEndViewFields); $i++) { 
				$fieldName ='';
				$fieldTitle = $frontEndViewFields[$i];
				$fieldData = explode(" ",$frontEndViewFields[$i]);
				for($rk=0;$rk<count($fieldData);$rk++){
					if($rk == 0){
						$fieldName .= strtolower($fieldData[$rk]);
					}else{
						$fieldName .= $fieldData[$rk];
					}
				}
				if($totalCount == $i){
					$coma='';
				}else{
					$coma = ',';
				}
				echo $fieldName.' : "required"'.$coma.'';
			}
			?>
        },
        messages:{  
			<?php 
			for($i=0;$i<count($frontEndViewFields); $i++) { 
				$fieldName ='';
				$fieldTitle = $frontEndViewFields[$i];
				$fieldData = explode(" ",$frontEndViewFields[$i]);
				for($rk=0;$rk<count($fieldData);$rk++){
					if($rk == 0){
						$fieldName .= strtolower($fieldData[$rk]);
					}else{
						$fieldName .= $fieldData[$rk];
					}
				}
				if($totalCount == $i){
					$coma='';
				}else{
					$coma = ',';
				}
				echo $fieldName.' : "Enter '.$fieldTitle.'"'.$coma.'';
			}
			?>
        }
});
</script>

</textarea>
<br>Helper code -------------------------------------<br>
