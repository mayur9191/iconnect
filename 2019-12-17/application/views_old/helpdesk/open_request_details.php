<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css"  />

<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
<link href="<?php echo base_url() ?>assets/datetime/bootstrap-material-datetimepicker.css"  rel="stylesheet" type="text/css"  />
<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" type"text/css" rel="stylesheet"/>
	 
   <?php //include APPPATH . "views/includs/hedder_code.php" ?>
<style>
body{margin-top:20px;
background:#fff;
}


.inbox .inbox-menu ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .inbox-menu ul li {
    height: 30px;
    padding: 5px 15px;
    position: relative
}

.inbox .inbox-menu ul li:hover,
.inbox .inbox-menu ul li.active {
    background: #e4e5e6
}

.inbox .inbox-menu ul li.title {
    margin: 20px 0 -5px 0;
    text-transform: uppercase;
    font-size: 10px;
    color: #d1d4d7
}

.inbox .inbox-menu ul li.title:hover {
    background: 0 0
}

.inbox .inbox-menu ul li a {
    display: block;
    width: 100%;
    text-decoration: none;
    color: #3d3f42
}

.inbox .inbox-menu ul li a i {
    margin-right: 10px
}

.inbox .inbox-menu ul li a .label {
    position: absolute;
    top: 10px;
    right: 15px;
    display: block;
    min-width: 14px;
    height: 14px;
    padding: 2px
}

.inbox ul.messages-list {
    list-style: none;
    margin: 15px -15px 0 -15px;
    padding: 15px 15px 0 15px;
    border-top: 1px solid #d1d4d7
}

.inbox ul.messages-list li {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    cursor: pointer;
    margin-bottom: 10px;
    padding: 10px
}

.inbox ul.messages-list li a {
    color: #3d3f42
}

.inbox ul.messages-list li a:hover {
    text-decoration: none
}

.inbox ul.messages-list li.unread .header,
.inbox ul.messages-list li.unread .title {
    font-weight: 700
}

.inbox ul.messages-list li:hover {
    background: #e4e5e6;
    border: 1px solid #d1d4d7;
    padding: 9px
}

.inbox ul.messages-list li:hover .action {
    color: #d1d4d7
}

.inbox ul.messages-list li .header {
    margin: 0 0 5px 0
}

.inbox ul.messages-list li .header .from {
    width: 49.9%;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .header .date {
    width: 50%;
    text-align: right;
    float: right
}

.inbox ul.messages-list li .title {
    margin: 0 0 5px 0;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .description {
    font-size: 12px;
    padding-left: 29px
}

.inbox ul.messages-list li .action {
    display: inline-block;
    width: 16px;
    text-align: center;
    margin-right: 10px;
    color: #d1d4d7
}

.inbox ul.messages-list li .action .fa-check-square-o {
    margin: 0 -1px 0 1px
}

.inbox ul.messages-list li .action .fa-square {
    float: left;
    margin-top: -16px;
    margin-left: 4px;
    font-size: 11px;
    color: #fff
}

.inbox ul.messages-list li .action .fa-star.bg {
    float: left;
    margin-top: -16px;
    margin-left: 3px;
    font-size: 12px;
    color: #fff
}

.inbox .message .message-title {
    margin-top: 30px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px
}

.inbox .message .header {
    margin: 20px 0 30px 0;
    padding: 10px 0 10px 0;
    border-top: 1px solid #d1d4d7;
    border-bottom: 1px solid #d1d4d7
}

.inbox .message .header .avatar {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    height: 34px;
    width: 34px;
    float: left;
    margin-right: 10px
}

.inbox .message .header i {
    margin-top: 1px
}

.inbox .message .header .from {
    display: inline-block;
    width: 50%;
    font-size: 12px;
    margin-top: -2px;
    color: #d1d4d7
}

.inbox .message .header .from span {
    display: block;
    font-size: 14px;
    font-weight: 700;
    color: #3d3f42
}

.inbox .message .header .date {
    display: inline-block;
    width: 29%;
    text-align: right;
    float: right;
    font-size: 12px;
    margin-top: 18px
}

.inbox .message .attachments {
    border-top: 3px solid #e4e5e6;
    border-bottom: 3px solid #e4e5e6;
    padding: 10px 0;
    margin-bottom: 20px;
    font-size: 12px
}

.inbox .message .attachments ul {
    list-style: none;
    margin: 0 0 0 -40px
}

.inbox .message .attachments ul li {
    margin: 10px 0
}

.inbox .message .attachments ul li .label {
    padding: 2px 4px
}

.inbox .message .attachments ul li span.quickMenu {
    float: right;
    text-align: right
}

.inbox .message .attachments ul li span.quickMenu .fa {
    padding: 5px 0 5px 25px;
    font-size: 14px;
    margin: -2px 0 0 5px;
    color: #d1d4d7
}

.inbox .contacts ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .contacts ul li {
    height: 30px;
    padding: 5px 15px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis!important;
    position: relative;
    cursor: pointer
}

.inbox .contacts ul li .label {
    display: inline-block;
    width: 6px;
    height: 6px;
    padding: 0;
    margin: 0 5px 2px 0
}

.inbox .contacts ul li:hover {
    background: #e4e5e6
}


.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}


.panel-success>.panel-heading {background-image:linear-gradient(to bottom,#c9da2c 0,#cbdb2b 100%)}
.with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {/* << CH1 >> color:#ffffff*/}
.with-nav-tabs.panel-success .nav-tabs > li > a:hover{background-color:#ffffff; color:#1c6b94;}
.backtab {background-color:#1c6b94; color:#ffffff;}
.backtab:hover {background-color:#1c6b94; color:#ffffff;}

.backtab1 {background-color:#cada2b; color:#1c6b94;}
.backtab1:hover {background-color:#cada2b; color:#1c6b94;}
#accordion .panel .panel-heading{
	padding-top: 10px;
    padding-bottom: 10px
}
.margin-top-10 { margin-top: 1.0em;margin-left: 12px; }
#update_request,#reset_request,#cancel_request,#update_requester,#reset_requester,#cancel_requester{
    display:none;
}
.col-sm-12 .col-sm-3 b {float:right;}

table tr.row0 td, tr.evenrow td, tr.oddrow td, tr.row1 td {
    background-color: #fff;
    color: #000;
    height: 32px;
    border-bottom: 1px solid #E5E5E5;
	padding:6px;
}

/* << CH2 >> */

/* ************************************************* */
.borderless table {
    border-top-style: none;
    border-left-style: none;
    border-right-style: none;
    border-bottom-style: none;
    border-code:#fff;
    font-size:12px;
}
 
.borderless > tbody > tr > td {
    padding-top:5px;
    padding-bottom:5px;
    font-size:12px;
}
 
.borderless > tbody > tr > td + td {
    width:60%;
}
.selectbox-status{
    background-color: #dbdcdb;
    margin: 0px !important;
    margin-top: -15px !important;
    padding-top: 5px !important;
    padding-bottom: 5px !important;
}
 
.worklog > tbody > tr > td {
    padding-top:5px;
    padding-bottom:5px;
}
body{
    font-family:sans-serif;
    font-size:13px;
}
 
.right-status > tbody > tr > td{
    padding:4px; 
}
 
 
/* sub tab css */
 
 
/* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}
 
/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #cada2c; /* 1c6b94 */
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #cada2c; /* 1c6b94 */
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
 
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}
 
/* Below tabs mode */
 
.tabbable-line.tabs-below > .nav-tabs > li {
  border-top: 4px solid transparent;
}
.tabbable-line.tabs-below > .nav-tabs > li > a {
  margin-top: 0;
}
.tabbable-line.tabs-below > .nav-tabs > li:hover {
  border-bottom: 0;
  border-top: 4px solid #fbcdcf;
}
.tabbable-line.tabs-below > .nav-tabs > li.active {
  margin-bottom: -2px;
  border-bottom: 0;
  border-top: 4px solid #f3565d;
}
.tabbable-line.tabs-below > .tab-content {
  margin-top: -10px;
  border-top: 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 15px;
}
 
/* sub tab css end */
	
  	
</style>


</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<?php //$approvalData = getMaterialApproval($passingData->material_master_id); ?>
<br><br><br>
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
<script src="https://select2.github.io/dist/js/select2.full.js"></script>
<div class="container">

	<div class="row">
		<div class="col-md-12">
            <div class="panel with-nav-tabs panel-success">
			<div class="row inbox">
				<div class="col-md-13">
                    <?php //echo "<pre>";print_r($request_details); ?>
                    <?php //echo "<pre>";print_r($request_more_details); ?>
					<?php $dept = get_record_by_id_code('dept',$request_more_details->dept_id);  ?>
					<?php $technician = get_record_by_id_code('technician',$request_more_details->technician_id);  ?>
				     <span class="btn backtab btn-block"><b>Request ID :<?php echo $request_details->user_request_id; ?>	</b></span>
					<div class="panel panel">
					
					 <div class="pull-right">
						<table style="background-color:#cadb2b;color:#1c6b94;border-radius:6px;margin:5px;" class="right-status">
							<tbody>
								<tr>
									<td>Status </td>
									<td>
										<b>:<div id="status_PH" style="display:inline" class="fontBlackBold"><?php echo ucfirst($request_more_details->task_status); ?></div></b>
									</td>
								</tr>
								
								<tr>
									<td>Priority</td>
									<td><b><div id="priority_PH">: <?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></div></b></td>
								</tr>
								
							</tbody>
						</table>
					</div>
					
						
						<div class="panel-body message">
							
							<p>
							<!--<i class="fa fa-edit" style="font-size:24px"></i>-->
							<img src="http://demo.servicedeskplusmsp.com/images/service-request-icon.png" />
							<b>
							  <?php echo ucfirst(strip_tags($request_details->request_subject)); ?>
							</b>
							</p>
							<p>
							<b>By <a onClick="window.open('<?php echo base_url(); ?>helpdesk/requester_details/<?php echo $request_details->user_request_id ; ?>','mywindow','menubar=1,resizable=1,width=450,height=400,location=center');" href="javascript:void(0);"><?php echo  $request_details->requester_name; ?></a>
							</b>
							<span style="font: normal 12px Arial, Helvetica, sans-serif;">on  <?php echo convert_std_time($request_details->request_created_dt);  ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Due Date :</b><span
							style="font: normal 12px Arial, Helvetica, sans-serif;"><?php $duedate = date('M d ,Y h:i:s A',strtotime('+2 hours',strtotime($request_details->request_created_dt))); echo $duedate; ?> </span></p>
						</div>	
				    </div>
				</div><!--/.col-->		
			</div>
                <div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1success" data-toggle="tab" id="tab1">&nbsp;&nbsp;&nbsp;&nbsp;Request&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
						<li><a href="#tab2success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;Resolution&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
						<li><a href="#tab3success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;History&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
					</ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1success">
							<div class="row inbox">
								<div class="col-md-13">
									<div class="panel panel-default">
										<span><b style="padding-left: 12px;padding-top: 6px;">Description</b></span><hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 9px;">
										<div class="panel-body message" style="height:250px;">
												
											       <?php  if($request_details->request_type=='email'){ ?>
											          <iframe style="border-style:none;"  width="100%" src="<?php echo base_url(); ?>helpdesk/request_details_email/<?php echo $request_details->user_request_id; ?>">
												     </iframe>
											      <?php }else{ ?>
											        <div>
												      <p>
													  <?php echo $request_details->request_desc; ?>
													  </p>
                                                       													  
											        </div>
												  <?php } ?>	  
											

											

										</div>	
										<div class="panel-footer">
											  <button onClick="window.open('<?php echo base_url(); ?>helpdesk/emailcomposer/<?php echo $request_details->user_request_id; ?>/REPLY','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-primary btn-sm">Reply</button>
											  <button onClick="window.open('<?php echo base_url(); ?>helpdesk/emailcomposer/<?php echo $request_details->user_request_id; ?>','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-default btn-sm">Forward</button>
											
										</div>
									</div>
								</div><!--/.col-->	
                                <div class="col-md-13">
                                 <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Requester Conversations</span>   | <span style="font: bold 14px Arial, Helvetica, sans-serif!important;"> <a style="color:inherit;" href="<?php echo base_url(); ?>helpdesk/requestdetails/<?php echo $request_details->user_request_id; ?>/viewall">[View All Conversations]</a></span>
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                           
											<?php if(!empty($emails)){
												    foreach($emails as $email){?>
												<!---code for each accordian starts from here---> 
												<div class="panel panel-default">
													<div class="panel-heading">
							                            <h4 class="panel-title">
								                        <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapse<?php echo $email->sytem_emails_id; ?>">
									                    <i class="fa fa-unlock" style="margin-left:1.0em"></i>
									                    <i class="glyphicon glyphicon-envelope" style="margin-left:3.0em"></i>
										                <span style="margin-left:3.0em;font-size:12px;">
											            <b>System</b>
										                </span>
										               <span style="margin-left:0.5em;font-size:12px;"> on <?php echo convert_std_time($email->created_date); ?>
										               </span>
								                       </a>
							                           </h4> 
						                            </div>
						                            <div id="collapse<?php echo $email->sytem_emails_id; ?>" class="panel-collapse collapse">
							                        <div class="panel-body" style="overflow-y:scroll;">
								                        <p>
											            <b>To</b>:<?php echo $email->to_emails; ?></p>
								                        <p><b>Summary</b></p>
								                        <hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 0.1px;">
								                       <p id="autosubj"><?php echo $email->subject ?></p>
								                      <p><b>Description</b></p>
								                     <hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 0.1px;">
								                    <div id="autodesc">
									                <?php  echo $email->description;  ?>
								                    </div>
											        <div class="panel-footer">
												         <button  onClick="window.open('<?php echo base_url(); ?>helpdesk/emailcomposer/<?php echo $request_details->user_request_id; ?>','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-primary btn-sm">Forward Request</button>
											        </div>
							                    </div>
						                    </div>
					                    </div>
				                        <!---code for each accordian ends here--->
			                        <?php }?>
			                    <?php } ?>
                                            
                                            
                                        </div>
        
                                    </div>
                                </div>    
                                <div class="col-md-13">
                                    <div class="row margin-top-10">
                                        <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Request Details</span> <button id="request_edit" name="request_edit"> Edit</button>
                                    </div>
									<hr/>
									<?php if($this->session->flashdata('msg')): ?>
									<div class="alert alert-success alert-dismissable">
                                     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                     <strong>Success!</strong>
									 <?php echo $this->session->flashdata('msg'); ?>
                                    </div>
                                    <?php endif; ?>
									<?php if($this->session->flashdata('error_msg')): ?>
									<div class="alert alert-danger alert-dismissable">
                                     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                     <strong>Error!</strong>
									 <?php echo $this->session->flashdata('error_msg'); ?>
                                    </div>
                                    <?php endif; ?>
                                    <form method="post" action="<?php echo base_url(); ?>helpdesk/saveRequestDetails">
                                
                                    <input type="hidden" name="request_id" id="request_id" value="<?php echo $request_details->user_request_id; ?>" />
									<input type="hidden" name="requester_user_code" id="requester_user_code" value="<?php echo $request_details->user_code; ?>" />
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <b>Status</b>
                                                </div>
                                                <div class="col-sm-3">
                                                  <span><?php echo ucfirst($request_more_details->task_status); ?></span>
                                                    <span style="display:none;">
                                                        <select name="status" id="status" class="form-control">
                                                            <option>Select</option>
															<option <?php if($request_more_details->task_status=='open')  echo 'selected'; ?> value="open">Open</option>
															<option <?php if($request_more_details->task_status=='wip') echo 'selected';  ?> value="wip">WIP</option>
															<option <?php if($request_more_details->task_status=='hold')  echo 'selected'; ?> value="hold">On Hold</option>
                                                            <option <?php if($request_more_details->task_status=='close')  echo 'selected'; ?> value="close">Closed</option>
                                                            <option <?php if($request_more_details->task_status=='resolve')  echo 'selected'; ?> value="resolve">Resolved</option>
                                                            
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->task_status;  ?>" name="prev_status" id="prev_status" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Workstation Number</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo $request_more_details->tech_workstation_no; ?></span>
                                                    <span style="display:none;"><input type="text" class="form-control" id="work_Station" value="<?php echo $request_more_details->tech_workstation_no;  ?>"  name="work_Station"/>
													<input type="hidden" value="<?php echo $request_more_details->tech_workstation_no;  ?>" name="prev_workstation" id="prev_workstation" />
													</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Mode</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo ucfirst($request_details->request_type); ?></span>
                                                    <span style="display:none;">
                                                        <select name="mode" id="mode" class="form-control">
                                                            <option>Select</option>
                                                            <!--<option>E-mail</option>
                                                            <option>Phone Call</option>
                                                            <option>Web Form</option>-->
                                                            <?php foreach($request_mode as $mode): ?>
                                                            <option <?php echo ($request_details->request_type==$mode->request_type)?'selected':''; ?> value="<?php echo $mode->request_type ?>"><?php echo ucfirst($mode->request_type); ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->request_type;  ?>" name="prev_mode" id="prev_mode" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Department</b></div>
                                                <div class="col-sm-3">  
                                                    <span><?php  echo $dept->dept_name; ?></span>
                                                    <span style="display:none;">
                                                    <select name="dept" id="dept" class="form-control">
                                                        <option>Select</option>
                                                        <?php foreach ($depts_hd as  $value) {?>
                                                        <option <?php echo ($value->dept_id == $request_more_details->dept_id)?'selected':''; ?> value="<?php echo $value->dept_id; ?>"><?php echo $value->dept_name; ?></option>
                                                        <?php } ?>
                                                    </select>
													<input type="hidden" value="<?php echo $request_more_details->dept_id;  ?>" name="prev_dept" id="prev_dept" />
                                                    </span>              

                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                               <!-- <div class="col-sm-3"><b>Level</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="level" id="level" class="form-control">
                                                            <option>Select</option>
                                                            <option>Tier1</option>
                                                            <option>Tier2</option>
                                                            <option>Tier3</option>
                                                            <option>Tier4</option>
                                                        </select>
                                                    </span>
                                                </div>-->
												<div class="col-sm-3"><b>Site</b></div>
                                                <div class="col-sm-3">
                                                    <?php //print_r($site_location_hd); ?>
                                                    <span>Pune</span>
                                                    <span style="display:none;">
                                                        <select name="site" id="site" class="form-control">
                                                            <option>Select</option>
                                                            <?php foreach($site_location_hd as $location): ?>
                                                            <option <?php echo ( $request_details->requester_location_id==$location->site_location_id)?'selected':''; ?> value="<?php echo $location->site_location_id; ?>"><?php echo $location->site_location_name;   ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->requester_location_id;  ?>" name="prev_site" id="prev_site" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Priority</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></span>
                                                    <span style="display:none;">
                                                        <select name="priority" id="priority" class="form-control">
                                                            <option>Select</option>
                                                            <option <?php echo ($request_details->request_priority=="High") ?'selected':''; ?>>High</option>
                                                            <option <?php echo ($request_details->request_priority=="Low") ?'selected':''; ?>>Low</option>
                                                            <option <?php echo ($request_details->request_priority=="Low") ?'selected':''; ?>>Medium</option>
                                                            <option <?php echo ($request_details->request_priority=="Normal") ?'selected':''; ?>>Normal</option>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->request_priority;  ?>" name="prev_priority" id="prev_priority" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Group</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span>L1 Desktop Team<?php //echo "<pre>";print_r($groups_hd); ?>
													</span>
													
                                                    <span style="display:none;">
                                                        <select name="group" id="group" class="form-control">
														
                                                            <option>Select</option>
                                                            <?php foreach ($groups_hd as  $value) {?>
                                                            <option <?php echo ($value->group_id == $request_more_details->group_id)?'selected':''; ?> value="<?php echo $value->group_id; ?>"><?php echo $value->group_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->group_id;  ?>" name="prev_group" id="prev_group" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Category</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php $category = get_record_by_id_code('category',$request_details->category_id);echo $category->category_name;  ?>
													</span>
                                                    <span style="display:none;"> 
                                                        <select name="category" id="category" class="form-control">
                                                            <option>Select</option>
                                                            <?php $categories = get_category();?>
                                                            <?php foreach ($categories as  $value) {?>
                                                            <option <?php echo ($request_details->category_id == $value->category_id )?'selected':''; ?> value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->category_id;  ?>" name="prev_category" id="prev_category" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Technician</b></div>
                                                <div class="col-sm-3">
                                                   <?php  $emp_det = getEmpByEmail($technician->technician_email);  ?>
                                                    <span><?php  echo  $technician->tech_name; ?>
													
													<div class="dropdown">
												    
													<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="material-icons" style="margin-top:-25px;float:right;" >search</i>
													</a>
													
													<div class="dropdown-menu" style="width:380px;max-height:325px;padding-left:8px;overflow-x:hidden;overflow-y:auto;">
														<button type="button" class="close pop" aria-label="Close">
															<span style="font-size:18px;" aria-hidden="true">&times;</span>
														</button>
														<table width="100%" height="100%">
			                                                <tbody>
																<tr>
																	<td valign="middle" height="22">
																	Technician details - <?php  echo  $technician->tech_name; ?>
																	<?php //print_r($emp_det); ?>
																	<?php $dept = get_record_by_id_code('dept',$request_details->dept_id);  ?>
																
																	</td>
																</tr>
					                                            <tr>
																    <td height="0" valign="top">
																        <table width="100%" border="0" cellpadding="2" cellspacing="0">
																	        <tbody>
																				<tr>
																					<td width="2%"><i class="material-icons" style="font-size:48px;color:black">person</i></td>
																					<td width="44%">
																							<span><?php  echo  $technician->tech_name; ?></span> 
																							<span class="fontBlack"><?php echo ($emp_det->u_designation!='')?"-". ucfirst($emp_det->u_designation):"";  ?>
																							</span>
																					</td> 										
																				</tr> 
																				<tr>
																					 <td colspan="2" valign="top"></td>
																				</tr>
																		        <tr valign="bottom">
																			       <td colspan="3" valign="top">
																					<div>
																						<table width="100%" id="techdetails">
																							<?php $technican_name = explode(" ",$technician->tech_name); ?>
																							<tbody>
																								<tr><td colspan="2" valign="top"></td></tr>
																								 <tr class="row0">
																									<td width="35%" valign="top">Department Name</td>
																									<td width="65%" valign="top"><?php echo $dept->dept_name; ?></td>
																								 </tr>
																								<tr class="row1">
																									<td width="35%" valign="top">E-Mail</td>
																									<td width="65%" valign="top"><?php echo ucfirst($technician->technician_email);  ?></td>
																								</tr>
																								<tr class="row0">
																									<td width="35%" valign="top">First Name</td>
																									<td width="65%" valign="top"><?php echo $technican_name[0] ; ?></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Job Details</td>
																									<td width="65%" valign="top"><?php $technican_name = explode(" ",$technician->tech_name); ?></td>
																								 </tr>
																								 <tr class="row0">
																									<td width="35%" valign="top">Last Name</td>
																									<td width="65%" valign="top"><?php echo $technican_name[1];  ?></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Middle Name</td>
																									<td width="65%" valign="top"></td>
																								 </tr>
																								 <tr class="row0">
																									<td width="35%" valign="top">Mobile</td>
																									<td width="65%" valign="top"><?php echo ($emp_det->u_mobile!='')?$emp_det->u_mobile:"";  ?></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Phone</td>
																									<td width="65%" valign="top"></td>
																								 </tr>	
																								 <tr class="row0">
																									<td width="35%" valign="top">Description</td>
																									<td width="65%" valign="top"></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Site Name</td>
																									<td width="65%" valign="top"><?php echo ($emp_det->u_streetaddress!='')?$emp_det->u_streetaddress:"";  ?></td>
																								 </tr>
																							</tbody>  
																						</table>  
																					</div>  
																			      </td> 
																		        </tr>
																	        </tbody> 
																        </table>
														            </td> 
					                                            </tr>
				                                            </tbody>
			                                            </table>
												    </div> 
													</div>
													</span>
                                                    <span style="display:none;">
                                                        <select name="technician" id="technician" class="form-control">
                                                            <option>Select</option>
                                                            <?php foreach($technicians_hd as $tech): ?>
                                                            <option <?php echo ($request_more_details->technician_id==$tech->tech_id)?'selected':''; ?> value="<?php echo $tech->tech_id;  ?>"><?php echo $tech->tech_name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->technician_id;  ?>" name="prev_technician" id="prev_technician" />
                                                    </span>
													
                                                </div>
                                                <div class="col-sm-3"><b>Subcategory</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php $subcategory = get_record_by_id_code('subcategory',$request_details->subcategory_id);echo $subcategory->sub_category_name;  ?></span>
                                                    <span style="display:none;">
                                                        <select name="subactegory" id="subactegory" class="form-control">
                                                            <option>Select</option>
                                                            <?php $subcategories = get_sub_category();?>
                                                            <?php foreach ($subcategories as  $value) {?>
                                                            <option <?php echo ($request_details->subcategory_id == $value->sub_category_id )?'selected':''; ?>  value="<?php echo $value->sub_category_id; ?>"><?php echo $value->sub_category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->subcategory_id;  ?>" name="prev_subcategory" id="prev_subcategory" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
											    <div class="col-sm-3"><b>SLA</b></div>
                                                <div class="col-sm-3">- </div>
                                                
                                                <div class="col-sm-3"><b>Item</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span><?php $item = get_record_by_id_code('item',$request_details->item_id);echo $item->item_name;  ?></span>
                                                    <span style="display:none;">
                                                        <?php $items = get_item();  ?>
                                                        <select name="item" id="item" class="form-control">
                                                            <option>Select</option>
                                                            
                                                            <?php foreach($items as $value): ?>
                                                            <option <?php echo ($request_details->item_id== $value->items_id) ? 'selected':''; ?> value="<?php echo $value->items_id; ?>"><?php echo $value->item_name; ?></option>
                                                           <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->item_id;  ?>" name="prev_item" id="prev_item" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <!--<div class="col-sm-3"><b>Service Category</b></div>
                                                <div class="col-sm-3">
                                                     
                                                     <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="service_category" id="service_category" class="form-control">
                                                            <option>-----Choose-----</option>
                                                        </select>
                                                    </span>
                                                </div>-->
												<div class="col-sm-3"><b>Template</b></div>
                                                <div class="col-sm-3">Default Request </div>
                                                <div class="col-sm-3"><b>Created By</b></div>
                                                <div class="col-sm-3"><?php echo $request_details->requester_name; ?></div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <!--<div class="col-sm-3"><b>Asset(s)</b></div>
                                                <div class="col-sm-3">
                                                     
                                                    <span>-</span>
                                                    <span style="display:none;">
                                                        <textarea name="assets" class="form-control" id="assets"></textarea>
                                                    </span>
                                                </div>-->
												
												<div class="col-sm-3"><b>Resolved Date</b></div>
                                                <div class="col-sm-3">- </div>
                                                <div class="col-sm-3"><b>Created Date</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo convert_std_time($request_details->request_created_dt); ?></span>
                                                    <span style="display:none;"><input class="form-control" type="text" readonly value="<?php echo convert_std_time($request_details->request_created_dt); ?>" id="create_date" name="create_date"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Time Elapsed</b></div>
                                                <div class="col-sm-3">
												<?php 
													//$currentTime = strtotime(date('Y-m-d h:i:s'));
													//$systemtTime  = strtotime($request_more_details->request_assign_person_dt);

													//$timeelapsed = $currentTime - $systemtTime;
												   $timeelpase_str = " "; 
		                                           $timeelapsed = get_time_diff(date('Y-m-d H:i:s'),$request_more_details->request_assign_person_dt);
												   if($timeelapsed["days"]!='0'){
													    $timeelpase_str .= $timeelapsed["days"].'days'.' ';   
												   }
												   if($timeelapsed["hurs"]!='0'){
													    $timeelpase_str .= $timeelapsed["hurs"].'hrs'.' ';   
												   }
												   if($timeelapsed["mins"]!='0'){
													    $timeelpase_str .= $timeelapsed["mins"].'min'.' ';   
												   }
												   if($timeelapsed["secd"]!='0'){
													   // $timeelpase_str .= $timeelapsed["secd"].'s';   
												   }
													echo $timeelpase_str;
													

												?>          
												</div>
                                                <div class="col-sm-3"><b>DueBy Date</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo $duedate; ?></span>
                                                    <span  style="display:none;" class="input-group date" ><input class="form-control " type="text" value="<?php echo $duedate;?>" id="dueby_date" name="dueby_date"/>
													<input type="hidden" value="<?php  echo $duedate;  ?>" name="prev_dueby_date" id="prev_dueby_date" />
													</span>
													
													
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Last Update Time</b></div>
                                                <div class="col-sm-3">
												<?php echo ($request_details->response_date!='0000-00-00 00:00:00')? convert_std_time($request_details->last_modified_dt):''; ?>
												</div>
												<div class="col-sm-3"><b>Response DueBy Time</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span><?php echo ($request_details->response_date!='0000-00-00 00:00:00')? convert_std_time($request_details->response_date):''; ?></span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="<?php echo ($request_details->response_date!='0000-00-00 00:00:00')?convert_std_time($request_details->response_date):''; ?>" id="response_dueby" name="response_dueby"/>
                                                    <input type="hidden" value="<?php  echo ($request_details->response_date!='0000-00-00 00:00:00')?convert_std_time($request_details->response_date):'';  ?>" name="prev_res_dueby_date" id="prev_res_dueby_date" />
													</span>
                                                </div>
                                            </div>
                                        </div>
										<div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 pull-right">
												    <button type="submit" id="update_request" name="update_request">Update</button>
                                                    <button type="reset" id="reset_request" onClick="location.reload();" name="reset_request">Reset</button>
                                                    <button type="button" id="cancel_request" name="cancel_request">Cancel</button>
												
												
												</div>
                                                
                                            </div>
                                        </div>
                                            
                                    </form>
                                </div>
                                <div class="col-md-13">
								    <div class="row margin-top-10">
									 <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Requester Details</span> <!--<button id="requester_edit" disabled name="requester_edit"> Edit</button>-->
								    </div>
									<hr/>
                                    <form method="post" action="<?php echo base_url(); ?>helpdesk/saveRequesterDetails" >
									
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Requester Name</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span><?php echo $request_details->requester_name; ?></span>
                                                    <span style="display:none;"><input type="text" class="form-control" value="<?php echo $request_details->requester_name; ?>" id="requester_name" name="requester_name"/> </span>
                                                </div>
                                                <div class="col-sm-3"><b>E-mail Address</b></div>
                                                <div class="col-sm-3">
                                                    <?php $email = get_email_by_emp_code($request_details->user_code); ?>
                                                    <span><?php echo (!empty($email))? $email[0]->u_email:''; ?></span>
                                                    <span style="display:none;"><input type="email" class="form-control" value="<?php echo (!empty($email))? $email[0]->u_email:''; ?>" id="email_address" name="email_address"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                       <!-- <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Contact number</b></div>
                                                <div class="col-sm-3">
                                                     <span>020 66278036</span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="020 66278036" id="contact_no" name="contact_no"/> </span>
                                                </div>
                                                <div class="col-sm-3"><b>Mobile number</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span>8805009266</span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="8805009266" id="mobile_no" name="mobile_no"/> </span>
                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Department</b></div>
                                                <div class="col-sm-3">
                                                    <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="depts" id="depts" class="form-control">
                                                            <option>-----Choose-----</option>
                                                            <option>Skeiron Group</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Business Impact</b></div>
                                                <div class="col-sm-3">
                                                    <span>-</span>
                                                    <span style="display:none;">
                                                        <select name="business_impact" id="business_impact" class="form-control">
                                                            <option>-----Choose-----</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>-->
										<div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 pull-right">
												    <button type="submit" id="update_requester" name="update_requester">Update</button>
                                                    <button type="reset" id="reset_requester" name="reset_requester">Reset</button>
                                                    <button type="button" id="cancel_requester" name="cancel_requester">Cancel</button>
												</div>
                                            </div>
                                        </div>
                                         
                                    </form>
                                </div>	
							</div>
						</div>
						
						
						<div class="tab-pane fade" id="tab2success">
							<div class="row inbox">
								<div class="col-md-12">
								<!-- sub tab -->
								<div class="col-md-12">
									<div class="tabbable-panel">
										<div class="tabbable-line">
											<ul class="nav nav-tabs ">
												<li class="active">
													<a href="#tab_default_1" data-toggle="tab"> Resolution </a>
												</li>
												<li>
													<a href="#tab_default_2" data-toggle="tab"> Solutions </a>
												</li>
												<li>
													<a href="#tab_default_3" data-toggle="tab"> Tried Solutions </a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_default_1">
													<form class="form-horizontal" role="form">
														<div class="form-group">
															<div class="col-sm-12 col-md-12">
																  <textarea class="form-control select2-offscreen" id="editor"></textarea>
															</div>
														</div>
														
														<div class="form-group selectbox-status">
															<label class="col-sm-3 control-label">Update request status to</label>
															<div class="col-sm-3" style="text-align:left;">
																<select name="statuschange" class="form-control select2-offscreen">
																	<option value="">Select Status</option>
																	<option value="open">Open</option>
																	<option value="wip">WIP</option>
																	<option value="on_hold">On Hold</option>
																	<option value="close">Close</option>
																	<option value="resolved">Resolved</option>
																</select>
															</div>
														</div>
														<br><br>
														<button type = "button" class="btn btn-success"> Submit </button>
														<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Open Modal</button>
													</form>
													
													
													&nbsp;&nbsp;<input type="checkbox" name="work_log" id="timeSpentId" /> <label for="timeSpentId">Add Work Log</label><br><br>
													
													<div id="logpage" style="display:none;">
														<div class="form-group selectbox-status"><h5><b>Add Work Log</b></h5></div>
														<table class="worklog">
															<tr>
																<td>Owner</td>
																<td>
																	<select name="owner_detail" class="form-control select2-offscreen" data-live-search="true" style="width:40%;">
																		<option value="1">test 1 </option>
																		<option value="2">test 2 </option>
																		<option value="3">test 3 </option>
																		<option value="4">test 4 </option>
																	</select>
																</td>
																<td>Start Time</td>
																<td>
																	<input type="text" name="start_time" class="form-control datepicker" />
																</td>
															</tr>
															
															<tr>
																<td>Time Taken To Resolve</td>
																<td>
																	<div class="form-group">
																	<span class="col-sm-2">Hours</span>
																		<div class="col-sm-3">
																			<input type="text" name="hrs" class="form-control" />
																		</div>
																	<span class="col-sm-2">Minutes</span>
																		<div class="col-sm-3 col-md-3">
																			<input type="text" name="mints" class="form-control" />
																		</div>
																	</div>
																</td>
																<td>End Time</td>
																<td>
																	<input type="text" name="start_time" class="form-control datepicker" />
																</td>
															</tr>
															
															<tr>
																<td colspan="4"><input type="checkbox" name="" /> Include non operational hours</td>
															</tr>
															
															<tr>
																<td>Other Charge (Rs)</td>
																<td><input type="text" name="rs_charge" class="form-control" /></td>
																<td></td><td></td>
															</tr>
															
															<tr>
																<td>Description</td>
																<td colspan="4"><textarea name="worklog_desc" class="form-control"></textarea></td>
															</tr>
															
														</table>
														<br>
														<button type = "button" class="btn btn-success col-md-offset-4"> Submit </button>
													</div>
													
												</div>
												
												
												<div class="tab-pane" id="tab_default_2">
													<h4><i class="fa fa-info-circle" aria-hidden="true"></i> No solutions tried </h4>
												</div>
												
												
												<div class="tab-pane" id="tab_default_3">
													<h4><i class="fa fa-info-circle" aria-hidden="true"></i> No solutions tried </h4>
												</div>
												
											</div>
										</div>
									</div>
								</div>
									
								<!-- sub tab end -->
								
								
									<?php /*<form class="form-horizontal" role="form">
										<div class="form-group">
											<div class="col-sm-12 col-md-12">
												  <textarea class="form-control select2-offscreen" id="editor"></textarea>
											</div>
										</div>
										
										<div class="form-group selectbox-status">
											<label class="col-sm-3 control-label">Update request status to</label>
											<div class="col-sm-3" style="text-align:left;">
												<select name="statuschange" class="form-control select2-offscreen">
													<option value="">Select Status</option>
													<option value="open">Open</option>
													<option value="wip">WIP</option>
													<option value="on_hold">On Hold</option>
													<option value="closed">Close</option>
													<option value="resolved">Resolved</option>
												</select>
											</div>
										</div>
										<br><br>
										<button type = "button" class="btn btn-success"> Submit </button>
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Open Modal</button>
									</form>*/ ?>
									
								</div><!--/.col-->
								<?php /*
								&nbsp;&nbsp;<input type="checkbox" name="work_log" id="timeSpentId" /> <label for="timeSpentId">Add Work Log</label><br><br>
								
								<div class="col-md-12" id="logpage" style="display:none;">
									<div class="form-group selectbox-status"><h5><b>Add Work Log</b></h5></div>
									<table class="worklog">
										<tr>
											<td>Owner</td>
											<td>
												<select name="owner_detail" class="form-control select2-offscreen" data-live-search="true" style="width:40%;">
													<option value="1">test 1 </option>
													<option value="2">test 2 </option>
													<option value="3">test 3 </option>
													<option value="4">test 4 </option>
												</select>
											</td>
											<td>Start Time</td>
											<td>
												<input type="text" name="start_time" class="form-control datepicker" />
											</td>
										</tr>
										
										<tr>
											<td>Time Taken To Resolve</td>
											<td>
												<div class="form-group">
												<span class="col-sm-2">Hours</span>
													<div class="col-sm-3">
														<input type="text" name="hrs" class="form-control" />
													</div>
												<span class="col-sm-2">Minutes</span>
													<div class="col-sm-3 col-md-3">
														<input type="text" name="mints" class="form-control" />
													</div>
												</div>
											</td>
											<td>End Time</td>
											<td>
												<input type="text" name="start_time" class="form-control datepicker" />
											</td>
										</tr>
										
										<tr>
											<td colspan="4"><input type="checkbox" name="" /> Include non operational hours</td>
										</tr>
										
										<tr>
											<td>Other Charge (Rs)</td>
											<td><input type="text" name="rs_charge" class="form-control" /></td>
											<td></td><td></td>
										</tr>
										
										<tr>
											<td>Description</td>
											<td colspan="4"><textarea name="worklog_desc" class="form-control"></textarea></td>
										</tr>
										
									</table>
									<br>
									<button type = "button" class="btn btn-success col-md-offset-4"> Submit </button>
								</div> */ ?>
							</div>
							
							
							
							<!-- Modal -->
							<!--<div id="myModal" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">

								
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Status change comment</h4>
								  </div>
								  <div class="modal-body">
									<textarea class="form-control select2-offscreen" placeholder="Comment mandatory"></textarea>
								  </div>
								  <div class="modal-footer">
									<button type = "button" class="btn btn-success" > Submit </button>
									<button type="button" data-dismiss="modal" class="btn btn-default closer">Cancel</button>
								  </div>
								</div>
								
							  </div>
							</div>-->
							
							<!-- modal closed -->
						</div>
						<div id="myModal1" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Close Request</h4>
								  </div>
								  <div class="modal-body">
									<table class="borderless">
										<tr>
											<td>First Call Resolution(FCR) : </td>
											<td><input type="checkbox" name="fcr_checkbox" value="" /></td>
										</tr>
										<tr>
											<td>Has requester acknowledge the resolution ? : </td>
											<td><input type="radio" name="requester_acknowledge" value="yes" />&nbsp;Yes &nbsp;&nbsp; <input type="radio" name="requester_acknowledge" value="no" />&nbsp;No&nbsp;</td>
										</tr>
										<tr>
											<td>Comment : </td>
											<td><textarea name="general_comment" class="form-control select2-offscreen" ></textarea></td>
										</tr>
										<tr>
											<td>Request Closure Code : </td>
											<td>
												<!-- Make this dynamic -- *12-5-2017* -->
												<select name="request_closure" class="form-control"> 
													<option value="null">Choose option </option>
													<option value="3">Canceled</option>
													<option value="2">Failed</option>
													<option value="4">Postponed</option>
													<option value="1">Success</option>
													<option value="5">Unable to Reproduce</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Request Closure Comment / Status Change Comment : </td>
											<td><textarea name="status_change_comment" class="form-control select2-offscreen" placeholder="Comment mandatory"> </textarea></td>
										</tr>
									</table>
									
								  </div>
								  <div class="modal-footer">
									<input type="submit" value="Close request" class="btn btn-success" />
									<button type="button" data-dismiss="modal" class="btn btn-default closer">Cancel</button>
								  </div>
								</div>
							  </div>
							</div>
						
						 <!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Status change comment</h4>
								  </div>
								  <div class="modal-body">
								  <div id="message_err"  style="color:red;"></div>
									<textarea id="comment"  name="comment" class="form-control select2-offscreen" placeholder="Comment mandatory"></textarea>
								  </div>
								  <div class="modal-footer">
									<button type = "button" onClick="saveStatus()" class="btn btn-success" > Submit </button>
								  </div>
								</div>
								
							  </div>
							</div>
							
							
                            <!-- modal closed << CH3 >>  -->
                            <div id="myModal1" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Close Request</h4>
                                  </div>
                                  <div class="modal-body">
                                    <table class="borderless">
                                        <tr>
                                            <td>First Call Resolution(FCR) : </td>
                                            <td><input type="checkbox" name="fcr_checkbox" value="" /></td>
                                        </tr>
                                        <tr>
                                            <td>Has requester acknowledge the resolution ? : </td>
                                            <td><input type="radio" name="requester_acknowledge" value="yes" />&nbsp;Yes &nbsp;&nbsp; <input type="radio" name="requester_acknowledge" value="no" />&nbsp;No&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Comment : </td>
                                            <td><textarea name="general_comment" class="form-control select2-offscreen" ></textarea></td>
                                        </tr>
                                        <tr>
                                            <td>Request Closure Code : </td>
                                            <td>
                                                <!-- Make this dynamic -- *12-5-2017* -->
                                                <select name="request_closure" class="form-control"> 
                                                    <option value="null">Choose option </option>
                                                    <option value="3">Canceled</option>
                                                    <option value="2">Failed</option>
                                                    <option value="4">Postponed</option>
                                                    <option value="1">Success</option>
                                                    <option value="5">Unable to Reproduce</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Request Closure Comment / Status Change Comment : </td>
                                            <td><textarea name="status_change_comment" class="form-control select2-offscreen" placeholder="Comment mandatory"> </textarea></td>
                                        </tr>
                                    </table>
                                    
                                  </div>
                                  <div class="modal-footer">
                                    <input type="submit" value="Close request" class="btn btn-success" />
                                    <button type="button" data-dismiss="modal" class="btn btn-default closer">Cancel</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
							
						
						 <div class="tab-pane fade" id="tab3success">
							<div class="row inbox">
								<div class="col-md-13">
									<div class="panel panel-default">
										<span class="btn backtab btn-block"><b>Request History</b></span><br>
										<div class="panel-body message">
										    <?php //echo "<pre>";print_r($history); ?>
											<?php echo display_history($request_details->user_request_id); ?>
											<!--<p  class="backtab1">Updated by Shaijad Kureshi on Apr 3, 2017 07:43 PM</p>
											<p>Request Updated by Shaijad Kureshi</p>
											<p>Group changed from L1 Desktop Team to None</p>
											<p>Technician changed from Rahul Survase to Shaijad Kureshi</p>
											<p>Time of technician assignment changed from Apr 3, 2017 07:42 PM to Apr 3, 2017 07:43 PM</p>-->
										</div>	
									</div>
								</div><!--/.col-->	

							</div>
						</div>
						
                    </div>
                </div>
            </div>
        </div>
	</div>


</div>

	<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>-->
	  
 <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>-->
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>

 <!--<script src="<?php echo base_url(); ?>assets/datetime/bootstrap-material-datetimepicker.js"></script>-->
 
<script>
     
	var trVal = 1;
	var statusValPrevious = 'open';
	var request_id;
	var dueby_date;
	var groupId;
	var technicianId;
	function saveStatus()
	 {
		 
		 var comment = $("#comment").val();
		 var status = $("#status").val();
		 var user_code = $("#requester_user_code").val();
		 var prev_status = $("#prev_status").val();
		 if(comment == ''){
			 $("#myModal").find("#message_err").html("Please enter Your Comment...");
			 return false;
		 }
		 $.ajax({
			 url: "<?php echo base_url() ?>helpdesk/statusChange",
			 type:"post",
			 dataType:"text",
			 data:{"status":status,"request_id":request_id,"group":groupId,"technician":technicianId,"comment":comment,"requester_user_code":user_code,"prev_status":prev_status},
			  success:function(response)
			  {
				$('#myModal').modal('hide');	   
				  
			  },
			  error:function()
			  {
				  
			  }
		 })
	 }
	$(document).ready(function(){
		
		$("#timeSpentId").on('click',function(){
			if($(this).prop('checked')){
				$("#logpage").show();
			}else{
				$("#logpage").hide();
			}
		});
		
		
		
		$("#message_err").html("");
		
		$('#myModal').on('hidden.bs.modal', function () {
			$("#message_err").html("");
       });
		request_id    = $("#request_id").val();
		dueby_date    = $("#dueby_date").val();
		groupId       = $("#group").val();
        technicianId  =	$("#technician").val(); 	
		
		var create_date = $("#create_date").val();
		var dueby_date = $("#dueby_date").val();
		
		$('#dueby_date').datetimepicker({ 
		format: 'MMM D,YYYY hh:mm:ss A',
		useCurrent:true,
		minDate: create_date });
			
		if($('#response_dueby').val()!=''){
			$('#response_dueby').datetimepicker({ 
			format: 'MMM D,YYYY hh:mm:ss A',
			useCurrent:true,
			minDate: create_date,
			maxDate: dueby_date,
			}); 
		}
		$("#dueby_date").on("dp.change", function (e) {
			
        $('#response_dueby').data("DateTimePicker").maxDate(e.date);
       });   
    	
		//$('#dueby_date').bootstrapMaterialDatePicker({ format : 'MMMM DD, YYYY - HH:mm:ss' });
		
		 
		$("select[name='statuschange'],select[name='status']").change(function(){
			statusVal = $(this).val();
			/*if(statusVal == 'open' || statusVal == ''){
			}else{
				$("#myModal").modal('show');
			}*/
			if(statusVal == 'open' || statusVal == ''){
            }else if (statusVal == 'close') { 
			
                $("#myModal1").modal('show');
            }else{
				
                $("#myModal").modal('show');
            }
		});
		
        $(".closer").on("click",function(){
            console.log('val get '+statusVal);
            var confirmStatus = confirm("Status comment is mandatory, cancelling this operation will set previous request status.");
            if(confirmStatus && statusVal !='open'){
                $("select[name='statuschange']").val(statusValPrevious);
                $("#myModal1").modal('hide');
            }
            return confirmStatus;
        });
		
		
		$("#tab1").click(function(){
			$(this).attr('aria-expanded',true);
			$(this).find('li').addClass('active');
            //alert("hello");
			$("#tab4success").addClass('active');
            //$("#tab5success").addClass('active in');

			$("#tab2").attr('aria-expanded',false);
			$("#tab2").find('li').removeClass('active');
		});
		
		$("#tab21").click(function(){
			$("#tab1").attr('aria-expanded',false);
			$("#tab1").parent('li').removeClass('active');
			
			$("#tab2").attr('aria-expanded',true);
			$("#tab2").parent('li').addClass('active');
			
			$("#tab2success").addClass('active in');
			$("#tab1success").removeClass('active in');
		});
		
		
		
		
		/*var myTable = 
		$('#dynamic-table').DataTable( {
			bAutoWidth: true,
			"aoColumns": [null, null,null,null, null,null,null, null,null,null],
			"aaSorting": [],
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
			//"bProcessing": true,
			//"bServerSide": true,
			//"sAjaxSource": "http://127.0.0.1/table.php"	,

			//,
			//"sScrollY": "200px",
			//"bPaginate": false,

			//"sScrollX": "100%",
			//"sScrollXInner": "120%",
			"bScrollCollapse": true,
			//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
			//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
			//"iDisplayLength": 50
			select: {
				style: 'multi'
			}
		});*/
        /****request edit button click code starts here****/
        $("#request_edit").on("click",function(){
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().prev().css("display","none");
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().css("display","block"); 
           
           // button set visible here 
           $("#update_request,#reset_request,#cancel_request").css("display","inline-block");

        });
		/****requester edit button click code starts here****/
        $("#requester_edit").on("click",function(){

            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().prev().css("display","none");
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().css("display","block"); 
         // button  set visible here
         $("#update_requester,#reset_requester,#cancel_requester").css("display","inline-block");

        });
        
        /***cancel request*******/
        $("#cancel_request").on("click",function(){
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().prev().css("display","block");
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().css("display","none"); 
            // button  set hidden here
         $("#update_request,#reset_request,#cancel_request").css("display","none");
        });
        /***cancel requester*******/
        $("#cancel_requester").on("click",function(){
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().prev().css("display","block");
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().css("display","none"); 
            // button  set hidden here
         $("#update_requester,#reset_requester,#cancel_requester").css("display","none");
        });
		/*****group change***********/
		$("#group").on("change",function(){
            var group_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/get_technician",
				type: "post",
				dataType:"json",
				data:{group_id:group_id },
				success:function(response){
					
					var str_technician = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_technician += "<option>"+response[i].tech_name+"</option>";
					}
					$("#technician").html(str_technician);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
		
		/*******department change**********************/
		$("#dept").on("change",function(){
            var dept_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/getCategoryByDeptId", <!--change the name of controller action-->
				type: "post",
				dataType:"json",
				data:{dept_id:dept_id },
				success:function(response){
					
					var str_categories = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_categories += "<option value='"+response[i].category_id +"'>"+response[i].category_name+"</option>";
					}
					$("#category").html(str_categories);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
		/*******category change**********************/
		$("#category").on("change",function(){
            var category_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/getSubCategoryByCategoryId", <!--change the name of controller action-->
				type: "post",
				dataType:"json",
				data:{category_id:category_id },
				success:function(response){
					
					var str_subcategories = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_subcategories += "<option value='"+response[i].sub_category_id +"'>"+response[i].sub_category_name+"</option>";
					}
					$("#subactegory").html(str_subcategories);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
		
		/*******sub category change**********************/
		$("#subactegory").on("change",function(){
            var sub_category_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/getItemBySubcategoryId", <!--change the name of controller action-->
				type: "post",
				dataType:"json",
				data:{sub_category_id:sub_category_id },
				success:function(response){
					
					var str_items = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_items += "<option value='"+response[i].items_id +"'>"+response[i].item_name+"</option>";
					}
					$("#item").html(str_items);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        }); 
		/********panel settings on click and on hover*******************/
		$(".panel-group > .panel.panel-default").on('mouseover',function(){
		   $(this).removeClass('panel-default').css('background-color',' rgba(28, 107, 148, 0.22)');
		   
		});
        $(".panel-group > .panel.panel-default").on('mouseout',function(){
          $(this).addClass('panel-default').css('background-color','');
        });
		
		$(".panel-group > .panel.panel-default").on('click',function(){
			
		   $(this).find('.panel-heading').css('background-color',' rgba(28, 107, 148, 0.22)');
		   $(this).find('.panel-heading').next().css('background-color','#fff');
		}); 
		
	});
	
	CKEDITOR.replace( 'editor', {
		plugins: 'wysiwygarea,basicstyles,toolbar,undo',
		on: {
			instanceReady: function() {
				// Show textarea for dev purposes.
				//this.element.show();
			},
			change: function() {
				// Sync textarea.
				this.updateElement();    
				// Fire keyup on <textarea> here?
			}
		}
	});
	
	
	
</script>
</body>
</html>


