<html>
     <head>
	     <title>Requester details</title>
		 <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
    body,table tr td{
	background-color:#fff;
	font: normal 12px Arial, Helvetica, sans-serif;
    }
	table tr.row0 td, tr.evenrow td, tr.oddrow td, tr.row1 td {
    background-color: #fff;
    color: #000;
    height: 32px;
    border-bottom: 1px solid #E5E5E5;
	padding:6px;
    }
</style>
	  </head>
	  <body>
	        <table width="100%" height="100%">
			     <tbody>
				    <tr>
					    <td valign="middle" height="22">
						Requester details - <?php echo ucfirst($emp_details->u_name);  ?>
	                    <?php //print_r($emp_details); ?>
						<?php $dept = get_record_by_id_code('dept',$request_details->dept_id);  ?>
					
						</td>
					</tr>
					<tr>
					    <td height="0" valign="top">
						    <table width="100%" border="0" cellpadding="2" cellspacing="0">
							    <tbody>
								    <tr>
									    <td width="2%"><i class="material-icons" style="font-size:48px;color:black">person</i></td>
                                        <td width="44%">
										        <span><?php echo ucfirst($emp_details->u_name);  ?></span> 
												<span class="fontBlack"><?php echo ucfirst($emp_details->u_designation);  ?>
                        </span>
										</td> 										
									</tr> 
									<tr>
									     <td colspan="2" valign="top"></td>
									</tr>
									<tr valign="bottom">
									    <td colspan="3" valign="top">
										    <div>
											    <table width="100%">
												<?php $name = explode(' ',$emp_details->u_name);  ?>
												    <tbody>
													    <tr><td colspan="2" valign="top"></td></tr>
													     <tr "row0">
														    <td width="25%" valign="top">Department Name</td>
															<td width="75%" valign="top"><?php echo $dept->dept_name; ?></td>
														 </tr>
                                                        <tr "row1">
														    <td width="25%" valign="top">E-Mail</td>
															<td width="75%" valign="top"><?php echo ucfirst($emp_details->u_email);  ?></td>
														</tr>
														<tr "row0">
														    <td width="25%" valign="top">First Name</td>
															<td width="75%" valign="top"><?php echo (!empty($name))? ucfirst($name[0]):''; ?></td>
														 </tr>
														 <tr "row1">
														    <td width="25%" valign="top">Job Details</td>
															<td width="75%" valign="top"><?php echo ucfirst($emp_details->u_designation);  ?></td>
														 </tr>
														 <tr "row0">
														    <td width="25%" valign="top">Last Name</td>
															<td width="75%" valign="top"><?php echo (!empty($name))? ucfirst($name[1]):''; ?></td>
														 </tr>
														 <tr "row1">
														    <td width="25%" valign="top">Middle Name</td>
															<td width="75%" valign="top"></td>
														 </tr>
														 <tr "row0">
														    <td width="25%" valign="top">Mobile</td>
															<td width="75%" valign="top"><?php echo $emp_details->u_mobile;  ?></td>
														 </tr>
														 <tr "row1">
														    <td width="25%" valign="top">Phone</td>
															<td width="75%" valign="top"></td>
														 </tr>	
														 <tr "row0">
														    <td width="25%" valign="top">Description</td>
															<td width="75%" valign="top"></td>
														 </tr>
														 <tr "row1">
														    <td width="25%" valign="top">Site Name</td>
															<td width="75%" valign="top"><?php echo  ucfirst($request_details->requester_location_name);  ?></td>
														 </tr>
													</tbody>  
												</table>  
											</div>  
										</td> 
									</tr>
								</tbody> 
							</table>
						</td> 
					</tr>
				 </tbody>
			</table>
	  </body>
</html>