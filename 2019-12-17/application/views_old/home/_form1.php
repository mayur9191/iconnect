<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
  
<style>
@import "font-awesome.min.css";
@import "font-awesome-ie7.min.css";
/* Space out content a bit */
body {
  padding-top: 20px;
  padding-bottom: 20px;
}

/* Everything but the jumbotron gets side spacing for mobile first views */
.header,
.marketing,
.footer {
  padding-right: 15px;
  padding-left: 15px;
}

/* Custom page header */
.header {
  border-bottom: 1px solid #e5e5e5;
}
/* Make the masthead heading the same height as the navigation */
.header h3 {
  padding-bottom: 19px;
  margin-top: 0;
  margin-bottom: 0;
  line-height: 40px;
}

/* Custom page footer */
.footer {
  padding-top: 19px;
  color: #777;
  border-top: 1px solid #e5e5e5;
}

/* Customize container */
@media (min-width: 768px) {
  .container {
    max-width: 850px;
  }
}
.container-narrow > hr {
  margin: 30px 0;
}

/* Main marketing message and sign up button */
.jumbotron {
  text-align: center;
  border-bottom: 1px solid #e5e5e5;
}
.jumbotron .btn {
  padding: 14px 24px;
  font-size: 21px;
}

/* Supporting marketing content */
.marketing {
  margin: 40px 0;
}
.marketing p + h4 {
  margin-top: 28px;
}

/* Responsive: Portrait tablets and up */
@media screen and (min-width: 768px) {
  /* Remove the padding we set earlier */
  .header,
  .marketing,
  .footer {
    padding-right: 0;
    padding-left: 0;
  }
  /* Space out the masthead */
  .header {
    margin-bottom: 30px;
  }
  /* Remove the bottom border on the jumbotron for visual effect */
  .jumbotron {
    border-bottom: 0;
  }
}

input {
  outline: 0;
  border-width: 0 0 2px 0;
  border-color: black;
}
input:focus {
  border-color: green
}

.alg-center{
	text-align:center;
}
</style>
 <?php include APPPATH . "views/includs/hedder_code.php" ?>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<form action="<?php echo base_url().'home/submitGateway'; ?>" method="POST">
<br><br><br>
 <div class="container" style="border: 2px groove;">
	
	<div class="row pull-right">
		<table class="table">
			<tbody>
			  <tr>
				<td>No. : </td>
				<td><input type="text" class="form-control" name="top_number" /></td>
				<td>Date : </td>
				<td><input type="text"  class="form-control datepicker" name="top_date" ></td>
			  </tr>
			</tbody>
		</table>
	</div>
    
   <br><br><br>
   <center><h3>MATERIAL GATE PASS </h3></center>
   <br>
   
	<div class="col-lg-12">
	<div class="row">
		
			<div class="col-lg-12">
				<div class="row">
					<div class="col-sm-6 form-group">
						<label>Skeiron Green Power PRIVATE LIMITED <input type="checkbox" name="company_check1" /></label>
					</div>
					<div class="col-sm-6 form-group">
						<label>Aspen INFRASTRUCTURES PRIVATE LIMITED <input type="checkbox" name="company_check2" /></label>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-2 form-group">
						<label>Location :  </label>
					</div>	
                    <div class="col-sm-4 form-group">
						<input type="text"  class="form-control" name="location" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 form-group">
						<label><!--The following material may please be allowed to go out of our office on date  21/02/017  at  2.00 pm.--></label>
							
					</div>
				</div>	
              <div class="row">
                
                  <table class="table" border="">
                    <thead>
                      <tr>
                        <th class="alg-center">S. No.</th>
                        <th class="alg-center">Item</th>
                        <th class="alg-center">Description</th>
                        <th class="alg-center">No</th>
                        <th class="alg-center">Unit</th>
                        <th class="alg-center">Returnable / Non-returnable</th>
                        <th class="alg-center">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="adrow">
                      <tr>
                        <td><input type="text" placeholder="S.No." name="sl_no[]" class="form-control" value="1" /></td>
                        <td><input type="text" placeholder="Item" name="item[]" class="form-control" /></td>
                        <td><input type="text" placeholder="Description" name="description[]" class="form-control" /></td>
                        <td><input type="text" placeholder="No" name="product_nob[]" class="form-control" /></td>
                        <td><input type="text" placeholder="Unit" name="product_unit[]" class="form-control" /></td>
                        <td><input type="text" placeholder="Returnable / Non-returnable" name="returnable_status[]" class="form-control" /></td>
                        <td><input type="text" placeholder="Remark" class="form-control" name="remark[]" /><button type="button" class="btn btn-sm btn-info" onclick="addRow()">+</button></td>
                      </tr>
					  
                    </tbody>
                </table>
              </div>
              
              <div class="row">
                
                <div class="col-sm-4 form-group">
                  <label> Sender Name & Contact No. : </label>
                </div>

                <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="sender_name_contact" />
                </div>
                
              </div>
              
              <div class="row">
                
                <div class="col-sm-4 form-group">
                  <label> Source Address : </label>
                </div>
                
                <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="source_address" />
                </div>
                
              </div>
              
              <div class="row">
                
                <div class="col-sm-4 form-group">
                  <label>Recipient Name & Contact No. : </label>
                </div>	
                
                <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="recipient_name_contact" />
                </div>
                
              </div>
              
               <div class="row">
                 
                <div class="col-sm-4 form-group">
                  <label> Destination Address : </label>
                </div>	
                 
                 <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="destination_address" />
                </div>
                 
              </div>
              
              <div class="row">
                 
                <div class="col-sm-4 form-group">
                  <label>Purpose : </label>
                </div>	
                 
                 <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="purpose" />
                </div>
                 
              </div>
              
              <div class="row">
                
                  <table class="table" border="">
                    <thead>
                      <tr>
                        <th></th>
                        <th class="alg-center">Prepared by</th>
                        <th class="alg-center">Checked by</th>
                        <th class="alg-center">Approved by</th>
                        <th class="alg-center">Approved by</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Department</td>
                        <td><input type="text" placeholder="" class="form-control approval0" name="department[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval1" name="department[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval2" name="department[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval3" name="department[]" /></td>
                      </tr>
                      <tr>
                        <td>Name</td>
                        <td><input type="text" placeholder="" class="form-control approval0" name="person_name[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval1" name="person_name[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval2" name="person_name[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval3" name="person_name[]" /></td>
                      </tr>
                       <tr>
                        <td>Designation</td>
                        <td><input type="text" placeholder="" class="form-control approval0" name="designation[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval1" name="designation[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval2" name="designation[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval3" name="designation[]" /></td>
                      </tr>
                      <tr>
                        <td>Signature</td>
                        <td><input type="text" placeholder="" class="form-control approval0" name="signature_upload[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval1" name="signature_upload[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval2" name="signature_upload[]" /></td>
                        <td><input type="text" placeholder="" class="form-control approval3" name="signature_upload[]" /></td>
                      </tr>
                      <tr>
                        <td>Date</td>
                        <td><input type="text" placeholder="" class="form-control datepicker approval0" name="approved_date[]" /></td>
                        <td><input type="text" placeholder="" class="form-control datepicker approval1" name="approved_date[]" /></td>
                        <td><input type="text" placeholder="" class="form-control datepicker approval2" name="approved_date[]" /></td>
                        <td><input type="text" placeholder="" class="form-control datepicker approval3" name="approved_date[]" /></td>
                      </tr>
                    </tbody>
                </table>
              </div>
              
			<div class="row">
              <div class="col-sm-12 form-group">
                <label>Remarks of Security : </label>
              </div>
            </div>
              
               <div class="row">
                <div class="col-sm-4 form-group">
                  <label>1. Material went out on date: </label>
                </div>
                <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="material_went_out_date" />
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>2. by which vehicle :</label>
                </div>
                <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="material_went_out_vehicle" />
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>3. Name of Recipient :</label>
                </div>	
                <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="name_of_recipient" />
                </div>
              </div>
              
               <div class="row">
                <div class="col-sm-4 form-group">
                  <label>4.Contact Number :</label>
                </div>
                 <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="contact_no" />
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>5. Signature :</label>
                </div>	
                 <div class="col-sm-8 form-group">
                  <input type="text"  class="form-control" name="security_signature" />
                </div>
              </div>
			  
				<input type="submit" value="Submit" class="btn btn-md btn-info" style="margin-left:40%;" />
				<a href="<?php echo base_url().'home/report'; ?>" class="btn btn-md btn-primary ">Cancel</a>
				
				<br><br>
			</div>
		
		</div>
	</div>
	</div>
	</form> 
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
<script>
	var trVal = 1;
	$(document).ready(function(){
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
		
		//disable field as per user role
		$(".approval<?php echo $user0; ?>").datepicker('destroy');
		$(".approval<?php echo $user0; ?>").attr('readonly','readonly');
		
		$(".approval<?php echo $user1; ?>").datepicker('destroy');
		$(".approval<?php echo $user1; ?>").attr('readonly','readonly');
		
		$(".approval<?php echo $user2; ?>").datepicker('destroy');
		$(".approval<?php echo $user2; ?>").attr('readonly','readonly');
		
		$(".approval<?php echo $user3; ?>").datepicker('destroy');
		$(".approval<?php echo $user3; ?>").attr('readonly','readonly');
	});
	
	function addRow(){
		var rowHtm = '<tr id="tr_'+trVal+'"><td><input type="text" placeholder="S.No." name="sl_no[]" class="form-control" value="1" /></td><td><input type="text" placeholder="Item" name="item[]" class="form-control" /></td><td><input type="text" placeholder="Description" name="description[]" class="form-control" /></td><td><input type="text" placeholder="No" name="product_nob[]" class="form-control" /></td><td><input type="text" placeholder="Unit" name="product_unit[]" class="form-control" /></td><td><input type="text" placeholder="Returnable / Non-returnable" name="returnable_status[]" class="form-control" /></td><td><input type="text" placeholder="Remark" class="form-control" name="remark[]" /><button type="button" class="btn btn-sm btn-info" onclick="removeRow('+trVal+')">-</button></td></tr>';
		$("#adrow").append(rowHtm);
		trVal++;
		console.log($("#adrow").find('> tr').length);	
		var lengthSn = $("#adrow").find('> tr').length;
		var j = 1;
		for(var i=0;i<=lengthSn;i++){
			$("#adrow").find('> tr').eq(i).find('> td').find('> input').eq(0).val(j);
			j++;
		}
	}
	
	function removeRow(val){
		console.log('the remove val is '+val);
		$("#tr_"+val).remove();
		var lengthSn = $("#adrow").find('> tr').length;
		var j = 1;
		for(var i=0;i<=lengthSn;i++){
			$("#adrow").find('> tr').eq(i).find('> td').find('> input').eq(0).val(j);
			j++;
		}
	}
</script>
</body>
</html>


