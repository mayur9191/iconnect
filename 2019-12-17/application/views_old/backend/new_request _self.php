<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />

<style>
body{margin-top:0px;
background:#fff;
}


.inbox .inbox-menu ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .inbox-menu ul li {
    height: 30px;
    padding: 5px 15px;
    position: relative
}

.inbox .inbox-menu ul li:hover,
.inbox .inbox-menu ul li.active {
    background: #e4e5e6
}

.inbox .inbox-menu ul li.title {
    margin: 20px 0 -5px 0;
    text-transform: uppercase;
    font-size: 10px;
    color: #d1d4d7
}

.inbox .inbox-menu ul li.title:hover {
    background: 0 0
}

.inbox .inbox-menu ul li a {
    display: block;
    width: 100%;
    text-decoration: none;
    color: #3d3f42
}

.inbox .inbox-menu ul li a i {
    margin-right: 10px
}

.inbox .inbox-menu ul li a .label {
    position: absolute;
    top: 10px;
    right: 15px;
    display: block;
    min-width: 14px;
    height: 14px;
    padding: 2px
}

.inbox ul.messages-list {
    list-style: none;
    margin: 15px -15px 0 -15px;
    padding: 15px 15px 0 15px;
    border-top: 1px solid #d1d4d7
}

.inbox ul.messages-list li {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    cursor: pointer;
    margin-bottom: 10px;
    padding: 10px
}

.inbox ul.messages-list li a {
    color: #3d3f42
}

.inbox ul.messages-list li a:hover {
    text-decoration: none
}

.inbox ul.messages-list li.unread .header,
.inbox ul.messages-list li.unread .title {
    font-weight: 700
}

.inbox ul.messages-list li:hover {
    background: #e4e5e6;
    border: 1px solid #d1d4d7;
    padding: 9px
}

.inbox ul.messages-list li:hover .action {
    color: #d1d4d7
}

.inbox ul.messages-list li .header {
    margin: 0 0 5px 0
}

.inbox ul.messages-list li .header .from {
    width: 49.9%;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .header .date {
    width: 50%;
    text-align: right;
    float: right
}

.inbox ul.messages-list li .title {
    margin: 0 0 5px 0;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .description {
    font-size: 12px;
    padding-left: 29px
}

.inbox ul.messages-list li .action {
    display: inline-block;
    width: 16px;
    text-align: center;
    margin-right: 10px;
    color: #d1d4d7
}

.inbox ul.messages-list li .action .fa-check-square-o {
    margin: 0 -1px 0 1px
}

.inbox ul.messages-list li .action .fa-square {
    float: left;
    margin-top: -16px;
    margin-left: 4px;
    font-size: 11px;
    color: #fff
}

.inbox ul.messages-list li .action .fa-star.bg {
    float: left;
    margin-top: -16px;
    margin-left: 3px;
    font-size: 12px;
    color: #fff
}

.inbox .message .message-title {
    margin-top: 30px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px
}

.inbox .message .header {
    margin: 20px 0 30px 0;
    padding: 10px 0 10px 0;
    border-top: 1px solid #d1d4d7;
    border-bottom: 1px solid #d1d4d7
}

.inbox .message .header .avatar {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    height: 34px;
    width: 34px;
    float: left;
    margin-right: 10px
}

.inbox .message .header i {
    margin-top: 1px
}

.inbox .message .header .from {
    display: inline-block;
    width: 50%;
    font-size: 12px;
    margin-top: -2px;
    color: #d1d4d7
}

.inbox .message .header .from span {
    display: block;
    font-size: 14px;
    font-weight: 700;
    color: #3d3f42
}

.inbox .message .header .date {
    display: inline-block;
    width: 29%;
    text-align: right;
    float: right;
    font-size: 12px;
    margin-top: 18px
}

.inbox .message .attachments {
    border-top: 3px solid #e4e5e6;
    border-bottom: 3px solid #e4e5e6;
    padding: 10px 0;
    margin-bottom: 20px;
    font-size: 12px
}

.inbox .message .attachments ul {
    list-style: none;
    margin: 0 0 0 -40px
}

.inbox .message .attachments ul li {
    margin: 10px 0
}

.inbox .message .attachments ul li .label {
    padding: 2px 4px
}

.inbox .message .attachments ul li span.quickMenu {
    float: right;
    text-align: right
}

.inbox .message .attachments ul li span.quickMenu .fa {
    padding: 5px 0 5px 25px;
    font-size: 14px;
    margin: -2px 0 0 5px;
    color: #d1d4d7
}

.inbox .contacts ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .contacts ul li {
    height: 30px;
    padding: 5px 15px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis!important;
    position: relative;
    cursor: pointer
}

.inbox .contacts ul li .label {
    display: inline-block;
    width: 6px;
    height: 6px;
    padding: 0;
    margin: 0 5px 2px 0
}

.inbox .contacts ul li:hover {
    background: #e4e5e6
}


.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}


.panel-success>.panel-heading {background-image:linear-gradient(to bottom,#c9da2c 0,#cbdb2b 100%)}
.with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {color:#ffffff}
.with-nav-tabs.panel-success .nav-tabs > li > a:hover{background-color:#ffffff; color:#1c6b94;}
.backtab {background-color:#1c6b94; color:#ffffff;}
.backtab:hover {background-color:#1c6b94; color:#ffffff;}
.mand{color:red;}
.dropdown-thin {
    height: 25px;
    padding-top: 2px;
}

.dep-btn {
	background-color: #c9da2b;
    color: #3c763d;
    float: left;
    margin-bottom: 10px;
    margin-top: 10px;
    margin-right: 10px;
}
.tooltiptext {
    display: none;
    background-color: #cada2c;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 1s;
	padding-right:10px;
}
.error {
    color: red;
    font-size: 12px;
}
.fileupload:hover .tooltiptext{
	 display: block;
     opacity: 1;
}
.tooltiptext ul li{
	text-align:left;
}
#btnSubmit{
	margin-top: -22px;
    position: relative;
    /*margin-left:240px;*/
	margin-left:50%;
}
.tooltiptext ul:first-child  li:before { content:"\2713\0020"; }  
.tooltiptext ul:last-child   li:before { content:"\2611\0020"; }
.tooltiptext ul { list-style-type: none; }

/* Spinner css start */

/* Spinner css end */
/* NEW CSS*/
.panel.panel-default{
	padding-bottom:20px;
}
</style>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
</head>
<body>
<br>

<div class="tab-content">
   <div class="tab-pane fade in active" id="tab2success">
		<div class="row inbox">
			<div class="col-md-3">
				
				
			</div><!--/.col-->
			
			<?php if($this->session->flashdata('msg')): ?>
			<div class="alert alert-success alert-dismissable">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?> 
			</div>
			<?php endif; ?>
			<?php if($this->session->flashdata('error_msg')): ?>
			<div class="alert alert-danger alert-dismissable">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php if(is_array($this->session->flashdata('error_msg'))){ ?>
				<?php    foreach($this->session->flashdata('error_msg') as $error): ?>
				
				<strong>Danger!</strong> <?php echo $error; ?><br/>
				<?php    endforeach;    ?>
				<?php }else{ ?>
				
				<strong>Danger!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
				<?php } ?>
				
			  </div>
			<?php endif; ?>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body message">
						<h3 class="text-center">Self Service Request Form </h3>
						<span  class="mand pull-right">All fields marked are Mandatory (*)</span>	
						
						
						<div id="errMsg"></div>
						<form action="<?php echo base_url() ?>helpdesk/save_request_self" class="form-horizontal"  id="service_request_form" enctype="multipart/form-data" method="post">
						<input type="hidden"value="<?php echo $dept_id; ?>" name="dept_id" id="dept_id" />
							<div class="form-group hide">
								<label for="to" class="col-sm-3 control-label">New request <span class="mand">*</span></label>
								<div class="col-md-6">
									<!--<label class="radio-inline"><input type="radio" checked id="request_type_self" name="request_type" value="Self">Self</label>-->
									<label class="radio-inline"><input type="radio" name="request_type" id="request_type_other" value="Other" checked="checked">On Behalf</label>
								</div>
							</div>
							
							<div class="form-group">
								<label for="cc" class="col-sm-3 control-label">Priority <span class="mand">*</span></label>
								<div class="col-md-6">
									<select name="priority" class="form-control select2-offscreen">
										<option name="medium">Medium</option>
										<option name="high">High</option>
										<option name="low">Low</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Requester name <span class="mand">*</span></label>
								<div class="col-md-6">
									  <input readonly type="text" id="request_name" name="request_name" class="form-control select2-offscreen" placeholder="requester name" value="<?php echo (!empty($user_ldamp) && !empty($user_ldamp['name']))?$user_ldamp['name']:''; ?>" />
									  
								</div>

							</div>
							<div class="form-group" id="behalf_of_div">
								<label for="behalfof" class="col-sm-3 control-label">Behalf of <span class="mand">*</span></label>
								<div class="col-md-6">
									  <input  class="form-control select2-offscreen" placeholder="Please specify email on behalf of whom" type="email" id="behalf_of" name="behalf_of" />
								</div>

							</div>
							
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Remark on Behalf <span class="mand">*</span></label>
								<div class="col-md-6">
									  <textarea id="behalf_remark" name="behalf_remark" class="form-control select2-offscreen" maxlength="250" ></textarea>
								</div>
							</div>
							
							<!--
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Base Location <span class="mand">*</span></label>
								<div class="col-md-6">
								
									 <select id="request_location" name="request_location" class="form-control select2-offscreen" >
										<option value="">Please select</option>
										<?php foreach($sitelocation as $locationRecord){ ?>
											<option value="<?php echo $locationRecord->site_location_id; ?>"><?php echo $locationRecord->site_location_name; ?></option>
										<?php } ?>
									 </select>
									 <input type="hidden" id="requester_location_name" name="requester_location_name">
									<input type="hidden" value="<?php echo $dept_id; ?>" name="dept_id" />
								</div>
							</div>
							-->
							
							
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Service required state <span class="mand">*</span></label>
								<div class="col-md-6">
									 <select id="request_state" name="request_state" class="form-control select2-offscreen" >
										<option value="">Please select</option>
										<?php $stateData = get_state(); foreach($stateData as $stateRecord){ ?>
											<option value="<?php echo $stateRecord->state_id; ?>"><?php echo $stateRecord->state_name; ?></option>
										<?php } ?>
									 </select>
									 <input type="hidden" id="requester_location_name" name="requester_location_name">
									<input type="hidden" value="<?php echo $dept_id; ?>" name="dept_id" />
								</div>
							</div>
							

							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Service required location <span class="mand">*</span></label>
								<div class="col-md-6">
									 <!-- <input type="text" name="request_location" class="form-control select2-offscreen" placeholder="location" />-->
									 <select id="request_location" name="request_location" class="form-control select2-offscreen" >
										<!--<option value="">Please select</option>
										<?php foreach($sitelocation as $locationRecord){ ?>
											<option value="<?php echo $locationRecord->site_location_id; ?>"><?php echo $locationRecord->site_location_name; ?></option>
										<?php } ?>-->
									 </select>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label for="cc" class="col-sm-3 control-label">Category <span class="mand">*</span></label>
								<div class="col-md-6">
									<select id="category" name="category" class="form-control select2-offscreen">
									<option value="">Select Catgeory</option>
									<?php foreach($categories_hd as $category){ ?>
									<option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>	
									<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="cc" class="col-sm-3 control-label">Sub - category <span class="mand">*</span></label>
								<div class="col-md-6">
									<select id="sub_category" name="sub_category" class="form-control select2-offscreen">
										
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="cc" class="col-sm-3 control-label">Item <span class="mand">*</span></label>
								<div class="col-md-6">
									<select id="item" name="item" class="form-control select2-offscreen">
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Subject <span class="mand">*</span></label>
								<div class="col-md-6">
									  <input type="text" name="request_subject" class="form-control select2-offscreen" placeholder="Subject" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Description <span class="mand">*</span></label>
								<div class="col-md-6">
									  <textarea id="request_description" name="request_description" class="form-control select2-offscreen" maxlength="250" ></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Group <span class="mand">*</span></label>
								<div class="col-md-6">
									<select class="form-control select2-offscreen" name="group" id="group"> 
									    <option value="">Select group</option>
										<?php foreach ($groups_hd as  $value) {?>
										<option value="<?php echo $value->group_id; ?>"><?php echo $value->group_name; ?></option>
										<?php } ?>
									<select>
								</div>
							</div>
							<div class="form-group">
								<label for="bcc" class="col-sm-3 control-label">Technician Assign <span class="mand">*</span></label>
								<div class="col-md-6">
									<select class="form-control select2-offscreen" name="technician" id="technician"> 
									<select>
								</div>
							</div>
							<div class="form-group">
								<label for="attachment" class="col-sm-3 control-label">Attachment </label>
								<div class="col-md-6 fileupload">
								<span class="tooltiptext"> 
								<ul>
									<li>Formats Supported .jpeg,.jpg,.png</li>
									<li>Maximum 1 MB can be Uploaded</li>
								</ul>
								</span>
									  <input  type="file" name="request_attach[]" class="form-control select2-offscreen multi" placeholder="attachment"  />
								</div>	  
								</div>
								
							</div>
							<div class="form-group">
								
								<div class="col-sm-6">
									  <!--<button id="btnSubmit" class="btn btn-primary">Submit</button>-->
									  <input type="submit" id="btnSubmit" class="btn btn-primary" value="Submit" />
								</div>
							</div>
						</form>
						
					</div>	
				</div>	
			</div><!--/.col-->		
		</div>
	</div>
	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>-->

<!-- editor -->
 
   <!--<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/editr/jquery-te-1.4.0.css">	
<script type="text/javascript" src="<?php echo base_url() ?>assets/editr/jquery-te-1.4.0.min.js" charset="utf-8"></script>-->
<!-- editor -->

<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/jQuery.MultiFile.min.js" type="text/javascript" language="javascript"></script>
  <script type="text/javascript" src='<?php echo base_url() ?>assets/JavaScriptSpellCheck/include.js' ></script>
  
<script>
var availableTags = [];
function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
function auto_complete()
{
	$( "#behalf_of")
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( "" );
          return false;
        }
      });
	
}
	var trVal = 1;
	$(document).ready(function(){
	//********code for suggestion *************//
	var searchphrase;
	if($("#behalf_of").val()!=""){
	    searchphrase = 	$("#behalf_of").val();
	}
    
    
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "<?php echo base_url().'helpdesk/getEmail/filter'; ?>",
		dataType: "json",
		data: "{'phrase':'" + searchphrase + "'}",
		success: function(data) {
		 
		 for(var i = 0;i<data.length;i++){
			 availableTags[i]= data[i].name;
		 }
		},
		error: function(result) {
		alert("Error");
		}
	});
	auto_complete();
	
		//********code for spellcheck **********//
		$('#request_description').spellAsYouType();
	//********code for spellcheck ends **********//
		
		
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
		
		
		//$('.jqte-test').jqte();
		
		// settings of status
		//var jqteStatus = true;
		/*$(".status").click(function(){
			jqteStatus = jqteStatus ? false : true;
			$('.jqte-test').jqte({"status" : jqteStatus})
		});*/
		
		//****code to validate the form************//
		//$("#btnSubmit").click(function(){
			//$("#errMsg").text('');
			//return true;
			//return false;
		//});
		/* $.validator.addMethod("regex", function (value, element, regexpr) {
			return regexpr.test(value);
		}, "Please enter a valid name.");

		  $.validator.setDefaults({
			 
			errorPlacement: function(error, element) {
				error.appendTo('#errMsg');
				$("#errMsg").append('<br>');
			}
		});*/
		
		$("#service_request_form").validate({
		 
			rules: {
				request_description:{
					required:true
					//regex: /^[A-Za-z]+$/
				},
				/*request_type: {required:true},*/
				priority: {required:true},
				request_name:{required:true},
				request_state:{required:true},
				request_location:{required:true},
				category: {required:true},
				sub_category: {required:true},
				item: {required:true},
				request_subject: {required:true},
				group:{required:true},
				technician:{required:true},
				behalf_remark:{required:true},
			/*	behalf_of:{required:'#request_type_other:checked',email:true} */
				behalf_of:{required:true,email:true}
				
			},
			 
			messages: {
				
				/*"request_type": {
					required: "Please select one request type"                
				}, */
				"priority": {
					required: "Please select one priority item"                
				},
				"request_name": {
					required: "Please enter requester name"                
				},
				"request_state": {
					required: "Please select requester state"                
				},
				"request_location": {
					required: "Please select requester location"                
				},
				"category": {
					required: "Please select category"                
				},
				"sub_category": {
					required: "Please select sub-category"                
				},
				"item": {
					required: "Please select item"                
				},
				"request_subject": {
					required: "Please enter subject"                
				},
				"request_description": {
					required: "Please enter the description"                
				},
				"group": {
					required: "Please select the group"                
				},
				"technician": {
					required: "Please select the technician"                
				},
				"behalf_of": {
					required: "Please enter the email on behalf of whom the request is raised"                
				},
				"behalf_remark": {
					required: "Please enter the remark"
				}
				
				
			},
			submitHandler: function (form) { 
				
				form.submit();
				$("#spinner").show();
			}
			
		});
		//*****code to validate the form ends here *****//
		
		//******code to change the category,sub-category begins *****//
		$("#category").on("change",function(){
			
			var category_id = $(this).val();
			if(category_id == ''){
				alert("please select a category");
				$("#sub_category").html('');
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>helpdesk/get_subcategory_hd",
					type:'POST',
					data:{categoryid:category_id},
					success:function(response)
					{
						console.log(response);
						$("#sub_category").html(response);
					},
					error:function(response){
						alert("Error occured"+response);
					}
				});
			}
			
		});
		$("#sub_category").on("change",function(){
			var sub_category_id = $(this).val();
			if(sub_category_id == ''){
				alert("please select a Sub category");
				$("#item").html('');
			}
			else
			{
				$.ajax({
					url:"<?php echo base_url(); ?>helpdesk/get_item_hd",
					type:'POST',
					data:{subcategory_id:sub_category_id},
					success:function(response)
					{
						console.log(response);
						$("#item").html(response);
					},
					error:function(response){
						alert("Error occured"+response);
					}
				});
			}
		});
		
		$("#request_state").on("change",function(){
			var state_id = $(this).val();
			if(state_id == ''){
				alert("please select a State");
				$("#request_location").html('');
			} else {
				$.ajax({
					url:"<?php echo base_url(); ?>helpdesk/get_location_hd",
					type:'POST',
					data:{state_id:state_id},
					success:function(response){
						console.log(response);
						$("#request_location").html(response);
					},error:function(response){
						alert("Error occured"+response);
					}
				});
			}
		});
		
		//******code to change the category,sub-category ends *****//
		
		//******code to raise new request for self or other starts here ********//
		   $("#request_type_other").on('change',function(){
			   if($(this).is(":checked")){
				  $("#behalf_of_div").show();
			   }
			   
		   });
		   $("#request_type_self").on('change',function(){
			   if($(this).is(":checked")){
				   $("#behalf_of_div").hide();
			   }
			   
		   });
		//******code to raise new request for self or other ends here ********//
		
		//******code to change the user location and set the user location id ****//
		$("#request_location").on("change",function(){
			
			var name = $(this).find(":selected").text();
			if (name == ""){
				alert("please select the location");
				
				$("#requester_location_name").val('');
			}
			else
			{
				
				$("#requester_location_name").val(name);
			}	
		});
		//******code to change the user location and set the user location id endshere ****//
		
		
		
		/*****************************group change***********/
		$("#group").on("change",function(){
            var group_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/get_technician",
				type: "post",
				dataType:"json",
				data:{group_id:group_id },
				success:function(response){
					
					var str_technician = "<option value=''>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_technician += "<option value='"+response[i].tech_id+"'>"+response[i].tech_name+"</option>";
					}
					$("#technician").html(str_technician);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
  
	});
	
</script>

</div>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>			