<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style1.css"  />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
<link href="<?php echo base_url() ?>assets/datetime/bootstrap-material-datetimepicker.css"  rel="stylesheet" type="text/css"  />
<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" type"text/css" rel="stylesheet"/>

<style>
  	
</style>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
	<script src="https://select2.github.io/dist/js/select2.full.js"></script>
</head>
<body>
<br>
<div class="tab-content">
	<div class="row">
		<div class="col-md-12">
            <div class="panel with-nav-tabs panel-success">
			<div class="row inbox">
				<div class="col-md-13">
                    <?php //echo "<pre>";print_r($request_details); ?>
                    <?php //echo "<pre>";print_r($request_more_details); ?>
					<?php $dept = get_record_by_id_code('dept',$request_more_details->dept_id);  ?>
					<?php $technician = get_record_by_id_code('technician',$request_more_details->technician_id);  ?>
					<?php $group = get_record_by_id_code('group',$request_more_details->group_id);  ?>
				     <span class="btn backtab btn-block" style="height:44px;"><b class="pull-left" style="padding-top:6px;">Request ID : <?php echo $request_details->user_request_id; ?>	</b>
					    <div class="dropdown pull-right">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a onClick="window.open('<?php echo base_url(); ?>helpdesk/print_preview/<?php echo $request_details->user_request_id ; ?>','mywindow','menubar=1,resizable=1,width=650,height=600,location=center');" href="#">Print Preview</a></li>
								<li><a href="#" data-toggle="modal" data-target="#myModalNotes">Add Notes</a></li>
								<li><a href="#" data-toggle="modal" data-target="#myModalAttachment">Add Attachment</a></li>
							</ul>
				        </div>
						<!--<div class="dropdown pull-right">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Reply
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#">Reply</a></li>
								
							</ul>
				        </div>-->
					 </span>
					

					<div class="panel panel">
					
					 <div class="pull-right">
						<table style="background-color:#cadb2b;color:#1c6b94;border-radius:6px;margin:5px;" class="right-status">
							<tbody>
								<tr>
									<td>Status </td>
									<td>
										<b>: <div id="status_PH" style="display:inline" class="fontBlackBold"><?php echo ucfirst($request_more_details->task_status); ?></div></b>
									</td>
								</tr>
								
								<tr>
									<td>Priority</td>
									<td><b><div id="priority_PH">: <?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></div></b></td>
								</tr>
								
							</tbody>
						</table>
					</div>
					
						
						<div class="panel-body message">
							
							<p>
							<!--<i class="fa fa-edit" style="font-size:24px"></i>-->
							<img src="http://demo.servicedeskplusmsp.com/images/service-request-icon.png" />
							<b>
							  <?php echo ucfirst(strip_tags($request_details->request_subject)); ?>
							</b>
							</p>
							<p>
							<b>By <a onClick="window.open('<?php echo base_url(); ?>helpdesk/requester_details/<?php echo $request_details->user_request_id ; ?>','mywindow','menubar=1,resizable=1,width=450,height=400,location=center');" href="javascript:void(0);"><?php echo  $request_details->requester_name; ?></a>
							</b>
							<span style="font: normal 12px Arial, Helvetica, sans-serif;">on  <?php echo convert_std_time($request_details->request_created_dt);  ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Due Date : </b><span
							style="font: normal 12px Arial, Helvetica, sans-serif;"><?php $duedate = date('M d ,Y h:i:s A',strtotime('+2 hours',strtotime($request_details->request_created_dt))); echo $duedate; ?> </span></p>
						</div>	
				    </div>
				</div><!--/.col-->		
			</div>
                <div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1success" data-toggle="tab" id="tab1">&nbsp;&nbsp;&nbsp;&nbsp;Request&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
						<li><a href="#tab2success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;Resolution&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
						<li><a href="#tab3success" data-toggle="tab" id="tab2">&nbsp;&nbsp;&nbsp;&nbsp;History&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
					</ul>
                </div>
					<!-- Modal To Add Notes -->
					<div id="myModalNotes" class="modal fade" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Notes</h4>
						  </div>
						  <div class="modal-body">
							<p><b>REQUEST ID:<?php echo $request_details->user_request_id; ?></b></p>
							<p> <textarea name="notes" id="notes" cols="80" rows="10"></textarea></p>
						  </div>
						  <div class="modal-footer">
							<button type="button" onclick="add_notes()" class="btn btn-default">Add Note</button>
							
						  </div>
						</div>

					  </div>
					</div>
					<!-- Modal To Attach File -->
					<div id="myModalAttachment" class="modal fade" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Attach File</h4>
						  </div>
						  <form method="post" action="<?php echo base_url() ?>helpdesk/save_attachments" enctype="multipart/form-data" onsubmit="return checkAttachment();">
							  <div class="modal-body">
							     <input type="hidden" name="user_request_id" value="<?php echo $request_details->user_request_id; ?>">
								<p>FILE:<input type="file" name="request_attach[]" id="request_attach" class="form-control select2-offscreen multi" placeholder="attachment" ></p>
								[Maximum size of an attachment is 10MB]
							  </div>
							  <div class="modal-footer">
								<input id="add_att" type="submit"  class="btn btn-default" value="Upload File">
								
							  </div>
						  </form>
						</div>

					  </div>
					</div>
                <div ><!-- class="panel-body" -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1success">
							<div class="row inbox">
								<div class="col-md-13">
									<div class="panel panel-default">
										<span><b style="padding-left: 12px;padding-top: 6px;">Description</b></span><hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 9px;">
										<div class="panel-body message">
												
											       <?php  if($request_details->request_type=='email'){ ?>
											          <iframe style="border-style:none;"  width="100%" src="<?php echo base_url(); ?>helpdesk/request_details_email/<?php echo $request_details->user_request_id; ?>">
												     </iframe>
											      <?php }else{ ?>
											        <div>
													  <p>
												        <?php echo $request_details->request_desc; ?><br/>
														
													  </p>
													  </div>
												  <?php } ?>
												  <?php
		//echo $request_details->user_code;
		 $email = get_email_by_emp_code($request_details->user_code); 
		 
		if(!empty($email)) {
			$user_details = get_all_details_by_emp_code($request_details->user_code);
			//print_r($user_details);
		}?>
		<?php $attachments = (get_attachment($request_details->user_request_id)); 
														if(!empty($attachments)){
															
														if(!empty($user_details)){
				                                      $u_id = $user_details->u_id;
			                                           
													   }	
														?>
														
														Attachments:
														<?php 
														echo '<ul style="position:relative;margin-left:70px;list-style-type:none;">';
													//print_r($attachments);
													foreach($attachments as $attach){
														$filesize_byte = (filesize($_SERVER['DOCUMENT_ROOT'].'/uploads/helpdesk/'.$u_id.'/'.$attach->attachment_name));
														$filesize = $filesize_byte/1024;$size=$filesize;
														$download_link =base_url().'uploads/helpdesk/'.$u_id.'/'.$attach->attachment_name; 														
														echo '<li style="color:black;text-decoration:underline;"><img src="'.base_url().'/assets/images/attach.png">'.' '.'<a download style="color:black;" href="'.$download_link.'">'.$attach->attachment_name.'</a>'.' ('.round($size,2).' Kb)'."<br/></li>";
													        }
														echo '</ul>';	
														}	
													?>	
													
											

										</div>	
										<div class="panel-footer">
											  <!--<button onClick="window.open('<?php echo base_url(); ?>helpdesk/emailcomposer/<?php echo $request_details->user_request_id; ?>/REPLY','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-primary btn-sm">Reply</button>-->
											 <!-- <button onClick="window.open('<?php echo base_url(); ?>helpdesk/emailcomposer/<?php echo $request_details->user_request_id; ?>','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-default btn-sm">Forward</button>-->
											
										</div>
									</div>
								</div><!--/.col-->	
                                <div class="col-md-13">
                                 <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Requester Conversations</span>   | <span style="font: bold 14px Arial, Helvetica, sans-serif!important;"> <a style="color:inherit;" href="<?php echo base_url(); ?>helpdesk/requestdetails/<?php echo $request_details->user_request_id; ?>/viewall">[View All Conversations]</a></span>
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                           
											<?php if(!empty($emails)){
												    foreach($emails as $email){?>
												<!---code for each accordian starts from here---> 
												<div class="panel panel-default">
													<div class="panel-heading">
							                            <h4 class="panel-title">
								                        <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapse<?php echo $email->sytem_emails_id; ?>">
									                    <!--<i class="fa fa-unlock" style="margin-left:1.0em"></i>-->
									                    <i class="glyphicon glyphicon-envelope" style="margin-left:1.0em"></i>
										                <span style="margin-left:1.0em;font-size:12px;">
											            <b>System</b>
										                </span>
										               <span style="margin-left:0.5em;font-size:12px;"> on <?php echo convert_std_time($email->created_date); ?>
										               </span>
								                       </a>
							                           </h4> 
						                            </div>
						                            <div id="collapse<?php echo $email->sytem_emails_id; ?>" class="panel-collapse collapse">
							                        <div class="panel-body" style="overflow-y:scroll;">
								                        <p>
											            <b>To</b>:<?php echo $email->to_emails; ?></p>
								                        <p><b>Summary</b></p>
								                        <hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 0.1px;">
								                       <p id="autosubj"><?php echo $email->subject ?></p>
								                      <p><b>Description</b></p>
								                     <hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 0.1px;">
								                    <div id="autodesc">
									                <?php  echo $email->description;  ?>
								                    </div>
											        <div class="panel-footer">
												         <button  onClick="window.open('<?php echo base_url(); ?>helpdesk/emailcomposer/<?php echo $request_details->user_request_id; ?>','mywindow','menubar=1,resizable=1,width=1100,height=600,location=center');" class="btn btn-primary btn-sm">Forward Request</button>
											        </div>
							                    </div>
						                    </div>
					                    </div>
				                        <!---code for each accordian ends here--->
			                        <?php }?>
			                    <?php } ?>
                                            
                                            
                                        </div>
        
                                    </div>
                                </div>    
                                <div class="col-md-13">
                                    <div class="row margin-top-10">
                                        <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Request Details</span> 
                                    </div>
									<hr/>
									<?php if($this->session->flashdata('msg')): ?>
									<div class="alert alert-success alert-dismissable">
                                     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                     <strong>Success!</strong>
									 <?php echo $this->session->flashdata('msg'); ?>
                                    </div>
                                    <?php endif; ?>
									<?php if($this->session->flashdata('error_msg')): ?>
									<div class="alert alert-danger alert-dismissable">
                                     <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                     <strong>Error!</strong>
									 <?php echo $this->session->flashdata('error_msg'); ?>
                                    </div>
                                    <?php endif; ?>
                                    <form method="post" action="#">
                                
                                    <input type="hidden" name="request_id" id="request_id" value="<?php echo $request_details->user_request_id; ?>" />
									<input type="hidden" name="requester_user_code" id="requester_user_code" value="<?php echo $request_details->user_code; ?>" />
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <b>Status</b>
                                                </div>
                                                <div class="col-sm-3">
                                                  <span><?php echo ucfirst($request_more_details->task_status); ?></span>
                                                    <span style="display:none;">
                                                        <select name="status" id="status" class="form-control">
                                                            <option>Select</option>
															<option <?php if($request_more_details->task_status=='open')  echo 'selected'; ?> value="open">Open</option>
															<option <?php if($request_more_details->task_status=='wip') echo 'selected';  ?> value="wip">WIP</option>
															<option <?php if($request_more_details->task_status=='hold')  echo 'selected'; ?> value="hold">On Hold</option>
                                                            <option <?php if($request_more_details->task_status=='close')  echo 'selected'; ?> value="close">Closed</option>
                                                            <option <?php if($request_more_details->task_status=='resolve')  echo 'selected'; ?> value="resolve">Resolved</option>
                                                            
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->task_status;  ?>" name="prev_status" id="prev_status" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Workstation Number</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo $request_more_details->tech_workstation_no; ?></span>
                                                    <span style="display:none;"><input type="text" class="form-control" id="work_Station" name="work_Station"/>
													<input type="hidden" value="<?php echo $request_more_details->tech_workstation_no;  ?>" name="prev_workstation" id="prev_workstation" />
													</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Mode</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo ucfirst($request_details->request_type); ?></span>
                                                    <span style="display:none;">
                                                        <select name="mode" id="mode" class="form-control">
                                                            <option>Select</option>
                                                            <!--<option>E-mail</option>
                                                            <option>Phone Call</option>
                                                            <option>Web Form</option>-->
                                                            <?php foreach($request_mode as $mode): ?>
                                                            <option <?php echo ($request_details->request_type==$mode->request_type)?'selected':''; ?> value="<?php echo $mode->request_type ?>"><?php echo ucfirst($mode->request_type); ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->request_type;  ?>" name="prev_mode" id="prev_mode" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Department</b></div>
                                                <div class="col-sm-3">  
                                                    <span><?php  echo $dept->dept_name; ?></span>
                                                    <span style="display:none;">
                                                    <select name="dept" id="dept" class="form-control">
                                                        <option>Select</option>
                                                        <?php foreach ($depts_hd as  $value) {?>
                                                        <option <?php echo ($value->dept_id == $request_more_details->dept_id)?'selected':''; ?> value="<?php echo $value->dept_id; ?>"><?php echo $value->dept_name; ?></option>
                                                        <?php } ?>
                                                    </select>
													<input type="hidden" value="<?php echo $request_more_details->dept_id;  ?>" name="prev_dept" id="prev_dept" />
                                                    </span>              

                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                               <!-- <div class="col-sm-3"><b>Level</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="level" id="level" class="form-control">
                                                            <option>Select</option>
                                                            <option>Tier1</option>
                                                            <option>Tier2</option>
                                                            <option>Tier3</option>
                                                            <option>Tier4</option>
                                                        </select>
                                                    </span>
                                                </div>-->
												<div class="col-sm-3"><b>Site</b></div>
                                                <div class="col-sm-3">
                                                    <?php //print_r($site_location_hd); ?>
                                                    <span>Pune</span>
                                                    <span style="display:none;">
                                                        <select name="site" id="site" class="form-control">
                                                            <option>Select</option>
                                                            <?php foreach($site_location_hd as $location): ?>
                                                            <option <?php echo ( $request_details->requester_location_id==$location->site_location_id)?'selected':''; ?> value="<?php echo $location->site_location_id; ?>"><?php echo $location->site_location_name;   ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->requester_location_id;  ?>" name="prev_site" id="prev_site" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Priority</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></span>
                                                    <span style="display:none;">
                                                        <select name="priority" id="priority" class="form-control">
                                                            <option>Select</option>
                                                            <option <?php echo ($request_details->request_priority=="High") ?'selected':''; ?> value="High">High</option>
                                                            <option <?php echo ($request_details->request_priority=="Low") ?'selected':''; ?> value="Low">Low</option>
                                                            <option <?php echo ($request_details->request_priority=="Medium") ?'selected':''; ?> value="Medium">Medium</option>
                                                            <option <?php echo ($request_details->request_priority=="Normal") ?'selected':''; ?> value="Normal">Normal</option>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->request_priority;  ?>" name="prev_priority" id="prev_priority" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Group</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span><?php echo $group->group_name ; ?>
													</span>
													
                                                    <span style="display:none;">
                                                        <select name="group" id="group" class="form-control">
														
                                                            <option>Select</option>
                                                            <?php foreach ($groups_hd as  $value) {?>
                                                            <option <?php echo ($value->group_id == $request_more_details->group_id)?'selected':''; ?> value="<?php echo $value->group_id; ?>"><?php echo $value->group_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->group_id;  ?>" name="prev_group" id="prev_group" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Category</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php $category = get_record_by_id_code('category',$request_details->category_id);echo $category->category_name;  ?>
													</span>
                                                    <span style="display:none;"> 
                                                        <select name="category" id="category" class="form-control">
                                                            <option>Select</option>
                                                            <?php $categories = get_category();?>
                                                            <?php foreach ($categories as  $value) {?>
                                                            <option <?php echo ($request_details->category_id == $value->category_id )?'selected':''; ?> value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->category_id;  ?>" name="prev_category" id="prev_category" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Technician</b></div>
                                                <div class="col-sm-3">
                                                   <?php  $emp_det = getEmpByEmail($technician->technician_email);  ?>
                                                    <span><?php  echo  $technician->tech_name; ?>
													
													<div class="dropdown">
												    
													<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="material-icons" style="margin-top:-25px;float:right;" >search</i>
													</a>
													
													<div class="dropdown-menu" style="width:380px;max-height:325px;padding-left:8px;overflow-x:hidden;overflow-y:auto;">
														<button type="button" class="close pop" aria-label="Close">
															<span style="font-size:18px;" aria-hidden="true">&times;</span>
														</button>
														<table width="100%" height="100%">
			                                                <tbody>
																<tr>
																	<td valign="middle" height="22">
																	Technician details - <?php  echo  $technician->tech_name; ?>
																	<?php //print_r($emp_det); ?>
																	<?php $dept = get_record_by_id_code('dept',$request_details->dept_id);  ?>
																
																	</td>
																</tr>
					                                            <tr>
																    <td height="0" valign="top">
																        <table width="100%" border="0" cellpadding="2" cellspacing="0">
																	        <tbody>
																				<tr>
																					<td width="2%"><i class="material-icons" style="font-size:48px;color:black">person</i></td>
																					<td width="44%">
																							<span><?php  echo  $technician->tech_name; ?></span> 
																							<span class="fontBlack"><?php echo ($emp_det->u_designation!='')?"-". ucfirst($emp_det->u_designation):"";  ?>
																							</span>
																					</td> 										
																				</tr> 
																				<tr>
																					 <td colspan="2" valign="top"></td>
																				</tr>
																		        <tr valign="bottom">
																			       <td colspan="3" valign="top">
																					<div>
																						<table width="100%" id="techdetails">
																							<?php $technican_name = explode(" ",$technician->tech_name); ?>
																							<tbody>
																								<tr><td colspan="2" valign="top"></td></tr>
																								 <tr class="row0">
																									<td width="35%" valign="top">Department Name</td>
																									<td width="65%" valign="top"><?php echo $dept->dept_name; ?></td>
																								 </tr>
																								<tr class="row1">
																									<td width="35%" valign="top">E-Mail</td>
																									<td width="65%" valign="top"><?php echo ucfirst($technician->technician_email);  ?></td>
																								</tr>
																								<tr class="row0">
																									<td width="35%" valign="top">First Name</td>
																									<td width="65%" valign="top"><?php echo $technican_name[0] ; ?></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Job Details</td>
																									<td width="65%" valign="top"><?php $technican_name = explode(" ",$technician->tech_name); ?></td>
																								 </tr>
																								 <tr class="row0">
																									<td width="35%" valign="top">Last Name</td>
																									<td width="65%" valign="top"><?php echo $technican_name[1];  ?></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Middle Name</td>
																									<td width="65%" valign="top"></td>
																								 </tr>
																								 <tr class="row0">
																									<td width="35%" valign="top">Mobile</td>
																									<td width="65%" valign="top"><?php echo ($emp_det->u_mobile!='')?$emp_det->u_mobile:"";  ?></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Phone</td>
																									<td width="65%" valign="top"></td>
																								 </tr>	
																								 <tr class="row0">
																									<td width="35%" valign="top">Description</td>
																									<td width="65%" valign="top"></td>
																								 </tr>
																								 <tr class="row1">
																									<td width="35%" valign="top">Site Name</td>
																									<td width="65%" valign="top"><?php echo ($emp_det->u_streetaddress!='')?$emp_det->u_streetaddress:"";  ?></td>
																								 </tr>
																							</tbody>  
																						</table>  
																					</div>  
																			      </td> 
																		        </tr>
																	        </tbody> 
																        </table>
														            </td> 
					                                            </tr>
				                                            </tbody>
			                                            </table>
												    </div> 
													</div>
													</span>
                                                    <span style="display:none;">
                                                        <select name="technician" id="technician" class="form-control">
                                                            <option>Select</option>
                                                            <?php foreach($technicians_hd as $tech): ?>
                                                            <option <?php echo ($request_more_details->technician_id==$tech->tech_id)?'selected':''; ?> value="<?php echo $tech->tech_id;  ?>"><?php echo $tech->tech_name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->technician_id;  ?>" name="prev_technician" id="prev_technician" />
                                                    </span>
													
                                                </div>
                                                <div class="col-sm-3"><b>Subcategory</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php $subcategory = get_record_by_id_code('subcategory',$request_details->subcategory_id);echo $subcategory->sub_category_name;  ?></span>
                                                    <span style="display:none;">
                                                        <select name="subactegory" id="subactegory" class="form-control">
                                                            <option>Select</option>
                                                            <?php $subcategories = get_sub_category();?>
                                                            <?php foreach ($subcategories as  $value) {?>
                                                            <option <?php echo ($request_details->subcategory_id == $value->sub_category_id )?'selected':''; ?>  value="<?php echo $value->sub_category_id; ?>"><?php echo $value->sub_category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->subcategory_id;  ?>" name="prev_subcategory" id="prev_subcategory" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
											    <div class="col-sm-3"><b>SLA</b></div>
                                                <div class="col-sm-3">- </div>
                                                
                                                <div class="col-sm-3"><b>Item</b></div>
                                                <div class="col-sm-3">
                                                
                                                    <span><?php $item = get_record_by_id_code('item',$request_details->item_id);echo $item->item_name;  ?></span>
                                                    <span style="display:none;">
                                                        <?php $items = get_item();  ?>
                                                        <select name="item" id="item" class="form-control">
                                                            <option>Select</option>
                                                            
                                                            <?php foreach($items as $value): ?>
                                                            <option <?php echo ($request_details->item_id== $value->items_id) ? 'selected':''; ?> value="<?php echo $value->items_id; ?>"><?php echo $value->item_name; ?></option>
                                                           <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->item_id;  ?>" name="prev_item" id="prev_item" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <!--<div class="col-sm-3"><b>Service Category</b></div>
                                                <div class="col-sm-3">
                                                     
                                                     <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="service_category" id="service_category" class="form-control">
                                                            <option>-----Choose-----</option>
                                                        </select>
                                                    </span>
                                                </div>-->
												<div class="col-sm-3"><b>Template</b></div>
                                                <div class="col-sm-3">Default Request </div>
                                                <div class="col-sm-3"><b>Created By</b></div>
                                                <div class="col-sm-3"><?php echo $request_details->requester_name; ?></div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <!--<div class="col-sm-3"><b>Asset(s)</b></div>
                                                <div class="col-sm-3">
                                                     
                                                    <span>-</span>
                                                    <span style="display:none;">
                                                        <textarea name="assets" class="form-control" id="assets"></textarea>
                                                    </span>
                                                </div>-->
												
												<div class="col-sm-3"><b>Resolved Date</b></div>
                                                <div class="col-sm-3">- </div>
                                                <div class="col-sm-3"><b>Created Date</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo convert_std_time($request_details->request_created_dt); ?></span>
                                                    <span style="display:none;"><input class="form-control" type="text" readonly value="<?php echo convert_std_time($request_details->request_created_dt); ?>" id="create_date" name="create_date"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Time Elapsed</b></div>
                                                <div class="col-sm-3">
												<?php 
													//$currentTime = strtotime(date('Y-m-d h:i:s'));
													//$systemtTime  = strtotime($request_more_details->request_assign_person_dt);

													//$timeelapsed = $currentTime - $systemtTime;
												   $timeelpase_str = " "; 
		                                           $timeelapsed = get_time_diff(date('Y-m-d H:i:s'),$request_more_details->request_assign_person_dt);
												   if($timeelapsed["days"]!='0'){
													    $timeelpase_str .= $timeelapsed["days"].'days'.' ';   
												   }
												   if($timeelapsed["hurs"]!='0'){
													    $timeelpase_str .= $timeelapsed["hurs"].'hrs'.' ';   
												   }
												   if($timeelapsed["mins"]!='0'){
													    $timeelpase_str .= $timeelapsed["mins"].'min'.' ';   
												   }
												   if($timeelapsed["secd"]!='0'){
													   // $timeelpase_str .= $timeelapsed["secd"].'s';   
												   }
													echo $timeelpase_str;
													

												?>          
												</div>
                                                <div class="col-sm-3"><b>DueBy Date</b></div>
                                                <div class="col-sm-3">
                                                    <span><?php echo $duedate; ?></span>
                                                    <span  style="display:none;" class="input-group date" ><input class="form-control " type="text" value="<?php echo $duedate;?>" id="dueby_date" name="dueby_date"/>
													<input type="hidden" value="<?php  echo $duedate;  ?>" name="prev_dueby_date" id="prev_dueby_date" />
													</span>
													
													
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Last Update Time</b></div>
                                                <div class="col-sm-3">
												<?php echo ($request_details->response_date!='0000-00-00 00:00:00')? convert_std_time($request_details->last_modified_dt):''; ?>
												</div>
												<div class="col-sm-3"><b>Response DueBy Time</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span><?php echo ($request_details->response_date!='0000-00-00 00:00:00')? convert_std_time($request_details->response_date):''; ?></span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="<?php echo ($request_details->response_date!='0000-00-00 00:00:00')?convert_std_time($request_details->response_date):''; ?>" id="response_dueby" name="response_dueby"/>
                                                    <input type="hidden" value="<?php  echo ($request_details->response_date!='0000-00-00 00:00:00')?convert_std_time($request_details->response_date):'';  ?>" name="prev_res_dueby_date" id="prev_res_dueby_date" />
													</span>
                                                </div>
                                            </div>
                                        </div>
										<div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 pull-right">
												    <button type="submit" id="update_request" name="update_request">Update</button>
                                                    <button type="reset" id="reset_request" onClick="location.reload();" name="reset_request">Reset</button>
                                                    <button type="button" id="cancel_request" name="cancel_request">Cancel</button>
												
												
												</div>
                                                
                                            </div>
                                        </div>
                                            
                                    </form>
                                </div>
                                <div class="col-md-13">
								    <div class="row margin-top-10">
									 <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Requester Details</span> <!--<button id="requester_edit" disabled name="requester_edit"> Edit</button>-->
								    </div>
									<hr/>
                                    <form method="post" action="<?php echo base_url(); ?>helpdesk/saveRequesterDetails" >
									
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Requester Name</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span><?php echo $request_details->requester_name; ?></span>
                                                    <span style="display:none;"><input type="text" class="form-control" value="<?php echo $request_details->requester_name; ?>" id="requester_name" name="requester_name"/> </span>
                                                </div>
                                                <div class="col-sm-3"><b>E-mail Address</b></div>
                                                <div class="col-sm-3">
                                                    <?php $email = get_email_by_emp_code($request_details->user_code); ?>
                                                    <span><?php echo (!empty($email))? $email[0]->u_email:''; ?></span>
                                                    <span style="display:none;"><input type="email" class="form-control" value="<?php echo (!empty($email))? $email[0]->u_email:''; ?>" id="email_address" name="email_address"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                       <!-- <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Contact number</b></div>
                                                <div class="col-sm-3">
                                                     <span>020 66278036</span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="020 66278036" id="contact_no" name="contact_no"/> </span>
                                                </div>
                                                <div class="col-sm-3"><b>Mobile number</b></div>
                                                <div class="col-sm-3">
                                                    
                                                    <span>8805009266</span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="8805009266" id="mobile_no" name="mobile_no"/> </span>
                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3"><b>Department</b></div>
                                                <div class="col-sm-3">
                                                    <span>Not Assigned</span>
                                                    <span style="display:none;">
                                                        <select name="depts" id="depts" class="form-control">
                                                            <option>-----Choose-----</option>
                                                            <option>Skeiron Group</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="col-sm-3"><b>Business Impact</b></div>
                                                <div class="col-sm-3">
                                                    <span>-</span>
                                                    <span style="display:none;">
                                                        <select name="business_impact" id="business_impact" class="form-control">
                                                            <option>-----Choose-----</option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>-->
										<div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 pull-right">
												    <button type="submit" id="update_requester" name="update_requester">Update</button>
                                                    <button type="reset" id="reset_requester" name="reset_requester">Reset</button>
                                                    <button type="button" id="cancel_requester" name="cancel_requester">Cancel</button>
												</div>
                                            </div>
                                        </div>
                                         
                                    </form>
                                </div>
								<?php $notesData = get_user_request_notes($request_details->user_request_id); ?>
								<?php $count_notes = count($notesData);
									  if(($count_notes)>0){	
									?>
								<div class="col-md-13">
								    <div class="row margin-top-10">
									 <span style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;">Discussion Notes</span> 
									 
									 <span  style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;" class="pull-right">[<?php echo $count_notes; ?> note(s)]</span> 
									 <hr/>
								    </div>
									
									<?php foreach($notesData as $noteRecord){?>
											<div class="col-md-12">
											    <p>
												   <div class="col-md-12" style="background-color:rgba(28, 107, 148, 0.22);padding-left:0px;">User:<?php echo  $request_details->requester_name; ?>
												   <span class="pull-right"><?php echo ($noteRecord->notes_added_date != NULL)? convert_std_time($noteRecord->notes_added_date):'';  ?></span>
												   </div><br/>
											       <?php  echo  $noteRecord->notes_description;    ?>
												</p>
											</div>
						            <?php  }
						            ?>
                                </div>
							  <?php }  ?>	
							</div>
						</div>
						
						
						
						<div class="tab-pane fade" id="tab2success">
							<div class="row inbox">
								<div class="col-md-12">
								<!-- sub tab -->
								<div class="col-md-12">
									<div class="tabbable-panel">
										<div class="tabbable-line">
											<ul class="nav nav-tabs ">
												<li class="active">
													<a href="#tab_default_1" data-toggle="tab"> Resolution </a>
												</li>
												<li>
													<a href="#tab_default_2" data-toggle="tab"> Solutions </a>
												</li>
												<li>
													<a href="#tab_default_3" data-toggle="tab"> Tried Solutions </a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_default_1">
												<?php if($request_more_details->task_status =='open'){
												    $display_resoln_msg = "none";
                                                    $display_form_other = "block";
												}
												else{
													$display_resoln_msg = "block";
                                                    $display_form_other = "none";
												}
												?>
												  <div id="resoln_msg" style="width:840px;height:100px;border:1px solid;padding:14px;display:<?php echo $display_resoln_msg; ?>">
												    <b>Resolution submitted by :</b>demo <a onclick="showForm_worklog(this)" href="javascript:void(0);"> Edit</a>
													<span class="pull-right"><b>Submitted on:</b><?php echo (isset($resolution_data->resolution_created_dt))? convert_std_time($resolution_data->resolution_created_dt) :''; ?></span>
													<hr/>
													<span><?php echo (isset($resolution_data->response_desc))? $resolution_data->response_desc :''; ?></span>
												  </div>
													<form style="display:<?php echo $display_form_other; ?>" class="form-horizontal" role="form" id="resolution_frm">
														<div class="form-group">
															<div class="col-sm-12 col-md-12">
																<?php //echo '<pre>'; print_r(); ?>
																  <textarea class="form-control select2-offscreen" name="resolution_txt" id="editor"><?php echo (isset($resolution_data->response_desc))? $resolution_data->response_desc :''; ?></textarea>
																  <input type="hidden" value="<?php echo $request_details->user_request_id; ?>" name="request_id" />
															</div>
														</div>
														
														<div class="form-group selectbox-status">
															<label class="col-sm-3 control-label">Update request status to</label>
															<div class="col-sm-3" style="text-align:left;">
																<select name="statuschange" class="form-control select2-offscreen">
																	<option value="">Select Status</option>
																	<option value="open" <?php echo ($request_more_details->task_status =='open')? 'selected' : '';  ?>>Open</option>
																	<option value="wip" <?php echo ($request_more_details->task_status =='wip')? 'selected' : '';  ?>>WIP</option>
																	<option value="hold" <?php echo ($request_more_details->task_status =='hold')? 'selected' : '';  ?>>On Hold</option>
																	<option value="close" <?php echo ($request_more_details->task_status =='close')? 'selected' : '';  ?>>Close</option>
																	<option value="resolve" <?php echo ($request_more_details->task_status =='resolve')? 'selected' : '';  ?>>Resolved</option>
																</select>
															</div>
														</div>
														<br><br>
														<button type = "button" class="btn btn-success" id="save_resolution"> Save </button>
														<a href="<?php echo base_url().'helpdesk/admin_addresolution/'.$request_details->user_request_id; ?>" class="btn btn-success" id="add_to_solution"> Save & Add to Solution </a>
														
													</form>
													
													
													&nbsp;&nbsp;<input style="display:<?php echo $display_form_other; ?>" type="checkbox" name="work_log" id="timeSpentId" /> <label style="display:<?php echo $display_form_other; ?>" for="timeSpentId">Add Work Log</label><br><br>
													
													<div id="logpage" style="display:none;">
														<div class="form-group selectbox-status"><h5><b>Add Work Log</b></h5></div>
														<table class="worklog">
															<tr>
																<td>Owner</td>
																<td>
																	<select name="owner_detail" class="form-control select2-offscreen" data-live-search="true" style="width:40%;">
																		<option value="1">test 1 </option>
																		<option value="2">test 2 </option>
																		<option value="3">test 3 </option>
																		<option value="4">test 4 </option>
																	</select>
																</td>
																<td>Start Time</td>
																<td>
																	<input type="text" name="start_time" id="res_start_time" class="form-control datepicker" />
																</td>
															</tr>
															
															<tr>
																<td>Time Taken To Resolve</td>
																<td>
																	<div class="form-group">
																	<span class="col-sm-2">Hours</span>
																		<div class="col-sm-3">
																			<input type="text" name="hrs" class="form-control" />
																		</div>
																	<span class="col-sm-2">Minutes</span>
																		<div class="col-sm-3 col-md-3">
																			<input type="text" name="mints" class="form-control" />
																		</div>
																	</div>
																</td>
																<td>End Time</td>
																<td>
																	<input type="text" name="start_time" id="res_end_time" class="form-control datepicker" />
																</td>
															</tr>
															
															<tr>
																<td colspan="4"><input type="checkbox" name="" /> Include non operational hours</td>
															</tr>
															
															<tr>
																<td>Other Charge (Rs)</td>
																<td><input type="text" name="rs_charge" class="form-control" /></td>
																<td></td><td></td>
															</tr>
															
															<tr>
																<td>Description</td>
																<td colspan="4"><textarea name="worklog_desc" class="form-control"></textarea></td>
															</tr>
															
														</table>
														<br>
														<button type = "button" class="btn btn-success col-md-offset-4"> Submit </button>
													</div>
													
												</div>
												
												
												<div class="tab-pane" id="tab_default_2">
													<h4><i class="fa fa-info-circle" aria-hidden="true"></i> No solutions</h4>
												</div>
												
												
												<div class="tab-pane" id="tab_default_3">
													<?php if(count($tried_solutions) > 0){ ?>
															<div class="list-group">
															<?php foreach($tried_solutions as $solutions_record) { 
																$emp_name = get_user_name_by_email($solutions_record->creator_email);
															?>
															
															  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start request_solution" style="margin-bottom:5px;" data-solutionid="<?php echo $solutions_record->request_solution_hd; ?>" >
																<div class="d-flex w-100 justify-content-between">
																  <h5 class="mb-1"><?php echo ucfirst($solutions_record->solution_title); ?></h5>
																</div>
																<p class="mb-1"><?php echo substr(strip_tags($solutions_record->solution_content),0,500); ?></p>
																<small class="text-muted">Applied By <?php echo ucfirst($emp_name).' on '.convert_std_time($solutions_record->request_solution_created_dt); ?></small>
															  </a>
															  
															<?php } ?>
															</div>
													<?php }else{ ?>
															<h4><i class="fa fa-info-circle" aria-hidden="true"></i> No solutions tried </h4>
															
													<?php } ?>
													
													<!-- Modal solution_details start  -->
													<div id="solution_details" class="modal fade" role="dialog">
														<style>
														.list-group-item:hover{
															background-color:rgba(202, 219, 43, 0.43) !important
														}
														</style>
													  <div class="modal-dialog modal-lg">
														
													  </div>
													</div>
													<!-- Modal solution_details end  -->
													
												</div>
												
											</div>
										</div>
									</div>
								</div>
									
								<!-- sub tab end -->
								
								
									<?php /*<form class="form-horizontal" role="form">
										<div class="form-group">
											<div class="col-sm-12 col-md-12">
												  <textarea class="form-control select2-offscreen" id="editor"></textarea>
											</div>
										</div>
										
										<div class="form-group selectbox-status">
											<label class="col-sm-3 control-label">Update request status to</label>
											<div class="col-sm-3" style="text-align:left;">
												<select name="statuschange" class="form-control select2-offscreen">
													<option value="">Select Status</option>
													<option value="open">Open</option>
													<option value="wip">WIP</option>
													<option value="on_hold">On Hold</option>
													<option value="closed">Close</option>
													<option value="resolved">Resolved</option>
												</select>
											</div>
										</div>
										<br><br>
										<button type = "button" class="btn btn-success"> Submit </button>
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Open Modal</button>
									</form>*/ ?>
									
								</div><!--/.col-->
								<?php /*
								&nbsp;&nbsp;<input type="checkbox" name="work_log" id="timeSpentId" /> <label for="timeSpentId">Add Work Log</label><br><br>
								
								<div class="col-md-12" id="logpage" style="display:none;">
									<div class="form-group selectbox-status"><h5><b>Add Work Log</b></h5></div>
									<table class="worklog">
										<tr>
											<td>Owner</td>
											<td>
												<select name="owner_detail" class="form-control select2-offscreen" data-live-search="true" style="width:40%;">
													<option value="1">test 1 </option>
													<option value="2">test 2 </option>
													<option value="3">test 3 </option>
													<option value="4">test 4 </option>
												</select>
											</td>
											<td>Start Time</td>
											<td>
												<input type="text" name="start_time" class="form-control datepicker" />
											</td>
										</tr>
										
										<tr>
											<td>Time Taken To Resolve</td>
											<td>
												<div class="form-group">
												<span class="col-sm-2">Hours</span>
													<div class="col-sm-3">
														<input type="text" name="hrs" class="form-control" />
													</div>
												<span class="col-sm-2">Minutes</span>
													<div class="col-sm-3 col-md-3">
														<input type="text" name="mints" class="form-control" />
													</div>
												</div>
											</td>
											<td>End Time</td>
											<td>
												<input type="text" name="start_time" class="form-control datepicker" />
											</td>
										</tr>
										
										<tr>
											<td colspan="4"><input type="checkbox" name="" /> Include non operational hours</td>
										</tr>
										
										<tr>
											<td>Other Charge (Rs)</td>
											<td><input type="text" name="rs_charge" class="form-control" /></td>
											<td></td><td></td>
										</tr>
										
										<tr>
											<td>Description</td>
											<td colspan="4"><textarea name="worklog_desc" class="form-control"></textarea></td>
										</tr>
										
									</table>
									<br>
									<button type = "button" class="btn btn-success col-md-offset-4"> Submit </button>
								</div> */ ?>
							</div>
							
							
							
							<!-- Modal -->
							<!--<div id="myModal" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">

								
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Status change comment</h4>
								  </div>
								  <div class="modal-body">
									<textarea class="form-control select2-offscreen" placeholder="Comment mandatory"></textarea>
								  </div>
								  <div class="modal-footer">
									<button type = "button" class="btn btn-success" > Submit </button>
									<button type="button" data-dismiss="modal" class="btn btn-default closer">Cancel</button>
								  </div>
								</div>
								
							  </div>
							</div>-->
							
							<!-- modal closed -->
						</div>
						<div id="myModal1" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Close Request</h4>
								  </div>
								  <div class="modal-body">
									<table class="borderless">
										<tr>
											<td>First Call Resolution(FCR) : </td>
											<td><input type="checkbox" name="fcr_checkbox" value="1" id="fcr_checkbox" value="" /></td>
										</tr>
										<tr>
											<td>Has requester acknowledge the resolution ? : </td>
											<td><input  type="radio" id="requester_acknowledge1" name="requester_acknowledge" value="yes" />&nbsp;Yes &nbsp;&nbsp; <input type="radio" name="requester_acknowledge" id="requester_acknowledge2" value="no" />&nbsp;No&nbsp;</td>
										</tr>
										<tr>
											<td>Comment : </td>
											<td><textarea id="general_comment" name="general_comment" class="form-control select2-offscreen" ></textarea></td>
										</tr>
										<tr>
											<td>Request Closure Code : </td>
											<td>
												<!-- Make this dynamic -- *12-5-2017* -->
												<select id="request_closure" name="request_closure" class="form-control"> 
													<option value="null">Choose option </option>
													<option value="3">Canceled</option>
													<option value="2">Failed</option>
													<option value="4">Postponed</option>
													<option value="1">Success</option>
													<option value="5">Unable to Reproduce</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Request Closure Comment / Status Change Comment : </td>
											<td><textarea id="status_change_comment" name="status_change_comment" class="form-control select2-offscreen" placeholder="Comment mandatory"> </textarea></td>
										</tr>
									</table>
									
								  </div>
								  <div class="modal-footer">
									<input type="button" value="Close request" class="btn btn-success" onclick="closeRequest()" />
									<button type="button" data-dismiss="modal" class="btn btn-default closer">Cancel</button>
								  </div>
								</div>
							  </div>
							</div>
						
						 <!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
							  <div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
								  <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Status change comment</h4>
								  </div>
								  <div class="modal-body">
								  <div id="message_err"  style="color:red;"></div>
									<textarea id="comment"  name="comment" class="form-control select2-offscreen" placeholder="Comment mandatory"></textarea>
								  </div>
								  <div class="modal-footer">
									<button type = "button" onClick="saveStatus()" class="btn btn-success" > Submit </button>
								  </div>
								</div>
								
							  </div>
							</div>
							
							
                            <!-- modal closed << CH3 >>  -->
                            <div id="myModal1" class="modal fade" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Close Request</h4>
                                  </div>
                                  <div class="modal-body">
                                    <table class="borderless">
                                        <tr>
                                            <td>First Call Resolution(FCR) : </td>
                                            <td><input type="checkbox" name="fcr_checkbox" value="" /></td>
                                        </tr>
                                        <tr>
                                            <td>Has requester acknowledge the resolution ? : </td>
                                            <td><input type="radio" name="requester_acknowledge" value="yes" />&nbsp;Yes &nbsp;&nbsp; <input type="radio" name="requester_acknowledge" value="no" />&nbsp;No&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Comment : </td>
                                            <td><textarea name="general_comment" class="form-control select2-offscreen" ></textarea></td>
                                        </tr>
                                        <tr>
                                            <td>Request Closure Code : </td>
                                            <td>
                                                <!-- Make this dynamic -- *12-5-2017* -->
                                                <select name="request_closure" class="form-control"> 
                                                    <option value="null">Choose option </option>
                                                    <option value="3">Canceled</option>
                                                    <option value="2">Failed</option>
                                                    <option value="4">Postponed</option>
                                                    <option value="1">Success</option>
                                                    <option value="5">Unable to Reproduce</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Request Closure Comment / Status Change Comment : </td>
                                            <td><textarea name="status_change_comment" class="form-control select2-offscreen" placeholder="Comment mandatory"> </textarea></td>
                                        </tr>
                                    </table>
                                    
                                  </div>
                                  <div class="modal-footer">
                                    <input type="submit" value="Close request" class="btn btn-success" />
                                    <button type="button" data-dismiss="modal" class="btn btn-default closer">Cancel</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
							
						
						 <div class="tab-pane fade" id="tab3success">
							<div class="row inbox">
								<div class="col-md-13">
									<div class="panel panel-default">
										<span class="btn backtab btn-block" style="height:30px;"><b class="pull-left" style="padding-top:1px;">Request History</b></span><br>
										<div class="panel-body message">
											<?php /*if(!empty($history)){ 
											        foreach($history as $history_row){*/
											?>
											<?php echo display_history($request_details->user_request_id); ?>
											<!--<p class="backtab1"><?php echo $history_row->history_subject; ?></p>
											<p>From Host/IP Address: 192.168.22.189</p>-->
											<!--<p><?php //echo $history_row->history_body; ?></p>-->
													<?php // }
											//} ?>
											<!--<p  class="backtab1">Updated by Shaijad Kureshi on Apr 3, 2017 07:43 PM</p>
											<p>Request Updated by Shaijad Kureshi</p>
											<p>Group changed from L1 Desktop Team to None</p>
											<p>Technician changed from Rahul Survase to Shaijad Kureshi</p>
											<p>Time of technician assignment changed from Apr 3, 2017 07:42 PM to Apr 3, 2017 07:43 PM</p>-->
										</div>	
									</div>
								</div><!--/.col-->	

							</div>
						</div>
						
                    </div>
                </div>
            </div>
        </div>
	</div>

</div>
<br><br>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>	  
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>-->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jQuery.MultiFile.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/datetime/bootstrap-material-datetimepicker.js"></script>-->
 
<script>
     
	var trVal = 1;
	var statusValPrevious = 'open';
	var request_id;
	var dueby_date;
	var groupId;
	var technicianId;
	function showForm_worklog(obj)
	{	
	    $(obj).parent().css("display","none");
		$("#resolution_frm").css("display","block");
		$("#timeSpentId").css("display","block");
		$("#timeSpentId").next("label").css("display","block");
	}
	function closeRequest()
	{
		var status = $("[name='statuschange'] option:selected").val();
		var request_id = $("#request_id").val();
		var fcr = ($("#fcr_checkbox").is(":checked"))?$("#fcr_checkbox").val():"";
		var fcr_gen_comment = $("#general_comment").val();
		var req_Ack;
		var req_closer = $("#request_closure").val();
		var req_closer_comment = $("#status_change_comment").val();
		if($("#requester_acknowledge1").is(":checked"))
		{
			req_Ack = $("#requester_acknowledge1").val();
		}
		if($("#requester_acknowledge2").is(":checked"))
		{
			req_Ack = $("#requester_acknowledge2").val();
		}
		
	 $.ajax({
		        url:"<?php echo base_url() ?>helpdesk/close_request_hd",
		        data:{"user_request_id":request_id,"fcr_chk":fcr,"fcr_gen_comment":fcr_gen_comment,"req_ack":req_Ack,"status":status,
		            "req_closer":req_closer,"req_closer_comment":req_closer_comment},
				type:'POST',
				dataType :'text',	
		        success:function(data){
			       //console.log(response);
				   var result = $.trim(data);
				   if(result ==="success"){
					    $("#myModal1").modal('hide');   
				   }
				   else{
					   alert("An error occured while saving");
				   }
						
				
		        },
		        error:function(response){
			        console.log(response);
		        }
		    });
		
	}
	function saveStatus()
	 {
		 
		 var comment = $("#comment").val();
		 var status = $("#status").val();
		 var user_code = $("#requester_user_code").val();
		 var prev_status = $("#prev_status").val();
		 if(comment == ''){
			 $("#myModal").find("#message_err").html("Please enter Your Comment...");
			 return false;
		 }
		 $.ajax({
			 url: "<?php echo base_url() ?>helpdesk/statusChange",
			 type:"post",
			 dataType:"text",
			 data:{"status":status,"request_id":request_id,"group":groupId,"technician":technicianId,"comment":comment,"requester_user_code":user_code,"prev_status":prev_status},
			  success:function(response)
			  {
				$('#myModal').modal('hide');	   
				  
			  },
			  error:function()
			  {
				  
			  }
		 })
	 }
	function add_notes()
	{
		//user_request_id ,notes,
		//action save_notes
		var notes = $("#notes").val();
		var user_request_id = "<?php echo $request_details->user_request_id; ?>";
		if(notes == ""){
			alert("Empty notes cannot be added.");
			return false;
		}
		else{
			$.ajax({
				url:"<?php echo base_url(); ?>helpdesk/save_notes",
				data:{"user_request_id":user_request_id,"notes":notes},
				type:"POST",
				dataType:'text',
				success:function(response){
					
					var res = $.trim(response);
					if(res == 'success')
					{
						alert("Notes Saved successfully");
						$('#myModalNotes').modal('hide');
						$("#notes").val('');
					}
					else
					{
						alert("notes failed to save");
					}
				}
				
			});
		}
	}
	/*document.getElementById("add_att").addEventListener("click", function(event){
		    var attachment = document.getElementById("request_attach").value;
			if(attachment == ""){
				alert("Please select a file and click Attach.");
				event.preventDefault();
			    return false;
			}
                
            });*/
	function checkAttachment()
	{	var flag = false;
		var attachment = document.getElementById("request_attach").value;
		if(attachment == undefined)
		{
			alert("Please select a file and click Attach.");
			flag =false;
		}
		else
		{
			flag =true;
		}
		
		return flag;
	}
	$(document).ready(function(){
		// on the click of resolution tab  
		$("a[href='#tab_default_1']").on('click',function(){
			
			$("#resoln_msg").css("display","block");
			$("#resolution_frm").css("display","none");
		    $("#timeSpentId").css("display","none");
		    $("#timeSpentId").next("label").css("display","none");
			

		});
		//resolution ST
		var localStorageVal = 0;
		localStorageVal = localStorage.getItem('resolution_failed');
		
		if(localStorageVal > 0){
			$("#tab1").parent(0).removeClass('active');
			$("#tab1").attr('aria-expanded','false');
			$("#tab1success").removeClass('active in');
			
			$("#tab2").parent(0).addClass('active');
			$("#tab2").attr('aria-expanded','true');
			$("#tab2success").addClass('active in');
			localStorage.setItem('resolution_failed',0);
		}
		
		//NEW* Solution details 
		$(".request_solution").on("click",function(){
			var solnId = $(this).attr("data-solutionid");
			var formUrl = "<?php echo base_url(); ?>helpdesk/get_solution_details";
			$.post(formUrl,{solnId:solnId},function(r){
				console.log(r);
				var obj = JSON.parse(r);
				$(".modal-dialog.modal-lg").html('');
				htmlTemp = ' <div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header bckColor"><button type="button" class="close" data-dismiss="modal">&times;</button><h5 class="modal-title">Solution Details</h5></div><div class="modal-body"><b>Category</b>: '+obj.category_name+' <br><b>Title</b> : '+obj.solution_title+' <br> <b>Content</b> : <br>'+obj.solution_desc+'</div><div class="modal-footer bckColor"><h5 class="modal-title pull-left">Applied By '+obj.creator_name+' on '+obj.solution_created+'</h5></div></div></div>';
				$(".modal-dialog.modal-lg").html(htmlTemp);
				$("#solution_details").modal('show');
			});
		});
		
		//NEW* Save resolution
		$("#save_resolution").on("click",function(){
			var formUrl = "<?php echo base_url();?>helpdesk/admin_ajxresolution";
			var dataval = $("#resolution_frm").serialize()+'&addresolution=1';
			$.post(formUrl, dataval, function(r){
				var obj = JSON.parse(r);
				console.log(obj);
				if(obj.status == 'success'){
					window.location = obj.redirect_url;
					localStorage.setItem('resolution_failed',1);
				}
			});
			return false;
		});
		//NEW* resolution
		$("#add_to_solution").on("click",function(){
			var resolution_val = $("#editor").val();
			if(resolution_val != ''){
				var formURL = "<?php echo base_url();?>helpdesk/admin_ajxresolution";
				$.post(formURL, $("#resolution_frm").serialize(), function(r){
					var obj = JSON.parse(r);
					console.log(obj);
					if(obj.status == 'success'){
						window.location = obj.redirect_url;
					}
					if(obj.status == 'failed'){
						alert("Error occured on resolution please try again");
						localStorage.setItem('resolution_failed',1);
						window.location.reload();
					}
				});
			}else{
				alert("Resolution should not be empty");
			}
			return false;
		});
		
		$('#res_start_time').datetimepicker({ 
			format: 'MMM D,YYYY hh:mm:ss A',
			useCurrent:true,
			minDate: create_date
		});
		
		$('#res_end_time').datetimepicker({
			format : 'MMM D,YYYY hh:mm:ss A',
			useCurrent:true
		});
		//resolution END
		
		$(".material-icons").on('click',function(){
			$(this).parent().attr("aria-expanded",true);$(this).parent().parent().addClass("open");
			
		});
		$("#timeSpentId").on('click',function(){
			if($(this).prop('checked')){
				$("#logpage").show();
			}else{
				$("#logpage").hide();
			}
		});
		
		
		
		$("#message_err").html("");
		
		$('#myModal').on('hidden.bs.modal', function () {
			$("#message_err").html("");
       });
		request_id    = $("#request_id").val();
		dueby_date    = $("#dueby_date").val();
		groupId       = $("#group").val();
        technicianId  =	$("#technician").val(); 	
		
		var create_date = $("#create_date").val();
		var dueby_date = $("#dueby_date").val();
		
		$('#dueby_date').datetimepicker({ 
		format: 'MMM D,YYYY hh:mm:ss A',
		useCurrent:true,
		minDate: create_date });
			
		if($('#response_dueby').val()!=''){
			$('#response_dueby').datetimepicker({ 
			format: 'MMM D,YYYY hh:mm:ss A',
			useCurrent:true,
			minDate: create_date,
			maxDate: dueby_date,
			}); 
		}
		$("#dueby_date").on("dp.change", function (e) {
			
        $('#response_dueby').data("DateTimePicker").maxDate(e.date);
       });   
    	
		//$('#dueby_date').bootstrapMaterialDatePicker({ format : 'MMMM DD, YYYY - HH:mm:ss' });
		
		 
		$("select[name='statuschange'],select[name='status']").change(function(){
			statusVal = $(this).val();
			/*if(statusVal == 'open' || statusVal == ''){
			}else{
				$("#myModal").modal('show');
			}*/
			if(statusVal == 'open' || statusVal == ''){
            }else if (statusVal == 'close') { 
			
                $("#myModal1").modal('show');
            }else{
				
                $("#myModal").modal('show');
            }
		});
		
        $(".closer").on("click",function(){
            console.log('val get '+statusVal);
            var confirmStatus = confirm("Status comment is mandatory, cancelling this operation will set previous request status.");
            if(confirmStatus && statusVal !='open'){
                $("select[name='statuschange']").val(statusValPrevious);
                $("#myModal1").modal('hide');
            }
            return confirmStatus;
        });
		
		
		$("#tab1").click(function(){
			$(this).attr('aria-expanded',true);
			$(this).find('li').addClass('active');
            //alert("hello");
			$("#tab4success").addClass('active');
            //$("#tab5success").addClass('active in');

			$("#tab2").attr('aria-expanded',false);
			$("#tab2").find('li').removeClass('active');
		});
		
		$("#tab21").click(function(){
			$("#tab1").attr('aria-expanded',false);
			$("#tab1").parent('li').removeClass('active');
			
			$("#tab2").attr('aria-expanded',true);
			$("#tab2").parent('li').addClass('active');
			
			$("#tab2success").addClass('active in');
			$("#tab1success").removeClass('active in');
		});
		
		
		
		
		/*var myTable = 
		$('#dynamic-table').DataTable( {
			bAutoWidth: true,
			"aoColumns": [null, null,null,null, null,null,null, null,null,null],
			"aaSorting": [],
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
			//"bProcessing": true,
			//"bServerSide": true,
			//"sAjaxSource": "http://127.0.0.1/table.php"	,

			//,
			//"sScrollY": "200px",
			//"bPaginate": false,

			//"sScrollX": "100%",
			//"sScrollXInner": "120%",
			"bScrollCollapse": true,
			//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
			//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
			//"iDisplayLength": 50
			select: {
				style: 'multi'
			}
		});*/
        /****request edit button click code starts here****/
        $("#request_edit").on("click",function(){
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().prev().css("display","none");
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().css("display","block"); 
           
           // button set visible here 
           $("#update_request,#reset_request,#cancel_request").css("display","inline-block");

        });
		/****requester edit button click code starts here****/
        $("#requester_edit").on("click",function(){

            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().prev().css("display","none");
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().css("display","block"); 
         // button  set visible here
         $("#update_requester,#reset_requester,#cancel_requester").css("display","inline-block");

        });
        
        /***cancel request*******/
        $("#cancel_request").on("click",function(){
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().prev().css("display","block");
            $("#status,#work_Station,#mode,#priority,#level,#category,#site,#subactegory,#group,#item,#technician,#service_category,#assets,#create_date,#dueby_date,#response_dueby,#dept").parent().css("display","none"); 
            // button  set hidden here
         $("#update_request,#reset_request,#cancel_request").css("display","none");
        });
        /***cancel requester*******/
        $("#cancel_requester").on("click",function(){
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().prev().css("display","block");
            $("#requester_name,#email_address,#contact_no,#mobile_no,#depts,#business_impact").parent().css("display","none"); 
            // button  set hidden here
         $("#update_requester,#reset_requester,#cancel_requester").css("display","none");
        });
		/*****group change***********/
		$("#group").on("change",function(){
            var group_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/get_technician",
				type: "post",
				dataType:"json",
				data:{group_id:group_id },
				success:function(response){
					
					var str_technician = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_technician += "<option>"+response[i].tech_name+"</option>";
					}
					$("#technician").html(str_technician);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
		
		/*******department change**********************/
		$("#dept").on("change",function(){
            var dept_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/getCategoryByDeptId", <!--change the name of controller action-->
				type: "post",
				dataType:"json",
				data:{dept_id:dept_id },
				success:function(response){
					
					var str_categories = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_categories += "<option value='"+response[i].category_id +"'>"+response[i].category_name+"</option>";
					}
					$("#category").html(str_categories);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
		/*******category change**********************/
		$("#category").on("change",function(){
            var category_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/getSubCategoryByCategoryId", <!--change the name of controller action-->
				type: "post",
				dataType:"json",
				data:{category_id:category_id },
				success:function(response){
					
					var str_subcategories = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_subcategories += "<option value='"+response[i].sub_category_id +"'>"+response[i].sub_category_name+"</option>";
					}
					$("#subactegory").html(str_subcategories);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        });
		
		/*******sub category change**********************/
		$("#subactegory").on("change",function(){
            var sub_category_id = $(this).val();
			$.ajax({
				url: "<?php echo base_url();  ?>helpdesk/getItemBySubcategoryId", <!--change the name of controller action-->
				type: "post",
				dataType:"json",
				data:{sub_category_id:sub_category_id },
				success:function(response){
					
					var str_items = "<option>select</option>";
					for(var i=0;i<response.length;i++)
					{
						str_items += "<option value='"+response[i].items_id +"'>"+response[i].item_name+"</option>";
					}
					$("#item").html(str_items);
				},
				error:function(response){
					alert("error occured");
				}
			}); 
        }); 
		/********panel settings on click and on hover*******************/
		$(".panel-group > .panel.panel-default").on('mouseover',function(){
		   $(this).removeClass('panel-default').css('background-color',' rgba(28, 107, 148, 0.22)');
		   
		});
        $(".panel-group > .panel.panel-default").on('mouseout',function(){
          $(this).addClass('panel-default').css('background-color','');
        });
		
		$(".panel-group > .panel.panel-default").on('click',function(){
			
		   //$(this).find('.panel-heading').css('background-color',' rgba(28, 107, 148, 0.22)');
		   $(this).find('.panel-heading').next().css('background-color','#fff');
		}); 
		
	});
	
	CKEDITOR.replace( 'editor', {
		plugins: 'wysiwygarea,basicstyles,toolbar,undo',
		on: {
			instanceReady: function() {
				// Show textarea for dev purposes.
				//this.element.show();
			},
			change: function() {
				// Sync textarea.
				this.updateElement();    
				// Fire keyup on <textarea> here?
			}
		}
	});
	
	
</script>
</body>
</html>


