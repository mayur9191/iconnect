<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	     table tbody tr td:nth-child(3) a.description .tooltiptext {
			display: none;
			width: 430px;
			background-color:#ccc
			color:black;
			text-align: left;
			border-radius: 6px;
			padding: 5px 0;
			position: absolute;
			z-index: 1;
			border:1px solid black;
			
			
		}
		table tbody tr td:nth-child(3) a.description:hover .tooltiptext {
			display: block;
			color:black;
			padding-left:12px;
			word-wrap: break-word;
		}
		table tbody tr td:nth-child(3) a.description{
			color:#333;
		}
		table tbody tr td:nth-child(3) a.description:hover{
			color:#337ab7;
		}
		span.tooltiptext ul li{
			list-style-type:none;
		} 
		.square {
		float: left;
		width: 10px;
		height: 10px;
		margin: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		}

		.Normal {
		  background: #006600;
		}

		.Low {
		  background: #e4af6d;
		}
		
		.Medium {
		  background: #ff6600;
		}

		.High {
		  background: #ff0000;
		}
		#dynamic-table{
			width:100% !important;
			font-size:12px !important;
		}
		#tableEditor {
			position: absolute;
			left: 20px; top: 20px;
			padding: 5px;
			border: 1px solid #000;
			background: #fff;
        }
		.dropdown-menu:before {
		  position: absolute;
		  top: -7px;
		  left: 9px;
		  display: inline-block;
		  border-right: 7px solid transparent;
		  border-bottom: 7px solid #ccc;
		  border-left: 7px solid transparent;
		  border-bottom-color: rgba(0, 0, 0, 0.2);
		  content: '';
		}

		.dropdown-menu:after {
		  position: absolute;
		  top: -6px;
		  left: 10px;
		  display: inline-block;
		  border-right: 6px solid transparent;
		  border-bottom: 6px solid #ffffff;
		  border-left: 6px solid transparent;
		  content: '';
		}
	</style>
</head>
<body>

<div class="tab-content">
	<div class="tab-pane fade in active" id="tab2success">
		<div class="row inbox">
			<div class="container-fluid col-md-12" style="background-color: white;">
		<!--<div class="page-header">
		</div>-->
		<h4>My Requests</h4>
		<?php //echo "<pre>";print_r($column_list);
		   $header_count = count($column_list); ?>
		   
		<div id="table_column" class="dropdown pull-right" style="padding-right:18px;">
				<a data-toggle="dropdown" href="#">
				 <span  aria-hidden="true">Show/Hide</span>
                </a>
			<?php   $table_header = array('Sr No','ID','Subject','Requester Name','Technician',
			'Status','Created date','Site','Priority','Group');
					$i=0;?>
					<ul class="dropdown-menu table_head" role="menu" aria-labelledby="dLabel" style="padding:5px 5px 5px 5px;height:auto;width:18%;">
					<a href="#" style="padding:3px 3px 3px 3px;border:1px solid black;" class="close" data-dismiss="alert">&times;</a>
					<h5><b>Show/Hide Columns</b></h5>
					
					<?php for($i=0;$i<count($table_header);$i++){ ?>
					<?php if(in_array($table_header[$i],$column_list))
					      {
							$checked = "checked";  
						  }
						  else
						  {
							$checked = "";  
						  }
						  ?>		
				        <li><input type="checkbox" data-title="<?php echo $table_header[$i]; ?>" value="<?php echo $i;?>" <?php echo $checked; ?> name="table_header[<?php echo $i;?>]"><?php echo $table_header[$i]; ?>
						</li>
				    
					<?php   }   ?>
				        <li>
					        <input type="button" id="save_header" name="save_header" value="Save">
				            <input type="button" id="cancel_header" name="cancel_header" value="Cancel">
					    </li>
				    </ul>
		</div>
		<div class="col-xs-12" style="font-size: 10px;font-family: inherit;">
		<!--<a href="edit" id=edit>Edit table</a>-->
			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
				<thead>
				   
					<tr style="font-weight:bold;font-size:12px !important;">
					<?php echo (in_array("Sr No",$column_list))? '<th>Sr No</th>': ''?>
					<?php echo(in_array("ID",$column_list))?'<th>ID</th>':'' ?>
					<?php echo(in_array("Subject",$column_list))?'<th>Subject</th>':''  ?>	
					<?php echo (in_array("Requester Name",$column_list))? '<th>Requester Name</th>':'' ?>
					<?php echo (in_array("Technician",$column_list))? '<th>Technician</th>':'' ?>
					<?php echo (in_array("Status",$column_list))? '<th style="width:127px;">Status </th>':'' ?>	
					<?php echo (in_array("Created date",$column_list))? '<th>Created date</th>':'' ?>		
					<?php echo (in_array("Site",$column_list))? '<th>Site</th>':'' ?>	
						<!--<th>Department</th>
						<th>Category</th>
						<th>Sub-category</th>
						<th>Item</th>-->
					<?php echo (in_array("Priority",$column_list))? '<th>Priority</th>':'' ?>	
					<?php echo (in_array("Group",$column_list))? '<th style="width:104px">Group</th>':'' ?>	
					</tr>
				</thead>

				<tbody>
				<?php $groups = get_groups(); ?>
				<?php $group_sr ='<option  value="0">Not Assigned</option>'; ?>
				<?php foreach($groups as $group): ?>
				<?php $group_sr .= '<option   value="'. $group->group_id.'">'.  $group->group_name .'</option>'; ?>				
			    <?php endforeach; ?>
				
				<?php $i=1; foreach($assign_tech_list as $reportRecord){ 
					$dept_data = get_record_by_id_code('dept',$reportRecord->dept_id);
					$category_data = get_record_by_id_code('category',$reportRecord->category_id);
					$subcategory_data = get_record_by_id_code('subcategory',$reportRecord->subcategory_id);
					$item_data = get_record_by_id_code('item',$reportRecord->item_id);
				?>
					<tr>
						<?php echo (in_array("Sr No",$column_list))? '<td>'.$i.'</td>': '<td style="display:none;"></td>'?>
						<?php echo(in_array("ID",$column_list))?'<td>'.'SK00'.$reportRecord->ur_user_request_id.'</td>':'' ?>
						<?php echo(in_array("Subject",$column_list))? '<td><a class="description"  href="'.base_url().'helpdesk/requestdetail/'.$reportRecord->ur_user_request_id.'">'.ucfirst($reportRecord->request_subject).'<span class="tooltiptext" style="background-color:#f5f5f5;">
						<strong>Request ID : </strong>&nbsp;'.'SK00'.$reportRecord->ur_user_request_id.'<br>'.
						'<strong>Category : </strong>&nbsp;'.ucfirst($category_data->category_name).'<br>'.
						'<strong>Subject : </strong>&nbsp;'.ucfirst($reportRecord->request_subject).'<br>'.
						'<strong>Description : </strong>&nbsp;'.ucfirst($reportRecord->request_desc).'</span></a></td>':''  ?>
						
						<!--<td>
						    <a class="description"  href="#"><?php echo ucfirst($reportRecord->request_subject);  ?>
						        <span class="tooltiptext" style="background-color:#f5f5f5;">
									<strong>Request ID : </strong>&nbsp;<?php echo 'SK00'.$reportRecord->ur_user_request_id; ?><br>
									<strong>Category : </strong>&nbsp;<?php echo ucfirst($category_data->category_name);  ?><br>
									<strong>Subject : </strong>&nbsp;<?php echo ucfirst($reportRecord->request_subject);  ?><br>
									<strong>Description : </strong>&nbsp;<?php echo ucfirst($reportRecord->request_desc);  ?>
									
						        </span>
						    </a>
						</td>-->
						<?php echo (in_array("Requester Name",$column_list))? '<td>'.ucfirst($reportRecord->requester_name).'</td>':'' ?>
						<!--<td><?php echo ucfirst($reportRecord->requester_name);  ?></td>-->
						<?php $tec = ($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : 'Not assign'; ?>
						<?php echo (in_array("Technician",$column_list))? '<td>'.$tec.'</td>':'' ?>
						<!--<td>
						<?php //echo ($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : 'Not assign';  ?>
						
						</td>-->
					  <?php if(in_array("Status",$column_list)){ ?>	
						<td style="width:127px;"><?php 
								if($reportRecord->request_assign_person_id != NULL){
									if($reportRecord->is_request_completed == 1){  
										echo '<span style="background-color:green;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;</span>';
									}else{
										echo '<span style="background-color:red;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Not completed&nbsp;&nbsp;&nbsp;</span>';
									}
								}else{
									echo '<span style="background-color:orange;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Not assign&nbsp;&nbsp;&nbsp;</span>';
								}
						?></td>
					  <?php } ?>
					  <?php echo (in_array("Created date",$column_list))? '<td style="width:81px;">'.date('d-m-Y',strtotime($reportRecord->request_created_dt)).'</td>':'' ?>
						<!--<td style="width:81px;"><?= date('d-m-Y',strtotime($reportRecord->request_created_dt)); ?></td>-->
						<?php echo (in_array("Site",$column_list))?'<td>'.ucfirst($reportRecord->requester_location_name).'</td>':'' ?>
						<!--<td><?php echo ucfirst($reportRecord->requester_location_name);  ?></td>-->
						<!--<td><?php echo ucfirst($dept_data->dept_name);  ?></td>
						<td><?php echo ucfirst($category_data->category_name);  ?></td>
						<td><?php echo ucfirst($subcategory_data->sub_category_name);  ?></td>
						<td><?php echo ucfirst($item_data->item_name);  ?></td>-->
						<?php echo (in_array("Priority",$column_list))? '<td><span class="square '.ucfirst($reportRecord->request_priority).'"></span>'.ucfirst($reportRecord->request_priority).'</td>':'' ?>
						<!--<td><span class="square <?php echo ucfirst($reportRecord->request_priority);  ?>"></span><?php echo ucfirst($reportRecord->request_priority);  ?></td>-->
						<?php $grp = ($reportRecord->group_name !=NULL)? ucfirst($reportRecord->group_name) : 'Not assign'; ?>
						<?php echo (in_array("Group",$column_list))? '<td style="width:104px"><div class="dropdown">'.$grp.'</div></td>':'' ?>
						<!--<td style="width:104px">
							<div class="dropdown">	
								<?php //echo ($reportRecord->group_name !=NULL)? ucfirst($reportRecord->group_name) : 'Not assign';  ?> 
								
								
							</div>
						</td>-->
						
						
						
						
					</tr>
				<?php $i++; }  ?>
			</tbody>
			
			</table>
			
			<span id="printTxt" style="position:absolute;" >
				<div id="request_gp" class="dropdown-menu" style="padding: 10px; padding-bottom: 0px; position:absolute;" >
					 <span><b>Assigned Technician</b></span>
					<button type="button" class="close pop" aria-label="Close">
						<span style="font-size:18px;" aria-hidden="true">&times;</span>
					</button>
					<form method="post" class="form-horizontal">
						<div class="form-group">
							<label for="sel1" class="col-sm-6">Group:</label>
							<div class="col-sm-8">
							
							
								<select class="form-control" style="width:172px"  name="groups" id="groups">
									
								<?php echo  $group_sr; ?>	
									
								</select>
							</div>
						</div>
						<p></p>
						<div class="form-group">
							<label for="sel1" class="col-sm-6">Technician:</label>
							<div class="col-sm-8">
								<select class="form-control" style="width:172px"  name="technicians" id="technicians">
									<option> --- Choose --- </option>
								</select>
							</div>

						</div>
						<p></p>
						<div class="form-group col-md-12">
							<button id="button1id" type="submit" class="btn btn-primary col-sm-6">Assign</button>
							<button id="button2id" type="button" class="btn btn-default pop">Cancel</button>
						</div>
						
					</form>	
				</div>
			</span>
			
		</div>
	</div> <!-- ./container -->
	
		</div><!--/.col-->		
	</div>
</div>
<!-- Include jQuery -->
  <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>

  <!-- Include jQuery Popup Overlay -->
  <script src="http://vast-eng.github.io/jquery-popup-overlay/jquery.popupoverlay.js"></script>
<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script>

/* function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coor = "X coords: " + x + ", Y coords: " + y;
	$("#printTxt").css({'top':y,'left':x,'display':'block'}).show(); 
}
 */
function get_technician(group_id,tech_id)
{
	        $.ajax({
				url : "<?php echo base_url() ?>"+ "helpdesk/get_technician",
				type:"POST",
				data: {group_id:group_id},
				success:function(response){
					var str = '<option value="0"> --- Choose --- </option>';
					var obj  =  JSON.parse(response);
					var selectop = '';
					$(obj).each(function(index,value){
						str += '<option id="'+obj[index].tech_id+'" value="'+obj[index].tech_id+'">'+obj[index].tech_name+'</option>';	
					});
					
					$("#technicians").html(str);
					document.getElementById(tech_id).selected = "true";
				},
				error:function(response){
					alert("error occured"+response)
				}
			});
	
}

$(document).ready(function(){
	
	var count_header = "<?php echo $header_count; ?>";
	var columns_to_null = new Array();
	var i;
	for(i=0;i<count_header;i++){
		columns_to_null[i] = null;
	}
	
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": columns_to_null,
		"aaSorting": [],
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
	console.log('columns null count '+columns_to_null);
	   //var table = $('#dynamic-table').DataTable();
	   //table.columns(column_header).visible(true );
	   
	$("#save_header").click(function(){
	    var table = $('#dynamic-table').DataTable();
		var numberOfChecked = $("input:checkbox:checked").length;
		//alert(numberOfChecked);
		var columns = [];
		if(numberOfChecked > 0){
			$('input[type=checkbox]').each( function () {
			if(this.checked){
				
				    var col = $(this).val();
				   // table.column(col).visible( true );
					columns.push($(this).data('title'));
					//columns.push(col);
			    }
				else{
					var col = $(this).val();
				  //  table.column(col).visible( false );
	
				}
		      		
	        });
			//alert(columns);return;
			// ajax code to save the columns chosen by user begins from here
			$.ajax({
				  
				url:"<?php echo base_url() ?>helpdesk/save_columns",
				type:"POST",
				data:{columns_list:columns.join()},
				success:function(){
					location.reload();
				},
				error:function(){
					
				}
			});
		}
		else{
			alert("Please check the columns");
			return false;
		}
		
		
        
	});
  // Hide a column
  //table.column( 1 ).visible( false );
	//Handles menu drop down
    $(".dropdown-menu.table_head").find("input[type='checkbox']").on("click",function(e){
		e.stopPropagation();
	});
	$(".dropdown-menu.table_head").find("input[type='button']").on("click",function(e){
		e.stopPropagation();
	});
	
	$("#cancel_header").on("click",function(e){
			
		$("#table_column").removeClass("open");
		$("#table_column").find("a").attr("aria-expanded",false);
	});
	$('#dynamic-table').on('click',' tbody >tr>td>.dropdown>a.dropdown-toggle.ab',function(e){
		//alert(($("#printTxt").position().left));
		
		var positionX;
		var positionY;
		var group_id = $(this).attr('data-gid');
		var tech_id  = $(this).attr('data-tid');
		positionX = e.clientX-30 + 'px',
		positionY = $(this).offset().top-110+'px';
		$("#groups").val('');
		$("#groups").val(group_id);
		//alert($(this).offset().top);
		//$("#request_gp").css({'top':positionY,'left':'75%','display':'block'}).show();
		//$("#request_gp").css({'top':e.clientY,'left':e.clientX,'display':'block'}).show();
		$("#printTxt").css({'top':positionY,'left':positionX,'display':'block'});
		// code to select technician
		get_technician(group_id,tech_id);
		$("#request_gp").show();
		
		
		///code to select group 
		/*
		$("#groups option").each(function(index){
			if(index == group_id){
				$(this).attr("selected","selected");
				
			}
			else{
				$(this).removeAttr("selected");
			}
			
				
			
		});*/
		
		
		
		
		
		
	});
	
	
	 // on menu selection
	 $( ".dropdown-menu" ).draggable();
	 
	 //on group selection
	$("#groups").on("change",function(){
		var group_id = $(this).find("option:selected").val();
		if(group_id == "")
		{
			alert("please select the correct group");
			
		}
		else
		{
			get_technician(group_id,tech_id =0);
		}
		
	});
	
	$(".close.pop").on("click",function(){
		$("#request_gp").hide();
	});
	$(".btn.btn-default.pop").on("click",function(){
		$("#request_gp").hide();
	});
	/*****column hide and unhide begins from here *****/
	$('#edit').click(function() {
		
    var headers = $('#dynamic-table th').map(function() {
        var th =  $(this);
        return {
            text: th.text(),
            shown: th.css('display') != 'none'
        };
    });
    
    var h = ['<div id="tableEditor"><button id="done">Done</button><table><thead><tr>'];
    $.each(headers, function() {
        h.push('<th><input type=checkbox',
               (this.shown ? ' checked ' : ' '),
               '/> ',
               this.text,
               '</th>');
    });
    h.push('</tr></thead></table></div>');
	
    $('body').append(h.join(''));
    
    $('#done').click(function() {
        var showHeaders = $('#tableEditor input').map(function() { return this.checked; });
        $.each(showHeaders, function(i, show) {
            var cssIndex = i + 1;
            var tags = $('#dynamic-table th:nth-child(' + cssIndex + '), #dynamic-table td:nth-child(' + cssIndex + ')');
            if (show)
                tags.show();
            else
                tags.hide();
        });
        
        $('#tableEditor').remove();
        return false;
    });
    
    return false;
});
	/*****column hide and unhide ends here *****/
	
});
</script>