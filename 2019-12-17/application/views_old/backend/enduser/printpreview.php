<!DOCTYPE html>
<html lang="en">
<head>
  <title>Print Customizer</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style1.css"  />
  <style>
  label{
	  font-size:12px;
  }
  .request{
	  font:bold 12px Arial, Verdana, Helvetica, sans-serif;color:#3665a9
  }
  </style>
</head>
<body>
 
<div class="container">
  <h6 style="text-align:center;">Print Customizer</h6>
  <?php //echo "<pre>";print_r($request_more_details);?>
  <?php $dept = get_record_by_id_code('dept',$request_more_details->dept_id);  ?>
  <?php $technician = get_record_by_id_code('technician',$request_more_details->technician_id);  ?>
  <?php $group = get_record_by_id_code('group',$request_more_details->group_id);  ?>
  <div class="panel panel-primary" id="custom_print_head">
    <div class="panel-heading">Select the required information</div>
    <div class="panel-body">
	  <div class="col-md-12">
	       <div class="col-md-3">
				
				<input type="checkbox" onclick="panel_toggle(this,'request_head','request_body');" name="request_details" id="request_details" checked value="">
				<label>Request Details</label>
		        
				
		   </div>
		   <div class="col-md-3">
				<input type="checkbox" onclick="panel_toggle(this,'requester_head','requester_body');" checked value="" name="requester_details" id="requester_details">
				<label>Requester Details</label>
		        
		   </div>
		   <div class="col-md-3">
				<input type="checkbox" onclick="panel_toggle(this,'resolution_head','resolution_body');" checked value="" name="resolution" id="resolution"> 
				<label>Resolution</label>
		        
		   
		   </div>
		   <div class="col-md-3">
			   
		       <input type="checkbox" onclick="panel_toggle(this,'conversation_head','conversation_body');" checked value="" name="conversations" id="conversations">
				<label>Conversations</label>
		   </div>
	  </div>
	  <div class="col-md-12">
	       <div class="col-md-3">
				
		       <input type="checkbox" onclick="panel_toggle(this,'notes_head','notes_body');" checked value="" name="notes" id="notes">
			   <label>Notes</label>
		   </div>
		   <div class="col-md-3">
				
		       <input type="checkbox" onclick="panel_toggle(this,'history_head','history_body');" checked value="" id="history" name="history">
			   <label>History</label>
		   </div>
	  </div>
	  <div class="col-md-12">
	       <div class="col-md-6">
				
		       <!--<button id="print_button" class="btn btn-default pull-right">Print</button>-->
			   <div id="print_button" class="btn btn-default pull-right">Print</div>
			   
		   </div>
		   <div class="col-md-6">
			   <button  onclick="custom_close();" class="btn btn-default pull-left">Cancel</button>
		   </div>
				
		       
			   
	
	  </div>
	</div>
  </div>
  <div id="custom_print_body" class="PrintArea">
  <span class="request">Print Preview</span>
  <hr/>
		<div class="pull-right">
			<table style="background-color:#cadb2b;color:#1c6b94;border-radius:6px;margin:5px;" class="right-status">
				<tbody>
					<tr>
						<td>Status </td>
						<td>
							<b>:<div id="status_PH" style="display:inline" class="fontBlackBold"><?php echo ucfirst($request_more_details->task_status); ?></div></b>
						</td>
					</tr>
					
					<tr>
						<td>Priority</td>
						<td><b><div id="priority_PH">: <?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></div></b></td>
					</tr>
					
				</tbody>
			</table>
	  </div>
         <p>
		   <img src="http://demo.servicedeskplusmsp.com/images/service-request-icon.png" />
		<b>
		  <?php echo ucfirst(strip_tags($request_details->request_subject)); ?>
		</b>
		</p>
		<p>
		<b style="font: normal 12px Arial, Helvetica, sans-serif;">By <?php echo  $request_details->requester_name; ?>
		</b>
		<span style="font: normal 12px Arial, Helvetica, sans-serif;">on  <?php echo convert_std_time($request_details->request_created_dt);  ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Due Date :</b><span
		style="font: normal 12px Arial, Helvetica, sans-serif;"><?php $duedate = date('M d ,Y h:i:s A',strtotime('+2 hours',strtotime($request_details->request_created_dt))); echo $duedate; ?> </span>
  </p>
  <div class="panel panel-primary">
    <div class="panel-heading">Description</div>
    <div class="panel-body">
	<!--panel for the ticket description begins from here-->
		<p>
		<?php echo $request_details->request_desc; ?><br/>
		<?php $attachments = (get_attachment($request_details->user_request_id));?>
		<?php
		//echo $request_details->user_code;
		 $email = get_email_by_emp_code($request_details->user_code); 
		 
		if(!empty($email)) {
			$user_details = get_all_details_by_emp_code($request_details->user_code);
			//print_r($user_details);
		}
		
		if(!empty($attachments)) {
			if(!empty($user_details)){
				$u_id = $user_details->u_id;
				
			}?>
			Attachments:
			<?php 
			echo '<ul style="position:relative;margin-right:70px;list-style-type:none;">';
			foreach($attachments as $attach){
				
				$filesize_byte = (filesize($_SERVER['DOCUMENT_ROOT'].'/uploads/helpdesk/'.$u_id.'/'.$attach->attachment_name));
				$filesize = $filesize_byte/1024;
				$size=$filesize;
				$download_link =base_url().'uploads/helpdesk/'.$u_id.'/'.$attach->attachment_name; 														
				echo '<li style="color:black;"><img src="'.base_url().'/assets/images/attach.png">'.' '.$attach->attachment_name.' ('.round($size,2).' Kb)'."<br/></li>";
				///echo '<img src="'.base_url().'/assets/images/attach.png">'.' '.$attach->attachment_name."<br/>";
				//' <a  href="'.base_url().'uploads/helpdesk/'.$u_id.'/'.$attach->attachment_name.'">'.'View'.'</a>'
			  }
			  echo '</ul>';
		}
		?>
		</p>
		
		
	<!-- panel ends-->
	
	</div>
  </div>
  <span id="conversation_head" class="request">Requester Conversations</span>
  <div id="conversation_body" class="panel panel-primary">
    <div class="panel-heading"></div>
    <div class="panel-body">
	 <!--panel for conversation begins -->
	    <div class="bs-example">
			<div class="panel-group" id="accordion">
			   
				<?php if(!empty($emails)){
						foreach($emails as $email){?>
					<!---code for each accordian starts from here---> 
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapse<?php echo $email->sytem_emails_id; ?>">
							<!--<i class="fa fa-unlock" style="margin-left:1.0em"></i>-->
							<i class="glyphicon glyphicon-envelope" style="margin-left:1.0em"></i>
							<span style="margin-left:1.0em;font-size:12px;">
							<b>System</b>
							</span>
						   <span style="margin-left:0.5em;font-size:12px;"> on <?php echo convert_std_time($email->created_date); ?>
						   </span>
						   </a>
						   </h4> 
						</div>
						<div id="collapse<?php echo $email->sytem_emails_id; ?>" class="panel-collapse ">
						<div class="panel-body" style="overflow-y:scroll;">
							<p>
							<b>To</b>:<?php echo $email->to_emails; ?></p>
							<p><b>Summary</b></p>
							<hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 0.1px;">
						   <p id="autosubj"><?php echo $email->subject ?></p>
						  <p><b>Description</b></p>
						 <hr style="border: 1px lightgray;max-width: 1024px;border-style:dashed;margin-top: 0.1px;">
						<div id="autodesc">
						<?php  echo $email->description;  ?>
						</div>
						
					</div>
				</div>
			</div>
			<!---code for each accordian ends here--->
		<?php }?>
	<?php } ?>
				
				
			</div>
										
        </div>
                                    
	 <!-- panel ends -->
	
	</div>
  </div>
  <span id="request_head" class="request">Request Details</span>
  <div id="request_body" class="panel panel-primary">
    <div class="panel-heading"></div>
    <div class="panel-body">
	<!--panel starts-->
	<div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2">
                                                    <b>Status</b>
                                                </div>
                                                <div class="col-xs-3">
                                                  <span><?php echo ucfirst($request_more_details->task_status); ?></span>
                                                    <span style="display:none;">
                                                        <select name="status" id="status" class="form-control">
                                                            <option>Select</option>
															<option <?php if($request_more_details->task_status=='open')  echo 'selected'; ?> value="open">Open</option>
															<option <?php if($request_more_details->task_status=='wip') echo 'selected';  ?> value="wip">WIP</option>
															<option <?php if($request_more_details->task_status=='hold')  echo 'selected'; ?> value="hold">On Hold</option>
                                                            <option <?php if($request_more_details->task_status=='close')  echo 'selected'; ?> value="close">Closed</option>
                                                            <option <?php if($request_more_details->task_status=='resolve')  echo 'selected'; ?> value="resolve">Resolved</option>
                                                            
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->task_status;  ?>" name="prev_status" id="prev_status" />
                                                    </span>
                                                </div>
                                                <div class="col-xs-2"><b>Workstation Number</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php echo $request_more_details->tech_workstation_no; ?></span>
                                                    <span style="display:none;"><input type="text" class="form-control" id="work_Station" name="work_Station"/>
													<input type="hidden" value="<?php echo $request_more_details->tech_workstation_no;  ?>" name="prev_workstation" id="prev_workstation" />
													</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2"><b>Mode</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php echo ucfirst($request_details->request_type); ?></span>
                                                    <span style="display:none;">
                                                        <select name="mode" id="mode" class="form-control">
                                                            <option>Select</option>
                                                            <!--<option>E-mail</option>
                                                            <option>Phone Call</option>
                                                            <option>Web Form</option>-->
                                                            <?php foreach($request_mode as $mode): ?>
                                                            <option <?php echo ($request_details->request_type==$mode->request_type)?'selected':''; ?> value="<?php echo $mode->request_type ?>"><?php echo ucfirst($mode->request_type); ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->request_type;  ?>" name="prev_mode" id="prev_mode" />
                                                    </span>
                                                </div>
                                                <div class="col-xs-2"><b>Department</b></div>
                                                <div class="col-xs-3">  
                                                    <span><?php  echo $dept->dept_name; ?></span>
                                                    <span style="display:none;">
                                                    <select name="dept" id="dept" class="form-control">
                                                        <option>Select</option>
                                                        <?php foreach ($depts_hd as  $value) {?>
                                                        <option <?php echo ($value->dept_id == $request_more_details->dept_id)?'selected':''; ?> value="<?php echo $value->dept_id; ?>"><?php echo $value->dept_name; ?></option>
                                                        <?php } ?>
                                                    </select>
													<input type="hidden" value="<?php echo $request_more_details->dept_id;  ?>" name="prev_dept" id="prev_dept" />
                                                    </span>              

                                                </div>
                                            </div>
                                        </div>                          
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                               
												<div class="col-xs-2"><b>Site</b></div>
                                                <div class="col-xs-3">
                                                    <?php //print_r($site_location_hd); ?>
                                                    <span>Pune</span>
                                                    <span style="display:none;">
                                                        <select name="site" id="site" class="form-control">
                                                            <option>Select</option>
                                                            <?php foreach($site_location_hd as $location): ?>
                                                            <option <?php echo ( $request_details->requester_location_id==$location->site_location_id)?'selected':''; ?> value="<?php echo $location->site_location_id; ?>"><?php echo $location->site_location_name;   ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->requester_location_id;  ?>" name="prev_site" id="prev_site" />
                                                    </span>
                                                </div>
                                                <div class="col-xs-2"><b>Priority</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php echo ($request_details->request_priority!='') ?$request_details->request_priority:''; ?></span>
                                                    <span style="display:none;">
                                                        <select name="priority" id="priority" class="form-control">
                                                            <option>Select</option>
                                                            <option <?php echo ($request_details->request_priority=="High") ?'selected':''; ?> value="High">High</option>
                                                            <option <?php echo ($request_details->request_priority=="Low") ?'selected':''; ?> value="Low">Low</option>
                                                            <option <?php echo ($request_details->request_priority=="Medium") ?'selected':''; ?> value="Medium">Medium</option>
                                                            <option <?php echo ($request_details->request_priority=="Normal") ?'selected':''; ?> value="Normal">Normal</option>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->request_priority;  ?>" name="prev_priority" id="prev_priority" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2"><b>Group</b></div>
                                                <div class="col-xs-3">
                                                
                                                    <span><?php echo $group->group_name ; ?>
													</span>
													
                                                    <span style="display:none;">
                                                        <select name="group" id="group" class="form-control">
														
                                                            <option>Select</option>
                                                            <?php foreach ($groups_hd as  $value) {?>
                                                            <option <?php echo ($value->group_id == $request_more_details->group_id)?'selected':''; ?> value="<?php echo $value->group_id; ?>"><?php echo $value->group_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->group_id;  ?>" name="prev_group" id="prev_group" />
                                                    </span>
                                                </div>
                                                <div class="col-xs-2"><b>Category</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php $category = get_record_by_id_code('category',$request_details->category_id);echo $category->category_name;  ?>
													</span>
                                                    <span style="display:none;"> 
                                                        <select name="category" id="category" class="form-control">
                                                            <option>Select</option>
                                                            <?php $categories = get_category();?>
                                                            <?php foreach ($categories as  $value) {?>
                                                            <option <?php echo ($request_details->category_id == $value->category_id )?'selected':''; ?> value="<?php echo $value->category_id; ?>"><?php echo $value->category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->category_id;  ?>" name="prev_category" id="prev_category" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2"><b>Technician</b></div>
                                                <div class="col-xs-3">
                                                   <?php  $emp_det = getEmpByEmail($technician->technician_email);  ?>
                                                    <span><?php  echo  $technician->tech_name; ?>
													
													</span>
                                                    <span style="display:none;">
                                                        <select name="technician" id="technician" class="form-control">
                                                            <option>Select</option>
                                                            <?php foreach($technicians_hd as $tech): ?>
                                                            <option <?php echo ($request_more_details->technician_id==$tech->tech_id)?'selected':''; ?> value="<?php echo $tech->tech_id;  ?>"><?php echo $tech->tech_name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_more_details->technician_id;  ?>" name="prev_technician" id="prev_technician" />
                                                    </span>
													
                                                </div>
                                                <div class="col-xs-2"><b>Subcategory</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php $subcategory = get_record_by_id_code('subcategory',$request_details->subcategory_id);echo $subcategory->sub_category_name;  ?></span>
                                                    <span style="display:none;">
                                                        <select name="subactegory" id="subactegory" class="form-control">
                                                            <option>Select</option>
                                                            <?php $subcategories = get_sub_category();?>
                                                            <?php foreach ($subcategories as  $value) {?>
                                                            <option <?php echo ($request_details->subcategory_id == $value->sub_category_id )?'selected':''; ?>  value="<?php echo $value->sub_category_id; ?>"><?php echo $value->sub_category_name; ?></option>
                                                            <?php } ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->subcategory_id;  ?>" name="prev_subcategory" id="prev_subcategory" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
											    <div class="col-xs-2"><b>SLA</b></div>
                                                <div class="col-xs-3">- </div>
                                                
                                                <div class="col-xs-2"><b>Item</b></div>
                                                <div class="col-xs-3">
                                                
                                                    <span><?php $item = get_record_by_id_code('item',$request_details->item_id);echo $item->item_name;  ?></span>
                                                    <span style="display:none;">
                                                        <?php $items = get_item();  ?>
                                                        <select name="item" id="item" class="form-control">
                                                            <option>Select</option>
                                                            
                                                            <?php foreach($items as $value): ?>
                                                            <option <?php echo ($request_details->item_id== $value->items_id) ? 'selected':''; ?> value="<?php echo $value->items_id; ?>"><?php echo $value->item_name; ?></option>
                                                           <?php endforeach; ?>
                                                        </select>
														<input type="hidden" value="<?php echo $request_details->item_id;  ?>" name="prev_item" id="prev_item" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                
												<div class="col-xs-2"><b>Template</b></div>
                                                <div class="col-xs-3">Default Request </div>
                                                <div class="col-xs-2"><b>Created By</b></div>
                                                <div class="col-xs-3"><?php echo $request_details->requester_name; ?></div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                            	<div class="col-xs-2"><b>Resolved Date</b></div>
                                                <div class="col-xs-3">- </div>
                                                <div class="col-xs-2"><b>Created Date</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php echo convert_std_time($request_details->request_created_dt); ?></span>
                                                    <span style="display:none;"><input class="form-control" type="text" readonly value="<?php echo convert_std_time($request_details->request_created_dt); ?>" id="create_date" name="create_date"/> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2"><b>Time Elapsed</b></div>
                                                <div class="col-xs-3">
												<?php 
													//$currentTime = strtotime(date('Y-m-d h:i:s'));
													//$systemtTime  = strtotime($request_more_details->request_assign_person_dt);

													//$timeelapsed = $currentTime - $systemtTime;
												   $timeelpase_str = " "; 
		                                           $timeelapsed = get_time_diff(date('Y-m-d H:i:s'),$request_more_details->request_assign_person_dt);
												   if($timeelapsed["days"]!='0'){
													    $timeelpase_str .= $timeelapsed["days"].'days'.' ';   
												   }
												   if($timeelapsed["hurs"]!='0'){
													    $timeelpase_str .= $timeelapsed["hurs"].'hrs'.' ';   
												   }
												   if($timeelapsed["mins"]!='0'){
													    $timeelpase_str .= $timeelapsed["mins"].'min'.' ';   
												   }
												   if($timeelapsed["secd"]!='0'){
													   // $timeelpase_str .= $timeelapsed["secd"].'s';   
												   }
													echo $timeelpase_str;
													

												?>          
												</div>
                                                <div class="col-xs-2"><b>DueBy Date</b></div>
                                                <div class="col-xs-3">
                                                    <span><?php echo $duedate; ?></span>
                                                    <span  style="display:none;" class="input-group date" ><input class="form-control " type="text" value="<?php echo $duedate;?>" id="dueby_date" name="dueby_date"/>
													<input type="hidden" value="<?php  echo $duedate;  ?>" name="prev_dueby_date" id="prev_dueby_date" />
													</span>
													
													
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin-top-10">
                                            <div class="col-xs-12">
                                                <div class="col-xs-2"><b>Last Update Time</b></div>
                                                <div class="col-xs-3">
												<?php echo ($request_details->response_date!='0000-00-00 00:00:00')? convert_std_time($request_details->last_modified_dt):''; ?>
												</div>
												<div class="col-xs-2"><b>Response DueBy Time</b></div>
                                                <div class="col-xs-3">
                                                    
                                                    <span><?php echo ($request_details->response_date!='0000-00-00 00:00:00')? convert_std_time($request_details->response_date):''; ?></span>
                                                    <span style="display:none;"><input class="form-control" type="text" value="<?php echo ($request_details->response_date!='0000-00-00 00:00:00')?convert_std_time($request_details->response_date):''; ?>" id="response_dueby" name="response_dueby"/>
                                                    <input type="hidden" value="<?php  echo ($request_details->response_date!='0000-00-00 00:00:00')?convert_std_time($request_details->response_date):'';  ?>" name="prev_res_dueby_date" id="prev_res_dueby_date" />
													</span>
                                                </div>
                                            </div>
                                        </div>
										<!--<div class="row margin-top-10">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6 pull-right">
												    <button type="submit" id="update_request" name="update_request">Update</button>
                                                    <button type="reset" id="reset_request" onClick="location.reload();" name="reset_request">Reset</button>
                                                    <button type="button" id="cancel_request" name="cancel_request">Cancel</button>
												
												
												</div>
                                                
                                            </div>
                                        </div>-->
                                            
                                    
	<!--panel ends here-->
	</div>
  </div>
  <span id="requester_head" class="request">Requester Details</span>
  <div id="requester_body" class="panel panel-primary">
    <div class="panel-heading"></div>
    <div class="panel-body">
	<!--panel starts here-->
	<div class="row margin-top-10">
		<div class="col-xs-12">
			<div class="col-xs-2"><b>Requester Name</b></div>
			<div class="col-xs-3">
				
				<span><?php echo $request_details->requester_name; ?></span>
				<span style="display:none;"><input type="text" class="form-control" value="<?php echo $request_details->requester_name; ?>" id="requester_name" name="requester_name"/> </span>
			</div>
			<div class="col-xs-2"><b>E-mail Address</b></div>
			<div class="col-xs-3">
				<?php $email = get_email_by_emp_code($request_details->user_code);?>
				<span><?php echo (!empty($email))? $email[0]->u_email:''; ?></span>
				<span style="display:none;"><input type="email" class="form-control" value="<?php echo (!empty($email))? $email[0]->u_email:''; ?>" id="email_address" name="email_address"/> </span>
			</div>
		</div>
	</div>
	<!--panel ends here-->
	</div>
  </div>
  <span id="notes_head" class="request">Discussion Notes
  <?php $notesData = get_user_request_notes($request_details->user_request_id); ?>
  <span  style="font: bold 14px Arial, Helvetica, sans-serif!important;color: #3466A9;" class="pull-right">[<?php echo count($notesData); ?> note(s)]</span></span>
  <div id="notes_body" class="panel panel-primary">
    <div class="panel-heading"></div>
    <div class="panel-body">
	<!--panel notes starts -->
	<div class="col-xs-12">
		
		<?php foreach($notesData as $noteRecord){?>
					
					<div class="col-xs-12" style="background-color:rgba(28, 107, 148, 0.22)">
					   <b>User:<?php echo  $request_details->requester_name; ?></b>
					   <span class="pull-right"><?php echo ($noteRecord->notes_added_date != NULL)? convert_std_time($noteRecord->notes_added_date):'';  ?>
					   </span><br/>
					   
					</div>
					<div class="col-xs-12"><?php  echo  $noteRecord->notes_description;    ?></div>   
					
				
		<?php  }
		?>
	</div>
	<!--panel ends-->
	</div>
  </div>
  <span id="resolution_head" class="request">Resolution</span>
  <div id="resolution_body" class="panel panel-primary">
    <div class="panel-heading"></div>
    <div class="panel-body">
		<span class="pull-right">
		<?php echo (isset($resolution_data->resolution_created_dt))? convert_std_time($resolution_data->resolution_created_dt) :''; ?>
		</span>
		<hr/>
		<span><?php echo (isset($resolution_data->response_desc))? $resolution_data->response_desc :''; ?></span>
	</div>
  </div>
  <span id="history_head" class="request">Request History</span>
  <div id="history_body" class="panel panel-primary">
    <div class="panel-heading"> Request History header</div>
    <div class="panel-body">
	<!--panel starts here-->
	<?php echo display_history($request_details->user_request_id); ?>
	<!-- panel ends here-->
	</div>
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script>
  function panel_toggle(obj,header_id,header_body)
  {
	  if(obj.checked)
	  {
		  $("#"+header_id).show();
		  $("#"+header_body).show();
	  }
	  else
	  {
		  $("#"+header_id).hide();
		  $("#"+header_body).hide();
	  }
  }
  
  function custom_close()
  {
	  window.close();
  }
  </script>
  <script>
   $(document).ready(function(){
	   <!-- settings for the print with PrintArea Plugin--> 
	   var options = { mode:"iframe",popHt: 500,popWd:800,popClose:true};
     $("div#print_button").click(function(){
          $("div.PrintArea").printArea(options );
      });

   });
  </script>
</body>
</html>
