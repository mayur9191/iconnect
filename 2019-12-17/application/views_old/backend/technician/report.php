<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
		
	     table tbody tr td:nth-child(3) a.description .tooltiptext {
			display: none;
			width: 430px;
			background-color:#ccc
			color:black;
			text-align: left;
			border-radius: 6px;
			padding: 8px 0;
			position: absolute;
			z-index: 1;
			border:1px solid black;
			
		}
		table tbody tr td:nth-child(3) a.description:hover .tooltiptext {
			display: block;
			color:black;
			word-wrap: break-word;
			margin-top:-35px;
			margin-left:120px;
		}
		table tbody tr td:nth-child(3) a.description{
			color:#333;
		}
		table tbody tr td:nth-child(3) a.description:hover{
			color:#337ab7;
		}
		span.tooltiptext ul li{
			list-style-type:none;
		} 
		.square {
		float: left;
		width: 10px;
		height: 10px;
		margin: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		}

		.Normal {
		  background: #006600;
		}
		.Low {
		  background: #e4af6d;
		}

		.Medium {
		  background: #ff6600;
		}

		.High {
		  background: #ff0000;
		}
		#dynamic-table{
			width:100% !important;
			font-size:12px !important;
		}
	</style>
</head>
<body>

	<div class="tab-content">
		<div class="tab-pane fade in active" id="tab2success">
			<div class="row inbox">
				<div class="container-fluid col-md-12" style="background-color: white;">
			<!--<div class="page-header"></div>-->
			<h4>Request List</h4>
			<?php $CI = & get_instance();  ?>
			<div class="col-xs-12" style="font-size: 10px;font-family: inherit;">
				<table id="dynamic-table" class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="font-weight:bold;font-size:12px !important;">
							<th>Sr No</th>
							<th>ID</th>
							<!--<th>Open</th>-->
							<th>Subject</th>
							<th>Requester Name</th>
							<th>Technician</th>
							<th>Status</th>
							<th>Created date</th>
							<th>SLA</th>
							<th>Time Elapse</th>
							<th>Site</th>
							<!--<th>Department</th>
							<th>Category</th>
							<th>Sub-category</th>
							<th>Item</th>-->
							<th>Priority</th>
							<?php if(check_technician($CI->user_ldamp["mail"],$CI->user_ldamp["employeeid"])){
								//print_r($CI->user_ldamp["employeeid"]);
								?>
							<th style="width:104px">Group</th>
							<?php } ?>
						</tr>
					</thead>

					<tbody>
					<?php $groups = get_groups(); ?>
					<?php $group_sr ='<option  value="">Not Assigned</option>'; ?>
					<?php foreach($groups as $group): ?>
					<?php $group_sr .= '<option   value="'. $group->group_id.'">'.  $group->group_name .'</option>'; ?>				
					<?php endforeach; ?>
					
					<?php $i=1; foreach($assign_tech_list as $reportRecord){ 
						$dept_data = get_record_by_id_code('dept',$reportRecord->dept_id);
						$category_data = get_record_by_id_code('category',$reportRecord->category_id);
						$subcategory_data = get_record_by_id_code('subcategory',$reportRecord->subcategory_id);
						$item_data = get_record_by_id_code('item',$reportRecord->item_id);
					?>
						<tr>
							<td><?= $i; ?></td>
							<td><a href="<?php echo base_url() ?>helpdesk/requestdetail/<?php echo $reportRecord->ur_user_request_id; ?>"><?php echo getRequestCode($reportRecord->ur_user_request_id); ?></a></td>
							<!--<td><a href="<?php echo base_url() ?>helpdesk/requestdetail/<?php echo $reportRecord->ur_user_request_id; ?>" title="Click here to open the request">Click Here</a></td>-->
							<td>
								<a class="description"  href="<?php echo base_url() ?>helpdesk/requestdetail/<?php echo $reportRecord->ur_user_request_id; ?>"><?php echo ucfirst($reportRecord->request_subject);  ?>
									<span class="tooltiptext" style="background-color:#f5f5f5;">
										<strong>Request ID : </strong>&nbsp;<?php echo getRequestCode($reportRecord->ur_user_request_id); ?><br>
										<strong>Category : </strong>&nbsp;<?php echo ucfirst($category_data->category_name);  ?><br>
										<strong>Subject : </strong>&nbsp;<?php echo ucfirst(strip_tags($reportRecord->request_subject));  ?><br>
										<strong>Description : </strong>&nbsp;<?php echo substr(ucfirst(strip_tags($reportRecord->request_desc)),0,300);  ?>
										
									</span>
								</a>
							</td>
							<td><?php echo ucfirst($reportRecord->requester_name);  ?></td>
							<td>
							<?php echo ($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : 'Not assign';  ?>
							
							</td>
							<td><?php 
									if($reportRecord->request_assign_person_id != NULL){
										if($reportRecord->is_request_completed == 1){  
											$rRequest_status = '<span style="background-color:green;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;</span>';
										}
										else if($reportRecord->is_request_completed == 0 && $reportRecord->task_status == 'hold'){
											$rRequest_status = '<span style="background-color:#F89406;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;OnHold&nbsp;&nbsp;</span>';
										}
										else{
											$rRequest_status = '<span style="background-color:#ffeb3b;color:#3c763d;padding: 4px;border-radius: 10px;">&nbsp;Inprocess&nbsp;&nbsp;</span>';
										}
										if($reportRecord->is_request_completed == 0 && $reportRecord->task_status == 'query_to_user'){
											$rRequest_status = '<span style="background-color:#F89406;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;Query to user&nbsp;</span>';
										} 
									}else{
										$rRequest_status= '<span style="background-color:orange;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Not assign&nbsp;&nbsp;&nbsp;</span>';
									}
									echo $rRequest_status;
							?></td>
							<td style="width:81px;"><?= date('d-m-Y h:i:s A',strtotime($reportRecord->request_created_dt)); ?></td>
							<td><?php 
								 $user_req = getReqInfoByUserRequestId($reportRecord->ur_user_request_id);
								 $get_sla = get_item_sla_duration($user_req["item_id"]);
								 echo $get_sla->sla_duration.''.$get_sla->sla_duration_unit;	 
							?>
							</td>
							<td><?php 
							$technician_id = $user_req['technician_id'];
							$time_spent = sla_time_spent($reportRecord->ur_user_request_id,$technician_id);
							echo format_time($time_spent);
							?></td>
							<td><?php echo ucfirst($reportRecord->requester_location_name);  ?></td>
							<!--<td><?php echo ucfirst($dept_data->dept_name);  ?></td>
							<td><?php echo ucfirst($category_data->category_name);  ?></td>
							<td><?php echo ucfirst($subcategory_data->sub_category_name);  ?></td>
							<td><?php echo ucfirst($item_data->item_name);  ?></td>-->
							<td><span class="square <?php echo ucfirst($reportRecord->request_priority);  ?>"></span><?php echo ucfirst($reportRecord->request_priority);  ?></td>
							<?php 
								if(check_technician($CI->user_ldamp["mail"],$CI->user_ldamp["employeeid"]))
								{
							?>
							<td style="width:104px">
								
								<?php if(check_is_technician() || is_escalated_emp($u_code)){ ?>
								<div class="dropdown">	
									<a class="dropdown-toggle ab" data-tid=<?php echo $reportRecord->technician_id;  ?> data-gid="<?php echo $reportRecord->group_id; ?>" data-rid=<?php echo $reportRecord->ur_user_request_id; ?> href="#" data-toggle="dropdown" ><?php echo ($reportRecord->group_name !=NULL)? ucfirst($reportRecord->group_name) : 'Not assign';  ?> <strong class="caret"></strong></a>
								</div>
								
								<?php
								}else{ 
									echo ($reportRecord->group_name !=NULL)? ucfirst($reportRecord->group_name): 'Not assign';
								}
								?>
								
							</td>
							<?php } ?>
						</tr>
					<?php $i++; }  ?>
				</tbody>
				
				</table>
				
				<span id="printTxt" style="position:absolute;" >
					<div id="request_gp" class="dropdown-menu" style="padding: 10px; padding-bottom: 0px; position:absolute;" >
						 <span><b>Assigned Technician</b></span>
						<button type="button" class="close pop" aria-label="Close">
							<span style="font-size:18px;" aria-hidden="true">&times;</span>
						</button>
						<form method="post" class="form-horizontal" name="reassign_task" id="reassign_task">
							<div class="form-group">
								<label for="sel1" class="col-sm-6">Group:</label>
								<div class="col-sm-8">
									<select class="form-control" style="width:172px"  name="groups" id="groups" required>
										<?php echo $group_sr; ?>	
									</select>
								</div>
							</div>
							<p></p>
							
							<div class="form-group">
								<label for="sel1" class="col-sm-6">Technician:</label>
								<div class="col-sm-8">
									<select class="form-control" style="width:172px"  name="technicians" id="technicians" required>
										<option> --- Choose --- </option>
									</select>
								</div>

							</div>
							<p></p>
							
							<div class="form-group col-md-12">
								<input type="submit" class="btn btn-primary col-md-6" value="Assign" />
								<button id="button2id" type="button" class="btn btn-default pop">Cancel</button>
							</div>
						</form>
					</div>
				</span>
			</div>
		</div> <!-- ./container -->
		
		</div><!--/.col-->		
	</div>
</div>
</body>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
<script>

var request_idaa = 0;
/* function showCoords(event) {
    var x = event.clientX;
    var y = event.clientY;
    var coor = "X coords: " + x + ", Y coords: " + y;
	$("#printTxt").css({'top':y,'left':x,'display':'block'}).show(); 
}
 */
function get_technician(group_id,tech_id)
{
	        $.ajax({
				url : "<?php echo base_url() ?>"+ "helpdesk/get_technician",
				type:"POST",
				data: {group_id:group_id},
				success:function(response){
					var str = '<option value=""> --- Choose --- </option>';
					var obj  =  JSON.parse(response);
					var selectop = '';
					$(obj).each(function(index,value){
						var sel = (tech_id == obj[index].tech_id)? 'selected':'';
						str += '<option '+sel+' id="'+obj[index].tech_id+'" value="'+obj[index].tech_id+'">'+obj[index].tech_name+'</option>';	
					});
					
					$("#technicians").html(str);
					//document.getElementById(tech_id).selected = "true";
					//$("#tech_id").prop("selected",true);
				},
				error:function(response){
					alert("error occured"+response)
				}
			});
	
}

$(document).ready(function(){
	$("#dynamic-table > tbody> tr").css("height","90px");
	//reassing technician 
	/*$("#button1id").on("click",function(){
		requtVal = false;
		var group_id = $("#groups").val();
		var technician_id = $("#technicians").val();
		console.log('reassing groupId = '+ group_id + ' and technician id = '+technician_id);
		if(group_id > 0){
			requtVal = true;
		}else{
			alert('Please select the group');
			requtVal = false;
		}
		
		if(technician_id > 0){
			requtVal = true;
		}else{
			alert('Please select the technician_id');
			requtVal = false;
		}
			
		if(requtVal){
			$.ajax({
				url: "<?php echo base_url() ?>/helpdesk/reassign_technician",
				type: "POST",
				data:{group_id:group_id,technician_id:technician_id},
				success:function(resp){
					console.log(resp);
				},error:function(resp){
					alert("error occured please try again later");
					console.log(resp);
				}
			});
		}
	});*/
	
	//reassing technician 
	$("#reassign_task").validate({
            rules: {
                groups: { required: true },
				technicians : { required : true }
            },
            messages: {
				groups: { required : "Please select group" },
                technician : { required : "Please select technician" }
            },
            submitHandler: function (form) {
				var group_id = $("#groups").val();
				var technician_id = $("#technicians").val();
				console.log('reassing groupId = '+ group_id + ' and technician id = '+technician_id);
				$("#spinner").show();
				$.ajax({
					//url: "<?php echo base_url() ?>/helpdesk/reassign_technician",
					url: "<?php echo base_url() ?>/helpdesk/tech_reassign_ajx",
					type: "POST",
					data:{group_id:group_id,technician_id:technician_id,request_id:request_idaa},
					
					success:function(resp){
						console.log(resp);
						console.log('response is = '+resp);
						//$("#spinner").show();
						console.log('request_id '+request_idaa);
						if(resp > 0 && resp !=2){
							location.reload();
						}else if(resp == 2){
							alert("You can not re-assign to same technician");
						}else{
							alert("error occured please try again later");
						}
					},error:function(resp){
						alert("Error occured please try again later");
						console.log(resp);
					}
				});
                return false;
            }
        });

	
	
	
	<?php if(check_technician($CI->user_ldamp["mail"],$CI->user_ldamp["employeeid"])){ ?>
		var nulval = [null,null,null,null, null,null,null, null,null,null,null,null];
	<?php }else{ ?>
		var nulval = [null,null,null,null, null,null,null, null,null,null,null];
	<?php } ?>
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": nulval,
		"aaSorting": [],
		"lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	
	});
	
	//Handles menu drop down
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
		
    });
	$('#dynamic-table').on('click',' tbody >tr>td>.dropdown>a.dropdown-toggle.ab',function(e){
		//alert(($("#printTxt").position().left));
		
		var positionX;
		var positionY;
		var group_id = $(this).attr('data-gid');
		var tech_id  = $(this).attr('data-tid');
		request_idaa = $(this).attr('data-rid');
		positionX = e.clientX-45 + 'px',
		positionY = $(this).offset().top-250+'px';
		$("#groups").val('');
		$("#groups").val(group_id);
		//alert($(this).offset().top);
		//$("#request_gp").css({'top':positionY,'left':'75%','display':'block'}).show();
		//$("#request_gp").css({'top':e.clientY,'left':e.clientX,'display':'block'}).show();
		$("#printTxt").css({'top':positionY,'left':positionX,'display':'block'});
		// code to select technician
		get_technician(group_id,tech_id);
		$("#request_gp").show();
		
		
		///code to select group 
		/*
		$("#groups option").each(function(index){
			if(index == group_id){
				$(this).attr("selected","selected");
				
			}
			else{
				$(this).removeAttr("selected");
			}
			
				
			
		});*/
		
	});
	
	
	 // on menu selection
	 $( ".dropdown-menu" ).draggable();
	 
	 //on group selection
	$("#groups").on("change",function(){
		var group_id = $(this).find("option:selected").val();
		if(group_id == "")
		{
			alert("please select the correct group");
			
		}
		else
		{
			get_technician(group_id,tech_id =0);
		}
		
	});
	
	$(".close.pop").on("click",function(){
		$("#request_gp").hide();
	});
	$(".btn.btn-default.pop").on("click",function(){
		$("#request_gp").hide();
	});
	
});
</script>
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>