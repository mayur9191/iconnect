<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <?php //include APPPATH . "views/includs/hedder_code.php" ?>
<style>
body{margin-top:0px;
background:#fff;
overflow:hidden;
}


.inbox .inbox-menu ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .inbox-menu ul li {
    height: 30px;
    padding: 5px 15px;
    position: relative
}

.inbox .inbox-menu ul li:hover,
.inbox .inbox-menu ul li.active {
    background: #e4e5e6
}

.inbox .inbox-menu ul li.title {
    margin: 20px 0 -5px 0;
    text-transform: uppercase;
    font-size: 10px;
    color: #d1d4d7
}

.inbox .inbox-menu ul li.title:hover {
    background: 0 0
}

.inbox .inbox-menu ul li a {
    display: block;
    width: 100%;
    text-decoration: none;
    color: #3d3f42
}

.inbox .inbox-menu ul li a i {
    margin-right: 10px
}

.inbox .inbox-menu ul li a .label {
    position: absolute;
    top: 10px;
    right: 15px;
    display: block;
    min-width: 14px;
    height: 14px;
    padding: 2px
}

.inbox ul.messages-list {
    list-style: none;
    margin: 15px -15px 0 -15px;
    padding: 15px 15px 0 15px;
    border-top: 1px solid #d1d4d7
}

.inbox ul.messages-list li {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    cursor: pointer;
    margin-bottom: 10px;
    padding: 10px
}

.inbox ul.messages-list li a {
    color: #3d3f42
}

.inbox ul.messages-list li a:hover {
    text-decoration: none
}

.inbox ul.messages-list li.unread .header,
.inbox ul.messages-list li.unread .title {
    font-weight: 700
}

.inbox ul.messages-list li:hover {
    background: #e4e5e6;
    border: 1px solid #d1d4d7;
    padding: 9px
}

.inbox ul.messages-list li:hover .action {
    color: #d1d4d7
}

.inbox ul.messages-list li .header {
    margin: 0 0 5px 0
}

.inbox ul.messages-list li .header .from {
    width: 49.9%;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .header .date {
    width: 50%;
    text-align: right;
    float: right
}

.inbox ul.messages-list li .title {
    margin: 0 0 5px 0;
    white-space: nowrap;
    overflow: hidden!important;
    text-overflow: ellipsis
}

.inbox ul.messages-list li .description {
    font-size: 12px;
    padding-left: 29px
}

.inbox ul.messages-list li .action {
    display: inline-block;
    width: 16px;
    text-align: center;
    margin-right: 10px;
    color: #d1d4d7
}

.inbox ul.messages-list li .action .fa-check-square-o {
    margin: 0 -1px 0 1px
}

.inbox ul.messages-list li .action .fa-square {
    float: left;
    margin-top: -16px;
    margin-left: 4px;
    font-size: 11px;
    color: #fff
}

.inbox ul.messages-list li .action .fa-star.bg {
    float: left;
    margin-top: -16px;
    margin-left: 3px;
    font-size: 12px;
    color: #fff
}

.inbox .message .message-title {
    margin-top: 30px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px
}

.inbox .message .header {
    margin: 20px 0 30px 0;
    padding: 10px 0 10px 0;
    border-top: 1px solid #d1d4d7;
    border-bottom: 1px solid #d1d4d7
}

.inbox .message .header .avatar {
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    height: 34px;
    width: 34px;
    float: left;
    margin-right: 10px
}

.inbox .message .header i {
    margin-top: 1px
}

.inbox .message .header .from {
    display: inline-block;
    width: 50%;
    font-size: 12px;
    margin-top: -2px;
    color: #d1d4d7
}

.inbox .message .header .from span {
    display: block;
    font-size: 14px;
    font-weight: 700;
    color: #3d3f42
}

.inbox .message .header .date {
    display: inline-block;
    width: 29%;
    text-align: right;
    float: right;
    font-size: 12px;
    margin-top: 18px
}

.inbox .message .attachments {
    border-top: 3px solid #e4e5e6;
    border-bottom: 3px solid #e4e5e6;
    padding: 10px 0;
    margin-bottom: 20px;
    font-size: 12px
}

.inbox .message .attachments ul {
    list-style: none;
    margin: 0 0 0 -40px
}

.inbox .message .attachments ul li {
    margin: 10px 0
}

.inbox .message .attachments ul li .label {
    padding: 2px 4px
}

.inbox .message .attachments ul li span.quickMenu {
    float: right;
    text-align: right
}

.inbox .message .attachments ul li span.quickMenu .fa {
    padding: 5px 0 5px 25px;
    font-size: 14px;
    margin: -2px 0 0 5px;
    color: #d1d4d7
}

.inbox .contacts ul {
    /*margin-top: 30px;*/
    padding: 0;
    list-style: none
}

.inbox .contacts ul li {
    height: 30px;
    padding: 5px 15px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis!important;
    position: relative;
    cursor: pointer
}

.inbox .contacts ul li .label {
    display: inline-block;
    width: 6px;
    height: 6px;
    padding: 0;
    margin: 0 5px 2px 0
}

.inbox .contacts ul li:hover {
    background: #e4e5e6
}


.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;   
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;   
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;   
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b; 
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */  
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}


.panel-success>.panel-heading {background-image:linear-gradient(to bottom,#c9da2c 0,#cbdb2b 100%)}
.with-nav-tabs.panel-success .nav-tabs > li > a, .with-nav-tabs.panel-success .nav-tabs > li > a:hover, .with-nav-tabs.panel-success .nav-tabs > li > a:focus {color:#ffffff}
.with-nav-tabs.panel-success .nav-tabs > li > a:hover{background-color:#ffffff; color:#1c6b94;}
.backtab {background-color:#1c6b94; color:#ffffff;}
.backtab:hover {background-color:#1c6b94; color:#ffffff;}
.mand{color:red;}
.dropdown-thin {
    height: 25px;
    padding-top: 2px;
}

.dep-btn {
	background-color: #c9da2b;
    color: #3c763d;
    float: left;
    margin-bottom: 10px;
    margin-top: 10px;
    margin-right: 10px;
}
.dep-btn_annou {
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
	font-size:12px;
}
.dep-btn_annou:hover{
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
}
.dep-btn_annou:active{
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
}
.dep-btn_annou:target{
	background-color: #c9da2b !important;
    color: #3c763d !important;
    margin-bottom: 10px;
    margin-top: 0px;
    margin-right: 8px;
}

.badge1 {
   position:relative;
}
.badge1[data-badge]:after {
   content:attr(data-badge);
   position:absolute;
   top:-10px;
   right:-9px;
   font-size:.7em;
   background:green;
   color:white;
   width:16px;height:16px;
   text-align:center;
   line-height:18px;
   border-radius:50%;
   box-shadow:0 0 1px #333;
}
.tooltiptext {
    display: none;
    width: 220px;
    background-color: #c9da2b;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 1s;
}
.badge1:hover .tooltiptext{
	 display: block;
     opacity: 1;
}
.panel-body{
	font-size:12px;
}
.left {
    float:left;
    width: 50%;
}
.left > ul {
	margin-left: 0px;
}
.right{
    float:right;
    width: 50%;
	height:140px;
}

#piechart10{
	height:140px;
}
#piechart9{
	height:140px;
}
#piechart8{
	height:140px;
}
#piechart7{
	height:140px;
}
#piechart6{
	height:140px;
}
#piechart5{
	height:140px;
}
#piechart4{
	height:140px;
}
#piechart3{
	height:140px;
}
#piechart2{
	height:140px;
}
#piechart1{
	height:140px;
}

</style>

 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
<script src="https://select2.github.io/dist/js/select2.full.js"></script>
</head>
<body style="overflow-y:auto;">
<!--<br><br><br><br>-->

<?php $approvalData = getMaterialApproval($passingData->material_master_id); ?>

 <div class="panel-body">
	<div class="tab-content">
		<div class="tab-pane fade in active" id="tab1success">
			<div class="row inbox">
			<?php $i=1; foreach($departments as $department): ?>
				<div class="col-md-3">
					<div class="panel panel-default">
					<span class="btn backtab btn-block"><!--Request Summary for--> <?php echo $department->dept_name; ?> </span>
						<div class="panel-body inbox-menu">	
							<?php $request_counter =get_request_count_all($department->dept_id);
							 //echo "<pre>";print_r($request_counter);
 							?>
							<div class="left">
							<?php //echo $department->dept_id; ?>
							    <ul>
									<li style="width:110px;">
										<a onclick="ajax_get_result('In Process','inprocess','<?php echo $department->dept_name; ?>','<?php echo $department->dept_id; ?>')" href="#"> In process <span style="background-color:#ffeb3b;color:#3c763d;" class="label label-primary" ><?php echo ($request_counter['inprocess'] !='')? $request_counter['inprocess'] : '0'; ?></span></a>
									</li>
									<li style="width:110px;">
										<a onclick="ajax_get_result('On hold','onhold','<?php echo $department->dept_name; ?>','<?php echo $department->dept_id; ?>')" href="#"> On hold <span style="color:#1c6b94;" class="label label-warning"><?php echo ($request_counter['hold']!='')? $request_counter['hold']: '0'; ?></span></a>
									</li>
									<li style="width:110px;">
										<a onclick="ajax_get_result('Completed','completed','<?php echo $department->dept_name; ?>','<?php echo $department->dept_id; ?>')" href="#"> Completed <span  class="label label-success" style="background-color:#4fa928;"><?php echo ($request_counter['completed']!='')? $request_counter['completed'] : '0'; ?></span></a>
									</li>
									<li style="width:110px;">
										<a onclick="ajax_get_result('Total','','<?php echo $department->dept_name; ?>','<?php echo $department->dept_id; ?>')" href="#"> Total <span  class="label label-primary"><?php echo ($request_counter['total'] !='') ? $request_counter['total'] : '0'; ?></span></a>
									</li>
								
							    </ul>
							</div>
                            <div class="right"><!-- style="width:145px;height:150px;" -->
								<div id="piechart<?php echo $department->dept_id; ?>"></div>
							</div>
							
						</div>
						
						<script>
						//***** pie chart for the Request statistics**************// 
						google.charts.load('current', {'packages':['corechart']});
						google.charts.setOnLoadCallback(drawChart_<?php echo $i; ?>);
						var inprocess_<?php echo $i; ?> = <?php echo ($request_counter['inprocess'] !='')? $request_counter['inprocess'] : '0'; ?>;
						var on_hold_<?php echo $i; ?> = <?php echo ($request_counter['hold']!='')? $request_counter['hold']: '0'; ?>;
						var complete_<?php echo $i; ?> = <?php echo ($request_counter['completed']!='')? $request_counter['completed'] : '0'; ?>;
						function drawChart_<?php echo $i; ?>() {
							var data = google.visualization.arrayToDataTable([
								['Request', 'Count Per Total'],
								['In Process',   inprocess_<?php echo $i; ?>],
								['Completed',    complete_<?php echo $i; ?>],
								['On Hold',      on_hold_<?php echo $i; ?>],			  
							]);
							
							var options = {
								//title: 'My Request Status',
								chartArea:{left:5,top:0,width:'70%',height:'70%'},
								fontSize:11,
								legend:{position:'none'},
								pieSliceTextStyle: {color: '#1c6b94' },
								colors: ['#ffeb3b','#4fa928','#f89406'],
								//colors: ['#cbdb2b','#82AF6F','#F89406'],
							};
							
							var chart = new google.visualization.PieChart(document.getElementById("piechart"+<?php echo $department->dept_id; ?>));
							chart.draw(data, options);
							//removeBottomSpace();
						}
						</script>
					<?php $i++; ?>
					</div>
					
					<div  class="panel panel-default" style="display:none;">
					<a href="#" class="btn backtab btn-block"> Important Links </a>
						<div  class="panel-body">
							
							<ul>
								<!--
								<li><a href="http://www.skeiron.com" target="_blank" title="Skeiron.com"> <span class="label label-danger"></span>Skeiron.com </a></li>
								
								<li><a href="https://apps.seshipping.net/tms" target="_blank" title="Transport Management System"><span class="label label-danger"></span>Transport Management System </a></li>
								
								<li><a href="http://outlook.office365.com/" target="_blank" title="Skeiron Webmail"><span class="label label-danger"></span>Skeiron Webmail</a></li>
								-->
								<?php foreach(get_implinks() as $linkRecord){ ?>
									<li><a href="<?php echo $linkRecord->implink_url; ?>" target="_blank" title="<?php echo $linkRecord->implink_name; ?>"><span class="label label-danger"></span><?php echo $linkRecord->implink_name; ?></a></li>
								<?php } ?>
							</ul>
						
						</div>
					
					</div>			
					
				</div><!--/.col-->
				<?php endforeach; ?>
				<div class="col-md-6" style="padding-left:0px">
					
					<!--<div class="panel panel-default">
						<span class="btn backtab btn-block">Raise Request</span>
						<div class="panel-body message">
							<p class="text-center" align="center" style="padding: 12px;margin-left:30px;">
								<?php foreach($departments as $department): ?>
								<button type = "button" class="btn label-success dep-btn_annou badge1" onclick="window.location='<?php echo base_url().'helpdesk/new_request/'.$department->dept_id; ?>'" ><?php echo $department->dept_name;  ?>
								</button>
								<?php endforeach; ?>
							</p>
							
						</div>	
					</div>-->
					
					<div class="panel panel-default" style="display:none;">
						<span class="btn backtab btn-block">Annoucements</span>
						<div class="panel-body message">
							<p class="text-center" align="center" style="padding: 12px;margin-left: 8px;">
								<?php foreach($departments as $department): ?>
								<?php if($department->dept_id == '1'){
									$notification_data = get_notification_dept($department->dept_id);
									$notification_count = count($notification_data);
									$notification_content = '';
									foreach($notification_data as $notifi){
										$notification_content .= '<a href="#">'.$notifi.'</a>';
										$notification_content.= '<br/>';
									}
									
								} 
									else{
										$notification_count =0;
										$notification_content="";
								   }
								?>
								<button <?php if($notification_count >0) { ?>data-badge="<?php echo $notification_count; } ?>"  type = "button" class="btn label-success dep-btn_annou badge1" id="tab<?php echo $department->dept_id; ?>"><?php echo $department->dept_name;  ?> <?php if($notification_count >0) {?>
								<span class="tooltiptext">
								<?php echo $notification_content; ?></span><?php } ?>
								</button>
								<?php endforeach; ?>
							</p>
							
						</div>	
					</div>
					<!--<div class="panel panel-default">
					<span class="btn backtab btn-block">Guideline
					
					<div  class="dropdown pull-right">
						<button class="btn btn-default dropdown-toggle dropdown-thin" type="button" data-toggle="dropdown">Department
						  <span class="caret"></span></button>
							<ul class="dropdown-menu">
								<?php foreach($departments as $department): ?>
								  <li><a href="#" class="dept_guideline" data-id="<?php echo $department->dept_id; ?>"><?php echo $department->dept_name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</span>
						<div class="panel-body message" id="hd_guideline">
							<p class="text-left ajx"></p>
						</div>	
					</div>-->
					
				</div><!--/.col-->	
                <div class="col-md-3" style="padding-left:0px">
					<!--<div class="panel panel-default">
					<span class="btn backtab btn-block">Request Statistics </span>
						<div class="panel-body inbox-menu">	
							
							<ul>
								<li>
									<a href="<?php echo base_url().'helpdesk/admin_requestlist/inprocess'; ?>"> In process <span style="background-color:#cbdb2b;color:#3c763d;" class="label label-primary" ><?php echo ($request_counter['open'] !='')? $request_counter['open'] : '0'; ?></span></a>
								</li>
								
								<li>
									<a href="<?php echo base_url().'helpdesk/admin_requestlist/onhold'; ?>"> On hold <span style="color:#1c6b94;" class="label label-warning"><?php echo ($request_counter['hold']!='')? $request_counter['hold']: '0'; ?></span></a>
								</li>
								
								<li>
									<a href="<?php echo base_url().'helpdesk/admin_requestlist/completed'; ?>"> Completed <span  class="label label-success"><?php echo ($request_counter['completed']!='')? $request_counter['completed'] : '0'; ?></span></a>
								</li>
								
								<li>
									<a href="<?php echo base_url().'helpdesk/admin_requestlist/'; ?>"> Total <span  class="label label-primary"><?php echo ($request_counter['total'] !='') ? $request_counter['total'] : '0'; ?></span></a>
								</li>
								
							</ul>
							<div id="piechart" style="width:210px;height:200px;"></div>
						</div>	
					</div>-->
					
					<!--<div  class="panel panel-default">
					<a href="#" class="btn backtab btn-block"> Quick Links </a>
						<div  class="panel-body">
							<?php echo get_quick_links(); ?>
							
						
						</div>
					
					</div>-->	
					<!--<ul >
								<li><a href="http://www.skeiron.com" target="_blank" title="Skeiron.com"> <span class="label label-danger"></span>Skeiron.com </a></li>
								<li><a href="https://apps.seshipping.net/tms" target="_blank" title="Transport Management System"><span class="label label-danger"></span>Transport Management System </a></li>
								<li><a href="http://outlook.office365.com/" target="_blank" title="Skeiron Webmail"><span class="label label-danger"></span>Skeiron Webmail</a></li>
							</ul>-->
					<!--start imortant links<div  class="panel panel-default">
					<a href="#" class="btn backtab btn-block"> Important Links </a>
						<div  class="panel-body">
							
							<ul>
								
								<?php foreach(get_implinks() as $linkRecord){ ?>
									<li><a href="<?php echo $linkRecord->implink_url; ?>" target="_blank" title="<?php echo $linkRecord->implink_name; ?>"><span class="label label-danger"></span><?php echo $linkRecord->implink_name; ?></a></li>
								<?php } ?>
							</ul>
						
						</div>
					
					</div>imortant links ends-->
					<!--
								<li><a href="http://www.skeiron.com" target="_blank" title="Skeiron.com"> <span class="label label-danger"></span>Skeiron.com </a></li>
								
								<li><a href="https://apps.seshipping.net/tms" target="_blank" title="Transport Management System"><span class="label label-danger"></span>Transport Management System </a></li>
								
								<li><a href="http://outlook.office365.com/" target="_blank" title="Skeiron Webmail"><span class="label label-danger"></span>Skeiron Webmail</a></li>
								-->
				</div><!--/.col-->				
			</div>
		</div>
		
		<!-- <button type="button" class="btn btn-info btn-sm pull-right" style="margin-right:18px;margin-bottom:-12px;" data-toggle="modal" data-target="#myModal">Contact Us</button>
		second div goes here -->
		
	</div>
	
</div>
      

<!-- Modal -->
<div id="myModal_div" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:100%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer" style="border-top:none;">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div>      




<!-- Modal for Contact US -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:green;">Contacts <i  class="fa fa-envelope" style="font-size:24px;color:green;"></i></h4>
      </div>
      <div class="modal-body" style="height:410px;overflow-y:scroll;">
		<table class="col-md-12">
			<tr>
				<td colspan="2"><h4>ITFM</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Rahul Survase</span><br/>
						<span>Rahul.Survase@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>Shaijad Kureshi</span><br/>
						<span>Shaijad.Kureshi@skeiron.com</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>HR</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Rudramahi Wadje</span><br/>
						<span>Rudramahi.Wadje@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>Amish Bhatt</span><br/>
						<span>Amish.Bhatt@skeiron.com</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Server Support</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Sunil Nale</span><br/>
						<span>Sunil.Nale@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Nav Support</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Shekhar Deshmukh</span><br/>
						<span>Shekhar.Deshmukh@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>Ajay Yadav</span><br/>
						<span>Ajay.Yadav@skeiron.com</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Network Support</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Johny Anthony</span><br/>
						<span>Johny.Anthony@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>New Purchase</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Nelson Komathan</span><br/>
						<span>Nelson.Komathan@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr><h4>Admin</h4></td>
			</tr>
			<tr>
				<td>
					<p style="font-size:16px;color:green;">Primary Contact</p>
						<span>Asha Bhandwalkar</span><br/>
						<span>Asha.Bhandwalkar@skeiron.com</span>
				</td>
				<td>
					<p style="font-size:16px;color:green;">Secondary Contact</p>
						<span>-</span><br/>
						<span>-</span>
				</td>
			</tr>
		</table>
      </div>
      
    </div>

  </div>
</div>

	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.2/ckeditor.js"></script>

<!-- editor -->
 
   <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/editr/jquery-te-1.4.0.css">	
<script type="text/javascript" src="<?php echo base_url() ?>assets/editr/jquery-te-1.4.0.min.js" charset="utf-8"></script>
<!-- editor -->

<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
  
<script>
	
    
	var trVal = 1;
	$(document).ready(function(){
		
		
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy'
		});
		
		$("#tab1").click(function(){
			
			$(this).attr('aria-expanded',true);
			$(this).find('li').addClass('active');
			
			//$("#tab2").attr('aria-expanded',false);
			//$("#tab2").find('li').removeClass('active');
		});
		$('.jqte-test').jqte();
		
		// settings of status
		//var jqteStatus = true;
		/*$(".status").click(function(){
			jqteStatus = jqteStatus ? false : true;
			$('.jqte-test').jqte({"status" : jqteStatus})
		});*/
		
		/*$("#tab21").click(function(){
			$("#tab1").attr('aria-expanded',false);
			$("#tab1").parent('li').removeClass('active');
			
			$("#tab2").attr('aria-expanded',true);
			$("#tab2").parent('li').addClass('active');
			
			$("#tab2success").addClass('active in');
			$("#tab1success").removeClass('active in');
		});*/
		$("#btnSubmit").click(function(){
			$("#errMsg").text('');
			return true;
		});
		 $.validator.addMethod("regex", function (value, element, regexpr) {
			return regexpr.test(value);
		}, "Please enter a valid name.");

		  $.validator.setDefaults({
			 
			errorPlacement: function(error, element) {
				error.appendTo('#errMsg');
				$("#errMsg").append('<br>');
			}
		});
		
		$("#service_request_form").validate({
		 
			rules: {
				request_description:{
					required:true,
					regex: /^[A-Za-z]+$/
				},
				request_type: {required:true},
				priority: {required:true},
				request_name:{required:true},
				request_location:{required:true},
				category: {required:true},
				sub_category: {required:true},
				item: {required:true},
				request_subject: {required:true}
				
				
			},
			 
			messages: {
				
				"request_type": {
					required: "Please select one request type"                
				},
				"priority": {
					required: "Please select one priority item"                
				},
				"request_name": {
					required: "Please enter requester name"                
				},
				"request_location": {
					required: "Please enter requester location"                
				},
				"category": {
					required: "Please select category"                
				},
				"sub_category": {
					required: "Please select sub-category"                
				},
				"item": {
					required: "Please select item"                
				},
				"request_subject": {
					required: "Please enter subject"                
				},
				"request_description": {
					required: "Please enter the Description"                
				}
				
				
			},
			
			
			submitHandler: function (form) { 
				
				alert('valid form submitted'); 
				return false; 
			}
			
			
			
		});
		
	});
	function enableTab(tabid)
	{
		$("#tab2success").addClass('active in');
	    $("#tab1success").removeClass('active in');
		
	}
	/*CKEDITOR.replace( 'editor', {
		//removed "sourcearea"
		plugins: 'wysiwygarea,basicstyles,toolbar,undo',
		on: {
			instanceReady: function() {
				// Show textarea for dev purposes.
				//this.element.show();
			},
			change: function() {
				// Sync textarea.
				this.updateElement();    
				// Fire keyup on <textarea> here?
			}
		}
	});*/
	//*******to change the department from the guideline code starts ***********//
	$(".dropdown-menu li a").click(function(){
	  $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
	  $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
	});
	
	$(".dept_guideline").on("click",function(){
		var dept_id = $(this).attr('data-id');
		  $.ajax({
		   type: "POST",
		   url: "<?php echo base_url().'helpdesk/getGuide' ?>",
		   data: "department=" + dept_id,
		   success: function(result) {
				if(result !=''){
					$(".text-left.ajx").html('');
					$(".text-left.ajx").html(result);
				}
		   }
		  });
	});
	
//*******to change the department from the guideline code ends here ***********//	 
	
	
</script>


<script type="text/javascript">

/*function removeBottomSpace(){
	$('rect').removeAttr('height');
	$('svg').removeAttr('height');
	$("#piechart").find('div').find('div').eq(0).css("height","");
}*/
function ajax_get_result(TYPE,type,dept_name,dept_id)
{
	$.get("<?php echo base_url() ?>helpdesk/requestlists_ajax/"+dept_id+"/"+type,function(data, status){
		//$("#table_data").html(data);
		//$("#dept_add").html(dept_name);
		$("#myModal_div").find(".modal-title").html(dept_name+" - "+TYPE);
		$("#myModal_div").find(".modal-body").html(data);
		$("#myModal_div").modal('show');
    });
	return false;
}
</script>
</body>
</html>