<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style1.css"  />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
		
	     table tbody tr td:nth-child(3) a.description .tooltiptext {
			display: none;
			width: 430px;
			background-color:#ccc
			color:black;
			text-align: left;
			border-radius: 6px;
			padding: 8px 0;
			position: absolute;
			z-index: 1;
			border:1px solid black;
			
		}
		table tbody tr td:nth-child(3) a.description:hover .tooltiptext {
			display: block;
			color:black;
			word-wrap: break-word;
			margin-top:-35px;
			margin-left:120px;
		}
		table tbody tr td:nth-child(3) a.description{
			color:#333;
		}
		table tbody tr td:nth-child(3) a.description:hover{
			color:#337ab7;
		}
		span.tooltiptext ul li{
			list-style-type:none;
		} 
		.square {
		float: left;
		width: 10px;
		height: 10px;
		margin: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		}

		.Normal {
		  background: #006600;
		}
		.Low {
		  background: #e4af6d;
		}

		.Medium {
		  background: #ff6600;
		}

		.High {
		  background: #ff0000;
		}
		#dynamic-table{
			width:100% !important;
			font-size:12px !important;
		}
	</style>
		
<style>
#spinner{
	position: fixed;
	z-index: 999;
	height: 100%;
	width: 100%;
	top: 0;
	left: 0;
	background-color: Black;
	filter: alpha(opacity=60);
	opacity: 0.1;
	-moz-opacity: 0.8;
	display:none;
}
</style>
</head>
<body>
	<div class="tab-content">
		<div class="tab-pane fade in active" id="tab2success">
			<div class="row inbox">
				<div class="container-fluid col-md-12" style="background-color: white;">
					<h4>Reports</h4>
					<div class="col-xs-12" style="font-size: 10px;font-family: inherit;">
						
						<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
						<div id="chart_div" style="width: 100%; height: 500px;"></div>
					   
						
					</div>
				</div> <!-- ./container -->
			</div><!--/.col-->		
		</div>
	</div>
</body>


<!-- Modal -->
<div id="myModal_div" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer" style="border-top:none;">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div>      



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script>
	//https://jsfiddle.net/api/post/library/pure/
	//https://jsfiddle.net/w8cmfws5/5/
	var chart;
	var options = [];
	var data;
	var clicked = false;
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
        // Some raw data (not necessarily accurate)
        data = google.visualization.arrayToDataTable([
			['Month', 'Inprocess', 'On-hold', 'Completed','Total'],
			<?php $ticketData = get_request_count_by_email_report();
				for($i=0;$i<count($ticketData['day_ary']);$i++){
					echo "['".$ticketData['day_ary'][$i]['ticket_date']."', ".$ticketData['day_ary'][$i]['inprocess'].", ".$ticketData['day_ary'][$i]['hold'].",".$ticketData['day_ary'][$i]['completed'].",".$ticketData['day_ary'][$i]['total']."]";
					if($i < (count($ticketData['day_ary']) -1 ) ){
						echo ',';
					}
				}
			?>
		]);
		
		options = {
		  title : 'Day wise ticket report',
		  vAxis: {title: 'Tickets'},
		  hAxis: {title: 'Dates'},
		  seriesType: 'bars',
		  bars: 'horizontal',
		  series: {5: {type: 'line'}},
		  colors: ['#cbdb2b','#F89406','#82AF6F','#58D68D'],
		};
		chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		chart.draw(data, options);
		google.visualization.events.addListener(chart, 'select', selectHandler);
		google.visualization.events.addListener(chart, 'click', clickHander);
		
	}
	
	function selectHandler(e) {
		var selection = [];
		var selection1 = chart.getSelection();
		var row = selection1[0].row;
		var click_date = data.getValue(row,0);
		console.log(click_date);
		get_click_data('day_ajax',click_date);
		if(selection1.length == 0){
			if(clicked !== false){
				selection = selection.filter(function (el) {
				  return el.row !== clicked;
				}
			)}
	  }else{
		selection = selection.concat(selection1);  
	  }
	  console.log(selection);
	  chart.setSelection(selection);
	}
	
	function clickHander(e) {
	  var target = e.targetID.split("#");
		if(target.length == 3){
		var row = parseInt(target[2]);
		clicked = row;
	  }else{
		clicked = false
	  }
	}
	
	function get_click_data(request_type,data_date) {
		$.ajax({
		   type: "POST",
		   url: "<?php echo base_url().'helpdesk/get_report' ?>",
		   data: "request_type=" + request_type+"&data_date="+data_date,
		   success: function(res) {
				if(res !=''){
					//$("#myModal_div").find(".modal-title").html(dept_name+" - "+TYPE);
					$("#myModal_div").find(".modal-body").html(res);
					$("#myModal_div").modal('show');
				}
			}
		});
		return false;
	}

</script>

<div id="spinner">
    <img src="<?php echo base_url().'assets/images/ajaxSpinner.gif';?>" style="position: fixed; left: 45%; top: 45%;width: 150px;"></img>
</div>