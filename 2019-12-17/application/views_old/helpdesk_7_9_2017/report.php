<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	 <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body>
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<br><br>
	
	<div class="container col-md-12" style="background-color: white;">
		<div class="page-header">
		</div>
		<h4>Technician assigned request</h4>
		<div class="col-xs-12" style="font-size: 10px;font-family: inherit;">
			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
				<thead>
					<tr style="font-weight:bold">
						<th>Sr No</th>
						<th> Request id </th>
						<th>Request date</th>
						<th>Requester name</th>
						<th>Requester location</th>
						<th>Department</th>
						<th>Category</th>
						<th>Sub-category</th>
						<th>Item</th>
						<th>Request Subject</th>
						<th>Requested group</th>
						<th>Technician name</th>
						<th>Request complete status </th>
					</tr>
				</thead>

				<tbody>
				<?php $i=1; foreach($assign_tech_list as $reportRecord){ 
					$dept_data = get_record_by_id_code('dept',$reportRecord->dept_id);
					$category_data = get_record_by_id_code('category',$reportRecord->category_id);
					$subcategory_data = get_record_by_id_code('subcategory',$reportRecord->subcategory_id);
					$item_data = get_record_by_id_code('item',$reportRecord->item_id);
				?>
					<tr>
						<td><?= $i; ?></td>
						<td><?php echo 'SK00'.$reportRecord->ur_user_request_id; ?>
						<td><?= date('d-m-Y',strtotime($reportRecord->request_created_dt)); ?></td>
						<td><?php echo ucfirst($reportRecord->requester_name);  ?></td>
						<td><?php echo ucfirst($reportRecord->requester_location_name);  ?></td>
						<td><?php echo ucfirst($dept_data->dept_name);  ?></td>
						<td><?php echo ucfirst($category_data->category_name);  ?></td>
						<td><?php echo ucfirst($subcategory_data->sub_category_name);  ?></td>
						<td><?php echo ucfirst($item_data->item_name);  ?></td>
						<td><?php echo ucfirst($reportRecord->request_subject);  ?></td>
						<td><?php echo ($reportRecord->group_name !=NULL)? ucfirst($reportRecord->group_name) : 'Not assign';  ?></td>
						<td><?php echo ($reportRecord->tech_name !=NULL) ? ucfirst($reportRecord->tech_name) : 'Not assign';  ?></td>
						<td><?php 
								if($reportRecord->request_assign_person_id != NULL){
									if($reportRecord->is_request_completed == 1){  
										echo '<span style="background-color:green;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;</span>';
									}else{
										echo '<span style="background-color:red;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Not completed&nbsp;&nbsp;&nbsp;</span>';
									}
								}else{
									echo '<span style="background-color:orange;color:#fff;padding: 4px;border-radius: 10px;">&nbsp;&nbsp;&nbsp;Not assign&nbsp;&nbsp;&nbsp;</span>';
								}
						?></td>
						
					</tr>
				<?php $i++; }  ?>
			</tbody>
			</table>
		</div>
	</div> <!-- ./container -->
</body>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	var myTable = 
	$('#dynamic-table').DataTable( {
		bAutoWidth: true,
		"aoColumns": [null, null,null,null, null,null,null, null,null,null,null,null,null],
		"aaSorting": [],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Show All"]],
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		//"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	});
	
});
</script>