<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <?php include APPPATH . "views/includs/hedder_code.php" ?>

</head>

<body style="/*background-color: #dff0d8;*/">
<?php include APPPATH . "views/includs/top_navbar.php" ?>


<div class="container">
<div class="row">
    <div class="col-md-12">

        <div class="col-md-3 col-sm-4 col-xs-12 oym-iconnect nopad_left_right">
            <?php include APPPATH . "views/module/profile/common/left_box.php" ?>
        </div>

       <div class="col-md-6 col-sm-8 col-xs-12">
	   
		 <div class="clear"></div>
			<div class="iconnect-ps">
			<a href="#">
				<h5 class="iconnect-psh5">
					<b class="ps-tag" style="padding-left: 8px;">Events</b> 
				</h5>
			</a>
		</div>
		
		<?php
		 if (count($prifile_slides) > 0) {
			 ?>
			 <div class="row">


                <div id="profile_sliders" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php
                        if (count($prifile_slides) > 0) {
                            $ii = 0;
                            foreach ($prifile_slides AS $wish_item) {
                                if ($ii == 0) {
                                    $active = 'active';
                                } else {
                                    $active = '';
                                }

                                ?>
                                <li data-target="#profile_sliders" data-slide-to="<?= $ii ?>"
                                    class="<?= $active ?>"></li>
                                <?php
                                $ii++;
                            }
                        }
                        ?>

                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox" >

                        <?php
                        if (count($prifile_slides) > 0) {
                            $iim = 0;
                            foreach ($prifile_slides AS $wish_item) {
                                if ($iim == 0) {
                                    $active = 'active';
                                } else {
                                    $active = '';
                                }


                                ?>
                                <div class="item <?= $active ?>">
                                    <img src="<?= base_url() . 'uploads/sliders/' . $wish_item->sl_image ?>"
                                         alt="Birthday Wishes">
                                </div>

                                <?php
                                $iim++;
                            }
                        }
                        ?>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#profile_sliders" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#profile_sliders" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>


            </div>
			 <?php
		 }
		?>

            
			<?php
			
			if ($logindata['role'] == 1) {
				
				?>
				 <div class="three-search">
                <form action="<?= base_url() . 'profile/announcement' ?>" enctype="multipart/form-data" method="post" id="wallpostForm">
					<div class="form-group" style="display:none;">
                        <textarea rows="4" class="form-control walltext" id="editor" placeholder="Enter Announcement"
                                  name="announcement_text"></textarea>

                    </div>
					 <div class="form-group">
                        <input type="file" name="attachemnet"  />

                    </div>
                    <div class="form-group">
                        <input type="submit" value="post" class="btn btn-sm btn-primary pull-right"/>
                    </div>
                </form>


                <!--  <input type="text" class="form-control" placeholder="what's on your mind"/>-->
            </div>
				<?php
				
			}
			
			?>
			
           <br>
            <div class="clear"></div>
			<div class="iconnect-ps">
				<a href="#">
					<h5 class="iconnect-psh5">
						<b class="ps-tag" style="padding-left: 8px;">Announcements</b> 
						<!--<span class="glyphicon glyphicon-globe navdown-list22"></span>-->
					</h5>
				</a>
			</div>


            <?php
            //            echo '<pre>';
            //            print_r($user_wall);
            //            echo '</pre>';
            ?>

            <ul class="nopad list-group" style="list-style:none">
                <?php
                if (count($announcements) > 0) {
                    foreach ($announcements AS $witem) {
                        ?>
                        <li class="wallitem_<?= $witem->an_id ?> nopad list-group-item" >

                            <?php
                            //                            echo '<pre>';
                            //                            print_r($witem);
                            //                            echo '</pre>';
                            ?>

                            <div class="row">
                              <!--  <div class="col-xs-1 col-md-1 nopad">
                                    <img src="<?= base_url() . 'assets/' ?>images/50.png" alt="client-user"
                                         clas="img-responsive"/>
                                </div> -->
                                <div class="col-xs-12 col-md-12 nopad">
                                    <div class="ptag1" style="font-size: 10px; font-weight: bold; display:none;">
                                        <span style="color: #1c6b94; display:none">
                                            <?= ucfirst($witem->u_name) ?>
                                        </span>


                                        <?php
                                        if ($logindata['user_id'] == $witem->an_created_by) {
                                            ?>
                                            <div class="btn-group pull-right " style="display:none">
                                                <button title="Edit" type="button" data-toggle="modal"
                                                        data-target="#wallmodel_<?= $witem->an_id ?>"
                                                        class="btn btn-primary btn-xs">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button type="button" title="Remove" wallid="<?= $witem->an_id ?>"
                                                        class="btn btn-primary btn-xs delete_wall_item wall_item_<?= $witem->an_id ?>">
                                                    <i class="fa fa-remove"></i>
                                                </button>

                                            </div>


                                            <div id="wallmodel_<?= $witem->an_id ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;</button>
                                                        </div>

                                                        <div class="modal-body">
                                                            <form method="post"
                                                                  action="<?= base_url() . 'profile/wall_update' ?>">
                                                                <input type="hidden" name="edit_wall_id"
                                                                       value="<?= $witem->an_id ?>"/>
                                                                <div class="form-group">
                                                                    <textarea style="font-weight: normal" rows="3"
                                                                              cols="" class="form-control"
                                                                              name="edit_wall_text"><?= $witem->an_text ?></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="submit" class="btn btn-primary"
                                                                           value="Update Now"/>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>


                                    </div>
                                   

                                    <div class="post" style="text-align:justify;">
                                        <p style="text-align:justify;"><?= stripcslashes($witem->an_text) ?></p>
                                    </div>
								<?php if($witem->an_file !=''){ ?>
									<div class="">
										<img src="<?= base_url().'uploads/announcement/'.$witem->an_file;?>" class="nopad" style="width:100%;" />
										
                                    </div>
								<?php } ?>
                                </div>
                            </div>
                        </li>
                        <?php
						break;
                    }
                }
                ?>


            </ul>

        </div>

        <div class="col-md-3 col-sm-4 col-xs-12 oym-iconnect nopad_left_right">
            <?php include APPPATH . "views/module/profile/common/right_box.php" ?>
        </div>

    </div>
</div>

<!--<div class="row">
    <a href="#" class="nextpage-arrow"><span class="glyphicon glyphicon-arrow-right"></span></a>
</div>-->
</div>



<?php include APPPATH . "views/includs/common_scripts.php" ?>

<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<!--<script type="text/javascript" src="--><? //= base_url() . 'assets/' ?><!--croppic/croppic.min.js"></script>-->


<script type="text/javascript">

CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;

};
CKEDITOR.replace('editor');


    $(function () {
		
		//$('#editor').ckeditor();
		
	
        $("#likeId").on("click", function () {
            var birthdayVal = $(this).attr("data-birthdays");
            var currntUid = $(this).attr("data-current");
            $.post('<?php echo base_url();?>Profile/birthdayLike', {
                    birthday_user_ids: birthdayVal,
                    like_user_id: currntUid
                },
                function (rps) {
                    console.log(rps);
                    $("#likeId").html('Liked');
                    $("#likeId").removeAttr("id");
                });
        });


        $("#wallpostForm").validate({
            rules: {
                walltext: {
                    required: true
//                    email: true
                }
            },
            messages: {
                walltext: {
                    required: " is required"
//                    email: "Enter Valid Email"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $(".changedp").validate({
            rules: {
                image: {
                    required: true
//                    email: true
                }
            },
            messages: {
                image: {
                    required: " is required"
//                    email: "Enter Valid Email"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $('.delete_wall_item').on('click', function () {
            console.log($(this).attr('wallid'))

            var wallid = $(this).attr('wallid');
            var wallcalss = $(this).attr('wallid');

            $.ajax({
                type: "POST",
                url: "<?= base_url() . 'ajax/wallitemremove' ?>",
                data: {wallid: wallid},
                cache: false,
                success: function (data) {
                    console.log(data);

                    if (data == 1) {
                        $('.wallitem_' + wallid).slideUp();
                    }

//                    $("#resultarea").text(data);
                }
            });


        })

        $(".fileUpload").on('change', function () {
            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#image-holder");
            image_holder.empty();
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof(FileReader) != "undefined") {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "img-responsive"
                            }).appendTo(image_holder);
                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Pls select only images");
            }
        });


    });


</script>
<?php include APPPATH . "views/includs/footer.php" ?>
</body>
</html>
