<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'theme/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'theme/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'theme/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'theme/' ?>bootstrap/js/bootstrap.min.js"></script>
	
	<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
	<script src="https://www.amcharts.com/lib/3/pie.js"></script>
	<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
	<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
	<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
	
<style>
#chartdiv{
  width: 100%;
  height: 500px;
}
</style>

<body>
	<div class="container col-md-12" style="background-color: white;">
		<div class="page-header">
			<center>
				<h3>Skeiron Group</h3>
				<h4>Learning & Development</h4>
				<h4>Individual Training Needs Identification Survey FY 2017 - 2018</h4>
			</center>
		</div>
		<h4>Survey Report</h4><br>
		<div class="col-xs-12" style="overflow-x:scroll;font-size: 10px;font-family: inherit;">
			<div id="chartdiv"></div>
		</div>
	</div> <!-- ./container -->
</body>
</html>

<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "dark",
  "dataProvider": [ {
    "employee": "Not Attended",
    "attended": 800
  }, {
    "employee": "Attended",
    "attended": 500
  } ],
  "valueField": "attended",
  "titleField": "employee",
   "balloon":{
   "fixedPosition":true
  },
  "export": {
    "enabled": false
  }
});
</script>