<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Daily task report</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="col-xs-12">
				<div class="clearfix">
					<div class="pull-right tableTools-container"></div>
				</div>
				<div class="table-header">
					Overall Daily Task Report 
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-primary" style="position:relative;">
							<span class="btn backtab btn-block">Daily Task Report Summary </span>
							<div class="panel-body inbox-menu">
								<ul style="list-style-type:none;">
								<?php $record = json_decode($report_data);
									  $person_ary = array();
									  $count_ary = array();
									  foreach($record as $rec){
										$person_ary[] = $rec->u_name.'('.$rec->count.')';
										$count_ary[] = $rec->count;
								?>
									<li style="width:200px;padding:5px;">
									<a href="javascript:void(0)" onclick="ajax_get_result('<?php echo $rec->emp_code; ?>','<?php echo$rec->u_name; ?>')"><?php echo $rec->u_name; ?>
									<span class="label label-primary pull-right" ><?php echo $rec->count; ?></span></a></li> 
								<?php } ?>
								
								</ul>	  
							</div>
						</div>
					</div>
					<div class="col-md-8">
						
						<?php 
							$color_ary = array('dc3912','3366cc','ff9900','990099','0099c6','dd4477','85C1E9','EDBB99','F9E79F','109618','FAD7A0','73C6B6','A9DFBF','ABEBC6');
							$color_ary1 = array('85C1E9','EDBB99','F9E79F','FAD7A0','73C6B6','A9DFBF','ABEBC6');
							//$count_ary = array(1,2,3,4,5,6,7);
							$color_str = implode("|",$color_ary);
							$color_str1 = implode("|",$color_ary1);
							$person_str = implode("|",$person_ary);
							$count_str = implode(",",$count_ary);
						?>
						<br><br>
						<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:<?php echo $count_str; ?>&chco=<?php echo $color_str; ?>&chs=600x200&chl=<?php echo $person_str; ?>" />
					</div>
				</div>
			</div>
		
<div id="myModal_div" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer" style="border-top:none;">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div> 
		
		
			
			<div class="row">
				<div class="col-xs-12">
					<?php 
						$color_ary = array('3366cc','dc3912','ff9900','109618','990099','0099c6','dd4477','85C1E9','EDBB99','F9E79F','FAD7A0','73C6B6','A9DFBF','ABEBC6');
						$color_ary1 = array('85C1E9','EDBB99','F9E79F','FAD7A0','73C6B6','A9DFBF','ABEBC6');
						//$count_ary = array(1,2,3,4,5,6,7);
						$color_str = implode("|",$color_ary);
						$color_str1 = implode("|",$color_ary1);
						$person_str = implode("|",$person_ary);
						$count_str = implode(",",$count_ary);
					?>
					<!--<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:<?php echo $count_str; ?>&chco=<?php echo $color_str; ?>&chs=600x250&chl=<?php echo $person_str; ?>" />-->
					
					<!--<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:<?php echo $count_str; ?>&chco=<?php echo $color_str1; ?>&chs=600x250&chl=<?php echo $person_str; ?>" />-->
					
					<!--<img src="http://chart.apis.google.com/chart?cht=p3&chd=t:5,5,5&chco=ffeb3b|f89406|4fa928&chs=600x150&chl=Inprocess(1)|Onhold(1)|Completed(1)" />-->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
					<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				<!--datatable js--->
				<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
					<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
					<!--<div id="piechart" style="width: 900px; height: 500px;"></div>-->
					<script>
						var chart;
						var options = [];
						var data;
						var clicked = false;
						google.charts.load('current', {'packages':['corechart']});
						google.charts.setOnLoadCallback(drawChart);

						function drawChart() {

							data = google.visualization.arrayToDataTable([
								['Task', 'Hours per Day'],
								['Work',     11],
								['Eat',      2],
								['Commute',  2],
								['Watch TV', 2],
								['Sleep',    7],
								['Sleep',    7],
								['Sleep',    7],
								['Sleep',    7],
								['Sleep',    7],
								['Sleep',    7],
								['Sleep',    7]
							]);

							options = {
							  title: 'Daily Activities Report',
							   is3D: true,
							};

							chart = new google.visualization.PieChart(document.getElementById('piechart'));
							chart.draw(data, options);
							google.visualization.events.addListener(chart, 'select', selectHandler);
							google.visualization.events.addListener(chart, 'click', clickHander);
						}
					  
					  
						function selectHandler(e) {
							console.log('re');
							var selection = [];
							var selection1 = chart.getSelection();
							var row = selection1[0].row;
							var click_date = data.getValue(row,0);
							console.log(click_date);
							//get_click_data('day_ajax',click_date);
							if(selection1.length == 0){
								if(clicked !== false){
									selection = selection.filter(function (el) {
									  return el.row !== clicked;
									}
								)}
							}else{
								selection = selection.concat(selection1);  
							}
							console.log(selection);
							chart.setSelection(selection);
						}
						
						function clickHander(e) {
							console.log('ra');
							var target = e.targetID.split("#");
							if(target.length == 3){
								var row = parseInt(target[2]);
								clicked = row;
							}else{
								clicked = false
							}
						}
						
					</script>
					<script>
					function ajax_get_result(emp_code,emp_name)
					{
						//alert(emp_code);
					$.get("<?php echo base_url() ?>admin/get_employee_tasks/"+emp_code,function(data, status){
						$("#myModal_div").find(".modal-title").html(emp_name);
						$("#myModal_div").find(".modal-body").html(data);
						$("#myModal_div").modal('show');
					});
					return false;
					}
					
					</script>
					
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

</div>
</body>