<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Category</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_category'; ?>" method="POST">
			  
			  <div class="form-group">
				<label ><b>Department Name : </b></label>
				<select class="form-control" name="dept_id">
					<?php 
						foreach(get_all_record_by_code('dept') as $dept_record){
							if($dept_record->dept_id == $category_data->dept_id){
								echo '<option value="'.$dept_record->dept_id.'" selected>'.ucfirst($dept_record->dept_name).'</option>';
							}else{
								echo '<option value="'.$dept_record->dept_id.'">'.ucfirst($dept_record->dept_name).'</option>';
							}
						} 
					?>
				</select>
			  </div>
			  
			  <div class="form-group">
				<label ><b>Category Name : </b></label>
				<textarea  name="category_txt" class="form-control"><?php echo $category_data->category_name; ?></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Category</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_category'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>Department Name : </b></label>
				<select class="form-control" name="dept_id">
					<?php 
						foreach(get_all_record_by_code('dept') as $dept_record){
							echo '<option value="'.$dept_record->dept_id.'">'.ucfirst($dept_record->dept_name).'</option>';
						}
					?>
				</select>
			  </div>
			  <div class="form-group">
				<label><b>Category Name : </b></label>
				<textarea  name="category_txt" class="form-control"></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Category</h3>
		</div>
		<div class="modal-body">
		  <div class="form-group">
			<label ><b>Department Name : </b></label>
			<span><?php 
					$deptData = get_record_by_id_code('dept',$category_data->dept_id);
					$deptName = $deptData->dept_name;
					echo $deptName;
				?></span>
		  </div>
		  <div class="form-group">
			<label ><b>Category Name : </b></label>
			<span><?php echo $category_data->category_name; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

