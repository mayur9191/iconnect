<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Daily task Report</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Overall Daily Task Report 
					</div>
					<div class="row">
						<div class="col-md-6">
							<div id="piechart" style="width: 400px; height: 400px;"></div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-primary" style="top:55px;position:relative;">
								<span class="btn backtab btn-block">Daily Task Report Summary </span>
								<div class="panel-body inbox-menu">
									<ul style="list-style-type:none;">
									<?php $record = json_decode($report_data);
										  foreach($record as $rec){	
										 // print_r($rec);	
									?>
										<li style="width:200px;padding:5px;">
										<a href="javascript:void(0)" onclick="ajax_get_result('<?php echo $rec->emp_code; ?>','<?php echo$rec->u_name; ?>')"><?php echo $rec->u_name; ?>
										<span class="label label-primary pull-right" ><?php echo $rec->count; ?></span></a></li> 
										  <?php } ?>
									</ul>	  
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->

<!-- Modal -->
<div id="myModal_div" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer" style="border-top:none;">
       <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div> 

</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--datatable js--->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
var data1 = '<?php print_r($report_data); ?>';
var ddd = '';
//alert(arr);
</script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
	  
      function drawChart() {

        var data = new google.visualization.DataTable();
		data.addColumn('string', 'IT Employee');
        data.addColumn('number', 'Count');
		var jsonData = $.parseJSON(data1);
		//Parse data into Json
		 var jsonData = $.parseJSON(data1);
		 ddd = jsonData;
		 for (var i = 0; i < jsonData.length; i++) {
		   data.addRow([jsonData[i].u_name, parseInt(jsonData[i].count)]);
		 }
        var options = {
          title: 'Daily Task Report',
		  is3D: true,
		  legend:{position:'none'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
	<script>
	function ajax_get_result(emp_code,emp_name)
	{
		//alert(emp_code);
	$.get("<?php echo base_url() ?>admin/get_employee_tasks/"+emp_code,function(data, status){
		$("#myModal_div").find(".modal-title").html(emp_name);
		$("#myModal_div").find(".modal-body").html(data);
		$("#myModal_div").modal('show');
    });
	return false;
	}
	
	</script>

