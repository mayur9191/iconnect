<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit IP link</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_iplink'; ?>" method="POST">
			  <div class="form-group">
				<label ><b>IP : </b></label>
				<input type="hidden" name="iplink_id" value="<?php echo $iplink_data->ip_monitor_id; ?>" />
				<input type="" name="implink_name" class="form-control" value="<?php echo $iplink_data->ip_number; ?>" />
			  </div>
			  <div class="form-group">
				<label ><b>IP Desc : </b></label>
				<textarea  name="implink_url" class="form-control"><?php echo $iplink_data->ip_email_id; ?></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add IP link</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_iplink'; ?>" method="POST">
			
			  <div class="form-group">
				<label ><b>IP  : </b></label>
				<input type="" name="implink_name" class="form-control" value="" />
			  </div>
			  
			  <div class="form-group">
				<label ><b>IP Desc : </b></label>
				<textarea  name="implink_url" class="form-control"></textarea>
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View IP link</h3>
		</div>
		<div class="modal-body">
		  
		  <div class="form-group">
			<label ><b>IP : </b></label>
			<span><?php echo $iplink_data->ip_number; ?></span>
		  </div>
		  <div class="form-group">
			<label ><b>IP Desc : </b></label>
			<span><?php echo $iplink_data->ip_email_id; ?></span>
		  </div>
		  
		</div>
		
	</div>
</div>

<?php } ?>

