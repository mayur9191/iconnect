<?php if($action == 'edit') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Edit Quote</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/edited_thought'; ?>" method="POST">
			  <div class="form-group">
				<label >Quote</label>
				<input type="hidden" name="thog_id" value="<?php echo $thought_data->thought_id; ?>" />
				<!--<input type="text" name="thought_txt" value="<?php //echo $thought_data->thought_text; ?>" class="form-control" />-->
				<textarea  name="thought_txt" class="form-control"><?php echo $thought_data->thought_text; ?></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>

<?php if($action == 'add') { ?>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Add Quote</h3>
		</div>
		<div class="modal-body">
			<!-- content goes here -->
			<form action="<?php echo base_url().'admin/added_thought'; ?>" method="POST">
			  <div class="form-group">
				<label >Quote</label>
				<textarea  name="thought_txt" class="form-control"></textarea>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
			
		</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($action == 'view') { ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">View Quote</h3>
		</div>
		<div class="modal-body">
		<center>
			<span>
				<?php echo $thought_data->thought_text; ?>
			</span>
		</center>
		</div>
		
	</div>
</div>

<?php } ?>

