<?php foreach($iplink_data as $iplink_record) { 
							 
	if($iplink_record->ip_email_shoot == 0){
		$clas = 'ace-icon fa fa-toggle-off bigger-130';
		$status = 'Active';
	}else{
		$clas = 'ace-icon fa fa-toggle-on bigger-130';
		$status = 'Inactive';
	}
?>
	<tr>
		<td><?php echo $iplink_record->ip_number; ?></td>
		<td><?php echo $iplink_record->ip_email_id; ?></td>
		<td><?php if($iplink_record->link_down_count > 0) { ?>
				<img src="<?php echo base_url().'assets/new_gif/red.png'; ?>" width="15px" />
			<?php }else{ ?>
				<img src="<?php echo base_url().'assets/new_gif/green.png'; ?>" width="15px" />
			<?php } ?>
			&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $iplink_record->link_down_count; ?>&nbsp;&nbsp;&nbsp;&nbsp;
			<?php if($iplink_record->link_down_count > 10) { 
					echo '(<span style="color:red;">Please check link is permanently down</span>)';
				  }
			?>
		</td>
		<td class="hidden-480">
			<span class="label label-sm label-default" id="stat_<?php echo $iplink_record->ip_monitor_id; ?>"><?php echo $status; ?></span>
		</td>

		<td>
			<div class="action-buttons">
				<a class="blue" href="#" onclick="view_record('<?php echo $iplink_record->ip_monitor_id; ?>');" title="View IP link">
					<i class="ace-icon fa fa-search-plus bigger-130"></i>
				</a>

				<a class="green" href="#" onclick="edit_record('<?php echo $iplink_record->ip_monitor_id; ?>');" title="Edit IP link">
					<i class="ace-icon fa fa-pencil bigger-130"></i>
				</a>

				<span class="red" data-val="<?php echo $iplink_record->ip_monitor_id; ?>" title="Email shoot status">
					<i class="<?php echo $clas; ?>"></i>
				</span>
				
			</div>										
		</td>
	</tr>
<?php } ?>