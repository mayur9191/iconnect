
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">Helpdesk Emails</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<?php echo ($msg !='')? '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg">'.$msg.'</span></div>' : ''; ?>
			<div class="alert alert-success alert-dismissable a" style="display:none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span id="success_msg"></span></div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Helpdesk Emails</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">
						Helpdesk Email List
					</div>
					
					<div>
						<table id="dynamic-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<tr>
										<th>Sr no</th>
										<th>Mail Date</th>
										<th>Email From</th>
										<th>Subject</th>
										<th style="width:100px;">Body</th>
										<th>EmailId</th>
									</tr>
								</tr>
							</thead>

							<tbody id="ip_data">
								<?php $i=1;
								foreach($emailData as $email){ ?>
								  <tr>
									<td><?php echo $i; ?></td>
									<td><?php echo date('d-m-Y',strtotime($email->email_created)); ?></td>
									<td><?php echo $email->email_from; ?></td>
									<td><?php echo $email->email_subject; ?></td>
									<td style="width:100px;"><div><?php echo $email->email_body; ?></div></td>
									<td><?php echo $email->outlook_email_id; ?></td>
									<?php $i++; ?>
								  </tr>
								<?php } ?> 
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->



<!-- Modal start -->
  <div class="modal fade" id="myModal" role="dialog">
	
  </div>
<!-- Modal end -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">

</script>