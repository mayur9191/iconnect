<div class="page-content">
			
			<div class="row">
				<div class="col-xs-12">
					<h3 class="header smaller lighter blue">Daily Task Report</h3>

					<div class="clearfix">
						<div class="pull-right tableTools-container"></div>
					</div>
					<div class="table-header">Daily Task Report</div>
					<div>
						<table  id="myTable" class="table table-striped table-bordered table-hover">
						<thead>
						  <tr>
							<th>Sr no</th>
							<th>Task name</th>
							<th>Start date</th>
							<th>End date</th>
							<th>Task activity</th>
							<th>Task priority</th>
							<th>Task status</th>
							<th>Remarks</th>
						  </tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($all_tasks as $task): ?>
					  <tr>
						<td><?php echo $i; ?></td>
						<td><?php echo ucfirst($task['project_name']); ?></td>
						<td><?php echo date('d-m-Y',strtotime($task['start_date'])); ?></td>
						<td><?php echo date('d-m-Y',strtotime($task['end_date'])); ?></td>
						<td><?php echo ucfirst($task['task_activity']); ?></td>
						<td><?php echo ucfirst($task['task_priority']); ?></td>
						<td>
						<?php 
								$task_status='';
								switch($task['task_status']){
								case 'not_started':
									$task_status = 'Not Started';
								break;
								case 'in_progress':
									$task_status = 'In-Process';
								break;
								case 'completed':
									$task_status = 'Completed';
								break;
								
						} echo $task_status; ?>
						</td>
						<td><?php echo $task['remarks']; ?></td>
						<?php $i++; ?>
					  </tr>
					 <?php endforeach; ?> 
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!-- /.page-content -->

<!--datatable js--->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function(){
	$('#myTable').DataTable();
});
</script>