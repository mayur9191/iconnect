<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url().'assets/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url().'assets/' ?>bootstrap/js/bootstrap.min.js"></script>
	
    <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	
	<style>
		@media (min-width: 800px){ .form-control{width: 135%;font-size:12px;} }
		
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
	</style>
</head>
<body style="background-color: lightgray;">
<?php include APPPATH . "views/includs/top_navbar.php" ?>

<div>&nbsp;</div><br>
	<div class="col-md-1"></div>
	<div class="container col-md-10" style="background-color: white;">
		
		
		<div class="page-header">
			<center style="padding-top: 20px;">
				<!--<h3>Skeiron Group</h3>-->
				<img src="<?= base_url().'assets/images/logo/Logo.png' ?>" style="width: 24%;float: right;" />
				<h4>Learning & Development</h4>
				<h4>Individual Training Needs Identification Survey FY 2017 - 2018</h4>
			</center>
		</div>
		
		<!--<div class="col-md-2"></div>-->
		<?php if($empData['is_submit'] == 1){ ?>
			<fieldset id="fidset1" disabled="disabled">
		<?php }else{ ?>
		<?php } ?>
		
		<form class="form-horizontal col-md-12" role="form" action="<?= 'submit_survey_v2' ?>" method="POST" >		
		
			<div class="form-group col-md-12">
				<h6 class="col-sm-6" style="margin-left:20px;color:red;">	Note - all fields marked * are mandatory.</h6>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of employee <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_name" value="<?= $empData['emp_name'] ? $empData['emp_name'] :'' ;  ?>" placeholder="" class="form-control" pattern="[A-Za-z ]+" title="Please enter only chracter" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of business <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="business_name" value="<?= $empData['business_name'] ? $empData['business_name'] :'' ;  ?>" placeholder="" pattern="[A-Za-z ]+" title="Please enter only chracter" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Employee code <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_code" value="<?= $empData['emp_code'] ? $empData['emp_code'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Location <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_location" value="<?= $empData['emp_location'] ? $empData['emp_location'] :'' ;  ?>" placeholder="" pattern="[A-Za-z ]+" title="Please enter only chracter" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of reporting manager <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_reporting_manager" value="<?= $empData['emp_reporting_manager'] ? $empData['emp_reporting_manager'] :'' ;  ?>" placeholder="" pattern="[A-Za-z ]+" title="Please enter only chracter" required="required" class="form-control" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of HOD <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_hod" value="<?= $empData['emp_hod'] ? $empData['emp_hod'] :'' ;  ?>" placeholder="" class="form-control" pattern="[A-Za-z ]+" title="Please enter only chracter" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				</div>
			</div>
			<br>
			<div style="margin-left:20px;">
				<div class="form-group col-md-12">
					<h6 class="col-sm-8">Please fill in the prioritised training needs in the below options </h6>
				</div>
				
				<div class="col-md-1"></div>
				<div class="form-group col-md-10">
					<label class="control-label">1.Please select from the dropdown the titles of your Top two Leadership / Managerial Development Training Needs</label>
					<div class="col-sm-9">
					</div>
				</div>
				
				<div class="form-group col-md-12">
					<div class="form-group col-md-6">                    
						<label class="col-sm-6 control-label"><h5>Category</h5></label>
						<div class="col-sm-6">
							<select id="ldp_mdp_1" name="ldp_mdp_1" class="form-control">
								<option value="">Please select </option>
								<?php foreach($ldp_mdp_cat as $ldp_mdp_record){ ?>
										<option value="<?= $ldp_mdp_record->tbl_primary_id ?>" data-value="<?= $ldp_mdp_record->category ?>" <?= $ldp_mdp_record->tbl_primary_id==$empData['ldp_mdp_1'] ? 'selected=""' :'' ;  ?> ><?= $ldp_mdp_record->category ?></option>
								<?php } ?>
							</select>
						</div>
                    </div>	
					<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Sub-Category</h5></label>
					<div class="col-sm-6">
						<select id="sub_ldp_mdp_1" name="sub_ldp_mdp_1" class="form-control">
						<?php getDrillDownSelect('ldp_mdp',$empData['sub_ldp_mdp_1'],$empData['ldp_mdp_1']); ?>
						</select>
					</div>
                    </div>
					<div class="form-group col-md-6">
						<label class="col-sm-6 control-label"><h5>Category</h5></label>
						<div class="col-sm-6" >
							<select id="ldp_mdp_2" name="ldp_mdp_2" class="form-control">
								<option value="">Please select </option>
								<?php foreach($ldp_mdp_cat as $ldp_mdp_record){ ?>
										<option value="<?= $ldp_mdp_record->tbl_primary_id ?>" data-value="<?= $ldp_mdp_record->category ?>" <?= $ldp_mdp_record->tbl_primary_id==$empData['ldp_mdp_2'] ? 'selected=""' :'' ;  ?> ><?= $ldp_mdp_record->category ?></option>
								<?php } ?>
							</select>
						</div>
                    </div>	
					<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Sub-Category</h5></label>
					<div class="col-sm-6">
						<select class="form-control" id="sub_ldp_mdp_2" name="sub_ldp_mdp_2">
						<?php getDrillDownSelect('ldp_mdp',$empData['sub_ldp_mdp_2'],$empData['ldp_mdp_2']); ?>
						</select>
					</div>
                    </div>
					
					
					<div class="col-md-12"></div>
				</div>
				
				<div class="col-md-1"></div>
				<div class="form-group col-md-10">
					<label class="control-label">2. Please select from the dropdown the titles of your Top Two Functional /  Technical Training Needs</label>
					<div class="col-sm-9">
					</div>
				</div>
				
				<div class="form-group col-md-12">
				<div class="form-group col-md-6">
					<label class="col-sm-6 control-label"><h5>Category</h5></label>
					<div class="col-sm-6">
					
						<select id="fun_tech_1" name="fun_tech_1" class="form-control">
							<option value="">Please select </option>
							<?php foreach($fun_tech_cat as $fun_tech_record){ ?>
									<option value="<?= $fun_tech_record->tbl_primary_id ?>" data-value="<?= $fun_tech_record->category ?>" <?= $fun_tech_record->tbl_primary_id==$empData['fun_tech_1'] ? 'selected=""' :'' ;  ?> ><?= $fun_tech_record->category ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				
				<div class="form-group col-md-6">
					<label class="col-sm-6 control-label"><h5>Sub-Category</h5></label>
					<div class="col-sm-6">
						<select id="sub_fun_tech_1" name="sub_fun_tech_1" class="form-control">
						<?php getDrillDownSelect('fun_tech',$empData['sub_fun_tech_1'],$empData['fun_tech_1']); ?>
						</select>
					</div>
                </div>
				
				<div class="form-group col-md-6">
					<label class="col-sm-6 control-label"><h5>Category</h5></label>
					<div class="col-sm-6">
						<select id="fun_tech_2" name="fun_tech_2" class="form-control">
							<option value="">Please select </option>
							<?php foreach($fun_tech_cat as $fun_tech_record){ ?>
									<option value="<?= $fun_tech_record->tbl_primary_id ?>" data-value="<?= $fun_tech_record->category ?>" <?= $fun_tech_record->tbl_primary_id==$empData['fun_tech_2'] ? 'selected=""' :'' ;  ?> ><?= $fun_tech_record->category ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				
				<div class="form-group col-md-6">
					<label class="col-sm-6 control-label"><h5>Sub-Category</h5></label>
					<div class="col-sm-6">
						<select id="sub_fun_tech_2" name="sub_fun_tech_2" class="form-control">
						<?php getDrillDownSelect('fun_tech',$empData['sub_fun_tech_2'],$empData['fun_tech_2']); ?>
						</select>
					</div>
                </div>
				</div>
				<div class="col-md-1"></div>
				<div class="form-group col-md-10">
					<label class="control-label">3.Please select the titles of your Top Two Personality Development / Behavioral Training Needs</label>
					<div class="col-sm-9"></div>
				</div>
				<div class="form-group col-md-12">
				<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Category</h5></label>
					<div class="col-sm-6">
						<select id="pd_beha_1" name="pd_beha_1" class="form-control">
							<option value="">Please select </option>
							<?php foreach($pd_beha_cat as $pd_beha_record){ ?>
									<option value="<?= $pd_beha_record->tbl_primary_id ?>" data-value="<?= $pd_beha_record->category ?>" <?= $pd_beha_record->tbl_primary_id==$empData['pd_beha_1'] ? 'selected=""' :'' ;  ?> ><?= $pd_beha_record->category ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Sub-Category</h5></label>
					<div class="col-sm-6">
						<select  class="form-control" id="sub_pd_beha_1" name="sub_pd_beha_1">
						<?php getDrillDownSelect('pd_beha',$empData['sub_pd_beha_1'],$empData['pd_beha_1']); ?>
						</select>
					</div>
                </div>
				<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Category</h5></label>
					<div class="col-sm-6">
						<select id="pd_beha_2" name="pd_beha_2" class="form-control">
							<option value="">Please select </option>
							<?php foreach($pd_beha_cat as $pd_beha_record){ ?>
									<option value="<?= $pd_beha_record->tbl_primary_id ?>" data-value="<?= $pd_beha_record->category ?>" <?= $pd_beha_record->tbl_primary_id==$empData['pd_beha_2'] ? 'selected=""' :'' ;  ?> ><?= $pd_beha_record->category ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Sub-Category</h5></label>
					<div class="col-sm-6">
						<select  class="form-control" id="sub_pd_beha_2" name="sub_pd_beha_2">
						<?php getDrillDownSelect('pd_beha',$empData['sub_pd_beha_2'],$empData['pd_beha_2']); ?>
						</select>
					</div>
                </div>
				</div>	
				<div class="col-md-1"></div>
				<div class="form-group col-md-10">
					<label class="control-label">4. Please select the titles of your Top Two QHSE Training Need..</label>
					<div class="col-sm-9"></div>
				</div>
				<div class="form-group col-md-12">
				<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Category</h5></label>
					<div class="col-sm-6">
						<select id="qhse_list_1" name="qhse_list_1" class="form-control">
							<option value="">Please select </option>
							<?php foreach($qhse_list as $qhse_record){ ?>
									<option value="<?= $qhse_record->tbl_primary_id ?>" <?= $qhse_record->tbl_primary_id==$empData['qhse_list_1'] ? 'selected=""' :'' ;  ?> ><?= $qhse_record->title ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				
				<div class="form-group col-md-6">
                    
					<label class="col-sm-6 control-label"><h5>Category</h5></label>
					<div class="col-sm-6">
						<select id="qhse_list_2" name="qhse_list_2" class="form-control">
							<option value="">Please select </option>
							<?php foreach($qhse_list as $qhse_record){ ?>
									<option value="<?= $qhse_record->tbl_primary_id ?>" <?= $qhse_record->tbl_primary_id==$empData['qhse_list_2'] ? 'selected=""' :'' ;  ?> ><?= $qhse_record->title ?></option>
							<?php } ?>
						</select>
					</div>
                </div>
				
				</div>
				
				<div class="form-group col-md-8"  style="margin-left: 60px;">
					<label class="control-label">5. Please insert maximum two training needs not available in the Drilldown list</label>
					<div class="col-sm-9"></div>
				</div>
				
				<div class="col-md-3"></div>
				<div class="form-group col-md-6">
					<label for="firstName" class="col-sm-6 control-label"><h5>Training Need 1</h5></label>
					<div class="col-sm-6">
						<input type="text" name="tr_need_1" maxlength="100" class="form-control" value="<?= $empData['tr_need_1'] ? $empData['tr_need_1'] :'' ;  ?>" />
					</div>
				</div>
				
				<div class="form-group col-md-6">
					<label for="firstName" class="col-sm-2 control-label"><h5></h5></label>
					<div class="col-sm-6">
						<input type="text" name="tr_respondent_1" placeholder="For internal use" maxlength="100" class="form-control" value="" />
					</div>
				</div>
				
				<div class="col-md-12"></div>
				<div class="form-group col-md-6">
					<label class="col-sm-6 control-label"><h5>Training Need 2</h5></label>
					<div class="col-sm-6">
						<input type="text" name="tr_need_2" maxlength="100" class="form-control" value="<?= $empData['tr_need_2'] ? $empData['tr_need_2'] :'' ;  ?>" />
					</div>
				</div>
				
				<div class="form-group col-md-6">
					<label class="col-sm-2 control-label"><h5></h5></label>
					<div class="col-sm-6">
						<input type="text" name="tr_respondent_2" placeholder="For internal use" maxlength="100" class="form-control" value="" />
					</div>
				</div>
				
				<div class="col-md-12"></div>
				<?php  if($empData['is_submit'] == 0){ ?>
					<div class="form-group" style="margin-left: 20%;">
						<div class="form-group col-md-6">
							<div class="col-sm-4 col-sm-offset-3">
								<input type="submit" class="btn btn-primary btn-block" name="submit" value="Save" />
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="col-sm-4">
								<input type="submit" class="btn btn-primary btn-block" name="submit" value="Submit" />
							</div>
						</div>
					</div>
				<?php  } ?>
				
				</div>
			</div>
		
		</form> <!-- /form -->
		</fieldset>
	</div> <!-- ./container -->
</body>
</html>

<script>
$(document).ready(function(){
	
	
	
	//type selection 
	$("#ldp_mdp_1").on("change",function(){
		console.log($(this).find(':selected').attr('data-value'));
		var surveyType = $(this).find(':selected').attr('data-value');
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType:'ldp_mdp',surveyText:surveyType
		},function (rps) {
			$("#sub_ldp_mdp_1").html(rps);
			console.log(rps);
		});
	});
	
	$("#ldp_mdp_2").on("change",function(){
		var surveyType = $(this).find(':selected').attr('data-value');
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType:'ldp_mdp',surveyText:surveyType
		},function (rps) {
			$("#sub_ldp_mdp_2").html(rps);
		});
	});
	
	$("#fun_tech_1").on("change",function(){
		var surveyType = $(this).find(':selected').attr('data-value');
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType:'fun_tech',surveyText:surveyType
		},function (rps) {
			$("#sub_fun_tech_1").html(rps);
		});
	});
	
	$("#fun_tech_2").on("change",function(){
		var surveyType = $(this).find(':selected').attr('data-value');
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType:'fun_tech',surveyText:surveyType
		},function (rps) {
			$("#sub_fun_tech_2").html(rps);
		});
	});
	
	$("#pd_beha_1").on("change",function(){
		var surveyType = $(this).find(':selected').attr('data-value');
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType:'pd_beha',surveyText:surveyType
		},function (rps) {
			$("#sub_pd_beha_1").html(rps);
		});
	});
	
	$("#pd_beha_2").on("change",function(){
		var surveyType = $(this).find(':selected').attr('data-value');
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType:'pd_beha',surveyText:surveyType
		},function (rps) {
			$("#sub_pd_beha_2").html(rps);
		});
	});
	
});
</script>

