<html>
  <head>
    <!--Load the AJAX API-->
	<script type="text/javascript" src="<?= base_url().'theme/' ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
		
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
		data.addColumn({type: 'string', role: 'tooltip'});
        data.addRows([
          ['Not Attended', 500, 'Not attended the survey'],
          ['Attended', 800, 'Attended the survey']
        ]);

        // Set chart options
        var options = {'title':'Learning & Development \n TNI Survey FY 2017 - 2018',
                       'width':600,
                       'height':500,
					   colors: ['RED', 'GREEN']};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
		
		// Wait for the chart to finish drawing before calling the getImageURI() method.
		google.visualization.events.addListener(chart, 'ready', function () {
			//chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
			document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '">Printable version</a>';
			console.log(chart_div.innerHTML);
		});
		
        chart.draw(data, options);
      }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
	 <div id='png'></div>
  </body>
</html>