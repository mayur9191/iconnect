<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <?php include APPPATH . "views/includs/hedder_code.php" ?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css"  rel="stylesheet" type="text/css"  />
	<style>
		.mandatory{color:red;}
		.margin_left {margin-left: 60px}
		.control-label{font-size:12px !important }
		.big {width:1.5em;height:1.5em;}	
		
		.skul {
			list-style-type: none;
			overflow: hidden;
		}

		.skli{
			margin-left:15px;
			text-align: center;
			padding-bottom:20px;
			float: left;
		}
		.span_size{font-size:20px;}
	</style>
</head>

<body style="background-color: lightgray;">
<?php include APPPATH . "views/includs/top_navbar.php" ?>
<div>&nbsp;</div><br><br><br>
	<div class="col-md-1"></div>
	<div class="container col-md-10" style="background-color: white;">
		
		<?php //print_r($filledFlag); ?>
		<!--<div class="table-responsive">          
		  <table class="table borderless">
			<tbody>
			  <tr>
				<td class="col-md-3">Technical / Functional Training Need 1</td>
				<td class="col-md-3">
					<select id="tech_func_1" name="tech_func_1" class="form-control">
						<option value="">Please select </option>
						<?php foreach($get_tec_func_data as $tech_func_record){ ?>
							<option <?= $empData['tech_func_training_need_one']==$tech_func_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $tech_func_record->tbl_primary_id ?>"><?= $tech_func_record->title ?></option>
						<?php } ?>
					</select>
				</td>
				<td class="col-md-1">Option 1 </td>
				<td class="col-md-3">
					<select id="tech_func_1" name="tech_func_1" class="form-control">
						<option value="">Please select </option>
						<?php foreach($get_tec_func_data as $tech_func_record){ ?>
							<option <?= $empData['tech_func_training_need_one']==$tech_func_record->tbl_primary_id ? 'selected=""' :'' ?> value="<?= $tech_func_record->tbl_primary_id ?>"><?= $tech_func_record->title ?></option>
						<?php } ?>
					</select>
				</td>
			  </tr>
			</tbody>
		  </table>
		</div>-->
		
		
	
		<div class="page-header">
			<center>
				<!--<h3>Skeiron Group</h3>-->
				<img src="<?= base_url().'assets/images/logo/Logo.png' ?>" style="width: 24%;" />
				
				<h4>Employee apparel size</h4>
			</center>
		</div>
		<!--<div class="col-md-2"></div>-->
		<form id="apparelform" class="form-horizontal col-md-12" role="form" action="<?= 'saveRequest' ?>" method="POST" >	
		<?php if($filledFlag == 0){ ?>
		<fieldset>
		<?php }else{  ?>
		<fieldset disabled="disabled">
		<?php } ?>
			<div class="form-group col-md-12">
				<h6 class="col-sm-6" style="margin-left:20px;color:red;">	Note - All fields marked * are mandatory.</h6>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Employee code <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<?php if(!empty($empApparel)&& $empApparel[0]->emp_code !='' ){ ?>
						<input type="text" id="firstName" name="emp_code" value="<?php echo $empApparel[0]->emp_code;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<?php }else{ ?>
						<input type="text" id="firstName" name="emp_code" value="<?= $empData['employee_code'] ? $empData['employee_code'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<?php } ?>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of employee <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
				<?php if(!empty($empApparel)&& $empApparel[0]->full_name !='' ){ ?>
					<input type="text" id="emp_name" name="emp_name" value="<?= $empApparel[0]->full_name;  ?>" placeholder="" class="form-control" required="required" autofocus />
				<?php }else{ ?>
					<input type="text" id="emp_name" name="emp_name" value="<?= $empData['employee_name'] ? $empData['employee_name'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
				<?php } ?>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Location <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
				<?php if(!empty($empApparel)&& $empApparel[0]->location !='' ){ ?>
					<input type="text" id="emp_location" name="emp_location" value="<?= $empApparel[0]->location;  ?>" placeholder="" class="form-control" required="required" autofocus />
				<?php }else{ ?>
					<input type="text" id="emp_location" name="emp_location" value="<?= $empData['location'] ? $empData['location'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
				<?php } ?>
				</div>
			</div>
			
			<!--<div class="form-group col-md-6">
				<label for="firstName" class="col-sm-6 control-label"><h5>Name of HOD <span class="mandatory">*</span></h5></label>
				<div class="col-sm-6">
					<input type="text" id="firstName" name="emp_hod" value="<?= $empData['name_of_hod'] ? $empData['name_of_hod'] :'' ;  ?>" placeholder="" class="form-control" required="required" autofocus />
					<!--<span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
				<!--</div>
			</div>-->
			<br>
			<div style="margin-left:20px;">
			    
				<div class="form-group col-md-12">
					<h3 class="col-sm-8">Kindly choose your apparel size below  </h3>
				</div>
				
				<div class="col-md-1"></div>
				
				<div class="form-group col-md-12">
					<ul class="skul">
						<li class="skli"><b><h4 style="margin-top:0px;font-weight:700;">Shirt size:<span class="mandatory">*</span></h4></b></li>
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 30  ) ? "checked": "" ?>  value="30" name="shirt_size" class="big"><span class="span_size">30</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 32  ) ? "checked": "" ?> value="32" name="shirt_size" class="big"><span class="span_size">32</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 34  ) ? "checked": "" ?> value="34" name="shirt_size" class="big"><span class="span_size">34</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 36  ) ? "checked": "" ?> value="36" name="shirt_size" class="big"><span class="span_size">36</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 38  ) ? "checked": "" ?> value="38" name="shirt_size" class="big"><span class="span_size">38</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 40  ) ? "checked": "" ?> value="40" name="shirt_size" class="big"><span class="span_size">40</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 42  ) ? "checked": "" ?> value="42" name="shirt_size" class="big"><span class="span_size">42</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 44  ) ? "checked": "" ?> value="44" name="shirt_size" class="big"><span class="span_size">44</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size == 46  ) ? "checked": "" ?> value="46" name="shirt_size" class="big"><span class="span_size">46</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size_other == 1) ? "checked" : "" ?>  value="" name="shirt_size" class="big" id="shirt_size"><span class="span_size">other</span></li>
						
						<li class="skli"><input type="input" id="shirt_size_text" class="form-control"  value="<?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size_other == 1) ? $empApparel[0]->shirt_size : "" ?>" name="shirt_size_text" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_size_other == 1) ? "style=width:100px;" : "style=display:none;width:100px;" ?> ></li>
					</ul>
					
					<ul class="skul">
						<li class="skli"><b><h4 style="margin-top:0px;font-weight:700;">Shirt collar size:<span class="mandatory">*</span></h4></b></li>
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == 14  ) ? "checked": "" ?> value="14" name="shirt_collar_size" class="big"><span class="span_size">14</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == '14.5'  ) ? "checked": "" ?> value="14.5" name="shirt_collar_size" class="big"><span class="span_size">14 ½</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == 15  ) ? "checked": "" ?> value="15" name="shirt_collar_size" class="big"><span class="span_size">15</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == '15.5'  ) ? "checked": "" ?>  value="15.5" name="shirt_collar_size" class="big"><span class="span_size">15 ½</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == 16  ) ? "checked": "" ?>  value="16" name="shirt_collar_size" class="big"><span class="span_size">16</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == '16.5'  ) ? "checked": "" ?> value="16.5" name="shirt_collar_size" class="big"><span class="span_size">16 ½</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == 17  ) ? "checked": "" ?> value="17" name="shirt_collar_size" class="big"><span class="span_size">17</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size == '17.5'  ) ? "checked": "" ?> value="17.5" name="shirt_collar_size" class="big"/><span class="span_size">17 ½</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size_other == 1)? "checked" :"" ?>  value="" name="shirt_collar_size" class="big" id="shirt_collar_size"><span class="span_size">other</span></li>
						
						<li class="skli"><input type="input"  value="<?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size_other == 1)? $empApparel[0]->shirt_collar_size :"" ?>"  class="form-control" name="shirt_collar_size_text" <?php echo (!empty($empApparel)&& $empApparel[0]->shirt_collar_size_other == 1)? "style=width:100px;" :"style=display:none;width:100px;" ?> id="shirt_collar_size_text"></li>
					</ul>
					
					<ul class="skul">
						<li class="skli"><b><h4 style="margin-top:0px;font-weight:700;">T-shirt size:<span class="mandatory">*</span></h4></b></li>
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 30  ) ? "checked": "" ?>  value="30"  name="t_shirt_size" class="big"><span class="span_size">30</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 32  ) ? "checked": "" ?> value="32" name="t_shirt_size" class="big"><span class="span_size">32</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 34  ) ? "checked": "" ?> value="34" name="t_shirt_size" class="big"><span class="span_size">34</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 36  ) ? "checked": "" ?> value="36" name="t_shirt_size" class="big"><span class="span_size">36</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 38  ) ? "checked": "" ?> value=38"" name="t_shirt_size" class="big"><span class="span_size">38</span></li>
						
						<li class="skli"><input type="radio" required="required"  <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 40  ) ? "checked": "" ?> value="40" name="t_shirt_size" class="big"><span class="span_size">40</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 42  ) ? "checked": "" ?>  value="42" name="t_shirt_size" class="big"><span class="span_size">42</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 44  ) ? "checked": "" ?>  value="44" name="t_shirt_size" class="big"><span class="span_size">44</span></li>
						
						<li class="skli"><input type="radio" required="required" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size == 46  ) ? "checked": "" ?>  value="46" name="t_shirt_size" class="big"><span class="span_size">46</span></li>
						
						<li class="skli"><input type="radio" required="required"  value="" name="t_shirt_size" class="big" id="t_shirt_size"><span class="span_size">other</span></li>
						
						<li class="skli"><input type="input" id="t_shirt_size_text"  class="form-control" value="<?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size_other == 1 ) ? $empApparel[0]->t_shirt_size: "" ?>" name="t_shirt_size_text" <?php echo (!empty($empApparel)&& $empApparel[0]->t_shirt_size_other == 1 ) ? "style=width:100px;":"style=display:none;width:100px;" ?> ></li>
					</ul>
				</div>
				<div class="col-md-12"></div>
					
				     <div class="form-group" style="margin-left: 20%;">
						<div class="form-group col-md-6">
						<?php if($filledFlag == 0){ ?>
							<div class="col-sm-4 col-sm-offset-3">
								<input type="submit" class="btn btn-primary btn-block" name="submit" value="Submit" />
							</div>
							<?php }else{ 
								echo "Status : Information is submitted!";
								}
							?>
						</div>
						
					</div>
					
				
				</div>
			</div>
			</fieldset>
		</form> <!-- /form -->
			<br><br><br><br><br><br>
	</div> <!-- ./container -->

</body>
</html>
<?php include APPPATH . "views/includs/common_scripts.php" ?>
<script type="text/javascript">
$(document).ready(function () {
   /* $("#apparelform").validate({
        rules: {
            "shirt_size": {
                required: true,
                       },
			"shirt_collar_size": {
                required: true,
                       },
            "t_shirt_size": {
                required: true,
                       },
        },
        messages: {
            "shirt_size": {
                required: "Please select the Shirt Size",
               
            },
			"shirt_collar_size": {
                required: "Please select the Shirt Collar Size",
               
            },
			"t_shirt_size": {
                required: "Please select the T- Shirt Size",
               
            },
            
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });*/
	
	
	$("input:radio[name=t_shirt_size]").click(function(){
		if($("#t_shirt_size").prop("checked")){
			
			$("#t_shirt_size_text").show();
			$("#t_shirt_size_text").prop("required",true);
			
		}else{
			$("#t_shirt_size_text").hide();
			$("#t_shirt_size_text").val('');
			$("#t_shirt_size_text").prop("required",false);
		}
	});
	$("input:radio[name=shirt_collar_size]").click(function(){
		if($("#shirt_collar_size").prop("checked")){
			
			$("#shirt_collar_size_text").show();
			$("#shirt_collar_size_text").prop("required",true);
		}else{
			$("#shirt_collar_size_text").hide();
			$("#shirt_collar_size_text").val('');
			$("#shirt_collar_size_text").prop("required",false);
		}
	});
	$("input:radio[name=shirt_size]").click(function(){
		if($("#shirt_size").prop("checked")){
			
			$("#shirt_size_text").show();
			$("#shirt_size_text").prop("required",true);
		}else{
			$("#shirt_size_text").hide();
			$("#shirt_size_text").val('');
			$("#shirt_size_text").prop("required",false);
		}
	});
	$("#shirt_size_text").keyup(function(){
		
		if($("#shirt_size_text").val() != '' && !$.isNumeric($("#shirt_size_text").val())){
		
		alert("Please enter only numeric value");
		$("#shirt_size_text").val('');
		return false;
	}
		
	});
	$("#shirt_collar_size_text").keyup(function(){
		
		if($("#shirt_collar_size_text").val() != '' && !$.isNumeric($("#shirt_collar_size_text").val())){
		
		alert("Please enter only numeric value");
		$("#shirt_collar_size_text").val('');
		return false;
	}
		
	});
	$("#t_shirt_size_text").keyup(function(){
		
		if($("#t_shirt_size_text").val() != '' && !$.isNumeric($("#t_shirt_size_text").val())){
		
		alert("Please enter only numeric value");
		$("#t_shirt_size_text").val('');
		return false;
	}
	
	
		
	});
	//value.match(/^[a-zA-Z ]*$/)
	var regex = new RegExp("^[a-zA-Z ]+$");
	var emp_name = $("#emp_name").val();
	var emp_location = $("#emp_location").val();
	$("#emp_name").keyup(function(){
		//alert("hello");
		if($("#emp_name").val() != '' && regex.test($("#emp_name").val())== false){
		$("#emp_name").val(emp_name);
		alert("Please enter only characters values");
		
		return false;
	}
	});
	$("#emp_location").keyup(function(){
		//alert("hello");
		if($("#emp_location").val() != '' && regex.test($("#emp_location").val())== false){
		$("#emp_location").val(emp_location);
		alert("Please enter only characters values");
		
		return false;
	}
	});
});
</script>
<script>
$(document).ready(function(){
	//type selection 
	$(".leadership_train1").on("click",function(){
		console.log($(this).val());
		var surveyType = $(this).val();
		$.post('<?php echo base_url();?>survey/getDrillDown', {
			traingType: surveyType
		},function (rps) {
			console.log(rps);
		});
	});
	
});
</script>