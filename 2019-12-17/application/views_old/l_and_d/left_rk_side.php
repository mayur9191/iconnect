<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= 'L&D Portal'; //isset($titile)?$titile." Portal":'Portal'?></title>

	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/bootstrap.min.css"  />
	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/style.css"  />

	<link rel="stylesheet" href="<?=base_url().'assets/'?>css/fontawesome.css" />

	<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>intro/introjs.css" />
	<script type="text/javascript" src="<?= base_url().'assets/' ?>intro/intro.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php
	$CI = & get_instance();
	$logindata = $this->session->userdata('user_ldamp');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" media="screen">
<style>
.panel-body.a{ padding:0px 0px 0px 0px;}
.panel-default>.panel-heading {
    color: #f8ffff;
    background-color: #8fb729;
    border-color: #8fb729;
}
.list-group-item{
	border: 1px solid #8fb729;
}
.sidebox{
	border: none;
}
</style>	 
</head>
<body style="/*background-color: #dff0d8;*/">
<nav class="navbar navbar-default navbar-inverse navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid ">
    <div class=" topbar">
	<span class="pull-right hidden-xs" style="margin-top:4px; margin-left:4px;">
	Welcome <br><?= (isset($logindata['displayname']))? ucwords($logindata['displayname']) : ''; ?>
	</span>
		
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
			
            <a class="navbar-brand" href="<?= base_url() ?>" style="font-size: 22px;font-weight: 700;">
                <img alt="alternative text" src="<?= base_url() . 'assets/' ?>images/iconnect-logo.png"/>
            </a>
        </div>
        <div class="animated fadeIn" id="bs-example-navbar-collapse-1">
            <!--<div class="collapse navbar-collapse animated fadeIn" id="bs-example-navbar-collapse-1">-->
            <center>
                <ul class="nav navbar-nav animated fadeIn text16">
                  
                </ul>
               
            </center>
			<?php

			if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
				$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
			} else {

				$dp_image = base_url() . 'assets/images/45.png';
			}
			?>
            <ul class="nav navbar-nav navbar-right nopadding" style="margin-right:0px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $dp_image; ?>" style="width: 30px;border-radius: 20px;"  alt="profile"
                             class="img-responsive hidden-xs"/>
                    </a>
                    <ul class="dropdown-menu animated flipInX" role="menu">

                        <?php

                        if ($logindata['role'] == 1) {
                            ?>
                            
                            <li>
                                <a href="#" data-toggle="modal" data-target="#profile_slide">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                    Banner Slider
                                </a>
                            </li>
                            <?php
                        }
                        ?>
						<!--<li><a href="<?= base_url() . 'profile/edit' ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i> Edit Profile </a></li>
                        <li class="divider"></li>
                        <li><a href="#">Welcome <?= ucwords($user_ldamp['name']) ?></a></li>
                        <li class="divider"></li>-->
                        <li><a href="<?= base_url() . 'profile/logout' ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </div><!-- /.container-fluid -->
</nav>

<?php

if ($logindata['role'] == 1) {
    ?>

    <div id="profile_slide" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="post" enctype="multipart/form-data" class="bithdaypost"
                      action="<?= base_url() . 'profile/sliders' ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Profile Slide</h4>
                    </div>
                    <div class="modal-body">
                        <div id="wrapper" style="margin-top: 20px;">
                            <div id="image-holder" class="col-md-12 nopad"  style="display: flex; max-height:250px; ">
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <input class="fileUpload" name="image" class="" type="file"/>
                                <i><b>Note:</b> Upload image size in 520 x 200 </i>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-primary" type="submit" value="Upload Now"/>

                    </div>
                </form>
            </div>

        </div>
    </div>



    <?php
}
?>



<div class="container-fluid main">
<div class="row">
    <div class="col-md-12">

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
						
			
			<div class="col-xs-12 col-md-12 asdf-icon" data-step="1" data-intro="Profile picture upload">

				<?php
				if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
					$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
				} else {

					$dp_image = base_url() . 'assets/images/45.png';
				}
				?>
				<div class="col-md-12 nopad" data-toggle="modal" data-target="#update_dp" style="display: flex;color: #8fb729;font-weight:700;">
					<a href="<?php echo base_url().'learn_and_development';?>"><h3>Learning & Development</h3></a>
				</div>
				<div class="clear"></div>
			</div>


				<div class="col-xs-8 col-md-9" style="padding-left: 30px; padding-top: 10px; line-height: 20px;">
					<div class="clear"></div>
					<span class="clientuser-name2">
					</span>
				</div>
			<div class="clear"></div>


			<div class="clear"></div>

			<!------------- Start  ------------------------------------>
				 <!-- first column-->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopad">
				
					<div class="panel-group sidebox community_update" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h6 class="panel-title iconnect-psh5">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										About L & D
									</a>
								</h6>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'aboutus';?>">About Us</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'key_info';?>">Key Information At L & D</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'roles';?>">Role & Responsibilities</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										L & D Offerings
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Programmes_Offerred';?>" target="_blank">Programmes & Offerings</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/L_and_D_Calendars';?>" target="_blank">L&D Calenders</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Virtual Learning
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item">E Learning</li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Presentation_of_Trainings';?>" target="_blank">Presentation of Trainings</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFour">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
										L&D Procedures
									</a>
								</h4>
							</div>
							<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Forms';?>" target="_blank">Forms</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Processes';?>" target="_blank">Processs</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Templates';?>" target="_blank">Templates</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFive">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
										L&D Dashboards
									</a>
								</h4>
							</div>
							<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Mis';?>" target="_blank">MIS</a></li>
										<li class="list-group-item">Locational Training Data</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!------------- END    ------------------------------------>
        </div>