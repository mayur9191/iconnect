<?php include('left_rk_side.php'); ?>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox" style="text-align: justify;text-justify: inter-word;">
	   
		
		
		<div class="row">
		
            <div style="height:345px;overflow-y:scroll;padding: 5px 5px 5px 5px;">
				<h2 style="color: #8fb729;">Learning and Development at Skeiron</h2>

				<h5 style="color: #8fb729;"><b>INTRODUCTION</b></h5>
				<h5 style="color: #8fb729;"><b>About L & D Skeiron</b></h5>

				<p>L&D is established to operate as a Centre of Excellence under the HR, at Group Level and is responsible for providing value added training interventions to the entire Skeiron value chain.</p>

				<p>In the year 2016, Skeiron set on a mission to establish a Learning and Development function which will cater to its envisaged growth and ever-growing need of human capability development. While this has addressed one area which strongly emerged in the Employee Engagement Survey, it was the need of the hour too, to have a full-fledged function at the Group level, A multi-disciplinary L & D unit which is responsible for rolling out learning interventions across various business of Skeiron. Types of trainings conducted are Technical, Behavioral, Functional and Induction.</p>

				<h5 style="color: #8fb729;"><b>L & D Vision</b></h5>
				To be a partner in the Organizational Growth by establishing as an Integrated L& D Function.
				Establishing partnership with all internal and external stakeholders to transform Skeiron into a Learning Organization.

				<h5 style="color: #8fb729;"><b>L & D Mission</b></h5>
				To drive business results by establishing a competitive advantage for Skeiron as the best place for RE Community to learn and develop, by delivering the highest quality learning and organizational development initiatives, to meet the prioritized needs of Today and Tomorrow.

				<h5 style="color: #8fb729;"><b>L & D Total Value Proposition</b><h5>
				<ul>
					<li>Strong business Alignment to realise Organizational Vision and Mission</li>
					<li>Learning Solutions to address Business Drivers - e. g. Execution Excellence, Technology Leadership, Strengthening People & Structure, etc.</li>
					<li>Support Performance Management and Improvement</li>
					<li>Catalyze Skill building through Customized Trainings</li>
					<li>Effect Knowledge Management</li>
					<li>Facilitate Organizational Development</li>
					<li>Achieve Standardization thru’ a Process Driven Approach w. r. t. L & D</li>
					<li>Developing employees with necessary skills and build competence to meet their growth aspirations</li>
					<li>One Window solution for Recruitment, Orientation and Induction of Fresh recruits to Offer Best in Class Experience</li>
					<li>Structured Induction and On Boarding Programme for Lateral Joinees for a Competitive Experience</li>
				</ul>

				<h5 style="color: #8fb729;"><b>L & D – The 3 S Approach</b></h5>
				 Strategize - Alignment, Execution, Tracking, Improvement<br>
				 Structurize -  Hub and Spoke structure, Reporting, <br>
				 Strengthen - Standardization, Process Orientation, L & D Audits
			</div>
			
            </div>
			<!-- image slide code end -->
			
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
				<div class="clear"></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
				
				<div class="col-md-6" style="padding: 0px 8px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Image Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_gallery/l_and_d'; ?>" target="_blank">
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4194.jpg" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="col-md-6" style="padding: 0px 0px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Video Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/video_gallery'; ?>" target="_blank">
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_170500909.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_171408479.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_172338706.JPG" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>	
			</div>
			
		
		
 
        </div>
		
<?php include('right_rk_side.php'); ?>
