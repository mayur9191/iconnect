<?php include('left_rk_side.php'); ?>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox" style="text-align: justify;text-justify: inter-word;">
	 
			<div class="row">
		
                <div style="height:345px;overflow-y:scroll;padding: 5px 5px 5px 5px;">
					
					<h2 style="color: #8fb729;">SKEIRON’s PROGRESSIVE NEEDS </h2>
					<p>Skeiron is a fast-growing organization and to aid its rapid growth several developmental needs have been identified. The needs cater to various aspects and people capability is one of the most important aspects.</p>
					<p>The Learning & Development is constantly in the endeavor of fulfilling the people capability aspect of Skeiron’s strategic needs. A robust resource base from business as well as External Knowledge Partners and a strong delivery focus would ensure a qualitative and timely addressing of the organization’s strategic learning needs.</p>
					 <img src="<?php echo base_url().'uploads/l_and_d/icons/landd.PNG'; ?>" style="width:100%;"></img>
					 
					<h5 style="color: #8fb729;">L & D’s Strategic Alignment <h5> 
					<p>The L & D would translate its mission into action through aligning its strategy to Skeiron’s developmental needs. Learning interventions are designed to foster development in the areas identified as key to Skeiron’s business growth. Some of the identified strategic areas are as follows:</p>

					<h5 style="color: #8fb729;">MANAGEMENT & LEADERSHIP </h5><p> The mission of L & D is to enable business units to develop leadership, managerial and behavioral capabilities through appropriately designed leadership and management development programs, catering to the various levels of leadership and support in the Talent Management initiatives.</p>

					<h5 style="color: #8fb729;">TECHNICAL & FUNCTIONAL</h5><p>L & D is also responsible for development of technical & functional L & D plans centrally and implementing it through quarterly L&D plans. L & D also supports deployment of business unit specific L&D initiatives across all businesses as identified and communicated. </p>

					<h5 style="color: #8fb729;">PROCESS ORIENTATION and STANDARDIZATION</h5><p>In the process domain the objective of L& D is to define, develop and implement L & D processes in a standardized way, develop centrally governed policies, procedures and templates to be used uniformly by all concerned people.
					INDUCTION – The L & D is focused on creating a structured and standardized induction and fresh talent pool management process for Skeiron with an objective of brand building in campuses, attracting and developing talent and creating a pool of talented human resources.</p>


				</div>
            </div>
			<!-- image slide code end -->
			
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
				<div class="clear"></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
				
				<div class="col-md-6" style="padding: 0px 8px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Image Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_gallery/l_and_d'; ?>" target="_blank">
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4194.jpg" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="col-md-6" style="padding: 0px 0px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Video Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/video_gallery'; ?>" target="_blank">
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_170500909.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_171408479.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_172338706.JPG" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>	
			</div>
			
        </div>
		
<?php include('right_rk_side.php'); ?>
