<?php include('left_rk_side.php'); ?>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox" style="text-align: justify;text-justify: inter-word;">
		
			<div class="row">
                <div style="height:345px;overflow-y:scroll;padding: 5px 5px 5px 5px;">
				
					<h2 style="color: #8fb729;">L & D DELIVERY FOCUS AREAS</h2>
					<p>At the Learning & Development the delivery focus encompasses 6 process competency areas. The team being developed will comprise of L & D professionals with expertise in one or multiple delivery focus areas. </p>
					 <img src="<?php echo base_url().'uploads/l_and_d/icons/role.PNG'; ?>" style="width:100%;"></img>
							

				</div>
            </div>
			<!-- image slide code end -->
			
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
				<div class="clear"></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
				
				<div class="col-md-6" style="padding: 0px 8px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Image Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_gallery/l_and_d'; ?>" target="_blank">
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4194.jpg" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="col-md-6" style="padding: 0px 0px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Video Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/video_gallery'; ?>" target="_blank">
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_170500909.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_171408479.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_172338706.JPG" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>	
			</div>
		
        </div>
<?php include('right_rk_side.php'); ?>
