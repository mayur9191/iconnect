<?php include('left_rk_side.php'); ?>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox">
	   
			<?php
			
			if ($logindata['role'] == 1) {
				
				?>
				 <div class="three-search">
                <form action="<?= base_url() . 'profile/announcement' ?>" enctype="multipart/form-data" method="post" id="wallpostForm">
					<div class="form-group" style="display:none;">
                        <textarea rows="4" class="form-control walltext" id="editor" placeholder="Enter Announcement"
                                  name="announcement_text"></textarea>

                    </div>
					 <div class="form-group">
                        <input type="file" name="attachemnet"  />

                    </div>
                    <div class="form-group">
                        <input type="submit" value="post" class="btn btn-sm btn-primary pull-right"/>
                    </div>
                </form>
            </div>
				<?php
				
			}
			
			?>
			
         
            <div class="clear"></div>
			

			<!-- image slider code start -->
			 <div class="row">
				
                <div id="profile_sliders" class="carousel slide" data-ride="carousel" >
				
                    <div class="carousel-inner" role="listbox" style="height:0%;"  >
						
						<div class="item active">
							<center>
							<a class="popup_image" rel="group1" href="<?php echo base_url(); ?>uploads/corpcom/annoucements/LD4.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url(); ?>uploads/corpcom/annoucements/LD4.png" alt="" style="width: 100%;" /></a>
							</center>
						</div>
						
                    </div>
                    <!-- Left and right controls -->
					
					
                    <a class="left carousel-control" href="#profile_sliders" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#profile_sliders" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
			<!-- image slide code end -->
			
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
				<div class="clear"></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0px 0px 0px 0px;">
				
				<div class="col-md-6" style="padding: 0px 8px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_gallery/l_and_d'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Image Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<!--<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_gallery/l_and_d'; ?>" target="_blank">
									<img src="<?php echo base_url().'uploads/corpcom/Synergy Projects Banglore/IMAG4140.jpg'; ?>" class="all studio img_width"/>
									<img src="<?php echo base_url().'uploads/corpcom/Synergy Projects Banglore/IMAG4140.jpg'; ?>" class="all studio img_width"/>
									<img src="<?php echo base_url().'uploads/corpcom/Synergy Projects Banglore/IMAG4140.jpg'; ?>" class="all studio img_width"/>
									</a>-->
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_gallery/l_and_d'; ?>" target="_blank">
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4140.jpg" class="all studio img_width"/>
									<img src="http://content.skeiron.com:85/uploads/corpcom/l_and_d/IMAG4194.jpg" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="col-md-6" style="padding: 0px 0px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="<?php echo 'http://content.skeiron.com:85/'.'content/image_library'; ?>" target="_blank">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Video Gallery</b>
								</h5>
							</a>
						</div>
					</div>
					<div class="albums row" style="height:170px;">
						<div class="albums-inner">
							<div class="albums-tab">
								<div class="albums-tab-thumb sim-anim-7">
									<a href="<?php echo 'http://content.skeiron.com:85/'.'content/video_gallery'; ?>" target="_blank">
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_170500909.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_171408479.JPG" class="all studio img_width"/>
										<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_172338706.JPG" class="all studio img_width"/>
									</a>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
				
				<!--
				<div class="col-md-6" style="padding:0px 0px 0px 0px;">
					<div class="sidebox community_update">
						<div class="iconnect-ps" style="">
							<a href="#">
								<h5 class="iconnect-psh5">
									<b class="ps-tag" style="padding-left: 8px;">Video Gallery</b>
								</h5>
							</a>
						</div>
						<div class="albums row" style="height:170px;">
							<div class="albums-inner">
								<div class="albums-tab">
									<div class="albums-tab-thumb sim-anim-7">
										<a href="<?php echo 'http://content.skeiron.com:85/'.'content/video_gallery'; ?>" target="_blank">
											<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_170500909.JPG" class="all studio img_width"/>
											<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_171408479.JPG" class="all studio img_width"/>
											<img src="http://content.skeiron.com:85/uploads/corpcom/Christmas_2016/IMG_20161223_172338706.JPG" class="all studio img_width"/>
										</a>
									</div>
								</div>
							</div>
					   </div>
					</div>
				</div>
				-->
				
			</div>
	
        </div>
<?php include('right_rk_side.php'); ?>