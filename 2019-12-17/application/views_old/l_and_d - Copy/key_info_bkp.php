<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=isset($titile)?$titile." Portal":'Portal'?></title>

	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/bootstrap.min.css"  />
	<link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>css/style.css"  />

	<link rel="stylesheet" href="<?=base_url().'assets/'?>css/fontawesome.css" />

	<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/' ?>intro/introjs.css" />
	<script type="text/javascript" src="<?= base_url().'assets/' ?>intro/intro.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<?php
	$CI = & get_instance();
	$logindata = $this->session->userdata('user_ldamp');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" media="screen">
<style>
.panel-body.a{ padding:0px 0px 0px 0px;}
.panel-default>.panel-heading {
    color: #f8ffff;
    background-color: #8fb729;
    border-color: #8fb729;
}
.list-group-item{
	border: 1px solid #8fb729;
}
.sidebox{
	border: none;
}
</style>	 
</head>
<body style="/*background-color: #dff0d8;*/">
<nav class="navbar navbar-default navbar-inverse navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid ">
    <div class=" topbar">
	<span class="pull-right hidden-xs" style="margin-top:4px; margin-left:4px;">
	Welcome <br><?= (isset($logindata['displayname']))? ucwords($logindata['displayname']) : ''; ?>
	</span>
		
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
			
            <a class="navbar-brand" href="<?= base_url() ?>" style="font-size: 22px;font-weight: 700;">
                <img alt="alternative text" src="<?= base_url() . 'assets/' ?>images/iconnect-logo.png"/>
            </a>
        </div>
        <div class="animated fadeIn" id="bs-example-navbar-collapse-1">
            <!--<div class="collapse navbar-collapse animated fadeIn" id="bs-example-navbar-collapse-1">-->
            <center>
                <ul class="nav navbar-nav animated fadeIn text16">
                  
                </ul>
               
            </center>
			<?php

			if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
				$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
			} else {

				$dp_image = base_url() . 'assets/images/45.png';
			}
			?>
            <ul class="nav navbar-nav navbar-right nopadding" style="margin-right:0px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $dp_image; ?>" style="width: 30px;border-radius: 20px;"  alt="profile"
                             class="img-responsive hidden-xs"/>
                    </a>
                    <ul class="dropdown-menu animated flipInX" role="menu">

                        <?php

                        if ($logindata['role'] == 1) {
                            ?>
                            
                            <li>
                                <a href="#" data-toggle="modal" data-target="#profile_slide">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                    Banner Slider
                                </a>
                            </li>
                            <?php
                        }
                        ?>
						<!--<li><a href="<?= base_url() . 'profile/edit' ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></i> Edit Profile </a></li>
                        <li class="divider"></li>
                        <li><a href="#">Welcome <?= ucwords($user_ldamp['name']) ?></a></li>
                        <li class="divider"></li>-->
                        <li><a href="<?= base_url() . 'profile/logout' ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </div><!-- /.container-fluid -->
</nav>

<?php

if ($logindata['role'] == 1) {
    ?>

    <div id="profile_slide" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
               
            </div>

        </div>
    </div>



    <?php
}
?>



<div class="container-fluid main">
<div class="row">
    <div class="col-md-12">

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
						
			
			<div class="col-xs-12 col-md-12 asdf-icon" data-step="1" data-intro="Profile picture upload">

				<?php
				if (isset($CI->user_indo[0]->u_image) && $CI->user_indo[0]->u_image != '') {
					$dp_image = base_url() . 'uploads/users/profiles/thumb/' . $CI->user_indo[0]->u_image;
				} else {

					$dp_image = base_url() . 'assets/images/45.png';
				}
				?>
				<div class="col-md-12 nopad" data-toggle="modal" data-target="#update_dp" style="display: flex;color: #8fb729;font-weight:700;">
					<a href="<?php echo base_url().'learn_and_development';?>"><h3>L & D Portal</h3></a>
				</div>
				<div class="clear"></div>
			</div>


				<div class="col-xs-8 col-md-9" style="padding-left: 30px; padding-top: 10px; line-height: 20px;">
					<div class="clear"></div>
					<span class="clientuser-name2">
					</span>
				</div>
			<div class="clear"></div>


			<div class="clear"></div>

			<!------------- Start  ------------------------------------>
				 <!-- first column-->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopad">
				
					<div class="panel-group sidebox community_update" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h6 class="panel-title iconnect-psh5">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										About L & D
									</a>
								</h6>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'aboutus';?>">About Us</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'key_info';?>">Key Information At L & D</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'roles';?>">Role & Responsibilities</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										L & D Offerings
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Programmes_Offerred';?>" target="_blank">Programmes & Offerings</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/L_and_D_Calendars';?>" target="_blank">L & D Calenders</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Virtual Learning
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item">E Learning</li>
										<li class="list-group-item">Presentation of Trainings</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFour">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
										L & D Procedures
									</a>
								</h4>
							</div>
							<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Forms';?>" target="_blank">Forms</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Processes';?>" target="_blank">Processs</a></li>
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Templates';?>" target="_blank">Templates</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingFive">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
										L & D Dashboards
									</a>
								</h4>
							</div>
							<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
								<div class="panel-body a">
									<ul class="list-group">
										<li class="list-group-item"><a href="<?php echo base_url().'documents/Mis';?>" target="_blank">MIS</a></li>
										<li class="list-group-item">Locational Training Data</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!------------- END    ------------------------------------>
				

        </div>

       <div class="col-md-6 col-sm-6 col-xs-12 middilebox" style="text-align: justify;text-justify: inter-word;border: 2px solid #8fb729;">
	 
<h2 style="color: #8fb729;">SKEIRON’s PROGRESSIVE NEEDS </h2>
<p>Skeiron is a fast-growing organization and to aid its rapid growth several developmental needs have been identified. The needs cater to various aspects and people capability is one of the most important aspects.</p>
<p>The Learning & Development is constantly in the endeavor of fulfilling the people capability aspect of Skeiron’s strategic needs. A robust resource base from business as well as External Knowledge Partners and a strong delivery focus would ensure a qualitative and timely addressing of the organization’s strategic learning needs.</p>
 <img src="<?php echo base_url().'uploads/l_and_d/icons/landd.PNG'; ?>" style="width:100%;"></img>
 
<h5 style="color: #8fb729;">L & D’s Strategic Alignment <h5> 
<p>The L & D would translate its mission into action through aligning its strategy to Skeiron’s developmental needs. Learning interventions are designed to foster development in the areas identified as key to Skeiron’s business growth. Some of the identified strategic areas are as follows:</p>

<h5 style="color: #8fb729;">MANAGEMENT & LEADERSHIP </h5><p> The mission of L & D is to enable business units to develop leadership, managerial and behavioral capabilities through appropriately designed leadership and management development programs, catering to the various levels of leadership and support in the Talent Management initiatives.</p>

<h5 style="color: #8fb729;">TECHNICAL & FUNCTIONAL</h5><p>L & D is also responsible for development of technical & functional L & D plans centrally and implementing it through quarterly L&D plans. L & D also supports deployment of business unit specific L&D initiatives across all businesses as identified and communicated. </p>

<h5 style="color: #8fb729;">PROCESS ORIENTATION and STANDARDIZATION</h5><p>In the process domain the objective of L& D is to define, develop and implement L & D processes in a standardized way, develop centrally governed policies, procedures and templates to be used uniformly by all concerned people.
INDUCTION – The L & D is focused on creating a structured and standardized induction and fresh talent pool management process for Skeiron with an objective of brand building in campuses, attracting and developing talent and creating a pool of talented human resources.</p>

 
 
 
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12 oym-iconnect nopad_left_right">
            
			<!----------------- ----------------------------->
	
	<link href="<?php echo base_url().'assets/img_library/_css/Icomoon/style.css';?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url().'assets/img_library/_css/main.css'; ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url().'assets/img_library/_css/sim-prev-anim.css'; ?>" rel="stylesheet" type="text/css" />
<style>
.iconnect-ps .active .ps-tag{
	color:#000;
}
body{
	background-color:#fff !important;
}

</style>
<style>
.sim-anim-7 {
    position: absolute; 
}

.img_width{
	max-width: 95%;
	
}
</style>
<div class="">
</div>

<div class="clear"></div>
<div class="clear"></div>


<div class="clear"></div>

<!-- events start -->
<div class="sidebox community_update">
		<div class="panel-group sidebox community_update" id="accordion" role="tablist" aria-multiselectable="true">
				 <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingfourty">
						<h6 class="panel-title iconnect-psh5">
							<a role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseFourty" aria-expanded="true" aria-controls="collapseFourty">
								Know SKEIRON
							</a>
						</h6>
					</div>
					<div id="collapseFourty" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfourty">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">About Skeiron</li>
								<li class="list-group-item">Vision</li>
								<li class="list-group-item">Mission</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingten">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseTen" aria-expanded="false" aria-controls="headingten">
								Polls & Survey
							</a>
						</h4>
					</div>
					<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingten">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">TNI Surveys</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingthirty">
						<h6 class="panel-title iconnect-psh5">
							<a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapsethirty" aria-expanded="true" aria-controls="collapsethirty">
								Food for Brain
							</a>
						</h6>
					</div>
					<div id="collapsethirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthirty">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">Good Articles</li>
								<li class="list-group-item">Quizzer & Teasers</li>
							</ul>
						</div>
					</div>
				</div>
				
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingEighty">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseEighty" aria-expanded="false" aria-controls="collapseEighty">
								Quick Links
							</a>
						</h4>
					</div>
					<div id="collapseEighty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighty">
						<div class="panel-body a">
							<ul class="list-group">
								<li class="list-group-item">Connect With Us</li>
								<li class="list-group-item">Help Desk</li>
							</ul>
						</div>
					</div>
				</div> 
			</div>
	</div>
<!-- events end -->
<div class="clearfix"></div>
<!-- events start -->

<div class="clearfix"></div>
<!-- events end -->

			
			<!----------------- ----------------------------->
			
						
			
        </div>

    </div>
</div>

</div>



<?php include APPPATH . "views/includs/common_scripts.php" ?>

<script src="https://cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
<!--<script type="text/javascript" src="--><? //= base_url() . 'assets/' ?><!--croppic/croppic.min.js"></script>-->

<div id="myModal" class="modal middle-slid" style="display: none;margin-top:80px;">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <img src="<?= base_url().'uploads/announcement/'.$witem->an_file;?>" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>
  </div>
</div>

<!-- <script src="<?= base_url().'assets/' ?>dist/js/lightbox-plus-jquery.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.js"></script>

<div class="container-fluid navbar-inverse " style="margin-top:10px;">
    <div class="container footer">
        <div class="col-md-12 col-xs-12">
            <span class="pull-left" style="color: darkgray;font-size:10px;"> © <?= date('Y') ?> ALL RIGHTS RESERVED BY <b>SKEIRON IT</b> </span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			 <!--<a href="javascript:void(0);" onclick="javascript:introJs().start();" class="pull-right" style="color:white;font-size: 12px;margin-left: 10px;">Website tour</a>-->
            <?= getLastLogin($CI->user_ldamp['employeeid']) ?>
        </div>
    </div>
</div>
<style>
<!-- Tree view code  -->

</style>
</body>
</html>
