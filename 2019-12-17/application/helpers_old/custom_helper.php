<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('ldap_connention')) {
    /**
     * Create LDAP CONNECTION
     *
     * @param    array $data data for the CAPTCHA
     * @param    string $font_path server path to font
     * @return    string
     */
    function ldap_connention($handle, $password)
    {

        $server = ldap_serverhost;
        $basedn = ldap_basedn;
        $dn = ldap_basedn . "\\" . $handle;

        $connect = ldap_connect($server);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

		//var_dump(ldap_bind($connect, $dn, $password));
        if ($ds = @ldap_bind($connect, $dn, $password)) {
            $filters = "(samaccountname=$handle)";
            //    $filters = "(cn=*)"; // get all results
            $results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
            $entries = ldap_get_entries($connect, $results);
            if (count($entries) > 0) {
                return $entries;
            } else {
                return 0;
            }

        } else {
            return 0;
        }


    }

    function testssss()
    {
        return 'test';
    }


    function togodate($futheredate){
        $cunentdate = date('Y-m-d', strtotime(date("Y-m-d")));

        $futheredate_ss = explode('-', $futheredate);
        $futheredate_ss[0]=date('Y');
        $futheredate = implode('-', $futheredate_ss);
        $futheredas = date('Y-m-d', strtotime($futheredate));
        $date1=date_create($cunentdate);
        $date2=date_create($futheredas);
        $diff=date_diff($date1,$date2);

//        echo '<pre>';
//        print_r($diff);
//        echo '<pre>';


        $prefix = '';

        if($diff->y > 0){
            if($diff->y == 1){
                $prefix .= $diff->y . " Year ";
            }
            else{
                $prefix .= $diff->y . " Years ";
            }
        }
        if($diff->m > 0){
            if($diff->m == 1){
                $prefix .= $diff->m . " Month ";
            }
            else{
                $prefix .= $diff->m . " Months ";
            }
        }
        if($diff->d > 0){
            if($diff->d == 1){
                $prefix .= $diff->d . " Day ";
            }
            else{
                $prefix .= $diff->d . " Days ";
            }
        }

        return $prefix;


//        return $diff->format("%R%a days");
    }

    function checkLike($current_user_id, $birthday_user_ids){
        $ci = & get_instance();
        //$ci->load->model('Base_model');
        $qry = $ci->Base_model->run_query("SELECT * FROM birthday_likes WHERE bl_user_id='".$current_user_id."' AND bl_buid IN (".$birthday_user_ids.")");
        if(count($qry) > 0){
            $likeStr = 'Liked';
        }else{
            $likeStr = 'Like';
        }
        return $likeStr;
    }


    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
	
	//current_user_id
	function getLastLogin($current_user_id){
		$ci = & get_instance();
        $qry = $ci->Base_model->run_query("SELECT * FROM users WHERE u_employeeid='".$current_user_id."'");
		
        if($qry[0]->u_last_login !='0000-00-00 00:00:00'){
            $lastSeen = '<span class="pull-right" style="color: darkgray;font-size:10px;"> LAST SEEN <i class="fa fa-clock-o"></i> '.date('d-m-Y h:i:s A',strtotime($qry[0]->u_last_login)).'</span>';
        }else{
            $lastSeen = '';
        }
        return $lastSeen;
	}
	
	
	function getTechnicalName($pId){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("tec_func_list")->where('tbl_primary_id',$pId)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->row();
			$returnData = $rData->title;
		}else{
			$returnData = '';
		}
		return $returnData;
	}


	function getBehsoftName($pId){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("behave_soft_skils_list")->where('tbl_primary_id',$pId)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->row();
			$returnData = $rData->title;
		}else{
			$returnData = '';
		}
		return $returnData;
	}

	function getQhseName($pId){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("qhse_list")->where('tbl_primary_id',$pId)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->row();
			$returnData = $rData->title;
		}else{
			$returnData = '';
		}
		return $returnData;
	}
	
	//mysql query 
	function checkEmpExsits($empId){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("du_emp_personal_detail_upd")->where('emp_staffid',$empId)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->row();
			$returnData = TRUE;
		}else{
			$returnData = FALSE;
		}
		return $returnData;
	}
	
	function getNameByCode($empId){
		$sk = & get_instance();
		$qry = $sk->db->select("official_name")->from("du_emp_personal_detail_upd")->where('emp_staffid',$empId)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->row();
			$returnData = $rData->official_name;
		}else{
			$returnData = $rData->official_name;
		}
		return $returnData;
	}
	
	//NEW survey desing helpers start 
	
	//Leadership / Managerial Development
	function leadership_development($categoryName){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("mdp_ldp_list_v2")->where('category',$categoryName)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//Functional / Technical
	function func_tech_development($categoryName){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("tec_func_list_v2")->where('category',$categoryName)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//Personality Development / Behavioral
	function personal_behav_development($categoryName){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("dp_bhav_list_v2")->where('category',$categoryName)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//QHSE Development
	function hqse_development(){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("qhse_list_v2")->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//get categories
	function getCategoryData($tblName){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from($tblName)->where('sr_no',0)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	function getTitleData($tblName,$categoryId){
		$sk = & get_instance();
		$qry1 = $sk->db->select("category")->from($tblName)->where('tbl_primary_id',$categoryId)->get();
		$qry_row = $qry1->row();
		$qry = $sk->db->select("*")->from($tblName)->where('category',$qry_row->category)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	
	function getDrillDownSelect($traingType,$keySelect,$fieldVal){
		switch(trim($traingType)){
			case "ldp_mdp":
				$resultData = getTitleData('mdp_ldp_list_v2',$fieldVal);
			break;
			case "fun_tech":
				$resultData = getTitleData('tec_func_list_v2',$fieldVal);
			break;
			case "pd_beha":
				$resultData = getTitleData('dp_bhav_list_v2',$fieldVal);
			break;
		}
		$drilDownList = '<option value="">Please select </option>';
		foreach($resultData as $resultRecord){ 
			if($resultRecord->tbl_primary_id == $keySelect){
				$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'" selected="selected">'.$resultRecord->title.'</option>';
			}else{
				$drilDownList.='<option value="'.$resultRecord->tbl_primary_id.'">'.$resultRecord->title.'</option>';
			}
		}
		echo $drilDownList;
	}
	
	function getCategoryDataName($tblName,$primary_id){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from($tblName)->where('tbl_primary_id',$primary_id)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->row();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	
	function getNameById($traingType='',$cat=0,$title=0,$tblId=0){
		switch(trim($traingType)){
			case "ldp_mdp":
				$resultData = getCategoryDataName('mdp_ldp_list_v2',$tblId);
			break;
			case "fun_tech":
				$resultData = getCategoryDataName('tec_func_list_v2',$tblId);
			break;
			case "pd_beha":
				$resultData = getCategoryDataName('dp_bhav_list_v2',$tblId);
			break;
			case "qhse":
				$resultData = getCategoryDataName('qhse_list_v2',$tblId);
			break;
		}
		if($cat > 0){
			$returnName = $resultData->category;
		}
		if($title > 0){
			$returnName = $resultData->title;
		}
		return $returnName;
	}
	//NEW survey desing helpers end 
	
	
	//home start 
	function getMaterialMaster(){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("material_master")->where('material_status',1)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->row();
		}else{
			$returnData = array();
		}
		return $returnData;
	}

	function getMaterialAllMaster(){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("material_master")->where('material_status',1)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}

	function getMaterialMasterDataById($material_master_id=0){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("material_master")->where('material_status',1)->where('material_master_id',$material_master_id)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->row();
		}else{
			$returnData = array();
		}
		return $returnData;
	}

	function getMterialProduct($materialId=0){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("material_product")->where('product_status',1)->where('material_master_id',$materialId)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}

	function getMaterialApproval($materialId=0){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("material_approval")->where('approval_status',1)->where('material_master_id',$materialId)->get();
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}

	function fillterInput($fieldval){
		return trim(stripcslashes($fieldval));
	}

	function getApprovalId($materialId=0,$squeId=5){
		$sk = & get_instance();
		$qry = $sk->db->select("*")->from("material_approval")->where('approval_status',1)->where('material_master_id',$materialId)->where('approval_sque_id',$squeId)->get();
		if($qry->num_rows() > 0){
			$rowData = $qry->row();
			$returnData = $rowData->material_approval_id;
		}else{
			$returnData = 0;
		}
		return $returnData;
	}

	function deactiveMaterialProducts($materialId){
		$sk = & get_instance();
		$ProductAry = array('product_status'=>0);
		$sk->db->where('material_master_id',$materialId);
		$sk->db->update('material_product',$ProductAry);
		return true;
	}
	//home end 
	//permission module start 
	function getUserList($permissionModuleColumnId = 0){
		$sk = & get_instance();
		if($permissionModuleColumnId > 0){
			$userQry = $sk->db->query("SELECT ur.* FROM gate_pass_permission gp LEFT JOIN user_roles ur ON (gp.user_role_id = ur.user_role_id) WHERE ur.user_is_active=1 AND gp.user_role_id != '0' AND permission_column != '$permissionModuleColumnId' ");
		}else{
			$userQry = $sk->db->query("SELECT * FROM user_roles WHERE user_is_active=1");
		}
		if($userQry->num_rows() > 0){
			$returnData = $userQry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	function checkPermission($roleId,$columnId){
		$sk = & get_instance();
		$returnStatus = 0;
		$qry = $sk->db->query("SELECT * FROM gate_pass_permission WHERE user_role_id='$roleId' AND permission_column='$columnId'");
		if($qry->num_rows() > 0){
			$returnStatus =1;
		}
		return $returnStatus;
	}
	
	function getUserListp($permissionModuleColumnId = 0){
		$sk = & get_instance();
		$userQry = $sk->db->query("SELECT group_concat(\"'\",ur.user_emp_code,\"'\") as empcode FROM gate_pass_permission gp LEFT JOIN user_roles ur ON (gp.user_role_id = ur.user_role_id) WHERE ur.user_is_active=1 AND gp.user_role_id != '0' AND gp.permission_column = '$permissionModuleColumnId' GROUP BY 'all'");
		if($userQry->num_rows() > 0){
			$resultData = $userQry->row_array();
			$returnData = $resultData['empcode'];
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//permission module end
	
	//************************help desk code starts here******* //
	
	function get_notification_dept($dept){ 
		return array(1 => 'Sudexo Coupon Available Free',2=>'Lunch for new Joiners today');
	}
	//get dept
	function get_dept(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM dept_hd WHERE dept_is_active=1");
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//get category
	function get_category(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM category_hd WHERE category_is_active=1");
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//get sub_category
	function get_sub_category(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM sub_category_hd WHERE sub_category_is_active=1");
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//get sub_category
	function get_item(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM sub_category_hd WHERE sub_category_is_active=1");
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	function get_record_by_id_code_old($tbl_code = 0, $key_id=0){
		$sk = & get_instance();
		if($tbl_code != '' && $key_id > 0){
			switch($tbl_code){
				case 'dept':
					$qry = $sk->db->query("SELECT * FROM dept_hd WHERE dept_is_active=1 AND dept_id='$key_id'");
				break;
				
				case 'category':
					$qry = $sk->db->query("SELECT * FROM category_hd WHERE category_is_active=1 AND category_id='$key_id'");
				break;
				
				case 'subcategory':
					$qry = $sk->db->query("SELECT * FROM sub_category_hd WHERE sub_category_is_active=1 AND sub_category_id='$key_id'");
				break;
				
				case 'item':
					$qry = $sk->db->query("SELECT * FROM items_hd WHERE item_is_active=1 AND items_id='$key_id'");
				break;
			}
			if($qry->num_rows() > 0){
				$returnData = $qry->row();
			}else{
				$returnData = array();
			}
			return $returnData;
		}
	}
		
	//nNEW
	function get_technician_id($item_id = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM group_item_map_hd WHERE group_item_map_is_active=1 AND item_id='$item_id'");
		if($qry->num_rows() > 0){
			$resultData = $qry->row();
			$returnData = $resultData->group_id;
			
			//get technition
			$tech_qry = $sk->db->query("SELECT * FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) WHERE tg.group_id ='$returnData' AND t.technician_is_active = 1");
			$tech_list = $tech_qry->result();
			foreach($tech_list as $tech_record){
				$returnData_tech = 0;
				//echo '<br>'.$tech_record->technician_id; var_dump(isAssign($returnData));
				if(isAssign($tech_record->technician_id)){ //check is technician is assigned or not 
					$returnData_tech = $tech_record->technician_id; // get the technician id and assign him to task
					break;
				}
			}
		}else{
			$returnData_tech = 0;
		}
		//echo '<br>return request '; var_dump(isAssign($returnData_tech));
		echo '<br>rechni id '.$returnData_tech.' ';
		return $returnData_tech;
	}
	
	//nNEW
	//check tech is assign or not 
	function isAssign($technician_id = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM request_assign_person_hd WHERE technician_id = '$technician_id' AND assign_status='1' AND request_assign_person_is_active = '1' AND is_escalated = 0");
		if($qry->num_rows() > 0){
			$assignStatus = false; //is assigned 
		}else{
			$assignStatus = true; // is not assigned
		}
		return $assignStatus;
	}
	
	function get_groupId_itemId($item_id=0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM group_item_map_hd WHERE group_item_map_is_active=1 AND item_id='$item_id'");
		if($qry->num_rows() > 0){
			$resultData = $qry->row();
			$group_id = $resultData->group_id;
		}else{
			$group_id = 0;
		}
		return $group_id;
	}
	
	//get request data
	function get_requestdata_by_id($request_id = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM user_request_hd WHERE user_request_id = '$request_id'");
		if($qry->num_rows() > 0){
			$resultData = $qry->row();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	//escalation technitian HO
	function ho_technician_assign(){
		$sk = & get_instance();
		return 0;
	}
	//************************help desk code ends  here******* //
	//get corp. comm event name start 
	function get_current_event(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM event_list WHERE event_is_active = '1'");
		if($qry->num_rows() > 0){
			$eventData = $qry->row();
			$eventName = $eventData->event_name;
		}else{
			$eventName = 'No event found';
		}
		return $eventName;
	}
	//get corp. comm event name end
	
	function get_announcements(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM announcements WHERE an_status = '1' AND is_deleted='0' ORDER BY an_id DESC");
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	/*
	*check for admin user
	*@param user_id
	*return true/false
	*/	
	function is_Admin($emp_code)
	{ 
		$is_admin = false;
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT t2.* FROM role t1 JOIN user_admin t2 ON t1.role_id = t2.role_id WHERE (t2.emp_code='$emp_code' AND t2.is_admin_active = '1' AND t1.is_role_active= '1' AND t1.role_id = '1')");
		//echo $sk->db->last_query();die;
		if($qry->num_rows() > 0){
			$is_admin = true;	
		}
		return $is_admin;
	}
	
}




//****************************** HELPDESK CODE START **********************************************************************************
	
	
	function get_assign_technician(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM group_item_map_hd WHERE group_item_map_is_active=1 AND item_id='$item_id'");
		if($qry->num_rows() > 0){
			$resultData = $qry->row();
			$returnData = $resultData->group_id;
		}
	}
	
	
	//get assign data by tequest id 
	function get_assign_request_data($request_id = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM request_assign_person_hd WHERE user_request_id = '$request_id' AND request_assign_person_is_active = 1");
		if($qry->num_rows() > 0){
			$returnStatus = $qry->row();
		}else{
			$returnStatus = array();
		}
		return $returnStatus;
	}
	
	//common header for po tracker and pre joinning starts **********
	
    function getHead(){
		require_once(APPPATH."views/common_header.php");
    }   
    
    function getFooter(){
		require_once(APPPATH."views/common_footer.php");
    }
	
    function getreminder($company_id,$op_option)
    {
    	$GL = get_instance();
     	$GL->db->select('*');
     	$GL->db->where('tbl_company_id',$company_id);
     	$GL->db->where('po_option_id',$op_option);
     	$query = $GL->db->get('po_reminder_detail');
     	if($query->num_rows()>0)
     	{
     		//print_r($query->row_array());exit();
     		return $query->row_array();
     	}
     	else
     	{
     		return array();
     	}
    }
     /// new changes for the tracker**88
     function getCompany()
     {
        $GL = get_instance();
        $GL->load->model('company_model');
        $data = $GL->company_model->showCompany();
        return $data;
     }
     //***********prejoining ************************
     function  getPreJoin($tblJoineId,$taskId)
     {
        $GL = get_instance();
        $GL->load->model('joinnedetail_model');
        $data = $GL->joinnedetail_model->getPreJoiningTasks($tblJoineId,$taskId);
        return $data;
     }
     function getExpectedCompletionDay($timeline)
     {
            $daysToAdd = '';
            switch($timeline){
                case '7 days':
                $daysToAdd = '+7';
                break;
                case '1 day':
                $daysToAdd = '+1';
                break;
                case '2 days':
                $daysToAdd = '+2';
                break;
                case '3 days':
                $daysToAdd = '+3';
                case '30 days':
                $daysToAdd = '+30';
                break;
                case '10 days':
                $daysToAdd = '+10';
                break;
                case 'Before 1 day':
                $daysToAdd = '-1';
                break;
                case 'Before 10 day':
                $daysToAdd = '-10';
                break;
                case 'On joining day':
                $daysToAdd = '+0';
                break;
                case 'Within 7-days':
                $daysToAdd = '+7';
                break;
               
            }
            return $daysToAdd;
     }
	
	
	function get_groups(){
		$sk = & get_instance();
		$user_ldamp = $sk->session->userdata('user_ldamp');
		$qry = $sk->db->query("SELECT * FROM technician_hd WHERE tech_code='".$user_ldamp['employeeid']."'");
		if($qry->num_rows() > 0){
			$tech_dept = $qry->row();
			$qry = $sk->db->query("SELECT * FROM groups_hd WHERE group_is_active = 1 AND group_id !='16' AND dept_id = '".$tech_dept->dept_id."'");
		}else{
			$qry = $sk->db->query("SELECT * FROM groups_hd WHERE group_is_active = 1 AND group_id !='16' ");
		}
		
		//$qry = $sk->db->query("SELECT * FROM groups_hd WHERE group_is_active = 1");
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	function getTechnicianByGroupId($group_id = 0){
		$sk = & get_instance();
		if($group_id > 0){
			$qry = $sk->db->query("SELECT t.technician_id as tech_id,t.tech_name FROM technician_group_map_hd tg LEFT JOIN technician_hd t ON (t.technician_id = tg.technician_id) WHERE tg.group_id ='$group_id' AND t.technician_is_active = 1");
		}else{
			$qry = $sk->db->query("SELECT technician_id as tech_id, tech_name FROM technician_hd WHERE technician_is_active =1");
		}
		
		if($qry->num_rows() > 0){
			$returnData = $qry->result();
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
	//get emp data by empcode 
	function getEmpDataByCode($request_id=''){
		$sk = & get_instance();
		$qry_1 = $sk->db->query("SELECT * FROM users u LEFT JOIN user_request_hd ur ON (u.u_employeeid = ur.user_code) WHERE ur.user_request_id='$request_id'");
		$empData = $qry_1->row();
	}
	
	//get emp data by email 
	//function getEmpByEmail($emailId = ''){
	//	$sk = & get_instance();
	//	$returnStatus = array();
	//	$qry = $sk->db->query("SELECT * FROM users WHERE u_email = '$emailId'");
	//	if($qry->num_rows() > 0){
	//		$returnStatus = $qry->row();
	//	}				}
	//	return $eventName;				
	//	return $returnStatus;
	//}
	//}
	function getEmpByEmail($emailId = ''){
		$sk = & get_instance();
		$returnStatus = array();
		$qry = $sk->db->query("SELECT * FROM users WHERE u_email = '$emailId'");
		if($qry->num_rows() > 0){
			$returnStatus = $qry->row();
		}
		
		return $returnStatus;
	}
	//get corp. comm event name end			
	function getEmpByEmail1($emailId = ''){
		$sk = & get_instance();
		$returnStatus = array();
		$qry = $sk->db->query("SELECT * FROM ldapusers WHERE u_email = '$emailId'");
		if($qry->num_rows() > 0){
			$returnStatus = $qry->row();
		}		
		return $returnStatus;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	//SMTP delete not used
	function emailConfig(){
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.office365.com',
			'smtp_port' => 587,
			'smtp_user' => sysu,
			'smtp_pass' => sysp,
			'mailtype'  => 'html', 
			'charset'   => 'UTF-8',
			'wordwrap' => TRUE,
			'newline' => "\r\n"
		);
		return $config;
	}
	
	function getLocations($id = '')
	{
		$sk = & get_instance();
		$tblName = 'cities';
		if($id != ''){
			$sk->db->where('tbl_primary_id',$id);
		}
		$qry = $sk->db->select("*")->from($tblName)->get();
		if($qry->num_rows() > 0){
			$rData = $qry->result_array();
			$returnData = $rData;
		}else{
			$returnData = array();
		}
		return $returnData;
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////
	function get_record_by_id_code($tbl_code = 0, $key_id=0){
		$sk = & get_instance();
		if($tbl_code != '' && $key_id > 0){
			switch($tbl_code){
				case 'dept':
					$qry = $sk->db->query("SELECT * FROM dept_hd WHERE dept_is_active=1 AND dept_id='$key_id'");
				break;
				
				case 'category':
					$qry = $sk->db->query("SELECT * FROM category_hd WHERE category_is_active=1 AND category_id='$key_id'");
				break;
				
				case 'subcategory':
					$qry = $sk->db->query("SELECT * FROM sub_category_hd WHERE sub_category_is_active=1 AND sub_category_id='$key_id'");
				break;
				
				case 'item':
					$qry = $sk->db->query("SELECT * FROM items_hd WHERE item_is_active=1 AND items_id='$key_id'");
				break;
				case 'technician':
				   $qry = $sk->db->query("SELECT * FROM  technician_hd WHERE technician_is_active=1 AND technician_id='$key_id'");
				break;   
				case 'sitelocation':   
				   $qry = $sk->db->query("SELECT * FROM  site_locations_hd WHERE site_location_is_active=1 AND site_location_id='$key_id'");
				break;
				case 'group':
				$qry = $sk->db->query("SELECT * FROM  groups_hd WHERE group_is_active=1 AND group_id='$key_id'");
			}
			if($qry->num_rows() > 0){
				$returnData = $qry->row();
			}else{
				$returnData = array();
			}
			return $returnData;
		}
	}
    
//******************************* NEW CODE UPDATED END ****************************************
	
    function get_email_by_emp_code($emp_code){
		$returnEmail = array();
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT u_email FROM users WHERE u_employeeid = '$emp_code'  and u_employeeid!='NULL'");
		if($qry->num_rows() > 0){
			$returnEmail = $qry->result();
		}
		return $returnEmail;
	}
	
	function get_all_details_by_emp_code($emp_code='',$email='')
	{  
	    $returnDetails = array();
		$sk = & get_instance();
		if($emp_code !="")
		{ 
			$sql = "SELECT * FROM users WHERE u_employeeid = '$emp_code'"; 
		}
		if($email !="")
		{ 
			$sql = "SELECT * FROM users WHERE u_employeeid = '$email'"; 
		}
		$qry = $sk->db->query($sql);
		if($qry->num_rows() > 0){
			$returnDetails = $qry->row();
		}
		//echo $sk->db->last_query();die;
		return $returnDetails;
	}
	
	//get time diff 
	function get_time_diff($dt1, $dt2){
		$seconds = strtotime($dt1) - strtotime($dt2);
		$days    = floor($seconds / 86400);
		$hours   = floor(($seconds - ($days * 86400)) / 3600);
		$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
		$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
		$returnAry = array(
			'days'=>$days,
			'hurs'=>$hours,
			'mins'=>$minutes,
			'secd'=>$seconds
		);
		return $returnAry;
	}
	
	// request counter
	function get_request_count_by_email($emaildata=''){
		$sk = & get_instance();
		$u_email = $emaildata['user_ldamp']['mail'];
		//get employee code by email
		$empdata = getEmpByEmail($u_email);
		$statusAry = array('open','hold','wip','close','resolve');
		
		if(isset($empdata->behalf_of)){
			$qry_str = " (ur.behalf_of='".$empdata->behalf_of."' OR ur.user_code='".$empdata->u_employeeid."') ";
		} else {
			$qry_str = " ur.user_code='".$empdata->u_employeeid."' ";
		}
		
		$qry = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) LEFT JOIN groups_hd g ON(rap.group_id=g.group_id) WHERE $qry_str AND ur.request_is_active='1' AND rap.request_assign_person_is_active=1");
		
		if($qry->num_rows() > 0){
			$countComplete = 0; $countResolve = 0; $countClose = 0; $countWip = 0; $countHold = 0; $countOpen = 0;
			$requestData = $qry->result();
			
			foreach($requestData as $requestRecord){
				
				if($requestRecord->task_status=='open' || $requestRecord->task_status == 'query_to_user' && $requestRecord->is_request_completed ==0){
					$countOpen += 1;
				}
				
				if($requestRecord->task_status=='hold' && $requestRecord->is_request_completed ==0){
					$countHold += 1;
				}
				
				if($requestRecord->task_status=='wip' && $requestRecord->is_request_completed ==0){
					$countWip += 1;
				}
				
				if($requestRecord->task_status=='close' && $requestRecord->is_request_completed ==0){
					$countClose += 1;
				}
				
				if($requestRecord->task_status=='resolve' && $requestRecord->is_request_completed ==0){
					$countResolve += 1;
				}
				
				if($requestRecord->is_request_completed ==1){
					$countComplete += 1;
				}
			}
			//$totalRequest = $countComplete + $countResolve + $countClose + $countWip + $countHold + $countOpen;
			$inprocess = $countClose + $countWip + $countOpen;
			$totalRequest = $inprocess + $countHold + $countComplete;
			$returnStatus = array(
				'open'=> $countOpen,
				'hold'=> $countHold,
				'wip'=> $countWip,
				'close'=> $countClose,
				'resolve'=> $countResolve,
				'completed'=> $countComplete,
				'inprocess'=>$inprocess,
				'onhold'=>$countHold,
				'total'=>$totalRequest
			);
		}else{
			$returnStatus = array();
		}
		
		//print_r($returnStatus); exit();
		return $returnStatus;
    }
	
	function get_request_all($request_code,$dept_id){
		
		$sk = & get_instance();
		$qry_extend = "";
		//$u_email = $emaildata['user_ldamp']['mail'];
		//$empdata = getEmpByEmail($u_email);
		switch($request_code){
			case 'inprocess':
				$qry_extend = " AND ra.task_status IN ('open','wip','query_to_user') AND ur.is_request_completed = 0 ";//,'close'
			break;
			
			case 'onhold':
				$qry_extend = " AND ra.task_status IN ('hold') AND ur.is_request_completed = 0 ";
			break;
			
			case 'completed':
				$qry_extend = " AND ur.is_request_completed = 1 OR (ra.task_status = 'close') ";
			break;
			
			default:
				$qry_extend = " AND ra.task_status IN ('open','wip','query_to_user') AND ur.is_request_completed = 0 ";
				$qry_extend .= " OR ra.task_status IN ('hold') AND ur.is_request_completed = 0 ";
				$qry_extend .= " OR ur.is_request_completed = 1 OR (ra.task_status = 'close') ";
			break;
		}
		$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) where ur.dept_id = $dept_id AND ur.request_is_active = 1 AND(ra.request_assign_person_is_active=1 $qry_extend) group by ur.user_request_id  ORDER BY ur.user_request_id DESC");
		//echo $sk->db->last_query();die();
		if($qry->num_rows() > 0){
			$requestList = $qry->result();
		}else{
			$requestList = array();
		}
		return $requestList;
    }
	
	function get_request_allERP($request_code,$dept_id){
		
		$sk = & get_instance();
		$qry_extend = "";
		switch($request_code){
			case 'inprocess':
				$qry_extend = " AND ra.task_status IN ('open','wip') AND ur.is_request_completed = 0 ";//,'close'
			break;
			
			case 'onhold':
				$qry_extend = " AND ra.task_status IN ('hold') AND ur.is_request_completed = 0 ";
			break;
			
			case 'completed':
				$qry_extend = " AND ur.is_request_completed = 1 OR (ra.task_status = 'close') ";
			break;
			
			default:
				$qry_extend = " AND ra.task_status IN ('open','wip') AND ur.is_request_completed = 0 ";
				$qry_extend .= " OR ra.task_status IN ('hold') AND ur.is_request_completed = 0 ";
				$qry_extend .= " OR ur.is_request_completed = 1 OR (ra.task_status = 'close') ";
			break;
		}
		$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) where ur.dept_id = $dept_id AND ur.category_id='25' AND ur.subcategory_id='101' AND ur.request_is_active = 1 AND(ra.request_assign_person_is_active=1 $qry_extend) group by ur.user_request_id  ORDER BY ur.user_request_id DESC");
		
		if($qry->num_rows() > 0){
			$requestList = $qry->result();
		}else{
			$requestList = array();
		}
		return $requestList;
    }
	
	function get_request_count_all($dept_id,$request_date=''){
		$sk = & get_instance();
		//$u_email = $emaildata['user_ldamp']['mail'];
		//get employee code by email
		//$empdata = getEmpByEmail($u_email);
		
		$statusAry = array('open','query_to_user','hold','wip','close','resolve');
		if($request_date !=''){
			$qry = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) WHERE rap.request_assign_person_is_active=1 AND ur.request_is_active = 1 AND ur.dept_id = $dept_id and DATE_FORMAT(ur.request_created_dt, '%Y-%m-%d')='".$request_date."'");
		}else{
			$qry = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) WHERE rap.request_assign_person_is_active=1 AND ur.request_is_active = 1 AND ur.dept_id = $dept_id");
		}
		
		//$qry = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) WHERE rap.request_assign_person_is_active=1 AND ur.request_is_active = 1 AND ur.dept_id = $dept_id");
		
		if($qry->num_rows() > 0){
			$countComplete = 0; $countResolve = 0; $countClose = 0; $countWip = 0; $countHold = 0; $countOpen = 0;
			$requestData = $qry->result();
			
			foreach($requestData as $requestRecord){
				
				if($requestRecord->task_status=='open' || $requestRecord->task_status == 'query_to_user' && $requestRecord->is_request_completed ==0){
					$countOpen += 1;
				}
				
				if($requestRecord->task_status=='hold' && $requestRecord->is_request_completed ==0){
					$countHold += 1;
				}
				
				if($requestRecord->task_status=='wip' && $requestRecord->is_request_completed ==0){
					$countWip += 1;
				}
				
				if($requestRecord->task_status=='close' && $requestRecord->is_request_completed ==0){
					$countClose += 1;
				}
				
				if($requestRecord->task_status=='resolve' && $requestRecord->is_request_completed ==0){
					$countResolve += 1;
				}
				
				if($requestRecord->is_request_completed ==1){
					$countComplete += 1;
				}
			}
			//$totalRequest = $countComplete + $countResolve + $countClose + $countWip + $countHold + $countOpen;
			$inprocess =   $countWip + $countOpen;
			$resolve = $countClose+$countComplete;
			$totalRequest = $countClose+$inprocess + $countHold + $countComplete;
			$returnStatus = array(
				'open'=> $countOpen,
				'hold'=> $countHold,
				'wip'=> $countWip,
				'close'=> $countClose,
				'resolve'=> $countComplete,
				'completed'=>$resolve,
				'inprocess'=>$inprocess,
				'onhold'=>$countHold,
				'total'=>$totalRequest
			);
		}else{
			$returnStatus = array();
		}
		
		//print_r($returnStatus); exit();
		return $returnStatus;
    }
	
	// save the history of a ticket
	function savehistory($history_type="0",$request_id="0",$technician_id,$creator_id,$creator_email,$subject="Update Request",$body)
	{	
	     $creator = get_all_details_by_emp_code($creator_id); 
		$data = array(
		 "history_type"    => $history_type,
		 "request_id"      =>$request_id,
		 "technician_id"   =>$technician_id,
		 "creator_id"      => $creator_id,
		 "creator_name"    => $creator->u_name,
		 "creator_email"   => $creator->u_email,
		 "created_date"    => date('Y-m-d h:i:s'),
		 "history_subject" => $subject,
         "history_body"	   => $body	 
		 
		);
		$sk = & get_instance();
		$sk->db->insert("request_history_hd",$data);
	}
	
    function get_request_data_by_code($request_code,$emaildata){
		
		$sk = & get_instance();
		$qry_extend = "";
		$u_email = $emaildata['user_ldamp']['mail'];
		$empdata = getEmpByEmail($u_email);
		switch($request_code){
			case 'inprocess':
				$qry_extend = " AND ra.task_status IN ('open','wip','query_to_user') ";//,'close'
			break;
			
			case 'onhold':
				$qry_extend = " AND ra.task_status IN ('hold') ";
			break;
			
			case 'completed':
				$qry_extend = " AND ur.is_request_completed = 1 ";
			break;
			
			case 'default':
				$qry_extend = "";
			break;
		}
		//$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) WHERE (ur.behalf_of='".$u_email."' OR ur.user_code='".$empdata->u_employeeid."') AND ra.request_assign_person_is_active=1 $qry_extend ORDER BY ur.user_request_id DESC");
		$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) WHERE ur.user_code='".$empdata->u_employeeid."' AND ur.request_is_active='1' AND ra.request_assign_person_is_active=1 $qry_extend ORDER BY ur.user_request_id DESC");
		
		if($qry->num_rows() > 0){
			$requestList = $qry->result();
		}else{
			$requestList = array();
		}
		return $requestList;
    }
	
	// convert standard time 
	function convert_std_time($timeVal=''){
		return date('M d ,Y h:i:s A',strtotime($timeVal));
	}
	function std_time(){
		return date('M d ,Y h:i:s A');
	}
	
//resolution code start 
	function is_resolution_exists($requestId = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM request_resolution_hd WHERE user_request_id='$requestId' AND resolution_is_active='1'");
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	//tried solutions 
	function get_tried_solutions($requestId = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM request_solutions_hd WHERE request_id='$requestId' AND request_solution_is_active='1' ORDER BY request_solution_hd DESC");
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	function get_user_name_by_email($creator_email = NULL){
		if($creator_email != NULL){
			$empdata = getEmpByEmail($creator_email);
			$emp_name = ucfirst($empdata->u_name);
		}else{
			$emp_name = 'Anonymous';
		}
		return $emp_name;
	}
//resolution code end
	
	
	//history code started 
	
	
	function add_history($history_code='',$post=array()){
		
		$sk = & get_instance(); 
		//$ldapdata  =    getldapDataByEmail($post["request_id"]);
		$request_info = getReqInfoByUserRequestId($post["request_id"]);
        $user_ldamp = $sk->session->userdata('user_ldamp');
			//echo "<pre>";print_r($post);exit();
            $history_ary = array();	
            //$creator_id = $request_info['creator_id'];//to be changed 
            $creator_id = (isset($user_ldamp['employeeid']))? $user_ldamp['employeeid']: "0";
 			$creator_email = (isset($user_ldamp['mail']))? $user_ldamp['mail'] : 'System';
 			//$creator_email = $request_info['email'];
			$creator_ip = '';  //to be changed
			$current_timestamp = date('Y-m-d H:i:s');
			
			//$common_history_ary = array(
			//	"request_id" => $post["request_id"],
			//	"creator_id" => $creator_id,
			//	"creator_email" => $creator_email,
			//	"currenttimestamp" => $current_timestamp,
			//	"creator_ip" => $creator_ip
			//);
			
            switch($history_code){
			case 'ReqStatus':
				$history_ary = array(
					'request_code'=>$history_code, 
					'status' =>$post["status"],
					'pre_status' =>$post["prev_status"]
				);
			break;
			
			case 'ReqWorkStation':
				$history_ary = array(
					'request_code'=>$history_code,
					'work_station' =>$post["work_Station"],
					'pre_work_station' =>$post["prev_workstation"]
				);
			break;
			
			case 'ReqMode':
			    $history_ary = array(
					'request_code'=>$history_code,
					'mode' =>$post["mode"],
					'pre_mode' =>$post["prev_mode"]
				);
			break;
			
			case 'ReqDept':
			    $history_ary = array(
					'request_code'=>$history_code,
					'dept_id' =>$post["dept"],
					'pre_dept_id' =>$post["prev_dept"]
				);
			break;
			
			
			case 'ReqSite':
			    $history_ary = array(
					'request_code'=>$history_code,
					'site_id' =>$post["site"],
					'pre_site_id' =>$post["prev_site"]
				);
			break;
			     
			
			case 'ReqPriority':
			    $history_ary = array(
					'request_code'=>$history_code,
					'priority' =>$post["priority"],
					'pre_priority' =>$post["prev_priority"]
				);
				break;
			
			
			case 'ReqGroup':
			    $history_ary = array(
					'request_code'=>$history_code,
					'group_id' =>$post["group"],
					'pre_group_id' =>$post["prev_group"]
				);
			break;
			
			case 'ReqCatgry':
			    $history_ary = array(
					'request_code'=>$history_code,
					'category_id' =>$post["category"],
					'pre_category_id' =>$post["prev_category"]
				);
			break;
			
			case 'ReqTechncian':
			    $history_ary = array(
					'request_code'=>$history_code,
					'technician_id' =>$post["technician"],
					'pre_technician_id' =>$post["prev_technician"]
				);
			break;
				
			case 'ReqSubCat':
			    $history_ary = array(
					'request_code'=>$history_code,
					'subcategory_id' =>$post["subactegory"],
					'pre_subcategory_id' =>$post["prev_subcategory"]
				);
			break;
			
			case 'ReqItem':
			    $history_ary = array(
					'request_code'=>$history_code,
					'item_id' =>$post["item"],
					'pre_item_id' =>$post["prev_item"]
				);
			break;
			
			
			case 'ReqDueDt':
				$history_ary = array(
					'request_code'=>$history_code,
					'dueby_date' =>date('Y-m-d H:i:s',strtotime($post["dueby_date"])),
					'pre_dueby_date' =>date('Y-m-d H:i:s',strtotime($post["prev_dueby_date"]))
				);
			break;
			
			case 'ReqResDt':
			    $history_ary = array(
					'request_code'=>$history_code,
					'response_dueby_dt' =>date('Y-m-d H:i:s',strtotime($post["response_dueby"])),
					'pre_response_dueby_dt' =>date('Y-m-d H:i:s',strtotime($post["prev_res_dueby_date"]))
				);
			break;
			
			
			case 'ReqCreated':
			    $history_ary = array(
					'request_code'=>$history_code,
                   // 'requestor_id'=>,
                    //'requestor_email'=>  					
				);
			break;
			
			case 'ReqAllChange':
				$history_ary = array(
					'request_code'=>$history_code,
					//'status' =>$post["status"],    // status
					//'pre_status' =>$post["prev_status"], // prev status
					//'work_station'=>$post["work_station"], // workstation
					//'pre_work_station'=>$post["prev_workstation"], // prev workstation
					//'mode' =>$post["mode"], //mode
					//'pre_mode' =>$post["prev_mode"], // prev mode
					//'dept' =>$post["dept"],  //dept
					//'pre_dept' =>$post["prev_dept"]  //prev dept
					//'site' =>$post["site"],       //site
					//'pre_site' =>$post["prev_site"]  // prev site
					//'priority' =>$post["priority"],          // priority
					//'pre_priority' =>$post["prev_priority"] // prev priority
					//'group' =>$post["group"],               // group
					//'pre_group' =>$post["prev_group"],      // prev group
					//'category' =>$post["category"],         // category
					//'pre_category' =>$post["prev_category"] // prev category
					//'technician' =>$post["technician"],      // technician
					//'pre_technician' =>$post["prev_technician"] // prev technician
					//'subcategory' =>$post["subactegory"],        //sub category
					//'pre_subcategory' =>$post["prev_subcategory"], // prev subcategory 
					//'item' =>$post["item"],                       // item
					//'pre_item' =>$post["prev_item"],               // prev item
					//'dueby_date' =>$post["dueby_date"],           //due by date  
					//'pre_dueby_date' =>$post["prev_dueby_date"],   // prev dueby date
					//'response_dueby_dt' =>$post["response_dueby"], // response due by date
					//'pre_response_dueby_dt' =>$post["prev_res_dueby_date"] // prev response due by date
				);
				$requestAry = array('request_id','requester_user_code','status','prev_status','work_Station','prev_workstation','mode','prev_mode','dept','prev_dept','site','prev_site','priority','prev_priority','group','prev_group','category','prev_category','technician','prev_technician','subactegory','prev_subcategory','item','prev_item','create_date','dueby_date','prev_dueby_date','response_dueby','prev_res_dueby_date','update_request');
		        $matchArray = array('status'=>'prev_status','work_Station'=>'prev_workstation',
		'mode'=>'prev_mode','dept'=>'prev_dept','site'=>'prev_site','priority'=>'prev_priority','group'=>'prev_group','category'=>'prev_category','technician'=>'prev_technician','subactegory'=>'prev_subcategory','item'=>'prev_item','dueby_date'=>'prev_dueby_date','response_dueby'=>'prev_res_dueby_date');
		        $tableArray = array('status'=>'status',
				'work_station'=>'work_station','prev_workstation'=>'pre_work_station',
				'mode'=>'mode','prev_mode'=>'pre_mode','dept'=>'dept_id',
				'prev_dept'=>'pre_dept_id','site'=>'site_id','priority'=>'priority',
				'prev_priority'=>'pre_priority','group'=>'group_id','prev_group'=>'pre_group_id','category'=>'category_id','prev_category'=>'pre_category_id','technician'=>'technician_id','prev_technician'
				=> 'pre_technician_id','subactegory'=>'subcategory_id','prev_subcategory'=>'pre_subcategory_id','item'=>'item_id','prev_item'=>'pre_item_id','dueby_date'=>'dueby_date','prev_dueby_date'=>'pre_dueby_date','response_dueby'=>'response_dueby_dt','prev_res_dueby_date'=>'pre_response_dueby_dt');
				for($i=0;$i<=count($post); $i++){
					
					if(in_array($requestAry[$i],array("request_id","requester_user_code","create_date","update_request"))){
						continue;
					} else {
						if(($matchArray[$requestAry[$i]]==$requestAry[$i+1]) && $post[$requestAry[$i]] != $post[$requestAry[$i+1]] && in_array($requestAry[$i],array('status','prev_status','work_Station','prev_workstation','mode','prev_mode','dept','prev_dept','site','prev_site','priority','prev_priority','group','prev_group','category','prev_category','technician','prev_technician','subactegory','prev_subcategory','item','prev_item'))){
							//echo '<br>['.$requestAry[$i].']['.$requestAry[$i+1].']';
							//echo '<br>'.$postData[$requestAry[$i]].'=='.$postData[$requestAry[$i+1]];
							
							$history_ary[$tableArray[$requestAry[$i]]]   = $post[$requestAry[$i]];
							$history_ary[$tableArray[$requestAry[$i+1]]] = $post[$requestAry[$i+1]];
						   
						}
						else{
							if(($matchArray[$requestAry[$i]]==$requestAry[$i+1]) &&strtotime($post[$requestAry[$i]])!=strtotime($post[$requestAry[$i+1]])){
								$history_ary[$tableArray[$requestAry[$i]]]   = date('Y-m-d H:i:s',strtotime($post[$requestAry[$i]]));
							    $history_ary[$tableArray[$requestAry[$i+1]]] = date('Y-m-d H:i:s',strtotime($post[$requestAry[$i+1]]));
							}
							
						}
					}
				}
				
				
			break;
			
			case 'ReqAssignTech':
				$history_ary = array(
					'request_code'=>$history_code,
					'group_id'     => $post['group'],
					'pre_group_id' => $post['prev_group'],
					'technician_id'=> $post['technician'],
                    'pre_technician_id' => $post['prev_technician']					
				);
			break;
			
			case 'ReqClosed':
				$history_ary = array(
					'request_code'=>$history_code
				);
			break;
			
			case 'ReqResolnUpdate':
				$history_ary = array(
					'request_code'=>$history_code
				);
			break;
			
			case 'ReqNewTask':
				$history_ary = array(
					'request_code'=>$history_code,
					'task_id' => $task_id
				);
			break;
			
			case 'ReqWorkLogAdd':
				$history_ary = array(
					'request_code'=>$history_code,
					'worklog_id'  =>$worklog_id
				);
			break;
			
			case 'ReqResolnAdd':
				$history_ary = array(
				     
					'request_code'  =>$history_code,
					'resolution_id' => $post['resolution_id']
					//'solution_id'   => $solution_id
				);
			break;
			case 'ReqSlaAdded':
			   $history_ary = array(
			      'request_code'  =>$history_code,
				  
			   );
		}
		
		
		 $history_ary["request_id"] = $post["request_id"];
		 $history_ary["creator_id"] = $creator_id;
		 $history_ary["creator_email"] = $creator_email;
		 $history_ary["currenttimestamp"] = $current_timestamp;
		 $history_ary["history_created_dt"] = $current_timestamp;
		 $history_ary["creator_ip"] = $creator_ip;
		 //array_merge($history_ary,$common_history_ary);
		 //echo "<pre>";print_r($history_ary);exit();
		 //save the history array to the table
		 //echo "<pre>";print_r($history_ary);exit();
		 $sk->db->insert("ticket_history_hd",$history_ary);
		 return true;
	}
	
    function getReqInfoByUserRequestId($requestId){
		$sk = & get_instance();
		
		 $sql="SELECT case when request_type = 'other' then user_req.behalf_of when request_type = 'email' then user.u_email when request_type IS NULL then user.u_email when request_type = 'select' then user.u_email when request_type = 'portal' then user.u_email when request_type = 'phone' then user.u_email else null end as email,user_req_assign.*,user.*,user_req.* FROM user_request_hd as user_req LEFT JOIN users as user ON user_req.user_code = user.u_employeeid JOIN request_assign_person_hd as user_req_assign ON user_req.user_request_id =user_req_assign.user_request_id where user_req.user_request_id ='".$requestId."' AND user_req_assign.request_assign_person_is_active = '1'";
     $qry = $sk->db->query($sql);
	 if($qry->num_rows() > 0){
			$requestList = $qry->row();
			//echo "<pre>";print_r($requestList);exit();
			$dept =($requestList->dept_id!='')?get_record_by_id_code('dept',$requestList->dept_id):'';
			$group = ($requestList->group_id!='')?get_record_by_id_code('group',$requestList->group_id):'';
			$category = ($requestList->category_id!='')?get_record_by_id_code('category',$requestList->category_id):'';
			$subCategory = ($requestList->subcategory_id!='')?get_record_by_id_code('subcategory',$requestList->subcategory_id):'';
			$technician = ($requestList->technician_id!='')?get_record_by_id_code('technician',$requestList->technician_id):'';
			$item = ($requestList->item_id!='')?get_record_by_id_code('item',$requestList->item_id):'';
			$returnAry = array(
			    'creator_id'=>$requestList->u_id  ,
				'request_id'=> $requestList->user_request_id,
				'requester_email'=>$requestList->email,
				'requester_emp_code'=>$requestList->user_code, 
				'requester_name'=>$requestList->requester_name,
				'group_id'=>$requestList->group_id,
				'group_name'=>$group->group_name,
				'technician_id'=> $requestList->technician_id,
				'technician_name' => $technician->tech_name,
				'dept_id'=>$requestList->dept_id, 
				'dept_name'=>$dept->dept_name, 
				'category_id'=>$requestList->category_id, 
				'category_name'=>$category->category_name, 
				'subcategory_id' => $requestList->subcategory_id,
				'subcategory_name' => '',
				'item_id' => $requestList->item_id,
				'item_name' => $item->item_name,
				'priority'=> $requestList->request_priority,
				'site_id'=>$requestList->requester_location_id,
				'site_name'=>$requestList->requester_location_name,
				'email' => $requestList->email,
				'task_status'=> $requestList->task_status,
				'request_assign_person_dt'=> $requestList->request_assign_person_dt,
				'technician_Email'=> $technician->technician_email,
				'request_subject' => $requestList->request_subject,
				'isCompleted'=>$requestList->is_request_completed
			);
		}else{
			$requestList = array();
			$returnAry = $requestList;
		}
	 return $returnAry;
	} 
	
	function getldapDataByEmail($requestId=0){
		//get email from request_id
		
		
		//assign email id to below ver
		$emailId = '';
		
		$server = ldap_serverhost;
        $basedn = ldap_basedn;
		$handle = sysu;
		$password = sysp;
        $dn = ldap_basedn . "\\" . $handle;
        $connect = ldap_connect($server);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
		
		//var_dump(ldap_bind($connect, $dn, $password));
        if ($ds = @ldap_bind($connect, $dn, $password)) {
            //$filters = "(samaccountname=$handle)";
			
            $filters = "(cn=*)"; // get all results
            $results = ldap_search($connect, "dc=" . ldap_basedn . ",dc=COM", $filters) or exit("unable tosearch");
            $entries = ldap_get_entries($connect, $results);
			
            if(count($entries) > 0){
				for ($i = 0; $i < 2000; $i++) {
					if(strtolower($emailId) === strtolower($entries[$i]["mail"][0])){
						$ldap_userinfo = array(
							'samaccountname' => $entries[$i]["samaccountname"][0],
							'employeeid' =>  (isset($entries[$i]["employeeid"][0])) ? $entries[$i]["employeeid"][0] : ''  ,
							'name' => (isset($entries[$i]["name"][0])) ? $entries[$i]["name"][0] : '',
							'streetaddress' => (isset($entries[$i]["streetaddress"][0])) ? $entries[$i]["streetaddress"][0] : '',
							'displayname' => (isset($entries[$i]["displayname"][0])) ? $entries[$i]["displayname"][0] : '',
							'telephonenumber' => (isset($entries[$i]["telephonenumber"][0])) ? $entries[$i]["telephonenumber"][0] : '',
							'physicaldeliveryofficename' => (isset($entries[$i]["physicaldeliveryofficename"][0])) ? $entries[$i]["physicaldeliveryofficename"][0] : '',
							'description' => (isset($entries[$i]["description"][0])) ? $entries[$i]["description"][0] : '',
							'state' => (isset($entries[$i]["st"][0])) ? $entries[$i]["st"][0] : '',
							'userprincipalname' => (isset($entries[$i]["userprincipalname"][0])) ? $entries[$i]["userprincipalname"][0] : '',
							'mail' => (isset($entries[$i]["mail"][0])) ? $entries[$i]["mail"][0] : '',
							'mobile' => (isset($entries[$i]["mobile"][0])) ? $entries[$i]["mobile"][0] : ''
						);
						break;
					}
				}
				//echo '<pre>';print_r($ldap_userinfo);exit;
                return $ldap_userinfo;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
	}
	
	//*******history functions*****************************
	function check_get_priority($pre_priority,$priority){
		$priorityValue = (!empty($pre_priority))?$pre_priority:"None";
		$priorityNewValue = (!empty($priority))?$priority:"None";
		$msgBody .= "Priority  Changed from ".$priorityValue." to ".$priorityNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_workstation($pre_workstation,$workstation){
		$workstationValue = (!empty($pre_workstation))?$pre_workstation:"None";
		$workstationNewValue = (!empty($workstation))?$workstation:"None";
		$msgBody .= "Workstation  Changed from ".$workstationValue." to ".$workstationNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_category($pre_category_id,$category_id)
	{
		$CategoryArrayValue = ($pre_category_id!='')?get_record_by_id_code('category',$pre_category_id):array();
					
		$CategoryNewArrayValue = ($category_id!='')?get_record_by_id_code('category',$category_id):array();
					
		$categoryValue = (!empty($CategoryArrayValue))?$CategoryArrayValue->category_name:"None";
					
	    $categoryNewValue = (!empty($CategoryNewArrayValue))?$CategoryNewArrayValue->category_name:"None";
		$msgBody .= "Category  Changed from ".$categoryValue." to ".$categoryNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_sub_category($pre_sub_category_id,$sub_category_id)
	{
		$subCategoryArrayValue = ($pre_sub_category_id!='')?get_record_by_id_code('subcategory',$pre_sub_category_id):array();
				
		$subCategoryNewArrayValue = ($sub_category_id!='')?get_record_by_id_code('subcategory',$sub_category_id):array();
		$subactegoryValue = (!empty($subCategoryArrayValue))?$subCategoryArrayValue->sub_category_name:"None";
				
		$subactegoryNewValue = (!empty($subCategoryNewArrayValue))?$subCategoryNewArrayValue->sub_category_name:"None";
		$msgBody .= "Subcategory  Changed from ".$subactegoryValue." to ".$subactegoryNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_items($pre_item_id,$item_id)
	{
		$itemArrayValue = ($pre_item_id!='')?get_record_by_id_code('item',$pre_item_id):array();
				
		$itemNewArrayValue = ($item_id!='')?get_record_by_id_code('item',$item_id):array();
		$itemValue = (!empty($itemArrayValue))?$itemArrayValue->item_name:"None";
				
		$itemNewValue = (!empty($itemNewArrayValue))?$itemNewArrayValue->item_name:"None";
		$msgBody .= "Item  Changed from ".$itemValue." to ".$itemNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_group($pre_group_id,$group_id){
		$groupArrayValue = ($pre_group_id!='')?get_record_by_id_code('group',$pre_group_id):array();
		
		$groupNewArrayValue = ($group_id!='')?get_record_by_id_code('group',$group_id):array();
		
		$groupValue = (!empty($groupArrayValue))?$groupArrayValue->group_name:"None";
		$groupNewValue = (!empty($groupNewArrayValue))?$groupNewArrayValue->group_name:"None";
		$msgBody .= "Group  Changed from ".$groupValue." to ".$groupNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_technician($pre_tech_id,$tech_id){
		
		$techArrayValue = ($pre_tech_id!='')?get_record_by_id_code('technician',$pre_tech_id):array();
				
		$techNewArrayValue = ($tech_id!='')?get_record_by_id_code('technician',$tech_id):array();
		$techValue = (!empty($techArrayValue))?$techArrayValue->tech_name:"None";
				
		$techNewValue = (!empty($techNewArrayValue))?$techNewArrayValue->tech_name:"None";
		
		$msgBody .= "Technician  Changed from ".$techValue." to ".$techNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_duebydate($prev_dateby,$dateby){
		$prev_datebyValue = (!empty($prev_dateby))?$prev_dateby:"None";
		$duebyNewValue = (!empty($dateby))?$dateby:"None";
		$msgBody .= "Due By Date  Changed from ".std_time($prev_datebyValue)." to ".std_time($duebyNewValue)."<br/>";
		return $msgBody;	
	}
	
	function check_get_respnsebydate($prev_responseBy,$responseBy){
		$responseByValue = (!empty($prev_responseBy))?$prev_responseBy:"None";
		$responseNewValue = (!empty($responseBy))?$responseBy:"None";
		$msgBody .= "Response Due By Changed from ".$responseByValue." to ".$responseNewValue."<br/>";
		return $msgBody;
	}
	
	function check_get_mode($pre_mode ,$mode){
		$pre_modeValue = (!empty($pre_mode))?$pre_mode:"None";
		$modeNewValue = (!empty($mode))?$mode:"None";
		$msgBody .= "Response Due By Changed from ".$pre_modeValue." to ".$modeNewValue."<br/>";
		return $msgBody;
	}
	
	function get_name_from_email($email)
	{ 
	    $sk = & get_instance();
		$qry = $sk->db->query("select u_name from users where u_status = '1' AND u_email ='$email'");
		if($qry->num_rows()>0){
		  $row = $qry->row();
          $name = 	ucfirst($row->u_name); 		  
		} else{
		  $name = "Anonymous";
		}
		return $name;
	}
	
	function update_subject($email,$date){
		$msgSubject = "Updated by : ".ucwords(get_name_from_email($email))." on ".convert_std_time($date);
		return $msgSubject;
	}
	
	function check_get_status($pre_status,$status){
		$statusValue = (!empty($pre_status))?$pre_status:"None";
		$statusNewValue = (!empty($status))?$status:"None";
		$msgBody .= "Status Changed from ".$statusValue." to ".$statusNewValue."<br/>";
		
		return $msgBody;
	}
	
	function check_get_location($pre_location,$location){
		$prev_location_array = (!empty($pre_location))?get_record_by_id_code("sitelocation",$pre_location):"None";
		$location_array =  (!empty($location))?get_record_by_id_code("sitelocation",$location):"None";
		$prevLocationValue = (!empty($prev_location_array))?$prev_location_array->site_location_name:"None";
				
		$LocationValue = (!empty($location_array))?$location_array->site_location_name:"None";
		
		$msgBody .= "Site Changed from ".$prevLocationValue." to ".$LocationValue."<br/>";
		return $msgBody;
		
	}
	
	// history code ends 
	function display_history($request_id){
		$sk = & get_instance();
		$sk->load->model('Helpdesk_model');
		$history = $sk->Helpdesk_model->get_request_history($request_id);
		$msg ='';
		$msgdisp = '';
		//echo "<pre>";print_r($history);
		if(!empty($history)){
		foreach($history as $history_row){
			$msgSubject = "";
		    $msgBody = "";
			
			switch($history_row->request_code){
				case "ReqSlaAdded":
					$msgSubject = "Updated by ".$history_row->creator_email." on ".convert_std_time($history_row->history_created_dt);
					$msgBody    = 'Resolution SLA Violated';
					$msgdisp   .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp   .= '<p><i class="fa fa-flag-o" aria-hidden="true" style="color:red;"></i> '.  $msgBody.'</p>';
				break;	
				case "ReqSite":
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					//***site  changed ****
					if($history_row->pre_site_id != $history_row->site_id){
						$msgBody .= check_get_location($history_row->pre_site_id,$history_row->site_id);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case "ReqItem":
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					
					//***item  changed ****
					if($history_row->pre_item_id != $history_row->item_id){
						$msgBody .= check_get_items($history_row->pre_item_id,$history_row->item_id);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case "ReqClosed":
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					$msgBody .= "Operation:CLOSED, "."Performed By :".get_name_from_email($history_row->creator_email);
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqAssignTech':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					//*** change group id ****
					if($history_row->pre_group_id != $history_row->group_id){
						$msgBody .= check_get_group($history_row->pre_group_id,$history_row->group_id);
					}
					if($history_row->pre_technician_id != $history_row->technician_id){
						$msgBody .= check_get_technician($history_row->pre_technician_id,$history_row->technician_id);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqResolnAdd':
					$msgSubject = "Resolution added by :".get_name_from_email($history_row->creator_email)." on ".convert_std_time($history_row->history_created_dt);
					$msgBody .= "CLICK HERE to compare the resolution";
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqResolnUpdate':
					$msgSubject = "Resolution Updated by :".get_name_from_email($history_row->creator_email)." on ".convert_std_time($history_row->history_created_dt);
					$msgBody .= "Resolution Updated by :".get_name_from_email($history_row->creator_email);
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqStatus':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					if($history_row->pre_status != $history_row->status){
						$msgBody .= check_get_status($history_row->pre_status,$history_row->status);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqDueDt':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					if(strtotime($history_row->pre_dueby_date) != strtotime($history_row->dueby_date)){
						$msgBody .= check_get_duebydate($history_row->pre_dueby_date,$history_row->dueby_date);
						
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqResDt':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					if(strtotime($history_row->pre_response_dueby_dt) != strtotime($history_row->response_dueby_dt)){
						$msgBody .= check_get_respnsebydate($history_row->pre_response_dueby_dt,$history_row->response_dueby_dt);
						//print_r($msgBody);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqMode':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					if($history_row->pre_mode != $history_row->mode){
						$msgBody .= check_get_mode($history_row->pre_mode,$history_row->mode);
						//print_r($msgBody);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				
				case 'ReqGroup':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					
					//*** change group id ****
					if($history_row->pre_group_id != $history_row->group_id){
						$msgBody .= check_get_group($history_row->pre_group_id,$history_row->group_id);
					}
				break;
				
				//*** change technician****
				case 'ReqTechncian':
					if($history_row->pre_technician_id != $history_row->technician_id){
						$msgBody .= check_get_technician($history_row->pre_technician_id,$history_row->technician_id);
					}
				break;
				
				case 'ReqPriority':
				
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					if($history_row->priority != $history_row->pre_priority){
					$msgBody .= check_get_priority($history_row->pre_priority,$history_row->priority);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
			    break;
				
				case 'ReqWorkStation':
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					if($history_row->work_station != $history_row->pre_work_station){
					$msgBody .= check_get_workstation($history_row->pre_work_station,$history_row->work_station);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
			    break;
				
				case 'ReqCreated':
				
					$msgSubject = "Created by : ".ucwords(get_name_from_email($history_row->creator_email))." on ".convert_std_time($history_row->history_created_dt);
					
					$msgBody = $messagebody = "Operation : CREATE, performed by : ".ucwords(get_name_from_email($history_row->creator_email))."<br/>";
					
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
				
				case 'ReqAllChange':
				
					$msgSubject = update_subject($history_row->creator_email,$history_row->history_created_dt);
					
					//***site  changed ****
					if($history_row->pre_site_id != $history_row->site_id){
						$msgBody .= check_get_location($history_row->pre_site_id,$history_row->site_id);
					}
					
					//***Category  changed ****
					if($history_row->pre_category_id != $history_row->category_id){
						
						$msgBody .= check_get_category($history_row->pre_category_id,$history_row->category_id);
					} 
					
					//***sub category  changed ****
					if($history_row->pre_subcategory_id != $history_row->subcategory_id){
						
						$msgBody .= check_get_sub_category($history_row->pre_subcategory_id,$history_row->subcategory_id);
					}
					
					//***item  changed ****
					if($history_row->pre_item_id != $history_row->item_id){
						$msgBody .= check_get_items($history_row->pre_item_id,$history_row->item_id);
					}
					//*** work station changed ****
					if($history_row->work_station != $history_row->pre_work_station){
					$msgBody .= check_get_workstation($history_row->pre_work_station,$history_row->work_station);
					}
					//*** priority changed ****
					if($history_row->priority != $history_row->pre_priority){
					$msgBody .= check_get_priority($history_row->pre_priority,$history_row->priority);
					}
					
					//*** change group id ****
					if($history_row->pre_group_id != $history_row->group_id){
						$msgBody .= check_get_group($history_row->pre_group_id,$history_row->group_id);
					}
					//*** change technician id ****
					if($history_row->pre_technician_id != $history_row->technician_id){
						$msgBody .= check_get_technician($history_row->pre_technician_id,$history_row->technician_id);
						//print_r($msgBody);
					}
					//*** change dueby date  ****
					if(strtotime($history_row->pre_dueby_date) != strtotime($history_row->dueby_date)){
						$msgBody .= check_get_duebydate($history_row->pre_dueby_date,$history_row->dueby_date);
						
					}
					//*** change response by date  ****
					if(strtotime($history_row->pre_response_dueby_dt) != strtotime($history_row->response_dueby_dt)){
						$msgBody .= check_get_respnsebydate($history_row->pre_response_dueby_dt,$history_row->response_dueby_dt);
						//print_r($msgBody);
					}
					$msgdisp .= '<p class="backtab1">'.$msgSubject.'</p>';
					$msgdisp .= '<p>'.$msgBody.'</p>';
				break;
											
			    } 
					$msg .= $msgdisp;
					$msgdisp = '';			
			}
			
	    }
		
		return $msg;
	}
	
	function get_resolution_id($user_request_id)
	{
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT request_resolution_id from request_resolution_hd where resolution_is_active='1'");
		if($qry->num_rows()>0){
			$result = $qry->row();
			$record = $result->request_resolution_id;
		}
		return $record;
	}
	
	//function to get the quick links
	function get_quick_links(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT quick_link_id,quick_link_name,quick_link_desc,quick_link_url from quick_link_hd where quick_line_is_active='1'");
		$quick_links = '<ul>';
		if($qry->num_rows()>0){
			$result = $qry->result();
			foreach($result as $row)
		    $quick_links .= '<li><a target="_blank" href="'.$row->quick_link_url.'">'.$row->quick_link_name.'</li>'; 	
		}
		$quick_links .= '</ul>';
		return $quick_links;
	}
	
	function checkColumn($userCode=''){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * from request_columns_hd where user_code ='$userCode'");
		if($qry->num_rows()>0){
			return true;
		}
		return false;
	}
	
	function save_columns($table,$postData,$userdata){
		$sk = & get_instance();
		$u_email = $userdata['user_ldamp']['mail'];
		$u_code  = $userdata['user_ldamp']['employeeid'];
		$header_array = explode(",",$postData);
		foreach($header_array as $header_row){
		    $data = array(
			    "user_code"   => $u_code,
				"user_email"  => $u_email,
				"column_list" => $header_row
			);
		    $sk->db->insert($table,$data);	
		}
		return true;
	}
	
	function update_column($table,$postData,$userdata){
		$sk = & get_instance();
		$u_email = $userdata['user_ldamp']['mail'];
		$u_code  = $userdata['user_ldamp']['employeeid'];
		$sk->db->delete($table,array("user_code"=>$u_code));
		if($sk->db->affected_rows()>0){
			$header_array = explode(",",$postData);
			foreach($header_array as $header_row){
				$data = array(
					"user_code"   => $u_code,
					"user_email"  => $u_email,
					"column_list" => $header_row
				);
				$sk->db->insert($table,$data);	
			}
		}
		return true;
	}
	
	function get_columns($userCode=''){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT column_list from request_columns_hd where user_code ='$userCode'");
		if($qry->num_rows()>0){
			$result = $qry->result_array();	
			foreach($result as $res){
				$record[] = $res['column_list'];
			}
		}
		else{
			$record = array('Sr No','ID','Subject','Requester Name','Technician',
			'Status','Created date','Site','Priority','Group');
		}
		return $record;
	}
	
	//** get important links
	function get_implinks(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM implinks WHERE implink_is_active='1'");
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	function get_attachment($requestId = 0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM request_attach_hd WHERE attach_is_active = '1' AND request_id='$requestId'");
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	function get_user_request_notes($requestId = 0)
	{
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM user_request_notes_hd WHERE notes_is_active = '1' AND user_request_id='$requestId' ORDER BY user_request_notes_id DESC");
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
			
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	function get_technician_assign_request_count($email='',$emp_code='')
	{
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT user_code,requester_name,task_status,request_assign_person_is_active,
  is_request_completed FROM user_request_hd ur
  LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id)
  LEFT JOIN groups_hd g ON(rap.group_id=g.group_id)
  LEFT JOIN technician_hd te ON (rap.technician_id = te.technician_id)
  where (te.technician_email = '$email' OR te.tech_code = '$emp_code') AND  rap.request_assign_person_is_active=1 AND ur.request_is_active='1'");
		if($qry->num_rows() > 0){
			$countComplete = 0; $countResolve = 0; $countClose = 0; $countWip = 0; $countHold = 0; $countOpen = 0;
			$requestData = $qry->result();
			//print_r($requestData);die;
			foreach($requestData as $requestRecord){
				
				if($requestRecord->task_status=='open' || $requestRecord->task_status == 'query_to_user' && $requestRecord->is_request_completed ==0 ){
					$countOpen += 1;
				}
				
				if($requestRecord->task_status=='hold' && $requestRecord->is_request_completed ==0){
					$countHold += 1;
				}
				
				if($requestRecord->task_status=='wip' && $requestRecord->is_request_completed ==0){
					$countWip += 1;
				}
				
				if($requestRecord->task_status=='close' && $requestRecord->is_request_completed ==0){
					$countClose += 1;
				}
				
				if($requestRecord->task_status=='resolve' && $requestRecord->is_request_completed ==0){
					$countResolve += 1;
				}
				
				if($requestRecord->is_request_completed ==1){
					$countComplete += 1;
				}
		     }
			 $inprocess = $countClose + $countWip + $countOpen;
			 $totalRequest = $inprocess + $countHold + $countComplete+$countResolve;
			 $returnStatus = array(
				'open'=> $countOpen,
				'hold'=> $countHold,
				'wip'=> $countWip,
				'close'=> $countClose,
				'resolve'=> $countResolve,
				'completed'=> $countComplete,
				'inprocess'=>$inprocess,
				'onhold'=>$countHold,
				'total'=>$totalRequest
			);
		  } else{
			$returnStatus = array();
		  }
		  //echo $sk->db->last_query();exit();
		  //print_r($returnStatus); exit();
		  return $returnStatus;
	}
	
	function get_technician_assign_by_code($request_code,$emaildata)
	{
		
		$sk = & get_instance();
		$qry_extend = "";
		$u_email = $emaildata['user_ldamp']['mail'];
		$u_code = $emaildata['user_ldamp']['employeeid'];
		//$empdata = getEmpByEmail($u_email);
		switch($request_code){
			case 'inprocess':
				$qry_extend = " AND ra.task_status IN ('open','wip','query_to_user') ";//,'close'
			break;
			
			case 'onhold':
				//$qry_extend = " AND ra.task_status IN ('hold') ";
				$qry_extend = " AND ra.task_status ='hold' ";
			break;
			
			case 'completed':
				$qry_extend = " AND ur.is_request_completed = 1 ";
			break;
			
			case 'default':
				$qry_extend = "";
			break;
		}
		//$qry = $sk->db->query("SELECT *,ur.user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON(ur.user_request_id=ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd te ON (ra.technician_id = te.technician_id)where te.technician_email = '$u_email' AND  ra.request_assign_person_is_active=1 $qry_extend ORDER BY ur.user_request_id DESC");
		
		$requestList  = $sk->Helpdesk_model->get_assign_tech_list($u_email,$u_code,$qry_extend);
		//echo $sk->db->last_query();exit();
		//if($qry->num_rows() > 0){
		//	$requestList = $qry->result();
		//}else{
		//	$requestList = array();
		//}
		//return $requestList;
		return $requestList;
    }
	
	function get_all_requestAdmin(){
		$sk = & get_instance();
		$user_ldamp = $sk->session->userdata('user_ldamp');
		$qry1 = $sk->db->query("SELECT * FROM technician_hd WHERE tech_code='".$user_ldamp['employeeid']."'");
		
		if($qry1->num_rows() > 0){
			$tech_dept = $qry1->row();
			$dept_id = $tech_dept->dept_id;
			$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON(ur.user_request_id=ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd te ON (ra.technician_id = te.technician_id) WHERE ra.request_assign_person_is_active='1' AND ur.dept_id='".$dept_id."' AND ur.request_is_active='1' AND (ra.task_status IN ('open','wip','query_to_user') OR ra.task_status IN ('hold') OR ur.is_request_completed ='1') AND ra.request_assign_person_is_active='1' GROUP BY ur.user_request_id ORDER BY ur.user_request_id DESC");
		}
		
		if(check_is_superadmin()){
			$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON(ur.user_request_id=ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd te ON (ra.technician_id = te.technician_id) WHERE ra.request_assign_person_is_active='1' AND ur.request_is_active='1' AND (ra.task_status IN ('open','wip','query_to_user') OR ra.task_status IN ('hold') OR ur.is_request_completed ='1') AND ra.request_assign_person_is_active='1' GROUP BY ur.user_request_id ORDER BY ur.user_request_id DESC");
		}
		
		//Old working query 
		//$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON(ur.user_request_id=ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd te ON (ra.technician_id = te.technician_id) WHERE ra.request_assign_person_is_active=1 AND ur.request_is_active='1' AND ra.task_status IN ('open','wip') OR ra.task_status IN ('hold') OR ur.is_request_completed ='1' AND ra.request_assign_person_is_active='1' GROUP BY ur.user_request_id ORDER BY ur.user_request_id DESC");
		
		$requestList = $qry->result();
		return $requestList;
	}
	
	//check admin_hd 
	function check_is_superadmin(){
		$rk = & get_instance();
		$returnStatus = false;
		$user_ldamp = $rk->session->userdata('user_ldamp');
		$qry = $rk->db->query("SELECT * FROM admin_hd WHERE admin_emp_code ='".$user_ldamp['employeeid']."' AND admin_is_active='1'");
		if($qry->num_rows() > 0){
			$returnStatus = true;
		}
		return $returnStatus;
	}
	
	//check is technician or user
	function check_is_technician($request_id=0){
		
		$sk = & get_instance();
		$sk->user_ldamp = $sk->session->userdata('user_ldamp');
		$emp_code = $sk->user_ldamp['employeeid'];
		if($request_id){
			//$qry =$sk->db->query("SELECT technician_id FROM technician_hd as t1 JOIN user_request_hd as t2 ON t1.tech_code = t2.user_code  WHERE t1.tech_code='$emp_code' AND technician_is_active='1' and t2.user_request_id =$request_id and t2.request_is_active = '1'");
			
			$qry = $sk->db->query("SELECT t1.technician_id FROM technician_hd as t1 JOIN request_assign_person_hd as t2 ON t1.technician_id = t2.technician_id JOIN user_request_hd as t3 ON t3.user_request_id = t2.user_request_id WHERE t1.tech_code='$emp_code' AND t1.technician_is_active='1' and t3.user_request_id =$request_id and t3.request_is_active = '1'");
		} else{
			$qry = $sk->db->query("SELECT technician_id FROM technician_hd  WHERE tech_code='$emp_code' AND technician_is_active='1'");	
		}
		//echo $sk->db->last_query();exit();
		if($qry->num_rows() > 0){
			$returnStatus = TRUE;
		}else{
			$returnStatus = false;
		}
		return $returnStatus;
	}
	
	
	function check_technician($email='',$emp_code=''){
		$sk = & get_instance();
		$returnStatus = FALSE;
		if($email !=''){
			$email = $email;
		}else{
			$email = '';
		}
		
		if($emp_code !=''){
			$emp_code = $emp_code;
		}else{
			$emp_code = '';
		}
			
		$qry = $sk->db->query("SELECT technician_id FROM technician_hd WHERE (tech_code='$emp_code' OR technician_email='$email') AND technician_is_active='1'");
		
		if($qry->num_rows() > 0){
			$returnStatus = TRUE;
		}
		//echo $sk->db->last_query();die;
		return $returnStatus;
	}
	
    function check_user($emp_id,$emp_code){
		$sk = & get_instance();
		$returnStatus = FALSE;
		if($email !=''){
			$email = $email;
		}else{
			$email = '';
		}
		
		if($emp_code !=''){
			$emp_code = $emp_code;
		}else{
			$emp_code = '';
		}
			
		$qry = $sk->db->query("SELECT technician_id FROM technician_hd WHERE (tech_code='$emp_code' OR technician_email='$email') AND technician_is_active='1'");
		if($qry->num_rows() > 0){
			$returnStatus = TRUE;
		}
		
		return $returnStatus;
	}	
	
	//get technician by id 
	function get_technician($technician_id=0){
		$sk = & get_instance();
		$returnAry = array();
		$qry = $sk->db->query("SELECT * FROM technician_hd WHERE technician_id='$technician_id'");
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}
		return $returnAry;
	}
	//***********************code for the sla starts from here ****************************************
	//get work start and end time 
	function get_work_start_end($id)
	{
		$sk = & get_instance();
		$returnAry = array();
		$qry = $sk->db->query("SELECT * FROM work_start_end_time_hd WHERE id='$id'");
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}
		return $returnAry;
	}
	
	function get_duration_from_type($convert_type)
	{	
		$duration = 1;
		switch($convert_type){
			case 'Hrs':
			    $duration *= ($duration*9.5); 
			break;
			case 'Min':
			    $duration *= ($duration*60); 
			break;
			case 'Sec':
			   $duration *= ($duration*60);
			break;	
		}
		return $duration;
	}
	
	function get_sla_duration_from_type($convert_type,$converted_type,$duration)
	{	
		$returnDuration = 1;
		switch($convert_type){
			case 'Day':
			switch($converted_type)
			{
				case 'Sec':
					$returnDuration *= get_duration_from_type('Hrs');
					$returnDuration *= get_duration_from_type('Min');
					$returnDuration *= get_duration_from_type('Sec');
				break;
				case 'Min':
					$returnDuration *= get_duration_from_type('Hrs');
					$returnDuration *= get_duration_from_type('Min');
				break;
				case 'Hrs':
				   $returnDuration *= get_duration_from_type('Hrs');
				break;
			}		
			break;
			case 'Hrs':
			switch($converted_type)
			{
				case 'Sec':
					
					$returnDuration *= get_duration_from_type('Min');
					$returnDuration *= get_duration_from_type('Sec');
				break;
				case 'Min':
					
					$returnDuration *= get_duration_from_type('Min');
				break;
			}
			break;
			case 'Min':
			switch($converted_type)
			{
				case 'Sec':
					$returnDuration *= get_duration_from_type('Sec');
				break;
			}
			break;
			
        }
		return ($returnDuration*$duration);
	}
	
	// get sla duration and sla duration unit
	function get_item_sla_duration($id)
	{
		$sk = & get_instance();
		$returnAry = array();
		$qry = $sk->db->query("SELECT sla_duration,sla_duration_unit FROM items_hd WHERE items_id='$id'");
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}
		return $returnAry;
	}
	
	function get_holidays_for_month($month =0)
	{
		$sk = & get_instance();
		$returnAry = array();
		$qry = $sk->db->query("SELECT * FROM holiday_list_hd WHERE YEAR(holiday_date)= YEAR(NOW()) AND MONTH(holiday_date)= MONTH(DATE_FORMAT(NOW() + INTERVAL $month MONTH ,'%Y-%m-%d')) AND is_holiday_active = 1");
		if($qry->num_rows() > 0){
			$result  = $qry->result_array();
			foreach($result as $res){
				$returnAry [] = $res["holiday_date"];
			}
		}
		return $returnAry;
	}
	
    function format_time($t,$f=':') 
	{
        return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
	}
	
	function get_all_request()
	{
	 	$sk = & get_instance();
		$request = array(); 
		//$qry = $sk->db->query("select * from user_request_hd where is_request_completed = 0  and request_is_active = 1 ORDER BY request_created_dt DESC");
		
		//before query to user
		//$qry = $sk->db->query("select DISTINCT t1.* from user_request_hd as t1 LEFT JOIN request_assign_person_hd as t2 ON t1.user_request_id = t2.user_request_id WHERE t1.is_request_completed = 0  and t1.request_is_active = 1 and t2.task_status NOT IN('close','resolve','query_to_user') ORDER BY t1.request_created_dt DESC;");
		
		$qry = $sk->db->query("select DISTINCT t1.* from user_request_hd as t1 LEFT JOIN request_assign_person_hd as t2 ON t1.user_request_id = t2.user_request_id WHERE t1.is_request_completed = 0  and t1.request_is_active = 1 and t2.task_status NOT IN('close','resolve') ORDER BY t1.request_created_dt DESC");
		
		if($qry->num_rows() > 0){
		  $result = $qry->result_array();
		  foreach($result as $record){
			  $request[] = $record["user_request_id"];  
		  }
		  
		}
		return $request;
	}
	
	function is_request_escalated($request_id,$escalation_level =0)
	{
		$sk = & get_instance();
		$row = 0;
		switch($escalation_level){
			case 'task_escalated';
				$escalation_level = 'task_escalated';	
		    break;
			case 'task_escalated_level_one';
				$escalation_level = 'task_escalated_level_one';
		    break;
			case 'task_escalated_level_two':
				$escalation_level = 'task_escalated_level_two';
			break;
		}
		$qry = $sk->db->query("SELECT $escalation_level as escalation  FROM user_request_hd where user_request_id = $request_id");
		if($qry->num_rows() > 0)
		{
			$result = $qry->row();
			$row = $result->escalation;
		}
		return $row;
	}
	
	// checks the request id  and find the emp code to whom this issue is escalated
	function get_escalation_emp_code($user_request_id){
		//task_escalated,task_escalated_level_one,task_escalated_level_two,task_escalated_level_three
		$emp_code = NULL;
		$sk = & get_instance();
		$escalation_level = get_escalation_level($user_request_id);
		$qry = $sk->db->query("SELECT t2.esc_level, t2.emp_code as escalated_ti, t1.*,t2.*,t3.*,t4.* from escalation_group t1 JOIN escalation_level_hd as t2 ON t1.escalation_level_id = t2.esc_level_id JOIN request_assign_person_hd as t3 ON t3.group_id = t1.group_id JOIN user_request_hd as t4 
		ON t4.user_request_id = t3.user_request_id where t3.request_assign_person_is_active ='1'and   t1.escalation_group_is_active ='1' and t4.user_request_id=$user_request_id limit $escalation_level,1; ");
		$result = $qry->row();
		if($qry->num_rows() > 0)
		{
			$result = $qry->row();
			$emp_code = $result->emp_code;
		}
		return $emp_code;
	}
	
	// check the escalation level from user request id
	function get_escalation_level($user_request_id)
	{
		$limit_val = 0;
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT (sum(t1.task_escalated + t1.task_escalated_level_one + t1.task_escalated_level_two + t1.task_escalated_level_three)-1) as limit_val from user_request_hd as t1 JOIN request_assign_person_hd as t2 ON t1.user_request_id = t2.user_request_id where t1.user_request_id=$user_request_id AND t2.request_assign_person_is_active='1'");
		if($qry->num_rows() > 0)
		{
			$result = $qry->row();
			$limit_val = $result->limit_val;
		}
		return $limit_val;
	}
	
	//this function is copied to check sla time spent for get_sla_time_spent
	function sla_time_spent($request_id,$tech_id)
    {	
		$sk = & get_instance();
        $result = 0;
		//get actual time 
	   $qry = $sk->db->query("SELECT SUM(time_spent) AS time_spent FROM sla_time_log_hd WHERE user_request_id = $request_id GROUP BY user_request_id");
	   
	    //$qry = $sk->db->query("SELECT SUM(time_spent) AS time_spent FROM sla_time_log_hd where user_request_id = $id and technician_id =$tech_id group by user_request_id");
	      
        if($qry->num_rows() > 0){
            $row  = $qry->row();
            $result = $row->time_spent;
			$returnData = $result;
        }
        return $returnData;
    }
	
	function get_sla_time_spent($request_id,$tech_id)
    {	
		$sk = & get_instance();
        $result = 0;
		//get actual time 
	   $qry = $sk->db->query("SELECT SUM(time_spent) AS time_spent FROM sla_time_log_hd WHERE user_request_id = $request_id AND time_spent_date = DATE(NOW()) GROUP BY user_request_id");
	  //$qry = $sk->db->query("SELECT SUM(time_spent) AS time_spent FROM sla_time_log_hd where user_request_id = $id and technician_id =$tech_id group by user_request_id");
	   
	      
        if($qry->num_rows() > 0){
            $row  = $qry->row();
            $result = $row->time_spent;
			$returnData = $result;
        }
        return $returnData;
    }
	
	function getClockTime_today($request_id,$tech_id){
		$sk = & get_instance();
		//get hold/query to user time
	   $qry1 = $sk->db->query("SELECT SUM(clock_duration) AS time_spent1 FROM clock_request_hd WHERE user_request_id = $request_id AND clock_duration_date = DATE(NOW()) GROUP By user_request_id");
	   if($qry1->num_rows() > 0){
		   $not_work_time = $qry1->row();
		   $not_work_time_sec = $not_work_time->time_spent1;
	   }else{
		   $not_work_time_sec = 0;
	   }
	   return $not_work_time_sec;
	}
	
	// this function is copied to check clock time for getClockTime_today
	function getClockTime($request_id,$tech_id){
		$sk = & get_instance();
		//get hold/query to user time
	   $qry1 = $sk->db->query("SELECT SUM(clock_duration) AS time_spent1 FROM clock_request_hd WHERE user_request_id = $request_id  GROUP By user_request_id");
	   
	   if($qry1->num_rows() > 0){
		   $not_work_time = $qry1->row();
		   $not_work_time_sec = $not_work_time->time_spent1;
	   }else{
		   $not_work_time_sec = 0;
	   }
	   return $not_work_time_sec;
	}
	
	// gets the sla duration of item and converts it into seconds
	function sla_duration_item($get_sla)
	{	
		$sla_duration = 0;
		if(!empty($get_sla))
		 {
			 //echo "<pre>";print_r($user_req);exit();
			 
			  $sla_duration      =  $get_sla->sla_duration;
			  $sla_duration_unit = $get_sla->sla_duration_unit;
			  $sla_duration = get_sla_duration_from_type($get_sla->sla_duration_unit,'Sec',$sla_duration);
		 }
		return $sla_duration; 
	}
	//******************code for the sla ends here ******************************************





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// code to stop one technician accessing another technician request by id
	function is_request_assign_technician($request_id,$email){
		$returnStatus = false;
		$sk = & get_instance();
		$qry = $sk->db->query("select t2.technician_id from user_request_hd as t1 JOIN request_assign_person_hd as t2 ON t1.user_request_id = t2.user_request_id JOIN technician_hd as t3 ON t3.technician_id = t2.technician_id 	where t1.user_request_id = $request_id and t3.technician_email ='$email' and t2.request_assign_person_is_active ='1'");
		//echo $sk->db->last_query();exit();
		$result = $qry->row();
		if($qry->num_rows() > 0){
		 $returnStatus = true;  		  
		}
		else{
			redirect("helpdesk/dashboard");
		}
        return $returnStatus;		
	}
	
	function is_escalated_emp($emp_code)
	{
		$returnStatus = false;
		$sk = & get_instance();
		$qry = $sk->db->query("Select * from escalation_level_hd where is_active = 1 and emp_code='$emp_code'");
		if($qry->num_rows() >0){
			$returnStatus = true;
		}
		return $returnStatus;
	}
	
	function is_queried_by_tech($user_request_id)
	{
		$returnStatus = false;
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT t2.task_status from user_request_hd  as t1 JOIN request_assign_person_hd as t2 ON t1.user_request_id = t2.user_request_id where t1.request_is_active =1 AND t1.user_request_id = $user_request_id AND t2.request_assign_person_is_active = '1';");
		if($qry->num_rows() >0){
			$result = $qry->row();
			$task_status = $result->task_status;
			if($task_status=='query_to_user'){
				$returnStatus = true;
			}
		}
		return $returnStatus;
	}
	
	function request_sla_stopped($user_request_id,$tech_id)
	{	
		$sla_stopped_sec = 0;
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT TIMESTAMPDIFF(SECOND,clock_start_datetime,clock_stop_datetime) as sla_stop_time FROM clock_request_hd where user_request_id = $user_request_id AND technician_id = $tech_id");
		if($qry->num_rows() >0)
		{	
			$result = $qry->row();
			$sla_stopped_sec = $result->sla_stop_time;
		}
		return $sla_stopped_sec;
	}
	
	//Admin panel code start 
	function get_all_record_by_code($tbl_code = 0){
		$sk = & get_instance();
		if($tbl_code != ''){
			switch($tbl_code){
				case 'dept':
					$qry = $sk->db->query("SELECT * FROM dept_hd WHERE dept_is_active=1");
				break;
				
				case 'category':
					$qry = $sk->db->query("SELECT * FROM category_hd WHERE category_is_active=1");
				break;
				
				case 'subcategory':
					$qry = $sk->db->query("SELECT * FROM sub_category_hd WHERE sub_category_is_active=1");
				break;
				
				case 'item':
					$qry = $sk->db->query("SELECT * FROM items_hd WHERE item_is_active=1");
				break;
				
				case 'group':
					$qry = $sk->db->query("SELECT * FROM groups_hd WHERE group_is_active=1");
				break;
				
				case 'technician':
					$qry = $sk->db->query("SELECT * FROM technician_hd WHERE technician_is_active=1 order by tech_name");
				break;
			}
			if($qry->num_rows() > 0){
				$returnData = $qry->result();
			}else{
				$returnData = array();
			}
			return $returnData;
		}
	}
	
	function get_Name_record_by_Id($tbl_code = 0,$primary_id = 0){
		$sk = & get_instance();
		if($tbl_code != ''){
			switch($tbl_code){
				case 'dept':
					$qry = $sk->db->query("SELECT dept_name as returnName FROM dept_hd WHERE dept_id=$primary_id");
				break;
				
				case 'category':
					$qry = $sk->db->query("SELECT category_name as returnName FROM category_hd WHERE category_id=$primary_id");
				break;
				
				case 'subcategory':
					$qry = $sk->db->query("SELECT sub_category_name as returnName FROM sub_category_hd WHERE sub_category_id=$primary_id");
				break;
				
				case 'item':
					$qry = $sk->db->query("SELECT item_name as returnName FROM items_hd WHERE items_id=$primary_id");
				break;
			}
			
			if($qry->num_rows() > 0){
				$recordData = $qry->row();
				$returnData = $recordData->returnName;
			}else{
				$returnData = 'No name found';
			}
			return $returnData;
		}
	}
	
	//get user list
	function get_user_list($emp_email_id = ''){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM ldapusers WHERE u_status='1'");
		$empList = '';
		if($qry->num_rows() > 0){
			$empListData = $qry->result();
			foreach($empListData as $emp_record){
				if($emp_email_id !=''){
					if($emp_email_id === $emp_record->u_email){
						$empList .='<option value="'.$emp_record->u_email.'~'.$emp_record->u_name.'~'.$emp_record->u_employeeid.'" selected>'.$emp_record->u_name.'</option>';
					}else{
						$empList .='<option value="'.$emp_record->u_email.'~'.$emp_record->u_name.'~'.$emp_record->u_employeeid.'">'.$emp_record->u_name.'</option>';
					}
				}else{
					$empList .='<option value="'.$emp_record->u_email.'~'.$emp_record->u_name.'~'.$emp_record->u_employeeid.'">'.$emp_record->u_name.'</option>';
				}
			}
		}else{
			$empList .='<option value="">No technician available</option>';
		}
		return $empList;
	}
	//Admin panel code end
	
	//get corp. comm event name start 
	function get_current_event_del(){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM event_list WHERE event_is_active = '1'");
		if($qry->num_rows() > 0){
			$eventData = $qry->row();
			$eventName = $eventData->event_name;
		}else{
			$eventName = 'No event found';
		}
		return $eventName;
	}
	//get corp. comm event name end
	
	function get_state(){
		$returnStatus = array();
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT * FROM state_hd WHERE state_id_active='1'");
		if($qry->num_rows() > 0){
			$returnStatus = $qry->result();
		}
		return $returnStatus;
	}
	
	function getRequestCode($requestId=0){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT ur.user_request_id,s.state_name,s.state_code,sl.site_location_name,sl.city_code FROM user_request_hd ur LEFT JOIN state_hd s ON (s.state_id = ur.state_id) LEFT JOIN site_locations_hd sl ON (sl.site_location_id=ur.requester_location_id) WHERE ur.request_is_active='1' AND ur.user_request_id='$requestId'");
		if($qry->num_rows() > 0){
			$requestData = $qry->row();
			//echo '<pre>';print_r($requestData);exit;
			$requestId1 = str_pad($requestId, 5, '0', STR_PAD_LEFT); 
			if($requestData->state_code !='' && $requestData->city_code !=''){
				$requestCode = $requestData->state_code.''.$requestData->city_code.''.$requestId1;
			}else{
				$requestCode = $requestId1;
			}
		}else{
			$requestCode = $requestId1;
		}
		return $requestCode;
	}

	function myrequest($emp_code){
		$sk = & get_instance();
		$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) WHERE ra.request_assign_person_is_active=1 AND ur.request_is_active=1 AND ur.user_code='$emp_code' ORDER BY ur.last_modified_dt DESC, ur.user_request_id DESC;");
		return $qry->result();
	}
	
	function check_ticket_view($emp_code = 0,$emp_email='',$request_id=0){
		$rk = & get_instance();
		$qry = $rk->db->query("SELECT * FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) WHERE ra.request_assign_person_is_active=1 AND ur.request_is_active=1 AND (ur.user_code='".$emp_code."' OR t.tech_code = '".$emp_code."' OR ur.behalf_of='".$emp_email."') AND ur.user_request_id='".$request_id."' ORDER BY ur.last_modified_dt DESC, ur.user_request_id DESC;");
		
		$qry1 = $rk->db->query("SELECT * FROM escalation_level_hd WHERE emp_code = '".$emp_code."' AND is_active='1'");
		
		if($qry->num_rows() > 0 || $qry1->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function get_request_count_by_email_report(){
		$sk = & get_instance();
		$returnStatusDay = array();
		$statusAry = array('open','hold','wip','close','resolve');
		
		//$qry = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) LEFT JOIN groups_hd g ON(rap.group_id=g.group_id) WHERE rap.request_assign_person_is_active=1");
		
		//SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed,request_created_dt,DATE_FORMAT(request_created_dt, "%Y-%m-%d") as date_time FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) LEFT JOIN groups_hd g ON(rap.group_id=g.group_id) WHERE rap.request_assign_person_is_active=1 AND DATE_FORMAT(ur.request_created_dt, "%Y-%m-%d")='2017-08-24' GROUP BY date_time;
		
		$qry1 = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active, is_request_completed,request_created_dt,DATE_FORMAT(request_created_dt, '%Y-%m-%d') as date_time FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) LEFT JOIN groups_hd g ON(rap.group_id=g.group_id) WHERE rap.request_assign_person_is_active=1 GROUP BY date_time ORDER BY ur.request_created_dt DESC LIMIT 0,12");
		
		$allTicketData = $qry1->result();
		
		foreach($allTicketData as $ticket_date){
			
			$qry = $sk->db->query("SELECT user_code,behalf_of,requester_name,task_status,request_assign_person_is_active,is_request_completed,request_created_dt,DATE_FORMAT(request_created_dt, '%Y-%m-%d') as date_time FROM user_request_hd ur LEFT JOIN request_assign_person_hd rap ON(ur.user_request_id=rap.user_request_id) LEFT JOIN groups_hd g ON(rap.group_id=g.group_id) WHERE rap.request_assign_person_is_active=1 AND DATE_FORMAT(ur.request_created_dt, '%Y-%m-%d')='".$ticket_date->date_time."'");

			if($qry->num_rows() > 0){
				$countComplete = 0; $countResolve = 0; $countClose = 0; $countWip = 0; $countHold = 0; $countOpen = 0;
				$requestData = $qry->result();
				
				foreach($requestData as $requestRecord){
					
					if($requestRecord->task_status=='open' && $requestRecord->is_request_completed ==0){
						$countOpen += 1;
					}
					
					if($requestRecord->task_status=='hold' && $requestRecord->is_request_completed ==0){
						$countHold += 1;
					}
					
					if($requestRecord->task_status=='wip' && $requestRecord->is_request_completed ==0){
						$countWip += 1;
					}
					
					if($requestRecord->task_status=='close' && $requestRecord->is_request_completed ==0){
						$countClose += 1;
					}
					
					if($requestRecord->task_status=='resolve' && $requestRecord->is_request_completed ==0){
						$countResolve += 1;
					}
					
					if($requestRecord->is_request_completed ==1){
						$countComplete += 1;
					}
				}
				//$totalRequest = $countComplete + $countResolve + $countClose + $countWip + $countHold + $countOpen;
				$inprocess = $countClose + $countWip + $countOpen;
				$totalRequest = $inprocess + $countHold + $countComplete;
				$returnStatus = array(
					'open'=> $countOpen,
					'hold'=> $countHold,
					'wip'=> $countWip,
					'close'=> $countClose,
					'resolve'=> $countResolve,
					'completed'=> $countComplete,
					'inprocess'=>$inprocess,
					'onhold'=>$countHold,
					'total'=>$totalRequest,
					'ticket_date'=>date('d-m-Y',strtotime($ticket_date->date_time))
				);
				$returnStatusDay['day_ary'][] = $returnStatus;
			}
		}
		
		return $returnStatusDay;
    }
	
	function get_request_by_date($request_date='',$dept_id=''){
		$sk = & get_instance();
		if($dept_id !=''){
			$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) where DATE_FORMAT(ur.request_created_dt, '%Y-%m-%d')='".$request_date."' AND ur.dept_id='".$dept_id."' AND ra.request_assign_person_is_active=1 AND ur.request_is_active = 1  group by ur.user_request_id  ORDER BY ur.user_request_id DESC");
			
		}else{
			
			$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) where DATE_FORMAT(ur.request_created_dt, '%Y-%m-%d')='".$request_date."' AND ra.request_assign_person_is_active=1 AND ur.request_is_active = 1  group by ur.user_request_id  ORDER BY ur.user_request_id DESC");
		}
		//$qry = $sk->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) where DATE_FORMAT(ur.request_created_dt, '%Y-%m-%d')='".$request_date."' AND ra.request_assign_person_is_active=1 AND ur.request_is_active = 1  group by ur.user_request_id  ORDER BY ur.user_request_id DESC");
		
		if($qry->num_rows() > 0){
			$requestList = $qry->result();
		}else{
			$requestList = array();
		}
		return $requestList;
    }
	
	//get dept heads name
	function get_head_email_by_dept_id($dept_id=0){
		$rk = & get_instance();
		$returnEmpData = '';
		$emp1_data = array();
		$qry = $rk->db->query("SELECT u.u_employeeid, u.u_name,u.u_email,elh.dept_id FROM escalation_level_hd elh LEFT JOIN users u ON (elh.emp_code=u.u_employeeid) WHERE elh.dept_id='$dept_id'");
		if($qry->num_rows() > 0){
			$emp_data = $qry->result();
			foreach($emp_data as $emp_record){
				$emp1_data[] = $emp_record->u_email;
			}
			$returnEmpData = implode(",",$emp1_data);
		}
		return $returnEmpData;
	}
//****************************** HELPDESK CODE END ****************************************************************************************

//****************************** Daily task CODE Start **************************************************************************************
	function is_daily_task_access($module_code=''){
		$rk = & get_instance();
		$returnStatus = false;
		$rk->user_ldamp = $rk->session->userdata('user_ldamp');
		$emp_code = $rk->user_ldamp['employeeid'];
		$qry = $rk->db->query("SELECT * FROM permission_module pm LEFT JOIN usr_permission_map upm ON (pm.permission_module_id = upm.permission_module_id) LEFT JOIN usr_module_permission ump ON (ump.daily_task_permission_id = upm.permission_usr_id) WHERE pm.module_is_active=1 AND ump.permission_is_active=1 AND upm.permission_map_is_active=1 AND ump.emp_code ='".$emp_code."' AND pm.module_code='".$module_code."' ");
		if($qry->num_rows() > 0){
			$returnStatus = true;
		}
		return $returnStatus;
	}
	
	function todayWorkingHrs(){
		$rk = & get_instance();
		$returnStatus = false;
		$rk->user_ldamp = $rk->session->userdata('user_ldamp');
		$emp_code = $rk->user_ldamp['employeeid'];
		$todayDate = date('Y-m-d');
		$qry = $rk->db->query("select sum(task_hrs) as hrs,sum(task_min) as min From daily_task WHERE task_is_active=1 AND emp_code ='".$emp_code."' AND (end_date = '".$todayDate."' OR start_date='".$todayDate."')");
		$resultRow = $qry->row();
		//echo 'Today working time <b>'.str_pad($resultRow->hrs, 2, '0', STR_PAD_LEFT).':'.str_pad($resultRow->min, 2, '0', STR_PAD_LEFT).'</b>';
	}
//****************************** Daily task CODE END ****************************************************************************************





