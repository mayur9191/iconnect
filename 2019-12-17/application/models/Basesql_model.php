<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_Model extends CI_Model {

    function __construct() {
        parent::__construct();
		//$this->mssql = $this->load->database('mssql', TRUE);
    }
	
    function run_query($query) {
        return ($this->db->query($query)->result());
    }

    function getMaxId($TableName, $ColName) {
        $query = $this->db->query("select max(" . $ColName . ") as Id from " . $this->db->dbprefix($TableName))->result();
        return $query[0]->Id;
    }

    function insert_operation($inputdata, $table, $email = '') {

        if ($this->db->insert($this->db->dbprefix($table), $inputdata))
            return 1;
        else
            return 0;
    }

    function insert_operation_id($inputdata, $table, $email = '') {
        $result = $this->db->insert($this->db->dbprefix($table), $inputdata);
        return $this->db->insert_id();
    }

    function update_operation($inputdata, $table, $where) {
//        print_r($inputdata);
//        exit();
        
        $result = $this->db->update($this->db->dbprefix($table), $inputdata, $where);
        return $result;
    }

    function fetch_records_from($table, $condition = '', $select = '*', $order_by = '', $order_type = 'desc', $limit = '') {
        $this->db->select($select, FALSE);
        $this->db->from($this->db->dbprefix($table));
        if (!empty($condition))
            $this->db->where($condition);
        if (!empty($order_by))
            $this->db->order_by($order_by, $order_type);
        if (!empty($limit))
            $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result();
    }

    function fetch_single_column_value($table, $column, $where) {
        $this->db->select($column, FALSE);
        $this->db->from($this->db->dbprefix($table));
        if (!empty($where))
            $this->db->where($where);
        $result_rs = $this->db->get();
        $result = $result_rs->result();
        if (count($result) > 0)
            $ret = $result[0]->$column;
        else
            $ret = '-';
        return $ret;
    }

    function changestatus($table, $inputdata, $where) {
        $result = $this->db->update($this->db->dbprefix($table), $inputdata, $where);
        return $result;
    }

    function delete_record($table, $where) {
        $result = $this->db->delete($table, $where);
        return $result;
    }

    function check_duplicate($table_name, $cond, $cond_val) {
        $col_name = '*';
        $this->db->where(array(
            $cond => $cond_val
        ));
        $this->db->from($this->db->dbprefix($table_name));
        $query = $this->db->get();
        $rows = $query->num_rows();
        if ($rows > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_details($table) {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function fetchDynamicData($formname, $uniqueFieldName, $uniqueId) {
        $this->db->where(array(
            $uniqueFieldName => $uniqueId
        ));
        $this->db->select('*');
        $this->db->from($this->db->dbprefix('dynamic_form_values'));
        $rs = $this->db->get();
        return $rs->result();
    }

    /*     * **** Get Category name by Id ***** */

    public function getCategoryNameById($category_id = null) {
        $category_name = "";
        if ($category_id > 0) {
            $category_name = $this->db->select('category_name')->get_where('categories', array(
                        'category_id' => $category_id
                    ))->row()->category_name;
        }
        return $category_name;
    }

    // Prepare Dropdown Data
    function prepareDropdown($table, $column_id, $column_name, $cond = array()) {
        $list = $this->db->get_where($this->db->dbprefix($table), $cond)->result();
        $option_data = array(
            '' => 'Select'
        );
        foreach ($list as $key => $val) {
            $option_data[$val->$column_id] = $val->$column_name;
        }
        return $option_data;
    }
    function getData($loadType,$loadId){
		if($loadType=="state"){
			$fieldList='id,name';
			$table='states';
			$fieldName='country_id';
			$orderByField='name';						
		}else{
			$fieldList='id,name';
			$table='cities';
			$fieldName='state_id';
			$orderByField='name';	
		}
		
		$this->db->select($fieldList);
		$this->db->from($table);
		$this->db->where($fieldName, $loadId);
		$this->db->order_by($orderByField, 'asc');
		$query=$this->db->get();
		return $query; 
	}

}
?> 
