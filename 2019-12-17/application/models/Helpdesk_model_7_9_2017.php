<?php
class Helpdesk_model extends CI_Model {

	public function __Construct()
	{
       parent::__Construct();
	}
	public function get_departments()
	{
	  $qry = $this->db->select("*")->from("dept_hd")->where("dept_is_active",'1')->get();
	    if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
		
	}
	public function get_categories_hd($dept_id=0){
		if($dept_id >0){
			$qry = $this->db->select("*")->from("category_hd")->where("dept_id",$dept_id)->where("category_is_active",'1')->get();
		}
		else{
			$qry = $this->db->select("*")->from("category_hd")->where("category_is_active",'1')->get();
		}
		
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function get_subcategory_hd($category_id)
	{
		$qry = $this->db->select("*")->from("sub_category_hd")->where("category_id",$category_id)->where("sub_category_is_active",'1')->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	public function get_item_hd($sub_category_id)
	{
		$qry = $this->db->select("*")->from("items_hd")->where("sub_category_id",$sub_category_id)->where("item_is_active",'1')->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	public function save_user_request()
	{	
		$user['user_ldamp'] = $this->user_ldamp;
		$postData = $this->input->post();
		$data = array(
			'user_id'=>$user['user_ldamp']['user_id'],
			'user_code'=>$user['user_ldamp']['employeeid'],
			//'request_type'=>'User', 
			'service_request_type'=>$postData['request_type'],
			'request_priority'=>$postData['priority'],
			'requester_name'=>$postData['request_name'],
			'requester_location_id'=>$postData['request_location'],
			'requester_location_name'=>$postData['requester_location_name'],
			'dept_id'=>$postData['dept_id'],
			'category_id'=>$postData['category'],
			'subcategory_id'=>$postData['sub_category'], 
			'item_id'=>$postData['item'], 
			'request_subject'=>$postData['request_subject'], 
			'request_desc'=>$postData['request_description'],
			//'technician_assign_status'=> 
			'request_created_dt'=>date('Y-m-d H:i:s')
 		);
		
		$this->db->insert('user_request_hd',$data);
		if($this->db->affected_rows()>0){
			return true;
		}
		return false;
		
	}
	
	//ajx guideline
	public function getAjxGuideDataByDeptId($dept_id){
		//sql statement 
		$qry = $this->db->select("*")->from("dept_guideline_hd")->where("dept_id",$dept_id)->where("guideline_is_active",1)->get();
		if($qry->num_rows() > 0){
			$guidelineData= $qry->row();
			$resultData = $guidelineData->guideline_txt;
		}else{
			$resultData = 'No guideline for the department';
		}
		return $resultData;
	}
	
	
	//RV
	//cron job code start 
	public function get_unassign_request(){
		$qry = $this->db->query("SELECT * FROM user_request_hd WHERE technician_assign_status = 0 AND request_is_active=1 AND is_request_completed =0");
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	//cron job code end
	//nNEW
	public function assign_request($technician_id=0, $request_id=0,$item_id=0){
		//assign status 1
		if($technician_id > 0){
			$assign_person = array(
				'group_id'=>get_groupId_itemId($item_id),
				'technician_id'=>$technician_id,
				'user_request_id'=>$request_id,
				'request_assign_person_dt'=>date("Y-m-d H:i:s"),
				'assign_status'=>1
			);
			if($this->db->insert('request_assign_person_hd',$assign_person)){
				$returnStatus = true;
				
				//update user request tbl 
				$request_ary = array(
					'technician_assign_status'=>1
				);
				$this->db->where('user_request_id',$request_id);
				$this->db->update('user_request_hd',$request_ary);
			}else{
				$returnStatus = false;
			}
		}else{
			$returnStatus = false;
		}
		return $returnStatus;
	}
	
	//task complete 
	public function tech_task_complete($request_id=0){
		//update status - complated 
		$updt_request = array(
			'is_request_completed'=>1,
			'task_completed_dt'=>date('Y-m-d H:i:s')
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->update('user_request_hd',$updt_request);
		
		//free the technician
		$updt_technician = array(
			'assign_status'=>0
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->where('assign_status',1);
		$this->db->where('is_escalated',0);
		if($this->db->update('request_assign_person_hd',$updt_technician)){
			$returnStatus = true;
		}else{
			$returnStatus = false;
		}
		return returnStatus;
	}
	
	//escalated task complete
	public function ho_task_complete($request_id=0){
		//update status - complated 
		$updt_request = array(
			'is_request_completed'=>1,
			'task_completed_dt'=>date('Y-m-d H:i:s')
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->update('user_request_hd',$updt_request);
		
		//free the technician
		$updt_technician = array(
			'assign_status'=>0,
			'escalated_dt'=>date('Y-m-d H:i:s')
		);
		$this->db->where('user_request_id',$request_id);
		$this->db->where('assign_status',1);
		$this->db->where('is_escalated',1);
		if($this->db->update('request_assign_person_hd',$updt_technician)){
			$returnStatus = true;
		}else{
			$returnStatus = false;
		}
		return $returnStatus;
	}
	
	//escalate task to HO cron job
	public function escalate_task($request_id = 0){
		//find task who it is assigned
		$qry = $this->db->query("SELECT * FROM request_assign_person_hd WHERE user_request_id = '$request_id' AND is_escalated = 0 AND request_assign_person_is_active = 1 AND technician_id !=0");
		if($qry->num_rows() > 0){
			$assign_data = $qry->row();
			$assign_request_id = $assign_data->request_assign_person_id;
			$updt_assign_array = array(
				'is_escalated'=>1,
				'escalated_dt'=>date('Y-m-d H:i:s')
			);
			$this->db->where('request_assign_person_id',$assign_request_id);
			$this->db->update('request_assign_person_hd',$updt_array);
			
			//update request escalted 
			$updt_request_ary = array(
				'task_escalated'=>1
			);
			$this->db->where('user_request_id',$request_id);
			$this->db->update('user_request_hd',$updt_request_ary);
			
			//add ecalated task 
			$task_data = get_requestdata_by_id($request_id);
			$escalate_ary = array(
				'group_id'=>get_groupId_itemId($task_data->item_id),
				'technician_id'=>ho_technician_assign($task_data->item_id),
				'user_request_id'=>$request_id,
				'request_assign_person_dt'=>date('Y-m-d H:i:s'),
				'assign_status'=>1,
				'escalation_status'=>0
			);
			if($this->db->insert('escalated_task',$escalate_ary)){
				$returnStatus = true;
			}else{
				$returnStatus = false;
			}
			return $returnStatus;
		}
	}
	
	
	//site location
	public function get_site_location(){
		$qry = $this->db->select("*")->from("site_locations_hd")->where("site_location_is_active",1)->get();
		if($qry->num_rows() > 0){
			$resultData = $qry->result();
		}else{
			$resultData = array();
		}
		return $resultData;
	}
	
	//get assign report
	public function get_assign_tech_list(){
		$qry = $this->db->query("SELECT *,ur.user_request_id as ur_user_request_id FROM user_request_hd ur LEFT JOIN request_assign_person_hd ra ON (ur.user_request_id = ra.user_request_id) LEFT JOIN groups_hd g ON(ra.group_id=g.group_id) LEFT JOIN technician_hd t ON (ra.technician_id = t.technician_id) ORDER BY ur.user_request_id DESC");
		if($qry->num_rows() > 0){
			$resultAry = $qry->result();
		}else{
			$resultAry = array();
		}
		return $resultAry;
	}
	
}