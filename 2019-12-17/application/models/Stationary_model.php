<?php 
class Stationary_model extends CI_Model{
	
	public function __Construct(){
		
		 parent::__Construct();
	}
	
	
	public function getStationary_master_list(){
		
		$result = array();
		$query = $this->db->select("*")->from("stationary_master_list")->get();
		if($query->num_rows()>0){
			$result = $query->result();
		}
		return $result;
	}
	
	public function getLastEnterItemCode(){
		$query = $this->db->select("item_code")->from("stationary_master_list")->order_by("item_code","desc")->limit(1)->get();
		$result = $query->row();
		return $result;
	}
	
	public function add_StationaryItem($data){
		$returnStatus = 0;
		if($this->db->insert("stationary_master_list",$data)){
			$returnStatus = 1;
		}
		return $returnStatus;
		
	}
	
	public function getStationary_item($id){
		$qry = $this->db->select("*")->from("stationary_master_list")->where("id",$id)->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function getStationary_itemBYCode($id){
		$qry = $this->db->select("*")->from("stationary_master_list")->where("item_code",$id)->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function getStationary_totalPurchasedQuantity($id){
		$qry = $this->db->select("total_purchased_quantity")->from("stationary_master_list")->where("id",$id)->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function update_StationaryItem($updateArray,$id){
		$returnStatus = 0;
		$this->db->where('id',$id);
		if($this->db->update("stationary_master_list",$updateArray)){
			$returnStatus = 1;
		}
		return $returnStatus;
		
	}
	
	public function getStationaryitems(){
		$result = array();
		$query = $this->db->select("item_name")->from("stationary_master_list")->get();
		if($query->num_rows()>0){
			$result = $query->result();
		}
		return $result;
	}
	
	public function assign_stationary_userList(){
		
		$result = array();
		$query = $this->db->select("*")->from("stationary_assign_item")->get();
		if($query->num_rows()>0){
			$result = $query->result();
		}
		return $result;
		
	}
	
	public function getItemPuchasedMIS($id){
		//$qry = $this->db->select("*")->from("stationary_item_date_quantity")->where("item_code",$id)->get();
		//$qry = $this->db->query("SELECT * FROM stationary_item_date_quantity WHERE YEAR(quantity_update) = YEAR(CURDATE()) AND item_code='$id'");
		$qry = $this->db->query("SELECT * FROM stationary_item_date_quantity WHERE item_code='$id'");
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
		
	}
	
}	


?>