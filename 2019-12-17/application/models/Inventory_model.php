<?php
class Inventory_model extends CI_Model {
	
	public function __Construct(){
       parent::__Construct();
	}
	public function get_model_no($manufacturer_id=0){
		$result = array();
		$query = $this->db->select("model_no_id,model_name")->from("model_no_in")->where("manufacturer_id",$manufacturer_id)->where('model_is_active','1')->get();
		if($query->num_rows()>0){
			$result = $query->result_array();
		}
		return $result;
	}
	
	public function get_manufacturer($asset_type_id=0){
		$result = array();
		$query = $this->db->query("SELECT manufacturer_in.* from asset_type_in JOIN asset_type_manufacturer_in 
ON  asset_type_manufacturer_in.asset_type_id = asset_type_in.asset_type_id
JOIN manufacturer_in ON manufacturer_in.manufacturer_id = asset_type_manufacturer_in.manufacturer_id
where asset_type_manufacturer_in.asset_type_id=$asset_type_id AND manufacturer_in.manufacturer_is_active =1");
		if($query->num_rows()>0){
			$result = $query->result_array();
		}
		return $result;
	}
	public function save_assigned_assets(){
		$postData = $this->input->post();
		$data_software = array();
		$data_hardware = array();
		$i=0;$j=0;$k=0;$m=0;
		$data = array(
			"emp_code" 			=> $postData["emp_code"],
			"employee_email" 	=> $postData["employee_email"],
			"procured_date"     => date("Y-m-d",strtotime($postData["procured_date"])),
			"procure_cmpy"      => $postData["procurecmpy"],
			"allocation_date"   => date("Y-m-d",strtotime($postData["allocation_date"])),
			"old_asset_tag"     => $postData["old_asset_tag"],
			"new_asset_tag"     => $postData["new_asset_tag"],
			"previous_username" => $postData["previous_username"],
			"previous_hod"      => $postData["previous_hod"],
			"user_cmpy"         => $postData["usercmpy"], 
			"current_username"  => $postData["current_username"], 
			"current_hod"       => $postData["current_hod"],
			"lob"               => $postData["lob"],
			"department"        => $postData["department"],
			"location"          => $postData["location"],
			"asset_type"        => $postData["asset_type"],
			"manufacturer"	    => $postData["manufacturer"],
			"model_no"          => $postData["model_no"],
			"serial_no"         => $postData["serial_no"]	,
			"processor"         => $postData["processor"] ,
			"ram"               => $postData["ram"],
			"hdd"               => $postData["hdd"],
			"cd_dvd"            => $postData["cd_dvd"] ,
			"os_types"          => $postData["os_types"],
			"os_key"	        => $postData["os_key"],
			"ms_office"	        => $postData["ms_office"],
			"created_dt"        => date("Y-m-d H:i:s"),
			"monitor_name"      => $postData["monitor"],
			"monitor_sr"        => $postData["monitor_sr"],
			"monitor_asset_tag" => $postData["monitor_asset_tag"],
			"keyboard"	        => $postData["keyboard"],
			"mouse"              => $postData["mouse"],
		);
		
		$this->db->insert("asset_assigned",$data);
		$insert_id = $this->db->insert_id();
		if($this->db->affected_rows() >0){
			foreach($postData['software'] as $software){
				$data_software[$i]['asset_assigned_id'] = $insert_id;
				$data_software[$i]['extra_sw_name'] = $software;
				$i++;	
			}
			foreach($postData['software_exp'] as $software){
				$data_software[$j]['extra_sw_exp_date'] = date("Y-m-d",strtotime($software));
				$j++;	
			}
			foreach($postData['software_procure'] as $software){
				$data_software[$k]['extra_sw_proc_date'] = date("Y-m-d",strtotime($software));
				$k++;	
			}
			
			foreach($postData['hardware'] as $hardware){
				
				$data_hardware[$m]['asset_assigned_id'] = $insert_id;
				$data_hardware[$m]['extra_hw'] = $hardware;
				$m++;	
			}
			//software_assigned_in
			$this->db->insert_batch('software_assigned_in', $data_software);
			//harware_assigned_in	
			$this->db->insert_batch('hardware_assigned_in', $data_hardware);
			return true;
		}
		else{
		   return false;	
		}
	}
	//inventory code start ***********************************************
	public function add_inv_asset_history($add_asset_his){
		$this->db->insert('inv_add_asset_history',$add_asset_his);
	}
	
	public function add_inv_sub_asset_history($add_sub_asset_his){
		$this->db->insert('inv_sub_asset_history',$add_sub_asset_his);
	}
	
	public function add_inv_assign_asset_for_emp_history($add_assign_asset_his){
		$this->db->insert('inv_assign_asset_for_emp_history',$add_assign_asset_his);
	}
	public function edit_asset_history($edit_asset_history){
		$this->db->insert('inv_add_asset_history',$edit_asset_history);
	}
		
	//inventory code end ***********************************************
}	
	