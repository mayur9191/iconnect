<?php
class Admin_Inventory_model extends CI_Model {
	
	public function __Construct(){
       parent::__Construct();
	}
	public function get_admin_LOB_by_ID($lobID){
		$query = $this->db->query("SELECT * FROM admin_inv_lob where lob_id=$lobID");
			if($query->num_rows() > 0){
				$returnarray = $query->row();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
	}
	public function add_asset($assetAry){
		$returnStatus = 0;
		if($this->db->insert('e_admin_inventory',$assetAry)){
			$returnStatus = 1;
		}
		return $returnStatus;
	}
	
	public function get_all_adminasset(){
		$qry = $this->db->select("*")->from('e_admin_inventory')->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_asset($assetId){
		$qry = $this->db->select("*")->from('e_admin_inventory')->where('id',$assetId)->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->row();
		}else{
			$returnAry = array();
		}
		return $returnAry;
		
	}
	
	public function get_asset_history($assetId){
		$qry = $this->db->select("*")->from('admin_asset_return_history')->where('asset_id',$assetId)->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
		
	}
	
	public function admin_asset_assign($assetAry,$assetId){
		$returnStatus = 0;
		$this->db->where('id',$assetId);
		if($this->db->update('e_admin_inventory',$assetAry)){
			$returnStatus = 1;
		}
		return $returnStatus;
	}
	
	public function get_all_adminassignasset(){
		$qry = $this->db->select("*")->from('e_admin_inventory')->where('availablity_status','assign')->order_by("asset_type","ASC")->get();
		if($qry->num_rows() > 0){
			$returnAry = $qry->result();
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function get_adminasset_by_id($asset_id=0){
		if($asset_id > 0){
			$qry = $this->db->select("*")->from('e_admin_inventory')->where("id",$asset_id)->get();
			$returnAry = $qry->row();
			
		}else{
			$returnAry = array();
		}
		return $returnAry;
	}
	
	public function edit_admin_asset($assetAry,$assetId){
		
		$returnStatus = 0;
		$this->db->where('id',$assetId);
		if($this->db->update('e_admin_inventory',$assetAry)){
			$returnStatus = 1;
		}
		return $returnStatus;
	}
	
	public function addReturn_admin_asset($retrunAssetarray){
		$returnStatus = 0;
		if($this->db->insert('admin_asset_return_history',$retrunAssetarray)){
			$returnStatus = 1;
		}
		return $returnStatus;
	}
	//infra
	public function getMobileVTSCountInfra(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Infrastructures' AND asset_type='Mobile (VTS)'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getMobileCountInfra(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Infrastructures' AND asset_type='Mobile SIM card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getDataCardCountInfra(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Infrastructures' AND asset_type='Data Card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getTollCountInfra(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Infrastructures' AND asset_type='Toll free'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	public function non_accepted_assets_user(){
			$query = $this->db->query("SELECT admin_inventory.asset_type,assetaccept.emp_name,assetaccept.emp_email,assetaccept.emp_code,assetaccept.assign_date,assetaccept.asset_id FROM iconnect.admin_asset_acept as assetaccept 
LEFT JOIN e_admin_inventory as admin_inventory ON assetaccept.asset_id = admin_inventory.id
where assetaccept.accept=0");
			if($query->num_rows() > 0){
				$returnarray = $query->result();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
	}
	
	public function get_non_accepted_assets_user($asset_id){
		$query = $this->db->query("SELECT admin_inventory.*,assetaccept.emp_name,assetaccept.emp_email,assetaccept.emp_code,assetaccept.assign_date,assetaccept.asset_id FROM iconnect.admin_asset_acept as assetaccept 
LEFT JOIN e_admin_inventory as admin_inventory ON assetaccept.asset_id = admin_inventory.id
where assetaccept.asset_id=$asset_id");
			if($query->num_rows() > 0){
				$returnarray = $query->result();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
	}
	
	public function get_user_email_byassetID($assetID){
		$query = $this->db->query("SELECT admin_inventory.*,assetaccept.emp_name,assetaccept.emp_email,assetaccept.emp_code,assetaccept.assign_date,assetaccept.asset_id FROM iconnect.admin_asset_acept as assetaccept 
LEFT JOIN e_admin_inventory as admin_inventory ON assetaccept.asset_id = admin_inventory.id
where assetaccept.asset_id=$assetID");
			if($query->num_rows() > 0){
				$returnarray = $query->row();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
	}
	
	public function get_admin_LOB(){
		
			$qry = $this->db->select("*")->from('admin_inv_lob')->where("lob_is_active",1)->get();
			$returnAry = $qry->result();
			
		
		return $returnAry;
	}
	public function get_admin_asset_type(){
		$qry = $this->db->select("*")->from('admin_asset_type')->where("asset_type_is_active",1)->get();
		$returnAry = $qry->result();
		
		return $returnAry;
		
	}
	public function getLOB_MIS($lobID){
		$query = $this->db->select("*")->from('e_admin_inventory')->where("user_lob_id",$lobID)->get();
			if($query->num_rows() > 0){
				$returnarray = $query->result();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
	}
	
	public function getLOB_HOD($lobID){
		
		$query = $this->db->query("SELECT lob_hod FROM admin_inv_lob where lob_id=$lobID");
			if($query->num_rows() > 0){
				$returnarray = $query->row();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
	}
	
	//Logistics
	
	
	public function getMobileVTSCountLogistics(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Logistics' AND asset_type='Mobile (VTS)'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getMobileCountLogistics(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Logistics' AND asset_type='Mobile SIM card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getDataCardCountLogistics(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Logistics' AND asset_type='Data Card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getTollCountLogistics(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Logistics' AND asset_type='Toll free'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	//Projects
	
	public function getMobileVTSCountProjects(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Projects' AND asset_type='Mobile (VTS)'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getMobileCountProjects(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Projects' AND asset_type='Mobile SIM card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getDataCardCountProjects(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Projects' AND asset_type='Data Card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getTollCountProjects(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Projects' AND asset_type='Toll free'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	//Renewables
	
	public function getMobileVTSCountRenewables(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Renewables' AND asset_type='Mobile (VTS)'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getMobileCountRenewables(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Renewables' AND asset_type='Mobile SIM card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getDataCardCountRenewables(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Renewables' AND asset_type='Data Card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getTollCountRenewables(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Renewables' AND asset_type='Toll free'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	//Group
	
	public function getMobileVTSCountGroup(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Group' AND asset_type='Mobile (VTS)'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getMobileCountGroup(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Group' AND asset_type='Mobile SIM card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getDataCardCountGroup(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Group' AND asset_type='Data Card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getTollCountGroup(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Group' AND asset_type='Toll free'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	//Individual (IOCR)
	
	public function getMobileVTSCountIOCR(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Individual (IOCR)' AND asset_type='Mobile (VTS)'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getMobileCountIOCR(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Individual (IOCR)' AND asset_type='Mobile SIM card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getDataCardCountIOCR(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Individual (IOCR)' AND asset_type='Data Card'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getTollCountIOCR(){
		$query = $this->db->query("SELECT * from e_admin_inventory where purchased_LOB ='Individual (IOCR)' AND asset_type='Toll free'");
		$returnarray = $query->num_rows();
			
			return $returnarray;
	}
	
	public function getLOB_HODByName($lobName){
		
		$query = $this->db->query("SELECT lob_hod FROM admin_inv_lob where lob_name='$lobName'");
			if($query->num_rows() > 0){
				$returnarray = $query->row();
			}else{
				$returnarray = array();
			}
			
			return $returnarray;
			
	}
	
}	
	