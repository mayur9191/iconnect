<?php
class Dailytask_model extends CI_Model {

	public function __Construct()
	{
       parent::__Construct();
	}
	public function save_dailytask($emp_info)
	{	
		if(!empty($emp_info)){
			$postData = $this->input->post();
			$emp_code = $emp_info['employeeid'];
			if($emp_code=='SG0282'){
				$itss = 1;
			}elseif($emp_code=='SG0494'){
				$itss = 1;
			}else{
				$itss = 0;
			}
			$data = array(
				"emp_code"      => $emp_code,
				"project_name"  => $postData['project_name'],
				"start_date"    => date('Y-m-d',strtotime($postData['start_date'])),
				"end_date"      => date('Y-m-d',strtotime($postData['end_date'])),
				"task_activity" => $postData['task_activity'],
				"task_priority" => $postData['task_priority'],
				"task_status"   => $postData['task_status'],
				"remarks"       => $postData['remarks'],
				"task_hrs"		=> $postData['hrs_time'],
				"task_min"		=> $postData['min_time'],
				"itss"			=> $itss
			);
			if(!empty($postData['task_id'])){
				$this->db->update("daily_task",$data,array('task_id'=>$postData['task_id']));
			}
			else{
				$data["created_date"] = date("Y-m-d H:i:s");
				$this->db->insert("daily_task",$data);	
			}
			if($this->db->affected_rows()>0){
				return true;
			}
			else{
			   return false;			
			}
		}
		else{
			return false;
		}
		
	}
	public function get_dailytask($emp_info)
	{
		$query = $this->db->select("*")->from("daily_task")->where("emp_code",$emp_info['employeeid'])->where("task_is_active",'1')->order_by('task_status')->order_by('start_date','DESC')->get();
		if($query->num_rows()>0){
			return $query->result_array();
		}
		else{
			return array();
		}
	}
	
	public function get_dailytask1($emp_code="", $filter_date="")
	{
		if($filter_date !=''){
			$query = $this->db->select("*")->from("daily_task")->where("emp_code",$emp_code)->where('start_date',date("Y-m-d",strtotime($filter_date)))->where("task_is_active",'1')->order_by('task_status')->order_by('start_date','DESC')->get();
		}else{
			$query = $this->db->select("*")->from("daily_task")->where("emp_code",$emp_code)->where("task_is_active",'1')->order_by('task_status')->order_by('start_date','DESC')->get();
		}
		if($query->num_rows()>0){
			return $query->result_array();
		}
		else{
			return array();
		}
	}
	
	public function get_all_emp_data(){
		$query = $this->db->query("SELECT u.u_name,d.* FROM daily_task d LEFT JOIN users u ON (u.u_employeeid=d.emp_code) WHERE d.task_is_active=1");
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	
	public function get_NelArp_emp_data(){
		$query = $this->db->query("SELECT u.u_name,d.* FROM daily_task d LEFT JOIN users u ON (u.u_employeeid=d.emp_code) WHERE d.task_is_active=1 AND d.itss=1");
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	
	public function get_employee_report(){
		/*$query = $this->db->select("emp_code,COUNT(*) as count")->from("daily_task")->group_by("emp_code")->where("task_is_active",'1')->get();*/
		$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.task_is_active",'1')->order_by('count(*)','ASC')->get();
		
		if($query->num_rows()>0){
			return $query->result_array();
		} else {
			return array();
		}
	}
	
	public function get_employee_report1($filter_type='', $filter_date=''){
		if(isset($filter_type)){
			if($filter_type == 'all'){
				$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.task_is_active",'1')->order_by('count(*)','ASC')->get();
			}
			if($filter_type == 'by_date'){
				$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.task_is_active",'1')->where('start_date',date("Y-m-d",strtotime($filter_date)))->order_by('count(*)','ASC')->get();
			}
		}else{
			$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.task_is_active",'1')->order_by('count(*)','ASC')->get();
		}
		//print_r($postData);
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0){
			return $query->result_array();
		} else {
			return array();
		}
	}
	
	public function get_employee_reportNel($filter_type='', $filter_date=''){
		if(isset($filter_type)){
			if($filter_type == 'all'){
				$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.itss",'1')->order_by('count(*)','ASC')->get();
			}
			if($filter_type == 'by_date'){
				$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.itss",'1')->where('start_date',date("Y-m-d",strtotime($filter_date)))->order_by('count(*)','ASC')->get();
			}
		}else{
			$query = $this->db->select("users.u_name,count(*) as count,daily_task.emp_code")->from("daily_task")->join("users","users.u_employeeid=daily_task.emp_code")->group_by("daily_task.emp_code")->where("daily_task.itss",'1')->order_by('count(*)','ASC')->get();
		}
		//print_r($postData);
		//echo $this->db->last_query();exit;
		if($query->num_rows()>0){
			return $query->result_array();
		} else {
			return array();
		}
	}
	
	public function get_dates(){
		$qry = $this->db->query("SELECT start_date FROM daily_task GROUP BY start_date");
		return $qry->result();
	}
	
	public function get_outlookemail(){
		$qry = $this->db->query("SELECT * FROM outlook_email WHERE email_from !='@' ORDER BY outlook_email_id DESC");
		return $qry->result();
	}
	
	public function get_all_emp_dataCurrentYear(){
		$query = $this->db->query("SELECT u.u_name,d.* FROM daily_task d LEFT JOIN users u ON (u.u_employeeid=d.emp_code) WHERE YEAR(d.start_date) = YEAR(CURDATE()) AND d.task_is_active=1");
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	
	public function get_datesCurrentYear(){
		$qry = $this->db->query("SELECT start_date FROM daily_task where YEAR(start_date) = YEAR(CURDATE()) GROUP BY start_date");
		return $qry->result();
	}
	
	public function get_all_emp_dataLastYear(){
		$query = $this->db->query("SELECT u.u_name,d.* FROM daily_task d LEFT JOIN users u ON (u.u_employeeid=d.emp_code) WHERE YEAR(d.start_date) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR)) AND d.task_is_active=1");
		if($query->num_rows()>0){
			return $query->result_array();
		}
	}
	
	public function get_datesLastYear(){
		$qry = $this->db->query("SELECT start_date FROM daily_task where YEAR(start_date) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR)) GROUP BY start_date");
		return $qry->result();
	}
	
}	